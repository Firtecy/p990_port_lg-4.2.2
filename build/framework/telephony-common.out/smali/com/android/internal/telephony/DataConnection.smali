.class public abstract Lcom/android/internal/telephony/DataConnection;
.super Lcom/android/internal/util/StateMachine;
.source "DataConnection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/DataConnection$1;,
        Lcom/android/internal/telephony/DataConnection$DcDisconnectionErrorCreatingConnection;,
        Lcom/android/internal/telephony/DataConnection$DcDisconnectingState;,
        Lcom/android/internal/telephony/DataConnection$DcActiveState;,
        Lcom/android/internal/telephony/DataConnection$DcActivatingState;,
        Lcom/android/internal/telephony/DataConnection$DcInactiveState;,
        Lcom/android/internal/telephony/DataConnection$DcDefaultState;,
        Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;,
        Lcom/android/internal/telephony/DataConnection$CallSetupException;,
        Lcom/android/internal/telephony/DataConnection$FailCause;,
        Lcom/android/internal/telephony/DataConnection$DisconnectParams;,
        Lcom/android/internal/telephony/DataConnection$ConnectionParams;
    }
.end annotation


# static fields
.field protected static final BASE:I = 0x40000

#the value of this static final field might be set in the static constructor
.field private static final CIQ_Support:Z = false

.field private static final CMD_TO_STRING_COUNT:I = 0xa

.field protected static final DBG:Z = true

.field protected static final EVENT_CONNECT:I = 0x40000

.field protected static final EVENT_DEACTIVATE_DONE:I = 0x40003

.field protected static final EVENT_DISCONNECT:I = 0x40004

.field protected static final EVENT_DISCONNECT_ALL:I = 0x40006

.field protected static final EVENT_GET_LAST_FAIL_DONE:I = 0x40002

.field protected static final EVENT_GET_PCSCF_ADDRESS_DONE:I = 0x40007

.field protected static final EVENT_GET_PCSCF_ADDRESS_FAIL:I = 0x40008

.field protected static final EVENT_LOG_BAD_DNS_ADDRESS:I = 0xc3b4

.field protected static final EVENT_QOS_CHANGED:I = 0x40009

.field protected static final EVENT_RIL_CONNECTED:I = 0x40005

.field protected static final EVENT_SETUP_DATA_CONNECTION_DONE:I = 0x40001

.field protected static final IP_ADDRESS:I = 0x0

.field protected static final NULL_IP:Ljava/lang/String; = "0.0.0.0"

.field protected static final PCSCF_ADDRESS_IPV4:I = 0x1

.field protected static final PCSCF_ADDRESS_IPV6:I = 0x2

.field protected static final RIL_MAX_FAIL:I = 0x6

.field protected static final VDBG:Z

.field protected static mCount:Ljava/util/concurrent/atomic/AtomicInteger;

.field public static rilFailCount:I

.field private static sCmdToString:[Ljava/lang/String;


# instance fields
.field protected cid:I

.field protected createTime:J

.field protected lastFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

.field protected lastFailTime:J

.field protected mAc:Lcom/android/internal/util/AsyncChannel;

.field private mActivatingState:Lcom/android/internal/telephony/DataConnection$DcActivatingState;

.field private mActiveState:Lcom/android/internal/telephony/DataConnection$DcActiveState;

.field protected mApn:Lcom/android/internal/telephony/DataProfile;

.field protected mApnList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/ApnContext;",
            ">;"
        }
    .end annotation
.end field

.field protected mCapabilities:Landroid/net/LinkCapabilities;

.field private mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

.field private mDefaultState:Lcom/android/internal/telephony/DataConnection$DcDefaultState;

.field private mDisconnectingErrorCreatingConnection:Lcom/android/internal/telephony/DataConnection$DcDisconnectionErrorCreatingConnection;

.field private mDisconnectingState:Lcom/android/internal/telephony/DataConnection$DcDisconnectingState;

.field private mId:I

.field private mInactiveState:Lcom/android/internal/telephony/DataConnection$DcInactiveState;

.field protected mLinkProperties:Landroid/net/LinkProperties;

.field mReconnectIntent:Landroid/app/PendingIntent;

.field protected mRefCount:I

.field private mRetryMgr:Lcom/android/internal/telephony/RetryManager;

.field protected mRetryOverride:I

.field protected mRilVersion:I

.field protected mTag:I

.field protected pcscfAddr_ipv4:[Ljava/lang/String;

.field protected pcscfAddr_ipv6:[Ljava/lang/String;

.field protected phone:Lcom/android/internal/telephony/PhoneBase;

.field userData:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 86
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    #@3
    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    #@6
    sput-object v0, Lcom/android/internal/telephony/DataConnection;->mCount:Ljava/util/concurrent/atomic/AtomicInteger;

    #@8
    .line 105
    sput v2, Lcom/android/internal/telephony/DataConnection;->rilFailCount:I

    #@a
    .line 109
    const-string v0, "persist.lgiqc.ext"

    #@c
    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@f
    move-result v0

    #@10
    sput-boolean v0, Lcom/android/internal/telephony/DataConnection;->CIQ_Support:Z

    #@12
    .line 357
    const/16 v0, 0xa

    #@14
    new-array v0, v0, [Ljava/lang/String;

    #@16
    sput-object v0, Lcom/android/internal/telephony/DataConnection;->sCmdToString:[Ljava/lang/String;

    #@18
    .line 359
    sget-object v0, Lcom/android/internal/telephony/DataConnection;->sCmdToString:[Ljava/lang/String;

    #@1a
    const-string v1, "EVENT_CONNECT"

    #@1c
    aput-object v1, v0, v2

    #@1e
    .line 360
    sget-object v0, Lcom/android/internal/telephony/DataConnection;->sCmdToString:[Ljava/lang/String;

    #@20
    const/4 v1, 0x1

    #@21
    const-string v2, "EVENT_SETUP_DATA_CONNECTION_DONE"

    #@23
    aput-object v2, v0, v1

    #@25
    .line 362
    sget-object v0, Lcom/android/internal/telephony/DataConnection;->sCmdToString:[Ljava/lang/String;

    #@27
    const/4 v1, 0x2

    #@28
    const-string v2, "EVENT_GET_LAST_FAIL_DONE"

    #@2a
    aput-object v2, v0, v1

    #@2c
    .line 363
    sget-object v0, Lcom/android/internal/telephony/DataConnection;->sCmdToString:[Ljava/lang/String;

    #@2e
    const/4 v1, 0x3

    #@2f
    const-string v2, "EVENT_DEACTIVATE_DONE"

    #@31
    aput-object v2, v0, v1

    #@33
    .line 364
    sget-object v0, Lcom/android/internal/telephony/DataConnection;->sCmdToString:[Ljava/lang/String;

    #@35
    const/4 v1, 0x4

    #@36
    const-string v2, "EVENT_DISCONNECT"

    #@38
    aput-object v2, v0, v1

    #@3a
    .line 365
    sget-object v0, Lcom/android/internal/telephony/DataConnection;->sCmdToString:[Ljava/lang/String;

    #@3c
    const/4 v1, 0x5

    #@3d
    const-string v2, "EVENT_RIL_CONNECTED"

    #@3f
    aput-object v2, v0, v1

    #@41
    .line 366
    sget-object v0, Lcom/android/internal/telephony/DataConnection;->sCmdToString:[Ljava/lang/String;

    #@43
    const/4 v1, 0x6

    #@44
    const-string v2, "EVENT_DISCONNECT_ALL"

    #@46
    aput-object v2, v0, v1

    #@48
    .line 368
    sget-object v0, Lcom/android/internal/telephony/DataConnection;->sCmdToString:[Ljava/lang/String;

    #@4a
    const/4 v1, 0x7

    #@4b
    const-string v2, "EVENT_GET_PCSCF_ADDRESS_DONE"

    #@4d
    aput-object v2, v0, v1

    #@4f
    .line 369
    sget-object v0, Lcom/android/internal/telephony/DataConnection;->sCmdToString:[Ljava/lang/String;

    #@51
    const/16 v1, 0x8

    #@53
    const-string v2, "EVENT_GET_PCSCF_ADDRESS_FAIL"

    #@55
    aput-object v2, v0, v1

    #@57
    .line 372
    sget-object v0, Lcom/android/internal/telephony/DataConnection;->sCmdToString:[Ljava/lang/String;

    #@59
    const/16 v1, 0x9

    #@5b
    const-string v2, "EVENT_QOS_CHANGED"

    #@5d
    aput-object v2, v0, v1

    #@5f
    .line 375
    return-void
.end method

.method protected constructor <init>(Lcom/android/internal/telephony/PhoneBase;Ljava/lang/String;ILcom/android/internal/telephony/RetryManager;Lcom/android/internal/telephony/DataConnectionTracker;)V
    .registers 10
    .parameter "phone"
    .parameter "name"
    .parameter "id"
    .parameter "rm"
    .parameter "dct"

    #@0
    .prologue
    const/4 v3, 0x5

    #@1
    const/4 v2, -0x1

    #@2
    const/4 v1, 0x0

    #@3
    .line 417
    invoke-direct {p0, p2}, Lcom/android/internal/util/StateMachine;-><init>(Ljava/lang/String;)V

    #@6
    .line 89
    iput-object v1, p0, Lcom/android/internal/telephony/DataConnection;->mApnList:Ljava/util/List;

    #@8
    .line 90
    iput-object v1, p0, Lcom/android/internal/telephony/DataConnection;->mReconnectIntent:Landroid/app/PendingIntent;

    #@a
    .line 92
    iput-object v1, p0, Lcom/android/internal/telephony/DataConnection;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@c
    .line 392
    iput v2, p0, Lcom/android/internal/telephony/DataConnection;->mRilVersion:I

    #@e
    .line 394
    new-instance v0, Landroid/net/LinkProperties;

    #@10
    invoke-direct {v0}, Landroid/net/LinkProperties;-><init>()V

    #@13
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mLinkProperties:Landroid/net/LinkProperties;

    #@15
    .line 395
    new-instance v0, Landroid/net/LinkCapabilities;

    #@17
    invoke-direct {v0}, Landroid/net/LinkCapabilities;-><init>()V

    #@1a
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mCapabilities:Landroid/net/LinkCapabilities;

    #@1c
    .line 399
    iput v2, p0, Lcom/android/internal/telephony/DataConnection;->mRetryOverride:I

    #@1e
    .line 1247
    new-instance v0, Lcom/android/internal/telephony/DataConnection$DcDefaultState;

    #@20
    invoke-direct {v0, p0, v1}, Lcom/android/internal/telephony/DataConnection$DcDefaultState;-><init>(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/telephony/DataConnection$1;)V

    #@23
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mDefaultState:Lcom/android/internal/telephony/DataConnection$DcDefaultState;

    #@25
    .line 1378
    new-instance v0, Lcom/android/internal/telephony/DataConnection$DcInactiveState;

    #@27
    invoke-direct {v0, p0, v1}, Lcom/android/internal/telephony/DataConnection$DcInactiveState;-><init>(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/telephony/DataConnection$1;)V

    #@2a
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mInactiveState:Lcom/android/internal/telephony/DataConnection$DcInactiveState;

    #@2c
    .line 1591
    new-instance v0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;

    #@2e
    invoke-direct {v0, p0, v1}, Lcom/android/internal/telephony/DataConnection$DcActivatingState;-><init>(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/telephony/DataConnection$1;)V

    #@31
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mActivatingState:Lcom/android/internal/telephony/DataConnection$DcActivatingState;

    #@33
    .line 2085
    new-instance v0, Lcom/android/internal/telephony/DataConnection$DcActiveState;

    #@35
    invoke-direct {v0, p0, v1}, Lcom/android/internal/telephony/DataConnection$DcActiveState;-><init>(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/telephony/DataConnection$1;)V

    #@38
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mActiveState:Lcom/android/internal/telephony/DataConnection$DcActiveState;

    #@3a
    .line 2132
    new-instance v0, Lcom/android/internal/telephony/DataConnection$DcDisconnectingState;

    #@3c
    invoke-direct {v0, p0, v1}, Lcom/android/internal/telephony/DataConnection$DcDisconnectingState;-><init>(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/telephony/DataConnection$1;)V

    #@3f
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mDisconnectingState:Lcom/android/internal/telephony/DataConnection$DcDisconnectingState;

    #@41
    .line 2177
    new-instance v0, Lcom/android/internal/telephony/DataConnection$DcDisconnectionErrorCreatingConnection;

    #@43
    invoke-direct {v0, p0, v1}, Lcom/android/internal/telephony/DataConnection$DcDisconnectionErrorCreatingConnection;-><init>(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/telephony/DataConnection$1;)V

    #@46
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mDisconnectingErrorCreatingConnection:Lcom/android/internal/telephony/DataConnection$DcDisconnectionErrorCreatingConnection;

    #@48
    .line 418
    const/16 v0, 0x64

    #@4a
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnection;->setLogRecSize(I)V

    #@4d
    .line 419
    const-string v0, "DataConnection constructor E"

    #@4f
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@52
    .line 420
    iput-object p1, p0, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@54
    .line 421
    iput-object p5, p0, Lcom/android/internal/telephony/DataConnection;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@56
    .line 422
    iput p3, p0, Lcom/android/internal/telephony/DataConnection;->mId:I

    #@58
    .line 423
    iput-object p4, p0, Lcom/android/internal/telephony/DataConnection;->mRetryMgr:Lcom/android/internal/telephony/RetryManager;

    #@5a
    .line 424
    iput v2, p0, Lcom/android/internal/telephony/DataConnection;->cid:I

    #@5c
    .line 427
    new-array v0, v3, [Ljava/lang/String;

    #@5e
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnection;->pcscfAddr_ipv4:[Ljava/lang/String;

    #@60
    .line 428
    new-array v0, v3, [Ljava/lang/String;

    #@62
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnection;->pcscfAddr_ipv6:[Ljava/lang/String;

    #@64
    .line 431
    const/4 v0, 0x0

    #@65
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnection;->setDbg(Z)V

    #@68
    .line 432
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mDefaultState:Lcom/android/internal/telephony/DataConnection$DcDefaultState;

    #@6a
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnection;->addState(Lcom/android/internal/util/State;)V

    #@6d
    .line 433
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mInactiveState:Lcom/android/internal/telephony/DataConnection$DcInactiveState;

    #@6f
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnection;->mDefaultState:Lcom/android/internal/telephony/DataConnection$DcDefaultState;

    #@71
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/DataConnection;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@74
    .line 434
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mActivatingState:Lcom/android/internal/telephony/DataConnection$DcActivatingState;

    #@76
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnection;->mDefaultState:Lcom/android/internal/telephony/DataConnection$DcDefaultState;

    #@78
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/DataConnection;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@7b
    .line 435
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mActiveState:Lcom/android/internal/telephony/DataConnection$DcActiveState;

    #@7d
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnection;->mDefaultState:Lcom/android/internal/telephony/DataConnection$DcDefaultState;

    #@7f
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/DataConnection;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@82
    .line 436
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mDisconnectingState:Lcom/android/internal/telephony/DataConnection$DcDisconnectingState;

    #@84
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnection;->mDefaultState:Lcom/android/internal/telephony/DataConnection$DcDefaultState;

    #@86
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/DataConnection;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@89
    .line 437
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mDisconnectingErrorCreatingConnection:Lcom/android/internal/telephony/DataConnection$DcDisconnectionErrorCreatingConnection;

    #@8b
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnection;->mDefaultState:Lcom/android/internal/telephony/DataConnection$DcDefaultState;

    #@8d
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/DataConnection;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@90
    .line 438
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mInactiveState:Lcom/android/internal/telephony/DataConnection$DcInactiveState;

    #@92
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnection;->setInitialState(Lcom/android/internal/util/State;)V

    #@95
    .line 440
    new-instance v0, Ljava/util/ArrayList;

    #@97
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@9a
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mApnList:Ljava/util/List;

    #@9c
    .line 441
    const-string v0, "DataConnection constructor X"

    #@9e
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@a1
    .line 442
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/telephony/DataConnection;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 82
    invoke-direct {p0}, Lcom/android/internal/telephony/DataConnection;->shutDown()V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/internal/telephony/DataConnection;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    iget v0, p0, Lcom/android/internal/telephony/DataConnection;->mId:I

    #@2
    return v0
.end method

.method static synthetic access$1000(Lcom/android/internal/telephony/DataConnection;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnection;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$1200()Z
    .registers 1

    #@0
    .prologue
    .line 82
    sget-boolean v0, Lcom/android/internal/telephony/DataConnection;->CIQ_Support:Z

    #@2
    return v0
.end method

.method static synthetic access$1300(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/telephony/DataConnection$DisconnectParams;Z)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 82
    invoke-direct {p0, p1, p2}, Lcom/android/internal/telephony/DataConnection;->notifyDisconnectCompleted(Lcom/android/internal/telephony/DataConnection$DisconnectParams;Z)V

    #@3
    return-void
.end method

.method static synthetic access$1400(Lcom/android/internal/telephony/DataConnection;)Lcom/android/internal/telephony/DataConnection$DcActivatingState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mActivatingState:Lcom/android/internal/telephony/DataConnection$DcActivatingState;

    #@2
    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnection;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$1700(Lcom/android/internal/telephony/DataConnection;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnection;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$1800(Lcom/android/internal/telephony/DataConnection;Landroid/os/AsyncResult;)Lcom/android/internal/telephony/DataCallState$SetupResult;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/DataConnection;->onSetupConnectionCompleted(Landroid/os/AsyncResult;)Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1900(Lcom/android/internal/telephony/DataConnection;)Lcom/android/internal/telephony/DataConnection$DcActiveState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mActiveState:Lcom/android/internal/telephony/DataConnection$DcActiveState;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/internal/telephony/DataConnection;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnection;->quit()V

    #@3
    return-void
.end method

.method static synthetic access$2000(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnection;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$2100(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnection;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$2200(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnection;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$2300(Lcom/android/internal/telephony/DataConnection;Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/DataConnection;->tearDownData(Ljava/lang/Object;)V

    #@3
    return-void
.end method

.method static synthetic access$2400(Lcom/android/internal/telephony/DataConnection;)Lcom/android/internal/telephony/DataConnection$DcDisconnectionErrorCreatingConnection;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mDisconnectingErrorCreatingConnection:Lcom/android/internal/telephony/DataConnection$DcDisconnectionErrorCreatingConnection;

    #@2
    return-object v0
.end method

.method static synthetic access$2500(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnection;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$2600(Lcom/android/internal/telephony/DataConnection;Landroid/os/AsyncResult;)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/DataConnection;->getSuggestedRetryTime(Landroid/os/AsyncResult;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2700(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnection;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$2800(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnection;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$2900(Lcom/android/internal/telephony/DataConnection;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnection;->removeMessages(I)V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/internal/telephony/DataConnection;)Lcom/android/internal/util/IState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/android/internal/telephony/DataConnection;->getCurrentState()Lcom/android/internal/util/IState;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$3000(Lcom/android/internal/telephony/DataConnection;Landroid/os/AsyncResult;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/DataConnection;->onGetPcscfAddressCompleted(Landroid/os/AsyncResult;)V

    #@3
    return-void
.end method

.method static synthetic access$3100(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnection;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$3200(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnection;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$3400(Lcom/android/internal/telephony/DataConnection;)Lcom/android/internal/telephony/DataConnection$DcDisconnectingState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mDisconnectingState:Lcom/android/internal/telephony/DataConnection$DcDisconnectingState;

    #@2
    return-object v0
.end method

.method static synthetic access$3500(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnection;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$3600(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnection;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$3700(Lcom/android/internal/telephony/DataConnection;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnection;->removeMessages(I)V

    #@3
    return-void
.end method

.method static synthetic access$3900(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnection;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/internal/telephony/DataConnection;)Lcom/android/internal/telephony/DataConnection$DcInactiveState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mInactiveState:Lcom/android/internal/telephony/DataConnection$DcInactiveState;

    #@2
    return-object v0
.end method

.method static synthetic access$4100(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnection;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/telephony/DataCallState;)Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/DataConnection;->updateLinkProperty(Lcom/android/internal/telephony/DataCallState;)Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$600(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnection;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/android/internal/telephony/DataConnection;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 82
    invoke-direct {p0}, Lcom/android/internal/telephony/DataConnection;->restartRild()V

    #@3
    return-void
.end method

.method static synthetic access$800(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/telephony/DataConnection$ConnectionParams;Lcom/android/internal/telephony/DataConnection$FailCause;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 82
    invoke-direct {p0, p1, p2}, Lcom/android/internal/telephony/DataConnection;->notifyConnectCompleted(Lcom/android/internal/telephony/DataConnection$ConnectionParams;Lcom/android/internal/telephony/DataConnection$FailCause;)V

    #@3
    return-void
.end method

.method static synthetic access$900(Lcom/android/internal/telephony/DataConnection;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/DataConnection;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method protected static cmdToString(I)Ljava/lang/String;
    .registers 2
    .parameter "cmd"

    #@0
    .prologue
    .line 377
    const/high16 v0, 0x4

    #@2
    sub-int/2addr p0, v0

    #@3
    .line 378
    if-ltz p0, :cond_f

    #@5
    sget-object v0, Lcom/android/internal/telephony/DataConnection;->sCmdToString:[Ljava/lang/String;

    #@7
    array-length v0, v0

    #@8
    if-ge p0, v0, :cond_f

    #@a
    .line 379
    sget-object v0, Lcom/android/internal/telephony/DataConnection;->sCmdToString:[Ljava/lang/String;

    #@c
    aget-object v0, v0, p0

    #@e
    .line 381
    :goto_e
    return-object v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method

.method private getSuggestedRetryTime(Landroid/os/AsyncResult;)I
    .registers 5
    .parameter "ar"

    #@0
    .prologue
    .line 917
    const/4 v1, -0x1

    #@1
    .line 918
    .local v1, retry:I
    iget-object v2, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@3
    if-nez v2, :cond_b

    #@5
    .line 919
    iget-object v0, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@7
    check-cast v0, Lcom/android/internal/telephony/DataCallState;

    #@9
    .line 920
    .local v0, response:Lcom/android/internal/telephony/DataCallState;
    iget v1, v0, Lcom/android/internal/telephony/DataCallState;->suggestedRetryTime:I

    #@b
    .line 922
    .end local v0           #response:Lcom/android/internal/telephony/DataCallState;
    :cond_b
    return v1
.end method

.method private isRildState(Ljava/lang/String;)Z
    .registers 5
    .parameter "state"

    #@0
    .prologue
    .line 1018
    const-string v1, "init.svc.ril-daemon"

    #@2
    const-string v2, ""

    #@4
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 1019
    .local v0, daemon_state:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "isRildState init.svc.ril-daemon : "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@1e
    .line 1020
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v1

    #@22
    if-eqz v1, :cond_26

    #@24
    .line 1021
    const/4 v1, 0x1

    #@25
    .line 1023
    :goto_25
    return v1

    #@26
    :cond_26
    const/4 v1, 0x0

    #@27
    goto :goto_25
.end method

.method private notifyConnectCompleted(Lcom/android/internal/telephony/DataConnection$ConnectionParams;Lcom/android/internal/telephony/DataConnection$FailCause;)V
    .registers 8
    .parameter "cp"
    .parameter "cause"

    #@0
    .prologue
    .line 499
    iget-object v0, p1, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->onCompletedMsg:Landroid/os/Message;

    #@2
    .line 500
    .local v0, connectionCompletedMsg:Landroid/os/Message;
    if-nez v0, :cond_5

    #@4
    .line 519
    :goto_4
    return-void

    #@5
    .line 504
    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@8
    move-result-wide v1

    #@9
    .line 505
    .local v1, timeStamp:J
    iget v3, p0, Lcom/android/internal/telephony/DataConnection;->cid:I

    #@b
    iput v3, v0, Landroid/os/Message;->arg1:I

    #@d
    .line 507
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->NONE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@f
    if-ne p2, v3, :cond_3a

    #@11
    .line 508
    iput-wide v1, p0, Lcom/android/internal/telephony/DataConnection;->createTime:J

    #@13
    .line 509
    invoke-static {v0}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@16
    .line 516
    :goto_16
    new-instance v3, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v4, "notifyConnectionCompleted at "

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    const-string v4, " cause="

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@36
    .line 518
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@39
    goto :goto_4

    #@3a
    .line 511
    :cond_3a
    iput-object p2, p0, Lcom/android/internal/telephony/DataConnection;->lastFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@3c
    .line 512
    iput-wide v1, p0, Lcom/android/internal/telephony/DataConnection;->lastFailTime:J

    #@3e
    .line 513
    new-instance v3, Lcom/android/internal/telephony/DataConnection$CallSetupException;

    #@40
    iget v4, p0, Lcom/android/internal/telephony/DataConnection;->mRetryOverride:I

    #@42
    invoke-direct {v3, v4}, Lcom/android/internal/telephony/DataConnection$CallSetupException;-><init>(I)V

    #@45
    invoke-static {v0, p2, v3}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@48
    goto :goto_16
.end method

.method private notifyDisconnectCompleted(Lcom/android/internal/telephony/DataConnection$DisconnectParams;Z)V
    .registers 14
    .parameter "dp"
    .parameter "sendAll"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v9, 0x1

    #@2
    .line 529
    const/4 v1, 0x0

    #@3
    .line 530
    .local v1, alreadySent:Lcom/android/internal/telephony/ApnContext;
    const/4 v4, 0x0

    #@4
    .line 532
    .local v4, reason:Ljava/lang/String;
    iget-object v7, p1, Lcom/android/internal/telephony/DataConnection$DisconnectParams;->onCompletedMsg:Landroid/os/Message;

    #@6
    if-eqz v7, :cond_1c

    #@8
    .line 534
    iget-object v3, p1, Lcom/android/internal/telephony/DataConnection$DisconnectParams;->onCompletedMsg:Landroid/os/Message;

    #@a
    .line 535
    .local v3, msg:Landroid/os/Message;
    iget-object v7, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@c
    instance-of v7, v7, Lcom/android/internal/telephony/ApnContext;

    #@e
    if-eqz v7, :cond_14

    #@10
    .line 536
    iget-object v1, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@12
    .end local v1           #alreadySent:Lcom/android/internal/telephony/ApnContext;
    check-cast v1, Lcom/android/internal/telephony/ApnContext;

    #@14
    .line 538
    .restart local v1       #alreadySent:Lcom/android/internal/telephony/ApnContext;
    :cond_14
    iget-object v4, p1, Lcom/android/internal/telephony/DataConnection$DisconnectParams;->reason:Ljava/lang/String;

    #@16
    .line 543
    invoke-static {v3}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@19
    .line 544
    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    #@1c
    .line 546
    .end local v3           #msg:Landroid/os/Message;
    :cond_1c
    if-eqz p2, :cond_47

    #@1e
    .line 547
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnection;->mApnList:Ljava/util/List;

    #@20
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@23
    move-result-object v2

    #@24
    .local v2, i$:Ljava/util/Iterator;
    :cond_24
    :goto_24
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@27
    move-result v7

    #@28
    if-eqz v7, :cond_47

    #@2a
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2d
    move-result-object v0

    #@2e
    check-cast v0, Lcom/android/internal/telephony/ApnContext;

    #@30
    .line 548
    .local v0, a:Lcom/android/internal/telephony/ApnContext;
    if-eq v0, v1, :cond_24

    #@32
    .line 549
    if-eqz v4, :cond_37

    #@34
    invoke-virtual {v0, v4}, Lcom/android/internal/telephony/ApnContext;->setReason(Ljava/lang/String;)V

    #@37
    .line 550
    :cond_37
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnection;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@39
    const v8, 0x4200f

    #@3c
    invoke-virtual {v7, v8, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@3f
    move-result-object v3

    #@40
    .line 552
    .restart local v3       #msg:Landroid/os/Message;
    invoke-static {v3}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@43
    .line 553
    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    #@46
    goto :goto_24

    #@47
    .line 565
    .end local v0           #a:Lcom/android/internal/telephony/ApnContext;
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #msg:Landroid/os/Message;
    :cond_47
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnection;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@49
    iget-boolean v7, v7, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_send_result:Z

    #@4b
    if-nez v7, :cond_8b

    #@4d
    .line 566
    const-string v7, "*** kjyean NotifyDisconnectCompleted() send cpa_onSetupConnectionCompleted intent"

    #@4f
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@52
    .line 567
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnection;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@54
    const/4 v8, 0x0

    #@55
    iput-boolean v8, v7, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_enable:Z

    #@57
    .line 568
    const-string v7, "ril.btdun.dns1"

    #@59
    invoke-static {v7, v10}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@5c
    .line 569
    const-string v7, "ril.btdun.dns2"

    #@5e
    invoke-static {v7, v10}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@61
    .line 571
    const/4 v5, -0x4

    #@62
    .line 572
    .local v5, sfail:I
    if-nez v4, :cond_a2

    #@64
    .line 573
    const/4 v5, -0x4

    #@65
    .line 576
    :goto_65
    new-instance v6, Landroid/content/Intent;

    #@67
    const-string v7, "cpa_onSetupConnectionCompleted"

    #@69
    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@6c
    .line 580
    .local v6, sintent:Landroid/content/Intent;
    const-string v7, "result"

    #@6e
    invoke-virtual {v6, v7, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@71
    .line 581
    const-string v7, "mFailCause"

    #@73
    invoke-virtual {v6, v7, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@76
    .line 582
    const-string v7, "status"

    #@78
    const-string v8, "DISCONNECTED"

    #@7a
    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@7d
    .line 584
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@7f
    iget-object v7, v7, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@81
    invoke-virtual {v7, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@84
    .line 587
    const-string v7, "ril.btdun.send"

    #@86
    const-string v8, "ture"

    #@88
    invoke-static {v7, v8}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@8b
    .line 590
    .end local v5           #sfail:I
    .end local v6           #sintent:Landroid/content/Intent;
    :cond_8b
    new-instance v7, Ljava/lang/StringBuilder;

    #@8d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@90
    const-string v8, "NotifyDisconnectCompleted DisconnectParams="

    #@92
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v7

    #@96
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v7

    #@9a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9d
    move-result-object v7

    #@9e
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@a1
    .line 591
    return-void

    #@a2
    .line 575
    .restart local v5       #sfail:I
    :cond_a2
    const/4 v5, -0x4

    #@a3
    goto :goto_65
.end method

.method private onGetPcscfAddressCompleted(Landroid/os/AsyncResult;)V
    .registers 8
    .parameter "ar"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 1031
    iget-object v2, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@4
    if-eqz v2, :cond_c

    #@6
    .line 1032
    const-string v2, "onGetPcscfAddressCompleted, there is Exception"

    #@8
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@b
    .line 1047
    :cond_b
    :goto_b
    return-void

    #@c
    .line 1035
    :cond_c
    iget-object v2, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@e
    check-cast v2, Ljava/lang/String;

    #@10
    move-object v1, v2

    #@11
    check-cast v1, Ljava/lang/String;

    #@13
    .line 1036
    .local v1, result:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v3, "onGetPcscfAddressCompleted, Result: "

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@29
    .line 1038
    const-string v2, ";"

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@2e
    move-result-object v0

    #@2f
    .line 1039
    .local v0, pcscf_address:[Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v3, "onGetPcscfAddressCompleted, pcscf_address.length : "

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    array-length v3, v0

    #@3b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v2

    #@43
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@46
    .line 1041
    array-length v2, v0

    #@47
    if-lez v2, :cond_53

    #@49
    aget-object v2, v0, v5

    #@4b
    const-string v3, ","

    #@4d
    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@50
    move-result-object v2

    #@51
    iput-object v2, p0, Lcom/android/internal/telephony/DataConnection;->pcscfAddr_ipv4:[Ljava/lang/String;

    #@53
    .line 1042
    :cond_53
    array-length v2, v0

    #@54
    if-le v2, v4, :cond_60

    #@56
    aget-object v2, v0, v4

    #@58
    const-string v3, ","

    #@5a
    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@5d
    move-result-object v2

    #@5e
    iput-object v2, p0, Lcom/android/internal/telephony/DataConnection;->pcscfAddr_ipv6:[Ljava/lang/String;

    #@60
    .line 1044
    :cond_60
    array-length v2, v0

    #@61
    if-lez v2, :cond_7b

    #@63
    new-instance v2, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string v3, "onGetPcscfAddressCompleted, IPv4 PCSCF Addresses : "

    #@6a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v2

    #@6e
    aget-object v3, v0, v5

    #@70
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v2

    #@74
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v2

    #@78
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@7b
    .line 1045
    :cond_7b
    array-length v2, v0

    #@7c
    if-le v2, v4, :cond_b

    #@7e
    new-instance v2, Ljava/lang/StringBuilder;

    #@80
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@83
    const-string v3, "onGetPcscfAddressCompleted, IPv6 PCSCF Addresses : "

    #@85
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v2

    #@89
    aget-object v3, v0, v4

    #@8b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v2

    #@8f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@92
    move-result-object v2

    #@93
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@96
    goto/16 :goto_b
.end method

.method private onSetupConnectionCompleted(Landroid/os/AsyncResult;)Lcom/android/internal/telephony/DataCallState$SetupResult;
    .registers 20
    .parameter "ar"

    #@0
    .prologue
    .line 736
    move-object/from16 v0, p1

    #@2
    iget-object v10, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@4
    check-cast v10, Lcom/android/internal/telephony/DataCallState;

    #@6
    .line 737
    .local v10, response:Lcom/android/internal/telephony/DataCallState;
    move-object/from16 v0, p1

    #@8
    iget-object v1, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@a
    check-cast v1, Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@c
    .line 740
    .local v1, cp:Lcom/android/internal/telephony/DataConnection$ConnectionParams;
    move-object/from16 v0, p1

    #@e
    iget-object v14, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@10
    if-eqz v14, :cond_cd

    #@12
    .line 742
    new-instance v14, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v15, "onSetupConnectionCompleted failed, ar.exception="

    #@19
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v14

    #@1d
    move-object/from16 v0, p1

    #@1f
    iget-object v15, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@21
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v14

    #@25
    const-string v15, " response="

    #@27
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v14

    #@2b
    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v14

    #@2f
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v14

    #@33
    move-object/from16 v0, p0

    #@35
    invoke-virtual {v0, v14}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@38
    .line 746
    move-object/from16 v0, p1

    #@3a
    iget-object v14, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@3c
    instance-of v14, v14, Lcom/android/internal/telephony/CommandException;

    #@3e
    if-eqz v14, :cond_a8

    #@40
    move-object/from16 v0, p1

    #@42
    iget-object v14, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@44
    check-cast v14, Lcom/android/internal/telephony/CommandException;

    #@46
    check-cast v14, Lcom/android/internal/telephony/CommandException;

    #@48
    invoke-virtual {v14}, Lcom/android/internal/telephony/CommandException;->getCommandError()Lcom/android/internal/telephony/CommandException$Error;

    #@4b
    move-result-object v14

    #@4c
    sget-object v15, Lcom/android/internal/telephony/CommandException$Error;->RADIO_NOT_AVAILABLE:Lcom/android/internal/telephony/CommandException$Error;

    #@4e
    if-ne v14, v15, :cond_a8

    #@50
    .line 749
    sget-object v11, Lcom/android/internal/telephony/DataCallState$SetupResult;->ERR_BadCommand:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@52
    .line 750
    .local v11, result:Lcom/android/internal/telephony/DataCallState$SetupResult;
    sget-object v14, Lcom/android/internal/telephony/DataConnection$FailCause;->RADIO_NOT_AVAILABLE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@54
    iput-object v14, v11, Lcom/android/internal/telephony/DataCallState$SetupResult;->mFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@56
    .line 859
    :goto_56
    move-object/from16 v0, p0

    #@58
    iget-object v14, v0, Lcom/android/internal/telephony/DataConnection;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@5a
    iget-boolean v14, v14, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_enable:Z

    #@5c
    if-eqz v14, :cond_a7

    #@5e
    .line 862
    new-instance v13, Landroid/content/Intent;

    #@60
    const-string v14, "cpa_onSetupConnectionCompleted"

    #@62
    invoke-direct {v13, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@65
    .line 863
    .local v13, sintent:Landroid/content/Intent;
    iget-object v14, v11, Lcom/android/internal/telephony/DataCallState$SetupResult;->mFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@67
    sget-object v15, Lcom/android/internal/telephony/DataConnection$FailCause;->NONE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@69
    if-eq v14, v15, :cond_3bb

    #@6b
    .line 867
    iget-object v14, v11, Lcom/android/internal/telephony/DataCallState$SetupResult;->mFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@6d
    sget-object v15, Lcom/android/internal/telephony/DataConnection$FailCause;->RADIO_NOT_AVAILABLE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@6f
    if-ne v14, v15, :cond_3a6

    #@71
    .line 868
    const/4 v12, -0x2

    #@72
    .line 876
    .local v12, sfail:I
    :goto_72
    const-string v14, "result"

    #@74
    const/4 v15, 0x0

    #@75
    invoke-virtual {v13, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@78
    .line 877
    const-string v14, "mFailCause"

    #@7a
    invoke-virtual {v13, v14, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@7d
    .line 879
    const-string v14, "*** onSetupConnectionCompleted() send cpa_onSetupConnectionCompleted intent"

    #@7f
    move-object/from16 v0, p0

    #@81
    invoke-virtual {v0, v14}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@84
    .line 880
    move-object/from16 v0, p0

    #@86
    iget-object v14, v0, Lcom/android/internal/telephony/DataConnection;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@88
    const/4 v15, 0x0

    #@89
    iput-boolean v15, v14, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_enable:Z

    #@8b
    .line 881
    const-string v14, "ril.btdun.dns1"

    #@8d
    const/4 v15, 0x0

    #@8e
    invoke-static {v14, v15}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@91
    .line 882
    const-string v14, "ril.btdun.dns2"

    #@93
    const/4 v15, 0x0

    #@94
    invoke-static {v14, v15}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@97
    .line 908
    .end local v12           #sfail:I
    :goto_97
    move-object/from16 v0, p0

    #@99
    iget-object v14, v0, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@9b
    iget-object v14, v14, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@9d
    invoke-virtual {v14, v13}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@a0
    .line 910
    move-object/from16 v0, p0

    #@a2
    iget-object v14, v0, Lcom/android/internal/telephony/DataConnection;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@a4
    const/4 v15, 0x0

    #@a5
    iput-boolean v15, v14, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_send_result:Z

    #@a7
    .line 913
    .end local v13           #sintent:Landroid/content/Intent;
    :cond_a7
    return-object v11

    #@a8
    .line 753
    .end local v11           #result:Lcom/android/internal/telephony/DataCallState$SetupResult;
    :cond_a8
    if-nez v10, :cond_b8

    #@aa
    .line 754
    const-string v14, "[LGE_DATA] onSetupConnectionCompleted response null ERR_UnacceptableParameter"

    #@ac
    move-object/from16 v0, p0

    #@ae
    invoke-virtual {v0, v14}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@b1
    .line 756
    sget-object v11, Lcom/android/internal/telephony/DataCallState$SetupResult;->ERR_UnacceptableParameter:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@b3
    .line 757
    .restart local v11       #result:Lcom/android/internal/telephony/DataCallState$SetupResult;
    sget-object v14, Lcom/android/internal/telephony/DataConnection$FailCause;->ERROR_UNSPECIFIED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@b5
    iput-object v14, v11, Lcom/android/internal/telephony/DataCallState$SetupResult;->mFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@b7
    goto :goto_56

    #@b8
    .line 758
    .end local v11           #result:Lcom/android/internal/telephony/DataCallState$SetupResult;
    :cond_b8
    if-eqz v10, :cond_bf

    #@ba
    iget v14, v10, Lcom/android/internal/telephony/DataCallState;->version:I

    #@bc
    const/4 v15, 0x4

    #@bd
    if-ge v14, v15, :cond_c2

    #@bf
    .line 759
    :cond_bf
    sget-object v11, Lcom/android/internal/telephony/DataCallState$SetupResult;->ERR_GetLastErrorFromRil:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@c1
    .restart local v11       #result:Lcom/android/internal/telephony/DataCallState$SetupResult;
    goto :goto_56

    #@c2
    .line 762
    .end local v11           #result:Lcom/android/internal/telephony/DataCallState$SetupResult;
    :cond_c2
    sget-object v11, Lcom/android/internal/telephony/DataCallState$SetupResult;->ERR_RilError:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@c4
    .line 763
    .restart local v11       #result:Lcom/android/internal/telephony/DataCallState$SetupResult;
    iget v14, v10, Lcom/android/internal/telephony/DataCallState;->status:I

    #@c6
    invoke-static {v14}, Lcom/android/internal/telephony/DataConnection$FailCause;->fromInt(I)Lcom/android/internal/telephony/DataConnection$FailCause;

    #@c9
    move-result-object v14

    #@ca
    iput-object v14, v11, Lcom/android/internal/telephony/DataCallState$SetupResult;->mFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@cc
    goto :goto_56

    #@cd
    .line 765
    .end local v11           #result:Lcom/android/internal/telephony/DataCallState$SetupResult;
    :cond_cd
    iget v14, v1, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->tag:I

    #@cf
    move-object/from16 v0, p0

    #@d1
    iget v15, v0, Lcom/android/internal/telephony/DataConnection;->mTag:I

    #@d3
    if-eq v14, v15, :cond_101

    #@d5
    .line 767
    new-instance v14, Ljava/lang/StringBuilder;

    #@d7
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@da
    const-string v15, "BUG: onSetupConnectionCompleted is stale cp.tag="

    #@dc
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@df
    move-result-object v14

    #@e0
    iget v15, v1, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->tag:I

    #@e2
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v14

    #@e6
    const-string v15, ", mtag="

    #@e8
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v14

    #@ec
    move-object/from16 v0, p0

    #@ee
    iget v15, v0, Lcom/android/internal/telephony/DataConnection;->mTag:I

    #@f0
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v14

    #@f4
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f7
    move-result-object v14

    #@f8
    move-object/from16 v0, p0

    #@fa
    invoke-virtual {v0, v14}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@fd
    .line 769
    sget-object v11, Lcom/android/internal/telephony/DataCallState$SetupResult;->ERR_Stale:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@ff
    .restart local v11       #result:Lcom/android/internal/telephony/DataCallState$SetupResult;
    goto/16 :goto_56

    #@101
    .line 770
    .end local v11           #result:Lcom/android/internal/telephony/DataCallState$SetupResult;
    :cond_101
    iget v14, v10, Lcom/android/internal/telephony/DataCallState;->status:I

    #@103
    if-eqz v14, :cond_111

    #@105
    .line 771
    sget-object v11, Lcom/android/internal/telephony/DataCallState$SetupResult;->ERR_RilError:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@107
    .line 772
    .restart local v11       #result:Lcom/android/internal/telephony/DataCallState$SetupResult;
    iget v14, v10, Lcom/android/internal/telephony/DataCallState;->status:I

    #@109
    invoke-static {v14}, Lcom/android/internal/telephony/DataConnection$FailCause;->fromInt(I)Lcom/android/internal/telephony/DataConnection$FailCause;

    #@10c
    move-result-object v14

    #@10d
    iput-object v14, v11, Lcom/android/internal/telephony/DataCallState$SetupResult;->mFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@10f
    goto/16 :goto_56

    #@111
    .line 778
    .end local v11           #result:Lcom/android/internal/telephony/DataCallState$SetupResult;
    :cond_111
    iget-object v14, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@113
    if-eqz v14, :cond_377

    #@115
    iget-object v14, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@117
    array-length v14, v14

    #@118
    if-eqz v14, :cond_377

    #@11a
    .line 779
    new-instance v14, Ljava/lang/StringBuilder;

    #@11c
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@11f
    const-string v15, "[IMS_AFW] response.addresses.length :"

    #@121
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@124
    move-result-object v14

    #@125
    iget-object v15, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@127
    array-length v15, v15

    #@128
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12b
    move-result-object v14

    #@12c
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12f
    move-result-object v14

    #@130
    move-object/from16 v0, p0

    #@132
    invoke-virtual {v0, v14}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@135
    .line 781
    move-object/from16 v0, p0

    #@137
    iget-object v14, v0, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@139
    iget-object v14, v14, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@13b
    invoke-interface {v14}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@13e
    move-result-object v14

    #@13f
    iget-boolean v14, v14, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_IPV46_PCSCF_INTERACE_ON_RIL_AFW:Z

    #@141
    if-eqz v14, :cond_316

    #@143
    .line 783
    const/4 v9, 0x0

    #@144
    .local v9, num:I
    :goto_144
    iget-object v14, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@146
    array-length v14, v14

    #@147
    if-ge v9, v14, :cond_261

    #@149
    .line 784
    new-instance v14, Ljava/lang/StringBuilder;

    #@14b
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@14e
    const-string v15, "[IMS_AFW] response.addresses include pcscfAddr ["

    #@150
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@153
    move-result-object v14

    #@154
    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@157
    move-result-object v14

    #@158
    const-string v15, "] :"

    #@15a
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15d
    move-result-object v14

    #@15e
    iget-object v15, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@160
    aget-object v15, v15, v9

    #@162
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@165
    move-result-object v14

    #@166
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@169
    move-result-object v14

    #@16a
    move-object/from16 v0, p0

    #@16c
    invoke-virtual {v0, v14}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@16f
    .line 785
    new-instance v14, Ljava/lang/StringBuilder;

    #@171
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@174
    const-string v15, "[IMS_AFW] response.addresses["

    #@176
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@179
    move-result-object v14

    #@17a
    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17d
    move-result-object v14

    #@17e
    const-string v15, "].split().length  :"

    #@180
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@183
    move-result-object v14

    #@184
    iget-object v15, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@186
    aget-object v15, v15, v9

    #@188
    const-string v16, ";"

    #@18a
    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@18d
    move-result-object v15

    #@18e
    array-length v15, v15

    #@18f
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@192
    move-result-object v14

    #@193
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@196
    move-result-object v14

    #@197
    move-object/from16 v0, p0

    #@199
    invoke-virtual {v0, v14}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@19c
    .line 787
    iget-object v14, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@19e
    aget-object v14, v14, v9

    #@1a0
    const-string v15, ";"

    #@1a2
    invoke-virtual {v14, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@1a5
    move-result-object v14

    #@1a6
    array-length v14, v14

    #@1a7
    const/4 v15, 0x1

    #@1a8
    if-le v14, v15, :cond_1f9

    #@1aa
    .line 788
    iget-object v14, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@1ac
    aget-object v14, v14, v9

    #@1ae
    const-string v15, ";"

    #@1b0
    invoke-virtual {v14, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@1b3
    move-result-object v14

    #@1b4
    const/4 v15, 0x1

    #@1b5
    aget-object v14, v14, v15

    #@1b7
    const-string v15, ","

    #@1b9
    invoke-virtual {v14, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@1bc
    move-result-object v14

    #@1bd
    move-object/from16 v0, p0

    #@1bf
    iput-object v14, v0, Lcom/android/internal/telephony/DataConnection;->pcscfAddr_ipv4:[Ljava/lang/String;

    #@1c1
    .line 789
    const/4 v6, 0x0

    #@1c2
    .local v6, i:I
    :goto_1c2
    move-object/from16 v0, p0

    #@1c4
    iget-object v14, v0, Lcom/android/internal/telephony/DataConnection;->pcscfAddr_ipv4:[Ljava/lang/String;

    #@1c6
    array-length v14, v14

    #@1c7
    if-ge v6, v14, :cond_1f9

    #@1c9
    .line 790
    new-instance v14, Ljava/lang/StringBuilder;

    #@1cb
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@1ce
    const-string v15, "[IMS_AFW] pcscfAddr_ipv4 ["

    #@1d0
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d3
    move-result-object v14

    #@1d4
    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d7
    move-result-object v14

    #@1d8
    const-string v15, "] :"

    #@1da
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1dd
    move-result-object v14

    #@1de
    move-object/from16 v0, p0

    #@1e0
    iget-object v15, v0, Lcom/android/internal/telephony/DataConnection;->pcscfAddr_ipv4:[Ljava/lang/String;

    #@1e2
    aget-object v15, v15, v6

    #@1e4
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e7
    move-result-object v14

    #@1e8
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1eb
    move-result-object v14

    #@1ec
    move-object/from16 v0, p0

    #@1ee
    invoke-virtual {v0, v14}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@1f1
    .line 791
    move-object/from16 v0, p0

    #@1f3
    iget-object v14, v0, Lcom/android/internal/telephony/DataConnection;->pcscfAddr_ipv4:[Ljava/lang/String;

    #@1f5
    aget-object v14, v14, v6

    #@1f7
    if-nez v14, :cond_25a

    #@1f9
    .line 795
    .end local v6           #i:I
    :cond_1f9
    iget-object v14, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@1fb
    aget-object v14, v14, v9

    #@1fd
    const-string v15, ";"

    #@1ff
    invoke-virtual {v14, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@202
    move-result-object v14

    #@203
    array-length v14, v14

    #@204
    const/4 v15, 0x2

    #@205
    if-le v14, v15, :cond_256

    #@207
    .line 796
    iget-object v14, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@209
    aget-object v14, v14, v9

    #@20b
    const-string v15, ";"

    #@20d
    invoke-virtual {v14, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@210
    move-result-object v14

    #@211
    const/4 v15, 0x2

    #@212
    aget-object v14, v14, v15

    #@214
    const-string v15, ","

    #@216
    invoke-virtual {v14, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@219
    move-result-object v14

    #@21a
    move-object/from16 v0, p0

    #@21c
    iput-object v14, v0, Lcom/android/internal/telephony/DataConnection;->pcscfAddr_ipv6:[Ljava/lang/String;

    #@21e
    .line 797
    const/4 v6, 0x0

    #@21f
    .restart local v6       #i:I
    :goto_21f
    move-object/from16 v0, p0

    #@221
    iget-object v14, v0, Lcom/android/internal/telephony/DataConnection;->pcscfAddr_ipv6:[Ljava/lang/String;

    #@223
    array-length v14, v14

    #@224
    if-ge v6, v14, :cond_256

    #@226
    .line 798
    new-instance v14, Ljava/lang/StringBuilder;

    #@228
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@22b
    const-string v15, "[IMS_AFW] pcscfAddr_ipv6 ["

    #@22d
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@230
    move-result-object v14

    #@231
    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@234
    move-result-object v14

    #@235
    const-string v15, "] :"

    #@237
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23a
    move-result-object v14

    #@23b
    move-object/from16 v0, p0

    #@23d
    iget-object v15, v0, Lcom/android/internal/telephony/DataConnection;->pcscfAddr_ipv6:[Ljava/lang/String;

    #@23f
    aget-object v15, v15, v6

    #@241
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@244
    move-result-object v14

    #@245
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@248
    move-result-object v14

    #@249
    move-object/from16 v0, p0

    #@24b
    invoke-virtual {v0, v14}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@24e
    .line 799
    move-object/from16 v0, p0

    #@250
    iget-object v14, v0, Lcom/android/internal/telephony/DataConnection;->pcscfAddr_ipv6:[Ljava/lang/String;

    #@252
    aget-object v14, v14, v6

    #@254
    if-nez v14, :cond_25e

    #@256
    .line 783
    .end local v6           #i:I
    :cond_256
    add-int/lit8 v9, v9, 0x1

    #@258
    goto/16 :goto_144

    #@25a
    .line 789
    .restart local v6       #i:I
    :cond_25a
    add-int/lit8 v6, v6, 0x1

    #@25c
    goto/16 :goto_1c2

    #@25e
    .line 797
    :cond_25e
    add-int/lit8 v6, v6, 0x1

    #@260
    goto :goto_21f

    #@261
    .line 805
    .end local v6           #i:I
    :cond_261
    const/4 v6, 0x0

    #@262
    .restart local v6       #i:I
    :goto_262
    iget-object v14, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@264
    array-length v14, v14

    #@265
    if-ge v6, v14, :cond_37e

    #@267
    .line 807
    iget-object v14, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@269
    aget-object v14, v14, v6

    #@26b
    if-eqz v14, :cond_286

    #@26d
    .line 808
    iget-object v14, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@26f
    aget-object v14, v14, v6

    #@271
    const-string v15, ";"

    #@273
    invoke-virtual {v14, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@276
    move-result-object v14

    #@277
    array-length v14, v14

    #@278
    if-nez v14, :cond_289

    #@27a
    .line 809
    const-string v14, "[IMS_AFW] Addresses is invalid. initialization Address."

    #@27c
    move-object/from16 v0, p0

    #@27e
    invoke-virtual {v0, v14}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@281
    .line 810
    const/4 v14, 0x0

    #@282
    new-array v14, v14, [Ljava/lang/String;

    #@284
    iput-object v14, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@286
    .line 805
    :cond_286
    :goto_286
    add-int/lit8 v6, v6, 0x1

    #@288
    goto :goto_262

    #@289
    .line 813
    :cond_289
    iget-object v14, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@28b
    iget-object v15, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@28d
    aget-object v15, v15, v6

    #@28f
    const-string v16, ";"

    #@291
    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@294
    move-result-object v15

    #@295
    const/16 v16, 0x0

    #@297
    aget-object v15, v15, v16

    #@299
    aput-object v15, v14, v6

    #@29b
    .line 814
    new-instance v14, Ljava/lang/StringBuilder;

    #@29d
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@2a0
    const-string v15, "[IMS_AFW] response.addresses["

    #@2a2
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a5
    move-result-object v14

    #@2a6
    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a9
    move-result-object v14

    #@2aa
    const-string v15, "]: "

    #@2ac
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2af
    move-result-object v14

    #@2b0
    iget-object v15, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@2b2
    aget-object v15, v15, v6

    #@2b4
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b7
    move-result-object v14

    #@2b8
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2bb
    move-result-object v14

    #@2bc
    move-object/from16 v0, p0

    #@2be
    invoke-virtual {v0, v14}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@2c1
    .line 816
    const-string v14, "ro.afwdata.LGfeatureset"

    #@2c3
    const-string v15, "none"

    #@2c5
    invoke-static {v14, v15}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2c8
    move-result-object v5

    #@2c9
    .line 817
    .local v5, featureset:Ljava/lang/String;
    const-string v14, "LGTBASE"

    #@2cb
    invoke-static {v5, v14}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@2ce
    move-result v14

    #@2cf
    if-eqz v14, :cond_286

    #@2d1
    .line 818
    iget-object v14, v1, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->apn:Lcom/android/internal/telephony/DataProfile;

    #@2d3
    const-string v15, "ims"

    #@2d5
    invoke-virtual {v14, v15}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@2d8
    move-result v14

    #@2d9
    if-eqz v14, :cond_286

    #@2db
    iget-object v14, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@2dd
    aget-object v14, v14, v6

    #@2df
    const-string v15, ":"

    #@2e1
    invoke-virtual {v14, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@2e4
    move-result v14

    #@2e5
    if-eqz v14, :cond_286

    #@2e7
    .line 819
    iget-object v14, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@2e9
    iget-object v15, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@2eb
    aget-object v15, v15, v6

    #@2ed
    const-string v16, "/"

    #@2ef
    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@2f2
    move-result-object v15

    #@2f3
    const/16 v16, 0x0

    #@2f5
    aget-object v15, v15, v16

    #@2f7
    aput-object v15, v14, v6

    #@2f9
    .line 820
    const-string v14, "ims.ipv6.address"

    #@2fb
    iget-object v15, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@2fd
    aget-object v15, v15, v6

    #@2ff
    invoke-static {v14, v15}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@302
    .line 821
    move-object/from16 v0, p0

    #@304
    iget-object v14, v0, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@306
    const v15, 0xb0010

    #@309
    iget-object v0, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@30b
    move-object/from16 v16, v0

    #@30d
    aget-object v16, v16, v6

    #@30f
    const/16 v17, 0x0

    #@311
    invoke-virtual/range {v14 .. v17}, Lcom/android/internal/telephony/PhoneBase;->setModemStringItem(ILjava/lang/String;Landroid/os/Message;)V

    #@314
    goto/16 :goto_286

    #@316
    .line 831
    .end local v5           #featureset:Ljava/lang/String;
    .end local v6           #i:I
    .end local v9           #num:I
    :cond_316
    const/4 v6, 0x0

    #@317
    .restart local v6       #i:I
    :goto_317
    iget-object v14, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@319
    array-length v14, v14

    #@31a
    if-ge v6, v14, :cond_37e

    #@31c
    .line 833
    iget-object v14, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@31e
    aget-object v14, v14, v6

    #@320
    if-eqz v14, :cond_33b

    #@322
    .line 834
    iget-object v14, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@324
    aget-object v14, v14, v6

    #@326
    const-string v15, ";"

    #@328
    invoke-virtual {v14, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@32b
    move-result-object v14

    #@32c
    array-length v14, v14

    #@32d
    if-nez v14, :cond_33e

    #@32f
    .line 835
    const-string v14, "[IMS_AFW] Addresses is invalid. initialization Address."

    #@331
    move-object/from16 v0, p0

    #@333
    invoke-virtual {v0, v14}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@336
    .line 836
    const/4 v14, 0x0

    #@337
    new-array v14, v14, [Ljava/lang/String;

    #@339
    iput-object v14, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@33b
    .line 831
    :cond_33b
    :goto_33b
    add-int/lit8 v6, v6, 0x1

    #@33d
    goto :goto_317

    #@33e
    .line 839
    :cond_33e
    iget-object v14, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@340
    iget-object v15, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@342
    aget-object v15, v15, v6

    #@344
    const-string v16, ";"

    #@346
    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@349
    move-result-object v15

    #@34a
    const/16 v16, 0x0

    #@34c
    aget-object v15, v15, v16

    #@34e
    aput-object v15, v14, v6

    #@350
    .line 840
    new-instance v14, Ljava/lang/StringBuilder;

    #@352
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@355
    const-string v15, "[IMS_AFW] response.addresses["

    #@357
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35a
    move-result-object v14

    #@35b
    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35e
    move-result-object v14

    #@35f
    const-string v15, "]: "

    #@361
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@364
    move-result-object v14

    #@365
    iget-object v15, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@367
    aget-object v15, v15, v6

    #@369
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36c
    move-result-object v14

    #@36d
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@370
    move-result-object v14

    #@371
    move-object/from16 v0, p0

    #@373
    invoke-virtual {v0, v14}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@376
    goto :goto_33b

    #@377
    .line 847
    .end local v6           #i:I
    :cond_377
    const-string v14, "[IMS_AFW] Setup data call response.addresses is null"

    #@379
    move-object/from16 v0, p0

    #@37b
    invoke-virtual {v0, v14}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@37e
    .line 851
    :cond_37e
    new-instance v14, Ljava/lang/StringBuilder;

    #@380
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@383
    const-string v15, "onSetupConnectionCompleted received DataCallState: "

    #@385
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@388
    move-result-object v14

    #@389
    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@38c
    move-result-object v14

    #@38d
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@390
    move-result-object v14

    #@391
    move-object/from16 v0, p0

    #@393
    invoke-virtual {v0, v14}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@396
    .line 852
    iget v14, v10, Lcom/android/internal/telephony/DataCallState;->cid:I

    #@398
    move-object/from16 v0, p0

    #@39a
    iput v14, v0, Lcom/android/internal/telephony/DataConnection;->cid:I

    #@39c
    .line 853
    move-object/from16 v0, p0

    #@39e
    invoke-direct {v0, v10}, Lcom/android/internal/telephony/DataConnection;->updateLinkProperty(Lcom/android/internal/telephony/DataCallState;)Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;

    #@3a1
    move-result-object v14

    #@3a2
    iget-object v11, v14, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;->setupResult:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@3a4
    .restart local v11       #result:Lcom/android/internal/telephony/DataCallState$SetupResult;
    goto/16 :goto_56

    #@3a6
    .line 869
    .restart local v13       #sintent:Landroid/content/Intent;
    :cond_3a6
    iget-object v14, v11, Lcom/android/internal/telephony/DataCallState$SetupResult;->mFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@3a8
    sget-object v15, Lcom/android/internal/telephony/DataConnection$FailCause;->UNACCEPTABLE_NETWORK_PARAMETER:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@3aa
    if-ne v14, v15, :cond_3af

    #@3ac
    .line 870
    const/4 v12, -0x1

    #@3ad
    .restart local v12       #sfail:I
    goto/16 :goto_72

    #@3af
    .line 871
    .end local v12           #sfail:I
    :cond_3af
    iget-object v14, v11, Lcom/android/internal/telephony/DataCallState$SetupResult;->mFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@3b1
    sget-object v15, Lcom/android/internal/telephony/DataConnection$FailCause;->USER_AUTHENTICATION:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@3b3
    if-ne v14, v15, :cond_3b8

    #@3b5
    .line 872
    const/4 v12, -0x3

    #@3b6
    .restart local v12       #sfail:I
    goto/16 :goto_72

    #@3b8
    .line 874
    .end local v12           #sfail:I
    :cond_3b8
    const/4 v12, -0x4

    #@3b9
    .restart local v12       #sfail:I
    goto/16 :goto_72

    #@3bb
    .line 886
    .end local v12           #sfail:I
    :cond_3bb
    const/4 v14, 0x0

    #@3bc
    new-array v2, v14, [Ljava/lang/String;

    #@3be
    .line 887
    .local v2, dns:[Ljava/lang/String;
    const-string v14, "result"

    #@3c0
    const/4 v15, 0x1

    #@3c1
    invoke-virtual {v13, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@3c4
    .line 888
    const-string v15, "addresses"

    #@3c6
    if-eqz v10, :cond_3f8

    #@3c8
    iget-object v14, v10, Lcom/android/internal/telephony/DataCallState;->addresses:[Ljava/lang/String;

    #@3ca
    :goto_3ca
    invoke-virtual {v13, v15, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    #@3cd
    .line 890
    const-string v14, "ril.btdun.dns1"

    #@3cf
    const/4 v15, 0x0

    #@3d0
    invoke-static {v14, v15}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3d3
    move-result-object v3

    #@3d4
    .line 891
    .local v3, dns1:Ljava/lang/String;
    const-string v14, "ril.btdun.dns2"

    #@3d6
    const/4 v15, 0x0

    #@3d7
    invoke-static {v14, v15}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3da
    move-result-object v4

    #@3db
    .line 894
    .local v4, dns2:Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3de
    move-result v14

    #@3df
    if-nez v14, :cond_402

    #@3e1
    .line 895
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3e4
    move-result v14

    #@3e5
    if-nez v14, :cond_3fa

    #@3e7
    .line 896
    const/4 v14, 0x2

    #@3e8
    new-array v7, v14, [Ljava/lang/String;

    #@3ea
    const/4 v14, 0x0

    #@3eb
    aput-object v3, v7, v14

    #@3ed
    const/4 v14, 0x1

    #@3ee
    aput-object v4, v7, v14

    #@3f0
    .line 897
    .local v7, mDns:[Ljava/lang/String;
    move-object v2, v7

    #@3f1
    .line 902
    .end local v7           #mDns:[Ljava/lang/String;
    :goto_3f1
    const-string v14, "dnses"

    #@3f3
    invoke-virtual {v13, v14, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    #@3f6
    goto/16 :goto_97

    #@3f8
    .line 888
    .end local v3           #dns1:Ljava/lang/String;
    .end local v4           #dns2:Ljava/lang/String;
    :cond_3f8
    const/4 v14, 0x0

    #@3f9
    goto :goto_3ca

    #@3fa
    .line 899
    .restart local v3       #dns1:Ljava/lang/String;
    .restart local v4       #dns2:Ljava/lang/String;
    :cond_3fa
    const/4 v14, 0x1

    #@3fb
    new-array v8, v14, [Ljava/lang/String;

    #@3fd
    const/4 v14, 0x0

    #@3fe
    aput-object v3, v8, v14

    #@400
    .line 900
    .local v8, mDns1:[Ljava/lang/String;
    move-object v2, v8

    #@401
    goto :goto_3f1

    #@402
    .line 905
    .end local v8           #mDns1:[Ljava/lang/String;
    :cond_402
    const-string v15, "dnses"

    #@404
    if-eqz v10, :cond_40d

    #@406
    iget-object v14, v10, Lcom/android/internal/telephony/DataCallState;->dnses:[Ljava/lang/String;

    #@408
    :goto_408
    invoke-virtual {v13, v15, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    #@40b
    goto/16 :goto_97

    #@40d
    :cond_40d
    const/4 v14, 0x0

    #@40e
    goto :goto_408
.end method

.method private restartRild()V
    .registers 4

    #@0
    .prologue
    .line 981
    const/4 v0, 0x0

    #@1
    .line 986
    .local v0, retry_count:I
    const-string v1, "ctl.stop"

    #@3
    const-string v2, "ril-daemon"

    #@5
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 988
    :goto_8
    const-string v1, "stopped"

    #@a
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/DataConnection;->isRildState(Ljava/lang/String;)Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_18

    #@10
    .line 990
    const-wide/16 v1, 0x1f4

    #@12
    :try_start_12
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_15} :catch_16

    #@15
    goto :goto_8

    #@16
    .line 992
    :catch_16
    move-exception v1

    #@17
    goto :goto_8

    #@18
    .line 996
    :cond_18
    const-string v1, "ctl.start"

    #@1a
    const-string v2, "ril-daemon"

    #@1c
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@1f
    .line 998
    :cond_1f
    :goto_1f
    const-string v1, "running"

    #@21
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/DataConnection;->isRildState(Ljava/lang/String;)Z

    #@24
    move-result v1

    #@25
    if-nez v1, :cond_3d

    #@27
    .line 1000
    const-wide/16 v1, 0x1f4

    #@29
    :try_start_29
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2c
    .catch Ljava/lang/Exception; {:try_start_29 .. :try_end_2c} :catch_3b

    #@2c
    .line 1004
    :goto_2c
    add-int/lit8 v0, v0, 0x1

    #@2e
    .line 1005
    const/16 v1, 0xa

    #@30
    if-le v0, v1, :cond_1f

    #@32
    .line 1007
    const-string v1, "ctl.start"

    #@34
    const-string v2, "ril-daemon"

    #@36
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@39
    .line 1008
    const/4 v0, 0x0

    #@3a
    goto :goto_1f

    #@3b
    .line 1002
    :catch_3b
    move-exception v1

    #@3c
    goto :goto_2c

    #@3d
    .line 1013
    :cond_3d
    return-void
.end method

.method private setLinkProperties(Lcom/android/internal/telephony/DataCallState;Landroid/net/LinkProperties;)Lcom/android/internal/telephony/DataCallState$SetupResult;
    .registers 9
    .parameter "response"
    .parameter "lp"

    #@0
    .prologue
    .line 928
    const/4 v1, 0x0

    #@1
    .line 929
    .local v1, okToUseSystemPropertyDns:Z
    new-instance v3, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v4, "net."

    #@8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v3

    #@c
    iget-object v4, p1, Lcom/android/internal/telephony/DataCallState;->ifname:Ljava/lang/String;

    #@e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v3

    #@12
    const-string v4, "."

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    .line 930
    .local v2, propertyPrefix:Ljava/lang/String;
    const/4 v3, 0x2

    #@1d
    new-array v0, v3, [Ljava/lang/String;

    #@1f
    .line 931
    .local v0, dnsServers:[Ljava/lang/String;
    const/4 v3, 0x0

    #@20
    new-instance v4, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    const-string v5, "dns1"

    #@2b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v4

    #@33
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@36
    move-result-object v4

    #@37
    aput-object v4, v0, v3

    #@39
    .line 932
    const/4 v3, 0x1

    #@3a
    new-instance v4, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v4

    #@43
    const-string v5, "dns2"

    #@45
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v4

    #@49
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v4

    #@4d
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@50
    move-result-object v4

    #@51
    aput-object v4, v0, v3

    #@53
    .line 933
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnection;->isDnsOk([Ljava/lang/String;)Z

    #@56
    move-result v1

    #@57
    .line 936
    invoke-virtual {p1, p2, v1}, Lcom/android/internal/telephony/DataCallState;->setLinkProperties(Landroid/net/LinkProperties;Z)Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@5a
    move-result-object v3

    #@5b
    return-object v3
.end method

.method private shutDown()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 448
    const-string v0, "shutDown"

    #@3
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@6
    .line 450
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mAc:Lcom/android/internal/util/AsyncChannel;

    #@8
    if-eqz v0, :cond_11

    #@a
    .line 451
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mAc:Lcom/android/internal/util/AsyncChannel;

    #@c
    invoke-virtual {v0}, Lcom/android/internal/util/AsyncChannel;->disconnected()V

    #@f
    .line 452
    iput-object v1, p0, Lcom/android/internal/telephony/DataConnection;->mAc:Lcom/android/internal/util/AsyncChannel;

    #@11
    .line 454
    :cond_11
    iput-object v1, p0, Lcom/android/internal/telephony/DataConnection;->mApnList:Ljava/util/List;

    #@13
    .line 455
    iput-object v1, p0, Lcom/android/internal/telephony/DataConnection;->mReconnectIntent:Landroid/app/PendingIntent;

    #@15
    .line 456
    iput-object v1, p0, Lcom/android/internal/telephony/DataConnection;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@17
    .line 457
    iput-object v1, p0, Lcom/android/internal/telephony/DataConnection;->mApn:Lcom/android/internal/telephony/DataProfile;

    #@19
    .line 458
    iput-object v1, p0, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@1b
    .line 459
    iput-object v1, p0, Lcom/android/internal/telephony/DataConnection;->mLinkProperties:Landroid/net/LinkProperties;

    #@1d
    .line 460
    iput-object v1, p0, Lcom/android/internal/telephony/DataConnection;->mCapabilities:Landroid/net/LinkCapabilities;

    #@1f
    .line 461
    iput-object v1, p0, Lcom/android/internal/telephony/DataConnection;->lastFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@21
    .line 462
    iput-object v1, p0, Lcom/android/internal/telephony/DataConnection;->userData:Ljava/lang/Object;

    #@23
    .line 463
    return-void
.end method

.method private tearDownData(Ljava/lang/Object;)V
    .registers 10
    .parameter "o"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const v6, 0x40003

    #@4
    .line 472
    const/4 v1, 0x0

    #@5
    .line 473
    .local v1, discReason:I
    if-eqz p1, :cond_1b

    #@7
    instance-of v4, p1, Lcom/android/internal/telephony/DataConnection$DisconnectParams;

    #@9
    if-eqz v4, :cond_1b

    #@b
    move-object v2, p1

    #@c
    .line 474
    check-cast v2, Lcom/android/internal/telephony/DataConnection$DisconnectParams;

    #@e
    .line 475
    .local v2, dp:Lcom/android/internal/telephony/DataConnection$DisconnectParams;
    iget-object v3, v2, Lcom/android/internal/telephony/DataConnection$DisconnectParams;->onCompletedMsg:Landroid/os/Message;

    #@10
    .line 476
    .local v3, m:Landroid/os/Message;
    iget-object v4, v2, Lcom/android/internal/telephony/DataConnection$DisconnectParams;->reason:Ljava/lang/String;

    #@12
    const-string v5, "radioTurnedOff"

    #@14
    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@17
    move-result v4

    #@18
    if-eqz v4, :cond_3c

    #@1a
    .line 477
    const/4 v1, 0x1

    #@1b
    .line 482
    .end local v2           #dp:Lcom/android/internal/telephony/DataConnection$DisconnectParams;
    .end local v3           #m:Landroid/os/Message;
    :cond_1b
    :goto_1b
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@1d
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1f
    invoke-interface {v4}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@22
    move-result-object v4

    #@23
    invoke-virtual {v4}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isOn()Z

    #@26
    move-result v4

    #@27
    if-eqz v4, :cond_48

    #@29
    .line 483
    const-string v4, "tearDownData radio is on, call deactivateDataCall"

    #@2b
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@2e
    .line 484
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@30
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@32
    iget v5, p0, Lcom/android/internal/telephony/DataConnection;->cid:I

    #@34
    invoke-virtual {p0, v6, p1}, Lcom/android/internal/telephony/DataConnection;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@37
    move-result-object v6

    #@38
    invoke-interface {v4, v5, v1, v6}, Lcom/android/internal/telephony/CommandsInterface;->deactivateDataCall(IILandroid/os/Message;)V

    #@3b
    .line 490
    :goto_3b
    return-void

    #@3c
    .line 478
    .restart local v2       #dp:Lcom/android/internal/telephony/DataConnection$DisconnectParams;
    .restart local v3       #m:Landroid/os/Message;
    :cond_3c
    iget-object v4, v2, Lcom/android/internal/telephony/DataConnection$DisconnectParams;->reason:Ljava/lang/String;

    #@3e
    const-string v5, "pdpReset"

    #@40
    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@43
    move-result v4

    #@44
    if-eqz v4, :cond_1b

    #@46
    .line 479
    const/4 v1, 0x2

    #@47
    goto :goto_1b

    #@48
    .line 486
    .end local v2           #dp:Lcom/android/internal/telephony/DataConnection$DisconnectParams;
    .end local v3           #m:Landroid/os/Message;
    :cond_48
    const-string v4, "tearDownData radio is off sendMessage EVENT_DEACTIVATE_DONE immediately"

    #@4a
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@4d
    .line 487
    new-instance v0, Landroid/os/AsyncResult;

    #@4f
    invoke-direct {v0, p1, v7, v7}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@52
    .line 488
    .local v0, ar:Landroid/os/AsyncResult;
    invoke-virtual {p0, v6, v0}, Lcom/android/internal/telephony/DataConnection;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@55
    move-result-object v4

    #@56
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/DataConnection;->sendMessage(Landroid/os/Message;)V

    #@59
    goto :goto_3b
.end method

.method private updateLinkProperty(Lcom/android/internal/telephony/DataCallState;)Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;
    .registers 5
    .parameter "newState"

    #@0
    .prologue
    .line 950
    new-instance v0, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;

    #@2
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnection;->mLinkProperties:Landroid/net/LinkProperties;

    #@4
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;-><init>(Landroid/net/LinkProperties;)V

    #@7
    .line 952
    .local v0, result:Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;
    if-nez p1, :cond_a

    #@9
    .line 972
    :goto_9
    return-object v0

    #@a
    .line 955
    :cond_a
    new-instance v1, Landroid/net/LinkProperties;

    #@c
    invoke-direct {v1}, Landroid/net/LinkProperties;-><init>()V

    #@f
    iput-object v1, v0, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;->newLp:Landroid/net/LinkProperties;

    #@11
    .line 958
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;->newLp:Landroid/net/LinkProperties;

    #@13
    invoke-direct {p0, p1, v1}, Lcom/android/internal/telephony/DataConnection;->setLinkProperties(Lcom/android/internal/telephony/DataCallState;Landroid/net/LinkProperties;)Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@16
    move-result-object v1

    #@17
    iput-object v1, v0, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;->setupResult:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@19
    .line 959
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;->setupResult:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@1b
    sget-object v2, Lcom/android/internal/telephony/DataCallState$SetupResult;->SUCCESS:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@1d
    if-eq v1, v2, :cond_38

    #@1f
    .line 960
    new-instance v1, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v2, "updateLinkProperty failed : "

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;->setupResult:Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@37
    goto :goto_9

    #@38
    .line 964
    :cond_38
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;->newLp:Landroid/net/LinkProperties;

    #@3a
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnection;->mLinkProperties:Landroid/net/LinkProperties;

    #@3c
    invoke-virtual {v2}, Landroid/net/LinkProperties;->getHttpProxy()Landroid/net/ProxyProperties;

    #@3f
    move-result-object v2

    #@40
    invoke-virtual {v1, v2}, Landroid/net/LinkProperties;->setHttpProxy(Landroid/net/ProxyProperties;)V

    #@43
    .line 966
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;->oldLp:Landroid/net/LinkProperties;

    #@45
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;->newLp:Landroid/net/LinkProperties;

    #@47
    invoke-virtual {v1, v2}, Landroid/net/LinkProperties;->equals(Ljava/lang/Object;)Z

    #@4a
    move-result v1

    #@4b
    if-nez v1, :cond_7d

    #@4d
    .line 967
    new-instance v1, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v2, "updateLinkProperty old LP="

    #@54
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v1

    #@58
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;->oldLp:Landroid/net/LinkProperties;

    #@5a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v1

    #@5e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v1

    #@62
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@65
    .line 968
    new-instance v1, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v2, "updateLinkProperty new LP="

    #@6c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v1

    #@70
    iget-object v2, v0, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;->newLp:Landroid/net/LinkProperties;

    #@72
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v1

    #@76
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v1

    #@7a
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@7d
    .line 970
    :cond_7d
    iget-object v1, v0, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;->newLp:Landroid/net/LinkProperties;

    #@7f
    iput-object v1, p0, Lcom/android/internal/telephony/DataConnection;->mLinkProperties:Landroid/net/LinkProperties;

    #@81
    goto :goto_9
.end method


# virtual methods
.method public bringUp(Landroid/os/Message;Lcom/android/internal/telephony/DataProfile;)V
    .registers 5
    .parameter "onCompletedMsg"
    .parameter "apn"

    #@0
    .prologue
    .line 2193
    const/high16 v0, 0x4

    #@2
    new-instance v1, Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@4
    invoke-direct {v1, p2, p1}, Lcom/android/internal/telephony/DataConnection$ConnectionParams;-><init>(Lcom/android/internal/telephony/DataProfile;Landroid/os/Message;)V

    #@7
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/DataConnection;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnection;->sendMessage(Landroid/os/Message;)V

    #@e
    .line 2194
    return-void
.end method

.method protected clearSettings()V
    .registers 6

    #@0
    .prologue
    const-wide/16 v3, -0x1

    #@2
    const/4 v2, 0x5

    #@3
    const/4 v1, -0x1

    #@4
    .line 711
    const-string v0, "clearSettings"

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@9
    .line 713
    iput-wide v3, p0, Lcom/android/internal/telephony/DataConnection;->createTime:J

    #@b
    .line 714
    iput-wide v3, p0, Lcom/android/internal/telephony/DataConnection;->lastFailTime:J

    #@d
    .line 715
    sget-object v0, Lcom/android/internal/telephony/DataConnection$FailCause;->NONE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@f
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnection;->lastFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@11
    .line 716
    iput v1, p0, Lcom/android/internal/telephony/DataConnection;->mRetryOverride:I

    #@13
    .line 717
    const/4 v0, 0x0

    #@14
    iput v0, p0, Lcom/android/internal/telephony/DataConnection;->mRefCount:I

    #@16
    .line 718
    iput v1, p0, Lcom/android/internal/telephony/DataConnection;->cid:I

    #@18
    .line 721
    new-array v0, v2, [Ljava/lang/String;

    #@1a
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnection;->pcscfAddr_ipv4:[Ljava/lang/String;

    #@1c
    .line 722
    new-array v0, v2, [Ljava/lang/String;

    #@1e
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnection;->pcscfAddr_ipv6:[Ljava/lang/String;

    #@20
    .line 725
    new-instance v0, Landroid/net/LinkProperties;

    #@22
    invoke-direct {v0}, Landroid/net/LinkProperties;-><init>()V

    #@25
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mLinkProperties:Landroid/net/LinkProperties;

    #@27
    .line 726
    const/4 v0, 0x0

    #@28
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mApn:Lcom/android/internal/telephony/DataProfile;

    #@2a
    .line 727
    return-void
.end method

.method public configureRetry(III)Z
    .registers 5
    .parameter "maxRetryCount"
    .parameter "retryTime"
    .parameter "randomizationTime"

    #@0
    .prologue
    .line 691
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mRetryMgr:Lcom/android/internal/telephony/RetryManager;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/internal/telephony/RetryManager;->configure(III)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public configureRetry(Ljava/lang/String;)Z
    .registers 3
    .parameter "configStr"

    #@0
    .prologue
    .line 698
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mRetryMgr:Lcom/android/internal/telephony/RetryManager;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/RetryManager;->configure(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 7
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 2246
    const-string v0, "DataConnection "

    #@2
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5
    .line 2247
    invoke-super {p0, p1, p2, p3}, Lcom/android/internal/util/StateMachine;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@8
    .line 2248
    new-instance v0, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v1, " mApnList="

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnection;->mApnList:Ljava/util/List;

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@20
    .line 2249
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    #@23
    .line 2250
    new-instance v0, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v1, " mDataConnectionTracker="

    #@2a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v0

    #@2e
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnection;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@30
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v0

    #@34
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v0

    #@38
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3b
    .line 2251
    new-instance v0, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v1, " mApn="

    #@42
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v0

    #@46
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnection;->mApn:Lcom/android/internal/telephony/DataProfile;

    #@48
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v0

    #@4c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v0

    #@50
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@53
    .line 2252
    new-instance v0, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v1, " mTag="

    #@5a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v0

    #@5e
    iget v1, p0, Lcom/android/internal/telephony/DataConnection;->mTag:I

    #@60
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@63
    move-result-object v0

    #@64
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v0

    #@68
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@6b
    .line 2253
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    #@6e
    .line 2254
    new-instance v0, Ljava/lang/StringBuilder;

    #@70
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@73
    const-string v1, " phone="

    #@75
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v0

    #@79
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@7b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v0

    #@7f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v0

    #@83
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@86
    .line 2255
    new-instance v0, Ljava/lang/StringBuilder;

    #@88
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@8b
    const-string v1, " mRilVersion="

    #@8d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v0

    #@91
    iget v1, p0, Lcom/android/internal/telephony/DataConnection;->mRilVersion:I

    #@93
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@96
    move-result-object v0

    #@97
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9a
    move-result-object v0

    #@9b
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@9e
    .line 2256
    new-instance v0, Ljava/lang/StringBuilder;

    #@a0
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@a3
    const-string v1, " cid="

    #@a5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v0

    #@a9
    iget v1, p0, Lcom/android/internal/telephony/DataConnection;->cid:I

    #@ab
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v0

    #@af
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b2
    move-result-object v0

    #@b3
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b6
    .line 2257
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    #@b9
    .line 2258
    new-instance v0, Ljava/lang/StringBuilder;

    #@bb
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@be
    const-string v1, " mLinkProperties="

    #@c0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v0

    #@c4
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnection;->mLinkProperties:Landroid/net/LinkProperties;

    #@c6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v0

    #@ca
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cd
    move-result-object v0

    #@ce
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@d1
    .line 2259
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    #@d4
    .line 2260
    new-instance v0, Ljava/lang/StringBuilder;

    #@d6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d9
    const-string v1, " mCapabilities="

    #@db
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v0

    #@df
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnection;->mCapabilities:Landroid/net/LinkCapabilities;

    #@e1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v0

    #@e5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e8
    move-result-object v0

    #@e9
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@ec
    .line 2261
    new-instance v0, Ljava/lang/StringBuilder;

    #@ee
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@f1
    const-string v1, " createTime="

    #@f3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v0

    #@f7
    iget-wide v1, p0, Lcom/android/internal/telephony/DataConnection;->createTime:J

    #@f9
    invoke-static {v1, v2}, Landroid/util/TimeUtils;->logTimeOfDay(J)Ljava/lang/String;

    #@fc
    move-result-object v1

    #@fd
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v0

    #@101
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@104
    move-result-object v0

    #@105
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@108
    .line 2262
    new-instance v0, Ljava/lang/StringBuilder;

    #@10a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@10d
    const-string v1, " lastFailTime="

    #@10f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@112
    move-result-object v0

    #@113
    iget-wide v1, p0, Lcom/android/internal/telephony/DataConnection;->lastFailTime:J

    #@115
    invoke-static {v1, v2}, Landroid/util/TimeUtils;->logTimeOfDay(J)Ljava/lang/String;

    #@118
    move-result-object v1

    #@119
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v0

    #@11d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@120
    move-result-object v0

    #@121
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@124
    .line 2263
    new-instance v0, Ljava/lang/StringBuilder;

    #@126
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@129
    const-string v1, " lastFailCause="

    #@12b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12e
    move-result-object v0

    #@12f
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnection;->lastFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@131
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@134
    move-result-object v0

    #@135
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@138
    move-result-object v0

    #@139
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@13c
    .line 2264
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    #@13f
    .line 2265
    new-instance v0, Ljava/lang/StringBuilder;

    #@141
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@144
    const-string v1, " mRetryOverride="

    #@146
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@149
    move-result-object v0

    #@14a
    iget v1, p0, Lcom/android/internal/telephony/DataConnection;->mRetryOverride:I

    #@14c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14f
    move-result-object v0

    #@150
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@153
    move-result-object v0

    #@154
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@157
    .line 2266
    new-instance v0, Ljava/lang/StringBuilder;

    #@159
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@15c
    const-string v1, " mRefCount="

    #@15e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@161
    move-result-object v0

    #@162
    iget v1, p0, Lcom/android/internal/telephony/DataConnection;->mRefCount:I

    #@164
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@167
    move-result-object v0

    #@168
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16b
    move-result-object v0

    #@16c
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@16f
    .line 2267
    new-instance v0, Ljava/lang/StringBuilder;

    #@171
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@174
    const-string v1, " userData="

    #@176
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@179
    move-result-object v0

    #@17a
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnection;->userData:Ljava/lang/Object;

    #@17c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@17f
    move-result-object v0

    #@180
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@183
    move-result-object v0

    #@184
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@187
    .line 2268
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mRetryMgr:Lcom/android/internal/telephony/RetryManager;

    #@189
    if-eqz v0, :cond_1a3

    #@18b
    new-instance v0, Ljava/lang/StringBuilder;

    #@18d
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@190
    const-string v1, " "

    #@192
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@195
    move-result-object v0

    #@196
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnection;->mRetryMgr:Lcom/android/internal/telephony/RetryManager;

    #@198
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19b
    move-result-object v0

    #@19c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19f
    move-result-object v0

    #@1a0
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1a3
    .line 2269
    :cond_1a3
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    #@1a6
    .line 2270
    return-void
.end method

.method public getDataConnectionId()I
    .registers 2

    #@0
    .prologue
    .line 619
    iget v0, p0, Lcom/android/internal/telephony/DataConnection;->mId:I

    #@2
    return v0
.end method

.method public getIpv4PcscfAddress()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 2275
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection;->pcscfAddr_ipv4:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getIpv6PcscfAddress()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 2279
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection;->pcscfAddr_ipv6:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getRetryCount()I
    .registers 2

    #@0
    .prologue
    .line 634
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mRetryMgr:Lcom/android/internal/telephony/RetryManager;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/RetryManager;->getRetryCount()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getRetryTimer()I
    .registers 2

    #@0
    .prologue
    .line 649
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mRetryMgr:Lcom/android/internal/telephony/RetryManager;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/RetryManager;->getRetryTimer()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method protected getRilRadioTechnology(I)I
    .registers 5
    .parameter "defaultRilRadioTechnology"

    #@0
    .prologue
    .line 595
    iget v1, p0, Lcom/android/internal/telephony/DataConnection;->mRilVersion:I

    #@2
    const/4 v2, 0x6

    #@3
    if-ge v1, v2, :cond_7

    #@5
    .line 596
    move v0, p1

    #@6
    .line 600
    .local v0, rilRadioTechnology:I
    :goto_6
    return v0

    #@7
    .line 598
    .end local v0           #rilRadioTechnology:I
    :cond_7
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@9
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1}, Landroid/telephony/ServiceState;->getRilRadioTechnology()I

    #@10
    move-result v1

    #@11
    add-int/lit8 v0, v1, 0x2

    #@13
    .restart local v0       #rilRadioTechnology:I
    goto :goto_6
.end method

.method protected getWhatToString(I)Ljava/lang/String;
    .registers 3
    .parameter "what"

    #@0
    .prologue
    .line 2229
    const/4 v0, 0x0

    #@1
    .line 2230
    .local v0, info:Ljava/lang/String;
    invoke-static {p1}, Lcom/android/internal/telephony/DataConnection;->cmdToString(I)Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 2231
    if-nez v0, :cond_b

    #@7
    .line 2232
    invoke-static {p1}, Lcom/android/internal/telephony/DataConnectionAc;->cmdToString(I)Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    .line 2234
    :cond_b
    return-object v0
.end method

.method public increaseRetryCount()V
    .registers 2

    #@0
    .prologue
    .line 656
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mRetryMgr:Lcom/android/internal/telephony/RetryManager;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/RetryManager;->increaseRetryCount()V

    #@5
    .line 657
    return-void
.end method

.method protected abstract isDnsOk([Ljava/lang/String;)Z
.end method

.method public isRetryForever()Z
    .registers 2

    #@0
    .prologue
    .line 684
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mRetryMgr:Lcom/android/internal/telephony/RetryManager;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/RetryManager;->isRetryForever()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isRetryNeeded()Z
    .registers 2

    #@0
    .prologue
    .line 663
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mRetryMgr:Lcom/android/internal/telephony/RetryManager;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/RetryManager;->isRetryNeeded()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method protected abstract log(Ljava/lang/String;)V
.end method

.method protected abstract onConnect(Lcom/android/internal/telephony/DataConnection$ConnectionParams;)V
.end method

.method public resetRetryCount()V
    .registers 2

    #@0
    .prologue
    .line 670
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mRetryMgr:Lcom/android/internal/telephony/RetryManager;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/RetryManager;->resetRetryCount()V

    #@5
    .line 671
    return-void
.end method

.method public retryForeverUsingLastTimeout()V
    .registers 2

    #@0
    .prologue
    .line 677
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mRetryMgr:Lcom/android/internal/telephony/RetryManager;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/RetryManager;->retryForeverUsingLastTimeout()V

    #@5
    .line 678
    return-void
.end method

.method public setRetryCount(I)V
    .registers 4
    .parameter "retryCount"

    #@0
    .prologue
    .line 641
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "setRetryCount: "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@16
    .line 642
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection;->mRetryMgr:Lcom/android/internal/telephony/RetryManager;

    #@18
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/RetryManager;->setRetryCount(I)V

    #@1b
    .line 643
    return-void
.end method

.method public tearDown(Ljava/lang/String;Landroid/os/Message;)V
    .registers 5
    .parameter "reason"
    .parameter "onCompletedMsg"

    #@0
    .prologue
    .line 2203
    const v0, 0x40004

    #@3
    new-instance v1, Lcom/android/internal/telephony/DataConnection$DisconnectParams;

    #@5
    invoke-direct {v1, p1, p2}, Lcom/android/internal/telephony/DataConnection$DisconnectParams;-><init>(Ljava/lang/String;Landroid/os/Message;)V

    #@8
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/DataConnection;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnection;->sendMessage(Landroid/os/Message;)V

    #@f
    .line 2204
    return-void
.end method

.method public tearDown(Ljava/lang/String;Landroid/os/Message;I)V
    .registers 7
    .parameter "reason"
    .parameter "onCompletedMsg"
    .parameter "delayMillis"

    #@0
    .prologue
    .line 2208
    const v0, 0x40004

    #@3
    new-instance v1, Lcom/android/internal/telephony/DataConnection$DisconnectParams;

    #@5
    invoke-direct {v1, p1, p2}, Lcom/android/internal/telephony/DataConnection$DisconnectParams;-><init>(Ljava/lang/String;Landroid/os/Message;)V

    #@8
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/DataConnection;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@b
    move-result-object v0

    #@c
    int-to-long v1, p3

    #@d
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/internal/telephony/DataConnection;->sendMessageDelayed(Landroid/os/Message;J)V

    #@10
    .line 2209
    return-void
.end method

.method public tearDownAll(Ljava/lang/String;Landroid/os/Message;)V
    .registers 5
    .parameter "reason"
    .parameter "onCompletedMsg"

    #@0
    .prologue
    .line 2220
    const v0, 0x40006

    #@3
    new-instance v1, Lcom/android/internal/telephony/DataConnection$DisconnectParams;

    #@5
    invoke-direct {v1, p1, p2}, Lcom/android/internal/telephony/DataConnection$DisconnectParams;-><init>(Ljava/lang/String;Landroid/os/Message;)V

    #@8
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/DataConnection;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnection;->sendMessage(Landroid/os/Message;)V

    #@f
    .line 2222
    return-void
.end method

.method public abstract toString()Ljava/lang/String;
.end method
