.class public Lcom/android/internal/telephony/SecurityManager;
.super Ljava/lang/Object;
.source "SecurityManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;,
        Lcom/android/internal/telephony/SecurityManager$Depersonalization;,
        Lcom/android/internal/telephony/SecurityManager$Personalization;,
        Lcom/android/internal/telephony/SecurityManager$PersonalizationCheck;,
        Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "SecurityManager"

.field private static final SECURITY_PROCESS_COMMAND_ANTI_ROLLBACK:I = 0xb

.field private static final SECURITY_PROCESS_COMMAND_APP_IMGAE_INTEGRITY:I = 0x9

.field private static final SECURITY_PROCESS_COMMAND_AT_COMMAND:I = 0xc

.field private static final SECURITY_PROCESS_COMMAND_BOOT_CODE_INTEGRITY:I = 0x7

.field private static final SECURITY_PROCESS_COMMAND_BOOT_CODE_PROTECTION:I = 0x5

.field private static final SECURITY_PROCESS_COMMAND_CHECK_APP_INTEGRITY:I = 0xa

.field private static final SECURITY_PROCESS_COMMAND_DEBUG_ENABLE:I = 0xe

.field private static final SECURITY_PROCESS_COMMAND_DEPERSONALIZATION:I = 0x2

.field private static final SECURITY_PROCESS_COMMAND_PERSONALIZATION:I = 0x3

.field private static final SECURITY_PROCESS_COMMAND_PERSONALIZATION_CHECK:I = 0x0

.field private static final SECURITY_PROCESS_COMMAND_PERSONALIZATION_INFORMATION:I = 0x1

.field private static final SECURITY_PROCESS_COMMAND_SECURITY_HEADER_INTEGRITY:I = 0x8

.field private static final SECURITY_PROCESS_COMMAND_SENSITIVE_DATA_INTEGRITY:I = 0x6

.field private static final SECURITY_PROCESS_COMMAND_SENSITIVE_DATA_PROTECTION:I = 0x4

.field private static final SECURITY_PROCESS_COMMAND_SW_VERSION:I = 0xd

.field private static final enableVLog:Z = true


# instance fields
.field public info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

.field private final length:B

.field private final mHeaderSize:B

.field public final mNtcodeSize:I

.field private final mOemIdentifier:Ljava/lang/String;

.field public final mTotalNtcodes:I

.field private final tag:B


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x4

    #@1
    .line 64
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 36
    const-string v0, "SECURITY"

    #@6
    iput-object v0, p0, Lcom/android/internal/telephony/SecurityManager;->mOemIdentifier:Ljava/lang/String;

    #@8
    .line 38
    const/16 v0, 0x8

    #@a
    iput-byte v0, p0, Lcom/android/internal/telephony/SecurityManager;->mHeaderSize:B

    #@c
    .line 39
    iput-byte v1, p0, Lcom/android/internal/telephony/SecurityManager;->tag:B

    #@e
    .line 40
    iput-byte v1, p0, Lcom/android/internal/telephony/SecurityManager;->length:B

    #@10
    .line 44
    const/16 v0, 0x10

    #@12
    iput v0, p0, Lcom/android/internal/telephony/SecurityManager;->mNtcodeSize:I

    #@14
    .line 45
    const/16 v0, 0x281

    #@16
    iput v0, p0, Lcom/android/internal/telephony/SecurityManager;->mTotalNtcodes:I

    #@18
    .line 66
    return-void
.end method

.method private SecurityCommandSender(I[B[B)I
    .registers 13
    .parameter "command"
    .parameter "request"
    .parameter "response"

    #@0
    .prologue
    .line 275
    const/4 v5, 0x0

    #@1
    .line 276
    .local v5, returnValue:I
    const-string v6, "SecurityCommandSender Start!"

    #@3
    invoke-static {v6}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@6
    .line 279
    :try_start_6
    new-instance v1, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;

    #@8
    invoke-direct {v1}, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;-><init>()V

    #@b
    .line 280
    .local v1, Thread:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;
    invoke-virtual {v1}, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->start()V

    #@e
    .line 281
    invoke-static {v1, p1, p2, p3}, Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;->access$200(Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;ILjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    move-result-object v4

    #@12
    .line 282
    .local v4, result:Ljava/lang/Object;
    new-instance v6, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v7, "SecurityCommandSender result = "

    #@19
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v6

    #@1d
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v6

    #@21
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v6

    #@25
    invoke-static {v6}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@28
    .line 284
    if-eqz v4, :cond_81

    #@2a
    .line 285
    check-cast v4, [B

    #@2c
    .end local v4           #result:Ljava/lang/Object;
    move-object v0, v4

    #@2d
    check-cast v0, [B

    #@2f
    move-object v3, v0

    #@30
    .line 286
    .local v3, responseData:[B
    array-length v6, v3

    #@31
    array-length v7, p3

    #@32
    if-le v6, v7, :cond_5e

    #@34
    .line 287
    const-string v6, "SecurityManager"

    #@36
    new-instance v7, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v8, "Buffer to copy response too small: Response length is "

    #@3d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v7

    #@41
    array-length v8, v3

    #@42
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@45
    move-result-object v7

    #@46
    const-string v8, "bytes. Buffer Size is "

    #@48
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v7

    #@4c
    array-length v8, p3

    #@4d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@50
    move-result-object v7

    #@51
    const-string v8, "bytes."

    #@53
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v7

    #@57
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v7

    #@5b
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 291
    :cond_5e
    const/4 v6, 0x0

    #@5f
    const/4 v7, 0x0

    #@60
    array-length v8, v3

    #@61
    invoke-static {v3, v6, p3, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@64
    .line 292
    array-length v5, v3
    :try_end_65
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_65} :catch_83

    #@65
    .line 303
    .end local v1           #Thread:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;
    .end local v3           #responseData:[B
    :cond_65
    :goto_65
    new-instance v6, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v7, "SecurityCommandSender returnValue = "

    #@6c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v6

    #@70
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@73
    move-result-object v6

    #@74
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v6

    #@78
    invoke-static {v6}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@7b
    .line 304
    const-string v6, "SecurityCommandSender End!"

    #@7d
    invoke-static {v6}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@80
    .line 306
    return v5

    #@81
    .line 295
    .restart local v1       #Thread:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;
    .restart local v4       #result:Ljava/lang/Object;
    :cond_81
    const/4 v5, 0x0

    #@82
    goto :goto_65

    #@83
    .line 297
    .end local v1           #Thread:Lcom/android/internal/telephony/SecurityManager$SecurityCommandThread;
    .end local v4           #result:Ljava/lang/Object;
    :catch_83
    move-exception v2

    #@84
    .line 298
    .local v2, e:Ljava/lang/RuntimeException;
    const-string v6, "SecurityManager"

    #@86
    const-string v7, "SecurityCommandSender: Runtime Exception"

    #@88
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@8b
    .line 299
    sget-object v6, Lcom/android/internal/telephony/CommandException$Error;->GENERIC_FAILURE:Lcom/android/internal/telephony/CommandException$Error;

    #@8d
    invoke-virtual {v6}, Lcom/android/internal/telephony/CommandException$Error;->ordinal()I

    #@90
    move-result v5

    #@91
    .line 300
    if-lez v5, :cond_65

    #@93
    mul-int/lit8 v5, v5, -0x1

    #@95
    goto :goto_65
.end method

.method private static SecurityManagerLog(Ljava/lang/String;)V
    .registers 2
    .parameter "logString"

    #@0
    .prologue
    .line 70
    const-string v0, "SecurityManager"

    #@2
    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 72
    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 31
    invoke-static {p0}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@3
    return-void
.end method


# virtual methods
.method public depersonalization(ILjava/lang/String;)I
    .registers 15
    .parameter "type"
    .parameter "pin"

    #@0
    .prologue
    const/4 v11, 0x2

    #@1
    const/4 v8, 0x1

    #@2
    const/4 v7, 0x0

    #@3
    .line 398
    const/16 v6, 0x19

    #@5
    .line 399
    .local v6, velueSize:B
    const/4 v5, 0x0

    #@6
    .line 400
    .local v5, termChar:B
    const/16 v9, 0x29

    #@8
    new-array v2, v9, [B

    #@a
    .line 401
    .local v2, request:[B
    const/16 v9, 0x800

    #@c
    new-array v3, v9, [B

    #@e
    .line 402
    .local v3, response:[B
    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    #@11
    move-result-object v1

    #@12
    .line 403
    .local v1, reqBuffer:Ljava/nio/ByteBuffer;
    int-to-byte v0, p1

    #@13
    .line 405
    .local v0, lock_type:B
    const-string v9, "depersonalization Start!"

    #@15
    invoke-static {v9}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@18
    .line 407
    new-instance v9, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v10, "depersonalization: type("

    #@1f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v9

    #@23
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v9

    #@27
    const-string v10, ")\t pin("

    #@29
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v9

    #@2d
    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v9

    #@31
    const-string v10, ")"

    #@33
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v9

    #@37
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v9

    #@3b
    invoke-static {v9}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@3e
    .line 409
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    #@41
    move-result-object v9

    #@42
    invoke-virtual {v1, v9}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@45
    .line 412
    const-string v9, "SECURITY"

    #@47
    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    #@4a
    move-result-object v9

    #@4b
    invoke-virtual {v1, v9}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@4e
    .line 415
    invoke-virtual {v1, v11}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@51
    .line 418
    invoke-virtual {v1, v6}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@54
    .line 421
    invoke-virtual {v1, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@57
    .line 422
    const/16 v9, 0x10

    #@59
    invoke-virtual {v1, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@5c
    .line 423
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    #@5f
    move-result-object v9

    #@60
    invoke-virtual {v1, v9}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@63
    .line 424
    invoke-virtual {v1, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@66
    .line 426
    invoke-direct {p0, v11, v2, v3}, Lcom/android/internal/telephony/SecurityManager;->SecurityCommandSender(I[B[B)I

    #@69
    move-result v4

    #@6a
    .line 428
    .local v4, result:I
    new-instance v9, Ljava/lang/StringBuilder;

    #@6c
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@6f
    const-string v10, "depersonalization: response ("

    #@71
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v9

    #@75
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v9

    #@79
    const-string v10, ")"

    #@7b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v9

    #@7f
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v9

    #@83
    invoke-static {v9}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@86
    .line 430
    const-string v9, "depersonalization End!"

    #@88
    invoke-static {v9}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@8b
    .line 433
    aget-byte v9, v3, v7

    #@8d
    if-ne v9, v8, :cond_92

    #@8f
    if-lez v4, :cond_92

    #@91
    .line 436
    :goto_91
    return v7

    #@92
    :cond_92
    move v7, v8

    #@93
    goto :goto_91
.end method

.method public enableRootPermission()I
    .registers 8

    #@0
    .prologue
    const/16 v6, 0xe

    #@2
    .line 478
    const/4 v4, 0x0

    #@3
    .line 479
    .local v4, termChar:B
    const/16 v5, 0x10

    #@5
    new-array v1, v5, [B

    #@7
    .line 480
    .local v1, request:[B
    const/16 v5, 0x800

    #@9
    new-array v2, v5, [B

    #@b
    .line 481
    .local v2, response:[B
    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    #@e
    move-result-object v0

    #@f
    .line 483
    .local v0, reqBuffer:Ljava/nio/ByteBuffer;
    const-string v5, "enableRootPermission --- "

    #@11
    invoke-static {v5}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@14
    .line 485
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    #@17
    move-result-object v5

    #@18
    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@1b
    .line 488
    const-string v5, "SECURITY"

    #@1d
    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    #@20
    move-result-object v5

    #@21
    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@24
    .line 491
    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@27
    .line 493
    const/4 v5, 0x1

    #@28
    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@2b
    .line 496
    invoke-direct {p0, v6, v1, v2}, Lcom/android/internal/telephony/SecurityManager;->SecurityCommandSender(I[B[B)I

    #@2e
    move-result v3

    #@2f
    .line 498
    .local v3, result:I
    const/4 v5, 0x0

    #@30
    aget-byte v5, v2, v5

    #@32
    return v5
.end method

.method public getSIMLockLeftCntWithIndex(I)B
    .registers 5
    .parameter "type"

    #@0
    .prologue
    .line 536
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "getSIMLockLeftCntWithIndex Start!, type = ("

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    const-string v2, ")"

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-static {v1}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@1c
    .line 537
    packed-switch p1, :pswitch_data_64

    #@1f
    .line 552
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@21
    iget-object v1, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->sim:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@23
    iget-byte v0, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->attemptsLeft:B

    #@25
    .line 556
    .local v0, curSimLockLeftCnt:B
    :goto_25
    new-instance v1, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v2, "getSIMLockLeftCntWithIndex!, curSimLockLeftCnt = ("

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    const-string v2, ")"

    #@36
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v1

    #@3a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v1

    #@3e
    invoke-static {v1}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@41
    .line 557
    const-string v1, "getSIMLockLeftCntWithIndex End!"

    #@43
    invoke-static {v1}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@46
    .line 559
    return v0

    #@47
    .line 540
    .end local v0           #curSimLockLeftCnt:B
    :pswitch_47
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@49
    iget-object v1, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->network:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@4b
    iget-byte v0, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->attemptsLeft:B

    #@4d
    .line 541
    .restart local v0       #curSimLockLeftCnt:B
    goto :goto_25

    #@4e
    .line 543
    .end local v0           #curSimLockLeftCnt:B
    :pswitch_4e
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@50
    iget-object v1, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->serviceprovider:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@52
    iget-byte v0, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->attemptsLeft:B

    #@54
    .line 544
    .restart local v0       #curSimLockLeftCnt:B
    goto :goto_25

    #@55
    .line 546
    .end local v0           #curSimLockLeftCnt:B
    :pswitch_55
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@57
    iget-object v1, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->networksubset:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@59
    iget-byte v0, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->attemptsLeft:B

    #@5b
    .line 547
    .restart local v0       #curSimLockLeftCnt:B
    goto :goto_25

    #@5c
    .line 549
    .end local v0           #curSimLockLeftCnt:B
    :pswitch_5c
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@5e
    iget-object v1, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->corporate:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@60
    iget-byte v0, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->attemptsLeft:B

    #@62
    .line 550
    .restart local v0       #curSimLockLeftCnt:B
    goto :goto_25

    #@63
    .line 537
    nop

    #@64
    :pswitch_data_64
    .packed-switch 0x1
        :pswitch_47
        :pswitch_4e
        :pswitch_55
        :pswitch_5c
    .end packed-switch
.end method

.method public getSIMLockRetryCntMaxWithIndex(I)B
    .registers 5
    .parameter "type"

    #@0
    .prologue
    .line 565
    packed-switch p1, :pswitch_data_48

    #@3
    .line 580
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@5
    iget-object v1, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->sim:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@7
    iget-byte v0, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->retry_count_max:B

    #@9
    .line 584
    .local v0, curSimLockRetryCntMax:B
    :goto_9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v2, "getSIMLockRetryCntMaxWithIndex!, curSimLockRetryCntMax = ("

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    const-string v2, ")"

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-static {v1}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@25
    .line 585
    const-string v1, "getSIMLockRetryCntMaxWithIndex End!"

    #@27
    invoke-static {v1}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@2a
    .line 587
    return v0

    #@2b
    .line 568
    .end local v0           #curSimLockRetryCntMax:B
    :pswitch_2b
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@2d
    iget-object v1, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->network:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@2f
    iget-byte v0, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->retry_count_max:B

    #@31
    .line 569
    .restart local v0       #curSimLockRetryCntMax:B
    goto :goto_9

    #@32
    .line 571
    .end local v0           #curSimLockRetryCntMax:B
    :pswitch_32
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@34
    iget-object v1, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->serviceprovider:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@36
    iget-byte v0, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->retry_count_max:B

    #@38
    .line 572
    .restart local v0       #curSimLockRetryCntMax:B
    goto :goto_9

    #@39
    .line 574
    .end local v0           #curSimLockRetryCntMax:B
    :pswitch_39
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@3b
    iget-object v1, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->networksubset:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@3d
    iget-byte v0, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->retry_count_max:B

    #@3f
    .line 575
    .restart local v0       #curSimLockRetryCntMax:B
    goto :goto_9

    #@40
    .line 577
    .end local v0           #curSimLockRetryCntMax:B
    :pswitch_40
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@42
    iget-object v1, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->corporate:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@44
    iget-byte v0, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->retry_count_max:B

    #@46
    .line 578
    .restart local v0       #curSimLockRetryCntMax:B
    goto :goto_9

    #@47
    .line 565
    nop

    #@48
    :pswitch_data_48
    .packed-switch 0x1
        :pswitch_2b
        :pswitch_32
        :pswitch_39
        :pswitch_40
    .end packed-switch
.end method

.method public getSIMLockflagWithIndex(I)I
    .registers 5
    .parameter "type"

    #@0
    .prologue
    .line 507
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "getSIMLockflagWithIndex Start!, type = ("

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    const-string v2, ")"

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-static {v1}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@1c
    .line 508
    packed-switch p1, :pswitch_data_64

    #@1f
    .line 523
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@21
    iget-object v1, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->sim:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@23
    iget v0, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->flag:I

    #@25
    .line 527
    .local v0, curSimLockflag:I
    :goto_25
    new-instance v1, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v2, "getSIMLockflagWithIndex!, curSimLockflag = ("

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    const-string v2, ")"

    #@36
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v1

    #@3a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v1

    #@3e
    invoke-static {v1}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@41
    .line 528
    const-string v1, "getSIMLockflagWithIndex End!"

    #@43
    invoke-static {v1}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@46
    .line 530
    return v0

    #@47
    .line 511
    .end local v0           #curSimLockflag:I
    :pswitch_47
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@49
    iget-object v1, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->network:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@4b
    iget v0, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->flag:I

    #@4d
    .line 512
    .restart local v0       #curSimLockflag:I
    goto :goto_25

    #@4e
    .line 514
    .end local v0           #curSimLockflag:I
    :pswitch_4e
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@50
    iget-object v1, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->serviceprovider:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@52
    iget v0, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->flag:I

    #@54
    .line 515
    .restart local v0       #curSimLockflag:I
    goto :goto_25

    #@55
    .line 517
    .end local v0           #curSimLockflag:I
    :pswitch_55
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@57
    iget-object v1, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->networksubset:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@59
    iget v0, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->flag:I

    #@5b
    .line 518
    .restart local v0       #curSimLockflag:I
    goto :goto_25

    #@5c
    .line 520
    .end local v0           #curSimLockflag:I
    :pswitch_5c
    iget-object v1, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@5e
    iget-object v1, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->corporate:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@60
    iget v0, v1, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->flag:I

    #@62
    .line 521
    .restart local v0       #curSimLockflag:I
    goto :goto_25

    #@63
    .line 508
    nop

    #@64
    :pswitch_data_64
    .packed-switch 0x1
        :pswitch_47
        :pswitch_4e
        :pswitch_55
        :pswitch_5c
    .end packed-switch
.end method

.method public personalization(II[B)I
    .registers 13
    .parameter "type"
    .parameter "cnt"
    .parameter "ntcode_list"

    #@0
    .prologue
    const/4 v8, 0x3

    #@1
    .line 442
    const/16 v5, 0x28a

    #@3
    .line 443
    .local v5, velueSize:I
    const/4 v4, 0x0

    #@4
    .line 444
    .local v4, termChar:B
    const/16 v6, 0x29a

    #@6
    new-array v1, v6, [B

    #@8
    .line 445
    .local v1, request:[B
    const/16 v6, 0x800

    #@a
    new-array v2, v6, [B

    #@c
    .line 446
    .local v2, response:[B
    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    #@f
    move-result-object v0

    #@10
    .line 448
    .local v0, reqBuffer:Ljava/nio/ByteBuffer;
    const-string v6, "personalization Start!"

    #@12
    invoke-static {v6}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@15
    .line 449
    new-instance v6, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v7, "personalization: type("

    #@1c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v6

    #@20
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v6

    #@24
    const-string v7, ")\t pin("

    #@26
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v6

    #@2a
    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v6

    #@2e
    const-string v7, ")"

    #@30
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v6

    #@34
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v6

    #@38
    invoke-static {v6}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@3b
    .line 451
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    #@3e
    move-result-object v6

    #@3f
    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@42
    .line 454
    const-string v6, "SECURITY"

    #@44
    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    #@47
    move-result-object v6

    #@48
    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@4b
    .line 457
    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@4e
    .line 460
    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@51
    .line 463
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@54
    .line 464
    mul-int/lit8 v6, p2, 0x10

    #@56
    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@59
    .line 466
    invoke-virtual {v0, p3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@5c
    .line 467
    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@5f
    .line 469
    invoke-direct {p0, v8, v1, v2}, Lcom/android/internal/telephony/SecurityManager;->SecurityCommandSender(I[B[B)I

    #@62
    move-result v3

    #@63
    .line 471
    .local v3, result:I
    const/4 v6, 0x0

    #@64
    aget-byte v6, v2, v6

    #@66
    return v6
.end method

.method public simlockinformation()I
    .registers 11

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 311
    const/4 v4, 0x0

    #@3
    .line 312
    .local v4, velueSize:B
    const/16 v7, 0x10

    #@5
    new-array v1, v7, [B

    #@7
    .line 313
    .local v1, request:[B
    const/16 v7, 0x800

    #@9
    new-array v2, v7, [B

    #@b
    .line 317
    .local v2, response:[B
    const-string v7, "simlockinformation Start!"

    #@d
    invoke-static {v7}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@10
    .line 319
    new-instance v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@12
    invoke-direct {v7, p0}, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;-><init>(Lcom/android/internal/telephony/SecurityManager;)V

    #@15
    iput-object v7, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@17
    .line 321
    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    #@1a
    move-result-object v0

    #@1b
    .line 322
    .local v0, reqBuffer:Ljava/nio/ByteBuffer;
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    #@1e
    move-result-object v7

    #@1f
    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@22
    .line 325
    const-string v7, "SECURITY"

    #@24
    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    #@27
    move-result-object v7

    #@28
    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@2b
    .line 328
    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@2e
    .line 331
    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@31
    .line 333
    invoke-direct {p0, v6, v1, v2}, Lcom/android/internal/telephony/SecurityManager;->SecurityCommandSender(I[B[B)I

    #@34
    move-result v3

    #@35
    .line 334
    .local v3, result:I
    const-string v7, "SecurityManager"

    #@37
    new-instance v8, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v9, "simlockinformation result: "

    #@3e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v8

    #@42
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@45
    move-result-object v8

    #@46
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v8

    #@4a
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    .line 335
    const-string v7, "SecurityManager"

    #@4f
    new-instance v8, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string v9, "simlockinformation response: "

    #@56
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v8

    #@5a
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v8

    #@5e
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v8

    #@62
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    .line 337
    if-lez v3, :cond_269

    #@67
    .line 339
    iget-object v7, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@69
    iget-object v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->network:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@6b
    aget-byte v8, v2, v5

    #@6d
    iput-byte v8, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->attemptsLeft:B

    #@6f
    .line 340
    iget-object v7, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@71
    iget-object v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->network:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@73
    aget-byte v6, v2, v6

    #@75
    iput v6, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->flag:I

    #@77
    .line 341
    iget-object v6, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@79
    iget-object v6, v6, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->network:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@7b
    const/4 v7, 0x2

    #@7c
    aget-byte v7, v2, v7

    #@7e
    iput-byte v7, v6, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->retry_count_max:B

    #@80
    .line 343
    iget-object v6, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@82
    iget-object v6, v6, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->networksubset:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@84
    const/4 v7, 0x3

    #@85
    aget-byte v7, v2, v7

    #@87
    iput-byte v7, v6, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->attemptsLeft:B

    #@89
    .line 344
    iget-object v6, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@8b
    iget-object v6, v6, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->networksubset:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@8d
    const/4 v7, 0x4

    #@8e
    aget-byte v7, v2, v7

    #@90
    iput v7, v6, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->flag:I

    #@92
    .line 345
    iget-object v6, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@94
    iget-object v6, v6, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->networksubset:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@96
    const/4 v7, 0x5

    #@97
    aget-byte v7, v2, v7

    #@99
    iput-byte v7, v6, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->retry_count_max:B

    #@9b
    .line 348
    iget-object v6, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@9d
    iget-object v6, v6, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->serviceprovider:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@9f
    const/4 v7, 0x6

    #@a0
    aget-byte v7, v2, v7

    #@a2
    iput-byte v7, v6, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->attemptsLeft:B

    #@a4
    .line 349
    iget-object v6, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@a6
    iget-object v6, v6, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->serviceprovider:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@a8
    const/4 v7, 0x7

    #@a9
    aget-byte v7, v2, v7

    #@ab
    iput v7, v6, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->flag:I

    #@ad
    .line 350
    iget-object v6, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@af
    iget-object v6, v6, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->serviceprovider:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@b1
    const/16 v7, 0x8

    #@b3
    aget-byte v7, v2, v7

    #@b5
    iput-byte v7, v6, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->retry_count_max:B

    #@b7
    .line 353
    iget-object v6, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@b9
    iget-object v6, v6, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->corporate:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@bb
    const/16 v7, 0x9

    #@bd
    aget-byte v7, v2, v7

    #@bf
    iput-byte v7, v6, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->attemptsLeft:B

    #@c1
    .line 354
    iget-object v6, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@c3
    iget-object v6, v6, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->corporate:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@c5
    const/16 v7, 0xa

    #@c7
    aget-byte v7, v2, v7

    #@c9
    iput v7, v6, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->flag:I

    #@cb
    .line 355
    iget-object v6, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@cd
    iget-object v6, v6, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->corporate:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@cf
    const/16 v7, 0xb

    #@d1
    aget-byte v7, v2, v7

    #@d3
    iput-byte v7, v6, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->retry_count_max:B

    #@d5
    .line 357
    iget-object v6, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@d7
    iget-object v6, v6, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->sim:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@d9
    const/16 v7, 0xc

    #@db
    aget-byte v7, v2, v7

    #@dd
    iput-byte v7, v6, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->attemptsLeft:B

    #@df
    .line 358
    iget-object v6, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@e1
    iget-object v6, v6, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->sim:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@e3
    const/16 v7, 0xd

    #@e5
    aget-byte v7, v2, v7

    #@e7
    iput v7, v6, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->flag:I

    #@e9
    .line 359
    iget-object v6, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@eb
    iget-object v6, v6, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->sim:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@ed
    const/16 v7, 0xe

    #@ef
    aget-byte v7, v2, v7

    #@f1
    iput-byte v7, v6, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->retry_count_max:B

    #@f3
    .line 362
    iget-object v6, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@f5
    const/16 v7, 0xf

    #@f7
    aget-byte v7, v2, v7

    #@f9
    iput v7, v6, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->country:I

    #@fb
    .line 363
    iget-object v6, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@fd
    const/16 v7, 0x13

    #@ff
    aget-byte v7, v2, v7

    #@101
    iput v7, v6, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->operator:I

    #@103
    .line 365
    new-instance v6, Ljava/lang/StringBuilder;

    #@105
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@108
    const-string v7, "network flag: "

    #@10a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v6

    #@10e
    iget-object v7, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@110
    iget-object v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->network:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@112
    iget v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->flag:I

    #@114
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@117
    move-result-object v6

    #@118
    const-string v7, "  network attemptsLeft: "

    #@11a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v6

    #@11e
    iget-object v7, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@120
    iget-object v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->network:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@122
    iget-byte v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->attemptsLeft:B

    #@124
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@127
    move-result-object v6

    #@128
    const-string v7, "  network retry_count_max: "

    #@12a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12d
    move-result-object v6

    #@12e
    iget-object v7, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@130
    iget-object v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->network:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@132
    iget-byte v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->retry_count_max:B

    #@134
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@137
    move-result-object v6

    #@138
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13b
    move-result-object v6

    #@13c
    invoke-static {v6}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@13f
    .line 369
    new-instance v6, Ljava/lang/StringBuilder;

    #@141
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@144
    const-string v7, "networksubset flag: "

    #@146
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@149
    move-result-object v6

    #@14a
    iget-object v7, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@14c
    iget-object v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->networksubset:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@14e
    iget v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->flag:I

    #@150
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@153
    move-result-object v6

    #@154
    const-string v7, "  networksubset attemptsLeft: "

    #@156
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@159
    move-result-object v6

    #@15a
    iget-object v7, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@15c
    iget-object v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->networksubset:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@15e
    iget-byte v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->attemptsLeft:B

    #@160
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@163
    move-result-object v6

    #@164
    const-string v7, "  networksubset retry_count_max: "

    #@166
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@169
    move-result-object v6

    #@16a
    iget-object v7, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@16c
    iget-object v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->networksubset:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@16e
    iget-byte v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->retry_count_max:B

    #@170
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@173
    move-result-object v6

    #@174
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@177
    move-result-object v6

    #@178
    invoke-static {v6}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@17b
    .line 373
    new-instance v6, Ljava/lang/StringBuilder;

    #@17d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@180
    const-string v7, "serviceprovider flag: "

    #@182
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@185
    move-result-object v6

    #@186
    iget-object v7, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@188
    iget-object v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->serviceprovider:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@18a
    iget v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->flag:I

    #@18c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18f
    move-result-object v6

    #@190
    const-string v7, "  serviceprovider attemptsLeft: "

    #@192
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@195
    move-result-object v6

    #@196
    iget-object v7, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@198
    iget-object v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->serviceprovider:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@19a
    iget-byte v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->attemptsLeft:B

    #@19c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19f
    move-result-object v6

    #@1a0
    const-string v7, "  serviceprovider retry_count_max: "

    #@1a2
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a5
    move-result-object v6

    #@1a6
    iget-object v7, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@1a8
    iget-object v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->serviceprovider:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@1aa
    iget-byte v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->retry_count_max:B

    #@1ac
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1af
    move-result-object v6

    #@1b0
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b3
    move-result-object v6

    #@1b4
    invoke-static {v6}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@1b7
    .line 377
    new-instance v6, Ljava/lang/StringBuilder;

    #@1b9
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1bc
    const-string v7, "corporate flag: "

    #@1be
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c1
    move-result-object v6

    #@1c2
    iget-object v7, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@1c4
    iget-object v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->corporate:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@1c6
    iget v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->flag:I

    #@1c8
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1cb
    move-result-object v6

    #@1cc
    const-string v7, "  corporate attemptsLeft: "

    #@1ce
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d1
    move-result-object v6

    #@1d2
    iget-object v7, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@1d4
    iget-object v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->corporate:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@1d6
    iget-byte v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->attemptsLeft:B

    #@1d8
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1db
    move-result-object v6

    #@1dc
    const-string v7, "  corporate retry_count_max: "

    #@1de
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e1
    move-result-object v6

    #@1e2
    iget-object v7, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@1e4
    iget-object v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->corporate:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@1e6
    iget-byte v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->retry_count_max:B

    #@1e8
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1eb
    move-result-object v6

    #@1ec
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ef
    move-result-object v6

    #@1f0
    invoke-static {v6}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@1f3
    .line 381
    new-instance v6, Ljava/lang/StringBuilder;

    #@1f5
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1f8
    const-string v7, "sim flag: "

    #@1fa
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fd
    move-result-object v6

    #@1fe
    iget-object v7, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@200
    iget-object v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->sim:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@202
    iget v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->flag:I

    #@204
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@207
    move-result-object v6

    #@208
    const-string v7, "  sim attemptsLeft: "

    #@20a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20d
    move-result-object v6

    #@20e
    iget-object v7, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@210
    iget-object v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->sim:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@212
    iget-byte v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->attemptsLeft:B

    #@214
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@217
    move-result-object v6

    #@218
    const-string v7, "  sim retry_count_max: "

    #@21a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21d
    move-result-object v6

    #@21e
    iget-object v7, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@220
    iget-object v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->sim:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;

    #@222
    iget-byte v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion$SIMLock;->retry_count_max:B

    #@224
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@227
    move-result-object v6

    #@228
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22b
    move-result-object v6

    #@22c
    invoke-static {v6}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@22f
    .line 385
    new-instance v6, Ljava/lang/StringBuilder;

    #@231
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@234
    const-string v7, "country: "

    #@236
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@239
    move-result-object v6

    #@23a
    iget-object v7, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@23c
    iget v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->country:I

    #@23e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@241
    move-result-object v6

    #@242
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@245
    move-result-object v6

    #@246
    invoke-static {v6}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@249
    .line 386
    new-instance v6, Ljava/lang/StringBuilder;

    #@24b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@24e
    const-string v7, "operator: "

    #@250
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@253
    move-result-object v6

    #@254
    iget-object v7, p0, Lcom/android/internal/telephony/SecurityManager;->info:Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;

    #@256
    iget v7, v7, Lcom/android/internal/telephony/SecurityManager$SIMLockInformtion;->operator:I

    #@258
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25b
    move-result-object v6

    #@25c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25f
    move-result-object v6

    #@260
    invoke-static {v6}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@263
    .line 388
    const-string v6, "simlockinformation End!"

    #@265
    invoke-static {v6}, Lcom/android/internal/telephony/SecurityManager;->SecurityManagerLog(Ljava/lang/String;)V

    #@268
    .line 393
    :goto_268
    return v5

    #@269
    :cond_269
    move v5, v6

    #@26a
    goto :goto_268
.end method
