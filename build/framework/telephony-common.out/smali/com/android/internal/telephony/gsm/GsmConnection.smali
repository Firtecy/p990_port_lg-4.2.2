.class public Lcom/android/internal/telephony/gsm/GsmConnection;
.super Lcom/android/internal/telephony/Connection;
.source "GsmConnection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/gsm/GsmConnection$1;,
        Lcom/android/internal/telephony/gsm/GsmConnection$MyHandler;
    }
.end annotation


# static fields
#the value of this static final field might be set in the static constructor
.field private static final CIQ_EXTENSION:Z = false

#the value of this static final field might be set in the static constructor
.field private static final DBG_CIQ:Z = false

.field static final EVENT_DTMF_DONE:I = 0x1

.field static final EVENT_NEXT_POST_DIAL:I = 0x3

.field static final EVENT_PAUSE_DONE:I = 0x2

.field static final EVENT_WAKE_LOCK_TIMEOUT:I = 0x4

.field static final LOG_TAG:Ljava/lang/String; = "GSM"

.field static final PAUSE_DELAY_FIRST_MILLIS:I = 0x64

.field static final PAUSE_DELAY_MILLIS:I = 0xbb8

.field static final WAKE_LOCK_TIMEOUT_MILLIS:I = 0xea60

.field private static ciqIDCount:I

.field private static tmus_ciqCount:I


# instance fields
.field CIQ_State:Lcom/android/internal/telephony/Call$State;

.field TOA:I

.field address:Ljava/lang/String;

.field cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field cdnipNumber:Ljava/lang/String;

.field private ciqID:I

.field private ciqState:B

.field connectTime:J

.field createTime:J

.field dialString:Ljava/lang/String;

.field dialedAddress:Ljava/lang/String;

.field disconnectTime:J

.field disconnected:Z

.field duration:J

.field h:Landroid/os/Handler;

.field holdingStartTime:J

.field index:I

.field isIncoming:Z

.field private isPause:Z

.field private mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

.field nextPostDialChar:I

.field numberPresentation:I

.field owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

.field parent:Lcom/android/internal/telephony/gsm/GsmCall;

.field postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

.field postDialString:Ljava/lang/String;

.field redialNumber:Ljava/lang/String;

.field redirectNumber:Ljava/lang/String;

.field private systemCode:I

.field private termCode:S

.field private tmus_ciqId:I

.field uusInfo:Lcom/android/internal/telephony/UUSInfo;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 64
    const-string v0, "persist.lgiqc.ext"

    #@4
    const-string v3, "0"

    #@6
    invoke-static {v0, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    const-string v3, "1"

    #@c
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_2c

    #@12
    move v0, v1

    #@13
    :goto_13
    sput-boolean v0, Lcom/android/internal/telephony/gsm/GsmConnection;->CIQ_EXTENSION:Z

    #@15
    .line 67
    sput v2, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqCount:I

    #@17
    .line 68
    const-string v0, "ro.debuggable"

    #@19
    const-string v3, "0"

    #@1b
    invoke-static {v0, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    const-string v3, "1"

    #@21
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v0

    #@25
    if-eqz v0, :cond_2e

    #@27
    :goto_27
    sput-boolean v1, Lcom/android/internal/telephony/gsm/GsmConnection;->DBG_CIQ:Z

    #@29
    .line 142
    sput v2, Lcom/android/internal/telephony/gsm/GsmConnection;->ciqIDCount:I

    #@2b
    return-void

    #@2c
    :cond_2c
    move v0, v2

    #@2d
    .line 64
    goto :goto_13

    #@2e
    :cond_2e
    move v1, v2

    #@2f
    .line 68
    goto :goto_27
.end method

.method constructor <init>(Landroid/content/Context;Lcom/android/internal/telephony/DriverCall;Lcom/android/internal/telephony/gsm/GsmCallTracker;I)V
    .registers 11
    .parameter "context"
    .parameter "dc"
    .parameter "ct"
    .parameter "index"

    #@0
    .prologue
    const/16 v5, 0x3e8

    #@2
    const/4 v4, 0x0

    #@3
    .line 177
    invoke-direct {p0}, Lcom/android/internal/telephony/Connection;-><init>()V

    #@6
    .line 66
    iput v4, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqId:I

    #@8
    .line 114
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->NOT_DISCONNECTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@a
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@c
    .line 115
    sget-object v2, Lcom/android/internal/telephony/Connection$PostDialState;->NOT_STARTED:Lcom/android/internal/telephony/Connection$PostDialState;

    #@e
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@10
    .line 116
    sget v2, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_ALLOWED:I

    #@12
    iput v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->numberPresentation:I

    #@14
    .line 124
    const-string v2, ""

    #@16
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->dialedAddress:Ljava/lang/String;

    #@18
    .line 143
    iput v4, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->ciqID:I

    #@1a
    .line 144
    iput v4, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->systemCode:I

    #@1c
    .line 145
    const/16 v2, 0x10

    #@1e
    iput-short v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->termCode:S

    #@20
    .line 150
    iput-boolean v4, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->isPause:Z

    #@22
    .line 178
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gsm/GsmConnection;->createWakeLock(Landroid/content/Context;)V

    #@25
    .line 179
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmConnection;->acquireWakeLock()V

    #@28
    .line 181
    iput-object p3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2a
    .line 182
    new-instance v2, Lcom/android/internal/telephony/gsm/GsmConnection$MyHandler;

    #@2c
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2e
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->getLooper()Landroid/os/Looper;

    #@31
    move-result-object v3

    #@32
    invoke-direct {v2, p0, v3}, Lcom/android/internal/telephony/gsm/GsmConnection$MyHandler;-><init>(Lcom/android/internal/telephony/gsm/GsmConnection;Landroid/os/Looper;)V

    #@35
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->h:Landroid/os/Handler;

    #@37
    .line 184
    iget-object v2, p2, Lcom/android/internal/telephony/DriverCall;->number:Ljava/lang/String;

    #@39
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@3b
    .line 186
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@3d
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gsm/GsmConnection;->setRedialNumber(Ljava/lang/String;)V

    #@40
    .line 189
    iget-boolean v2, p2, Lcom/android/internal/telephony/DriverCall;->isMT:Z

    #@42
    iput-boolean v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->isIncoming:Z

    #@44
    .line 190
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@47
    move-result-wide v2

    #@48
    iput-wide v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->createTime:J

    #@4a
    .line 192
    iget-object v2, p2, Lcom/android/internal/telephony/DriverCall;->cdnipNumber:Ljava/lang/String;

    #@4c
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->cdnipNumber:Ljava/lang/String;

    #@4e
    .line 193
    new-instance v2, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v3, "cdnipNumber 2 = "

    #@55
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v2

    #@59
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->cdnipNumber:Ljava/lang/String;

    #@5b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v2

    #@5f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v2

    #@63
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gsm/GsmConnection;->log(Ljava/lang/String;)V

    #@66
    .line 195
    iget-object v2, p2, Lcom/android/internal/telephony/DriverCall;->name:Ljava/lang/String;

    #@68
    iput-object v2, p0, Lcom/android/internal/telephony/Connection;->cnapName:Ljava/lang/String;

    #@6a
    .line 196
    iget v2, p2, Lcom/android/internal/telephony/DriverCall;->namePresentation:I

    #@6c
    iput v2, p0, Lcom/android/internal/telephony/Connection;->cnapNamePresentation:I

    #@6e
    .line 197
    iget v2, p2, Lcom/android/internal/telephony/DriverCall;->numberPresentation:I

    #@70
    iput v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->numberPresentation:I

    #@72
    .line 198
    iget-object v2, p2, Lcom/android/internal/telephony/DriverCall;->uusInfo:Lcom/android/internal/telephony/UUSInfo;

    #@74
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->uusInfo:Lcom/android/internal/telephony/UUSInfo;

    #@76
    .line 200
    iget v2, p2, Lcom/android/internal/telephony/DriverCall;->TOA:I

    #@78
    iput v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->TOA:I

    #@7a
    .line 204
    const/4 v2, 0x0

    #@7b
    const-string v3, "RingBackTone"

    #@7d
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@80
    move-result v2

    #@81
    if-eqz v2, :cond_87

    #@83
    .line 205
    iget v2, p2, Lcom/android/internal/telephony/DriverCall;->signal:I

    #@85
    iput v2, p0, Lcom/android/internal/telephony/Connection;->ringbackToneSignal:I

    #@87
    .line 210
    :cond_87
    iget-object v2, p2, Lcom/android/internal/telephony/DriverCall;->redirectNumber:Ljava/lang/String;

    #@89
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->redirectNumber:Ljava/lang/String;

    #@8b
    .line 213
    iput p4, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->index:I

    #@8d
    .line 215
    iget-object v2, p2, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@8f
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gsm/GsmConnection;->parentFromDCState(Lcom/android/internal/telephony/DriverCall$State;)Lcom/android/internal/telephony/gsm/GsmCall;

    #@92
    move-result-object v2

    #@93
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->parent:Lcom/android/internal/telephony/gsm/GsmCall;

    #@95
    .line 216
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->parent:Lcom/android/internal/telephony/gsm/GsmCall;

    #@97
    invoke-virtual {v2, p0, p2}, Lcom/android/internal/telephony/gsm/GsmCall;->attach(Lcom/android/internal/telephony/Connection;Lcom/android/internal/telephony/DriverCall;)V

    #@9a
    .line 218
    sget-boolean v2, Lcom/android/internal/telephony/gsm/GsmConnection;->CIQ_EXTENSION:Z

    #@9c
    if-eqz v2, :cond_c9

    #@9e
    .line 219
    sget v2, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqCount:I

    #@a0
    iput v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqId:I

    #@a2
    .line 220
    iget v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqId:I

    #@a4
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/GsmConnection;->setCIQIdForTMUS(I)V

    #@a7
    .line 221
    sget-boolean v2, Lcom/android/internal/telephony/gsm/GsmConnection;->DBG_CIQ:Z

    #@a9
    if-eqz v2, :cond_c3

    #@ab
    new-instance v2, Ljava/lang/StringBuilder;

    #@ad
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b0
    const-string v3, "[EXTENSION] GsmConnection Constructor -1- ciqID: "

    #@b2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v2

    #@b6
    iget v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqId:I

    #@b8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v2

    #@bc
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bf
    move-result-object v2

    #@c0
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gsm/GsmConnection;->log(Ljava/lang/String;)V

    #@c3
    .line 223
    :cond_c3
    iget v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqId:I

    #@c5
    if-lt v2, v5, :cond_116

    #@c7
    .line 224
    sput v4, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqCount:I

    #@c9
    .line 231
    :cond_c9
    :goto_c9
    const-string v2, "1"

    #@cb
    const-string v3, "service.iq.active"

    #@cd
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@d0
    move-result-object v3

    #@d1
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d4
    move-result v2

    #@d5
    if-eqz v2, :cond_115

    #@d7
    .line 232
    const/4 v0, 0x0

    #@d8
    .line 233
    .local v0, gs01:Lcom/carrieriq/iqagent/client/metrics/gs/GS01;
    const/4 v1, 0x0

    #@d9
    .line 235
    .local v1, gs02:Lcom/carrieriq/iqagent/client/metrics/gs/GS02;
    sget v2, Lcom/android/internal/telephony/gsm/GsmConnection;->ciqIDCount:I

    #@db
    add-int/lit8 v2, v2, 0x1

    #@dd
    sput v2, Lcom/android/internal/telephony/gsm/GsmConnection;->ciqIDCount:I

    #@df
    iput v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->ciqID:I

    #@e1
    .line 236
    sget v2, Lcom/android/internal/telephony/gsm/GsmConnection;->ciqIDCount:I

    #@e3
    if-lt v2, v5, :cond_e7

    #@e5
    .line 237
    sput v4, Lcom/android/internal/telephony/gsm/GsmConnection;->ciqIDCount:I

    #@e7
    .line 240
    :cond_e7
    invoke-static {}, Lcom/lge/ciq/VoiceCallCIQ;->getGS01Metric()Lcom/carrieriq/iqagent/client/metrics/gs/GS01;

    #@ea
    move-result-object v0

    #@eb
    .line 241
    if-eqz v0, :cond_120

    #@ed
    .line 242
    iget v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->ciqID:I

    #@ef
    iput v2, v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->dwCallId:I

    #@f1
    .line 243
    const/4 v2, 0x1

    #@f2
    iput-byte v2, v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->ucCallState:B

    #@f4
    .line 244
    iget-object v2, p2, Lcom/android/internal/telephony/DriverCall;->number:Ljava/lang/String;

    #@f6
    if-eqz v2, :cond_11d

    #@f8
    iget-object v2, p2, Lcom/android/internal/telephony/DriverCall;->number:Ljava/lang/String;

    #@fa
    :goto_fa
    iput-object v2, v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->szNumber:Ljava/lang/String;

    #@fc
    .line 246
    invoke-virtual {v0}, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->setTerminated()V

    #@ff
    .line 247
    invoke-virtual {v0}, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->setVoice()V

    #@102
    .line 248
    invoke-static {p1, v0}, Lcom/lge/ciq/VoiceCallCIQ;->submitCSCall(Landroid/content/Context;Lcom/carrieriq/iqagent/client/Metric;)V

    #@105
    .line 253
    :goto_105
    invoke-static {}, Lcom/lge/ciq/VoiceCallCIQ;->getGS02Metric()Lcom/carrieriq/iqagent/client/metrics/gs/GS02;

    #@108
    move-result-object v1

    #@109
    .line 254
    if-eqz v1, :cond_128

    #@10b
    .line 255
    iget v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->ciqID:I

    #@10d
    iput v2, v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS02;->dwCallId:I

    #@10f
    .line 256
    const/4 v2, 0x4

    #@110
    iput-byte v2, v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS02;->ucCallState:B

    #@112
    .line 257
    invoke-static {p1, v1}, Lcom/lge/ciq/VoiceCallCIQ;->submitCSCall(Landroid/content/Context;Lcom/carrieriq/iqagent/client/Metric;)V

    #@115
    .line 263
    .end local v0           #gs01:Lcom/carrieriq/iqagent/client/metrics/gs/GS01;
    .end local v1           #gs02:Lcom/carrieriq/iqagent/client/metrics/gs/GS02;
    :cond_115
    :goto_115
    return-void

    #@116
    .line 226
    :cond_116
    sget v2, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqCount:I

    #@118
    add-int/lit8 v2, v2, 0x1

    #@11a
    sput v2, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqCount:I

    #@11c
    goto :goto_c9

    #@11d
    .line 244
    .restart local v0       #gs01:Lcom/carrieriq/iqagent/client/metrics/gs/GS01;
    .restart local v1       #gs02:Lcom/carrieriq/iqagent/client/metrics/gs/GS02;
    :cond_11d
    const-string v2, "Private Number"

    #@11f
    goto :goto_fa

    #@120
    .line 250
    :cond_120
    const-string v2, "LGDDM-CSC"

    #@122
    const-string v3, "CSC Start metric instance is null!"

    #@124
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@127
    goto :goto_105

    #@128
    .line 259
    :cond_128
    const-string v2, "LGDDM-CSC"

    #@12a
    const-string v3, "CSC State Transition metric instance is null!"

    #@12c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@12f
    goto :goto_115
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/android/internal/telephony/gsm/GsmCallTracker;Lcom/android/internal/telephony/gsm/GsmCall;)V
    .registers 12
    .parameter "context"
    .parameter "dialString"
    .parameter "ct"
    .parameter "parent"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/16 v5, 0x3e8

    #@3
    const/4 v4, 0x0

    #@4
    .line 267
    invoke-direct {p0}, Lcom/android/internal/telephony/Connection;-><init>()V

    #@7
    .line 66
    iput v4, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqId:I

    #@9
    .line 114
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->NOT_DISCONNECTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@b
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@d
    .line 115
    sget-object v2, Lcom/android/internal/telephony/Connection$PostDialState;->NOT_STARTED:Lcom/android/internal/telephony/Connection$PostDialState;

    #@f
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@11
    .line 116
    sget v2, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_ALLOWED:I

    #@13
    iput v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->numberPresentation:I

    #@15
    .line 124
    const-string v2, ""

    #@17
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->dialedAddress:Ljava/lang/String;

    #@19
    .line 143
    iput v4, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->ciqID:I

    #@1b
    .line 144
    iput v4, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->systemCode:I

    #@1d
    .line 145
    const/16 v2, 0x10

    #@1f
    iput-short v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->termCode:S

    #@21
    .line 150
    iput-boolean v4, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->isPause:Z

    #@23
    .line 268
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gsm/GsmConnection;->createWakeLock(Landroid/content/Context;)V

    #@26
    .line 269
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmConnection;->acquireWakeLock()V

    #@29
    .line 271
    iput-object p3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2b
    .line 272
    new-instance v2, Lcom/android/internal/telephony/gsm/GsmConnection$MyHandler;

    #@2d
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2f
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->getLooper()Landroid/os/Looper;

    #@32
    move-result-object v3

    #@33
    invoke-direct {v2, p0, v3}, Lcom/android/internal/telephony/gsm/GsmConnection$MyHandler;-><init>(Lcom/android/internal/telephony/gsm/GsmConnection;Landroid/os/Looper;)V

    #@36
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->h:Landroid/os/Handler;

    #@38
    .line 274
    iput-object p2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->dialString:Ljava/lang/String;

    #@3a
    .line 276
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/gsm/GsmConnection;->setRedialNumber(Ljava/lang/String;)V

    #@3d
    .line 279
    invoke-static {p2}, Landroid/telephony/PhoneNumberUtils;->extractNetworkPortionAlt(Ljava/lang/String;)Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@43
    .line 280
    invoke-static {p2}, Landroid/telephony/PhoneNumberUtils;->extractPostDialPortion(Ljava/lang/String;)Ljava/lang/String;

    #@46
    move-result-object v2

    #@47
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->postDialString:Ljava/lang/String;

    #@49
    .line 283
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@4b
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->dialedAddress:Ljava/lang/String;

    #@4d
    .line 287
    iput-object v6, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->cdnipNumber:Ljava/lang/String;

    #@4f
    .line 289
    const/4 v2, -0x1

    #@50
    iput v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->index:I

    #@52
    .line 291
    iput-boolean v4, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->isIncoming:Z

    #@54
    .line 292
    iput-object v6, p0, Lcom/android/internal/telephony/Connection;->cnapName:Ljava/lang/String;

    #@56
    .line 293
    sget v2, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_ALLOWED:I

    #@58
    iput v2, p0, Lcom/android/internal/telephony/Connection;->cnapNamePresentation:I

    #@5a
    .line 294
    sget v2, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_ALLOWED:I

    #@5c
    iput v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->numberPresentation:I

    #@5e
    .line 295
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@61
    move-result-wide v2

    #@62
    iput-wide v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->createTime:J

    #@64
    .line 297
    iput-object p4, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->parent:Lcom/android/internal/telephony/gsm/GsmCall;

    #@66
    .line 298
    sget-object v2, Lcom/android/internal/telephony/Call$State;->DIALING:Lcom/android/internal/telephony/Call$State;

    #@68
    invoke-virtual {p4, p0, v2}, Lcom/android/internal/telephony/gsm/GsmCall;->attachFake(Lcom/android/internal/telephony/Connection;Lcom/android/internal/telephony/Call$State;)V

    #@6b
    .line 300
    sget-boolean v2, Lcom/android/internal/telephony/gsm/GsmConnection;->CIQ_EXTENSION:Z

    #@6d
    if-eqz v2, :cond_ae

    #@6f
    .line 301
    sget v2, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqCount:I

    #@71
    iput v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqId:I

    #@73
    .line 302
    iget v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqId:I

    #@75
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/GsmConnection;->setCIQIdForTMUS(I)V

    #@78
    .line 303
    sget-boolean v2, Lcom/android/internal/telephony/gsm/GsmConnection;->DBG_CIQ:Z

    #@7a
    if-eqz v2, :cond_94

    #@7c
    new-instance v2, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const-string v3, "[EXTENSION] GsmConnection Constructor -2- ciqID: "

    #@83
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v2

    #@87
    iget v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqId:I

    #@89
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v2

    #@8d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v2

    #@91
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gsm/GsmConnection;->log(Ljava/lang/String;)V

    #@94
    .line 305
    :cond_94
    sget-boolean v2, Lcom/android/internal/telephony/gsm/GsmConnection;->DBG_CIQ:Z

    #@96
    if-eqz v2, :cond_9d

    #@98
    const-string v2, "[CIQ_EXTENSION] update GsmConnection() DIALING"

    #@9a
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gsm/GsmConnection;->log(Ljava/lang/String;)V

    #@9d
    .line 306
    :cond_9d
    sget-object v2, Lcom/android/internal/telephony/Call$State;->DIALING:Lcom/android/internal/telephony/Call$State;

    #@9f
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->CIQ_State:Lcom/android/internal/telephony/Call$State;

    #@a1
    .line 307
    iget v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqId:I

    #@a3
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->CIQ_State:Lcom/android/internal/telephony/Call$State;

    #@a5
    invoke-static {v2, p2, v3}, Lcom/android/internal/telephony/CallStateBroadcaster;->SendCallStatus(ILjava/lang/String;Lcom/android/internal/telephony/Call$State;)V

    #@a8
    .line 309
    iget v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqId:I

    #@aa
    if-lt v2, v5, :cond_f7

    #@ac
    .line 310
    sput v4, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqCount:I

    #@ae
    .line 317
    :cond_ae
    :goto_ae
    const-string v2, "1"

    #@b0
    const-string v3, "service.iq.active"

    #@b2
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@b5
    move-result-object v3

    #@b6
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b9
    move-result v2

    #@ba
    if-eqz v2, :cond_f6

    #@bc
    .line 318
    const/4 v0, 0x0

    #@bd
    .line 319
    .local v0, gs01:Lcom/carrieriq/iqagent/client/metrics/gs/GS01;
    const/4 v1, 0x0

    #@be
    .line 321
    .local v1, gs02:Lcom/carrieriq/iqagent/client/metrics/gs/GS02;
    sget v2, Lcom/android/internal/telephony/gsm/GsmConnection;->ciqIDCount:I

    #@c0
    add-int/lit8 v2, v2, 0x1

    #@c2
    sput v2, Lcom/android/internal/telephony/gsm/GsmConnection;->ciqIDCount:I

    #@c4
    iput v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->ciqID:I

    #@c6
    .line 322
    sget v2, Lcom/android/internal/telephony/gsm/GsmConnection;->ciqIDCount:I

    #@c8
    if-lt v2, v5, :cond_cc

    #@ca
    .line 323
    sput v4, Lcom/android/internal/telephony/gsm/GsmConnection;->ciqIDCount:I

    #@cc
    .line 326
    :cond_cc
    invoke-static {}, Lcom/lge/ciq/VoiceCallCIQ;->getGS01Metric()Lcom/carrieriq/iqagent/client/metrics/gs/GS01;

    #@cf
    move-result-object v0

    #@d0
    .line 327
    if-eqz v0, :cond_101

    #@d2
    .line 328
    iget v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->ciqID:I

    #@d4
    iput v2, v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->dwCallId:I

    #@d6
    .line 329
    const/4 v2, 0x1

    #@d7
    iput-byte v2, v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->ucCallState:B

    #@d9
    .line 330
    if-eqz p2, :cond_fe

    #@db
    .end local p2
    :goto_db
    iput-object p2, v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->szNumber:Ljava/lang/String;

    #@dd
    .line 332
    invoke-virtual {v0}, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->setOriginated()V

    #@e0
    .line 333
    invoke-virtual {v0}, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->setVoice()V

    #@e3
    .line 334
    invoke-static {p1, v0}, Lcom/lge/ciq/VoiceCallCIQ;->submitCSCall(Landroid/content/Context;Lcom/carrieriq/iqagent/client/Metric;)V

    #@e6
    .line 339
    :goto_e6
    invoke-static {}, Lcom/lge/ciq/VoiceCallCIQ;->getGS02Metric()Lcom/carrieriq/iqagent/client/metrics/gs/GS02;

    #@e9
    move-result-object v1

    #@ea
    .line 340
    if-eqz v1, :cond_109

    #@ec
    .line 341
    iget v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->ciqID:I

    #@ee
    iput v2, v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS02;->dwCallId:I

    #@f0
    .line 342
    const/4 v2, 0x3

    #@f1
    iput-byte v2, v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS02;->ucCallState:B

    #@f3
    .line 343
    invoke-static {p1, v1}, Lcom/lge/ciq/VoiceCallCIQ;->submitCSCall(Landroid/content/Context;Lcom/carrieriq/iqagent/client/Metric;)V

    #@f6
    .line 349
    .end local v0           #gs01:Lcom/carrieriq/iqagent/client/metrics/gs/GS01;
    .end local v1           #gs02:Lcom/carrieriq/iqagent/client/metrics/gs/GS02;
    :cond_f6
    :goto_f6
    return-void

    #@f7
    .line 312
    .restart local p2
    :cond_f7
    sget v2, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqCount:I

    #@f9
    add-int/lit8 v2, v2, 0x1

    #@fb
    sput v2, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqCount:I

    #@fd
    goto :goto_ae

    #@fe
    .line 330
    .restart local v0       #gs01:Lcom/carrieriq/iqagent/client/metrics/gs/GS01;
    .restart local v1       #gs02:Lcom/carrieriq/iqagent/client/metrics/gs/GS02;
    :cond_fe
    const-string p2, "Private Number"

    #@100
    goto :goto_db

    #@101
    .line 336
    :cond_101
    const-string v2, "LGDDM-CSC"

    #@103
    const-string v3, "CSC Start metric instance is null!"

    #@105
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@108
    goto :goto_e6

    #@109
    .line 345
    .end local p2
    :cond_109
    const-string v2, "LGDDM-CSC"

    #@10b
    const-string v3, "CSC State Transition metric instance is null!"

    #@10d
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@110
    goto :goto_f6
.end method

.method static synthetic access$000(Lcom/android/internal/telephony/gsm/GsmConnection;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 60
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmConnection;->processNextPostDialChar()V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/internal/telephony/gsm/GsmConnection;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 60
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmConnection;->releaseWakeLock()V

    #@3
    return-void
.end method

.method private acquireWakeLock()V
    .registers 2

    #@0
    .prologue
    .line 1497
    const-string v0, "acquireWakeLock"

    #@2
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gsm/GsmConnection;->log(Ljava/lang/String;)V

    #@5
    .line 1498
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    #@7
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@a
    .line 1499
    return-void
.end method

.method private createWakeLock(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 1491
    const-string v1, "power"

    #@2
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/os/PowerManager;

    #@8
    .line 1492
    .local v0, pm:Landroid/os/PowerManager;
    const/4 v1, 0x1

    #@9
    const-string v2, "GSM"

    #@b
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@e
    move-result-object v1

    #@f
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    #@11
    .line 1493
    return-void
.end method

.method static equalsHandlesNulls(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 3
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    .line 356
    if-nez p0, :cond_8

    #@2
    if-nez p1, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5

    #@8
    :cond_8
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    goto :goto_5
.end method

.method private isConnectingInOrOut()Z
    .registers 3

    #@0
    .prologue
    .line 1441
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->parent:Lcom/android/internal/telephony/gsm/GsmCall;

    #@2
    if-eqz v0, :cond_1c

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->parent:Lcom/android/internal/telephony/gsm/GsmCall;

    #@6
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@8
    iget-object v1, v1, Lcom/android/internal/telephony/gsm/GsmCallTracker;->ringingCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@a
    if-eq v0, v1, :cond_1c

    #@c
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->parent:Lcom/android/internal/telephony/gsm/GsmCall;

    #@e
    iget-object v0, v0, Lcom/android/internal/telephony/Call;->state:Lcom/android/internal/telephony/Call$State;

    #@10
    sget-object v1, Lcom/android/internal/telephony/Call$State;->DIALING:Lcom/android/internal/telephony/Call$State;

    #@12
    if-eq v0, v1, :cond_1c

    #@14
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->parent:Lcom/android/internal/telephony/gsm/GsmCall;

    #@16
    iget-object v0, v0, Lcom/android/internal/telephony/Call;->state:Lcom/android/internal/telephony/Call$State;

    #@18
    sget-object v1, Lcom/android/internal/telephony/Call$State;->ALERTING:Lcom/android/internal/telephony/Call$State;

    #@1a
    if-ne v0, v1, :cond_1e

    #@1c
    :cond_1c
    const/4 v0, 0x1

    #@1d
    :goto_1d
    return v0

    #@1e
    :cond_1e
    const/4 v0, 0x0

    #@1f
    goto :goto_1d
.end method

.method private log(Ljava/lang/String;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 1512
    const-string v0, "GSM"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[GSMConn] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1513
    return-void
.end method

.method private parentFromDCState(Lcom/android/internal/telephony/DriverCall$State;)Lcom/android/internal/telephony/gsm/GsmCall;
    .registers 5
    .parameter "state"

    #@0
    .prologue
    .line 1448
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmConnection$1;->$SwitchMap$com$android$internal$telephony$DriverCall$State:[I

    #@2
    invoke-virtual {p1}, Lcom/android/internal/telephony/DriverCall$State;->ordinal()I

    #@5
    move-result v1

    #@6
    aget v0, v0, v1

    #@8
    packed-switch v0, :pswitch_data_34

    #@b
    .line 1465
    new-instance v0, Ljava/lang/RuntimeException;

    #@d
    new-instance v1, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v2, "illegal call state: "

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@23
    throw v0

    #@24
    .line 1452
    :pswitch_24
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@26
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@28
    .line 1461
    :goto_28
    return-object v0

    #@29
    .line 1456
    :pswitch_29
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2b
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->backgroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@2d
    goto :goto_28

    #@2e
    .line 1461
    :pswitch_2e
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@30
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->ringingCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@32
    goto :goto_28

    #@33
    .line 1448
    nop

    #@34
    :pswitch_data_34
    .packed-switch 0x1
        :pswitch_24
        :pswitch_24
        :pswitch_24
        :pswitch_29
        :pswitch_2e
        :pswitch_2e
    .end packed-switch
.end method

.method private processNextPostDialChar()V
    .registers 10

    #@0
    .prologue
    .line 1376
    const/4 v1, 0x0

    #@1
    .line 1379
    .local v1, c:C
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@3
    sget-object v7, Lcom/android/internal/telephony/Connection$PostDialState;->CANCELLED:Lcom/android/internal/telephony/Connection$PostDialState;

    #@5
    if-ne v6, v7, :cond_8

    #@7
    .line 1433
    :cond_7
    :goto_7
    return-void

    #@8
    .line 1384
    :cond_8
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->postDialString:Ljava/lang/String;

    #@a
    if-eqz v6, :cond_16

    #@c
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->postDialString:Ljava/lang/String;

    #@e
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@11
    move-result v6

    #@12
    iget v7, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->nextPostDialChar:I

    #@14
    if-gt v6, v7, :cond_41

    #@16
    .line 1386
    :cond_16
    sget-object v6, Lcom/android/internal/telephony/Connection$PostDialState;->COMPLETE:Lcom/android/internal/telephony/Connection$PostDialState;

    #@18
    invoke-direct {p0, v6}, Lcom/android/internal/telephony/gsm/GsmConnection;->setPostDialState(Lcom/android/internal/telephony/Connection$PostDialState;)V

    #@1b
    .line 1389
    const/4 v1, 0x0

    #@1c
    .line 1408
    :cond_1c
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@1e
    iget-object v6, v6, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@20
    iget-object v4, v6, Lcom/android/internal/telephony/gsm/GSMPhone;->mPostDialHandler:Landroid/os/Registrant;

    #@22
    .line 1412
    .local v4, postDialHandler:Landroid/os/Registrant;
    if-eqz v4, :cond_7

    #@24
    invoke-virtual {v4}, Landroid/os/Registrant;->messageForRegistrant()Landroid/os/Message;

    #@27
    move-result-object v3

    #@28
    .local v3, notifyMessage:Landroid/os/Message;
    if-eqz v3, :cond_7

    #@2a
    .line 1415
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@2c
    .line 1417
    .local v5, state:Lcom/android/internal/telephony/Connection$PostDialState;
    sget-object v6, Lcom/android/internal/telephony/Connection$PostDialState;->PAUSE:Lcom/android/internal/telephony/Connection$PostDialState;

    #@2e
    if-ne v5, v6, :cond_81

    #@30
    .line 1418
    const/4 v6, 0x1

    #@31
    iput-boolean v6, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->isPause:Z

    #@33
    .line 1423
    :goto_33
    invoke-static {v3}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@36
    move-result-object v0

    #@37
    .line 1424
    .local v0, ar:Landroid/os/AsyncResult;
    iput-object p0, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@39
    .line 1425
    iput-object v5, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@3b
    .line 1428
    iput v1, v3, Landroid/os/Message;->arg1:I

    #@3d
    .line 1431
    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    #@40
    goto :goto_7

    #@41
    .line 1393
    .end local v0           #ar:Landroid/os/AsyncResult;
    .end local v3           #notifyMessage:Landroid/os/Message;
    .end local v4           #postDialHandler:Landroid/os/Registrant;
    .end local v5           #state:Lcom/android/internal/telephony/Connection$PostDialState;
    :cond_41
    sget-object v6, Lcom/android/internal/telephony/Connection$PostDialState;->STARTED:Lcom/android/internal/telephony/Connection$PostDialState;

    #@43
    invoke-direct {p0, v6}, Lcom/android/internal/telephony/gsm/GsmConnection;->setPostDialState(Lcom/android/internal/telephony/Connection$PostDialState;)V

    #@46
    .line 1395
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->postDialString:Ljava/lang/String;

    #@48
    iget v7, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->nextPostDialChar:I

    #@4a
    add-int/lit8 v8, v7, 0x1

    #@4c
    iput v8, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->nextPostDialChar:I

    #@4e
    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    #@51
    move-result v1

    #@52
    .line 1397
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gsm/GsmConnection;->processPostDialChar(C)Z

    #@55
    move-result v2

    #@56
    .line 1399
    .local v2, isValid:Z
    if-nez v2, :cond_1c

    #@58
    .line 1401
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->h:Landroid/os/Handler;

    #@5a
    const/4 v7, 0x3

    #@5b
    invoke-virtual {v6, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@5e
    move-result-object v6

    #@5f
    invoke-virtual {v6}, Landroid/os/Message;->sendToTarget()V

    #@62
    .line 1403
    const-string v6, "GSM"

    #@64
    new-instance v7, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v8, "processNextPostDialChar: c="

    #@6b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v7

    #@6f
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@72
    move-result-object v7

    #@73
    const-string v8, " isn\'t valid!"

    #@75
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v7

    #@79
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v7

    #@7d
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@80
    goto :goto_7

    #@81
    .line 1420
    .end local v2           #isValid:Z
    .restart local v3       #notifyMessage:Landroid/os/Message;
    .restart local v4       #postDialHandler:Landroid/os/Registrant;
    .restart local v5       #state:Lcom/android/internal/telephony/Connection$PostDialState;
    :cond_81
    const/4 v6, 0x0

    #@82
    iput-boolean v6, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->isPause:Z

    #@84
    goto :goto_33
.end method

.method private processPostDialChar(C)Z
    .registers 7
    .parameter "c"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 1315
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->is12Key(C)Z

    #@4
    move-result v1

    #@5
    if-eqz v1, :cond_15

    #@7
    .line 1316
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@9
    iget-object v1, v1, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@b
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->h:Landroid/os/Handler;

    #@d
    invoke-virtual {v2, v0}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@10
    move-result-object v2

    #@11
    invoke-interface {v1, p1, v2}, Lcom/android/internal/telephony/CommandsInterface;->sendDtmf(CLandroid/os/Message;)V

    #@14
    .line 1343
    :goto_14
    return v0

    #@15
    .line 1317
    :cond_15
    const/16 v1, 0x2c

    #@17
    if-ne p1, v1, :cond_2d

    #@19
    .line 1331
    sget-object v1, Lcom/android/internal/telephony/Connection$PostDialState;->PAUSE:Lcom/android/internal/telephony/Connection$PostDialState;

    #@1b
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gsm/GsmConnection;->setPostDialState(Lcom/android/internal/telephony/Connection$PostDialState;)V

    #@1e
    .line 1333
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->h:Landroid/os/Handler;

    #@20
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->h:Landroid/os/Handler;

    #@22
    const/4 v3, 0x2

    #@23
    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@26
    move-result-object v2

    #@27
    const-wide/16 v3, 0xbb8

    #@29
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@2c
    goto :goto_14

    #@2d
    .line 1335
    :cond_2d
    const/16 v1, 0x3b

    #@2f
    if-ne p1, v1, :cond_37

    #@31
    .line 1336
    sget-object v1, Lcom/android/internal/telephony/Connection$PostDialState;->WAIT:Lcom/android/internal/telephony/Connection$PostDialState;

    #@33
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gsm/GsmConnection;->setPostDialState(Lcom/android/internal/telephony/Connection$PostDialState;)V

    #@36
    goto :goto_14

    #@37
    .line 1337
    :cond_37
    const/16 v1, 0x4e

    #@39
    if-ne p1, v1, :cond_41

    #@3b
    .line 1338
    sget-object v1, Lcom/android/internal/telephony/Connection$PostDialState;->WILD:Lcom/android/internal/telephony/Connection$PostDialState;

    #@3d
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gsm/GsmConnection;->setPostDialState(Lcom/android/internal/telephony/Connection$PostDialState;)V

    #@40
    goto :goto_14

    #@41
    .line 1340
    :cond_41
    const/4 v0, 0x0

    #@42
    goto :goto_14
.end method

.method private releaseWakeLock()V
    .registers 3

    #@0
    .prologue
    .line 1503
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    monitor-enter v1

    #@3
    .line 1504
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    #@5
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_15

    #@b
    .line 1505
    const-string v0, "releaseWakeLock"

    #@d
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gsm/GsmConnection;->log(Ljava/lang/String;)V

    #@10
    .line 1506
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    #@12
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@15
    .line 1508
    :cond_15
    monitor-exit v1

    #@16
    .line 1509
    return-void

    #@17
    .line 1508
    :catchall_17
    move-exception v0

    #@18
    monitor-exit v1
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_17

    #@19
    throw v0
.end method

.method private setPostDialState(Lcom/android/internal/telephony/Connection$PostDialState;)V
    .registers 6
    .parameter "s"

    #@0
    .prologue
    const/4 v3, 0x4

    #@1
    .line 1476
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@3
    sget-object v2, Lcom/android/internal/telephony/Connection$PostDialState;->STARTED:Lcom/android/internal/telephony/Connection$PostDialState;

    #@5
    if-eq v1, v2, :cond_1f

    #@7
    sget-object v1, Lcom/android/internal/telephony/Connection$PostDialState;->STARTED:Lcom/android/internal/telephony/Connection$PostDialState;

    #@9
    if-ne p1, v1, :cond_1f

    #@b
    .line 1478
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmConnection;->acquireWakeLock()V

    #@e
    .line 1479
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->h:Landroid/os/Handler;

    #@10
    invoke-virtual {v1, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@13
    move-result-object v0

    #@14
    .line 1480
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->h:Landroid/os/Handler;

    #@16
    const-wide/32 v2, 0xea60

    #@19
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@1c
    .line 1486
    .end local v0           #msg:Landroid/os/Message;
    :cond_1c
    :goto_1c
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@1e
    .line 1487
    return-void

    #@1f
    .line 1481
    :cond_1f
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@21
    sget-object v2, Lcom/android/internal/telephony/Connection$PostDialState;->STARTED:Lcom/android/internal/telephony/Connection$PostDialState;

    #@23
    if-ne v1, v2, :cond_1c

    #@25
    sget-object v1, Lcom/android/internal/telephony/Connection$PostDialState;->STARTED:Lcom/android/internal/telephony/Connection$PostDialState;

    #@27
    if-eq p1, v1, :cond_1c

    #@29
    .line 1483
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->h:Landroid/os/Handler;

    #@2b
    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    #@2e
    .line 1484
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmConnection;->releaseWakeLock()V

    #@31
    goto :goto_1c
.end method

.method private setRedialNumber(Ljava/lang/String;)V
    .registers 2
    .parameter "redialNumber"

    #@0
    .prologue
    .line 379
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->redialNumber:Ljava/lang/String;

    #@2
    .line 380
    return-void
.end method


# virtual methods
.method additionalDisconnectCauseForKR(I)Lcom/android/internal/telephony/Connection$DisconnectCause;
    .registers 3
    .parameter "causeCode"

    #@0
    .prologue
    .line 795
    packed-switch p1, :pswitch_data_3c

    #@3
    .line 850
    :pswitch_3
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->NOT_DISCONNECTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@5
    :goto_5
    return-object v0

    #@6
    .line 797
    :pswitch_6
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_SIM:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@8
    goto :goto_5

    #@9
    .line 800
    :pswitch_9
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->WRONG_STATE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@b
    goto :goto_5

    #@c
    .line 803
    :pswitch_c
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_CLASS_BLOCKED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@e
    goto :goto_5

    #@f
    .line 806
    :pswitch_f
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->NO_RESOURCES:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@11
    goto :goto_5

    #@12
    .line 809
    :pswitch_12
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_USER_DATA:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@14
    goto :goto_5

    #@15
    .line 812
    :pswitch_15
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->TIMER_T3230_EXPIRED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@17
    goto :goto_5

    #@18
    .line 815
    :pswitch_18
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->NO_CELL_AVAILABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@1a
    goto :goto_5

    #@1b
    .line 818
    :pswitch_1b
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->TIMER_T303_EXPIRED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@1d
    goto :goto_5

    #@1e
    .line 821
    :pswitch_1e
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->CNM_MM_REL_PENDING:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@20
    goto :goto_5

    #@21
    .line 824
    :pswitch_21
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_STRATUM_REJ_RR_REL_IND:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@23
    goto :goto_5

    #@24
    .line 827
    :pswitch_24
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_STRATUM_REJ_RR_RANDOM_ACCESS_FAILURE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@26
    goto :goto_5

    #@27
    .line 830
    :pswitch_27
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_STRATUM_REJ_RRC_REL_IND:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@29
    goto :goto_5

    #@2a
    .line 833
    :pswitch_2a
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_STRATUM_REJ_RRC_CLOSE_SESSION_IND:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2c
    goto :goto_5

    #@2d
    .line 836
    :pswitch_2d
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_STRATUM_REJ_RRC_OPEN_SESSION_FAILURE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2f
    goto :goto_5

    #@30
    .line 839
    :pswitch_30
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_STRATUM_REJ_LOW_LEVEL_FAIL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@32
    goto :goto_5

    #@33
    .line 842
    :pswitch_33
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_STRATUM_REJ_LOW_LEVEL_FAIL_REDIAL_NOT_ALLOWED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@35
    goto :goto_5

    #@36
    .line 845
    :pswitch_36
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_STRATUM_REJ_LOW_LEVEL_IMMED_RETRY:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@38
    goto :goto_5

    #@39
    .line 848
    :pswitch_39
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_STRATUM_REJ_ABORT_RADIO_UNAVAILABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3b
    goto :goto_5

    #@3c
    .line 795
    :pswitch_data_3c
    .packed-switch 0x834
        :pswitch_6
        :pswitch_9
        :pswitch_c
        :pswitch_f
        :pswitch_12
        :pswitch_15
        :pswitch_18
        :pswitch_3
        :pswitch_3
        :pswitch_1b
        :pswitch_1e
        :pswitch_21
        :pswitch_24
        :pswitch_27
        :pswitch_2a
        :pswitch_2d
        :pswitch_30
        :pswitch_33
        :pswitch_36
        :pswitch_39
    .end packed-switch
.end method

.method public cancelPostDial()V
    .registers 2

    #@0
    .prologue
    .line 545
    sget-object v0, Lcom/android/internal/telephony/Connection$PostDialState;->CANCELLED:Lcom/android/internal/telephony/Connection$PostDialState;

    #@2
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gsm/GsmConnection;->setPostDialState(Lcom/android/internal/telephony/Connection$PostDialState;)V

    #@5
    .line 546
    return-void
.end method

.method compareTo(Lcom/android/internal/telephony/DriverCall;)Z
    .registers 6
    .parameter "c"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 366
    iget-boolean v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->isIncoming:Z

    #@3
    if-nez v2, :cond_a

    #@5
    iget-boolean v2, p1, Lcom/android/internal/telephony/DriverCall;->isMT:Z

    #@7
    if-nez v2, :cond_a

    #@9
    .line 372
    :cond_9
    :goto_9
    return v1

    #@a
    .line 371
    :cond_a
    iget-object v2, p1, Lcom/android/internal/telephony/DriverCall;->number:Ljava/lang/String;

    #@c
    iget v3, p1, Lcom/android/internal/telephony/DriverCall;->TOA:I

    #@e
    invoke-static {v2, v3}, Landroid/telephony/PhoneNumberUtils;->stringFromStringAndTOA(Ljava/lang/String;I)Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    .line 372
    .local v0, cAddress:Ljava/lang/String;
    iget-boolean v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->isIncoming:Z

    #@14
    iget-boolean v3, p1, Lcom/android/internal/telephony/DriverCall;->isMT:Z

    #@16
    if-ne v2, v3, :cond_20

    #@18
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@1a
    invoke-static {v2, v0}, Lcom/android/internal/telephony/gsm/GsmConnection;->equalsHandlesNulls(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@1d
    move-result v2

    #@1e
    if-nez v2, :cond_9

    #@20
    :cond_20
    const/4 v1, 0x0

    #@21
    goto :goto_9
.end method

.method disconnectCauseFromCode(I)Lcom/android/internal/telephony/Connection$DisconnectCause;
    .registers 9
    .parameter "causeCode"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 577
    const-string v5, "KR"

    #@3
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@6
    move-result v5

    #@7
    if-eqz v5, :cond_12

    #@9
    .line 578
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/gsm/GsmConnection;->additionalDisconnectCauseForKR(I)Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@c
    move-result-object v1

    #@d
    .line 579
    .local v1, disConnectCause:Lcom/android/internal/telephony/Connection$DisconnectCause;
    sget-object v5, Lcom/android/internal/telephony/Connection$DisconnectCause;->NOT_DISCONNECTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@f
    if-eq v1, v5, :cond_12

    #@11
    .line 788
    .end local v1           #disConnectCause:Lcom/android/internal/telephony/Connection$DisconnectCause;
    :goto_11
    return-object v1

    #@12
    .line 586
    :cond_12
    sparse-switch p1, :sswitch_data_14a

    #@15
    .line 759
    :cond_15
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@17
    iget-object v2, v5, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@19
    .line 760
    .local v2, phone:Lcom/android/internal/telephony/gsm/GSMPhone;
    invoke-virtual {v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->getServiceState()Landroid/telephony/ServiceState;

    #@1c
    move-result-object v5

    #@1d
    invoke-virtual {v5}, Landroid/telephony/ServiceState;->getState()I

    #@20
    move-result v3

    #@21
    .line 761
    .local v3, serviceState:I
    invoke-static {}, Lcom/android/internal/telephony/uicc/UiccController;->getInstance()Lcom/android/internal/telephony/uicc/UiccController;

    #@24
    move-result-object v5

    #@25
    invoke-virtual {v5, v6}, Lcom/android/internal/telephony/uicc/UiccController;->getUiccCardApplication(I)Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@28
    move-result-object v0

    #@29
    .line 764
    .local v0, cardApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    if-eqz v0, :cond_f6

    #@2b
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getState()Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@2e
    move-result-object v4

    #@2f
    .line 766
    .local v4, uiccAppState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;
    :goto_2f
    const/4 v5, 0x3

    #@30
    if-ne v3, v5, :cond_fa

    #@32
    .line 767
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->POWER_OFF:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@34
    goto :goto_11

    #@35
    .line 589
    .end local v0           #cardApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    .end local v2           #phone:Lcom/android/internal/telephony/gsm/GSMPhone;
    .end local v3           #serviceState:I
    .end local v4           #uiccAppState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;
    :sswitch_35
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->NO_ROUTE_TO_DEST:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@37
    goto :goto_11

    #@38
    .line 592
    :sswitch_38
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->CHANNEL_UNACCEPTABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3a
    goto :goto_11

    #@3b
    .line 595
    :sswitch_3b
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->OPERATOR_DETERMINED_BARRING:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3d
    goto :goto_11

    #@3e
    .line 598
    :sswitch_3e
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->BUSY:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@40
    goto :goto_11

    #@41
    .line 601
    :sswitch_41
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->NO_USER_RESPONDING:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@43
    goto :goto_11

    #@44
    .line 604
    :sswitch_44
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->NO_ANSWER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@46
    goto :goto_11

    #@47
    .line 607
    :sswitch_47
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->CALL_REJECTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@49
    goto :goto_11

    #@4a
    .line 610
    :sswitch_4a
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->NUMBER_CHANGED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4c
    goto :goto_11

    #@4d
    .line 613
    :sswitch_4d
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->PREEMPTION:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4f
    goto :goto_11

    #@50
    .line 616
    :sswitch_50
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->NON_SELECTED_USER_CLEAR:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@52
    goto :goto_11

    #@53
    .line 619
    :sswitch_53
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->DEST_OUT_OF_ORDER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@55
    goto :goto_11

    #@56
    .line 622
    :sswitch_56
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_NUMBER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@58
    goto :goto_11

    #@59
    .line 625
    :sswitch_59
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->FACILITY_REJECTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@5b
    goto :goto_11

    #@5c
    .line 628
    :sswitch_5c
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->RESPONSE_TO_STA_ENQ:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@5e
    goto :goto_11

    #@5f
    .line 631
    :sswitch_5f
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->CONGESTION:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@61
    goto :goto_11

    #@62
    .line 634
    :sswitch_62
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->NETWORK_OUT_OF_ORDER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@64
    goto :goto_11

    #@65
    .line 637
    :sswitch_65
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->TEMPORARY_FAIL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@67
    goto :goto_11

    #@68
    .line 640
    :sswitch_68
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->SWITCHING_EQUIP_CONGESTION:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@6a
    goto :goto_11

    #@6b
    .line 643
    :sswitch_6b
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_INFO_DISCARD:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@6d
    goto :goto_11

    #@6e
    .line 646
    :sswitch_6e
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->REQUESTED_CIRCUIT_NOT_AVAILABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@70
    goto :goto_11

    #@71
    .line 649
    :sswitch_71
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->RESOURCE_UNAVAILABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@73
    goto :goto_11

    #@74
    .line 652
    :sswitch_74
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->QOS_UNAVAILABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@76
    goto :goto_11

    #@77
    .line 655
    :sswitch_77
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->REQUESTED_FACILITY_NOT_SUBSCRIBED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@79
    goto :goto_11

    #@7a
    .line 658
    :sswitch_7a
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->INCOMING_CALLS_BARRED_CUG:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@7c
    goto :goto_11

    #@7d
    .line 661
    :sswitch_7d
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->BEARER_CAPABILITY_NOT_AUTHORIZED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@7f
    goto :goto_11

    #@80
    .line 664
    :sswitch_80
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->BEARER_CAPABILITY_NOT_PRESENTLY_AVAILABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@82
    goto :goto_11

    #@83
    .line 667
    :sswitch_83
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->SERVICE_NOT_AVAILABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@85
    goto :goto_11

    #@86
    .line 670
    :sswitch_86
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->BEARER_SERVICE_NOT_IMPLEMENTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@88
    goto :goto_11

    #@89
    .line 673
    :sswitch_89
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->LIMIT_EXCEEDED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@8b
    goto :goto_11

    #@8c
    .line 676
    :sswitch_8c
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->REQUESTED_FACILITY_NOT_IMPLEMENTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@8e
    goto :goto_11

    #@8f
    .line 679
    :sswitch_8f
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->ONLY_RESTRICTED_DIGIT_INFO_BEARER_CAPABILITY:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@91
    goto :goto_11

    #@92
    .line 682
    :sswitch_92
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->SERVICE_NOT_IMPLEMENTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@94
    goto/16 :goto_11

    #@96
    .line 685
    :sswitch_96
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_TRANSACTION_ID_VALUE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@98
    goto/16 :goto_11

    #@9a
    .line 688
    :sswitch_9a
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->USER_NOT_MEMBER_CUG:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@9c
    goto/16 :goto_11

    #@9e
    .line 691
    :sswitch_9e
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->INCOMPATIBLE_DESTINATION:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@a0
    goto/16 :goto_11

    #@a2
    .line 694
    :sswitch_a2
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_TRANSIT_NETWORK_SELECTION:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@a4
    goto/16 :goto_11

    #@a6
    .line 697
    :sswitch_a6
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->SEMANTICALLY_INCORRECT_MESSAGE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@a8
    goto/16 :goto_11

    #@aa
    .line 700
    :sswitch_aa
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_MANDATORY_INFO:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@ac
    goto/16 :goto_11

    #@ae
    .line 703
    :sswitch_ae
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->MESSAGE_TYPE_NON_COMPATIBLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@b0
    goto/16 :goto_11

    #@b2
    .line 706
    :sswitch_b2
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->MESSAGE_TYPE_NOT_COMPATIBLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@b4
    goto/16 :goto_11

    #@b6
    .line 709
    :sswitch_b6
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->INFORMATION_ELEMENT_NOT_IMPLEMENTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@b8
    goto/16 :goto_11

    #@ba
    .line 712
    :sswitch_ba
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->CONDITIONAL_IE_ERROR:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@bc
    goto/16 :goto_11

    #@be
    .line 715
    :sswitch_be
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->MESSAGE_NOT_COMPATIBLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@c0
    goto/16 :goto_11

    #@c2
    .line 718
    :sswitch_c2
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->RECOVER_ON_TIMER_EXPIRY:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@c4
    goto/16 :goto_11

    #@c6
    .line 721
    :sswitch_c6
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->PROTOCOL_ERROR:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@c8
    goto/16 :goto_11

    #@ca
    .line 724
    :sswitch_ca
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->INTERWORKING:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@cc
    goto/16 :goto_11

    #@ce
    .line 727
    :sswitch_ce
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->CALL_BARRED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@d0
    goto/16 :goto_11

    #@d2
    .line 730
    :sswitch_d2
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->FDN_BLOCKED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@d4
    goto/16 :goto_11

    #@d6
    .line 734
    :sswitch_d6
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->MS_ACCESS_CLASS_BARRED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@d8
    goto/16 :goto_11

    #@da
    .line 738
    :sswitch_da
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->UNOBTAINABLE_NUMBER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@dc
    goto/16 :goto_11

    #@de
    .line 741
    :sswitch_de
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->DIAL_MODIFIED_TO_USSD:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@e0
    goto/16 :goto_11

    #@e2
    .line 744
    :sswitch_e2
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->DIAL_MODIFIED_TO_SS:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@e4
    goto/16 :goto_11

    #@e6
    .line 747
    :sswitch_e6
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->DIAL_MODIFIED_TO_DIAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@e8
    goto/16 :goto_11

    #@ea
    .line 750
    :sswitch_ea
    const-string v5, "TCL"

    #@ec
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@ef
    move-result v5

    #@f0
    if-eqz v5, :cond_15

    #@f2
    .line 751
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->NORMAL_UNSPECIFIED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@f4
    goto/16 :goto_11

    #@f6
    .line 764
    .restart local v0       #cardApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    .restart local v2       #phone:Lcom/android/internal/telephony/gsm/GSMPhone;
    .restart local v3       #serviceState:I
    :cond_f6
    sget-object v4, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;->APPSTATE_UNKNOWN:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@f8
    goto/16 :goto_2f

    #@fa
    .line 768
    .restart local v4       #uiccAppState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;
    :cond_fa
    if-eq v3, v6, :cond_ff

    #@fc
    const/4 v5, 0x2

    #@fd
    if-ne v3, v5, :cond_103

    #@ff
    .line 770
    :cond_ff
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->OUT_OF_SERVICE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@101
    goto/16 :goto_11

    #@103
    .line 771
    :cond_103
    sget-object v5, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;->APPSTATE_READY:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@105
    if-eq v4, v5, :cond_10b

    #@107
    .line 772
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->ICC_ERROR:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@109
    goto/16 :goto_11

    #@10b
    .line 773
    :cond_10b
    const v5, 0xffff

    #@10e
    if-ne p1, v5, :cond_13e

    #@110
    .line 774
    iget-object v5, v2, Lcom/android/internal/telephony/gsm/GSMPhone;->mSST:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@112
    iget-object v5, v5, Lcom/android/internal/telephony/ServiceStateTracker;->mRestrictedState:Lcom/android/internal/telephony/RestrictedState;

    #@114
    invoke-virtual {v5}, Lcom/android/internal/telephony/RestrictedState;->isCsRestricted()Z

    #@117
    move-result v5

    #@118
    if-eqz v5, :cond_11e

    #@11a
    .line 775
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->CS_RESTRICTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@11c
    goto/16 :goto_11

    #@11e
    .line 776
    :cond_11e
    iget-object v5, v2, Lcom/android/internal/telephony/gsm/GSMPhone;->mSST:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@120
    iget-object v5, v5, Lcom/android/internal/telephony/ServiceStateTracker;->mRestrictedState:Lcom/android/internal/telephony/RestrictedState;

    #@122
    invoke-virtual {v5}, Lcom/android/internal/telephony/RestrictedState;->isCsEmergencyRestricted()Z

    #@125
    move-result v5

    #@126
    if-eqz v5, :cond_12c

    #@128
    .line 777
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->CS_RESTRICTED_EMERGENCY:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@12a
    goto/16 :goto_11

    #@12c
    .line 778
    :cond_12c
    iget-object v5, v2, Lcom/android/internal/telephony/gsm/GSMPhone;->mSST:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@12e
    iget-object v5, v5, Lcom/android/internal/telephony/ServiceStateTracker;->mRestrictedState:Lcom/android/internal/telephony/RestrictedState;

    #@130
    invoke-virtual {v5}, Lcom/android/internal/telephony/RestrictedState;->isCsNormalRestricted()Z

    #@133
    move-result v5

    #@134
    if-eqz v5, :cond_13a

    #@136
    .line 779
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->CS_RESTRICTED_NORMAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@138
    goto/16 :goto_11

    #@13a
    .line 781
    :cond_13a
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->ERROR_UNSPECIFIED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@13c
    goto/16 :goto_11

    #@13e
    .line 783
    :cond_13e
    const/16 v5, 0x10

    #@140
    if-ne p1, v5, :cond_146

    #@142
    .line 784
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->NORMAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@144
    goto/16 :goto_11

    #@146
    .line 788
    :cond_146
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->ERROR_UNSPECIFIED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@148
    goto/16 :goto_11

    #@14a
    .line 586
    :sswitch_data_14a
    .sparse-switch
        0x1 -> :sswitch_da
        0x3 -> :sswitch_35
        0x6 -> :sswitch_38
        0x8 -> :sswitch_3b
        0x11 -> :sswitch_3e
        0x12 -> :sswitch_41
        0x13 -> :sswitch_44
        0x15 -> :sswitch_47
        0x16 -> :sswitch_4a
        0x19 -> :sswitch_4d
        0x1a -> :sswitch_50
        0x1b -> :sswitch_53
        0x1c -> :sswitch_56
        0x1d -> :sswitch_59
        0x1e -> :sswitch_5c
        0x1f -> :sswitch_ea
        0x22 -> :sswitch_5f
        0x26 -> :sswitch_62
        0x29 -> :sswitch_65
        0x2a -> :sswitch_68
        0x2b -> :sswitch_6b
        0x2c -> :sswitch_6e
        0x2f -> :sswitch_71
        0x31 -> :sswitch_74
        0x32 -> :sswitch_77
        0x37 -> :sswitch_7a
        0x39 -> :sswitch_7d
        0x3a -> :sswitch_80
        0x3f -> :sswitch_83
        0x41 -> :sswitch_86
        0x44 -> :sswitch_89
        0x45 -> :sswitch_8c
        0x46 -> :sswitch_8f
        0x4f -> :sswitch_92
        0x51 -> :sswitch_96
        0x57 -> :sswitch_9a
        0x58 -> :sswitch_9e
        0x5b -> :sswitch_a2
        0x5f -> :sswitch_a6
        0x60 -> :sswitch_aa
        0x61 -> :sswitch_ae
        0x62 -> :sswitch_b2
        0x63 -> :sswitch_b6
        0x64 -> :sswitch_ba
        0x65 -> :sswitch_be
        0x66 -> :sswitch_c2
        0x6f -> :sswitch_c6
        0x7f -> :sswitch_ca
        0xf0 -> :sswitch_ce
        0xf1 -> :sswitch_d2
        0xf4 -> :sswitch_de
        0xf5 -> :sswitch_e2
        0xf6 -> :sswitch_e6
        0x121 -> :sswitch_d6
    .end sparse-switch
.end method

.method public dispose()V
    .registers 1

    #@0
    .prologue
    .line 352
    return-void
.end method

.method fakeHoldBeforeDial()V
    .registers 5

    #@0
    .prologue
    .line 1194
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->parent:Lcom/android/internal/telephony/gsm/GsmCall;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 1195
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->parent:Lcom/android/internal/telephony/gsm/GsmCall;

    #@6
    invoke-virtual {v1, p0}, Lcom/android/internal/telephony/gsm/GsmCall;->detach(Lcom/android/internal/telephony/gsm/GsmConnection;)V

    #@9
    .line 1198
    :cond_9
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@b
    iget-object v1, v1, Lcom/android/internal/telephony/gsm/GsmCallTracker;->backgroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@d
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->parent:Lcom/android/internal/telephony/gsm/GsmCall;

    #@f
    .line 1199
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->parent:Lcom/android/internal/telephony/gsm/GsmCall;

    #@11
    sget-object v2, Lcom/android/internal/telephony/Call$State;->HOLDING:Lcom/android/internal/telephony/Call$State;

    #@13
    invoke-virtual {v1, p0, v2}, Lcom/android/internal/telephony/gsm/GsmCall;->attachFake(Lcom/android/internal/telephony/Connection;Lcom/android/internal/telephony/Call$State;)V

    #@16
    .line 1201
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmConnection;->onStartedHolding()V

    #@19
    .line 1203
    sget-boolean v1, Lcom/android/internal/telephony/gsm/GsmConnection;->CIQ_EXTENSION:Z

    #@1b
    if-eqz v1, :cond_2f

    #@1d
    .line 1204
    sget-boolean v1, Lcom/android/internal/telephony/gsm/GsmConnection;->DBG_CIQ:Z

    #@1f
    if-eqz v1, :cond_26

    #@21
    const-string v1, "[CIQ EXTENSION] fakeHoldBeforeDial : Call.State.HOLDING"

    #@23
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gsm/GsmConnection;->log(Ljava/lang/String;)V

    #@26
    .line 1205
    :cond_26
    iget v1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqId:I

    #@28
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@2a
    sget-object v3, Lcom/android/internal/telephony/Call$State;->HOLDING:Lcom/android/internal/telephony/Call$State;

    #@2c
    invoke-static {v1, v2, v3}, Lcom/android/internal/telephony/CallStateBroadcaster;->SendCallStatus(ILjava/lang/String;Lcom/android/internal/telephony/Call$State;)V

    #@2f
    .line 1210
    :cond_2f
    const-string v1, "1"

    #@31
    const-string v2, "service.iq.active"

    #@33
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3a
    move-result v1

    #@3b
    if-eqz v1, :cond_57

    #@3d
    .line 1211
    const/4 v0, 0x0

    #@3e
    .line 1213
    .local v0, gs02:Lcom/carrieriq/iqagent/client/metrics/gs/GS02;
    invoke-static {}, Lcom/lge/ciq/VoiceCallCIQ;->getGS02Metric()Lcom/carrieriq/iqagent/client/metrics/gs/GS02;

    #@41
    move-result-object v0

    #@42
    .line 1214
    if-eqz v0, :cond_58

    #@44
    .line 1215
    iget v1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->ciqID:I

    #@46
    iput v1, v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS02;->dwCallId:I

    #@48
    .line 1216
    const/16 v1, 0x8

    #@4a
    iput-byte v1, v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS02;->ucCallState:B

    #@4c
    .line 1217
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@4e
    iget-object v1, v1, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@50
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@53
    move-result-object v1

    #@54
    invoke-static {v1, v0}, Lcom/lge/ciq/VoiceCallCIQ;->submitCSCall(Landroid/content/Context;Lcom/carrieriq/iqagent/client/Metric;)V

    #@57
    .line 1223
    .end local v0           #gs02:Lcom/carrieriq/iqagent/client/metrics/gs/GS02;
    :cond_57
    :goto_57
    return-void

    #@58
    .line 1219
    .restart local v0       #gs02:Lcom/carrieriq/iqagent/client/metrics/gs/GS02;
    :cond_58
    const-string v1, "LGDDM-CSC"

    #@5a
    const-string v2, "CSC State Transition metric instance is null!"

    #@5c
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    goto :goto_57
.end method

.method protected finalize()V
    .registers 3

    #@0
    .prologue
    .line 1368
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_f

    #@8
    .line 1369
    const-string v0, "GSM"

    #@a
    const-string v1, "[GSMConn] UNEXPECTED; mPartialWakeLock is held when finalizing."

    #@c
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 1371
    :cond_f
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmConnection;->releaseWakeLock()V

    #@12
    .line 1372
    return-void
.end method

.method public getAddress()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 389
    const-string v0, "CA"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_28

    #@8
    const-string v0, "TLS"

    #@a
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_18

    #@10
    const-string v0, "RGS"

    #@12
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_28

    #@18
    .line 390
    :cond_18
    const-string v0, ""

    #@1a
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->dialedAddress:Ljava/lang/String;

    #@1c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v0

    #@20
    if-eqz v0, :cond_25

    #@22
    .line 391
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@24
    .line 397
    :goto_24
    return-object v0

    #@25
    .line 393
    :cond_25
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->dialedAddress:Ljava/lang/String;

    #@27
    goto :goto_24

    #@28
    .line 397
    :cond_28
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@2a
    goto :goto_24
.end method

.method public getBeforeFowardingNumber()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1528
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->redirectNumber:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method getCIQIdForTMUS()I
    .registers 2

    #@0
    .prologue
    .line 1182
    iget v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqId:I

    #@2
    return v0
.end method

.method public bridge synthetic getCall()Lcom/android/internal/telephony/Call;
    .registers 2

    #@0
    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmConnection;->getCall()Lcom/android/internal/telephony/gsm/GsmCall;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getCall()Lcom/android/internal/telephony/gsm/GsmCall;
    .registers 2

    #@0
    .prologue
    .line 401
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->parent:Lcom/android/internal/telephony/gsm/GsmCall;

    #@2
    return-object v0
.end method

.method public getCdnipNumber()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 453
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->cdnipNumber:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getConnectTime()J
    .registers 3

    #@0
    .prologue
    .line 409
    iget-wide v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->connectTime:J

    #@2
    return-wide v0
.end method

.method public getCreateTime()J
    .registers 3

    #@0
    .prologue
    .line 405
    iget-wide v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->createTime:J

    #@2
    return-wide v0
.end method

.method public getDisconnectCause()Lcom/android/internal/telephony/Connection$DisconnectCause;
    .registers 2

    #@0
    .prologue
    .line 436
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2
    return-object v0
.end method

.method public getDisconnectTime()J
    .registers 3

    #@0
    .prologue
    .line 413
    iget-wide v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->disconnectTime:J

    #@2
    return-wide v0
.end method

.method public getDurationMillis()J
    .registers 5

    #@0
    .prologue
    const-wide/16 v0, 0x0

    #@2
    .line 417
    iget-wide v2, p0, Lcom/android/internal/telephony/Connection;->connectTimeReal:J

    #@4
    cmp-long v2, v2, v0

    #@6
    if-nez v2, :cond_9

    #@8
    .line 422
    :goto_8
    return-wide v0

    #@9
    .line 419
    :cond_9
    iget-wide v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->duration:J

    #@b
    cmp-long v0, v2, v0

    #@d
    if-nez v0, :cond_17

    #@f
    .line 420
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@12
    move-result-wide v0

    #@13
    iget-wide v2, p0, Lcom/android/internal/telephony/Connection;->connectTimeReal:J

    #@15
    sub-long/2addr v0, v2

    #@16
    goto :goto_8

    #@17
    .line 422
    :cond_17
    iget-wide v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->duration:J

    #@19
    goto :goto_8
.end method

.method getGSMIndex()I
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1227
    iget v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->index:I

    #@2
    if-ltz v0, :cond_9

    #@4
    .line 1228
    iget v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->index:I

    #@6
    add-int/lit8 v0, v0, 0x1

    #@8
    return v0

    #@9
    .line 1230
    :cond_9
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@b
    const-string v1, "GSM index not yet assigned"

    #@d
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0
.end method

.method public getHoldDurationMillis()J
    .registers 5

    #@0
    .prologue
    .line 427
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmConnection;->getState()Lcom/android/internal/telephony/Call$State;

    #@3
    move-result-object v0

    #@4
    sget-object v1, Lcom/android/internal/telephony/Call$State;->HOLDING:Lcom/android/internal/telephony/Call$State;

    #@6
    if-eq v0, v1, :cond_b

    #@8
    .line 429
    const-wide/16 v0, 0x0

    #@a
    .line 431
    :goto_a
    return-wide v0

    #@b
    :cond_b
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@e
    move-result-wide v0

    #@f
    iget-wide v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->holdingStartTime:J

    #@11
    sub-long/2addr v0, v2

    #@12
    goto :goto_a
.end method

.method public getNumberPresentation()I
    .registers 2

    #@0
    .prologue
    .line 1517
    iget v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->numberPresentation:I

    #@2
    return v0
.end method

.method public getPostDialState()Lcom/android/internal/telephony/Connection$PostDialState;
    .registers 2

    #@0
    .prologue
    .line 482
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@2
    return-object v0
.end method

.method public getRedialNumber()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 383
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->redialNumber:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getRemainingPostDialString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 1348
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@2
    sget-object v1, Lcom/android/internal/telephony/Connection$PostDialState;->CANCELLED:Lcom/android/internal/telephony/Connection$PostDialState;

    #@4
    if-eq v0, v1, :cond_1a

    #@6
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@8
    sget-object v1, Lcom/android/internal/telephony/Connection$PostDialState;->COMPLETE:Lcom/android/internal/telephony/Connection$PostDialState;

    #@a
    if-eq v0, v1, :cond_1a

    #@c
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->postDialString:Ljava/lang/String;

    #@e
    if-eqz v0, :cond_1a

    #@10
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->postDialString:Ljava/lang/String;

    #@12
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@15
    move-result v0

    #@16
    iget v1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->nextPostDialChar:I

    #@18
    if-gt v0, v1, :cond_1d

    #@1a
    .line 1353
    :cond_1a
    const-string v0, ""

    #@1c
    .line 1356
    :goto_1c
    return-object v0

    #@1d
    :cond_1d
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->postDialString:Ljava/lang/String;

    #@1f
    iget v1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->nextPostDialChar:I

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    goto :goto_1c
.end method

.method public getState()Lcom/android/internal/telephony/Call$State;
    .registers 2

    #@0
    .prologue
    .line 458
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->disconnected:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 459
    sget-object v0, Lcom/android/internal/telephony/Call$State;->DISCONNECTED:Lcom/android/internal/telephony/Call$State;

    #@6
    .line 461
    :goto_6
    return-object v0

    #@7
    :cond_7
    invoke-super {p0}, Lcom/android/internal/telephony/Connection;->getState()Lcom/android/internal/telephony/Call$State;

    #@a
    move-result-object v0

    #@b
    goto :goto_6
.end method

.method public getToa()I
    .registers 2

    #@0
    .prologue
    .line 445
    iget v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->TOA:I

    #@2
    return v0
.end method

.method public getUUSInfo()Lcom/android/internal/telephony/UUSInfo;
    .registers 2

    #@0
    .prologue
    .line 1522
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->uusInfo:Lcom/android/internal/telephony/UUSInfo;

    #@2
    return-object v0
.end method

.method getcqID()I
    .registers 2

    #@0
    .prologue
    .line 1543
    iget v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->ciqID:I

    #@2
    return v0
.end method

.method public hangup()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 466
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->disconnected:Z

    #@2
    if-nez v0, :cond_a

    #@4
    .line 467
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@6
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangup(Lcom/android/internal/telephony/gsm/GsmConnection;)V

    #@9
    .line 471
    return-void

    #@a
    .line 469
    :cond_a
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@c
    const-string v1, "disconnected"

    #@e
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0
.end method

.method public isIncoming()Z
    .registers 2

    #@0
    .prologue
    .line 440
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->isIncoming:Z

    #@2
    return v0
.end method

.method onConnectedInOrOut()V
    .registers 5

    #@0
    .prologue
    .line 1239
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3
    move-result-wide v0

    #@4
    iput-wide v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->connectTime:J

    #@6
    .line 1240
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@9
    move-result-wide v0

    #@a
    iput-wide v0, p0, Lcom/android/internal/telephony/Connection;->connectTimeReal:J

    #@c
    .line 1241
    const-wide/16 v0, 0x0

    #@e
    iput-wide v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->duration:J

    #@10
    .line 1246
    new-instance v0, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v1, "onConnectedInOrOut: connectTime="

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    iget-wide v1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->connectTime:J

    #@1d
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gsm/GsmConnection;->log(Ljava/lang/String;)V

    #@28
    .line 1249
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->isIncoming:Z

    #@2a
    if-nez v0, :cond_2f

    #@2c
    .line 1251
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmConnection;->processNextPostDialChar()V

    #@2f
    .line 1254
    :cond_2f
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->isPause:Z

    #@31
    if-eqz v0, :cond_42

    #@33
    .line 1255
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->h:Landroid/os/Handler;

    #@35
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->h:Landroid/os/Handler;

    #@37
    const/4 v2, 0x4

    #@38
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@3b
    move-result-object v1

    #@3c
    const-wide/16 v2, 0xbb8

    #@3e
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@41
    .line 1259
    :goto_41
    return-void

    #@42
    .line 1257
    :cond_42
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmConnection;->releaseWakeLock()V

    #@45
    goto :goto_41
.end method

.method onConnectedSRVCC()V
    .registers 7

    #@0
    .prologue
    const-wide/16 v4, 0x0

    #@2
    .line 1266
    const-wide/16 v0, 0x0

    #@4
    .line 1268
    .local v0, LTECallStartTime:J
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@6
    iget-object v2, v2, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@8
    invoke-virtual {v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@f
    move-result-object v2

    #@10
    const-string v3, "LTEStartCallTime"

    #@12
    invoke-static {v2, v3, v4, v5}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    #@15
    move-result-wide v0

    #@16
    .line 1270
    new-instance v2, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v3, "SRVCC onConnectedInOrOut: LTECallStartTime="

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gsm/GsmConnection;->log(Ljava/lang/String;)V

    #@2c
    .line 1272
    cmp-long v2, v0, v4

    #@2e
    if-gtz v2, :cond_70

    #@30
    .line 1274
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@33
    move-result-wide v2

    #@34
    iput-wide v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->connectTime:J

    #@36
    .line 1275
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@39
    move-result-wide v2

    #@3a
    iput-wide v2, p0, Lcom/android/internal/telephony/Connection;->connectTimeReal:J

    #@3c
    .line 1282
    :goto_3c
    iput-wide v4, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->duration:J

    #@3e
    .line 1287
    new-instance v2, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v3, "SRVCC onConnectedInOrOut: connectTimeReal="

    #@45
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    iget-wide v3, p0, Lcom/android/internal/telephony/Connection;->connectTimeReal:J

    #@4b
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v2

    #@4f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v2

    #@53
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gsm/GsmConnection;->log(Ljava/lang/String;)V

    #@56
    .line 1290
    iget-boolean v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->isIncoming:Z

    #@58
    if-nez v2, :cond_5d

    #@5a
    .line 1292
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmConnection;->processNextPostDialChar()V

    #@5d
    .line 1295
    :cond_5d
    iget-boolean v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->isPause:Z

    #@5f
    if-eqz v2, :cond_75

    #@61
    .line 1296
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->h:Landroid/os/Handler;

    #@63
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->h:Landroid/os/Handler;

    #@65
    const/4 v4, 0x4

    #@66
    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@69
    move-result-object v3

    #@6a
    const-wide/16 v4, 0xbb8

    #@6c
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@6f
    .line 1300
    :goto_6f
    return-void

    #@70
    .line 1279
    :cond_70
    iput-wide v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->connectTime:J

    #@72
    .line 1280
    iput-wide v0, p0, Lcom/android/internal/telephony/Connection;->connectTimeReal:J

    #@74
    goto :goto_3c

    #@75
    .line 1298
    :cond_75
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmConnection;->releaseWakeLock()V

    #@78
    goto :goto_6f
.end method

.method onDisconnect(Lcom/android/internal/telephony/Connection$DisconnectCause;)V
    .registers 13
    .parameter "cause"

    #@0
    .prologue
    const/4 v10, 0x7

    #@1
    const/4 v9, 0x6

    #@2
    const/4 v8, 0x1

    #@3
    const/4 v7, -0x1

    #@4
    .line 940
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@6
    .line 942
    iget-boolean v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->disconnected:Z

    #@8
    if-nez v3, :cond_2d

    #@a
    .line 943
    iput v7, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->index:I

    #@c
    .line 945
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@f
    move-result-wide v3

    #@10
    iput-wide v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->disconnectTime:J

    #@12
    .line 946
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@15
    move-result-wide v3

    #@16
    iget-wide v5, p0, Lcom/android/internal/telephony/Connection;->connectTimeReal:J

    #@18
    sub-long/2addr v3, v5

    #@19
    iput-wide v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->duration:J

    #@1b
    .line 947
    iput-boolean v8, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->disconnected:Z

    #@1d
    .line 952
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@1f
    iget-object v3, v3, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@21
    invoke-virtual {v3, p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifyDisconnect(Lcom/android/internal/telephony/Connection;)V

    #@24
    .line 954
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->parent:Lcom/android/internal/telephony/gsm/GsmCall;

    #@26
    if-eqz v3, :cond_2d

    #@28
    .line 955
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->parent:Lcom/android/internal/telephony/gsm/GsmCall;

    #@2a
    invoke-virtual {v3, p0}, Lcom/android/internal/telephony/gsm/GsmCall;->connectionDisconnected(Lcom/android/internal/telephony/gsm/GsmConnection;)V

    #@2d
    .line 958
    :cond_2d
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmConnection;->releaseWakeLock()V

    #@30
    .line 960
    sget-boolean v3, Lcom/android/internal/telephony/gsm/GsmConnection;->CIQ_EXTENSION:Z

    #@32
    if-eqz v3, :cond_50

    #@34
    iget v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqId:I

    #@36
    if-eq v3, v7, :cond_50

    #@38
    .line 961
    sget-object v3, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_NUMBER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3a
    if-ne p1, v3, :cond_bb

    #@3c
    .line 962
    iget v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqId:I

    #@3e
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@40
    const/16 v5, 0x1c

    #@42
    invoke-static {v3, v4, v5}, Lcom/android/internal/telephony/CallStateBroadcaster;->SendCallDisconnected(ILjava/lang/String;I)V

    #@45
    .line 968
    :goto_45
    sget-boolean v3, Lcom/android/internal/telephony/gsm/GsmConnection;->DBG_CIQ:Z

    #@47
    if-eqz v3, :cond_4e

    #@49
    const-string v3, "[CIQ_EXTENSION] onDisconnect() normal call clearing"

    #@4b
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/gsm/GsmConnection;->log(Ljava/lang/String;)V

    #@4e
    .line 969
    :cond_4e
    iput v7, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqId:I

    #@50
    .line 974
    :cond_50
    const-string v3, "1"

    #@52
    const-string v4, "service.iq.active"

    #@54
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@57
    move-result-object v4

    #@58
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5b
    move-result v3

    #@5c
    if-eqz v3, :cond_ba

    #@5e
    .line 975
    const/4 v2, 0x0

    #@5f
    .line 976
    .local v2, state:B
    const/4 v0, 0x0

    #@60
    .line 977
    .local v0, gs02:Lcom/carrieriq/iqagent/client/metrics/gs/GS02;
    const/4 v1, 0x0

    #@61
    .line 997
    .local v1, gs03:Lcom/carrieriq/iqagent/client/metrics/gs/GS03;
    sget-boolean v3, Lcom/android/internal/telephony/RIL;->IsAnswered:Z

    #@63
    if-eqz v3, :cond_dd

    #@65
    sget-object v3, Lcom/android/internal/telephony/Connection$DisconnectCause;->INCOMING_MISSED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@67
    if-ne p1, v3, :cond_dd

    #@69
    .line 998
    invoke-static {}, Lcom/lge/ciq/VoiceCallCIQ;->getGS02Metric()Lcom/carrieriq/iqagent/client/metrics/gs/GS02;

    #@6c
    move-result-object v0

    #@6d
    .line 999
    const-string v3, "LGDDM-CSC"

    #@6f
    new-instance v4, Ljava/lang/StringBuilder;

    #@71
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@74
    const-string v5, "onDisconnect: termCode="

    #@76
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v4

    #@7a
    iget-short v5, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->termCode:S

    #@7c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v4

    #@80
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v4

    #@84
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@87
    .line 1000
    if-eqz v0, :cond_d5

    #@89
    .line 1001
    iget v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->ciqID:I

    #@8b
    iput v3, v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS02;->dwCallId:I

    #@8d
    .line 1002
    iput-byte v9, v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS02;->ucCallState:B

    #@8f
    .line 1003
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@91
    iget-object v3, v3, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@93
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@96
    move-result-object v3

    #@97
    invoke-static {v3, v0}, Lcom/lge/ciq/VoiceCallCIQ;->submitCSCall(Landroid/content/Context;Lcom/carrieriq/iqagent/client/Metric;)V

    #@9a
    .line 1007
    :goto_9a
    const/4 v3, 0x0

    #@9b
    sput-boolean v3, Lcom/android/internal/telephony/RIL;->IsAnswered:Z

    #@9d
    .line 1008
    iput-short v8, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->termCode:S

    #@9f
    .line 1015
    :cond_9f
    :goto_9f
    invoke-static {}, Lcom/lge/ciq/VoiceCallCIQ;->getGS03Metric()Lcom/carrieriq/iqagent/client/metrics/gs/GS03;

    #@a2
    move-result-object v1

    #@a3
    .line 1016
    if-eqz v1, :cond_10f

    #@a5
    .line 1017
    iget v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->ciqID:I

    #@a7
    iput v3, v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS03;->dwCallId:I

    #@a9
    .line 1018
    iput v7, v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS03;->dwErrCode:I

    #@ab
    .line 1019
    iget-short v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->termCode:S

    #@ad
    iput-short v3, v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS03;->wTermCode:S

    #@af
    .line 1020
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@b1
    iget-object v3, v3, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@b3
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@b6
    move-result-object v3

    #@b7
    invoke-static {v3, v1}, Lcom/lge/ciq/VoiceCallCIQ;->submitCSCall(Landroid/content/Context;Lcom/carrieriq/iqagent/client/Metric;)V

    #@ba
    .line 1026
    .end local v0           #gs02:Lcom/carrieriq/iqagent/client/metrics/gs/GS02;
    .end local v1           #gs03:Lcom/carrieriq/iqagent/client/metrics/gs/GS03;
    .end local v2           #state:B
    :cond_ba
    :goto_ba
    return-void

    #@bb
    .line 963
    :cond_bb
    sget-object v3, Lcom/android/internal/telephony/Connection$DisconnectCause;->TIMED_OUT:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@bd
    if-ne p1, v3, :cond_ca

    #@bf
    .line 964
    iget v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqId:I

    #@c1
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@c3
    const/16 v5, 0x66

    #@c5
    invoke-static {v3, v4, v5}, Lcom/android/internal/telephony/CallStateBroadcaster;->SendCallDisconnected(ILjava/lang/String;I)V

    #@c8
    goto/16 :goto_45

    #@ca
    .line 966
    :cond_ca
    iget v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqId:I

    #@cc
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@ce
    const/16 v5, 0x10

    #@d0
    invoke-static {v3, v4, v5}, Lcom/android/internal/telephony/CallStateBroadcaster;->SendCallDisconnected(ILjava/lang/String;I)V

    #@d3
    goto/16 :goto_45

    #@d5
    .line 1005
    .restart local v0       #gs02:Lcom/carrieriq/iqagent/client/metrics/gs/GS02;
    .restart local v1       #gs03:Lcom/carrieriq/iqagent/client/metrics/gs/GS03;
    .restart local v2       #state:B
    :cond_d5
    const-string v3, "LGDDM-CSC"

    #@d7
    const-string v4, "CSC State Transition metric instance is null!"

    #@d9
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@dc
    goto :goto_9a

    #@dd
    .line 1009
    :cond_dd
    iget-byte v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->ciqState:B

    #@df
    if-eq v3, v9, :cond_ef

    #@e1
    if-eq v2, v9, :cond_ef

    #@e3
    iget-short v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->termCode:S

    #@e5
    const/16 v4, 0x100

    #@e7
    if-eq v3, v4, :cond_ef

    #@e9
    iget-short v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->termCode:S

    #@eb
    const/16 v4, 0x3e9

    #@ed
    if-ne v3, v4, :cond_f2

    #@ef
    .line 1010
    :cond_ef
    iput-short v8, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->termCode:S

    #@f1
    goto :goto_9f

    #@f2
    .line 1011
    :cond_f2
    iget-byte v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->ciqState:B

    #@f4
    if-eq v3, v10, :cond_10a

    #@f6
    if-eq v2, v10, :cond_10a

    #@f8
    iget-short v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->termCode:S

    #@fa
    const/16 v4, 0x158

    #@fc
    if-eq v3, v4, :cond_10a

    #@fe
    iget-short v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->termCode:S

    #@100
    const/16 v4, 0x164

    #@102
    if-eq v3, v4, :cond_10a

    #@104
    iget-short v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->termCode:S

    #@106
    const/16 v4, 0x81

    #@108
    if-ne v3, v4, :cond_9f

    #@10a
    .line 1012
    :cond_10a
    const/16 v3, 0x202

    #@10c
    iput-short v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->termCode:S

    #@10e
    goto :goto_9f

    #@10f
    .line 1022
    :cond_10f
    const-string v3, "LGDDM-CSC"

    #@111
    const-string v4, "CSC End metric instance is null!!"

    #@113
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@116
    goto :goto_ba
.end method

.method onHangupLocal()V
    .registers 2

    #@0
    .prologue
    .line 555
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->LOCAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4
    .line 562
    return-void
.end method

.method onHangupLocalMissed()V
    .registers 2

    #@0
    .prologue
    .line 567
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->INCOMING_MISSED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4
    .line 568
    return-void
.end method

.method onRemoteDisconnect(I)V
    .registers 12
    .parameter "causeCode"

    #@0
    .prologue
    const/4 v9, 0x6

    #@1
    const/4 v8, -0x1

    #@2
    const/16 v7, 0x81

    #@4
    const/4 v6, 0x7

    #@5
    const/4 v5, 0x0

    #@6
    .line 858
    const/4 v2, 0x0

    #@7
    const-string v3, "support_network_change_auto_retry"

    #@9
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_5a

    #@f
    .line 859
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmConnection;->getAddress()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-static {v2}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumber(Ljava/lang/String;)Z

    #@16
    move-result v2

    #@17
    if-eqz v2, :cond_5a

    #@19
    .line 860
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/gsm/GsmConnection;->disconnectCauseFromCode(I)Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@1c
    move-result-object v0

    #@1d
    .line 861
    .local v0, cause:Lcom/android/internal/telephony/Connection$DisconnectCause;
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->NORMAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@1f
    if-eq v0, v2, :cond_25

    #@21
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->LOCAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@23
    if-ne v0, v2, :cond_5a

    #@25
    .line 862
    :cond_25
    new-instance v2, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v3, "cause = "

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    const-string v3, "getAddress() = "

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmConnection;->getAddress()Ljava/lang/String;

    #@3d
    move-result-object v3

    #@3e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v2

    #@46
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gsm/GsmConnection;->log(Ljava/lang/String;)V

    #@49
    .line 864
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@4b
    iget-object v2, v2, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@4d
    invoke-virtual {v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@50
    move-result-object v2

    #@51
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@54
    move-result-object v2

    #@55
    const-string v3, "network_change_auto_retry"

    #@57
    invoke-static {v2, v3, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@5a
    .line 872
    .end local v0           #cause:Lcom/android/internal/telephony/Connection$DisconnectCause;
    :cond_5a
    sget-boolean v2, Lcom/android/internal/telephony/gsm/GsmConnection;->CIQ_EXTENSION:Z

    #@5c
    if-eqz v2, :cond_7e

    #@5e
    iget v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqId:I

    #@60
    if-eq v2, v8, :cond_7e

    #@62
    .line 873
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/gsm/GsmConnection;->disconnectCauseFromCode(I)Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@65
    move-result-object v0

    #@66
    .line 874
    .restart local v0       #cause:Lcom/android/internal/telephony/Connection$DisconnectCause;
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->OUT_OF_SERVICE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@68
    if-ne v0, v2, :cond_155

    #@6a
    .line 875
    sget-boolean v2, Lcom/android/internal/telephony/gsm/GsmConnection;->DBG_CIQ:Z

    #@6c
    if-eqz v2, :cond_73

    #@6e
    const-string v2, "[EXTENSION] update onRemotedisconnect() CallFailCause.SERVICE_NOT_AVAILABLE(63)"

    #@70
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gsm/GsmConnection;->log(Ljava/lang/String;)V

    #@73
    .line 876
    :cond_73
    iget v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqId:I

    #@75
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@77
    const/16 v4, 0x3f

    #@79
    invoke-static {v2, v3, v4}, Lcom/android/internal/telephony/CallStateBroadcaster;->SendCallDisconnected(ILjava/lang/String;I)V

    #@7c
    .line 882
    :goto_7c
    iput v8, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqId:I

    #@7e
    .line 887
    .end local v0           #cause:Lcom/android/internal/telephony/Connection$DisconnectCause;
    :cond_7e
    const-string v2, "1"

    #@80
    const-string v3, "service.iq.active"

    #@82
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@85
    move-result-object v3

    #@86
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@89
    move-result v2

    #@8a
    if-eqz v2, :cond_14d

    #@8c
    .line 888
    iput-byte v5, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->ciqState:B

    #@8e
    .line 889
    iput p1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->systemCode:I

    #@90
    .line 890
    const-string v2, "LGDDM-CSC"

    #@92
    new-instance v3, Ljava/lang/StringBuilder;

    #@94
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@97
    const-string v4, "onRemoteDisconnect, systemCode1 -"

    #@99
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v3

    #@9d
    iget v4, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->systemCode:I

    #@9f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v3

    #@a3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a6
    move-result-object v3

    #@a7
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@aa
    .line 892
    iget v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->systemCode:I

    #@ac
    const v3, 0xffff

    #@af
    if-ne v2, v3, :cond_b3

    #@b1
    .line 894
    iput v7, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->systemCode:I

    #@b3
    .line 896
    :cond_b3
    iget v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->systemCode:I

    #@b5
    const/16 v3, 0x7f

    #@b7
    if-ge v2, v3, :cond_c0

    #@b9
    .line 898
    iget v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->systemCode:I

    #@bb
    int-to-short v2, v2

    #@bc
    iput-short v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->termCode:S

    #@be
    .line 899
    iput v5, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->systemCode:I

    #@c0
    .line 902
    :cond_c0
    iget v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->systemCode:I

    #@c2
    const/16 v3, 0x158

    #@c4
    if-eq v2, v3, :cond_dc

    #@c6
    iget v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->systemCode:I

    #@c8
    const/16 v3, 0x100

    #@ca
    if-eq v2, v3, :cond_dc

    #@cc
    iget v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->systemCode:I

    #@ce
    const/16 v3, 0x164

    #@d0
    if-eq v2, v3, :cond_dc

    #@d2
    iget v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->systemCode:I

    #@d4
    if-eq v2, v7, :cond_dc

    #@d6
    iget v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->systemCode:I

    #@d8
    const/16 v3, 0x3e9

    #@da
    if-ne v2, v3, :cond_e3

    #@dc
    .line 903
    :cond_dc
    iget v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->systemCode:I

    #@de
    int-to-short v2, v2

    #@df
    iput-short v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->termCode:S

    #@e1
    .line 904
    iput v5, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->systemCode:I

    #@e3
    .line 907
    :cond_e3
    const-string v2, "LGDDM-CSC"

    #@e5
    new-instance v3, Ljava/lang/StringBuilder;

    #@e7
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@ea
    const-string v4, "onRemoteDisconnect, systemCode2 -"

    #@ec
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v3

    #@f0
    iget v4, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->systemCode:I

    #@f2
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v3

    #@f6
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f9
    move-result-object v3

    #@fa
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@fd
    .line 908
    const-string v2, "LGDDM-CSC"

    #@ff
    new-instance v3, Ljava/lang/StringBuilder;

    #@101
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@104
    const-string v4, "onRemoteDisconnect, termCode -"

    #@106
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@109
    move-result-object v3

    #@10a
    iget-short v4, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->termCode:S

    #@10c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10f
    move-result-object v3

    #@110
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@113
    move-result-object v3

    #@114
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@117
    .line 910
    iget-short v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->termCode:S

    #@119
    const/16 v3, 0x10

    #@11b
    if-eq v2, v3, :cond_14d

    #@11d
    .line 911
    const/4 v1, 0x0

    #@11e
    .line 913
    .local v1, gs02:Lcom/carrieriq/iqagent/client/metrics/gs/GS02;
    invoke-static {}, Lcom/lge/ciq/VoiceCallCIQ;->getGS02Metric()Lcom/carrieriq/iqagent/client/metrics/gs/GS02;

    #@121
    move-result-object v1

    #@122
    .line 914
    if-eqz v1, :cond_186

    #@124
    .line 915
    iget v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->ciqID:I

    #@126
    iput v2, v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS02;->dwCallId:I

    #@128
    .line 917
    iget-short v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->termCode:S

    #@12a
    const/16 v3, 0x158

    #@12c
    if-eq v2, v3, :cond_13e

    #@12e
    iget-short v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->termCode:S

    #@130
    const/16 v3, 0x164

    #@132
    if-eq v2, v3, :cond_13e

    #@134
    iget-short v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->termCode:S

    #@136
    if-eq v2, v7, :cond_13e

    #@138
    iget-short v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->termCode:S

    #@13a
    const/16 v3, 0x3e9

    #@13c
    if-ne v2, v3, :cond_178

    #@13e
    .line 918
    :cond_13e
    iput-byte v6, v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS02;->ucCallState:B

    #@140
    .line 919
    iput-byte v6, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->ciqState:B

    #@142
    .line 927
    :goto_142
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@144
    iget-object v2, v2, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@146
    invoke-virtual {v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@149
    move-result-object v2

    #@14a
    invoke-static {v2, v1}, Lcom/lge/ciq/VoiceCallCIQ;->submitCSCall(Landroid/content/Context;Lcom/carrieriq/iqagent/client/Metric;)V

    #@14d
    .line 934
    .end local v1           #gs02:Lcom/carrieriq/iqagent/client/metrics/gs/GS02;
    :cond_14d
    :goto_14d
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/gsm/GsmConnection;->disconnectCauseFromCode(I)Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@150
    move-result-object v2

    #@151
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/GsmConnection;->onDisconnect(Lcom/android/internal/telephony/Connection$DisconnectCause;)V

    #@154
    .line 935
    return-void

    #@155
    .line 879
    .restart local v0       #cause:Lcom/android/internal/telephony/Connection$DisconnectCause;
    :cond_155
    sget-boolean v2, Lcom/android/internal/telephony/gsm/GsmConnection;->DBG_CIQ:Z

    #@157
    if-eqz v2, :cond_16f

    #@159
    new-instance v2, Ljava/lang/StringBuilder;

    #@15b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@15e
    const-string v3, "[CIQ_EXTENSION] update onRemotedisconnect() CallFailCause"

    #@160
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@163
    move-result-object v2

    #@164
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@167
    move-result-object v2

    #@168
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16b
    move-result-object v2

    #@16c
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gsm/GsmConnection;->log(Ljava/lang/String;)V

    #@16f
    .line 880
    :cond_16f
    iget v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqId:I

    #@171
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@173
    invoke-static {v2, v3, p1}, Lcom/android/internal/telephony/CallStateBroadcaster;->SendCallDisconnected(ILjava/lang/String;I)V

    #@176
    goto/16 :goto_7c

    #@178
    .line 920
    .end local v0           #cause:Lcom/android/internal/telephony/Connection$DisconnectCause;
    .restart local v1       #gs02:Lcom/carrieriq/iqagent/client/metrics/gs/GS02;
    :cond_178
    iget-short v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->termCode:S

    #@17a
    const/16 v3, 0x100

    #@17c
    if-ne v2, v3, :cond_183

    #@17e
    .line 921
    iput-byte v9, v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS02;->ucCallState:B

    #@180
    .line 922
    iput-byte v9, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->ciqState:B

    #@182
    goto :goto_142

    #@183
    .line 924
    :cond_183
    iput-byte v6, v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS02;->ucCallState:B

    #@185
    goto :goto_142

    #@186
    .line 929
    :cond_186
    const-string v2, "LGDDM-CSC"

    #@188
    const-string v3, "CSC State Transition metric instance is null!"

    #@18a
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18d
    goto :goto_14d
.end method

.method onStartedHolding()V
    .registers 3

    #@0
    .prologue
    .line 1306
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@3
    move-result-wide v0

    #@4
    iput-wide v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->holdingStartTime:J

    #@6
    .line 1307
    return-void
.end method

.method public proceedAfterWaitChar()V
    .registers 4

    #@0
    .prologue
    .line 486
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@2
    sget-object v1, Lcom/android/internal/telephony/Connection$PostDialState;->WAIT:Lcom/android/internal/telephony/Connection$PostDialState;

    #@4
    if-eq v0, v1, :cond_21

    #@6
    .line 487
    const-string v0, "GSM"

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "GsmConnection.proceedAfterWaitChar(): Expected getPostDialState() to be WAIT but was "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 495
    :goto_20
    return-void

    #@21
    .line 492
    :cond_21
    sget-object v0, Lcom/android/internal/telephony/Connection$PostDialState;->STARTED:Lcom/android/internal/telephony/Connection$PostDialState;

    #@23
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gsm/GsmConnection;->setPostDialState(Lcom/android/internal/telephony/Connection$PostDialState;)V

    #@26
    .line 494
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmConnection;->processNextPostDialChar()V

    #@29
    goto :goto_20
.end method

.method public proceedAfterWildChar(Ljava/lang/String;)V
    .registers 6
    .parameter "str"

    #@0
    .prologue
    .line 498
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@2
    sget-object v2, Lcom/android/internal/telephony/Connection$PostDialState;->WILD:Lcom/android/internal/telephony/Connection$PostDialState;

    #@4
    if-eq v1, v2, :cond_21

    #@6
    .line 499
    const-string v1, "GSM"

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "GsmConnection.proceedAfterWaitChar(): Expected getPostDialState() to be WILD but was "

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->postDialState:Lcom/android/internal/telephony/Connection$PostDialState;

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 542
    :goto_20
    return-void

    #@21
    .line 504
    :cond_21
    sget-object v1, Lcom/android/internal/telephony/Connection$PostDialState;->STARTED:Lcom/android/internal/telephony/Connection$PostDialState;

    #@23
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gsm/GsmConnection;->setPostDialState(Lcom/android/internal/telephony/Connection$PostDialState;)V

    #@26
    .line 531
    new-instance v0, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@2b
    .line 532
    .local v0, buf:Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->postDialString:Ljava/lang/String;

    #@2d
    iget v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->nextPostDialChar:I

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    .line 533
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v1

    #@3a
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->postDialString:Ljava/lang/String;

    #@3c
    .line 534
    const/4 v1, 0x0

    #@3d
    iput v1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->nextPostDialChar:I

    #@3f
    .line 536
    new-instance v1, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v2, "proceedAfterWildChar: new postDialString is "

    #@46
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->postDialString:Ljava/lang/String;

    #@4c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v1

    #@50
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v1

    #@54
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gsm/GsmConnection;->log(Ljava/lang/String;)V

    #@57
    .line 540
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmConnection;->processNextPostDialChar()V

    #@5a
    goto :goto_20
.end method

.method public separate()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 474
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->disconnected:Z

    #@2
    if-nez v0, :cond_a

    #@4
    .line 475
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@6
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->separate(Lcom/android/internal/telephony/gsm/GsmConnection;)V

    #@9
    .line 479
    return-void

    #@a
    .line 477
    :cond_a
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@c
    const-string v1, "disconnected"

    #@e
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0
.end method

.method public setAsFake(Z)V
    .registers 2
    .parameter "fake"

    #@0
    .prologue
    .line 1534
    if-eqz p1, :cond_5

    #@2
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmConnection;->releaseWakeLock()V

    #@5
    .line 1535
    :cond_5
    return-void
.end method

.method setCIQIdForTMUS(I)V
    .registers 2
    .parameter "id"

    #@0
    .prologue
    .line 1176
    iput p1, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqId:I

    #@2
    .line 1177
    return-void
.end method

.method update(Lcom/android/internal/telephony/DriverCall;)Z
    .registers 14
    .parameter "dc"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v9, 0x0

    #@2
    .line 1032
    const/4 v0, 0x0

    #@3
    .line 1033
    .local v0, changed:Z
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmConnection;->isConnectingInOrOut()Z

    #@6
    move-result v6

    #@7
    .line 1034
    .local v6, wasConnectingInOrOut:Z
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmConnection;->getState()Lcom/android/internal/telephony/Call$State;

    #@a
    move-result-object v10

    #@b
    sget-object v11, Lcom/android/internal/telephony/Call$State;->HOLDING:Lcom/android/internal/telephony/Call$State;

    #@d
    if-ne v10, v11, :cond_187

    #@f
    move v7, v8

    #@10
    .line 1037
    .local v7, wasHolding:Z
    :goto_10
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmConnection;->getState()Lcom/android/internal/telephony/Call$State;

    #@13
    move-result-object v3

    #@14
    .line 1040
    .local v3, oldState:Lcom/android/internal/telephony/Call$State;
    iget-object v10, p1, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@16
    invoke-direct {p0, v10}, Lcom/android/internal/telephony/gsm/GsmConnection;->parentFromDCState(Lcom/android/internal/telephony/DriverCall$State;)Lcom/android/internal/telephony/gsm/GsmCall;

    #@19
    move-result-object v2

    #@1a
    .line 1042
    .local v2, newParent:Lcom/android/internal/telephony/gsm/GsmCall;
    iget-object v10, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@1c
    iget-object v11, p1, Lcom/android/internal/telephony/DriverCall;->number:Ljava/lang/String;

    #@1e
    invoke-static {v10, v11}, Lcom/android/internal/telephony/gsm/GsmConnection;->equalsHandlesNulls(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@21
    move-result v10

    #@22
    if-nez v10, :cond_2e

    #@24
    .line 1043
    const-string v10, "update: phone # changed!"

    #@26
    invoke-direct {p0, v10}, Lcom/android/internal/telephony/gsm/GsmConnection;->log(Ljava/lang/String;)V

    #@29
    .line 1044
    iget-object v10, p1, Lcom/android/internal/telephony/DriverCall;->number:Ljava/lang/String;

    #@2b
    iput-object v10, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@2d
    .line 1045
    const/4 v0, 0x1

    #@2e
    .line 1049
    :cond_2e
    iget-object v10, p1, Lcom/android/internal/telephony/DriverCall;->name:Ljava/lang/String;

    #@30
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@33
    move-result v10

    #@34
    if-eqz v10, :cond_18a

    #@36
    .line 1050
    iget-object v10, p0, Lcom/android/internal/telephony/Connection;->cnapName:Ljava/lang/String;

    #@38
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3b
    move-result v10

    #@3c
    if-nez v10, :cond_43

    #@3e
    .line 1051
    const/4 v0, 0x1

    #@3f
    .line 1052
    const-string v10, ""

    #@41
    iput-object v10, p0, Lcom/android/internal/telephony/Connection;->cnapName:Ljava/lang/String;

    #@43
    .line 1059
    :cond_43
    :goto_43
    new-instance v10, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v11, "--dssds----"

    #@4a
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v10

    #@4e
    iget-object v11, p0, Lcom/android/internal/telephony/Connection;->cnapName:Ljava/lang/String;

    #@50
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v10

    #@54
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v10

    #@58
    invoke-direct {p0, v10}, Lcom/android/internal/telephony/gsm/GsmConnection;->log(Ljava/lang/String;)V

    #@5b
    .line 1060
    iget v10, p1, Lcom/android/internal/telephony/DriverCall;->namePresentation:I

    #@5d
    iput v10, p0, Lcom/android/internal/telephony/Connection;->cnapNamePresentation:I

    #@5f
    .line 1061
    iget v10, p1, Lcom/android/internal/telephony/DriverCall;->numberPresentation:I

    #@61
    iput v10, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->numberPresentation:I

    #@63
    .line 1063
    iget-object v10, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->parent:Lcom/android/internal/telephony/gsm/GsmCall;

    #@65
    if-eq v2, v10, :cond_19b

    #@67
    .line 1064
    iget-object v10, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->parent:Lcom/android/internal/telephony/gsm/GsmCall;

    #@69
    if-eqz v10, :cond_70

    #@6b
    .line 1065
    iget-object v10, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->parent:Lcom/android/internal/telephony/gsm/GsmCall;

    #@6d
    invoke-virtual {v10, p0}, Lcom/android/internal/telephony/gsm/GsmCall;->detach(Lcom/android/internal/telephony/gsm/GsmConnection;)V

    #@70
    .line 1067
    :cond_70
    invoke-virtual {v2, p0, p1}, Lcom/android/internal/telephony/gsm/GsmCall;->attach(Lcom/android/internal/telephony/Connection;Lcom/android/internal/telephony/DriverCall;)V

    #@73
    .line 1068
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->parent:Lcom/android/internal/telephony/gsm/GsmCall;

    #@75
    .line 1069
    const/4 v0, 0x1

    #@76
    .line 1078
    :goto_76
    new-instance v10, Ljava/lang/StringBuilder;

    #@78
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@7b
    const-string v11, "update: parent="

    #@7d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v10

    #@81
    iget-object v11, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->parent:Lcom/android/internal/telephony/gsm/GsmCall;

    #@83
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v10

    #@87
    const-string v11, ", hasNewParent="

    #@89
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v10

    #@8d
    iget-object v11, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->parent:Lcom/android/internal/telephony/gsm/GsmCall;

    #@8f
    if-eq v2, v11, :cond_1aa

    #@91
    :goto_91
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@94
    move-result-object v8

    #@95
    const-string v9, ", wasConnectingInOrOut="

    #@97
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v8

    #@9b
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v8

    #@9f
    const-string v9, ", wasHolding="

    #@a1
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v8

    #@a5
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v8

    #@a9
    const-string v9, ", isConnectingInOrOut="

    #@ab
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v8

    #@af
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmConnection;->isConnectingInOrOut()Z

    #@b2
    move-result v9

    #@b3
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v8

    #@b7
    const-string v9, ", changed="

    #@b9
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v8

    #@bd
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v8

    #@c1
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c4
    move-result-object v8

    #@c5
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/gsm/GsmConnection;->log(Ljava/lang/String;)V

    #@c8
    .line 1087
    if-eqz v6, :cond_d3

    #@ca
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GsmConnection;->isConnectingInOrOut()Z

    #@cd
    move-result v8

    #@ce
    if-nez v8, :cond_d3

    #@d0
    .line 1088
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmConnection;->onConnectedInOrOut()V

    #@d3
    .line 1091
    :cond_d3
    if-eqz v0, :cond_e2

    #@d5
    if-nez v7, :cond_e2

    #@d7
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmConnection;->getState()Lcom/android/internal/telephony/Call$State;

    #@da
    move-result-object v8

    #@db
    sget-object v9, Lcom/android/internal/telephony/Call$State;->HOLDING:Lcom/android/internal/telephony/Call$State;

    #@dd
    if-ne v8, v9, :cond_e2

    #@df
    .line 1093
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmConnection;->onStartedHolding()V

    #@e2
    .line 1097
    :cond_e2
    const/4 v8, 0x0

    #@e3
    const-string v9, "RingBackTone"

    #@e5
    invoke-static {v8, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@e8
    move-result v8

    #@e9
    if-eqz v8, :cond_ef

    #@eb
    .line 1098
    iget v8, p1, Lcom/android/internal/telephony/DriverCall;->signal:I

    #@ed
    iput v8, p0, Lcom/android/internal/telephony/Connection;->ringbackToneSignal:I

    #@ef
    .line 1102
    :cond_ef
    iget-object v8, p1, Lcom/android/internal/telephony/DriverCall;->cdnipNumber:Ljava/lang/String;

    #@f1
    iput-object v8, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->cdnipNumber:Ljava/lang/String;

    #@f3
    .line 1103
    new-instance v8, Ljava/lang/StringBuilder;

    #@f5
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@f8
    const-string v9, "cdnipNumber 1 = "

    #@fa
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fd
    move-result-object v8

    #@fe
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->cdnipNumber:Ljava/lang/String;

    #@100
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@103
    move-result-object v8

    #@104
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@107
    move-result-object v8

    #@108
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/gsm/GsmConnection;->log(Ljava/lang/String;)V

    #@10b
    .line 1107
    sget-boolean v8, Lcom/android/internal/telephony/gsm/GsmConnection;->CIQ_EXTENSION:Z

    #@10d
    if-eqz v8, :cond_150

    #@10f
    .line 1108
    iget-object v8, p1, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@111
    sget-object v9, Lcom/android/internal/telephony/DriverCall$State;->DIALING:Lcom/android/internal/telephony/DriverCall$State;

    #@113
    if-ne v8, v9, :cond_1ad

    #@115
    iget-object v8, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->CIQ_State:Lcom/android/internal/telephony/Call$State;

    #@117
    sget-object v9, Lcom/android/internal/telephony/Call$State;->DIALING:Lcom/android/internal/telephony/Call$State;

    #@119
    if-eq v8, v9, :cond_1ad

    #@11b
    .line 1109
    sget-object v8, Lcom/android/internal/telephony/Call$State;->DIALING:Lcom/android/internal/telephony/Call$State;

    #@11d
    iput-object v8, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->CIQ_State:Lcom/android/internal/telephony/Call$State;

    #@11f
    .line 1110
    sget-boolean v8, Lcom/android/internal/telephony/gsm/GsmConnection;->DBG_CIQ:Z

    #@121
    if-eqz v8, :cond_147

    #@123
    new-instance v8, Ljava/lang/StringBuilder;

    #@125
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@128
    const-string v9, "[CIQ_EXTENSION] update() dc.state="

    #@12a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12d
    move-result-object v8

    #@12e
    iget-object v9, p1, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@130
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@133
    move-result-object v8

    #@134
    const-string v9, ", CIQ_State="

    #@136
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@139
    move-result-object v8

    #@13a
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->CIQ_State:Lcom/android/internal/telephony/Call$State;

    #@13c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13f
    move-result-object v8

    #@140
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@143
    move-result-object v8

    #@144
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/gsm/GsmConnection;->log(Ljava/lang/String;)V

    #@147
    .line 1111
    :cond_147
    iget v8, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqId:I

    #@149
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@14b
    iget-object v10, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->CIQ_State:Lcom/android/internal/telephony/Call$State;

    #@14d
    invoke-static {v8, v9, v10}, Lcom/android/internal/telephony/CallStateBroadcaster;->SendCallStatus(ILjava/lang/String;Lcom/android/internal/telephony/Call$State;)V

    #@150
    .line 1142
    :cond_150
    :goto_150
    const-string v8, "1"

    #@152
    const-string v9, "service.iq.active"

    #@154
    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@157
    move-result-object v9

    #@158
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15b
    move-result v8

    #@15c
    if-eqz v8, :cond_186

    #@15e
    .line 1143
    if-eqz v0, :cond_186

    #@160
    .line 1144
    const/4 v5, 0x0

    #@161
    .line 1145
    .local v5, state:B
    const/4 v1, 0x0

    #@162
    .line 1147
    .local v1, gs02:Lcom/carrieriq/iqagent/client/metrics/gs/GS02;
    iget-object v8, p1, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@164
    sget-object v9, Lcom/android/internal/telephony/DriverCall$State;->DIALING:Lcom/android/internal/telephony/DriverCall$State;

    #@166
    if-eq v8, v9, :cond_16e

    #@168
    iget-object v8, p1, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@16a
    sget-object v9, Lcom/android/internal/telephony/DriverCall$State;->INCOMING:Lcom/android/internal/telephony/DriverCall$State;

    #@16c
    if-ne v8, v9, :cond_273

    #@16e
    .line 1148
    :cond_16e
    const/4 v5, 0x3

    #@16f
    .line 1157
    :cond_16f
    :goto_16f
    invoke-static {}, Lcom/lge/ciq/VoiceCallCIQ;->getGS02Metric()Lcom/carrieriq/iqagent/client/metrics/gs/GS02;

    #@172
    move-result-object v1

    #@173
    .line 1158
    if-eqz v1, :cond_295

    #@175
    .line 1159
    iget v8, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->ciqID:I

    #@177
    iput v8, v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS02;->dwCallId:I

    #@179
    .line 1160
    iput-byte v5, v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS02;->ucCallState:B

    #@17b
    .line 1161
    iget-object v8, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@17d
    iget-object v8, v8, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@17f
    invoke-virtual {v8}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@182
    move-result-object v8

    #@183
    invoke-static {v8, v1}, Lcom/lge/ciq/VoiceCallCIQ;->submitCSCall(Landroid/content/Context;Lcom/carrieriq/iqagent/client/Metric;)V

    #@186
    .line 1168
    .end local v1           #gs02:Lcom/carrieriq/iqagent/client/metrics/gs/GS02;
    .end local v5           #state:B
    :cond_186
    :goto_186
    return v0

    #@187
    .end local v2           #newParent:Lcom/android/internal/telephony/gsm/GsmCall;
    .end local v3           #oldState:Lcom/android/internal/telephony/Call$State;
    .end local v7           #wasHolding:Z
    :cond_187
    move v7, v9

    #@188
    .line 1034
    goto/16 :goto_10

    #@18a
    .line 1054
    .restart local v2       #newParent:Lcom/android/internal/telephony/gsm/GsmCall;
    .restart local v3       #oldState:Lcom/android/internal/telephony/Call$State;
    .restart local v7       #wasHolding:Z
    :cond_18a
    iget-object v10, p1, Lcom/android/internal/telephony/DriverCall;->name:Ljava/lang/String;

    #@18c
    iget-object v11, p0, Lcom/android/internal/telephony/Connection;->cnapName:Ljava/lang/String;

    #@18e
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@191
    move-result v10

    #@192
    if-nez v10, :cond_43

    #@194
    .line 1055
    const/4 v0, 0x1

    #@195
    .line 1056
    iget-object v10, p1, Lcom/android/internal/telephony/DriverCall;->name:Ljava/lang/String;

    #@197
    iput-object v10, p0, Lcom/android/internal/telephony/Connection;->cnapName:Ljava/lang/String;

    #@199
    goto/16 :goto_43

    #@19b
    .line 1072
    :cond_19b
    iget-object v10, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->parent:Lcom/android/internal/telephony/gsm/GsmCall;

    #@19d
    invoke-virtual {v10, p0, p1}, Lcom/android/internal/telephony/gsm/GsmCall;->update(Lcom/android/internal/telephony/gsm/GsmConnection;Lcom/android/internal/telephony/DriverCall;)Z

    #@1a0
    move-result v4

    #@1a1
    .line 1073
    .local v4, parentStateChange:Z
    if-nez v0, :cond_1a5

    #@1a3
    if-eqz v4, :cond_1a8

    #@1a5
    :cond_1a5
    move v0, v8

    #@1a6
    :goto_1a6
    goto/16 :goto_76

    #@1a8
    :cond_1a8
    move v0, v9

    #@1a9
    goto :goto_1a6

    #@1aa
    .end local v4           #parentStateChange:Z
    :cond_1aa
    move v8, v9

    #@1ab
    .line 1078
    goto/16 :goto_91

    #@1ad
    .line 1112
    :cond_1ad
    if-eqz v0, :cond_150

    #@1af
    .line 1113
    iget-object v8, p1, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@1b1
    sget-object v9, Lcom/android/internal/telephony/DriverCall$State;->ALERTING:Lcom/android/internal/telephony/DriverCall$State;

    #@1b3
    if-ne v8, v9, :cond_1e0

    #@1b5
    .line 1114
    sget-object v8, Lcom/android/internal/telephony/Call$State;->ALERTING:Lcom/android/internal/telephony/Call$State;

    #@1b7
    iput-object v8, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->CIQ_State:Lcom/android/internal/telephony/Call$State;

    #@1b9
    .line 1115
    sget-boolean v8, Lcom/android/internal/telephony/gsm/GsmConnection;->DBG_CIQ:Z

    #@1bb
    if-eqz v8, :cond_1d5

    #@1bd
    new-instance v8, Ljava/lang/StringBuilder;

    #@1bf
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@1c2
    const-string v9, "[CIQ_EXTENSION] update() CIQ_State"

    #@1c4
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c7
    move-result-object v8

    #@1c8
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->CIQ_State:Lcom/android/internal/telephony/Call$State;

    #@1ca
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1cd
    move-result-object v8

    #@1ce
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d1
    move-result-object v8

    #@1d2
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/gsm/GsmConnection;->log(Ljava/lang/String;)V

    #@1d5
    .line 1116
    :cond_1d5
    iget v8, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqId:I

    #@1d7
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@1d9
    iget-object v10, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->CIQ_State:Lcom/android/internal/telephony/Call$State;

    #@1db
    invoke-static {v8, v9, v10}, Lcom/android/internal/telephony/CallStateBroadcaster;->SendCallStatus(ILjava/lang/String;Lcom/android/internal/telephony/Call$State;)V

    #@1de
    goto/16 :goto_150

    #@1e0
    .line 1117
    :cond_1e0
    iget-object v8, p1, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@1e2
    sget-object v9, Lcom/android/internal/telephony/DriverCall$State;->WAITING:Lcom/android/internal/telephony/DriverCall$State;

    #@1e4
    if-ne v8, v9, :cond_211

    #@1e6
    .line 1118
    sget-object v8, Lcom/android/internal/telephony/Call$State;->WAITING:Lcom/android/internal/telephony/Call$State;

    #@1e8
    iput-object v8, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->CIQ_State:Lcom/android/internal/telephony/Call$State;

    #@1ea
    .line 1119
    sget-boolean v8, Lcom/android/internal/telephony/gsm/GsmConnection;->DBG_CIQ:Z

    #@1ec
    if-eqz v8, :cond_206

    #@1ee
    new-instance v8, Ljava/lang/StringBuilder;

    #@1f0
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@1f3
    const-string v9, "[CIQ_EXTENSION] update() CIQ_State"

    #@1f5
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f8
    move-result-object v8

    #@1f9
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->CIQ_State:Lcom/android/internal/telephony/Call$State;

    #@1fb
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1fe
    move-result-object v8

    #@1ff
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@202
    move-result-object v8

    #@203
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/gsm/GsmConnection;->log(Ljava/lang/String;)V

    #@206
    .line 1120
    :cond_206
    iget v8, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqId:I

    #@208
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@20a
    iget-object v10, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->CIQ_State:Lcom/android/internal/telephony/Call$State;

    #@20c
    invoke-static {v8, v9, v10}, Lcom/android/internal/telephony/CallStateBroadcaster;->SendCallStatus(ILjava/lang/String;Lcom/android/internal/telephony/Call$State;)V

    #@20f
    goto/16 :goto_150

    #@211
    .line 1121
    :cond_211
    iget-object v8, p1, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@213
    sget-object v9, Lcom/android/internal/telephony/DriverCall$State;->ACTIVE:Lcom/android/internal/telephony/DriverCall$State;

    #@215
    if-ne v8, v9, :cond_242

    #@217
    .line 1122
    sget-object v8, Lcom/android/internal/telephony/Call$State;->ACTIVE:Lcom/android/internal/telephony/Call$State;

    #@219
    iput-object v8, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->CIQ_State:Lcom/android/internal/telephony/Call$State;

    #@21b
    .line 1123
    sget-boolean v8, Lcom/android/internal/telephony/gsm/GsmConnection;->DBG_CIQ:Z

    #@21d
    if-eqz v8, :cond_237

    #@21f
    new-instance v8, Ljava/lang/StringBuilder;

    #@221
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@224
    const-string v9, "[CIQ_EXTENSION] update() CIQ_State"

    #@226
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@229
    move-result-object v8

    #@22a
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->CIQ_State:Lcom/android/internal/telephony/Call$State;

    #@22c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22f
    move-result-object v8

    #@230
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@233
    move-result-object v8

    #@234
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/gsm/GsmConnection;->log(Ljava/lang/String;)V

    #@237
    .line 1124
    :cond_237
    iget v8, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqId:I

    #@239
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@23b
    iget-object v10, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->CIQ_State:Lcom/android/internal/telephony/Call$State;

    #@23d
    invoke-static {v8, v9, v10}, Lcom/android/internal/telephony/CallStateBroadcaster;->SendCallStatus(ILjava/lang/String;Lcom/android/internal/telephony/Call$State;)V

    #@240
    goto/16 :goto_150

    #@242
    .line 1125
    :cond_242
    iget-object v8, p1, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@244
    sget-object v9, Lcom/android/internal/telephony/DriverCall$State;->HOLDING:Lcom/android/internal/telephony/DriverCall$State;

    #@246
    if-ne v8, v9, :cond_150

    #@248
    .line 1126
    sget-object v8, Lcom/android/internal/telephony/Call$State;->HOLDING:Lcom/android/internal/telephony/Call$State;

    #@24a
    iput-object v8, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->CIQ_State:Lcom/android/internal/telephony/Call$State;

    #@24c
    .line 1127
    sget-boolean v8, Lcom/android/internal/telephony/gsm/GsmConnection;->DBG_CIQ:Z

    #@24e
    if-eqz v8, :cond_268

    #@250
    new-instance v8, Ljava/lang/StringBuilder;

    #@252
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@255
    const-string v9, "[CIQ_EXTENSION] update() CIQ_State"

    #@257
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25a
    move-result-object v8

    #@25b
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->CIQ_State:Lcom/android/internal/telephony/Call$State;

    #@25d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@260
    move-result-object v8

    #@261
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@264
    move-result-object v8

    #@265
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/gsm/GsmConnection;->log(Ljava/lang/String;)V

    #@268
    .line 1128
    :cond_268
    iget v8, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->tmus_ciqId:I

    #@26a
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->address:Ljava/lang/String;

    #@26c
    iget-object v10, p0, Lcom/android/internal/telephony/gsm/GsmConnection;->CIQ_State:Lcom/android/internal/telephony/Call$State;

    #@26e
    invoke-static {v8, v9, v10}, Lcom/android/internal/telephony/CallStateBroadcaster;->SendCallStatus(ILjava/lang/String;Lcom/android/internal/telephony/Call$State;)V

    #@271
    goto/16 :goto_150

    #@273
    .line 1149
    .restart local v1       #gs02:Lcom/carrieriq/iqagent/client/metrics/gs/GS02;
    .restart local v5       #state:B
    :cond_273
    iget-object v8, p1, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@275
    sget-object v9, Lcom/android/internal/telephony/DriverCall$State;->ALERTING:Lcom/android/internal/telephony/DriverCall$State;

    #@277
    if-eq v8, v9, :cond_27f

    #@279
    iget-object v8, p1, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@27b
    sget-object v9, Lcom/android/internal/telephony/DriverCall$State;->WAITING:Lcom/android/internal/telephony/DriverCall$State;

    #@27d
    if-ne v8, v9, :cond_282

    #@27f
    .line 1150
    :cond_27f
    const/4 v5, 0x4

    #@280
    goto/16 :goto_16f

    #@282
    .line 1151
    :cond_282
    iget-object v8, p1, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@284
    sget-object v9, Lcom/android/internal/telephony/DriverCall$State;->ACTIVE:Lcom/android/internal/telephony/DriverCall$State;

    #@286
    if-ne v8, v9, :cond_28b

    #@288
    .line 1152
    const/4 v5, 0x5

    #@289
    goto/16 :goto_16f

    #@28b
    .line 1153
    :cond_28b
    iget-object v8, p1, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@28d
    sget-object v9, Lcom/android/internal/telephony/DriverCall$State;->HOLDING:Lcom/android/internal/telephony/DriverCall$State;

    #@28f
    if-ne v8, v9, :cond_16f

    #@291
    .line 1154
    const/16 v5, 0x8

    #@293
    goto/16 :goto_16f

    #@295
    .line 1163
    :cond_295
    const-string v8, "LGDDM-CSC"

    #@297
    const-string v9, "CSC State Transition metric instance is null!"

    #@299
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@29c
    goto/16 :goto_186
.end method
