.class Lcom/android/internal/telephony/LGDataRecovery$2;
.super Landroid/os/Handler;
.source "LGDataRecovery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/telephony/LGDataRecovery;-><init>(Lcom/android/internal/telephony/PhoneBase;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/LGDataRecovery;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/LGDataRecovery;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter "x0"

    #@0
    .prologue
    .line 836
    iput-object p1, p0, Lcom/android/internal/telephony/LGDataRecovery$2;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@2
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 839
    iget v1, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v1, :pswitch_data_2e

    #@5
    .line 848
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "Undefine Message: "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    iget v2, p1, Landroid/os/Message;->what:I

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-static {v1}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@1d
    .line 849
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    #@20
    .line 852
    :goto_20
    return-void

    #@21
    .line 843
    :pswitch_21
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@23
    check-cast v0, Landroid/os/AsyncResult;

    #@25
    .line 844
    .local v0, ar:Landroid/os/AsyncResult;
    iget-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery$2;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@27
    iget v2, p1, Landroid/os/Message;->what:I

    #@29
    invoke-virtual {v1, v2, v0}, Lcom/android/internal/telephony/LGDataRecovery;->onGetModemPacketCountDone(ILandroid/os/AsyncResult;)V

    #@2c
    goto :goto_20

    #@2d
    .line 839
    nop

    #@2e
    :pswitch_data_2e
    .packed-switch 0x42072
        :pswitch_21
    .end packed-switch
.end method
