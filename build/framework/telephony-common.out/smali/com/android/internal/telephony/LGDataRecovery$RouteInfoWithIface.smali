.class public Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;
.super Ljava/lang/Object;
.source "LGDataRecovery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/LGDataRecovery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RouteInfoWithIface"
.end annotation


# instance fields
.field dstAddr:Ljava/lang/String;

.field iface:Ljava/lang/String;

.field mtu:I

.field ri:Landroid/net/RouteInfo;

.field final synthetic this$0:Lcom/android/internal/telephony/LGDataRecovery;


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/LGDataRecovery;Ljava/lang/String;Landroid/net/RouteInfo;I)V
    .registers 6
    .parameter
    .parameter "interfaceName"
    .parameter "routeInfo"
    .parameter "mtuSize"

    #@0
    .prologue
    .line 1327
    iput-object p1, p0, Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 1328
    iput-object p2, p0, Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;->iface:Ljava/lang/String;

    #@7
    .line 1329
    iput-object p3, p0, Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;->ri:Landroid/net/RouteInfo;

    #@9
    .line 1330
    invoke-virtual {p3}, Landroid/net/RouteInfo;->getDestination()Landroid/net/LinkAddress;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {v0}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    iput-object v0, p0, Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;->dstAddr:Ljava/lang/String;

    #@17
    .line 1331
    iput p4, p0, Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;->mtu:I

    #@19
    .line 1332
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 1336
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "[iface: "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;->iface:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", mtu: "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;->mtu:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ", "

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget-object v1, p0, Lcom/android/internal/telephony/LGDataRecovery$RouteInfoWithIface;->ri:Landroid/net/RouteInfo;

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, "]"

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v0

    #@33
    return-object v0
.end method
