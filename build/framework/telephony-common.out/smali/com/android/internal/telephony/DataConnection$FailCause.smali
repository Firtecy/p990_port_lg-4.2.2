.class public final enum Lcom/android/internal/telephony/DataConnection$FailCause;
.super Ljava/lang/Enum;
.source "DataConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/DataConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FailCause"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/DataConnection$FailCause;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum ACTIVATION_REJECT_GGSN:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum ACTIVATION_REJECT_UNSPECIFIED:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum APN_RESTRICTION_VALUE:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum CONNECTION_TO_DATACONNECTIONAC_BROKEN:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum EMEREGNCY_CALL_ATTACH_FAIL:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum EMERGENCY_CALL_PDN_CONNECTION_FAIL:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum ERROR_UNSPECIFIED:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum GPRS_REGISTRATION_FAIL:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum INSUFFICIENT_RESOURCES:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum INTERNAL_REASON:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum MISSING_UNKNOWN_APN:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum NONE:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum NSAPI_IN_USE:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum ONLY_IPV4_ALLOWED:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum ONLY_IPV6_ALLOWED:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum ONLY_SINGLE_BEARER_ALLOWED:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum OPERATOR_BARRED:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum PREF_RADIO_TECH_CHANGED:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum PROTOCOL_ERRORS:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum RADIO_NOT_AVAILABLE:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum RADIO_POWER_OFF:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum REGISTRATION_FAIL:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum SERVICE_OPTION_NOT_SUBSCRIBED:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum SERVICE_OPTION_NOT_SUPPORTED:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum SERVICE_OPTION_OUT_OF_ORDER:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum SIGNAL_LOST:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum SM_ACTIVATION_REJECTED_BCM_VIOLATION:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum SM_CONDITIONAL_IE_ERR:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum SM_FEATURE_NOT_SUPPORTED:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum SM_IE_NON_EXISTENT:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum SM_INVALID_MANDATORY_INFO:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum SM_INVALID_TRANS_ID:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum SM_LLC_SNDCP_FAILURE:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum SM_MBMS_BEARER_CAP_INSUFFICIENT:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum SM_MBMS_GROUP_MEMBERSHIP_TIMEOUT:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum SM_MESG_NOT_COMPATIBLE:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum SM_MESG_TYPE_NON_EXISTENT:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum SM_MESG_TYPE_NOT_COMPATIBLE:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum SM_NETWORK_FAILURE:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum SM_PDP_WITHOUT_TFT_ACTIVE:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum SM_QOS_NOT_ACCEPTED:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum SM_REACTIVATION_REQUIRED:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum SM_REGULAR_DEACTIVATION:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum SM_SEMANTIC_ERR_IN_FILTER:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum SM_SEMANTIC_ERR_IN_TFT:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum SM_SEMANTIC_INCORRECT_MESG:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum SM_SYNTACTIC_ERR_IN_FILTER:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum SM_SYNTACTIC_ERR_IN_TFT:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum SM_UNKNOWN_PDP_CONTEXT:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum TETHERED_CALL_ACTIVE:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum UNACCEPTABLE_NETWORK_PARAMETER:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum UNKNOWN:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum UNKNOWN_PDP_ADDRESS_TYPE:Lcom/android/internal/telephony/DataConnection$FailCause;

.field public static final enum USER_AUTHENTICATION:Lcom/android/internal/telephony/DataConnection$FailCause;

.field private static final sErrorCodeToFailCauseMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/android/internal/telephony/DataConnection$FailCause;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mErrorCode:I


# direct methods
.method static constructor <clinit>()V
    .registers 13

    #@0
    .prologue
    const/16 v12, 0x1a

    #@2
    const/16 v11, 0x19

    #@4
    const/16 v10, 0x18

    #@6
    const/16 v9, 0x8

    #@8
    const/4 v8, 0x0

    #@9
    .line 145
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@b
    const-string v5, "NONE"

    #@d
    invoke-direct {v4, v5, v8, v8}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@10
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->NONE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@12
    .line 149
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@14
    const-string v5, "OPERATOR_BARRED"

    #@16
    const/4 v6, 0x1

    #@17
    invoke-direct {v4, v5, v6, v9}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@1a
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->OPERATOR_BARRED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@1c
    .line 150
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@1e
    const-string v5, "INSUFFICIENT_RESOURCES"

    #@20
    const/4 v6, 0x2

    #@21
    invoke-direct {v4, v5, v6, v12}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@24
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->INSUFFICIENT_RESOURCES:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@26
    .line 151
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@28
    const-string v5, "MISSING_UNKNOWN_APN"

    #@2a
    const/4 v6, 0x3

    #@2b
    const/16 v7, 0x1b

    #@2d
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@30
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->MISSING_UNKNOWN_APN:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@32
    .line 152
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@34
    const-string v5, "UNKNOWN_PDP_ADDRESS_TYPE"

    #@36
    const/4 v6, 0x4

    #@37
    const/16 v7, 0x1c

    #@39
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@3c
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->UNKNOWN_PDP_ADDRESS_TYPE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@3e
    .line 153
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@40
    const-string v5, "USER_AUTHENTICATION"

    #@42
    const/4 v6, 0x5

    #@43
    const/16 v7, 0x1d

    #@45
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@48
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->USER_AUTHENTICATION:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@4a
    .line 154
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@4c
    const-string v5, "ACTIVATION_REJECT_GGSN"

    #@4e
    const/4 v6, 0x6

    #@4f
    const/16 v7, 0x1e

    #@51
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@54
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->ACTIVATION_REJECT_GGSN:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@56
    .line 155
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@58
    const-string v5, "ACTIVATION_REJECT_UNSPECIFIED"

    #@5a
    const/4 v6, 0x7

    #@5b
    const/16 v7, 0x1f

    #@5d
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@60
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->ACTIVATION_REJECT_UNSPECIFIED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@62
    .line 156
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@64
    const-string v5, "SERVICE_OPTION_NOT_SUPPORTED"

    #@66
    const/16 v6, 0x20

    #@68
    invoke-direct {v4, v5, v9, v6}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@6b
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->SERVICE_OPTION_NOT_SUPPORTED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@6d
    .line 157
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@6f
    const-string v5, "SERVICE_OPTION_NOT_SUBSCRIBED"

    #@71
    const/16 v6, 0x9

    #@73
    const/16 v7, 0x21

    #@75
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@78
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->SERVICE_OPTION_NOT_SUBSCRIBED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@7a
    .line 158
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@7c
    const-string v5, "SERVICE_OPTION_OUT_OF_ORDER"

    #@7e
    const/16 v6, 0xa

    #@80
    const/16 v7, 0x22

    #@82
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@85
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->SERVICE_OPTION_OUT_OF_ORDER:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@87
    .line 159
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@89
    const-string v5, "NSAPI_IN_USE"

    #@8b
    const/16 v6, 0xb

    #@8d
    const/16 v7, 0x23

    #@8f
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@92
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->NSAPI_IN_USE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@94
    .line 160
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@96
    const-string v5, "ONLY_IPV4_ALLOWED"

    #@98
    const/16 v6, 0xc

    #@9a
    const/16 v7, 0x32

    #@9c
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@9f
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->ONLY_IPV4_ALLOWED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@a1
    .line 161
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@a3
    const-string v5, "ONLY_IPV6_ALLOWED"

    #@a5
    const/16 v6, 0xd

    #@a7
    const/16 v7, 0x33

    #@a9
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@ac
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->ONLY_IPV6_ALLOWED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@ae
    .line 162
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@b0
    const-string v5, "ONLY_SINGLE_BEARER_ALLOWED"

    #@b2
    const/16 v6, 0xe

    #@b4
    const/16 v7, 0x34

    #@b6
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@b9
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->ONLY_SINGLE_BEARER_ALLOWED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@bb
    .line 163
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@bd
    const-string v5, "PROTOCOL_ERRORS"

    #@bf
    const/16 v6, 0xf

    #@c1
    const/16 v7, 0x6f

    #@c3
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@c6
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->PROTOCOL_ERRORS:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@c8
    .line 165
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@ca
    const-string v5, "APN_RESTRICTION_VALUE"

    #@cc
    const/16 v6, 0x10

    #@ce
    const/16 v7, 0x70

    #@d0
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@d3
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->APN_RESTRICTION_VALUE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@d5
    .line 167
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@d7
    const-string v5, "SM_MBMS_BEARER_CAP_INSUFFICIENT"

    #@d9
    const/16 v6, 0x11

    #@db
    invoke-direct {v4, v5, v6, v10}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@de
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_MBMS_BEARER_CAP_INSUFFICIENT:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@e0
    .line 168
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@e2
    const-string v5, "SM_LLC_SNDCP_FAILURE"

    #@e4
    const/16 v6, 0x12

    #@e6
    invoke-direct {v4, v5, v6, v11}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@e9
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_LLC_SNDCP_FAILURE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@eb
    .line 169
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@ed
    const-string v5, "SM_REGULAR_DEACTIVATION"

    #@ef
    const/16 v6, 0x13

    #@f1
    const/16 v7, 0x24

    #@f3
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@f6
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_REGULAR_DEACTIVATION:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@f8
    .line 170
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@fa
    const-string v5, "SM_QOS_NOT_ACCEPTED"

    #@fc
    const/16 v6, 0x14

    #@fe
    const/16 v7, 0x25

    #@100
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@103
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_QOS_NOT_ACCEPTED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@105
    .line 171
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@107
    const-string v5, "SM_NETWORK_FAILURE"

    #@109
    const/16 v6, 0x15

    #@10b
    const/16 v7, 0x26

    #@10d
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@110
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_NETWORK_FAILURE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@112
    .line 172
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@114
    const-string v5, "SM_REACTIVATION_REQUIRED"

    #@116
    const/16 v6, 0x16

    #@118
    const/16 v7, 0x27

    #@11a
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@11d
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_REACTIVATION_REQUIRED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@11f
    .line 173
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@121
    const-string v5, "SM_FEATURE_NOT_SUPPORTED"

    #@123
    const/16 v6, 0x17

    #@125
    const/16 v7, 0x28

    #@127
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@12a
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_FEATURE_NOT_SUPPORTED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@12c
    .line 174
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@12e
    const-string v5, "SM_SEMANTIC_ERR_IN_TFT"

    #@130
    const/16 v6, 0x29

    #@132
    invoke-direct {v4, v5, v10, v6}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@135
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_SEMANTIC_ERR_IN_TFT:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@137
    .line 175
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@139
    const-string v5, "SM_SYNTACTIC_ERR_IN_TFT"

    #@13b
    const/16 v6, 0x2a

    #@13d
    invoke-direct {v4, v5, v11, v6}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@140
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_SYNTACTIC_ERR_IN_TFT:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@142
    .line 176
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@144
    const-string v5, "SM_UNKNOWN_PDP_CONTEXT"

    #@146
    const/16 v6, 0x2b

    #@148
    invoke-direct {v4, v5, v12, v6}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@14b
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_UNKNOWN_PDP_CONTEXT:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@14d
    .line 177
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@14f
    const-string v5, "SM_SEMANTIC_ERR_IN_FILTER"

    #@151
    const/16 v6, 0x1b

    #@153
    const/16 v7, 0x2c

    #@155
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@158
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_SEMANTIC_ERR_IN_FILTER:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@15a
    .line 178
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@15c
    const-string v5, "SM_SYNTACTIC_ERR_IN_FILTER"

    #@15e
    const/16 v6, 0x1c

    #@160
    const/16 v7, 0x2d

    #@162
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@165
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_SYNTACTIC_ERR_IN_FILTER:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@167
    .line 179
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@169
    const-string v5, "SM_PDP_WITHOUT_TFT_ACTIVE"

    #@16b
    const/16 v6, 0x1d

    #@16d
    const/16 v7, 0x2e

    #@16f
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@172
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_PDP_WITHOUT_TFT_ACTIVE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@174
    .line 180
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@176
    const-string v5, "SM_MBMS_GROUP_MEMBERSHIP_TIMEOUT"

    #@178
    const/16 v6, 0x1e

    #@17a
    const/16 v7, 0x2f

    #@17c
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@17f
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_MBMS_GROUP_MEMBERSHIP_TIMEOUT:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@181
    .line 181
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@183
    const-string v5, "SM_ACTIVATION_REJECTED_BCM_VIOLATION"

    #@185
    const/16 v6, 0x1f

    #@187
    const/16 v7, 0x30

    #@189
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@18c
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_ACTIVATION_REJECTED_BCM_VIOLATION:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@18e
    .line 182
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@190
    const-string v5, "SM_INVALID_TRANS_ID"

    #@192
    const/16 v6, 0x20

    #@194
    const/16 v7, 0x51

    #@196
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@199
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_INVALID_TRANS_ID:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@19b
    .line 183
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@19d
    const-string v5, "SM_SEMANTIC_INCORRECT_MESG"

    #@19f
    const/16 v6, 0x21

    #@1a1
    const/16 v7, 0x5f

    #@1a3
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@1a6
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_SEMANTIC_INCORRECT_MESG:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@1a8
    .line 184
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@1aa
    const-string v5, "SM_INVALID_MANDATORY_INFO"

    #@1ac
    const/16 v6, 0x22

    #@1ae
    const/16 v7, 0x60

    #@1b0
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@1b3
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_INVALID_MANDATORY_INFO:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@1b5
    .line 185
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@1b7
    const-string v5, "SM_MESG_TYPE_NON_EXISTENT"

    #@1b9
    const/16 v6, 0x23

    #@1bb
    const/16 v7, 0x61

    #@1bd
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@1c0
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_MESG_TYPE_NON_EXISTENT:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@1c2
    .line 186
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@1c4
    const-string v5, "SM_MESG_TYPE_NOT_COMPATIBLE"

    #@1c6
    const/16 v6, 0x24

    #@1c8
    const/16 v7, 0x62

    #@1ca
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@1cd
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_MESG_TYPE_NOT_COMPATIBLE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@1cf
    .line 187
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@1d1
    const-string v5, "SM_IE_NON_EXISTENT"

    #@1d3
    const/16 v6, 0x25

    #@1d5
    const/16 v7, 0x63

    #@1d7
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@1da
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_IE_NON_EXISTENT:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@1dc
    .line 188
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@1de
    const-string v5, "SM_CONDITIONAL_IE_ERR"

    #@1e0
    const/16 v6, 0x26

    #@1e2
    const/16 v7, 0x64

    #@1e4
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@1e7
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_CONDITIONAL_IE_ERR:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@1e9
    .line 189
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@1eb
    const-string v5, "SM_MESG_NOT_COMPATIBLE"

    #@1ed
    const/16 v6, 0x27

    #@1ef
    const/16 v7, 0x65

    #@1f1
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@1f4
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_MESG_NOT_COMPATIBLE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@1f6
    .line 190
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@1f8
    const-string v5, "INTERNAL_REASON"

    #@1fa
    const/16 v6, 0x28

    #@1fc
    const/16 v7, 0x378

    #@1fe
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@201
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->INTERNAL_REASON:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@203
    .line 196
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@205
    const-string v5, "REGISTRATION_FAIL"

    #@207
    const/16 v6, 0x29

    #@209
    const/4 v7, -0x1

    #@20a
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@20d
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->REGISTRATION_FAIL:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@20f
    .line 197
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@211
    const-string v5, "GPRS_REGISTRATION_FAIL"

    #@213
    const/16 v6, 0x2a

    #@215
    const/4 v7, -0x2

    #@216
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@219
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->GPRS_REGISTRATION_FAIL:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@21b
    .line 198
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@21d
    const-string v5, "SIGNAL_LOST"

    #@21f
    const/16 v6, 0x2b

    #@221
    const/4 v7, -0x3

    #@222
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@225
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->SIGNAL_LOST:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@227
    .line 199
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@229
    const-string v5, "PREF_RADIO_TECH_CHANGED"

    #@22b
    const/16 v6, 0x2c

    #@22d
    const/4 v7, -0x4

    #@22e
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@231
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->PREF_RADIO_TECH_CHANGED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@233
    .line 200
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@235
    const-string v5, "RADIO_POWER_OFF"

    #@237
    const/16 v6, 0x2d

    #@239
    const/4 v7, -0x5

    #@23a
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@23d
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->RADIO_POWER_OFF:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@23f
    .line 201
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@241
    const-string v5, "TETHERED_CALL_ACTIVE"

    #@243
    const/16 v6, 0x2e

    #@245
    const/4 v7, -0x6

    #@246
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@249
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->TETHERED_CALL_ACTIVE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@24b
    .line 203
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@24d
    const-string v5, "EMERGENCY_CALL_PDN_CONNECTION_FAIL"

    #@24f
    const/16 v6, 0x2f

    #@251
    const/16 v7, 0xfa

    #@253
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@256
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->EMERGENCY_CALL_PDN_CONNECTION_FAIL:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@258
    .line 204
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@25a
    const-string v5, "EMEREGNCY_CALL_ATTACH_FAIL"

    #@25c
    const/16 v6, 0x30

    #@25e
    const/16 v7, 0xfb

    #@260
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@263
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->EMEREGNCY_CALL_ATTACH_FAIL:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@265
    .line 206
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@267
    const-string v5, "ERROR_UNSPECIFIED"

    #@269
    const/16 v6, 0x31

    #@26b
    const v7, 0xffff

    #@26e
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@271
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->ERROR_UNSPECIFIED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@273
    .line 210
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@275
    const-string v5, "UNKNOWN"

    #@277
    const/16 v6, 0x32

    #@279
    const/high16 v7, 0x1

    #@27b
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@27e
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->UNKNOWN:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@280
    .line 211
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@282
    const-string v5, "RADIO_NOT_AVAILABLE"

    #@284
    const/16 v6, 0x33

    #@286
    const v7, 0x10001

    #@289
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@28c
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->RADIO_NOT_AVAILABLE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@28e
    .line 212
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@290
    const-string v5, "UNACCEPTABLE_NETWORK_PARAMETER"

    #@292
    const/16 v6, 0x34

    #@294
    const v7, 0x10002

    #@297
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@29a
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->UNACCEPTABLE_NETWORK_PARAMETER:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@29c
    .line 213
    new-instance v4, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@29e
    const-string v5, "CONNECTION_TO_DATACONNECTIONAC_BROKEN"

    #@2a0
    const/16 v6, 0x35

    #@2a2
    const v7, 0x10003

    #@2a5
    invoke-direct {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection$FailCause;-><init>(Ljava/lang/String;II)V

    #@2a8
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->CONNECTION_TO_DATACONNECTIONAC_BROKEN:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2aa
    .line 144
    const/16 v4, 0x36

    #@2ac
    new-array v4, v4, [Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2ae
    sget-object v5, Lcom/android/internal/telephony/DataConnection$FailCause;->NONE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2b0
    aput-object v5, v4, v8

    #@2b2
    const/4 v5, 0x1

    #@2b3
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->OPERATOR_BARRED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2b5
    aput-object v6, v4, v5

    #@2b7
    const/4 v5, 0x2

    #@2b8
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->INSUFFICIENT_RESOURCES:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2ba
    aput-object v6, v4, v5

    #@2bc
    const/4 v5, 0x3

    #@2bd
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->MISSING_UNKNOWN_APN:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2bf
    aput-object v6, v4, v5

    #@2c1
    const/4 v5, 0x4

    #@2c2
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->UNKNOWN_PDP_ADDRESS_TYPE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2c4
    aput-object v6, v4, v5

    #@2c6
    const/4 v5, 0x5

    #@2c7
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->USER_AUTHENTICATION:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2c9
    aput-object v6, v4, v5

    #@2cb
    const/4 v5, 0x6

    #@2cc
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->ACTIVATION_REJECT_GGSN:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2ce
    aput-object v6, v4, v5

    #@2d0
    const/4 v5, 0x7

    #@2d1
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->ACTIVATION_REJECT_UNSPECIFIED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2d3
    aput-object v6, v4, v5

    #@2d5
    sget-object v5, Lcom/android/internal/telephony/DataConnection$FailCause;->SERVICE_OPTION_NOT_SUPPORTED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2d7
    aput-object v5, v4, v9

    #@2d9
    const/16 v5, 0x9

    #@2db
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->SERVICE_OPTION_NOT_SUBSCRIBED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2dd
    aput-object v6, v4, v5

    #@2df
    const/16 v5, 0xa

    #@2e1
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->SERVICE_OPTION_OUT_OF_ORDER:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2e3
    aput-object v6, v4, v5

    #@2e5
    const/16 v5, 0xb

    #@2e7
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->NSAPI_IN_USE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2e9
    aput-object v6, v4, v5

    #@2eb
    const/16 v5, 0xc

    #@2ed
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->ONLY_IPV4_ALLOWED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2ef
    aput-object v6, v4, v5

    #@2f1
    const/16 v5, 0xd

    #@2f3
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->ONLY_IPV6_ALLOWED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2f5
    aput-object v6, v4, v5

    #@2f7
    const/16 v5, 0xe

    #@2f9
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->ONLY_SINGLE_BEARER_ALLOWED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2fb
    aput-object v6, v4, v5

    #@2fd
    const/16 v5, 0xf

    #@2ff
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->PROTOCOL_ERRORS:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@301
    aput-object v6, v4, v5

    #@303
    const/16 v5, 0x10

    #@305
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->APN_RESTRICTION_VALUE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@307
    aput-object v6, v4, v5

    #@309
    const/16 v5, 0x11

    #@30b
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_MBMS_BEARER_CAP_INSUFFICIENT:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@30d
    aput-object v6, v4, v5

    #@30f
    const/16 v5, 0x12

    #@311
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_LLC_SNDCP_FAILURE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@313
    aput-object v6, v4, v5

    #@315
    const/16 v5, 0x13

    #@317
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_REGULAR_DEACTIVATION:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@319
    aput-object v6, v4, v5

    #@31b
    const/16 v5, 0x14

    #@31d
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_QOS_NOT_ACCEPTED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@31f
    aput-object v6, v4, v5

    #@321
    const/16 v5, 0x15

    #@323
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_NETWORK_FAILURE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@325
    aput-object v6, v4, v5

    #@327
    const/16 v5, 0x16

    #@329
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_REACTIVATION_REQUIRED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@32b
    aput-object v6, v4, v5

    #@32d
    const/16 v5, 0x17

    #@32f
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_FEATURE_NOT_SUPPORTED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@331
    aput-object v6, v4, v5

    #@333
    sget-object v5, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_SEMANTIC_ERR_IN_TFT:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@335
    aput-object v5, v4, v10

    #@337
    sget-object v5, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_SYNTACTIC_ERR_IN_TFT:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@339
    aput-object v5, v4, v11

    #@33b
    sget-object v5, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_UNKNOWN_PDP_CONTEXT:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@33d
    aput-object v5, v4, v12

    #@33f
    const/16 v5, 0x1b

    #@341
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_SEMANTIC_ERR_IN_FILTER:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@343
    aput-object v6, v4, v5

    #@345
    const/16 v5, 0x1c

    #@347
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_SYNTACTIC_ERR_IN_FILTER:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@349
    aput-object v6, v4, v5

    #@34b
    const/16 v5, 0x1d

    #@34d
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_PDP_WITHOUT_TFT_ACTIVE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@34f
    aput-object v6, v4, v5

    #@351
    const/16 v5, 0x1e

    #@353
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_MBMS_GROUP_MEMBERSHIP_TIMEOUT:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@355
    aput-object v6, v4, v5

    #@357
    const/16 v5, 0x1f

    #@359
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_ACTIVATION_REJECTED_BCM_VIOLATION:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@35b
    aput-object v6, v4, v5

    #@35d
    const/16 v5, 0x20

    #@35f
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_INVALID_TRANS_ID:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@361
    aput-object v6, v4, v5

    #@363
    const/16 v5, 0x21

    #@365
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_SEMANTIC_INCORRECT_MESG:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@367
    aput-object v6, v4, v5

    #@369
    const/16 v5, 0x22

    #@36b
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_INVALID_MANDATORY_INFO:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@36d
    aput-object v6, v4, v5

    #@36f
    const/16 v5, 0x23

    #@371
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_MESG_TYPE_NON_EXISTENT:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@373
    aput-object v6, v4, v5

    #@375
    const/16 v5, 0x24

    #@377
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_MESG_TYPE_NOT_COMPATIBLE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@379
    aput-object v6, v4, v5

    #@37b
    const/16 v5, 0x25

    #@37d
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_IE_NON_EXISTENT:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@37f
    aput-object v6, v4, v5

    #@381
    const/16 v5, 0x26

    #@383
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_CONDITIONAL_IE_ERR:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@385
    aput-object v6, v4, v5

    #@387
    const/16 v5, 0x27

    #@389
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->SM_MESG_NOT_COMPATIBLE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@38b
    aput-object v6, v4, v5

    #@38d
    const/16 v5, 0x28

    #@38f
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->INTERNAL_REASON:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@391
    aput-object v6, v4, v5

    #@393
    const/16 v5, 0x29

    #@395
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->REGISTRATION_FAIL:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@397
    aput-object v6, v4, v5

    #@399
    const/16 v5, 0x2a

    #@39b
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->GPRS_REGISTRATION_FAIL:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@39d
    aput-object v6, v4, v5

    #@39f
    const/16 v5, 0x2b

    #@3a1
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->SIGNAL_LOST:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@3a3
    aput-object v6, v4, v5

    #@3a5
    const/16 v5, 0x2c

    #@3a7
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->PREF_RADIO_TECH_CHANGED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@3a9
    aput-object v6, v4, v5

    #@3ab
    const/16 v5, 0x2d

    #@3ad
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->RADIO_POWER_OFF:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@3af
    aput-object v6, v4, v5

    #@3b1
    const/16 v5, 0x2e

    #@3b3
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->TETHERED_CALL_ACTIVE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@3b5
    aput-object v6, v4, v5

    #@3b7
    const/16 v5, 0x2f

    #@3b9
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->EMERGENCY_CALL_PDN_CONNECTION_FAIL:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@3bb
    aput-object v6, v4, v5

    #@3bd
    const/16 v5, 0x30

    #@3bf
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->EMEREGNCY_CALL_ATTACH_FAIL:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@3c1
    aput-object v6, v4, v5

    #@3c3
    const/16 v5, 0x31

    #@3c5
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->ERROR_UNSPECIFIED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@3c7
    aput-object v6, v4, v5

    #@3c9
    const/16 v5, 0x32

    #@3cb
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->UNKNOWN:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@3cd
    aput-object v6, v4, v5

    #@3cf
    const/16 v5, 0x33

    #@3d1
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->RADIO_NOT_AVAILABLE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@3d3
    aput-object v6, v4, v5

    #@3d5
    const/16 v5, 0x34

    #@3d7
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->UNACCEPTABLE_NETWORK_PARAMETER:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@3d9
    aput-object v6, v4, v5

    #@3db
    const/16 v5, 0x35

    #@3dd
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->CONNECTION_TO_DATACONNECTIONAC_BROKEN:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@3df
    aput-object v6, v4, v5

    #@3e1
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->$VALUES:[Lcom/android/internal/telephony/DataConnection$FailCause;

    #@3e3
    .line 218
    new-instance v4, Ljava/util/HashMap;

    #@3e5
    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    #@3e8
    sput-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->sErrorCodeToFailCauseMap:Ljava/util/HashMap;

    #@3ea
    .line 219
    invoke-static {}, Lcom/android/internal/telephony/DataConnection$FailCause;->values()[Lcom/android/internal/telephony/DataConnection$FailCause;

    #@3ed
    move-result-object v0

    #@3ee
    .local v0, arr$:[Lcom/android/internal/telephony/DataConnection$FailCause;
    array-length v3, v0

    #@3ef
    .local v3, len$:I
    const/4 v2, 0x0

    #@3f0
    .local v2, i$:I
    :goto_3f0
    if-ge v2, v3, :cond_404

    #@3f2
    aget-object v1, v0, v2

    #@3f4
    .line 220
    .local v1, fc:Lcom/android/internal/telephony/DataConnection$FailCause;
    sget-object v4, Lcom/android/internal/telephony/DataConnection$FailCause;->sErrorCodeToFailCauseMap:Ljava/util/HashMap;

    #@3f6
    invoke-virtual {v1}, Lcom/android/internal/telephony/DataConnection$FailCause;->getErrorCode()I

    #@3f9
    move-result v5

    #@3fa
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3fd
    move-result-object v5

    #@3fe
    invoke-virtual {v4, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@401
    .line 219
    add-int/lit8 v2, v2, 0x1

    #@403
    goto :goto_3f0

    #@404
    .line 222
    .end local v1           #fc:Lcom/android/internal/telephony/DataConnection$FailCause;
    :cond_404
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter "errorCode"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 224
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 225
    iput p3, p0, Lcom/android/internal/telephony/DataConnection$FailCause;->mErrorCode:I

    #@5
    .line 226
    return-void
.end method

.method public static fromInt(I)Lcom/android/internal/telephony/DataConnection$FailCause;
    .registers 4
    .parameter "errorCode"

    #@0
    .prologue
    .line 318
    sget-object v1, Lcom/android/internal/telephony/DataConnection$FailCause;->sErrorCodeToFailCauseMap:Ljava/util/HashMap;

    #@2
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v2

    #@6
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@c
    .line 319
    .local v0, fc:Lcom/android/internal/telephony/DataConnection$FailCause;
    if-nez v0, :cond_10

    #@e
    .line 320
    sget-object v0, Lcom/android/internal/telephony/DataConnection$FailCause;->UNKNOWN:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@10
    .line 322
    :cond_10
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/DataConnection$FailCause;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 144
    const-class v0, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/DataConnection$FailCause;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/DataConnection$FailCause;
    .registers 1

    #@0
    .prologue
    .line 144
    sget-object v0, Lcom/android/internal/telephony/DataConnection$FailCause;->$VALUES:[Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/DataConnection$FailCause;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/DataConnection$FailCause;

    #@8
    return-object v0
.end method


# virtual methods
.method public getErrorCode()I
    .registers 2

    #@0
    .prologue
    .line 230
    iget v0, p0, Lcom/android/internal/telephony/DataConnection$FailCause;->mErrorCode:I

    #@2
    return v0
.end method

.method public isDataProfileFailure()Z
    .registers 2

    #@0
    .prologue
    .line 309
    sget-object v0, Lcom/android/internal/telephony/DataConnection$FailCause;->MISSING_UNKNOWN_APN:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2
    if-eq p0, v0, :cond_8

    #@4
    sget-object v0, Lcom/android/internal/telephony/DataConnection$FailCause;->USER_AUTHENTICATION:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@6
    if-ne p0, v0, :cond_a

    #@8
    :cond_8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public isEventLoggable()Z
    .registers 2

    #@0
    .prologue
    .line 298
    sget-object v0, Lcom/android/internal/telephony/DataConnection$FailCause;->OPERATOR_BARRED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2
    if-eq p0, v0, :cond_30

    #@4
    sget-object v0, Lcom/android/internal/telephony/DataConnection$FailCause;->INSUFFICIENT_RESOURCES:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@6
    if-eq p0, v0, :cond_30

    #@8
    sget-object v0, Lcom/android/internal/telephony/DataConnection$FailCause;->UNKNOWN_PDP_ADDRESS_TYPE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@a
    if-eq p0, v0, :cond_30

    #@c
    sget-object v0, Lcom/android/internal/telephony/DataConnection$FailCause;->USER_AUTHENTICATION:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@e
    if-eq p0, v0, :cond_30

    #@10
    sget-object v0, Lcom/android/internal/telephony/DataConnection$FailCause;->ACTIVATION_REJECT_GGSN:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@12
    if-eq p0, v0, :cond_30

    #@14
    sget-object v0, Lcom/android/internal/telephony/DataConnection$FailCause;->ACTIVATION_REJECT_UNSPECIFIED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@16
    if-eq p0, v0, :cond_30

    #@18
    sget-object v0, Lcom/android/internal/telephony/DataConnection$FailCause;->SERVICE_OPTION_NOT_SUBSCRIBED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@1a
    if-eq p0, v0, :cond_30

    #@1c
    sget-object v0, Lcom/android/internal/telephony/DataConnection$FailCause;->SERVICE_OPTION_NOT_SUPPORTED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@1e
    if-eq p0, v0, :cond_30

    #@20
    sget-object v0, Lcom/android/internal/telephony/DataConnection$FailCause;->SERVICE_OPTION_OUT_OF_ORDER:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@22
    if-eq p0, v0, :cond_30

    #@24
    sget-object v0, Lcom/android/internal/telephony/DataConnection$FailCause;->NSAPI_IN_USE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@26
    if-eq p0, v0, :cond_30

    #@28
    sget-object v0, Lcom/android/internal/telephony/DataConnection$FailCause;->PROTOCOL_ERRORS:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2a
    if-eq p0, v0, :cond_30

    #@2c
    sget-object v0, Lcom/android/internal/telephony/DataConnection$FailCause;->UNACCEPTABLE_NETWORK_PARAMETER:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2e
    if-ne p0, v0, :cond_32

    #@30
    :cond_30
    const/4 v0, 0x1

    #@31
    :goto_31
    return v0

    #@32
    :cond_32
    const/4 v0, 0x0

    #@33
    goto :goto_31
.end method

.method public isPdpAvailabilityFailure()Z
    .registers 2

    #@0
    .prologue
    .line 313
    sget-object v0, Lcom/android/internal/telephony/DataConnection$FailCause;->UNKNOWN_PDP_ADDRESS_TYPE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2
    if-eq p0, v0, :cond_c

    #@4
    sget-object v0, Lcom/android/internal/telephony/DataConnection$FailCause;->ONLY_IPV4_ALLOWED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@6
    if-eq p0, v0, :cond_c

    #@8
    sget-object v0, Lcom/android/internal/telephony/DataConnection$FailCause;->ONLY_IPV6_ALLOWED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@a
    if-ne p0, v0, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public isPermanentFail()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 237
    const-string v3, "ro.afwdata.LGfeatureset"

    #@4
    const-string v4, "none"

    #@6
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .line 239
    .local v0, featureset:Ljava/lang/String;
    const-string v3, "SKTBASE"

    #@c
    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@f
    move-result v3

    #@10
    if-eqz v3, :cond_13

    #@12
    .line 290
    :cond_12
    :goto_12
    return v1

    #@13
    .line 246
    :cond_13
    const-string v3, "DCMBASE"

    #@15
    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@18
    move-result v3

    #@19
    if-eqz v3, :cond_29

    #@1b
    .line 247
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->MISSING_UNKNOWN_APN:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@1d
    if-eq p0, v3, :cond_27

    #@1f
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->ONLY_IPV4_ALLOWED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@21
    if-eq p0, v3, :cond_27

    #@23
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->ONLY_IPV6_ALLOWED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@25
    if-ne p0, v3, :cond_12

    #@27
    :cond_27
    move v1, v2

    #@28
    .line 248
    goto :goto_12

    #@29
    .line 256
    :cond_29
    const-string v3, "LGTBASE"

    #@2b
    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@2e
    move-result v3

    #@2f
    if-eqz v3, :cond_53

    #@31
    const-string v3, "false"

    #@33
    const-string v4, "gsm.operator.isroaming"

    #@35
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3c
    move-result v3

    #@3d
    if-eqz v3, :cond_53

    #@3f
    const-string v3, "lgu"

    #@41
    const-string v4, "gsm.sim.type"

    #@43
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@46
    move-result-object v4

    #@47
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4a
    move-result v3

    #@4b
    if-eqz v3, :cond_53

    #@4d
    .line 257
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->OPERATOR_BARRED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@4f
    if-ne p0, v3, :cond_12

    #@51
    move v1, v2

    #@52
    .line 258
    goto :goto_12

    #@53
    .line 264
    :cond_53
    const-string v3, "KTBASE"

    #@55
    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@58
    move-result v3

    #@59
    if-eqz v3, :cond_81

    #@5b
    .line 266
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->OPERATOR_BARRED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@5d
    if-eq p0, v3, :cond_7f

    #@5f
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->MISSING_UNKNOWN_APN:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@61
    if-eq p0, v3, :cond_7f

    #@63
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->UNKNOWN_PDP_ADDRESS_TYPE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@65
    if-eq p0, v3, :cond_7f

    #@67
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->USER_AUTHENTICATION:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@69
    if-eq p0, v3, :cond_7f

    #@6b
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->ACTIVATION_REJECT_GGSN:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@6d
    if-eq p0, v3, :cond_7f

    #@6f
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->SERVICE_OPTION_NOT_SUPPORTED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@71
    if-eq p0, v3, :cond_7f

    #@73
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->SERVICE_OPTION_NOT_SUBSCRIBED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@75
    if-eq p0, v3, :cond_7f

    #@77
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->NSAPI_IN_USE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@79
    if-eq p0, v3, :cond_7f

    #@7b
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->PROTOCOL_ERRORS:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@7d
    if-ne p0, v3, :cond_12

    #@7f
    :cond_7f
    move v1, v2

    #@80
    .line 271
    goto :goto_12

    #@81
    .line 279
    :cond_81
    invoke-static {}, Lcom/android/internal/telephony/LGfeature;->getInstance()Lcom/android/internal/telephony/LGfeature;

    #@84
    move-result-object v3

    #@85
    iget-boolean v3, v3, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_REDEFINE_PERMANENT_CAUSE_EU:Z

    #@87
    if-eqz v3, :cond_ac

    #@89
    .line 280
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->OPERATOR_BARRED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@8b
    if-eq p0, v3, :cond_a9

    #@8d
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->MISSING_UNKNOWN_APN:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@8f
    if-eq p0, v3, :cond_a9

    #@91
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->UNKNOWN_PDP_ADDRESS_TYPE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@93
    if-eq p0, v3, :cond_a9

    #@95
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->USER_AUTHENTICATION:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@97
    if-eq p0, v3, :cond_a9

    #@99
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->ACTIVATION_REJECT_GGSN:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@9b
    if-eq p0, v3, :cond_a9

    #@9d
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->SERVICE_OPTION_NOT_SUPPORTED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@9f
    if-eq p0, v3, :cond_a9

    #@a1
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->SERVICE_OPTION_NOT_SUBSCRIBED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@a3
    if-eq p0, v3, :cond_a9

    #@a5
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->NSAPI_IN_USE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@a7
    if-ne p0, v3, :cond_12

    #@a9
    :cond_a9
    move v1, v2

    #@aa
    .line 284
    goto/16 :goto_12

    #@ac
    .line 290
    :cond_ac
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->OPERATOR_BARRED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@ae
    if-eq p0, v3, :cond_d0

    #@b0
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->MISSING_UNKNOWN_APN:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@b2
    if-eq p0, v3, :cond_d0

    #@b4
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->UNKNOWN_PDP_ADDRESS_TYPE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@b6
    if-eq p0, v3, :cond_d0

    #@b8
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->USER_AUTHENTICATION:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@ba
    if-eq p0, v3, :cond_d0

    #@bc
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->ACTIVATION_REJECT_GGSN:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@be
    if-eq p0, v3, :cond_d0

    #@c0
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->SERVICE_OPTION_NOT_SUPPORTED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@c2
    if-eq p0, v3, :cond_d0

    #@c4
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->SERVICE_OPTION_NOT_SUBSCRIBED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@c6
    if-eq p0, v3, :cond_d0

    #@c8
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->NSAPI_IN_USE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@ca
    if-eq p0, v3, :cond_d0

    #@cc
    sget-object v3, Lcom/android/internal/telephony/DataConnection$FailCause;->PROTOCOL_ERRORS:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@ce
    if-ne p0, v3, :cond_12

    #@d0
    :cond_d0
    move v1, v2

    #@d1
    goto/16 :goto_12
.end method
