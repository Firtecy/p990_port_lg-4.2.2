.class public Lcom/android/internal/telephony/uicc/AdnRecord;
.super Ljava/lang/Object;
.source "AdnRecord.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field static final ADN_BCD_NUMBER_LENGTH:I = 0x0

.field static final ADN_CAPABILITY_ID:I = 0xc

.field static final ADN_DIALING_NUMBER_END:I = 0xb

.field static final ADN_DIALING_NUMBER_START:I = 0x2

.field static final ADN_EXTENSION_ID:I = 0xd

.field static final ADN_TON_AND_NPI:I = 0x1

.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/internal/telephony/uicc/AdnRecord;",
            ">;"
        }
    .end annotation
.end field

.field static final EXT_RECORD_LENGTH_BYTES:I = 0xd

.field static final EXT_RECORD_TYPE_ADDITIONAL_DATA:I = 0x2

.field static final EXT_RECORD_TYPE_MASK:I = 0x3

.field static final FOOTER_SIZE_BYTES:I = 0xe

.field static final LOG_TAG:Ljava/lang/String; = "GSM"

.field static final MAX_EXT_CALLED_PARTY_LENGTH:I = 0xa

.field static final MAX_NUMBER_SIZE_BYTES:I = 0xb


# instance fields
.field alphaTag:Ljava/lang/String;

.field efid:I

.field emails:[Ljava/lang/String;

.field extRecord:I

.field number:Ljava/lang/String;

.field recordNumber:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 74
    new-instance v0, Lcom/android/internal/telephony/uicc/AdnRecord$1;

    #@2
    invoke-direct {v0}, Lcom/android/internal/telephony/uicc/AdnRecord$1;-><init>()V

    #@5
    sput-object v0, Lcom/android/internal/telephony/uicc/AdnRecord;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(IILjava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "efid"
    .parameter "recordNumber"
    .parameter "alphaTag"
    .parameter "number"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 125
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 42
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->alphaTag:Ljava/lang/String;

    #@6
    .line 43
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->number:Ljava/lang/String;

    #@8
    .line 45
    const/16 v0, 0xff

    #@a
    iput v0, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->extRecord:I

    #@c
    .line 126
    iput p1, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->efid:I

    #@e
    .line 127
    iput p2, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->recordNumber:I

    #@10
    .line 128
    iput-object p3, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->alphaTag:Ljava/lang/String;

    #@12
    .line 129
    iput-object p4, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->number:Ljava/lang/String;

    #@14
    .line 130
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->emails:[Ljava/lang/String;

    #@16
    .line 131
    return-void
.end method

.method public constructor <init>(IILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .registers 7
    .parameter "efid"
    .parameter "recordNumber"
    .parameter "alphaTag"
    .parameter "number"
    .parameter "emails"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 117
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 42
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->alphaTag:Ljava/lang/String;

    #@6
    .line 43
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->number:Ljava/lang/String;

    #@8
    .line 45
    const/16 v0, 0xff

    #@a
    iput v0, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->extRecord:I

    #@c
    .line 118
    iput p1, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->efid:I

    #@e
    .line 119
    iput p2, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->recordNumber:I

    #@10
    .line 120
    iput-object p3, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->alphaTag:Ljava/lang/String;

    #@12
    .line 121
    iput-object p4, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->number:Ljava/lang/String;

    #@14
    .line 122
    iput-object p5, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->emails:[Ljava/lang/String;

    #@16
    .line 123
    return-void
.end method

.method public constructor <init>(II[B)V
    .registers 5
    .parameter "efid"
    .parameter "recordNumber"
    .parameter "record"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 103
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 42
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->alphaTag:Ljava/lang/String;

    #@6
    .line 43
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->number:Ljava/lang/String;

    #@8
    .line 45
    const/16 v0, 0xff

    #@a
    iput v0, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->extRecord:I

    #@c
    .line 104
    iput p1, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->efid:I

    #@e
    .line 105
    iput p2, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->recordNumber:I

    #@10
    .line 106
    invoke-direct {p0, p3}, Lcom/android/internal/telephony/uicc/AdnRecord;->parseRecord([B)V

    #@13
    .line 107
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "alphaTag"
    .parameter "number"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 110
    invoke-direct {p0, v0, v0, p1, p2}, Lcom/android/internal/telephony/uicc/AdnRecord;-><init>(IILjava/lang/String;Ljava/lang/String;)V

    #@4
    .line 111
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .registers 10
    .parameter "alphaTag"
    .parameter "number"
    .parameter "emails"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 114
    move-object v0, p0

    #@2
    move v2, v1

    #@3
    move-object v3, p1

    #@4
    move-object v4, p2

    #@5
    move-object v5, p3

    #@6
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/telephony/uicc/AdnRecord;-><init>(IILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    #@9
    .line 115
    return-void
.end method

.method public constructor <init>([B)V
    .registers 3
    .parameter "record"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 100
    invoke-direct {p0, v0, v0, p1}, Lcom/android/internal/telephony/uicc/AdnRecord;-><init>(II[B)V

    #@4
    .line 101
    return-void
.end method

.method private parseRecord([B)V
    .registers 8
    .parameter "record"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 288
    const/4 v3, 0x0

    #@2
    :try_start_2
    array-length v4, p1

    #@3
    add-int/lit8 v4, v4, -0xe

    #@5
    invoke-static {p1, v3, v4}, Lcom/android/internal/telephony/uicc/IccUtils;->adnStringFieldToString([BII)Ljava/lang/String;

    #@8
    move-result-object v3

    #@9
    iput-object v3, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->alphaTag:Ljava/lang/String;

    #@b
    .line 291
    array-length v3, p1

    #@c
    add-int/lit8 v1, v3, -0xe

    #@e
    .line 293
    .local v1, footerOffset:I
    aget-byte v3, p1, v1

    #@10
    and-int/lit16 v2, v3, 0xff

    #@12
    .line 295
    .local v2, numberLength:I
    const/16 v3, 0xb

    #@14
    if-le v2, v3, :cond_1b

    #@16
    .line 297
    const-string v3, ""

    #@18
    iput-object v3, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->number:Ljava/lang/String;

    #@1a
    .line 322
    .end local v1           #footerOffset:I
    .end local v2           #numberLength:I
    :goto_1a
    return-void

    #@1b
    .line 308
    .restart local v1       #footerOffset:I
    .restart local v2       #numberLength:I
    :cond_1b
    add-int/lit8 v3, v1, 0x1

    #@1d
    invoke-static {p1, v3, v2}, Landroid/telephony/PhoneNumberUtils;->calledPartyBCDToString([BII)Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    iput-object v3, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->number:Ljava/lang/String;

    #@23
    .line 312
    array-length v3, p1

    #@24
    add-int/lit8 v3, v3, -0x1

    #@26
    aget-byte v3, p1, v3

    #@28
    and-int/lit16 v3, v3, 0xff

    #@2a
    iput v3, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->extRecord:I

    #@2c
    .line 314
    const/4 v3, 0x0

    #@2d
    iput-object v3, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->emails:[Ljava/lang/String;
    :try_end_2f
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2f} :catch_30

    #@2f
    goto :goto_1a

    #@30
    .line 316
    .end local v1           #footerOffset:I
    .end local v2           #numberLength:I
    :catch_30
    move-exception v0

    #@31
    .line 317
    .local v0, ex:Ljava/lang/RuntimeException;
    const-string v3, "GSM"

    #@33
    const-string v4, "Error parsing AdnRecord"

    #@35
    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@38
    .line 318
    const-string v3, ""

    #@3a
    iput-object v3, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->number:Ljava/lang/String;

    #@3c
    .line 319
    const-string v3, ""

    #@3e
    iput-object v3, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->alphaTag:Ljava/lang/String;

    #@40
    .line 320
    iput-object v5, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->emails:[Ljava/lang/String;

    #@42
    goto :goto_1a
.end method

.method private static stringCompareNullEqualsEmpty(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 3
    .parameter "s1"
    .parameter "s2"

    #@0
    .prologue
    .line 165
    if-ne p0, p1, :cond_4

    #@2
    .line 166
    const/4 v0, 0x1

    #@3
    .line 174
    :goto_3
    return v0

    #@4
    .line 168
    :cond_4
    if-nez p0, :cond_8

    #@6
    .line 169
    const-string p0, ""

    #@8
    .line 171
    :cond_8
    if-nez p1, :cond_c

    #@a
    .line 172
    const-string p1, ""

    #@c
    .line 174
    :cond_c
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    goto :goto_3
.end method


# virtual methods
.method public appendExtRecord([B)V
    .registers 6
    .parameter "extRecord"

    #@0
    .prologue
    const/4 v3, 0x2

    #@1
    .line 256
    :try_start_1
    array-length v1, p1

    #@2
    const/16 v2, 0xd

    #@4
    if-eq v1, v2, :cond_7

    #@6
    .line 278
    :cond_6
    :goto_6
    return-void

    #@7
    .line 260
    :cond_7
    const/4 v1, 0x0

    #@8
    aget-byte v1, p1, v1

    #@a
    and-int/lit8 v1, v1, 0x3

    #@c
    if-ne v1, v3, :cond_6

    #@e
    .line 265
    const/4 v1, 0x1

    #@f
    aget-byte v1, p1, v1

    #@11
    and-int/lit16 v1, v1, 0xff

    #@13
    const/16 v2, 0xa

    #@15
    if-gt v1, v2, :cond_6

    #@17
    .line 270
    new-instance v1, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->number:Ljava/lang/String;

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    const/4 v2, 0x2

    #@23
    const/4 v3, 0x1

    #@24
    aget-byte v3, p1, v3

    #@26
    and-int/lit16 v3, v3, 0xff

    #@28
    invoke-static {p1, v2, v3}, Landroid/telephony/PhoneNumberUtils;->calledPartyBCDFragmentToString([BII)Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->number:Ljava/lang/String;
    :try_end_36
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_36} :catch_37

    #@36
    goto :goto_6

    #@37
    .line 275
    :catch_37
    move-exception v0

    #@38
    .line 276
    .local v0, ex:Ljava/lang/RuntimeException;
    const-string v1, "GSM"

    #@3a
    const-string v2, "Error parsing AdnRecord ext record"

    #@3c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3f
    goto :goto_6
.end method

.method public buildAdnString(I)[B
    .registers 12
    .parameter "recordSize"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v9, 0x0

    #@2
    const/4 v8, -0x1

    #@3
    .line 208
    add-int/lit8 v3, p1, -0xe

    #@5
    .line 211
    .local v3, footerOffset:I
    new-array v0, p1, [B

    #@7
    .line 212
    .local v0, adnString:[B
    const/4 v4, 0x0

    #@8
    .local v4, i:I
    :goto_8
    if-ge v4, p1, :cond_f

    #@a
    .line 213
    aput-byte v8, v0, v4

    #@c
    .line 212
    add-int/lit8 v4, v4, 0x1

    #@e
    goto :goto_8

    #@f
    .line 216
    :cond_f
    iget-object v6, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->number:Ljava/lang/String;

    #@11
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@14
    move-result v6

    #@15
    if-eqz v6, :cond_1f

    #@17
    .line 217
    const-string v5, "GSM"

    #@19
    const-string v6, "[buildAdnString] Empty dialing number"

    #@1b
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 246
    .end local v0           #adnString:[B
    :cond_1e
    :goto_1e
    return-object v0

    #@1f
    .line 219
    .restart local v0       #adnString:[B
    :cond_1f
    iget-object v6, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->number:Ljava/lang/String;

    #@21
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@24
    move-result v6

    #@25
    const/16 v7, 0x14

    #@27
    if-le v6, v7, :cond_32

    #@29
    .line 221
    const-string v6, "GSM"

    #@2b
    const-string v7, "[buildAdnString] Max length of dialing number is 20"

    #@2d
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    move-object v0, v5

    #@31
    .line 223
    goto :goto_1e

    #@32
    .line 224
    :cond_32
    iget-object v6, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->alphaTag:Ljava/lang/String;

    #@34
    if-eqz v6, :cond_58

    #@36
    iget-object v6, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->alphaTag:Ljava/lang/String;

    #@38
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@3b
    move-result v6

    #@3c
    if-le v6, v3, :cond_58

    #@3e
    .line 225
    const-string v6, "GSM"

    #@40
    new-instance v7, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v8, "[buildAdnString] Max length of tag is "

    #@47
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v7

    #@4b
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v7

    #@4f
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v7

    #@53
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    move-object v0, v5

    #@57
    .line 227
    goto :goto_1e

    #@58
    .line 229
    :cond_58
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->number:Ljava/lang/String;

    #@5a
    invoke-static {v5}, Landroid/telephony/PhoneNumberUtils;->numberToCalledPartyBCD(Ljava/lang/String;)[B

    #@5d
    move-result-object v1

    #@5e
    .line 231
    .local v1, bcdNumber:[B
    add-int/lit8 v5, v3, 0x1

    #@60
    array-length v6, v1

    #@61
    invoke-static {v1, v9, v0, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@64
    .line 234
    add-int/lit8 v5, v3, 0x0

    #@66
    array-length v6, v1

    #@67
    int-to-byte v6, v6

    #@68
    aput-byte v6, v0, v5

    #@6a
    .line 236
    add-int/lit8 v5, v3, 0xc

    #@6c
    aput-byte v8, v0, v5

    #@6e
    .line 238
    add-int/lit8 v5, v3, 0xd

    #@70
    aput-byte v8, v0, v5

    #@72
    .line 241
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->alphaTag:Ljava/lang/String;

    #@74
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@77
    move-result v5

    #@78
    if-nez v5, :cond_1e

    #@7a
    .line 242
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->alphaTag:Ljava/lang/String;

    #@7c
    invoke-static {v5}, Lcom/android/internal/telephony/GsmAlphabet;->stringToGsm8BitPacked(Ljava/lang/String;)[B

    #@7f
    move-result-object v2

    #@80
    .line 243
    .local v2, byteTag:[B
    array-length v5, v2

    #@81
    invoke-static {v2, v9, v0, v9, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@84
    goto :goto_1e
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 185
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getAlphaTag()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 136
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->alphaTag:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getEmails()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 144
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->emails:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getNumber()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 140
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->number:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public hasExtendedRecord()Z
    .registers 3

    #@0
    .prologue
    .line 160
    iget v0, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->extRecord:I

    #@2
    if-eqz v0, :cond_c

    #@4
    iget v0, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->extRecord:I

    #@6
    const/16 v1, 0xff

    #@8
    if-eq v0, v1, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public isEmpty()Z
    .registers 2

    #@0
    .prologue
    .line 156
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->alphaTag:Ljava/lang/String;

    #@2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_16

    #@8
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->number:Ljava/lang/String;

    #@a
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_16

    #@10
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->emails:[Ljava/lang/String;

    #@12
    if-nez v0, :cond_16

    #@14
    const/4 v0, 0x1

    #@15
    :goto_15
    return v0

    #@16
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_15
.end method

.method public isEqual(Lcom/android/internal/telephony/uicc/AdnRecord;)Z
    .registers 4
    .parameter "adn"

    #@0
    .prologue
    .line 178
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->alphaTag:Ljava/lang/String;

    #@2
    iget-object v1, p1, Lcom/android/internal/telephony/uicc/AdnRecord;->alphaTag:Ljava/lang/String;

    #@4
    invoke-static {v0, v1}, Lcom/android/internal/telephony/uicc/AdnRecord;->stringCompareNullEqualsEmpty(Ljava/lang/String;Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_20

    #@a
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->number:Ljava/lang/String;

    #@c
    iget-object v1, p1, Lcom/android/internal/telephony/uicc/AdnRecord;->number:Ljava/lang/String;

    #@e
    invoke-static {v0, v1}, Lcom/android/internal/telephony/uicc/AdnRecord;->stringCompareNullEqualsEmpty(Ljava/lang/String;Ljava/lang/String;)Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_20

    #@14
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->emails:[Ljava/lang/String;

    #@16
    iget-object v1, p1, Lcom/android/internal/telephony/uicc/AdnRecord;->emails:[Ljava/lang/String;

    #@18
    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_20

    #@1e
    const/4 v0, 0x1

    #@1f
    :goto_1f
    return v0

    #@20
    :cond_20
    const/4 v0, 0x0

    #@21
    goto :goto_1f
.end method

.method public setEmails([Ljava/lang/String;)V
    .registers 2
    .parameter "emails"

    #@0
    .prologue
    .line 148
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->emails:[Ljava/lang/String;

    #@2
    .line 149
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 152
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "ADN Record \'"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->alphaTag:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, "\' \'"

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->number:Ljava/lang/String;

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, " "

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->emails:[Ljava/lang/String;

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, "\'"

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v0

    #@33
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 189
    iget v0, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->efid:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 190
    iget v0, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->recordNumber:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 191
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->alphaTag:Ljava/lang/String;

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 192
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->number:Ljava/lang/String;

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 193
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/AdnRecord;->emails:[Ljava/lang/String;

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@19
    .line 194
    return-void
.end method
