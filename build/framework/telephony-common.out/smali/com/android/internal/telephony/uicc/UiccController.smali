.class public Lcom/android/internal/telephony/uicc/UiccController;
.super Landroid/os/Handler;
.source "UiccController.java"


# static fields
.field public static final APP_FAM_3GPP:I = 0x1

.field public static final APP_FAM_3GPP2:I = 0x2

.field public static final APP_FAM_IMS:I = 0x3

.field protected static final DBG:Z = true

.field protected static final EVENT_GET_ICC_STATUS_DONE:I = 0x2

.field protected static final EVENT_ICC_STATUS_CHANGED:I = 0x1

.field private static final EVENT_RADIO_AVAILABLE:I = 0x5

.field private static final EVENT_RADIO_UNAVAILABLE:I = 0x3

.field private static final INTENT_GBA_INIT:Ljava/lang/String; = "com.movial.gba_initialized"

.field protected static final LOG_TAG:Ljava/lang/String; = "RIL_UiccController"

.field protected static mInstance:Lcom/android/internal/telephony/uicc/UiccController;

.field protected static final mLock:Ljava/lang/Object;


# instance fields
.field private isBoot:Z

.field private mCi:Lcom/android/internal/telephony/CommandsInterface;

.field protected mContext:Landroid/content/Context;

.field protected mIccChangedRegistrants:Landroid/os/RegistrantList;

.field protected mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 100
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Lcom/android/internal/telephony/uicc/UiccController;->mLock:Ljava/lang/Object;

    #@7
    return-void
.end method

.method protected constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 317
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@3
    .line 107
    new-instance v0, Landroid/os/RegistrantList;

    #@5
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/UiccController;->mIccChangedRegistrants:Landroid/os/RegistrantList;

    #@a
    .line 110
    const/4 v0, 0x1

    #@b
    iput-boolean v0, p0, Lcom/android/internal/telephony/uicc/UiccController;->isBoot:Z

    #@d
    .line 318
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)V
    .registers 6
    .parameter "c"
    .parameter "ci"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 263
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    .line 107
    new-instance v0, Landroid/os/RegistrantList;

    #@7
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@a
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/UiccController;->mIccChangedRegistrants:Landroid/os/RegistrantList;

    #@c
    .line 110
    iput-boolean v1, p0, Lcom/android/internal/telephony/uicc/UiccController;->isBoot:Z

    #@e
    .line 264
    const-string v0, "Creating UiccController"

    #@10
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/UiccController;->log(Ljava/lang/String;)V

    #@13
    .line 265
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/UiccController;->mContext:Landroid/content/Context;

    #@15
    .line 266
    iput-object p2, p0, Lcom/android/internal/telephony/uicc/UiccController;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@17
    .line 267
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccController;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@19
    invoke-interface {v0, p0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->registerForIccStatusChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@1c
    .line 269
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccController;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@1e
    invoke-interface {v0, p0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->registerForOn(Landroid/os/Handler;ILjava/lang/Object;)V

    #@21
    .line 274
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccController;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@23
    const/4 v1, 0x3

    #@24
    invoke-interface {v0, p0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->registerForNotAvailable(Landroid/os/Handler;ILjava/lang/Object;)V

    #@27
    .line 277
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccController;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@29
    const/4 v1, 0x5

    #@2a
    invoke-interface {v0, p0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->registerForAvailable(Landroid/os/Handler;ILjava/lang/Object;)V

    #@2d
    .line 280
    return-void
.end method

.method private declared-synchronized disposeCard()V
    .registers 2

    #@0
    .prologue
    .line 255
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "Disposing card"

    #@3
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/UiccController;->log(Ljava/lang/String;)V

    #@6
    .line 256
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccController;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@8
    if-eqz v0, :cond_17

    #@a
    .line 257
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccController;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@c
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/UiccCard;->dispose()V

    #@f
    .line 258
    const/4 v0, 0x0

    #@10
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/UiccController;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@12
    .line 259
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccController;->mIccChangedRegistrants:Landroid/os/RegistrantList;

    #@14
    invoke-virtual {v0}, Landroid/os/RegistrantList;->notifyRegistrants()V
    :try_end_17
    .catchall {:try_start_1 .. :try_end_17} :catchall_19

    #@17
    .line 261
    :cond_17
    monitor-exit p0

    #@18
    return-void

    #@19
    .line 255
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit p0

    #@1b
    throw v0
.end method

.method public static getInstance()Lcom/android/internal/telephony/uicc/UiccController;
    .registers 3

    #@0
    .prologue
    .line 124
    sget-object v1, Lcom/android/internal/telephony/uicc/UiccController;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 125
    :try_start_3
    sget-object v0, Lcom/android/internal/telephony/uicc/UiccController;->mInstance:Lcom/android/internal/telephony/uicc/UiccController;

    #@5
    if-nez v0, :cond_12

    #@7
    .line 126
    new-instance v0, Ljava/lang/RuntimeException;

    #@9
    const-string v2, "UiccController.getInstance can\'t be called before make()"

    #@b
    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 130
    :catchall_f
    move-exception v0

    #@10
    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    #@11
    throw v0

    #@12
    .line 129
    :cond_12
    :try_start_12
    sget-object v0, Lcom/android/internal/telephony/uicc/UiccController;->mInstance:Lcom/android/internal/telephony/uicc/UiccController;

    #@14
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_12 .. :try_end_15} :catchall_f

    #@15
    return-object v0
.end method

.method private log(Ljava/lang/String;)V
    .registers 3
    .parameter "string"

    #@0
    .prologue
    .line 321
    const-string v0, "RIL_UiccController"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 322
    return-void
.end method

.method public static make(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)Lcom/android/internal/telephony/uicc/UiccController;
    .registers 5
    .parameter "c"
    .parameter "ci"

    #@0
    .prologue
    .line 114
    sget-object v1, Lcom/android/internal/telephony/uicc/UiccController;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 115
    :try_start_3
    sget-object v0, Lcom/android/internal/telephony/uicc/UiccController;->mInstance:Lcom/android/internal/telephony/uicc/UiccController;

    #@5
    if-eqz v0, :cond_12

    #@7
    .line 116
    new-instance v0, Ljava/lang/RuntimeException;

    #@9
    const-string v2, "UiccController.make() should only be called once"

    #@b
    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 120
    :catchall_f
    move-exception v0

    #@10
    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    #@11
    throw v0

    #@12
    .line 118
    :cond_12
    :try_start_12
    new-instance v0, Lcom/android/internal/telephony/uicc/UiccController;

    #@14
    invoke-direct {v0, p0, p1}, Lcom/android/internal/telephony/uicc/UiccController;-><init>(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)V

    #@17
    sput-object v0, Lcom/android/internal/telephony/uicc/UiccController;->mInstance:Lcom/android/internal/telephony/uicc/UiccController;

    #@19
    .line 119
    sget-object v0, Lcom/android/internal/telephony/uicc/UiccController;->mInstance:Lcom/android/internal/telephony/uicc/UiccController;

    #@1b
    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_12 .. :try_end_1c} :catchall_f

    #@1c
    return-object v0
.end method

.method private declared-synchronized onGetIccCardStatusDone(Landroid/os/AsyncResult;)V
    .registers 6
    .parameter "ar"

    #@0
    .prologue
    .line 283
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v1, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@3
    if-eqz v1, :cond_10

    #@5
    .line 284
    const-string v1, "RIL_UiccController"

    #@7
    const-string v2, "Error getting ICC status. RIL_REQUEST_GET_ICC_STATUS should never return an error"

    #@9
    iget-object v3, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@b
    invoke-static {v1, v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_40

    #@e
    .line 315
    :goto_e
    monitor-exit p0

    #@f
    return-void

    #@10
    .line 290
    :cond_10
    :try_start_10
    iget-object v0, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@12
    check-cast v0, Lcom/android/internal/telephony/uicc/IccCardStatus;

    #@14
    .line 292
    .local v0, status:Lcom/android/internal/telephony/uicc/IccCardStatus;
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccController;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@16
    if-nez v1, :cond_43

    #@18
    .line 294
    const-string v1, "Creating a new card"

    #@1a
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/uicc/UiccController;->log(Ljava/lang/String;)V

    #@1d
    .line 295
    new-instance v1, Lcom/android/internal/telephony/uicc/UiccCard;

    #@1f
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccController;->mContext:Landroid/content/Context;

    #@21
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccController;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@23
    invoke-direct {v1, v2, v3, v0}, Lcom/android/internal/telephony/uicc/UiccCard;-><init>(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Lcom/android/internal/telephony/uicc/IccCardStatus;)V

    #@26
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/UiccController;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@28
    .line 303
    :goto_28
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccController;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@2a
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCard;->getCardState()Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@2d
    move-result-object v1

    #@2e
    sget-object v2, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_ABSENT:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@30
    if-ne v1, v2, :cond_52

    #@32
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccController;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@34
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCard;->getNumApplications()I

    #@37
    move-result v1

    #@38
    if-eqz v1, :cond_52

    #@3a
    .line 305
    const-string v1, "[LGE_UICC] When ME power off, mIccChangedRegistrants didin\'t notify"

    #@3c
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/uicc/UiccController;->log(Ljava/lang/String;)V
    :try_end_3f
    .catchall {:try_start_10 .. :try_end_3f} :catchall_40

    #@3f
    goto :goto_e

    #@40
    .line 283
    .end local v0           #status:Lcom/android/internal/telephony/uicc/IccCardStatus;
    :catchall_40
    move-exception v1

    #@41
    monitor-exit p0

    #@42
    throw v1

    #@43
    .line 298
    .restart local v0       #status:Lcom/android/internal/telephony/uicc/IccCardStatus;
    :cond_43
    :try_start_43
    const-string v1, "Update already existing card"

    #@45
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/uicc/UiccController;->log(Ljava/lang/String;)V

    #@48
    .line 299
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccController;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@4a
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccController;->mContext:Landroid/content/Context;

    #@4c
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccController;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@4e
    invoke-virtual {v1, v2, v3, v0}, Lcom/android/internal/telephony/uicc/UiccCard;->update(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Lcom/android/internal/telephony/uicc/IccCardStatus;)V

    #@51
    goto :goto_28

    #@52
    .line 310
    :cond_52
    const-string v1, "[LGE_UICC] Notifying IccChangedRegistrants"

    #@54
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/uicc/UiccController;->log(Ljava/lang/String;)V

    #@57
    .line 311
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccController;->mIccChangedRegistrants:Landroid/os/RegistrantList;

    #@59
    invoke-virtual {v1}, Landroid/os/RegistrantList;->notifyRegistrants()V
    :try_end_5c
    .catchall {:try_start_43 .. :try_end_5c} :catchall_40

    #@5c
    goto :goto_e
.end method


# virtual methods
.method public getIccFileHandler(I)Lcom/android/internal/telephony/uicc/IccFileHandler;
    .registers 5
    .parameter "family"

    #@0
    .prologue
    .line 164
    sget-object v2, Lcom/android/internal/telephony/uicc/UiccController;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 165
    :try_start_3
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccController;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@5
    if-eqz v1, :cond_15

    #@7
    .line 166
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccController;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@9
    invoke-virtual {v1, p1}, Lcom/android/internal/telephony/uicc/UiccCard;->getApplication(I)Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@c
    move-result-object v0

    #@d
    .line 167
    .local v0, app:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    if-eqz v0, :cond_15

    #@f
    .line 168
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getIccFileHandler()Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@12
    move-result-object v1

    #@13
    monitor-exit v2

    #@14
    .line 171
    .end local v0           #app:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    :goto_14
    return-object v1

    #@15
    :cond_15
    const/4 v1, 0x0

    #@16
    monitor-exit v2

    #@17
    goto :goto_14

    #@18
    .line 172
    :catchall_18
    move-exception v1

    #@19
    monitor-exit v2
    :try_end_1a
    .catchall {:try_start_3 .. :try_end_1a} :catchall_18

    #@1a
    throw v1
.end method

.method public getIccRecords(I)Lcom/android/internal/telephony/uicc/IccRecords;
    .registers 5
    .parameter "family"

    #@0
    .prologue
    .line 151
    sget-object v2, Lcom/android/internal/telephony/uicc/UiccController;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 152
    :try_start_3
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccController;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@5
    if-eqz v1, :cond_15

    #@7
    .line 153
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccController;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@9
    invoke-virtual {v1, p1}, Lcom/android/internal/telephony/uicc/UiccCard;->getApplication(I)Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@c
    move-result-object v0

    #@d
    .line 154
    .local v0, app:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    if-eqz v0, :cond_15

    #@f
    .line 155
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getIccRecords()Lcom/android/internal/telephony/uicc/IccRecords;

    #@12
    move-result-object v1

    #@13
    monitor-exit v2

    #@14
    .line 158
    .end local v0           #app:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    :goto_14
    return-object v1

    #@15
    :cond_15
    const/4 v1, 0x0

    #@16
    monitor-exit v2

    #@17
    goto :goto_14

    #@18
    .line 159
    :catchall_18
    move-exception v1

    #@19
    monitor-exit v2
    :try_end_1a
    .catchall {:try_start_3 .. :try_end_1a} :catchall_18

    #@1a
    throw v1
.end method

.method public getUiccCard()Lcom/android/internal/telephony/uicc/UiccCard;
    .registers 3

    #@0
    .prologue
    .line 134
    sget-object v1, Lcom/android/internal/telephony/uicc/UiccController;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 135
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccController;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 136
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public getUiccCardApplication(I)Lcom/android/internal/telephony/uicc/UiccCardApplication;
    .registers 4
    .parameter "family"

    #@0
    .prologue
    .line 141
    sget-object v1, Lcom/android/internal/telephony/uicc/UiccController;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 142
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccController;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@5
    if-eqz v0, :cond_f

    #@7
    .line 143
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccController;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@9
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/uicc/UiccCard;->getApplication(I)Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@c
    move-result-object v0

    #@d
    monitor-exit v1

    #@e
    .line 145
    :goto_e
    return-object v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    monitor-exit v1

    #@11
    goto :goto_e

    #@12
    .line 146
    :catchall_12
    move-exception v0

    #@13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 8
    .parameter "msg"

    #@0
    .prologue
    .line 194
    sget-object v3, Lcom/android/internal/telephony/uicc/UiccController;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 195
    :try_start_3
    iget v2, p1, Landroid/os/Message;->what:I

    #@5
    packed-switch v2, :pswitch_data_dc

    #@8
    .line 248
    :pswitch_8
    const-string v2, "RIL_UiccController"

    #@a
    new-instance v4, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v5, " Unknown Event "

    #@11
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v4

    #@15
    iget v5, p1, Landroid/os/Message;->what:I

    #@17
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 250
    :cond_22
    :goto_22
    monitor-exit v3

    #@23
    .line 251
    return-void

    #@24
    .line 197
    :pswitch_24
    const-string v2, "Received EVENT_ICC_STATUS_CHANGED, calling getIccCardStatus"

    #@26
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/uicc/UiccController;->log(Ljava/lang/String;)V

    #@29
    .line 198
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccController;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@2b
    const/4 v4, 0x2

    #@2c
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/uicc/UiccController;->obtainMessage(I)Landroid/os/Message;

    #@2f
    move-result-object v4

    #@30
    invoke-interface {v2, v4}, Lcom/android/internal/telephony/CommandsInterface;->getIccCardStatus(Landroid/os/Message;)V

    #@33
    goto :goto_22

    #@34
    .line 250
    :catchall_34
    move-exception v2

    #@35
    monitor-exit v3
    :try_end_36
    .catchall {:try_start_3 .. :try_end_36} :catchall_34

    #@36
    throw v2

    #@37
    .line 201
    :pswitch_37
    :try_start_37
    const-string v2, "Received EVENT_GET_ICC_STATUS_DONE"

    #@39
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/uicc/UiccController;->log(Ljava/lang/String;)V

    #@3c
    .line 202
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3e
    check-cast v0, Landroid/os/AsyncResult;

    #@40
    .line 203
    .local v0, ar:Landroid/os/AsyncResult;
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/UiccController;->onGetIccCardStatusDone(Landroid/os/AsyncResult;)V

    #@43
    .line 206
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@46
    move-result-object v2

    #@47
    const-string v4, "TMO"

    #@49
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4c
    move-result v2

    #@4d
    if-eqz v2, :cond_80

    #@4f
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@52
    move-result-object v2

    #@53
    const-string v4, "US"

    #@55
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@58
    move-result v2

    #@59
    if-eqz v2, :cond_80

    #@5b
    .line 208
    iget-boolean v2, p0, Lcom/android/internal/telephony/uicc/UiccController;->isBoot:Z

    #@5d
    if-eqz v2, :cond_80

    #@5f
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccController;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@61
    if-eqz v2, :cond_80

    #@63
    .line 210
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccController;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@65
    invoke-virtual {v2}, Lcom/android/internal/telephony/uicc/UiccCard;->getCardState()Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@68
    move-result-object v2

    #@69
    sget-object v4, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_PRESENT:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@6b
    if-eq v2, v4, :cond_84

    #@6d
    .line 212
    const-string v2, "RIL_UiccController"

    #@6f
    const-string v4, "[ISIM] Send Intent - IPUtils.INTENT_GBA_INIT"

    #@71
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@74
    .line 213
    new-instance v1, Landroid/content/Intent;

    #@76
    const-string v2, "com.movial.gba_initialized"

    #@78
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7b
    .line 214
    .local v1, intent:Landroid/content/Intent;
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccController;->mContext:Landroid/content/Context;

    #@7d
    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@80
    .line 225
    .end local v1           #intent:Landroid/content/Intent;
    :cond_80
    :goto_80
    const/4 v2, 0x0

    #@81
    iput-boolean v2, p0, Lcom/android/internal/telephony/uicc/UiccController;->isBoot:Z

    #@83
    goto :goto_22

    #@84
    .line 216
    :cond_84
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccController;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@86
    invoke-virtual {v2}, Lcom/android/internal/telephony/uicc/UiccCard;->getCardState()Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@89
    move-result-object v2

    #@8a
    sget-object v4, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_PRESENT:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@8c
    if-ne v2, v4, :cond_80

    #@8e
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccController;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@90
    const/4 v4, 0x3

    #@91
    invoke-virtual {v2, v4}, Lcom/android/internal/telephony/uicc/UiccCard;->getApplication(I)Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@94
    move-result-object v2

    #@95
    if-nez v2, :cond_80

    #@97
    .line 218
    const-string v2, "RIL_UiccController"

    #@99
    const-string v4, "[ISIM] Send Intent - IPUtils.INTENT_GBA_INIT"

    #@9b
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9e
    .line 219
    new-instance v1, Landroid/content/Intent;

    #@a0
    const-string v2, "com.movial.gba_initialized"

    #@a2
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@a5
    .line 220
    .restart local v1       #intent:Landroid/content/Intent;
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccController;->mContext:Landroid/content/Context;

    #@a7
    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@aa
    goto :goto_80

    #@ab
    .line 230
    .end local v0           #ar:Landroid/os/AsyncResult;
    .end local v1           #intent:Landroid/content/Intent;
    :pswitch_ab
    const-string v2, "EVENT_RADIO_UNAVAILABLE "

    #@ad
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/uicc/UiccController;->log(Ljava/lang/String;)V

    #@b0
    .line 231
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UiccController;->disposeCard()V

    #@b3
    goto/16 :goto_22

    #@b5
    .line 236
    :pswitch_b5
    const-string v2, "RIL_UiccController"

    #@b7
    const-string v4, "EVENT_RADIO_AVAILABLE on index "

    #@b9
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@bc
    .line 239
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccController;->mContext:Landroid/content/Context;

    #@be
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/UiccController;->isAirplaneModeOn(Landroid/content/Context;)Z

    #@c1
    move-result v2

    #@c2
    if-eqz v2, :cond_22

    #@c4
    iget-boolean v2, p0, Lcom/android/internal/telephony/uicc/UiccController;->isBoot:Z

    #@c6
    if-eqz v2, :cond_22

    #@c8
    .line 240
    const-string v2, "RIL_UiccController"

    #@ca
    const-string v4, "[pjman] Airplane mode is on. Forcing sim status update for WiFi"

    #@cc
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@cf
    .line 241
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccController;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@d1
    const/4 v4, 0x2

    #@d2
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/uicc/UiccController;->obtainMessage(I)Landroid/os/Message;

    #@d5
    move-result-object v4

    #@d6
    invoke-interface {v2, v4}, Lcom/android/internal/telephony/CommandsInterface;->getIccCardStatus(Landroid/os/Message;)V
    :try_end_d9
    .catchall {:try_start_37 .. :try_end_d9} :catchall_34

    #@d9
    goto/16 :goto_22

    #@db
    .line 195
    nop

    #@dc
    :pswitch_data_dc
    .packed-switch 0x1
        :pswitch_24
        :pswitch_37
        :pswitch_ab
        :pswitch_8
        :pswitch_b5
    .end packed-switch
.end method

.method public isAirplaneModeOn(Landroid/content/Context;)Z
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 326
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4
    move-result-object v1

    #@5
    const-string v2, "airplane_mode_on"

    #@7
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_e

    #@d
    const/4 v0, 0x1

    #@e
    :cond_e
    return v0
.end method

.method public registerForIccChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 7
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 177
    sget-object v2, Lcom/android/internal/telephony/uicc/UiccController;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 178
    :try_start_3
    new-instance v0, Landroid/os/Registrant;

    #@5
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@8
    .line 179
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccController;->mIccChangedRegistrants:Landroid/os/RegistrantList;

    #@a
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@d
    .line 182
    invoke-virtual {v0}, Landroid/os/Registrant;->notifyRegistrant()V

    #@10
    .line 183
    monitor-exit v2

    #@11
    .line 184
    return-void

    #@12
    .line 183
    .end local v0           #r:Landroid/os/Registrant;
    :catchall_12
    move-exception v1

    #@13
    monitor-exit v2
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    #@14
    throw v1
.end method

.method public unregisterForIccChanged(Landroid/os/Handler;)V
    .registers 4
    .parameter "h"

    #@0
    .prologue
    .line 187
    sget-object v1, Lcom/android/internal/telephony/uicc/UiccController;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 188
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccController;->mIccChangedRegistrants:Landroid/os/RegistrantList;

    #@5
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@8
    .line 189
    monitor-exit v1

    #@9
    .line 190
    return-void

    #@a
    .line 189
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method
