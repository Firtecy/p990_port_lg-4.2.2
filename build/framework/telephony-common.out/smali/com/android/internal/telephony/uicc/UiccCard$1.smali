.class Lcom/android/internal/telephony/uicc/UiccCard$1;
.super Landroid/os/Handler;
.source "UiccCard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/uicc/UiccCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/uicc/UiccCard;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/uicc/UiccCard;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 461
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/UiccCard$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCard;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 9
    .parameter "msg"

    #@0
    .prologue
    .line 464
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/UiccCard$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCard;

    #@2
    invoke-static {v4}, Lcom/android/internal/telephony/uicc/UiccCard;->access$000(Lcom/android/internal/telephony/uicc/UiccCard;)Z

    #@5
    move-result v4

    #@6
    if-eqz v4, :cond_33

    #@8
    .line 465
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/UiccCard$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCard;

    #@a
    new-instance v5, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v6, "Received message "

    #@11
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v5

    #@15
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v5

    #@19
    const-string v6, "["

    #@1b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v5

    #@1f
    iget v6, p1, Landroid/os/Message;->what:I

    #@21
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v5

    #@25
    const-string v6, "] while being destroyed. Ignoring."

    #@27
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v5

    #@2b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v5

    #@2f
    invoke-static {v4, v5}, Lcom/android/internal/telephony/uicc/UiccCard;->access$100(Lcom/android/internal/telephony/uicc/UiccCard;Ljava/lang/String;)V

    #@32
    .line 520
    :cond_32
    :goto_32
    return-void

    #@33
    .line 470
    :cond_33
    iget v4, p1, Landroid/os/Message;->what:I

    #@35
    packed-switch v4, :pswitch_data_ce

    #@38
    .line 518
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/UiccCard$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCard;

    #@3a
    new-instance v5, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v6, "Unknown Event "

    #@41
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v5

    #@45
    iget v6, p1, Landroid/os/Message;->what:I

    #@47
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v5

    #@4b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v5

    #@4f
    invoke-static {v4, v5}, Lcom/android/internal/telephony/uicc/UiccCard;->access$100(Lcom/android/internal/telephony/uicc/UiccCard;Ljava/lang/String;)V

    #@52
    goto :goto_32

    #@53
    .line 474
    :pswitch_53
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@56
    move-result-object v4

    #@57
    const-string v5, "SKT"

    #@59
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5c
    move-result v4

    #@5d
    if-nez v4, :cond_32

    #@5f
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@62
    move-result-object v4

    #@63
    const-string v5, "KT"

    #@65
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@68
    move-result v4

    #@69
    if-nez v4, :cond_32

    #@6b
    .line 478
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/UiccCard$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCard;

    #@6d
    const/4 v5, 0x0

    #@6e
    invoke-static {v4, v5}, Lcom/android/internal/telephony/uicc/UiccCard;->access$200(Lcom/android/internal/telephony/uicc/UiccCard;Z)V

    #@71
    goto :goto_32

    #@72
    .line 483
    :pswitch_72
    new-instance v1, Landroid/content/Intent;

    #@74
    const-string v4, "android.intent.action.SIM_STATE_CHANGED"

    #@76
    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@79
    .line 484
    .local v1, intent:Landroid/content/Intent;
    const/high16 v4, 0x2000

    #@7b
    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@7e
    .line 485
    const-string v4, "phoneName"

    #@80
    const-string v5, "Phone"

    #@82
    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@85
    .line 486
    const-string v4, "ss"

    #@87
    const-string v5, "SIM_INSERTED"

    #@89
    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@8c
    .line 489
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/UiccCard$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCard;

    #@8e
    const-string v5, "Broadcasting intent ACTION_SIM_STATE_CHANGED SIM_INSERTED"

    #@90
    invoke-virtual {v4, v5}, Lcom/android/internal/telephony/uicc/UiccCard;->log(Ljava/lang/String;)V

    #@93
    .line 490
    const-string v4, "android.permission.READ_PHONE_STATE"

    #@95
    const/4 v5, -0x1

    #@96
    invoke-static {v1, v4, v5}, Landroid/app/ActivityManagerNative;->broadcastStickyIntent(Landroid/content/Intent;Ljava/lang/String;I)V

    #@99
    goto :goto_32

    #@9a
    .line 500
    .end local v1           #intent:Landroid/content/Intent;
    :pswitch_9a
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9c
    if-eqz v4, :cond_32

    #@9e
    .line 501
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a0
    check-cast v0, Landroid/app/ProgressDialog;

    #@a2
    .line 503
    .local v0, dialog:Landroid/app/ProgressDialog;
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getProgress()I

    #@a5
    move-result v3

    #@a6
    .line 505
    .local v3, progress:I
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getMax()I

    #@a9
    move-result v4

    #@aa
    if-ge v3, v4, :cond_ba

    #@ac
    .line 506
    add-int/lit8 v3, v3, 0x1

    #@ae
    .line 507
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setProgress(I)V

    #@b1
    .line 508
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/UiccCard$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCard;

    #@b3
    const-wide/16 v5, 0x3e8

    #@b5
    invoke-static {v4, v5, v6, v0}, Lcom/android/internal/telephony/uicc/UiccCard;->access$300(Lcom/android/internal/telephony/uicc/UiccCard;JLandroid/app/ProgressDialog;)V

    #@b8
    goto/16 :goto_32

    #@ba
    .line 510
    :cond_ba
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/UiccCard$1;->this$0:Lcom/android/internal/telephony/uicc/UiccCard;

    #@bc
    iget-object v4, v4, Lcom/android/internal/telephony/uicc/UiccCard;->mContext:Landroid/content/Context;

    #@be
    const-string v5, "power"

    #@c0
    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@c3
    move-result-object v2

    #@c4
    check-cast v2, Landroid/os/PowerManager;

    #@c6
    .line 512
    .local v2, pm:Landroid/os/PowerManager;
    const-string v4, "Phone reboot."

    #@c8
    invoke-virtual {v2, v4}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    #@cb
    goto/16 :goto_32

    #@cd
    .line 470
    nop

    #@ce
    :pswitch_data_ce
    .packed-switch 0xd
        :pswitch_53
        :pswitch_72
        :pswitch_9a
    .end packed-switch
.end method
