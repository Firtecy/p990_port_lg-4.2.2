.class public Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;
.super Lcom/android/internal/telephony/uicc/IUsimInfoCallback$Stub;
.source "IccPhonebookProviderBackendImp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "qctInfo"
.end annotation


# static fields
.field private static final PBM_FILE_TYPE_COMPARE_MASK:I = 0xf


# instance fields
.field private mAsyncCallSynchronizer:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentSliceIndex:I

.field private mGlobalInfo:[Ljava/lang/Object;

.field private mRequestEf:I

.field private mSliceInfo:[[Ljava/lang/Object;

.field final synthetic this$0:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;)V
    .registers 5
    .parameter

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 576
    iput-object p1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->this$0:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;

    #@3
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/IUsimInfoCallback$Stub;-><init>()V

    #@6
    .line 582
    iput v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mRequestEf:I

    #@8
    .line 583
    iput v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mCurrentSliceIndex:I

    #@a
    .line 584
    new-instance v0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;

    #@c
    iget-object v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->this$0:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;

    #@e
    const/4 v2, 0x0

    #@f
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;-><init>(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$1;)V

    #@12
    iput-object v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mAsyncCallSynchronizer:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;

    #@14
    return-void
.end method

.method static synthetic access$100(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;Landroid/content/Context;)Landroid/database/Cursor;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 576
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->read(Landroid/content/Context;)Landroid/database/Cursor;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private dumpNativeInfo(Ljava/lang/String;Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;)V
    .registers 8
    .parameter "description"
    .parameter "info"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 859
    const-string v1, "IccPhonebookProvider"

    #@3
    const-string v2, "<----------------------------------------------------"

    #@5
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 860
    const-string v1, "IccPhonebookProvider"

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "|"

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 861
    const-string v1, "IccPhonebookProvider"

    #@22
    new-instance v2, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v3, "|device="

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    iget v3, p2, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->device:I

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 862
    const-string v1, "IccPhonebookProvider"

    #@3c
    new-instance v2, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v3, "|file_type="

    #@43
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    iget v3, p2, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->file_type:I

    #@49
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v2

    #@4d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v2

    #@51
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    .line 863
    const-string v1, "IccPhonebookProvider"

    #@56
    new-instance v2, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v3, "|max_num_length="

    #@5d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v2

    #@61
    iget v3, p2, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->max_num_length:I

    #@63
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@66
    move-result-object v2

    #@67
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v2

    #@6b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    .line 864
    const-string v1, "IccPhonebookProvider"

    #@70
    new-instance v2, Ljava/lang/StringBuilder;

    #@72
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@75
    const-string v3, "|max_text_length="

    #@77
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v2

    #@7b
    iget v3, p2, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->max_text_length:I

    #@7d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@80
    move-result-object v2

    #@81
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@84
    move-result-object v2

    #@85
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@88
    .line 865
    const-string v1, "IccPhonebookProvider"

    #@8a
    new-instance v2, Ljava/lang/StringBuilder;

    #@8c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8f
    const-string v3, "|num_of_free_ext="

    #@91
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v2

    #@95
    iget v3, p2, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->num_of_free_ext:I

    #@97
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v2

    #@9b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9e
    move-result-object v2

    #@9f
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a2
    .line 866
    const-string v1, "IccPhonebookProvider"

    #@a4
    new-instance v2, Ljava/lang/StringBuilder;

    #@a6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a9
    const-string v3, "|num_of_tot_rec="

    #@ab
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v2

    #@af
    iget v3, p2, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->num_of_tot_rec:I

    #@b1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v2

    #@b5
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b8
    move-result-object v2

    #@b9
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@bc
    .line 867
    const-string v1, "IccPhonebookProvider"

    #@be
    new-instance v2, Ljava/lang/StringBuilder;

    #@c0
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c3
    const-string v3, "|num_of_used_rec="

    #@c5
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v2

    #@c9
    iget v3, p2, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->num_of_used_rec:I

    #@cb
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v2

    #@cf
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d2
    move-result-object v2

    #@d3
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d6
    .line 868
    const-string v1, "IccPhonebookProvider"

    #@d8
    new-instance v2, Ljava/lang/StringBuilder;

    #@da
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@dd
    const-string v3, "|RECORD_INFO_MAX_PB_SETS="

    #@df
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v2

    #@e3
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@e6
    const/16 v3, 0x8

    #@e8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v2

    #@ec
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ef
    move-result-object v2

    #@f0
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f3
    .line 869
    const-string v1, "IccPhonebookProvider"

    #@f5
    new-instance v2, Ljava/lang/StringBuilder;

    #@f7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@fa
    const-string v3, "|status="

    #@fc
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v2

    #@100
    iget v3, p2, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->status:I

    #@102
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@105
    move-result-object v2

    #@106
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@109
    move-result-object v2

    #@10a
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10d
    .line 870
    const-string v1, "IccPhonebookProvider"

    #@10f
    new-instance v2, Ljava/lang/StringBuilder;

    #@111
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@114
    const-string v3, "|CONTENTS_FILE_DESCRIPTOR="

    #@116
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@119
    move-result-object v2

    #@11a
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v2

    #@11e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@121
    move-result-object v2

    #@122
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@125
    .line 871
    const-string v1, "IccPhonebookProvider"

    #@127
    new-instance v2, Ljava/lang/StringBuilder;

    #@129
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12c
    const-string v3, "|ARCELABLE_WRITE_RETURN_VALUE="

    #@12e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@131
    move-result-object v2

    #@132
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@135
    move-result-object v2

    #@136
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@139
    move-result-object v2

    #@13a
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@13d
    .line 872
    iget-object v1, p2, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->records_in_pb_set:[I

    #@13f
    if-eqz v1, :cond_196

    #@141
    .line 873
    const/4 v0, 0x0

    #@142
    .local v0, index:I
    :goto_142
    iget-object v1, p2, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->records_in_pb_set:[I

    #@144
    array-length v1, v1

    #@145
    if-ge v0, v1, :cond_196

    #@147
    .line 874
    const-string v1, "IccPhonebookProvider"

    #@149
    new-instance v2, Ljava/lang/StringBuilder;

    #@14b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@14e
    const-string v3, "| records_in_pb_set["

    #@150
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@153
    move-result-object v2

    #@154
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@157
    move-result-object v2

    #@158
    const-string v3, "]="

    #@15a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15d
    move-result-object v2

    #@15e
    iget-object v3, p2, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->records_in_pb_set:[I

    #@160
    aget v3, v3, v0

    #@162
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@165
    move-result-object v2

    #@166
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@169
    move-result-object v2

    #@16a
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16d
    .line 875
    const-string v1, "IccPhonebookProvider"

    #@16f
    new-instance v2, Ljava/lang/StringBuilder;

    #@171
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@174
    const-string v3, "| used_records_in_pb_set["

    #@176
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@179
    move-result-object v2

    #@17a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17d
    move-result-object v2

    #@17e
    const-string v3, "]="

    #@180
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@183
    move-result-object v2

    #@184
    iget-object v3, p2, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->used_records_in_pb_set:[I

    #@186
    aget v3, v3, v0

    #@188
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18b
    move-result-object v2

    #@18c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18f
    move-result-object v2

    #@190
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@193
    .line 873
    add-int/lit8 v0, v0, 0x1

    #@195
    goto :goto_142

    #@196
    .line 878
    .end local v0           #index:I
    :cond_196
    const-string v1, "IccPhonebookProvider"

    #@198
    const-string v2, "---------------------------------------------------->"

    #@19a
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19d
    .line 879
    return-void
.end method

.method private handleResult(Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;)Z
    .registers 4
    .parameter "info"

    #@0
    .prologue
    .line 630
    const/4 v0, 0x0

    #@1
    .line 631
    .local v0, hasMoreRequest:Z
    iget v1, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mRequestEf:I

    #@3
    packed-switch v1, :pswitch_data_1c

    #@6
    .line 662
    :goto_6
    :pswitch_6
    return v0

    #@7
    .line 633
    :pswitch_7
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->onAdn(Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;)Z

    #@a
    move-result v0

    #@b
    .line 634
    goto :goto_6

    #@c
    .line 643
    :pswitch_c
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->onAnr(Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;)Z

    #@f
    move-result v0

    #@10
    .line 644
    goto :goto_6

    #@11
    .line 653
    :pswitch_11
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->onEmail(Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;)Z

    #@14
    move-result v0

    #@15
    .line 654
    goto :goto_6

    #@16
    .line 659
    :pswitch_16
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->onGas(Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;)Z

    #@19
    move-result v0

    #@1a
    goto :goto_6

    #@1b
    .line 631
    nop

    #@1c
    :pswitch_data_1c
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_16
    .end packed-switch
.end method

.method private onAdn(Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;)Z
    .registers 10
    .parameter "info"

    #@0
    .prologue
    const/16 v7, 0x14

    #@2
    const/16 v6, 0xf

    #@4
    const/4 v2, 0x1

    #@5
    const/4 v1, 0x0

    #@6
    .line 668
    iget v3, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->status:I

    #@8
    if-nez v3, :cond_e

    #@a
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->records_in_pb_set:[I

    #@c
    if-nez v3, :cond_34

    #@e
    .line 669
    :cond_e
    const-string v2, "IccPhonebookProvider"

    #@10
    new-instance v3, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v4, "onAdn:end with status="

    #@17
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    iget v4, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->status:I

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    const-string v4, ")"

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 670
    const-string v2, "passed info is"

    #@30
    invoke-direct {p0, v2, p1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->dumpNativeInfo(Ljava/lang/String;Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;)V

    #@33
    .line 706
    :goto_33
    return v1

    #@34
    .line 674
    :cond_34
    iget v3, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->num_of_tot_rec:I

    #@36
    if-nez v3, :cond_40

    #@38
    .line 676
    const-string v2, "IccPhonebookProvider"

    #@3a
    const-string v3, "not ready!"

    #@3c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    goto :goto_33

    #@40
    .line 680
    :cond_40
    sget-object v3, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@42
    array-length v3, v3

    #@43
    new-array v3, v3, [Ljava/lang/Object;

    #@45
    iput-object v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mGlobalInfo:[Ljava/lang/Object;

    #@47
    .line 681
    iget-object v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mGlobalInfo:[Ljava/lang/Object;

    #@49
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4c
    move-result-object v4

    #@4d
    aput-object v4, v3, v6

    #@4f
    .line 682
    iget-object v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mGlobalInfo:[Ljava/lang/Object;

    #@51
    iget-object v4, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->records_in_pb_set:[I

    #@53
    array-length v4, v4

    #@54
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@57
    move-result-object v4

    #@58
    aput-object v4, v3, v1

    #@5a
    .line 683
    iget-object v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mGlobalInfo:[Ljava/lang/Object;

    #@5c
    const/16 v4, 0xc

    #@5e
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@61
    move-result-object v5

    #@62
    aput-object v5, v3, v4

    #@64
    .line 684
    iget-object v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mGlobalInfo:[Ljava/lang/Object;

    #@66
    const/16 v4, 0xd

    #@68
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6b
    move-result-object v5

    #@6c
    aput-object v5, v3, v4

    #@6e
    .line 685
    iget-object v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mGlobalInfo:[Ljava/lang/Object;

    #@70
    const/16 v4, 0xe

    #@72
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@75
    move-result-object v5

    #@76
    aput-object v5, v3, v4

    #@78
    .line 686
    iget-object v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mGlobalInfo:[Ljava/lang/Object;

    #@7a
    const-string v4, "GSMAlpha"

    #@7c
    aput-object v4, v3, v2

    #@7e
    .line 687
    iget-object v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mGlobalInfo:[Ljava/lang/Object;

    #@80
    const/4 v4, 0x2

    #@81
    iget v5, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->max_text_length:I

    #@83
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@86
    move-result-object v5

    #@87
    aput-object v5, v3, v4

    #@89
    .line 688
    iget-object v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mGlobalInfo:[Ljava/lang/Object;

    #@8b
    const/4 v4, 0x3

    #@8c
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8f
    move-result-object v5

    #@90
    aput-object v5, v3, v4

    #@92
    .line 689
    iget-object v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mGlobalInfo:[Ljava/lang/Object;

    #@94
    const/16 v4, 0x8

    #@96
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@99
    move-result-object v5

    #@9a
    aput-object v5, v3, v4

    #@9c
    .line 690
    iget-object v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mGlobalInfo:[Ljava/lang/Object;

    #@9e
    const/4 v4, 0x7

    #@9f
    iget v5, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->num_of_free_ext:I

    #@a1
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a4
    move-result-object v5

    #@a5
    aput-object v5, v3, v4

    #@a7
    .line 691
    iget-object v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mGlobalInfo:[Ljava/lang/Object;

    #@a9
    const/16 v4, 0xa

    #@ab
    const-string v5, ""

    #@ad
    aput-object v5, v3, v4

    #@af
    .line 692
    iget-object v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mGlobalInfo:[Ljava/lang/Object;

    #@b1
    const/16 v4, 0x9

    #@b3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b6
    move-result-object v5

    #@b7
    aput-object v5, v3, v4

    #@b9
    .line 693
    iget-object v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mGlobalInfo:[Ljava/lang/Object;

    #@bb
    const/16 v4, 0xb

    #@bd
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c0
    move-result-object v5

    #@c1
    aput-object v5, v3, v4

    #@c3
    .line 695
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->records_in_pb_set:[I

    #@c5
    array-length v3, v3

    #@c6
    new-array v3, v3, [[Ljava/lang/Object;

    #@c8
    iput-object v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mSliceInfo:[[Ljava/lang/Object;

    #@ca
    .line 697
    const/4 v0, 0x0

    #@cb
    .local v0, index:I
    :goto_cb
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->records_in_pb_set:[I

    #@cd
    array-length v3, v3

    #@ce
    if-ge v0, v3, :cond_ea

    #@d0
    .line 698
    iget-object v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mSliceInfo:[[Ljava/lang/Object;

    #@d2
    sget-object v4, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@d4
    array-length v4, v4

    #@d5
    new-array v4, v4, [Ljava/lang/Object;

    #@d7
    aput-object v4, v3, v0

    #@d9
    .line 699
    iget-object v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mSliceInfo:[[Ljava/lang/Object;

    #@db
    aget-object v3, v3, v0

    #@dd
    iget-object v4, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->records_in_pb_set:[I

    #@df
    aget v4, v4, v0

    #@e1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@e4
    move-result-object v4

    #@e5
    aput-object v4, v3, v1

    #@e7
    .line 697
    add-int/lit8 v0, v0, 0x1

    #@e9
    goto :goto_cb

    #@ea
    .line 702
    :cond_ea
    iput v6, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mRequestEf:I

    #@ec
    .line 704
    const-string v1, "onAdn"

    #@ee
    invoke-direct {p0, v1, p1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->dumpNativeInfo(Ljava/lang/String;Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;)V

    #@f1
    move v1, v2

    #@f2
    .line 706
    goto/16 :goto_33
.end method

.method private onAnr(Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;)Z
    .registers 11
    .parameter "info"

    #@0
    .prologue
    const/4 v8, 0x7

    #@1
    const/4 v7, 0x3

    #@2
    const/4 v6, 0x2

    #@3
    const/4 v2, 0x0

    #@4
    const/4 v1, 0x1

    #@5
    .line 712
    iget v3, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->status:I

    #@7
    if-eqz v3, :cond_37

    #@9
    .line 714
    const-string v3, "IccPhonebookProvider"

    #@b
    new-instance v4, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v5, "onAnr:end with status="

    #@12
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v4

    #@16
    iget v5, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->status:I

    #@18
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    const-string v5, ")"

    #@1e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v4

    #@26
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 715
    const-string v3, "passed info is"

    #@2b
    invoke-direct {p0, v3, p1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->dumpNativeInfo(Ljava/lang/String;Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;)V

    #@2e
    .line 717
    iput v8, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mRequestEf:I

    #@30
    .line 719
    iget v3, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->status:I

    #@32
    if-ne v3, v6, :cond_35

    #@34
    .line 755
    :goto_34
    return v1

    #@35
    :cond_35
    move v1, v2

    #@36
    .line 719
    goto :goto_34

    #@37
    .line 722
    :cond_37
    iget v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mRequestEf:I

    #@39
    packed-switch v3, :pswitch_data_f2

    #@3c
    .line 739
    :goto_3c
    iget v3, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->file_type:I

    #@3e
    and-int/lit8 v0, v3, 0xf

    #@40
    .line 740
    .local v0, anrType:I
    if-ne v0, v1, :cond_c4

    #@42
    .line 741
    iget-object v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mSliceInfo:[[Ljava/lang/Object;

    #@44
    iget v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mCurrentSliceIndex:I

    #@46
    aget-object v2, v2, v3

    #@48
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4b
    move-result-object v3

    #@4c
    aput-object v3, v2, v1

    #@4e
    .line 749
    :goto_4e
    iget-object v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mSliceInfo:[[Ljava/lang/Object;

    #@50
    iget v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mCurrentSliceIndex:I

    #@52
    aget-object v2, v2, v3

    #@54
    iget v3, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->num_of_tot_rec:I

    #@56
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@59
    move-result-object v3

    #@5a
    aput-object v3, v2, v6

    #@5c
    .line 751
    new-instance v2, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    const-string v3, "onAnr["

    #@63
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v2

    #@67
    iget v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mCurrentSliceIndex:I

    #@69
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v2

    #@6d
    const-string v3, "]"

    #@6f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v2

    #@73
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v2

    #@77
    invoke-direct {p0, v2, p1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->dumpNativeInfo(Ljava/lang/String;Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;)V

    #@7a
    .line 753
    iget v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mCurrentSliceIndex:I

    #@7c
    add-int/lit8 v2, v2, 0x1

    #@7e
    iput v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mCurrentSliceIndex:I

    #@80
    goto :goto_34

    #@81
    .line 724
    .end local v0           #anrType:I
    :pswitch_81
    iput v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mCurrentSliceIndex:I

    #@83
    .line 725
    iget-object v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mGlobalInfo:[Ljava/lang/Object;

    #@85
    const/4 v4, 0x4

    #@86
    const/16 v5, 0x14

    #@88
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8b
    move-result-object v5

    #@8c
    aput-object v5, v3, v4

    #@8e
    .line 726
    const/16 v3, 0x10

    #@90
    iput v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mRequestEf:I

    #@92
    .line 727
    iget-object v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mGlobalInfo:[Ljava/lang/Object;

    #@94
    const/16 v4, 0xc

    #@96
    iget-object v5, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->records_in_pb_set:[I

    #@98
    aget v5, v5, v2

    #@9a
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9d
    move-result-object v5

    #@9e
    aput-object v5, v3, v4

    #@a0
    goto :goto_3c

    #@a1
    .line 730
    :pswitch_a1
    const/16 v3, 0x11

    #@a3
    iput v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mRequestEf:I

    #@a5
    goto :goto_3c

    #@a6
    .line 731
    :pswitch_a6
    const/16 v3, 0x12

    #@a8
    iput v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mRequestEf:I

    #@aa
    goto :goto_3c

    #@ab
    .line 732
    :pswitch_ab
    const/16 v3, 0x13

    #@ad
    iput v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mRequestEf:I

    #@af
    goto :goto_3c

    #@b0
    .line 733
    :pswitch_b0
    const/16 v3, 0x14

    #@b2
    iput v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mRequestEf:I

    #@b4
    goto :goto_3c

    #@b5
    .line 734
    :pswitch_b5
    const/16 v3, 0x15

    #@b7
    iput v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mRequestEf:I

    #@b9
    goto :goto_3c

    #@ba
    .line 735
    :pswitch_ba
    const/16 v3, 0x16

    #@bc
    iput v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mRequestEf:I

    #@be
    goto/16 :goto_3c

    #@c0
    .line 736
    :pswitch_c0
    iput v8, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mRequestEf:I

    #@c2
    goto/16 :goto_3c

    #@c4
    .line 742
    .restart local v0       #anrType:I
    :cond_c4
    if-ne v0, v6, :cond_d4

    #@c6
    .line 743
    iget-object v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mSliceInfo:[[Ljava/lang/Object;

    #@c8
    iget v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mCurrentSliceIndex:I

    #@ca
    aget-object v2, v2, v3

    #@cc
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@cf
    move-result-object v3

    #@d0
    aput-object v3, v2, v1

    #@d2
    goto/16 :goto_4e

    #@d4
    .line 744
    :cond_d4
    if-ne v0, v7, :cond_e4

    #@d6
    .line 745
    iget-object v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mSliceInfo:[[Ljava/lang/Object;

    #@d8
    iget v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mCurrentSliceIndex:I

    #@da
    aget-object v2, v2, v3

    #@dc
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@df
    move-result-object v3

    #@e0
    aput-object v3, v2, v1

    #@e2
    goto/16 :goto_4e

    #@e4
    .line 747
    :cond_e4
    iget-object v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mSliceInfo:[[Ljava/lang/Object;

    #@e6
    iget v4, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mCurrentSliceIndex:I

    #@e8
    aget-object v3, v3, v4

    #@ea
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ed
    move-result-object v2

    #@ee
    aput-object v2, v3, v1

    #@f0
    goto/16 :goto_4e

    #@f2
    .line 722
    :pswitch_data_f2
    .packed-switch 0xf
        :pswitch_81
        :pswitch_a1
        :pswitch_a6
        :pswitch_ab
        :pswitch_b0
        :pswitch_b5
        :pswitch_ba
        :pswitch_c0
    .end packed-switch
.end method

.method private onEmail(Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;)Z
    .registers 11
    .parameter "info"

    #@0
    .prologue
    const/16 v8, 0x20

    #@2
    const/4 v7, 0x2

    #@3
    const/4 v2, 0x0

    #@4
    const/4 v1, 0x1

    #@5
    const/4 v6, 0x3

    #@6
    .line 761
    iget v3, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->status:I

    #@8
    if-eqz v3, :cond_38

    #@a
    .line 762
    const-string v3, "IccPhonebookProvider"

    #@c
    new-instance v4, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v5, "onEmail:end with status="

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    iget v5, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->status:I

    #@19
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v4

    #@1d
    const-string v5, ")"

    #@1f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v4

    #@27
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 763
    const-string v3, "passed info is"

    #@2c
    invoke-direct {p0, v3, p1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->dumpNativeInfo(Ljava/lang/String;Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;)V

    #@2f
    .line 766
    iput v8, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mRequestEf:I

    #@31
    .line 767
    iget v3, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->status:I

    #@33
    if-ne v3, v7, :cond_36

    #@35
    .line 802
    :goto_35
    return v1

    #@36
    :cond_36
    move v1, v2

    #@37
    .line 767
    goto :goto_35

    #@38
    .line 770
    :cond_38
    iget v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mRequestEf:I

    #@3a
    packed-switch v3, :pswitch_data_ea

    #@3d
    .line 786
    :goto_3d
    iget v3, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->file_type:I

    #@3f
    and-int/lit8 v0, v3, 0xf

    #@41
    .line 787
    .local v0, emailType:I
    if-ne v0, v1, :cond_bd

    #@43
    .line 788
    iget-object v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mSliceInfo:[[Ljava/lang/Object;

    #@45
    iget v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mCurrentSliceIndex:I

    #@47
    aget-object v2, v2, v3

    #@49
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4c
    move-result-object v3

    #@4d
    aput-object v3, v2, v6

    #@4f
    .line 796
    :goto_4f
    iget-object v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mSliceInfo:[[Ljava/lang/Object;

    #@51
    iget v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mCurrentSliceIndex:I

    #@53
    aget-object v2, v2, v3

    #@55
    const/4 v3, 0x4

    #@56
    iget v4, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->num_of_tot_rec:I

    #@58
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5b
    move-result-object v4

    #@5c
    aput-object v4, v2, v3

    #@5e
    .line 798
    new-instance v2, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v3, "onEmail["

    #@65
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v2

    #@69
    iget v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mCurrentSliceIndex:I

    #@6b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v2

    #@6f
    const-string v3, "]"

    #@71
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v2

    #@75
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v2

    #@79
    invoke-direct {p0, v2, p1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->dumpNativeInfo(Ljava/lang/String;Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;)V

    #@7c
    .line 800
    iget v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mCurrentSliceIndex:I

    #@7e
    add-int/lit8 v2, v2, 0x1

    #@80
    iput v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mCurrentSliceIndex:I

    #@82
    goto :goto_35

    #@83
    .line 772
    .end local v0           #emailType:I
    :pswitch_83
    iput v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mCurrentSliceIndex:I

    #@85
    .line 773
    iget-object v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mGlobalInfo:[Ljava/lang/Object;

    #@87
    const/4 v4, 0x6

    #@88
    iget v5, p1, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->max_text_length:I

    #@8a
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8d
    move-result-object v5

    #@8e
    aput-object v5, v3, v4

    #@90
    .line 774
    iget-object v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mGlobalInfo:[Ljava/lang/Object;

    #@92
    const/4 v4, 0x5

    #@93
    const-string v5, "GSM7"

    #@95
    aput-object v5, v3, v4

    #@97
    .line 775
    const/16 v3, 0x8

    #@99
    iput v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mRequestEf:I

    #@9b
    goto :goto_3d

    #@9c
    .line 777
    :pswitch_9c
    const/16 v3, 0x9

    #@9e
    iput v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mRequestEf:I

    #@a0
    goto :goto_3d

    #@a1
    .line 778
    :pswitch_a1
    const/16 v3, 0xa

    #@a3
    iput v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mRequestEf:I

    #@a5
    goto :goto_3d

    #@a6
    .line 779
    :pswitch_a6
    const/16 v3, 0xb

    #@a8
    iput v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mRequestEf:I

    #@aa
    goto :goto_3d

    #@ab
    .line 780
    :pswitch_ab
    const/16 v3, 0xc

    #@ad
    iput v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mRequestEf:I

    #@af
    goto :goto_3d

    #@b0
    .line 781
    :pswitch_b0
    const/16 v3, 0xd

    #@b2
    iput v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mRequestEf:I

    #@b4
    goto :goto_3d

    #@b5
    .line 782
    :pswitch_b5
    const/16 v3, 0xe

    #@b7
    iput v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mRequestEf:I

    #@b9
    goto :goto_3d

    #@ba
    .line 784
    :pswitch_ba
    iput v8, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mRequestEf:I

    #@bc
    goto :goto_3d

    #@bd
    .line 789
    .restart local v0       #emailType:I
    :cond_bd
    if-ne v0, v7, :cond_cc

    #@bf
    .line 790
    iget-object v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mSliceInfo:[[Ljava/lang/Object;

    #@c1
    iget v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mCurrentSliceIndex:I

    #@c3
    aget-object v2, v2, v3

    #@c5
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c8
    move-result-object v3

    #@c9
    aput-object v3, v2, v6

    #@cb
    goto :goto_4f

    #@cc
    .line 791
    :cond_cc
    if-ne v0, v6, :cond_dc

    #@ce
    .line 792
    iget-object v2, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mSliceInfo:[[Ljava/lang/Object;

    #@d0
    iget v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mCurrentSliceIndex:I

    #@d2
    aget-object v2, v2, v3

    #@d4
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d7
    move-result-object v3

    #@d8
    aput-object v3, v2, v6

    #@da
    goto/16 :goto_4f

    #@dc
    .line 794
    :cond_dc
    iget-object v3, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mSliceInfo:[[Ljava/lang/Object;

    #@de
    iget v4, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mCurrentSliceIndex:I

    #@e0
    aget-object v3, v3, v4

    #@e2
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@e5
    move-result-object v2

    #@e6
    aput-object v2, v3, v6

    #@e8
    goto/16 :goto_4f

    #@ea
    .line 770
    :pswitch_data_ea
    .packed-switch 0x7
        :pswitch_83
        :pswitch_9c
        :pswitch_a1
        :pswitch_a6
        :pswitch_ab
        :pswitch_b0
        :pswitch_b5
        :pswitch_ba
    .end packed-switch
.end method

.method private onGas(Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;)Z
    .registers 6
    .parameter "info"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 828
    const-string v0, "onGas"

    #@3
    invoke-direct {p0, v0, p1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->dumpNativeInfo(Ljava/lang/String;Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;)V

    #@6
    .line 834
    iget-object v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mGlobalInfo:[Ljava/lang/Object;

    #@8
    const/16 v1, 0xb

    #@a
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d
    move-result-object v2

    #@e
    aput-object v2, v0, v1

    #@10
    .line 835
    iget-object v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mGlobalInfo:[Ljava/lang/Object;

    #@12
    const/16 v1, 0x9

    #@14
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v2

    #@18
    aput-object v2, v0, v1

    #@1a
    .line 836
    iget-object v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mGlobalInfo:[Ljava/lang/Object;

    #@1c
    const/16 v1, 0xa

    #@1e
    const-string v2, ""

    #@20
    aput-object v2, v0, v1

    #@22
    .line 838
    return v3
.end method

.method private read(Landroid/content/Context;)Landroid/database/Cursor;
    .registers 9
    .parameter "context"

    #@0
    .prologue
    .line 591
    new-instance v3, Lcom/android/internal/telephony/uicc/UsimService;

    #@2
    invoke-direct {v3, p1}, Lcom/android/internal/telephony/uicc/UsimService;-><init>(Landroid/content/Context;)V

    #@5
    .line 592
    .local v3, service:Lcom/android/internal/telephony/uicc/UsimService;
    invoke-virtual {v3, p0}, Lcom/android/internal/telephony/uicc/UsimService;->registerCallback(Lcom/android/internal/telephony/uicc/IUsimInfoCallback;)V

    #@8
    .line 593
    const/4 v4, 0x0

    #@9
    iput v4, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mRequestEf:I

    #@b
    .line 596
    :cond_b
    iget-object v4, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mAsyncCallSynchronizer:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;

    #@d
    invoke-static {v4}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->access$1300(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;)V

    #@10
    .line 598
    iget v4, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mRequestEf:I

    #@12
    invoke-virtual {v3, v4}, Lcom/android/internal/telephony/uicc/UsimService;->PBMGetInfo(I)V

    #@15
    .line 600
    iget-object v4, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mAsyncCallSynchronizer:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;

    #@17
    invoke-static {v4}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->access$1400(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;)Ljava/lang/Object;

    #@1a
    move-result-object v2

    #@1b
    .line 604
    .local v2, result:Ljava/lang/Object;
    if-eqz v2, :cond_25

    #@1d
    check-cast v2, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;

    #@1f
    .end local v2           #result:Ljava/lang/Object;
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->handleResult(Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;)Z

    #@22
    move-result v4

    #@23
    if-nez v4, :cond_b

    #@25
    .line 609
    :cond_25
    invoke-virtual {v3, p0}, Lcom/android/internal/telephony/uicc/UsimService;->unregisterCallback(Lcom/android/internal/telephony/uicc/IUsimInfoCallback;)V

    #@28
    .line 611
    new-instance v0, Landroid/database/MatrixCursor;

    #@2a
    sget-object v4, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@2c
    invoke-direct {v0, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    #@2f
    .line 612
    .local v0, cursor:Landroid/database/MatrixCursor;
    iget-object v4, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mGlobalInfo:[Ljava/lang/Object;

    #@31
    if-eqz v4, :cond_37

    #@33
    iget-object v4, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mSliceInfo:[[Ljava/lang/Object;

    #@35
    if-nez v4, :cond_65

    #@37
    .line 613
    :cond_37
    const-string v4, "IccPhonebookProvider"

    #@39
    new-instance v5, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v6, "can\'t load info: global-info="

    #@40
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v5

    #@44
    iget-object v6, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mGlobalInfo:[Ljava/lang/Object;

    #@46
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v5

    #@4a
    const-string v6, ", sliceInfo="

    #@4c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v5

    #@50
    iget-object v6, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mSliceInfo:[[Ljava/lang/Object;

    #@52
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v5

    #@56
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v5

    #@5a
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    .line 614
    invoke-static {}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp;->access$1800()[Ljava/lang/Object;

    #@60
    move-result-object v4

    #@61
    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    #@64
    .line 624
    :cond_64
    return-object v0

    #@65
    .line 617
    :cond_65
    iget-object v4, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mGlobalInfo:[Ljava/lang/Object;

    #@67
    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    #@6a
    .line 619
    iget-object v4, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mSliceInfo:[[Ljava/lang/Object;

    #@6c
    if-eqz v4, :cond_64

    #@6e
    .line 620
    const/4 v1, 0x0

    #@6f
    .local v1, index:I
    :goto_6f
    iget-object v4, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mSliceInfo:[[Ljava/lang/Object;

    #@71
    array-length v4, v4

    #@72
    if-ge v1, v4, :cond_64

    #@74
    .line 621
    iget-object v4, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mSliceInfo:[[Ljava/lang/Object;

    #@76
    aget-object v4, v4, v1

    #@78
    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    #@7b
    .line 620
    add-int/lit8 v1, v1, 0x1

    #@7d
    goto :goto_6f
.end method


# virtual methods
.method public onPBMDeleteCB(II)V
    .registers 6
    .parameter "retPbmResult"
    .parameter "deleteIndex"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 850
    const-string v0, "IccPhonebookProvider"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Unplaned delete operation [result="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ", deleteIndex="

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, "]"

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 851
    return-void
.end method

.method public onPBMInfoCB(Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;)V
    .registers 3
    .parameter "info"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 844
    iget-object v0, p0, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$qctInfo;->mAsyncCallSynchronizer:Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;

    #@2
    invoke-static {v0, p1}, Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;->access$1700(Lcom/android/internal/telephony/IccPhonebookProviderBackendImp$AsyncCallSynchronizer;Ljava/lang/Object;)V

    #@5
    .line 845
    return-void
.end method

.method public onPBMReadCB(Lcom/android/internal/telephony/uicc/LGE_PBM_Records;)V
    .registers 5
    .parameter "records"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 853
    const-string v0, "IccPhonebookProvider"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Unplaned read operation [records="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, "]"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 854
    return-void
.end method

.method public onPBMWriteCB(II)V
    .registers 6
    .parameter "retPbmResult"
    .parameter "writtenSimIndex"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 847
    const-string v0, "IccPhonebookProvider"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Unplaned write operation [result="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ", writtenSimIndex="

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, "]"

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 848
    return-void
.end method
