.class public Lcom/android/internal/telephony/gsm/SuppServiceNotification;
.super Ljava/lang/Object;
.source "SuppServiceNotification.java"


# static fields
.field public static final MO_CODE_CALL_DEFLECTED:I = 0x8

.field public static final MO_CODE_CALL_FORWARDED:I = 0x2

.field public static final MO_CODE_CALL_IS_WAITING:I = 0x3

.field public static final MO_CODE_CLIR_SUPPRESSION_REJECTED:I = 0x7

.field public static final MO_CODE_CUG_CALL:I = 0x4

.field public static final MO_CODE_INCOMING_CALLS_BARRED:I = 0x6

.field public static final MO_CODE_OUTGOING_CALLS_BARRED:I = 0x5

.field public static final MO_CODE_SOME_CF_ACTIVE:I = 0x1

.field public static final MO_CODE_UNCONDITIONAL_CF_ACTIVE:I = 0x0

.field public static final MT_CODE_ADDITIONAL_CALL_FORWARDED:I = 0xa

.field public static final MT_CODE_CALL_CONNECTED_ECT:I = 0x8

.field public static final MT_CODE_CALL_CONNECTING_ECT:I = 0x7

.field public static final MT_CODE_CALL_ON_HOLD:I = 0x2

.field public static final MT_CODE_CALL_RETRIEVED:I = 0x3

.field public static final MT_CODE_CUG_CALL:I = 0x1

.field public static final MT_CODE_DEFLECTED_CALL:I = 0x9

.field public static final MT_CODE_FORWARDED_CALL:I = 0x0

.field public static final MT_CODE_FORWARD_CHECK_RECEIVED:I = 0x6

.field public static final MT_CODE_MULTI_PARTY_CALL:I = 0x4

.field public static final MT_CODE_ON_HOLD_CALL_RELEASED:I = 0x5


# instance fields
.field public code:I

.field public index:I

.field public notificationType:I

.field public number:Ljava/lang/String;

.field public type:I


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 26
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    const-string v1, " mobile"

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    iget v0, p0, Lcom/android/internal/telephony/gsm/SuppServiceNotification;->notificationType:I

    #@15
    if-nez v0, :cond_52

    #@17
    const-string v0, " originated "

    #@19
    :goto_19
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, " code: "

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Lcom/android/internal/telephony/gsm/SuppServiceNotification;->code:I

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, " index: "

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    iget v1, p0, Lcom/android/internal/telephony/gsm/SuppServiceNotification;->index:I

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const-string v1, " \""

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/SuppServiceNotification;->number:Ljava/lang/String;

    #@3d
    iget v2, p0, Lcom/android/internal/telephony/gsm/SuppServiceNotification;->type:I

    #@3f
    invoke-static {v1, v2}, Landroid/telephony/PhoneNumberUtils;->stringFromStringAndTOA(Ljava/lang/String;I)Ljava/lang/String;

    #@42
    move-result-object v1

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    const-string v1, "\" "

    #@49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v0

    #@51
    return-object v0

    #@52
    :cond_52
    const-string v0, " terminated "

    #@54
    goto :goto_19
.end method
