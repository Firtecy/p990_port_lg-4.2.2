.class public Lcom/android/internal/telephony/gsm/GSMPhone;
.super Lcom/android/internal/telephony/PhoneBase;
.source "GSMPhone.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/gsm/GSMPhone$4;,
        Lcom/android/internal/telephony/gsm/GSMPhone$PendingWrite;,
        Lcom/android/internal/telephony/gsm/GSMPhone$PendingNaf;,
        Lcom/android/internal/telephony/gsm/GSMPhone$PendingBootstrap;,
        Lcom/android/internal/telephony/gsm/GSMPhone$PendingAka;,
        Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;,
        Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;
    }
.end annotation


# static fields
#the value of this static final field might be set in the static constructor
.field static final synthetic $assertionsDisabled:Z = false

.field static final CANCEL_ECM_TIMER:I = 0x1

.field public static final CF_ENABLED:Ljava/lang/String; = "cf_enabled_key"

.field private static final CHECK_CALLFORWARDING_STATUS:I = 0x4b

.field public static final CIPHERING_KEY:Ljava/lang/String; = "ciphering_key"

.field private static final DEFAULT_ECM_EXIT_TIMER_VALUE:I = 0x493e0

.field private static ENABLE_PRIVACY_LOG:Z = false

.field private static ENABLE_PRIVACY_LOG_CALL:Z = false

.field static final EVENT_CANCEL_MANUAL_SEARCHING:I = 0x27

.field static final EVENT_GET_ENGINEERING_MODE_INFO_DONE:I = 0x25

.field static final EVENT_SET_NETWORK_MANUAL_PREVIOUS:I = 0x3d

.field private static final LOCAL_DEBUG:Z = true

.field protected static final LOG_TAG:Ljava/lang/String; = "GSM"

.field static final RESTART_ECM_TIMER:I = 0x0

.field private static final VDBG:Z = false

.field public static final VM_NUMBER:Ljava/lang/String; = "vm_number_key"

.field public static final VM_SIM_IMSI:Ljava/lang/String; = "vm_sim_imsi_key"


# instance fields
.field private SimStateReceiver:Landroid/content/BroadcastReceiver;

.field debugPortThread:Ljava/lang/Thread;

.field debugSocket:Ljava/net/ServerSocket;

.field gfUtils:Lcom/android/internal/telephony/gfit/GfitUtils;

.field mAssistDialPhoneNumberUtils:Landroid/telephony/AssistDialPhoneNumberUtils;

.field mBtSimMgr:Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;

.field mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

.field private mEcmExitRespRegistrant:Landroid/os/Registrant;

.field private final mEcmTimerResetRegistrants:Landroid/os/RegistrantList;

.field private mEmodeInfoforAndroid:[Ljava/lang/String;

.field private mEmodeModemInfo:Ljava/lang/String;

.field protected mEsn:Ljava/lang/String;

.field private mExitEcmRunnable:Ljava/lang/Runnable;

.field private mISimSessionId:I

.field protected mImei:Ljava/lang/String;

.field protected mImeiSv:Ljava/lang/String;

.field protected mIsPhoneInEcmExitDelayState:Z

.field protected mIsPhoneInEcmState:Z

.field private final mLogicalChannelSetupLock:Ljava/util/concurrent/Semaphore;

.field protected mMeid:Ljava/lang/String;

.field mPendingMMIs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/gsm/GsmMmiCode;",
            ">;"
        }
    .end annotation
.end field

.field private mPendingOperation:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;",
            ">;"
        }
    .end annotation
.end field

.field mPostDialHandler:Landroid/os/Registrant;

.field protected mSST:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

.field private mSetCfNumber:Ljava/lang/String;

.field protected mSimPhoneBookIntManager:Lcom/android/internal/telephony/gsm/SimPhoneBookInterfaceManager;

.field mSsnRegistrants:Landroid/os/RegistrantList;

.field mSubInfo:Lcom/android/internal/telephony/PhoneSubInfo;

.field private mVmNumber:Ljava/lang/String;

.field protected mWakeLock:Landroid/os/PowerManager$WakeLock;

.field protected retryNum:I


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 194
    const-class v0, Lcom/android/internal/telephony/gsm/GSMPhone;

    #@4
    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_53

    #@a
    move v0, v1

    #@b
    :goto_b
    sput-boolean v0, Lcom/android/internal/telephony/gsm/GSMPhone;->$assertionsDisabled:Z

    #@d
    .line 202
    const-string v3, "persist.service.privacy.enable"

    #@f
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    const-string v4, "ATT"

    #@15
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v0

    #@19
    if-nez v0, :cond_27

    #@1b
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    const-string v4, "TMO"

    #@21
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v0

    #@25
    if-eqz v0, :cond_55

    #@27
    :cond_27
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@2a
    move-result-object v0

    #@2b
    const-string v4, "US"

    #@2d
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v0

    #@31
    if-eqz v0, :cond_55

    #@33
    move v0, v2

    #@34
    :goto_34
    invoke-static {v3, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@37
    move-result v0

    #@38
    sput-boolean v0, Lcom/android/internal/telephony/gsm/GSMPhone;->ENABLE_PRIVACY_LOG:Z

    #@3a
    .line 205
    const-string v0, "persist.service.privacy.enable"

    #@3c
    const-string v3, "TMO"

    #@3e
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@41
    move-result v3

    #@42
    if-eqz v3, :cond_57

    #@44
    const-string v3, "US"

    #@46
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@49
    move-result v3

    #@4a
    if-eqz v3, :cond_57

    #@4c
    :goto_4c
    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@4f
    move-result v0

    #@50
    sput-boolean v0, Lcom/android/internal/telephony/gsm/GSMPhone;->ENABLE_PRIVACY_LOG_CALL:Z

    #@52
    return-void

    #@53
    :cond_53
    move v0, v2

    #@54
    .line 194
    goto :goto_b

    #@55
    :cond_55
    move v0, v1

    #@56
    .line 202
    goto :goto_34

    #@57
    :cond_57
    move v2, v1

    #@58
    .line 205
    goto :goto_4c
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Lcom/android/internal/telephony/PhoneNotifier;)V
    .registers 5
    .parameter "context"
    .parameter "ci"
    .parameter "notifier"

    #@0
    .prologue
    .line 316
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/internal/telephony/gsm/GSMPhone;-><init>(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Lcom/android/internal/telephony/PhoneNotifier;Z)V

    #@4
    .line 317
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Lcom/android/internal/telephony/PhoneNotifier;Z)V
    .registers 13
    .parameter "context"
    .parameter "ci"
    .parameter "notifier"
    .parameter "unitTestMode"

    #@0
    .prologue
    .line 321
    invoke-direct {p0, p3, p1, p2, p4}, Lcom/android/internal/telephony/PhoneBase;-><init>(Lcom/android/internal/telephony/PhoneNotifier;Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Z)V

    #@3
    .line 223
    new-instance v4, Ljava/util/ArrayList;

    #@5
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mPendingMMIs:Ljava/util/ArrayList;

    #@a
    .line 236
    new-instance v4, Landroid/os/RegistrantList;

    #@c
    invoke-direct {v4}, Landroid/os/RegistrantList;-><init>()V

    #@f
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSsnRegistrants:Landroid/os/RegistrantList;

    #@11
    .line 252
    const/4 v4, 0x0

    #@12
    iput v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->retryNum:I

    #@14
    .line 264
    new-instance v4, Landroid/telephony/AssistDialPhoneNumberUtils;

    #@16
    invoke-direct {v4}, Landroid/telephony/AssistDialPhoneNumberUtils;-><init>()V

    #@19
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mAssistDialPhoneNumberUtils:Landroid/telephony/AssistDialPhoneNumberUtils;

    #@1b
    .line 268
    const/16 v4, 0x10

    #@1d
    new-array v4, v4, [Ljava/lang/String;

    #@1f
    const/4 v5, 0x0

    #@20
    const-string v6, " "

    #@22
    aput-object v6, v4, v5

    #@24
    const/4 v5, 0x1

    #@25
    const-string v6, " "

    #@27
    aput-object v6, v4, v5

    #@29
    const/4 v5, 0x2

    #@2a
    const-string v6, " "

    #@2c
    aput-object v6, v4, v5

    #@2e
    const/4 v5, 0x3

    #@2f
    const-string v6, " "

    #@31
    aput-object v6, v4, v5

    #@33
    const/4 v5, 0x4

    #@34
    const-string v6, " "

    #@36
    aput-object v6, v4, v5

    #@38
    const/4 v5, 0x5

    #@39
    const-string v6, " "

    #@3b
    aput-object v6, v4, v5

    #@3d
    const/4 v5, 0x6

    #@3e
    const-string v6, " "

    #@40
    aput-object v6, v4, v5

    #@42
    const/4 v5, 0x7

    #@43
    const-string v6, " "

    #@45
    aput-object v6, v4, v5

    #@47
    const/16 v5, 0x8

    #@49
    const-string v6, " "

    #@4b
    aput-object v6, v4, v5

    #@4d
    const/16 v5, 0x9

    #@4f
    const-string v6, " "

    #@51
    aput-object v6, v4, v5

    #@53
    const/16 v5, 0xa

    #@55
    const-string v6, " "

    #@57
    aput-object v6, v4, v5

    #@59
    const/16 v5, 0xb

    #@5b
    const-string v6, " "

    #@5d
    aput-object v6, v4, v5

    #@5f
    const/16 v5, 0xc

    #@61
    const-string v6, " "

    #@63
    aput-object v6, v4, v5

    #@65
    const/16 v5, 0xd

    #@67
    const-string v6, " "

    #@69
    aput-object v6, v4, v5

    #@6b
    const/16 v5, 0xe

    #@6d
    const-string v6, " "

    #@6f
    aput-object v6, v4, v5

    #@71
    const/16 v5, 0xf

    #@73
    const-string v6, " "

    #@75
    aput-object v6, v4, v5

    #@77
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mEmodeInfoforAndroid:[Ljava/lang/String;

    #@79
    .line 284
    new-instance v4, Landroid/os/RegistrantList;

    #@7b
    invoke-direct {v4}, Landroid/os/RegistrantList;-><init>()V

    #@7e
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mEcmTimerResetRegistrants:Landroid/os/RegistrantList;

    #@80
    .line 297
    const/4 v4, -0x1

    #@81
    iput v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mISimSessionId:I

    #@83
    .line 298
    new-instance v4, Ljava/util/concurrent/Semaphore;

    #@85
    const/4 v5, 0x1

    #@86
    invoke-direct {v4, v5}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    #@89
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mLogicalChannelSetupLock:Ljava/util/concurrent/Semaphore;

    #@8b
    .line 299
    new-instance v4, Ljava/util/ArrayList;

    #@8d
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@90
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mPendingOperation:Ljava/util/List;

    #@92
    .line 304
    new-instance v4, Lcom/android/internal/telephony/gsm/GSMPhone$1;

    #@94
    invoke-direct {v4, p0}, Lcom/android/internal/telephony/gsm/GSMPhone$1;-><init>(Lcom/android/internal/telephony/gsm/GSMPhone;)V

    #@97
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mExitEcmRunnable:Ljava/lang/Runnable;

    #@99
    .line 444
    new-instance v4, Lcom/android/internal/telephony/gsm/GSMPhone$3;

    #@9b
    invoke-direct {v4, p0}, Lcom/android/internal/telephony/gsm/GSMPhone$3;-><init>(Lcom/android/internal/telephony/gsm/GSMPhone;)V

    #@9e
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->SimStateReceiver:Landroid/content/BroadcastReceiver;

    #@a0
    .line 323
    instance-of v4, p2, Lcom/android/internal/telephony/test/SimulatedRadioControl;

    #@a2
    if-eqz v4, :cond_a8

    #@a4
    .line 324
    check-cast p2, Lcom/android/internal/telephony/test/SimulatedRadioControl;

    #@a6
    .end local p2
    iput-object p2, p0, Lcom/android/internal/telephony/PhoneBase;->mSimulatedRadioControl:Lcom/android/internal/telephony/test/SimulatedRadioControl;

    #@a8
    .line 327
    :cond_a8
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@aa
    const/4 v5, 0x1

    #@ab
    invoke-interface {v4, v5}, Lcom/android/internal/telephony/CommandsInterface;->setPhoneType(I)V

    #@ae
    .line 328
    new-instance v4, Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@b0
    invoke-direct {v4, p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;-><init>(Lcom/android/internal/telephony/gsm/GSMPhone;)V

    #@b3
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@b5
    .line 330
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->initSubscriptionSpecifics()V

    #@b8
    .line 333
    new-instance v4, Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@ba
    iget-object v5, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@bc
    invoke-direct {v4, v5, p0}, Lcom/android/internal/telephony/LGEDataConnectionTracker;-><init>(Lcom/android/internal/telephony/DataConnectionTracker;Lcom/android/internal/telephony/PhoneBase;)V

    #@bf
    iput-object v4, p0, Lcom/android/internal/telephony/PhoneBase;->mLGEDataConnectionTracker:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@c1
    .line 337
    const-string v4, "vzw_gfit"

    #@c3
    invoke-static {p1, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@c6
    move-result v4

    #@c7
    if-eqz v4, :cond_d2

    #@c9
    .line 338
    new-instance v4, Lcom/android/internal/telephony/gfit/GfitUtils;

    #@cb
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSST:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@cd
    invoke-direct {v4, v5, p0}, Lcom/android/internal/telephony/gfit/GfitUtils;-><init>(Lcom/android/internal/telephony/ServiceStateTracker;Lcom/android/internal/telephony/PhoneBase;)V

    #@d0
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->gfUtils:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@d2
    .line 343
    :cond_d2
    new-instance v4, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;

    #@d4
    invoke-direct {v4, p1, p0}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;-><init>(Landroid/content/Context;Lcom/android/internal/telephony/gsm/GSMPhone;)V

    #@d7
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mBtSimMgr:Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;

    #@d9
    .line 345
    if-nez p4, :cond_e9

    #@db
    .line 346
    new-instance v4, Lcom/android/internal/telephony/gsm/SimPhoneBookInterfaceManager;

    #@dd
    invoke-direct {v4, p0}, Lcom/android/internal/telephony/gsm/SimPhoneBookInterfaceManager;-><init>(Lcom/android/internal/telephony/gsm/GSMPhone;)V

    #@e0
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSimPhoneBookIntManager:Lcom/android/internal/telephony/gsm/SimPhoneBookInterfaceManager;

    #@e2
    .line 347
    new-instance v4, Lcom/android/internal/telephony/PhoneSubInfo;

    #@e4
    invoke-direct {v4, p0}, Lcom/android/internal/telephony/PhoneSubInfo;-><init>(Lcom/android/internal/telephony/Phone;)V

    #@e7
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSubInfo:Lcom/android/internal/telephony/PhoneSubInfo;

    #@e9
    .line 350
    :cond_e9
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@eb
    const/4 v5, 0x1

    #@ec
    const/4 v6, 0x0

    #@ed
    invoke-interface {v4, p0, v5, v6}, Lcom/android/internal/telephony/CommandsInterface;->registerForAvailable(Landroid/os/Handler;ILjava/lang/Object;)V

    #@f0
    .line 351
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@f2
    const/16 v5, 0x8

    #@f4
    const/4 v6, 0x0

    #@f5
    invoke-interface {v4, p0, v5, v6}, Lcom/android/internal/telephony/CommandsInterface;->registerForOffOrNotAvailable(Landroid/os/Handler;ILjava/lang/Object;)V

    #@f8
    .line 352
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@fa
    const/4 v5, 0x5

    #@fb
    const/4 v6, 0x0

    #@fc
    invoke-interface {v4, p0, v5, v6}, Lcom/android/internal/telephony/CommandsInterface;->registerForOn(Landroid/os/Handler;ILjava/lang/Object;)V

    #@ff
    .line 353
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@101
    const/4 v5, 0x7

    #@102
    const/4 v6, 0x0

    #@103
    invoke-interface {v4, p0, v5, v6}, Lcom/android/internal/telephony/CommandsInterface;->setOnUSSD(Landroid/os/Handler;ILjava/lang/Object;)V

    #@106
    .line 354
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@108
    const/4 v5, 0x2

    #@109
    const/4 v6, 0x0

    #@10a
    invoke-interface {v4, p0, v5, v6}, Lcom/android/internal/telephony/CommandsInterface;->setOnSuppServiceNotification(Landroid/os/Handler;ILjava/lang/Object;)V

    #@10d
    .line 355
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSST:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@10f
    const/16 v5, 0x13

    #@111
    const/4 v6, 0x0

    #@112
    invoke-virtual {v4, p0, v5, v6}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->registerForNetworkAttached(Landroid/os/Handler;ILjava/lang/Object;)V

    #@115
    .line 356
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@117
    const/16 v5, 0x1f

    #@119
    const/4 v6, 0x0

    #@11a
    invoke-interface {v4, p0, v5, v6}, Lcom/android/internal/telephony/CommandsInterface;->setOnSS(Landroid/os/Handler;ILjava/lang/Object;)V

    #@11d
    .line 359
    const-string v4, "support_emergency_callback_mode_for_gsm"

    #@11f
    invoke-static {p1, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@122
    move-result v4

    #@123
    if-eqz v4, :cond_135

    #@125
    .line 360
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@127
    const/16 v5, 0x19

    #@129
    const/4 v6, 0x0

    #@12a
    invoke-interface {v4, p0, v5, v6}, Lcom/android/internal/telephony/CommandsInterface;->setEmergencyCallbackMode(Landroid/os/Handler;ILjava/lang/Object;)V

    #@12d
    .line 361
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@12f
    const/16 v5, 0x1a

    #@131
    const/4 v6, 0x0

    #@132
    invoke-interface {v4, p0, v5, v6}, Lcom/android/internal/telephony/CommandsInterface;->registerForExitEmergencyCallbackMode(Landroid/os/Handler;ILjava/lang/Object;)V

    #@135
    .line 364
    :cond_135
    const-string v4, "power"

    #@137
    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@13a
    move-result-object v3

    #@13b
    check-cast v3, Landroid/os/PowerManager;

    #@13d
    .line 366
    .local v3, pm:Landroid/os/PowerManager;
    const/4 v4, 0x1

    #@13e
    const-string v5, "GSM"

    #@140
    invoke-virtual {v3, v4, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@143
    move-result-object v4

    #@144
    iput-object v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@146
    .line 368
    const-string v4, "ril.cdma.inecmmode"

    #@148
    const-string v5, "false"

    #@14a
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@14d
    move-result-object v0

    #@14e
    .line 369
    .local v0, inEcm:Ljava/lang/String;
    const-string v4, "true"

    #@150
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@153
    move-result v4

    #@154
    iput-boolean v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mIsPhoneInEcmState:Z

    #@156
    .line 371
    iget-boolean v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mIsPhoneInEcmState:Z

    #@158
    if-eqz v4, :cond_16f

    #@15a
    .line 372
    const-string v4, "support_emergency_callback_mode_for_gsm"

    #@15c
    invoke-static {p1, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@15f
    move-result v4

    #@160
    if-eqz v4, :cond_16f

    #@162
    .line 373
    invoke-static {}, Lcom/android/internal/telephony/gsm/GSMPhone;->getEndTimeForEcm()J

    #@165
    move-result-wide v1

    #@166
    .line 374
    .local v1, mEcmTimeout:J
    const-wide/16 v4, 0x0

    #@168
    cmp-long v4, v1, v4

    #@16a
    if-lez v4, :cond_19a

    #@16c
    .line 375
    invoke-direct {p0, v1, v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->handleEnterEmergencyCallbackMode(J)V

    #@16f
    .line 385
    .end local v1           #mEcmTimeout:J
    :cond_16f
    :goto_16f
    const/4 v4, 0x0

    #@170
    iput-boolean v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mIsPhoneInEcmExitDelayState:Z

    #@172
    .line 421
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->setProperties()V

    #@175
    .line 425
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->SimStateReceiver:Landroid/content/BroadcastReceiver;

    #@177
    new-instance v5, Landroid/content/IntentFilter;

    #@179
    const-string v6, "android.intent.action.SIM_STATE_CHANGED"

    #@17b
    invoke-direct {v5, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@17e
    const/4 v6, 0x0

    #@17f
    const/4 v7, 0x0

    #@180
    invoke-virtual {p1, v4, v5, v6, v7}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@183
    .line 428
    sget-boolean v4, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->MULTI_SIM_ENABLED:Z

    #@185
    if-eqz v4, :cond_195

    #@187
    .line 429
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->SimStateReceiver:Landroid/content/BroadcastReceiver;

    #@189
    new-instance v5, Landroid/content/IntentFilter;

    #@18b
    const-string v6, "qualcomm.intent.action.ACTION_DEFAULT_SUBSCRIPTION_CHANGED"

    #@18d
    invoke-direct {v5, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@190
    const/4 v6, 0x0

    #@191
    const/4 v7, 0x0

    #@192
    invoke-virtual {p1, v4, v5, v6, v7}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@195
    .line 432
    :cond_195
    const/4 v4, 0x1

    #@196
    invoke-static {p1, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->init(Landroid/content/Context;I)V

    #@199
    .line 435
    return-void

    #@19a
    .line 378
    .restart local v1       #mEcmTimeout:J
    :cond_19a
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@19c
    const/16 v5, 0x1a

    #@19e
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/gsm/GSMPhone;->obtainMessage(I)Landroid/os/Message;

    #@1a1
    move-result-object v5

    #@1a2
    invoke-interface {v4, v5}, Lcom/android/internal/telephony/CommandsInterface;->exitEmergencyCallbackMode(Landroid/os/Message;)V

    #@1a5
    goto :goto_16f
.end method

.method static synthetic access$000(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 194
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 194
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 194
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 194
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 194
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 194
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 194
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/internal/telephony/gsm/GSMPhone;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 194
    iget v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mISimSessionId:I

    #@2
    return v0
.end method

.method static synthetic access$1700(Lcom/android/internal/telephony/gsm/GSMPhone;)Lcom/android/internal/telephony/uicc/UiccController;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 194
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 194
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 194
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 194
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 194
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 194
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 194
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 194
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 194
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method private appendGbaParameter(Ljava/io/ByteArrayOutputStream;[B)V
    .registers 6
    .parameter "os"
    .parameter "data"

    #@0
    .prologue
    .line 3390
    array-length v0, p2

    #@1
    .line 3393
    .local v0, len:I
    const/16 v1, 0xff

    #@3
    if-le v0, v1, :cond_e

    #@5
    .line 3394
    const/16 v0, 0xff

    #@7
    .line 3395
    const-string v1, "GSM"

    #@9
    const-string v2, "Too long value in GBA Bootstrapping parameters"

    #@b
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 3398
    :cond_e
    invoke-virtual {p1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@11
    .line 3399
    const/4 v1, 0x0

    #@12
    invoke-virtual {p1, p2, v1, v0}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@15
    .line 3400
    return-void
.end method

.method private ensureISimSession(Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;)V
    .registers 8
    .parameter "operation"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 3307
    sget-boolean v3, Lcom/android/internal/telephony/gsm/GSMPhone;->$assertionsDisabled:Z

    #@3
    if-nez v3, :cond_d

    #@5
    if-nez p1, :cond_d

    #@7
    new-instance v2, Ljava/lang/AssertionError;

    #@9
    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    #@c
    throw v2

    #@d
    .line 3309
    :cond_d
    const-string v3, "GSM"

    #@f
    const-string v4, "ensureISimSession"

    #@11
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 3312
    :try_start_14
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mLogicalChannelSetupLock:Ljava/util/concurrent/Semaphore;

    #@16
    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->acquire()V
    :try_end_19
    .catch Ljava/lang/InterruptedException; {:try_start_14 .. :try_end_19} :catch_46

    #@19
    .line 3321
    iget v3, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mISimSessionId:I

    #@1b
    const/4 v4, -0x1

    #@1c
    if-ne v3, v4, :cond_60

    #@1e
    .line 3332
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@20
    if-eqz v3, :cond_4b

    #@22
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@24
    const/4 v4, 0x3

    #@25
    invoke-virtual {v3, v4}, Lcom/android/internal/telephony/uicc/UiccController;->getUiccCardApplication(I)Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@28
    move-result-object v1

    #@29
    .line 3333
    .local v1, uiccapp:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    :goto_29
    if-eqz v1, :cond_50

    #@2b
    .line 3334
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mPendingOperation:Ljava/util/List;

    #@2d
    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@30
    .line 3335
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@32
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getAid()Ljava/lang/String;

    #@35
    move-result-object v2

    #@36
    if-eqz v2, :cond_4d

    #@38
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getAid()Ljava/lang/String;

    #@3b
    move-result-object v2

    #@3c
    :goto_3c
    const/16 v4, 0x27

    #@3e
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/gsm/GSMPhone;->obtainMessage(I)Landroid/os/Message;

    #@41
    move-result-object v4

    #@42
    invoke-interface {v3, v2, v4}, Lcom/android/internal/telephony/CommandsInterface;->uiccSelectApplication(Ljava/lang/String;Landroid/os/Message;)V

    #@45
    .line 3347
    .end local v1           #uiccapp:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    :goto_45
    return-void

    #@46
    .line 3313
    :catch_46
    move-exception v0

    #@47
    .line 3314
    .local v0, ie:Ljava/lang/InterruptedException;
    invoke-virtual {p1, v2, v0}, Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;->onSessionStarted(Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@4a
    goto :goto_45

    #@4b
    .end local v0           #ie:Ljava/lang/InterruptedException;
    :cond_4b
    move-object v1, v2

    #@4c
    .line 3332
    goto :goto_29

    #@4d
    .line 3335
    .restart local v1       #uiccapp:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    :cond_4d
    const-string v2, ""

    #@4f
    goto :goto_3c

    #@50
    .line 3339
    :cond_50
    new-instance v3, Ljava/lang/Exception;

    #@52
    const-string v4, "ISIM application not found or ready"

    #@54
    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    #@57
    invoke-virtual {p1, v2, v3}, Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;->onSessionStarted(Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@5a
    .line 3340
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mLogicalChannelSetupLock:Ljava/util/concurrent/Semaphore;

    #@5c
    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->release()V

    #@5f
    goto :goto_45

    #@60
    .line 3344
    .end local v1           #uiccapp:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    :cond_60
    const/4 v3, 0x1

    #@61
    new-array v3, v3, [I

    #@63
    const/4 v4, 0x0

    #@64
    iget v5, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mISimSessionId:I

    #@66
    aput v5, v3, v4

    #@68
    invoke-virtual {p1, v3, v2}, Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;->onSessionStarted(Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@6b
    .line 3345
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mLogicalChannelSetupLock:Ljava/util/concurrent/Semaphore;

    #@6d
    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->release()V

    #@70
    goto :goto_45
.end method

.method private getStoredVoiceMessageCount()I
    .registers 8

    #@0
    .prologue
    .line 2918
    const/4 v0, 0x0

    #@1
    .line 2919
    .local v0, countVoiceMessages:I
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@3
    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@6
    move-result-object v3

    #@7
    .line 2920
    .local v3, sp:Landroid/content/SharedPreferences;
    const-string v4, "vm_id_key"

    #@9
    const/4 v5, 0x0

    #@a
    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@d
    move-result-object v2

    #@e
    .line 2921
    .local v2, imsi:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getSubscriberId()Ljava/lang/String;

    #@11
    move-result-object v1

    #@12
    .line 2924
    .local v1, currentImsi:Ljava/lang/String;
    sget-boolean v4, Lcom/android/internal/telephony/gsm/GSMPhone;->ENABLE_PRIVACY_LOG_CALL:Z

    #@14
    if-eqz v4, :cond_38

    #@16
    const-string v4, "GSM"

    #@18
    new-instance v5, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v6, "Voicemail count retrieval for Imsi = "

    #@1f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v5

    #@23
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v5

    #@27
    const-string v6, " current Imsi = "

    #@29
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v5

    #@2d
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v5

    #@31
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v5

    #@35
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 2928
    :cond_38
    if-eqz v2, :cond_61

    #@3a
    if-eqz v1, :cond_61

    #@3c
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3f
    move-result v4

    #@40
    if-eqz v4, :cond_61

    #@42
    .line 2931
    const-string v4, "vm_count_key"

    #@44
    const/4 v5, 0x0

    #@45
    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    #@48
    move-result v0

    #@49
    .line 2932
    const-string v4, "GSM"

    #@4b
    new-instance v5, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v6, "Voice Mail Count from preference = "

    #@52
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v5

    #@56
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@59
    move-result-object v5

    #@5a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v5

    #@5e
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@61
    .line 2934
    :cond_61
    return v0
.end method

.method private getVmSimImsi()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 1415
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@3
    move-result-object v1

    #@4
    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@7
    move-result-object v0

    #@8
    .line 1416
    .local v0, sp:Landroid/content/SharedPreferences;
    const-string v1, "vm_sim_imsi_key"

    #@a
    const/4 v2, 0x0

    #@b
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    return-object v1
.end method

.method private handleCallDeflectionIncallSupplementaryService(Ljava/lang/String;)Z
    .registers 6
    .parameter "dialString"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 913
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@4
    move-result v2

    #@5
    if-le v2, v1, :cond_9

    #@7
    .line 914
    const/4 v1, 0x0

    #@8
    .line 932
    :cond_8
    :goto_8
    return v1

    #@9
    .line 917
    :cond_9
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getRingingCall()Lcom/android/internal/telephony/gsm/GsmCall;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2}, Lcom/android/internal/telephony/gsm/GsmCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@10
    move-result-object v2

    #@11
    sget-object v3, Lcom/android/internal/telephony/Call$State;->IDLE:Lcom/android/internal/telephony/Call$State;

    #@13
    if-eq v2, v3, :cond_30

    #@15
    .line 918
    const-string v2, "GSM"

    #@17
    const-string v3, "MmiCode 0: rejectCall"

    #@19
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 920
    :try_start_1c
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@1e
    invoke-virtual {v2}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->rejectCall()V
    :try_end_21
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_1c .. :try_end_21} :catch_22

    #@21
    goto :goto_8

    #@22
    .line 921
    :catch_22
    move-exception v0

    #@23
    .line 922
    .local v0, e:Lcom/android/internal/telephony/CallStateException;
    const-string v2, "GSM"

    #@25
    const-string v3, "reject failed"

    #@27
    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2a
    .line 924
    sget-object v2, Lcom/android/internal/telephony/Phone$SuppService;->REJECT:Lcom/android/internal/telephony/Phone$SuppService;

    #@2c
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifySuppServiceFailed(Lcom/android/internal/telephony/Phone$SuppService;)V

    #@2f
    goto :goto_8

    #@30
    .line 926
    .end local v0           #e:Lcom/android/internal/telephony/CallStateException;
    :cond_30
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getBackgroundCall()Lcom/android/internal/telephony/gsm/GsmCall;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2}, Lcom/android/internal/telephony/gsm/GsmCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@37
    move-result-object v2

    #@38
    sget-object v3, Lcom/android/internal/telephony/Call$State;->IDLE:Lcom/android/internal/telephony/Call$State;

    #@3a
    if-eq v2, v3, :cond_8

    #@3c
    .line 927
    const-string v2, "GSM"

    #@3e
    const-string v3, "MmiCode 0: hangupWaitingOrBackground"

    #@40
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 929
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@45
    invoke-virtual {v2}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangupWaitingOrBackground()V

    #@48
    goto :goto_8
.end method

.method private handleCallHoldIncallSupplementaryService(Ljava/lang/String;)Z
    .registers 12
    .parameter "dialString"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 979
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@4
    move-result v5

    #@5
    .line 981
    .local v5, len:I
    const/4 v7, 0x2

    #@6
    if-le v5, v7, :cond_a

    #@8
    .line 982
    const/4 v6, 0x0

    #@9
    .line 1026
    :goto_9
    return v6

    #@a
    .line 985
    :cond_a
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getForegroundCall()Lcom/android/internal/telephony/gsm/GsmCall;

    #@d
    move-result-object v0

    #@e
    .line 987
    .local v0, call:Lcom/android/internal/telephony/gsm/GsmCall;
    if-le v5, v6, :cond_6e

    #@10
    .line 989
    const/4 v7, 0x1

    #@11
    :try_start_11
    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    #@14
    move-result v2

    #@15
    .line 990
    .local v2, ch:C
    add-int/lit8 v1, v2, -0x30

    #@17
    .line 991
    .local v1, callIndex:I
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@19
    invoke-virtual {v7, v0, v1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->getConnectionByIndex(Lcom/android/internal/telephony/gsm/GsmCall;I)Lcom/android/internal/telephony/gsm/GsmConnection;

    #@1c
    move-result-object v3

    #@1d
    .line 994
    .local v3, conn:Lcom/android/internal/telephony/gsm/GsmConnection;
    if-eqz v3, :cond_50

    #@1f
    if-lt v1, v6, :cond_50

    #@21
    const/4 v7, 0x7

    #@22
    if-gt v1, v7, :cond_50

    #@24
    .line 995
    const-string v7, "GSM"

    #@26
    new-instance v8, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v9, "MmiCode 2: separate call "

    #@2d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v8

    #@31
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v8

    #@35
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v8

    #@39
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 997
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@3e
    invoke-virtual {v7, v3}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->separate(Lcom/android/internal/telephony/gsm/GsmConnection;)V
    :try_end_41
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_11 .. :try_end_41} :catch_42

    #@41
    goto :goto_9

    #@42
    .line 1003
    .end local v1           #callIndex:I
    .end local v2           #ch:C
    .end local v3           #conn:Lcom/android/internal/telephony/gsm/GsmConnection;
    :catch_42
    move-exception v4

    #@43
    .line 1004
    .local v4, e:Lcom/android/internal/telephony/CallStateException;
    const-string v7, "GSM"

    #@45
    const-string v8, "separate failed"

    #@47
    invoke-static {v7, v8, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4a
    .line 1006
    sget-object v7, Lcom/android/internal/telephony/Phone$SuppService;->SEPARATE:Lcom/android/internal/telephony/Phone$SuppService;

    #@4c
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifySuppServiceFailed(Lcom/android/internal/telephony/Phone$SuppService;)V

    #@4f
    goto :goto_9

    #@50
    .line 999
    .end local v4           #e:Lcom/android/internal/telephony/CallStateException;
    .restart local v1       #callIndex:I
    .restart local v2       #ch:C
    .restart local v3       #conn:Lcom/android/internal/telephony/gsm/GsmConnection;
    :cond_50
    :try_start_50
    const-string v7, "GSM"

    #@52
    new-instance v8, Ljava/lang/StringBuilder;

    #@54
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@57
    const-string v9, "separate: invalid call index "

    #@59
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v8

    #@5d
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@60
    move-result-object v8

    #@61
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v8

    #@65
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    .line 1001
    sget-object v7, Lcom/android/internal/telephony/Phone$SuppService;->SEPARATE:Lcom/android/internal/telephony/Phone$SuppService;

    #@6a
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifySuppServiceFailed(Lcom/android/internal/telephony/Phone$SuppService;)V
    :try_end_6d
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_50 .. :try_end_6d} :catch_42

    #@6d
    goto :goto_9

    #@6e
    .line 1010
    .end local v1           #callIndex:I
    .end local v2           #ch:C
    .end local v3           #conn:Lcom/android/internal/telephony/gsm/GsmConnection;
    :cond_6e
    :try_start_6e
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getRingingCall()Lcom/android/internal/telephony/gsm/GsmCall;

    #@71
    move-result-object v7

    #@72
    invoke-virtual {v7}, Lcom/android/internal/telephony/gsm/GsmCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@75
    move-result-object v7

    #@76
    sget-object v8, Lcom/android/internal/telephony/Call$State;->IDLE:Lcom/android/internal/telephony/Call$State;

    #@78
    if-eq v7, v8, :cond_96

    #@7a
    .line 1011
    const-string v7, "GSM"

    #@7c
    const-string v8, "MmiCode 2: accept ringing call"

    #@7e
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    .line 1013
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@83
    invoke-virtual {v7}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->acceptCall()V
    :try_end_86
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_6e .. :try_end_86} :catch_87

    #@86
    goto :goto_9

    #@87
    .line 1019
    :catch_87
    move-exception v4

    #@88
    .line 1020
    .restart local v4       #e:Lcom/android/internal/telephony/CallStateException;
    const-string v7, "GSM"

    #@8a
    const-string v8, "switch failed"

    #@8c
    invoke-static {v7, v8, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@8f
    .line 1022
    sget-object v7, Lcom/android/internal/telephony/Phone$SuppService;->SWITCH:Lcom/android/internal/telephony/Phone$SuppService;

    #@91
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifySuppServiceFailed(Lcom/android/internal/telephony/Phone$SuppService;)V

    #@94
    goto/16 :goto_9

    #@96
    .line 1015
    .end local v4           #e:Lcom/android/internal/telephony/CallStateException;
    :cond_96
    :try_start_96
    const-string v7, "GSM"

    #@98
    const-string v8, "MmiCode 2: switchWaitingOrHoldingAndActive"

    #@9a
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9d
    .line 1017
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@9f
    invoke-virtual {v7}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->switchWaitingOrHoldingAndActive()V
    :try_end_a2
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_96 .. :try_end_a2} :catch_87

    #@a2
    goto/16 :goto_9
.end method

.method private handleCallWaitingIncallSupplementaryService(Ljava/lang/String;)Z
    .registers 11
    .parameter "dialString"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 937
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@4
    move-result v4

    #@5
    .line 939
    .local v4, len:I
    const/4 v6, 0x2

    #@6
    if-le v4, v6, :cond_a

    #@8
    .line 940
    const/4 v5, 0x0

    #@9
    .line 974
    :cond_9
    :goto_9
    return v5

    #@a
    .line 943
    :cond_a
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getForegroundCall()Lcom/android/internal/telephony/gsm/GsmCall;

    #@d
    move-result-object v0

    #@e
    .line 946
    .local v0, call:Lcom/android/internal/telephony/gsm/GsmCall;
    if-le v4, v5, :cond_48

    #@10
    .line 947
    const/4 v6, 0x1

    #@11
    :try_start_11
    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    #@14
    move-result v2

    #@15
    .line 948
    .local v2, ch:C
    add-int/lit8 v1, v2, -0x30

    #@17
    .line 950
    .local v1, callIndex:I
    if-lt v1, v5, :cond_9

    #@19
    const/4 v6, 0x7

    #@1a
    if-gt v1, v6, :cond_9

    #@1c
    .line 951
    const-string v6, "GSM"

    #@1e
    new-instance v7, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v8, "MmiCode 1: hangupConnectionByIndex "

    #@25
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v7

    #@29
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v7

    #@2d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v7

    #@31
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 954
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@36
    invoke-virtual {v6, v0, v1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangupConnectionByIndex(Lcom/android/internal/telephony/gsm/GsmCall;I)V
    :try_end_39
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_11 .. :try_end_39} :catch_3a

    #@39
    goto :goto_9

    #@3a
    .line 968
    .end local v1           #callIndex:I
    .end local v2           #ch:C
    :catch_3a
    move-exception v3

    #@3b
    .line 969
    .local v3, e:Lcom/android/internal/telephony/CallStateException;
    const-string v6, "GSM"

    #@3d
    const-string v7, "hangup failed"

    #@3f
    invoke-static {v6, v7, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@42
    .line 971
    sget-object v6, Lcom/android/internal/telephony/Phone$SuppService;->HANGUP:Lcom/android/internal/telephony/Phone$SuppService;

    #@44
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifySuppServiceFailed(Lcom/android/internal/telephony/Phone$SuppService;)V

    #@47
    goto :goto_9

    #@48
    .line 957
    .end local v3           #e:Lcom/android/internal/telephony/CallStateException;
    :cond_48
    :try_start_48
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@4b
    move-result-object v6

    #@4c
    sget-object v7, Lcom/android/internal/telephony/Call$State;->IDLE:Lcom/android/internal/telephony/Call$State;

    #@4e
    if-eq v6, v7, :cond_5d

    #@50
    .line 958
    const-string v6, "GSM"

    #@52
    const-string v7, "MmiCode 1: hangup foreground"

    #@54
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    .line 961
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@59
    invoke-virtual {v6, v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangup(Lcom/android/internal/telephony/gsm/GsmCall;)V

    #@5c
    goto :goto_9

    #@5d
    .line 963
    :cond_5d
    const-string v6, "GSM"

    #@5f
    const-string v7, "MmiCode 1: switchWaitingOrHoldingAndActive"

    #@61
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    .line 965
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@66
    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->switchWaitingOrHoldingAndActive()V
    :try_end_69
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_48 .. :try_end_69} :catch_3a

    #@69
    goto :goto_9
.end method

.method private handleCcbsIncallSupplementaryService(Ljava/lang/String;)Z
    .registers 5
    .parameter "dialString"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 1068
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@4
    move-result v1

    #@5
    if-le v1, v0, :cond_9

    #@7
    .line 1069
    const/4 v0, 0x0

    #@8
    .line 1075
    :goto_8
    return v0

    #@9
    .line 1072
    :cond_9
    const-string v1, "GSM"

    #@b
    const-string v2, "MmiCode 5: CCBS not supported!"

    #@d
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 1074
    sget-object v1, Lcom/android/internal/telephony/Phone$SuppService;->UNKNOWN:Lcom/android/internal/telephony/Phone$SuppService;

    #@12
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifySuppServiceFailed(Lcom/android/internal/telephony/Phone$SuppService;)V

    #@15
    goto :goto_8
.end method

.method private handleCfuQueryResult([Lcom/android/internal/telephony/CallForwardInfo;)V
    .registers 10
    .parameter "infos"

    #@0
    .prologue
    const/4 v7, 0x7

    #@1
    const/4 v5, 0x0

    #@2
    const/4 v4, 0x1

    #@3
    .line 2766
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@5
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@8
    move-result-object v1

    #@9
    check-cast v1, Lcom/android/internal/telephony/uicc/IccRecords;

    #@b
    .line 2767
    .local v1, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v1, :cond_15

    #@d
    .line 2768
    if-eqz p1, :cond_12

    #@f
    array-length v3, p1

    #@10
    if-nez v3, :cond_16

    #@12
    .line 2771
    :cond_12
    invoke-virtual {v1, v4, v5}, Lcom/android/internal/telephony/uicc/IccRecords;->setVoiceCallForwardingFlag(IZ)V

    #@15
    .line 2796
    :cond_15
    :goto_15
    return-void

    #@16
    .line 2773
    :cond_16
    const/4 v0, 0x0

    #@17
    .local v0, i:I
    array-length v2, p1

    #@18
    .local v2, s:I
    :goto_18
    if-ge v0, v2, :cond_15

    #@1a
    .line 2774
    aget-object v3, p1, v0

    #@1c
    iget v3, v3, Lcom/android/internal/telephony/CallForwardInfo;->serviceClass:I

    #@1e
    and-int/lit8 v3, v3, 0x1

    #@20
    if-eqz v3, :cond_64

    #@22
    .line 2776
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@25
    move-result-object v3

    #@26
    const-string v6, "KT_CFU_FROM_JB"

    #@28
    invoke-static {v3, v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@2b
    move-result v3

    #@2c
    if-eqz v3, :cond_49

    #@2e
    .line 2777
    aget-object v3, p1, v0

    #@30
    iget v3, v3, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    #@32
    if-ne v3, v7, :cond_47

    #@34
    move v3, v4

    #@35
    :goto_35
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/gsm/GSMPhone;->setCallForwardingPreference(Z)V

    #@38
    .line 2778
    aget-object v3, p1, v0

    #@3a
    iget v3, v3, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    #@3c
    if-ne v3, v7, :cond_3f

    #@3e
    move v5, v4

    #@3f
    :cond_3f
    aget-object v3, p1, v0

    #@41
    iget-object v3, v3, Lcom/android/internal/telephony/CallForwardInfo;->number:Ljava/lang/String;

    #@43
    invoke-virtual {v1, v4, v5, v3}, Lcom/android/internal/telephony/uicc/IccRecords;->setVoiceCallForwardingFlag(IZLjava/lang/String;)V

    #@46
    goto :goto_15

    #@47
    :cond_47
    move v3, v5

    #@48
    .line 2777
    goto :goto_35

    #@49
    .line 2785
    :cond_49
    aget-object v3, p1, v0

    #@4b
    iget v3, v3, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    #@4d
    if-ne v3, v4, :cond_62

    #@4f
    move v3, v4

    #@50
    :goto_50
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/gsm/GSMPhone;->setCallForwardingPreference(Z)V

    #@53
    .line 2786
    aget-object v3, p1, v0

    #@55
    iget v3, v3, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    #@57
    if-ne v3, v4, :cond_5a

    #@59
    move v5, v4

    #@5a
    :cond_5a
    aget-object v3, p1, v0

    #@5c
    iget-object v3, v3, Lcom/android/internal/telephony/CallForwardInfo;->number:Ljava/lang/String;

    #@5e
    invoke-virtual {v1, v4, v5, v3}, Lcom/android/internal/telephony/uicc/IccRecords;->setVoiceCallForwardingFlag(IZLjava/lang/String;)V

    #@61
    goto :goto_15

    #@62
    :cond_62
    move v3, v5

    #@63
    .line 2785
    goto :goto_50

    #@64
    .line 2773
    :cond_64
    add-int/lit8 v0, v0, 0x1

    #@66
    goto :goto_18
.end method

.method private handleEctIncallSupplementaryService(Ljava/lang/String;)Z
    .registers 7
    .parameter "dialString"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 1049
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@4
    move-result v1

    #@5
    .line 1051
    .local v1, len:I
    if-eq v1, v2, :cond_9

    #@7
    .line 1052
    const/4 v2, 0x0

    #@8
    .line 1063
    :goto_8
    return v2

    #@9
    .line 1055
    :cond_9
    const-string v3, "GSM"

    #@b
    const-string v4, "MmiCode 4: explicit call transfer"

    #@d
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 1057
    :try_start_10
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->explicitCallTransfer()V
    :try_end_13
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_10 .. :try_end_13} :catch_14

    #@13
    goto :goto_8

    #@14
    .line 1058
    :catch_14
    move-exception v0

    #@15
    .line 1059
    .local v0, e:Lcom/android/internal/telephony/CallStateException;
    const-string v3, "GSM"

    #@17
    const-string v4, "transfer failed"

    #@19
    invoke-static {v3, v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1c
    .line 1061
    sget-object v3, Lcom/android/internal/telephony/Phone$SuppService;->TRANSFER:Lcom/android/internal/telephony/Phone$SuppService;

    #@1e
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifySuppServiceFailed(Lcom/android/internal/telephony/Phone$SuppService;)V

    #@21
    goto :goto_8
.end method

.method private handleEnterEmergencyCallbackMode(J)V
    .registers 6
    .parameter "delayInMillis"

    #@0
    .prologue
    .line 2153
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mIsPhoneInEcmState:Z

    #@2
    const/4 v1, 0x1

    #@3
    if-ne v0, v1, :cond_27

    #@5
    .line 2154
    const-string v0, "GSM"

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "handleEnterEmergencyCallbackMode, delayInMillis= "

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 2156
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mExitEcmRunnable:Ljava/lang/Runnable;

    #@1f
    invoke-virtual {p0, v0, p1, p2}, Lcom/android/internal/telephony/gsm/GSMPhone;->postDelayed(Ljava/lang/Runnable;J)Z

    #@22
    .line 2157
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@24
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@27
    .line 2159
    :cond_27
    return-void
.end method

.method private handleEnterEmergencyCallbackMode(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    .line 2130
    const-string v2, "GSM"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "handleEnterEmergencyCallbackMode,mIsPhoneInEcmState= "

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    iget-boolean v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mIsPhoneInEcmState:Z

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v3

    #@17
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 2133
    iget-boolean v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mIsPhoneInEcmState:Z

    #@1c
    if-nez v2, :cond_41

    #@1e
    .line 2134
    const/4 v2, 0x1

    #@1f
    iput-boolean v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mIsPhoneInEcmState:Z

    #@21
    .line 2136
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->sendEmergencyCallbackModeChange()V

    #@24
    .line 2137
    const-string v2, "ril.cdma.inecmmode"

    #@26
    const-string v3, "true"

    #@28
    invoke-virtual {p0, v2, v3}, Lcom/android/internal/telephony/gsm/GSMPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@2b
    .line 2141
    const-string v2, "ro.cdma.ecmexittimer"

    #@2d
    const-wide/32 v3, 0x493e0

    #@30
    invoke-static {v2, v3, v4}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    #@33
    move-result-wide v0

    #@34
    .line 2143
    .local v0, delayInMillis:J
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mExitEcmRunnable:Ljava/lang/Runnable;

    #@36
    invoke-virtual {p0, v2, v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->postDelayed(Ljava/lang/Runnable;J)Z

    #@39
    .line 2145
    invoke-static {v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->setCurrentTimeForEcm(J)V

    #@3c
    .line 2148
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@3e
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@41
    .line 2150
    .end local v0           #delayInMillis:J
    :cond_41
    return-void
.end method

.method private handleExitEmergencyCallbackMode(Landroid/os/Message;)V
    .registers 9
    .parameter "msg"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 2162
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4
    check-cast v1, Landroid/os/AsyncResult;

    #@6
    .line 2163
    .local v1, ar:Landroid/os/AsyncResult;
    const-string v4, "GSM"

    #@8
    new-instance v5, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v6, "handleExitEmergencyCallbackMode,ar.exception , mIsPhoneInEcmState "

    #@f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v5

    #@13
    iget-object v6, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@15
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v5

    #@19
    iget-boolean v6, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mIsPhoneInEcmState:Z

    #@1b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v5

    #@1f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v5

    #@23
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 2166
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mExitEcmRunnable:Ljava/lang/Runnable;

    #@28
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/gsm/GSMPhone;->removeCallbacks(Ljava/lang/Runnable;)V

    #@2b
    .line 2168
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mEcmExitRespRegistrant:Landroid/os/Registrant;

    #@2d
    if-eqz v4, :cond_34

    #@2f
    .line 2169
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mEcmExitRespRegistrant:Landroid/os/Registrant;

    #@31
    invoke-virtual {v4, v1}, Landroid/os/Registrant;->notifyRegistrant(Landroid/os/AsyncResult;)V

    #@34
    .line 2172
    :cond_34
    iget-object v4, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@36
    if-nez v4, :cond_86

    #@38
    .line 2173
    iget-boolean v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mIsPhoneInEcmState:Z

    #@3a
    if-eqz v4, :cond_7e

    #@3c
    .line 2174
    iput-boolean v3, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mIsPhoneInEcmState:Z

    #@3e
    .line 2176
    iput-boolean v3, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mIsPhoneInEcmExitDelayState:Z

    #@40
    .line 2178
    const-string v4, "ril.cdma.inecmmode"

    #@42
    const-string v5, "false"

    #@44
    invoke-virtual {p0, v4, v5}, Lcom/android/internal/telephony/gsm/GSMPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@47
    .line 2181
    const/4 v4, 0x0

    #@48
    const-string v5, "support_network_change_auto_retry"

    #@4a
    invoke-static {v4, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@4d
    move-result v4

    #@4e
    if-eqz v4, :cond_5d

    #@50
    .line 2182
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@53
    move-result-object v4

    #@54
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@57
    move-result-object v4

    #@58
    const-string v5, "network_change_auto_retry"

    #@5a
    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@5d
    .line 2187
    :cond_5d
    invoke-static {}, Lcom/android/internal/telephony/gsm/GSMPhone;->resetEcmExitTime()V

    #@60
    .line 2190
    const-string v4, "VZW"

    #@62
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@65
    move-result v4

    #@66
    if-eqz v4, :cond_7e

    #@68
    .line 2191
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@6b
    move-result-object v4

    #@6c
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6f
    move-result-object v4

    #@70
    const-string v5, "apn2_disable"

    #@72
    invoke-static {v4, v5, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@75
    move-result v4

    #@76
    if-ne v4, v2, :cond_87

    #@78
    move v0, v2

    #@79
    .line 2195
    .local v0, apn2Disabled:Z
    :goto_79
    if-eqz v0, :cond_7e

    #@7b
    .line 2196
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/gsm/GSMPhone;->setRadioPower(Z)V

    #@7e
    .line 2203
    .end local v0           #apn2Disabled:Z
    :cond_7e
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->sendEmergencyCallbackModeChange()V

    #@81
    .line 2205
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@83
    invoke-virtual {v3, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->setInternalDataEnabled(Z)Z

    #@86
    .line 2207
    :cond_86
    return-void

    #@87
    :cond_87
    move v0, v3

    #@88
    .line 2191
    goto :goto_79
.end method

.method private handleMultipartyIncallSupplementaryService(Ljava/lang/String;)Z
    .registers 6
    .parameter "dialString"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1031
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@4
    move-result v2

    #@5
    if-le v2, v1, :cond_9

    #@7
    .line 1032
    const/4 v1, 0x0

    #@8
    .line 1043
    :goto_8
    return v1

    #@9
    .line 1035
    :cond_9
    const-string v2, "GSM"

    #@b
    const-string v3, "MmiCode 3: merge calls"

    #@d
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 1037
    :try_start_10
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->conference()V
    :try_end_13
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_10 .. :try_end_13} :catch_14

    #@13
    goto :goto_8

    #@14
    .line 1038
    :catch_14
    move-exception v0

    #@15
    .line 1039
    .local v0, e:Lcom/android/internal/telephony/CallStateException;
    const-string v2, "GSM"

    #@17
    const-string v3, "conference failed"

    #@19
    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1c
    .line 1041
    sget-object v2, Lcom/android/internal/telephony/Phone$SuppService;->CONFERENCE:Lcom/android/internal/telephony/Phone$SuppService;

    #@1e
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifySuppServiceFailed(Lcom/android/internal/telephony/Phone$SuppService;)V

    #@21
    goto :goto_8
.end method

.method private handleSetSelectNetwork(Landroid/os/AsyncResult;)V
    .registers 8
    .parameter "ar"

    #@0
    .prologue
    .line 2703
    iget-object v3, p1, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@2
    instance-of v3, v3, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;

    #@4
    if-nez v3, :cond_e

    #@6
    .line 2704
    const-string v3, "GSM"

    #@8
    const-string v4, "unexpected result from user object."

    #@a
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 2747
    :cond_d
    :goto_d
    return-void

    #@e
    .line 2708
    :cond_e
    iget-object v1, p1, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@10
    check-cast v1, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;

    #@12
    .line 2712
    .local v1, nsm:Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;
    iget-object v3, v1, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->message:Landroid/os/Message;

    #@14
    if-eqz v3, :cond_2b

    #@16
    .line 2713
    const-string v3, "GSM"

    #@18
    const-string v4, "sending original message to recipient"

    #@1a
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 2714
    iget-object v3, v1, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->message:Landroid/os/Message;

    #@1f
    iget-object v4, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@21
    iget-object v5, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@23
    invoke-static {v3, v4, v5}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@26
    .line 2715
    iget-object v3, v1, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->message:Landroid/os/Message;

    #@28
    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    #@2b
    .line 2720
    :cond_2b
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@2e
    move-result-object v3

    #@2f
    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@32
    move-result-object v2

    #@33
    .line 2721
    .local v2, sp:Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@36
    move-result-object v0

    #@37
    .line 2722
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v3, "network_selection_key"

    #@39
    iget-object v4, v1, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->operatorNumeric:Ljava/lang/String;

    #@3b
    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@3e
    .line 2727
    const/4 v3, 0x0

    #@3f
    const-string v4, "SAVE_NETWORK_OPERATOR_SHORT_NAME"

    #@41
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@44
    move-result v3

    #@45
    if-nez v3, :cond_80

    #@47
    .line 2728
    const-string v3, "network_selection_name_key"

    #@49
    iget-object v4, v1, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->operatorAlphaLong:Ljava/lang/String;

    #@4b
    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@4e
    .line 2729
    const-string v3, "GSM"

    #@50
    new-instance v4, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v5, "Writing NETWORK_SELECTION_NAME_KEY "

    #@57
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v4

    #@5b
    iget-object v5, v1, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->operatorAlphaLong:Ljava/lang/String;

    #@5d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v4

    #@61
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v4

    #@65
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    .line 2737
    :goto_68
    const-string v3, "KR"

    #@6a
    const-string v4, "LGU"

    #@6c
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@6f
    move-result v3

    #@70
    if-nez v3, :cond_d

    #@72
    .line 2743
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    #@75
    move-result v3

    #@76
    if-nez v3, :cond_d

    #@78
    .line 2744
    const-string v3, "GSM"

    #@7a
    const-string v4, "failed to commit network selection preference"

    #@7c
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7f
    goto :goto_d

    #@80
    .line 2731
    :cond_80
    const-string v3, "network_selection_name_key"

    #@82
    iget-object v4, v1, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->operatorAlphaShort:Ljava/lang/String;

    #@84
    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@87
    .line 2732
    const-string v3, "GSM"

    #@89
    new-instance v4, Ljava/lang/StringBuilder;

    #@8b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@8e
    const-string v5, "Writing NETWORK_SELECTION_NAME_KEY "

    #@90
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v4

    #@94
    iget-object v5, v1, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->operatorAlphaShort:Ljava/lang/String;

    #@96
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v4

    #@9a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9d
    move-result-object v4

    #@9e
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a1
    goto :goto_68
.end method

.method private isCbEnable(I)Z
    .registers 3
    .parameter "action"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 1679
    if-ne p1, v0, :cond_4

    #@3
    :goto_3
    return v0

    #@4
    :cond_4
    const/4 v0, 0x0

    #@5
    goto :goto_3
.end method

.method private isValidCommandInterfaceCFAction(I)Z
    .registers 3
    .parameter "commandInterfaceCFAction"

    #@0
    .prologue
    .line 1560
    packed-switch p1, :pswitch_data_8

    #@3
    .line 1567
    :pswitch_3
    const/4 v0, 0x0

    #@4
    :goto_4
    return v0

    #@5
    .line 1565
    :pswitch_5
    const/4 v0, 0x1

    #@6
    goto :goto_4

    #@7
    .line 1560
    nop

    #@8
    :pswitch_data_8
    .packed-switch 0x0
        :pswitch_5
        :pswitch_5
        :pswitch_3
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method private isValidCommandInterfaceCFReason(I)Z
    .registers 3
    .parameter "commandInterfaceCFReason"

    #@0
    .prologue
    .line 1546
    packed-switch p1, :pswitch_data_8

    #@3
    .line 1555
    const/4 v0, 0x0

    #@4
    :goto_4
    return v0

    #@5
    .line 1553
    :pswitch_5
    const/4 v0, 0x1

    #@6
    goto :goto_4

    #@7
    .line 1546
    nop

    #@8
    :pswitch_data_8
    .packed-switch 0x0
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method private onIncomingUSSD(ILjava/lang/String;)V
    .registers 10
    .parameter "ussdMode"
    .parameter "ussdMessage"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 2010
    if-ne p1, v2, :cond_31

    #@4
    move v3, v2

    #@5
    .line 2013
    .local v3, isUssdRequest:Z
    :goto_5
    if-eqz p1, :cond_33

    #@7
    if-eq p1, v2, :cond_33

    #@9
    .line 2020
    .local v2, isUssdError:Z
    :goto_9
    const/4 v0, 0x0

    #@a
    .line 2021
    .local v0, found:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    const/4 v1, 0x0

    #@b
    .local v1, i:I
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mPendingMMIs:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@10
    move-result v5

    #@11
    .local v5, s:I
    :goto_11
    if-ge v1, v5, :cond_29

    #@13
    .line 2022
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mPendingMMIs:Ljava/util/ArrayList;

    #@15
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@18
    move-result-object v6

    #@19
    check-cast v6, Lcom/android/internal/telephony/gsm/GsmMmiCode;

    #@1b
    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isPendingUSSD()Z

    #@1e
    move-result v6

    #@1f
    if-eqz v6, :cond_35

    #@21
    .line 2023
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mPendingMMIs:Ljava/util/ArrayList;

    #@23
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@26
    move-result-object v0

    #@27
    .end local v0           #found:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    check-cast v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;

    #@29
    .line 2028
    .restart local v0       #found:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    :cond_29
    if-eqz v0, :cond_3c

    #@2b
    .line 2031
    if-eqz v2, :cond_38

    #@2d
    .line 2032
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->onUssdFinishedError()V

    #@30
    .line 2050
    :cond_30
    :goto_30
    return-void

    #@31
    .end local v0           #found:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    .end local v1           #i:I
    .end local v2           #isUssdError:Z
    .end local v3           #isUssdRequest:Z
    .end local v5           #s:I
    :cond_31
    move v3, v6

    #@32
    .line 2010
    goto :goto_5

    #@33
    .restart local v3       #isUssdRequest:Z
    :cond_33
    move v2, v6

    #@34
    .line 2013
    goto :goto_9

    #@35
    .line 2021
    .restart local v0       #found:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    .restart local v1       #i:I
    .restart local v2       #isUssdError:Z
    .restart local v5       #s:I
    :cond_35
    add-int/lit8 v1, v1, 0x1

    #@37
    goto :goto_11

    #@38
    .line 2034
    :cond_38
    invoke-virtual {v0, p2, v3}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->onUssdFinished(Ljava/lang/String;Z)V

    #@3b
    goto :goto_30

    #@3c
    .line 2041
    :cond_3c
    if-nez v2, :cond_30

    #@3e
    if-eqz p2, :cond_30

    #@40
    .line 2043
    iget-object v6, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccApplication:Ljava/util/concurrent/atomic/AtomicReference;

    #@42
    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@45
    move-result-object v6

    #@46
    check-cast v6, Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@48
    invoke-static {p2, v3, p0, v6}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->newNetworkInitiatedUssd(Ljava/lang/String;ZLcom/android/internal/telephony/gsm/GSMPhone;Lcom/android/internal/telephony/uicc/UiccCardApplication;)Lcom/android/internal/telephony/gsm/GsmMmiCode;

    #@4b
    move-result-object v4

    #@4c
    .line 2047
    .local v4, mmi:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    invoke-direct {p0, v4}, Lcom/android/internal/telephony/gsm/GSMPhone;->onNetworkInitiatedUssd(Lcom/android/internal/telephony/gsm/GsmMmiCode;)V

    #@4f
    goto :goto_30
.end method

.method private onIncomingUSSD_KT([Ljava/lang/String;)V
    .registers 6
    .parameter "ussdResult"

    #@0
    .prologue
    const/4 v3, 0x2

    #@1
    const/4 v2, 0x1

    #@2
    const/4 v1, 0x0

    #@3
    .line 1952
    aget-object v0, p1, v2

    #@5
    if-nez v0, :cond_13

    #@7
    .line 1955
    aget-object v0, p1, v1

    #@9
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@c
    move-result v0

    #@d
    aget-object v1, p1, v3

    #@f
    invoke-direct {p0, v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->onIncomingUSSD(ILjava/lang/String;)V

    #@12
    .line 2000
    :goto_12
    return-void

    #@13
    .line 1959
    :cond_13
    aget-object v0, p1, v2

    #@15
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@18
    move-result v0

    #@19
    sparse-switch v0, :sswitch_data_aa

    #@1c
    .line 1996
    aget-object v0, p1, v1

    #@1e
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@21
    move-result v0

    #@22
    aget-object v1, p1, v3

    #@24
    invoke-direct {p0, v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->onIncomingUSSD(ILjava/lang/String;)V

    #@27
    goto :goto_12

    #@28
    .line 1963
    :sswitch_28
    aget-object v0, p1, v1

    #@2a
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@2d
    move-result v0

    #@2e
    const-string v1, "kt_ussd_err_code_0x09"

    #@30
    invoke-static {v1}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    invoke-direct {p0, v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->onIncomingUSSD(ILjava/lang/String;)V

    #@37
    goto :goto_12

    #@38
    .line 1967
    :sswitch_38
    aget-object v0, p1, v1

    #@3a
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@3d
    move-result v0

    #@3e
    const-string v1, "kt_ussd_err_code_0x0C"

    #@40
    invoke-static {v1}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@43
    move-result-object v1

    #@44
    invoke-direct {p0, v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->onIncomingUSSD(ILjava/lang/String;)V

    #@47
    goto :goto_12

    #@48
    .line 1971
    :sswitch_48
    aget-object v0, p1, v1

    #@4a
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@4d
    move-result v0

    #@4e
    const-string v1, "kt_ussd_err_code_0x1B"

    #@50
    invoke-static {v1}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@53
    move-result-object v1

    #@54
    invoke-direct {p0, v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->onIncomingUSSD(ILjava/lang/String;)V

    #@57
    goto :goto_12

    #@58
    .line 1975
    :sswitch_58
    aget-object v0, p1, v1

    #@5a
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@5d
    move-result v0

    #@5e
    const-string v1, "kt_ussd_err_code_0x22"

    #@60
    invoke-static {v1}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@63
    move-result-object v1

    #@64
    invoke-direct {p0, v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->onIncomingUSSD(ILjava/lang/String;)V

    #@67
    goto :goto_12

    #@68
    .line 1979
    :sswitch_68
    aget-object v0, p1, v1

    #@6a
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@6d
    move-result v0

    #@6e
    const-string v1, "kt_ussd_err_code_0x23"

    #@70
    invoke-static {v1}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@73
    move-result-object v1

    #@74
    invoke-direct {p0, v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->onIncomingUSSD(ILjava/lang/String;)V

    #@77
    goto :goto_12

    #@78
    .line 1983
    :sswitch_78
    aget-object v0, p1, v1

    #@7a
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@7d
    move-result v0

    #@7e
    const-string v1, "kt_ussd_err_code_0x24"

    #@80
    invoke-static {v1}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@83
    move-result-object v1

    #@84
    invoke-direct {p0, v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->onIncomingUSSD(ILjava/lang/String;)V

    #@87
    goto :goto_12

    #@88
    .line 1987
    :sswitch_88
    aget-object v0, p1, v1

    #@8a
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@8d
    move-result v0

    #@8e
    const-string v1, "kt_ussd_err_code_0x47"

    #@90
    invoke-static {v1}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@93
    move-result-object v1

    #@94
    invoke-direct {p0, v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->onIncomingUSSD(ILjava/lang/String;)V

    #@97
    goto/16 :goto_12

    #@99
    .line 1991
    :sswitch_99
    aget-object v0, p1, v1

    #@9b
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@9e
    move-result v0

    #@9f
    const-string v1, "kt_ussd_err_code_0x48"

    #@a1
    invoke-static {v1}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@a4
    move-result-object v1

    #@a5
    invoke-direct {p0, v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->onIncomingUSSD(ILjava/lang/String;)V

    #@a8
    goto/16 :goto_12

    #@aa
    .line 1959
    :sswitch_data_aa
    .sparse-switch
        0x9 -> :sswitch_28
        0xc -> :sswitch_38
        0x1b -> :sswitch_48
        0x22 -> :sswitch_58
        0x23 -> :sswitch_68
        0x24 -> :sswitch_78
        0x47 -> :sswitch_88
        0x48 -> :sswitch_99
    .end sparse-switch
.end method

.method private onNetworkInitiatedUssd(Lcom/android/internal/telephony/gsm/GsmMmiCode;)V
    .registers 5
    .parameter "mmi"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1944
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mMmiCompleteRegistrants:Landroid/os/RegistrantList;

    #@3
    new-instance v1, Landroid/os/AsyncResult;

    #@5
    invoke-direct {v1, v2, p1, v2}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@8
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@b
    .line 1946
    return-void
.end method

.method private processIccRecordEvents(I)V
    .registers 2
    .parameter "eventCode"

    #@0
    .prologue
    .line 2666
    packed-switch p1, :pswitch_data_c

    #@3
    .line 2674
    :goto_3
    return-void

    #@4
    .line 2668
    :pswitch_4
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifyCallForwardingIndicator()V

    #@7
    goto :goto_3

    #@8
    .line 2671
    :pswitch_8
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifyMessageWaitingIndicator()V

    #@b
    goto :goto_3

    #@c
    .line 2666
    :pswitch_data_c
    .packed-switch 0x0
        :pswitch_8
        :pswitch_4
    .end packed-switch
.end method

.method private registerForSimRecordEvents()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2878
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@3
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Lcom/android/internal/telephony/uicc/IccRecords;

    #@9
    .line 2879
    .local v0, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-nez v0, :cond_c

    #@b
    .line 2886
    :goto_b
    return-void

    #@c
    .line 2882
    :cond_c
    const/16 v1, 0x1c

    #@e
    invoke-virtual {v0, p0, v1, v2}, Lcom/android/internal/telephony/uicc/IccRecords;->registerForNetworkSelectionModeAutomatic(Landroid/os/Handler;ILjava/lang/Object;)V

    #@11
    .line 2884
    const/16 v1, 0x1d

    #@13
    invoke-virtual {v0, p0, v1, v2}, Lcom/android/internal/telephony/uicc/IccRecords;->registerForRecordsEvents(Landroid/os/Handler;ILjava/lang/Object;)V

    #@16
    .line 2885
    const/4 v1, 0x3

    #@17
    invoke-virtual {v0, p0, v1, v2}, Lcom/android/internal/telephony/uicc/IccRecords;->registerForRecordsLoaded(Landroid/os/Handler;ILjava/lang/Object;)V

    #@1a
    goto :goto_b
.end method

.method private unregisterForSimRecordEvents()V
    .registers 3

    #@0
    .prologue
    .line 2889
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@2
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/uicc/IccRecords;

    #@8
    .line 2890
    .local v0, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-nez v0, :cond_b

    #@a
    .line 2896
    :goto_a
    return-void

    #@b
    .line 2893
    :cond_b
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/uicc/IccRecords;->unregisterForNetworkSelectionModeAutomatic(Landroid/os/Handler;)V

    #@e
    .line 2894
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/uicc/IccRecords;->unregisterForRecordsEvents(Landroid/os/Handler;)V

    #@11
    .line 2895
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/uicc/IccRecords;->unregisterForRecordsLoaded(Landroid/os/Handler;)V

    #@14
    goto :goto_a
.end method

.method private updateCallForwardStatus()V
    .registers 5

    #@0
    .prologue
    .line 1929
    const-string v2, "GSM"

    #@2
    const-string v3, "updateCallForwardStatus got sim records"

    #@4
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1930
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@9
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@c
    move-result-object v1

    #@d
    check-cast v1, Lcom/android/internal/telephony/uicc/IccRecords;

    #@f
    .line 1931
    .local v1, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v1, :cond_22

    #@11
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/IccRecords;->isCallForwardStatusStored()Z

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_22

    #@17
    .line 1933
    const-string v2, "GSM"

    #@19
    const-string v3, "Callforwarding info is present on sim"

    #@1b
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 1934
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifyCallForwardingIndicator()V

    #@21
    .line 1939
    :goto_21
    return-void

    #@22
    .line 1936
    :cond_22
    const/16 v2, 0x4b

    #@24
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->obtainMessage(I)Landroid/os/Message;

    #@27
    move-result-object v0

    #@28
    .line 1937
    .local v0, msg:Landroid/os/Message;
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GSMPhone;->sendMessage(Landroid/os/Message;)Z

    #@2b
    goto :goto_21
.end method

.method private updateVoiceMail()V
    .registers 4

    #@0
    .prologue
    .line 630
    const/4 v0, 0x0

    #@1
    .line 631
    .local v0, countVoiceMessages:I
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@3
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@6
    move-result-object v1

    #@7
    check-cast v1, Lcom/android/internal/telephony/uicc/IccRecords;

    #@9
    .line 632
    .local v1, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v1, :cond_f

    #@b
    .line 634
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/IccRecords;->getVoiceMessageCount()I

    #@e
    move-result v0

    #@f
    .line 636
    :cond_f
    if-nez v0, :cond_15

    #@11
    .line 637
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getStoredVoiceMessageCount()I

    #@14
    move-result v0

    #@15
    .line 639
    :cond_15
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GSMPhone;->setVoiceMessageCount(I)V

    #@18
    .line 640
    return-void
.end method


# virtual methods
.method public IsVMNumberNotInSIM()Z
    .registers 3

    #@0
    .prologue
    .line 2940
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@2
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/uicc/IccRecords;

    #@8
    .line 2941
    .local v0, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v0, :cond_f

    #@a
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/IccRecords;->IsVMNumberNotInSIM()Z

    #@d
    move-result v1

    #@e
    :goto_e
    return v1

    #@f
    :cond_f
    const/4 v1, 0x1

    #@10
    goto :goto_e
.end method

.method public acceptCall()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 859
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->acceptCall()V

    #@5
    .line 860
    return-void
.end method

.method public activateCellBroadcastSms(ILandroid/os/Message;)V
    .registers 5
    .parameter "activate"
    .parameter "response"

    #@0
    .prologue
    .line 2819
    const-string v0, "GSM"

    #@2
    const-string v1, "[GSMPhone] activateCellBroadcastSms() is obsolete; use SmsManager"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2820
    invoke-virtual {p2}, Landroid/os/Message;->sendToTarget()V

    #@a
    .line 2821
    return-void
.end method

.method public akaAuthenticate([B[BLandroid/os/Message;)V
    .registers 5
    .parameter "rand"
    .parameter "autn"
    .parameter "onComplete"

    #@0
    .prologue
    .line 3378
    new-instance v0, Lcom/android/internal/telephony/gsm/GSMPhone$PendingAka;

    #@2
    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/internal/telephony/gsm/GSMPhone$PendingAka;-><init>(Lcom/android/internal/telephony/gsm/GSMPhone;[B[BLandroid/os/Message;)V

    #@5
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gsm/GSMPhone;->ensureISimSession(Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;)V

    #@8
    .line 3379
    return-void
.end method

.method public canConference()Z
    .registers 2

    #@0
    .prologue
    .line 873
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->canConference()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public canDial()Z
    .registers 2

    #@0
    .prologue
    .line 877
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->canDial()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public canTransfer()Z
    .registers 2

    #@0
    .prologue
    .line 889
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->canTransfer()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public cancelManualSearchingRequest()V
    .registers 4

    #@0
    .prologue
    .line 1809
    const/16 v1, 0x27

    #@2
    const/4 v2, 0x0

    #@3
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 1811
    .local v0, msg:Landroid/os/Message;
    const-string v1, "GSMPhone"

    #@9
    const-string v2, "cancelManualSearchingRequest()"

    #@b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1813
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@10
    invoke-interface {v1, v0}, Lcom/android/internal/telephony/CommandsInterface;->cancelManualSearchingRequest(Landroid/os/Message;)V

    #@13
    .line 1814
    return-void
.end method

.method public checkDataProfileEx(II)Z
    .registers 4
    .parameter "type"
    .parameter "Q_IPv"

    #@0
    .prologue
    .line 3150
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/telephony/DataConnectionTracker;->checkDataProfileEx(II)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public clearDisconnected()V
    .registers 2

    #@0
    .prologue
    .line 885
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->clearDisconnected()V

    #@5
    .line 886
    return-void
.end method

.method public conference()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 881
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->conference()V

    #@5
    .line 882
    return-void
.end method

.method public dial(Ljava/lang/String;)Lcom/android/internal/telephony/Connection;
    .registers 3
    .parameter "dialString"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1130
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/telephony/gsm/GSMPhone;->dial(Ljava/lang/String;Lcom/android/internal/telephony/UUSInfo;)Lcom/android/internal/telephony/Connection;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public dial(Ljava/lang/String;Lcom/android/internal/telephony/UUSInfo;)Lcom/android/internal/telephony/Connection;
    .registers 13
    .parameter "dialString"
    .parameter "uusInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 1136
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v3

    #@6
    .line 1139
    .local v3, newDialString:Ljava/lang/String;
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/gsm/GSMPhone;->handleInCallMmiCommands(Ljava/lang/String;)Z

    #@9
    move-result v5

    #@a
    if-eqz v5, :cond_e

    #@c
    move-object v5, v6

    #@d
    .line 1213
    :goto_d
    return-object v5

    #@e
    .line 1144
    :cond_e
    invoke-static {v3}, Landroid/telephony/PhoneNumberUtils;->extractNetworkPortionAlt(Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v2

    #@12
    .line 1145
    .local v2, networkPortion:Ljava/lang/String;
    iget-object v5, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccApplication:Ljava/util/concurrent/atomic/AtomicReference;

    #@14
    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@17
    move-result-object v5

    #@18
    check-cast v5, Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@1a
    invoke-static {v2, p0, v5}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->newFromDialString(Ljava/lang/String;Lcom/android/internal/telephony/gsm/GSMPhone;Lcom/android/internal/telephony/uicc/UiccCardApplication;)Lcom/android/internal/telephony/gsm/GsmMmiCode;

    #@1d
    move-result-object v1

    #@1e
    .line 1150
    .local v1, mmi:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    sget-boolean v5, Lcom/android/internal/telephony/gsm/GSMPhone;->ENABLE_PRIVACY_LOG_CALL:Z

    #@20
    if-eqz v5, :cond_a0

    #@22
    .line 1151
    const-string v5, "GSM"

    #@24
    new-instance v7, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v8, "dialing w/ mmi \'"

    #@2b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v7

    #@2f
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v7

    #@33
    const-string v8, "\'..."

    #@35
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v7

    #@39
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v7

    #@3d
    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 1160
    :goto_40
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@43
    move-result-object v5

    #@44
    const-string v7, "eccnoti"

    #@46
    invoke-static {v5, v7}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@49
    move-result v5

    #@4a
    if-eqz v5, :cond_5c

    #@4c
    .line 1161
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumber(Ljava/lang/String;)Z

    #@4f
    move-result v5

    #@50
    if-eqz v5, :cond_a8

    #@52
    .line 1162
    const-string v5, "GSM"

    #@54
    const-string v7, " EccNoti SetProcessing"

    #@56
    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    .line 1163
    invoke-static {v9}, Lcom/android/internal/telephony/gsm/EccNoti;->setProcessECCNoti(Z)V

    #@5c
    .line 1170
    :cond_5c
    :goto_5c
    if-nez v1, :cond_ce

    #@5e
    .line 1172
    iget-object v5, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@60
    const-string v6, "set_clir_option_by_call_setting"

    #@62
    invoke-static {v5, v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@65
    move-result v5

    #@66
    if-eqz v5, :cond_c6

    #@68
    .line 1173
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@6b
    move-result-object v5

    #@6c
    invoke-static {v5}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@6f
    move-result-object v4

    #@70
    .line 1174
    .local v4, pref:Landroid/content/SharedPreferences;
    const-string v5, "button_clir_key"

    #@72
    const-string v6, "DEFAULT"

    #@74
    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@77
    move-result-object v0

    #@78
    .line 1175
    .local v0, list_value:Ljava/lang/String;
    const-string v5, "GSM"

    #@7a
    new-instance v6, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    const-string v7, "ATT CLIR setting value "

    #@81
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v6

    #@85
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v6

    #@89
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8c
    move-result-object v6

    #@8d
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@90
    .line 1177
    const-string v5, "HIDE"

    #@92
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@95
    move-result v5

    #@96
    if-eqz v5, :cond_ac

    #@98
    .line 1178
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@9a
    invoke-virtual {v5, v3, v9, p2}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->dial(Ljava/lang/String;ILcom/android/internal/telephony/UUSInfo;)Lcom/android/internal/telephony/Connection;

    #@9d
    move-result-object v5

    #@9e
    goto/16 :goto_d

    #@a0
    .line 1154
    .end local v0           #list_value:Ljava/lang/String;
    .end local v4           #pref:Landroid/content/SharedPreferences;
    :cond_a0
    const-string v5, "GSM"

    #@a2
    const-string v7, "dialing w/ mmi"

    #@a4
    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a7
    goto :goto_40

    #@a8
    .line 1165
    :cond_a8
    invoke-static {}, Lcom/android/internal/telephony/gsm/EccNoti;->CompleteECCNoti()V

    #@ab
    goto :goto_5c

    #@ac
    .line 1180
    .restart local v0       #list_value:Ljava/lang/String;
    .restart local v4       #pref:Landroid/content/SharedPreferences;
    :cond_ac
    const-string v5, "SHOW"

    #@ae
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b1
    move-result v5

    #@b2
    if-eqz v5, :cond_bd

    #@b4
    .line 1181
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@b6
    const/4 v6, 0x2

    #@b7
    invoke-virtual {v5, v3, v6, p2}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->dial(Ljava/lang/String;ILcom/android/internal/telephony/UUSInfo;)Lcom/android/internal/telephony/Connection;

    #@ba
    move-result-object v5

    #@bb
    goto/16 :goto_d

    #@bd
    .line 1184
    :cond_bd
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@bf
    const/4 v6, 0x0

    #@c0
    invoke-virtual {v5, v3, v6, p2}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->dial(Ljava/lang/String;ILcom/android/internal/telephony/UUSInfo;)Lcom/android/internal/telephony/Connection;

    #@c3
    move-result-object v5

    #@c4
    goto/16 :goto_d

    #@c6
    .line 1189
    .end local v0           #list_value:Ljava/lang/String;
    .end local v4           #pref:Landroid/content/SharedPreferences;
    :cond_c6
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@c8
    invoke-virtual {v5, v3, p2}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->dial(Ljava/lang/String;Lcom/android/internal/telephony/UUSInfo;)Lcom/android/internal/telephony/Connection;

    #@cb
    move-result-object v5

    #@cc
    goto/16 :goto_d

    #@ce
    .line 1191
    :cond_ce
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isTemporaryModeCLIR()Z

    #@d1
    move-result v5

    #@d2
    if-eqz v5, :cond_e2

    #@d4
    .line 1192
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@d6
    iget-object v6, v1, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialingNumber:Ljava/lang/String;

    #@d8
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->getCLIRMode()I

    #@db
    move-result v7

    #@dc
    invoke-virtual {v5, v6, v7, p2}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->dial(Ljava/lang/String;ILcom/android/internal/telephony/UUSInfo;)Lcom/android/internal/telephony/Connection;

    #@df
    move-result-object v5

    #@e0
    goto/16 :goto_d

    #@e2
    .line 1194
    :cond_e2
    const-string v5, "vzw_gfit"

    #@e4
    invoke-static {v6, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@e7
    move-result v5

    #@e8
    if-eqz v5, :cond_fa

    #@ea
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isGlobalDevMmi()Z

    #@ed
    move-result v5

    #@ee
    if-eqz v5, :cond_fa

    #@f0
    .line 1195
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@f2
    iget-object v6, v1, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialingNumber:Ljava/lang/String;

    #@f4
    invoke-virtual {v5, v6, p2}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->dial(Ljava/lang/String;Lcom/android/internal/telephony/UUSInfo;)Lcom/android/internal/telephony/Connection;

    #@f7
    move-result-object v5

    #@f8
    goto/16 :goto_d

    #@fa
    .line 1198
    :cond_fa
    const-string v5, "KR"

    #@fc
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@ff
    move-result v5

    #@100
    if-eqz v5, :cond_110

    #@102
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isTemporaryModeCLIRKorea()Z

    #@105
    move-result v5

    #@106
    if-eqz v5, :cond_110

    #@108
    .line 1200
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@10a
    invoke-virtual {v5, v3, p2}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->dial(Ljava/lang/String;Lcom/android/internal/telephony/UUSInfo;)Lcom/android/internal/telephony/Connection;

    #@10d
    move-result-object v5

    #@10e
    goto/16 :goto_d

    #@110
    .line 1202
    :cond_110
    const-string v5, "KR"

    #@112
    const-string v7, "SKT"

    #@114
    invoke-static {v5, v7}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@117
    move-result v5

    #@118
    if-eqz v5, :cond_128

    #@11a
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isTemporaryModeSKTKorea()Z

    #@11d
    move-result v5

    #@11e
    if-eqz v5, :cond_128

    #@120
    .line 1203
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@122
    invoke-virtual {v5, v3, p2}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->dial(Ljava/lang/String;Lcom/android/internal/telephony/UUSInfo;)Lcom/android/internal/telephony/Connection;

    #@125
    move-result-object v5

    #@126
    goto/16 :goto_d

    #@128
    .line 1204
    :cond_128
    const-string v5, "KR"

    #@12a
    const-string v7, "KT"

    #@12c
    invoke-static {v5, v7}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@12f
    move-result v5

    #@130
    if-eqz v5, :cond_140

    #@132
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isTemporaryModeKTKorea()Z

    #@135
    move-result v5

    #@136
    if-eqz v5, :cond_140

    #@138
    .line 1205
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@13a
    invoke-virtual {v5, v3, p2}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->dial(Ljava/lang/String;Lcom/android/internal/telephony/UUSInfo;)Lcom/android/internal/telephony/Connection;

    #@13d
    move-result-object v5

    #@13e
    goto/16 :goto_d

    #@140
    .line 1208
    :cond_140
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mPendingMMIs:Ljava/util/ArrayList;

    #@142
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@145
    .line 1209
    iget-object v5, p0, Lcom/android/internal/telephony/PhoneBase;->mMmiRegistrants:Landroid/os/RegistrantList;

    #@147
    new-instance v7, Landroid/os/AsyncResult;

    #@149
    invoke-direct {v7, v6, v1, v6}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@14c
    invoke-virtual {v5, v7}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@14f
    .line 1210
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->processCode()V

    #@152
    move-object v5, v6

    #@153
    .line 1213
    goto/16 :goto_d
.end method

.method public dial(Ljava/lang/String;Z)Lcom/android/internal/telephony/Connection;
    .registers 10
    .parameter "dialString"
    .parameter "subaddress"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1221
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    #@4
    move-result-object v2

    #@5
    .line 1224
    .local v2, newDialString:Ljava/lang/String;
    sget-boolean v3, Lcom/android/internal/telephony/gsm/GSMPhone;->ENABLE_PRIVACY_LOG_CALL:Z

    #@7
    if-eqz v3, :cond_43

    #@9
    .line 1225
    const-string v3, "GSM"

    #@b
    new-instance v5, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v6, "[maekyun]dial, dialString = "

    #@12
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v5

    #@16
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v5

    #@1a
    const-string v6, ", subaddress = "

    #@1c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v5

    #@20
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@23
    move-result-object v5

    #@24
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v5

    #@28
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 1226
    const-string v3, "GSM"

    #@2d
    new-instance v5, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v6, "[maekyun]dial, newDialString = "

    #@34
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v5

    #@38
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v5

    #@3c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v5

    #@40
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 1231
    :cond_43
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->handleInCallMmiCommands(Ljava/lang/String;)Z

    #@46
    move-result v3

    #@47
    if-eqz v3, :cond_4b

    #@49
    move-object v3, v4

    #@4a
    .line 1266
    :goto_4a
    return-object v3

    #@4b
    .line 1236
    :cond_4b
    invoke-static {v2}, Landroid/telephony/PhoneNumberUtils;->extractNetworkPortionAlt(Ljava/lang/String;)Ljava/lang/String;

    #@4e
    move-result-object v1

    #@4f
    .line 1237
    .local v1, networkPortion:Ljava/lang/String;
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccApplication:Ljava/util/concurrent/atomic/AtomicReference;

    #@51
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@54
    move-result-object v3

    #@55
    check-cast v3, Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@57
    invoke-static {v1, p0, v3}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->newFromDialString(Ljava/lang/String;Lcom/android/internal/telephony/gsm/GSMPhone;Lcom/android/internal/telephony/uicc/UiccCardApplication;)Lcom/android/internal/telephony/gsm/GsmMmiCode;

    #@5a
    move-result-object v0

    #@5b
    .line 1242
    .local v0, mmi:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    sget-boolean v3, Lcom/android/internal/telephony/gsm/GSMPhone;->ENABLE_PRIVACY_LOG_CALL:Z

    #@5d
    if-eqz v3, :cond_86

    #@5f
    .line 1243
    const-string v3, "GSM"

    #@61
    new-instance v5, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    const-string v6, "dialing w/ mmi \'"

    #@68
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v5

    #@6c
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v5

    #@70
    const-string v6, "\'..."

    #@72
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v5

    #@76
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v5

    #@7a
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7d
    .line 1250
    :goto_7d
    if-nez v0, :cond_8e

    #@7f
    .line 1251
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@81
    invoke-virtual {v3, v2, p2}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->dial(Ljava/lang/String;Z)Lcom/android/internal/telephony/Connection;

    #@84
    move-result-object v3

    #@85
    goto :goto_4a

    #@86
    .line 1245
    :cond_86
    const-string v3, "GSM"

    #@88
    const-string v5, "dialing w/ mmi"

    #@8a
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8d
    goto :goto_7d

    #@8e
    .line 1252
    :cond_8e
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isTemporaryModeCLIR()Z

    #@91
    move-result v3

    #@92
    if-eqz v3, :cond_bf

    #@94
    .line 1255
    sget-boolean v3, Lcom/android/internal/telephony/gsm/GSMPhone;->ENABLE_PRIVACY_LOG_CALL:Z

    #@96
    if-eqz v3, :cond_b2

    #@98
    .line 1256
    const-string v3, "GSM"

    #@9a
    new-instance v4, Ljava/lang/StringBuilder;

    #@9c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9f
    const-string v5, "[maekyun]dial, mmi.dialingNumber = "

    #@a1
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v4

    #@a5
    iget-object v5, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialingNumber:Ljava/lang/String;

    #@a7
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v4

    #@ab
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ae
    move-result-object v4

    #@af
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b2
    .line 1259
    :cond_b2
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@b4
    iget-object v4, v0, Lcom/android/internal/telephony/gsm/GsmMmiCode;->dialingNumber:Ljava/lang/String;

    #@b6
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->getCLIRMode()I

    #@b9
    move-result v5

    #@ba
    invoke-virtual {v3, v4, v5, p2}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->dial(Ljava/lang/String;IZ)Lcom/android/internal/telephony/Connection;

    #@bd
    move-result-object v3

    #@be
    goto :goto_4a

    #@bf
    .line 1261
    :cond_bf
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mPendingMMIs:Ljava/util/ArrayList;

    #@c1
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c4
    .line 1262
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneBase;->mMmiRegistrants:Landroid/os/RegistrantList;

    #@c6
    new-instance v5, Landroid/os/AsyncResult;

    #@c8
    invoke-direct {v5, v4, v0, v4}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@cb
    invoke-virtual {v3, v5}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@ce
    .line 1263
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->processCode()V

    #@d1
    move-object v3, v4

    #@d2
    .line 1266
    goto/16 :goto_4a
.end method

.method public disableLocationUpdates()V
    .registers 2

    #@0
    .prologue
    .line 1872
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSST:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->disableLocationUpdates()V

    #@5
    .line 1873
    return-void
.end method

.method public dispose()V
    .registers 4

    #@0
    .prologue
    .line 514
    sget-object v1, Lcom/android/internal/telephony/PhoneProxy;->lockForRadioTechnologyChange:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 515
    :try_start_3
    invoke-super {p0}, Lcom/android/internal/telephony/PhoneBase;->dispose()V

    #@6
    .line 518
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@8
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForAvailable(Landroid/os/Handler;)V

    #@b
    .line 519
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->unregisterForSimRecordEvents()V

    #@e
    .line 520
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@10
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForOffOrNotAvailable(Landroid/os/Handler;)V

    #@13
    .line 521
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@15
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForOn(Landroid/os/Handler;)V

    #@18
    .line 522
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSST:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@1a
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->unregisterForNetworkAttached(Landroid/os/Handler;)V

    #@1d
    .line 523
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1f
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unSetOnUSSD(Landroid/os/Handler;)V

    #@22
    .line 524
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@24
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unSetOnSuppServiceNotification(Landroid/os/Handler;)V

    #@27
    .line 525
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@29
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unSetOnSS(Landroid/os/Handler;)V

    #@2c
    .line 527
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@2f
    move-result-object v0

    #@30
    const-string v2, "support_emergency_callback_mode_for_gsm"

    #@32
    invoke-static {v0, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@35
    move-result v0

    #@36
    if-eqz v0, :cond_42

    #@38
    .line 528
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@3a
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForExitEmergencyCallbackMode(Landroid/os/Handler;)V

    #@3d
    .line 529
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mExitEcmRunnable:Ljava/lang/Runnable;

    #@3f
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GSMPhone;->removeCallbacks(Ljava/lang/Runnable;)V

    #@42
    .line 533
    :cond_42
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mPendingMMIs:Ljava/util/ArrayList;

    #@44
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@47
    .line 536
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@49
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->dispose()V

    #@4c
    .line 537
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@4e
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnectionTracker;->dispose()V

    #@51
    .line 539
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mLGEDataConnectionTracker:Lcom/android/internal/telephony/LGEDataConnectionTracker;

    #@53
    invoke-virtual {v0}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->dispose()V

    #@56
    .line 541
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSST:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@58
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->dispose()V

    #@5b
    .line 542
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSimPhoneBookIntManager:Lcom/android/internal/telephony/gsm/SimPhoneBookInterfaceManager;

    #@5d
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/SimPhoneBookInterfaceManager;->dispose()V

    #@60
    .line 543
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSubInfo:Lcom/android/internal/telephony/PhoneSubInfo;

    #@62
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneSubInfo;->dispose()V

    #@65
    .line 546
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@67
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->SimStateReceiver:Landroid/content/BroadcastReceiver;

    #@69
    invoke-virtual {v0, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@6c
    .line 551
    const/4 v0, 0x0

    #@6d
    const-string v2, "vzw_gfit"

    #@6f
    invoke-static {v0, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@72
    move-result v0

    #@73
    if-eqz v0, :cond_7a

    #@75
    .line 552
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->gfUtils:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@77
    invoke-virtual {v0}, Lcom/android/internal/telephony/gfit/GfitUtils;->dispose()V

    #@7a
    .line 555
    :cond_7a
    monitor-exit v1

    #@7b
    .line 556
    return-void

    #@7c
    .line 555
    :catchall_7c
    move-exception v0

    #@7d
    monitor-exit v1
    :try_end_7e
    .catchall {:try_start_3 .. :try_end_7e} :catchall_7c

    #@7e
    throw v0
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 6
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 2900
    const-string v0, "GSMPhone extends:"

    #@2
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5
    .line 2901
    invoke-super {p0, p1, p2, p3}, Lcom/android/internal/telephony/PhoneBase;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@8
    .line 2902
    new-instance v0, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v1, " mCT="

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@20
    .line 2903
    new-instance v0, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v1, " mSST="

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSST:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v0

    #@31
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v0

    #@35
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@38
    .line 2904
    new-instance v0, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v1, " mPendingMMIs="

    #@3f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v0

    #@43
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mPendingMMIs:Ljava/util/ArrayList;

    #@45
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v0

    #@49
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v0

    #@4d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@50
    .line 2905
    new-instance v0, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v1, " mSimPhoneBookIntManager="

    #@57
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v0

    #@5b
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSimPhoneBookIntManager:Lcom/android/internal/telephony/gsm/SimPhoneBookInterfaceManager;

    #@5d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v0

    #@61
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v0

    #@65
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@68
    .line 2906
    new-instance v0, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v1, " mSubInfo="

    #@6f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v0

    #@73
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSubInfo:Lcom/android/internal/telephony/PhoneSubInfo;

    #@75
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v0

    #@79
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v0

    #@7d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@80
    .line 2909
    new-instance v0, Ljava/lang/StringBuilder;

    #@82
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@85
    const-string v1, " mVmNumber="

    #@87
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v0

    #@8b
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mVmNumber:Ljava/lang/String;

    #@8d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v0

    #@91
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v0

    #@95
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@98
    .line 2910
    return-void
.end method

.method public enableLocationUpdates()V
    .registers 2

    #@0
    .prologue
    .line 1868
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSST:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->enableLocationUpdates()V

    #@5
    .line 1869
    return-void
.end method

.method ensureISimSession(Landroid/os/Message;)V
    .registers 3
    .parameter "onComplete"

    #@0
    .prologue
    .line 3374
    new-instance v0, Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;

    #@2
    invoke-direct {v0, p0, p1}, Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;-><init>(Lcom/android/internal/telephony/gsm/GSMPhone;Landroid/os/Message;)V

    #@5
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gsm/GSMPhone;->ensureISimSession(Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;)V

    #@8
    .line 3375
    return-void
.end method

.method public exitEmergencyCallbackMode()V
    .registers 3

    #@0
    .prologue
    .line 2114
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_d

    #@8
    .line 2115
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@a
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@d
    .line 2119
    :cond_d
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@10
    move-result-object v0

    #@11
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@13
    if-eq v0, v1, :cond_19

    #@15
    .line 2120
    const/4 v0, 0x1

    #@16
    iput-boolean v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mIsPhoneInEcmExitDelayState:Z

    #@18
    .line 2127
    :goto_18
    return-void

    #@19
    .line 2126
    :cond_19
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1b
    const/16 v1, 0x1a

    #@1d
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->obtainMessage(I)Landroid/os/Message;

    #@20
    move-result-object v1

    #@21
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->exitEmergencyCallbackMode(Landroid/os/Message;)V

    #@24
    goto :goto_18
.end method

.method public explicitCallTransfer()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 893
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->explicitCallTransfer()V

    #@5
    .line 894
    return-void
.end method

.method protected finalize()V
    .registers 3

    #@0
    .prologue
    .line 579
    const-string v0, "GSM"

    #@2
    const-string v1, "GSMPhone finalized"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 582
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@9
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_1b

    #@f
    .line 583
    const-string v0, "GSM"

    #@11
    const-string v1, "UNEXPECTED; mWakeLock is held when finalizing."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 584
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@18
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@1b
    .line 587
    :cond_1b
    return-void
.end method

.method forgetISimSession()V
    .registers 3

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 3366
    iget v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mISimSessionId:I

    #@3
    if-eq v0, v1, :cond_e

    #@5
    .line 3367
    iput v1, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mISimSessionId:I

    #@7
    .line 3368
    const-string v0, "GSM"

    #@9
    const-string v1, "Dropping cached logical channel"

    #@b
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 3370
    :cond_e
    return-void
.end method

.method public gbaAuthenticateBootstrap([B[BLandroid/os/Message;)V
    .registers 5
    .parameter "rand"
    .parameter "autn"
    .parameter "onComplete"

    #@0
    .prologue
    .line 3382
    new-instance v0, Lcom/android/internal/telephony/gsm/GSMPhone$PendingBootstrap;

    #@2
    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/internal/telephony/gsm/GSMPhone$PendingBootstrap;-><init>(Lcom/android/internal/telephony/gsm/GSMPhone;[B[BLandroid/os/Message;)V

    #@5
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gsm/GSMPhone;->ensureISimSession(Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;)V

    #@8
    .line 3383
    return-void
.end method

.method public gbaAuthenticateNaf([BLandroid/os/Message;)V
    .registers 4
    .parameter "nafId"
    .parameter "onComplete"

    #@0
    .prologue
    .line 3386
    new-instance v0, Lcom/android/internal/telephony/gsm/GSMPhone$PendingNaf;

    #@2
    invoke-direct {v0, p0, p1, p2}, Lcom/android/internal/telephony/gsm/GSMPhone$PendingNaf;-><init>(Lcom/android/internal/telephony/gsm/GSMPhone;[BLandroid/os/Message;)V

    #@5
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gsm/GSMPhone;->ensureISimSession(Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;)V

    #@8
    .line 3387
    return-void
.end method

.method public getAPNList()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 3143
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnectionTracker;->getAPNList()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getAvailableNetworks(Landroid/os/Message;)V
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 1689
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->getAvailableNetworks(Landroid/os/Message;)V

    #@5
    .line 1690
    return-void
.end method

.method public bridge synthetic getBackgroundCall()Lcom/android/internal/telephony/Call;
    .registers 2

    #@0
    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getBackgroundCall()Lcom/android/internal/telephony/gsm/GsmCall;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getBackgroundCall()Lcom/android/internal/telephony/gsm/GsmCall;
    .registers 2

    #@0
    .prologue
    .line 903
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->backgroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@4
    return-object v0
.end method

.method public getCallBarringOption(Ljava/lang/String;Landroid/os/Message;)V
    .registers 6
    .parameter "commandInterfaceCBReason"
    .parameter "onComplete"

    #@0
    .prologue
    .line 1666
    const-string v0, "Radhika"

    #@2
    const-string v1, "Setting the service class to None"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1667
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@9
    const/4 v1, 0x0

    #@a
    const/4 v2, 0x0

    #@b
    invoke-interface {v0, p1, v1, v2, p2}, Lcom/android/internal/telephony/CommandsInterface;->queryFacilityLock(Ljava/lang/String;Ljava/lang/String;ILandroid/os/Message;)V

    #@e
    .line 1668
    return-void
.end method

.method public getCallForwardingIndicator()Z
    .registers 4

    #@0
    .prologue
    .line 643
    const/4 v0, 0x0

    #@1
    .line 644
    .local v0, cf:Z
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@3
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@6
    move-result-object v1

    #@7
    check-cast v1, Lcom/android/internal/telephony/uicc/IccRecords;

    #@9
    .line 654
    .local v1, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v1, :cond_16

    #@b
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/IccRecords;->isCallForwardStatusStored()Z

    #@e
    move-result v2

    #@f
    if-eqz v2, :cond_16

    #@11
    .line 655
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/IccRecords;->getVoiceCallForwardingFlag()Z

    #@14
    move-result v0

    #@15
    .line 660
    :goto_15
    return v0

    #@16
    .line 657
    :cond_16
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getCallForwardingPreference()Z

    #@19
    move-result v0

    #@1a
    goto :goto_15
.end method

.method public getCallForwardingOption(ILandroid/os/Message;)V
    .registers 7
    .parameter "commandInterfaceCFReason"
    .parameter "onComplete"

    #@0
    .prologue
    .line 1576
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->isValidCommandInterfaceCFReason(I)Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_1c

    #@6
    .line 1577
    const-string v1, "GSM"

    #@8
    const-string v2, "requesting call forwarding query."

    #@a
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 1579
    if-nez p1, :cond_1d

    #@f
    .line 1580
    const/16 v1, 0xd

    #@11
    invoke-virtual {p0, v1, p2}, Lcom/android/internal/telephony/gsm/GSMPhone;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@14
    move-result-object v0

    #@15
    .line 1584
    .local v0, resp:Landroid/os/Message;
    :goto_15
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@17
    const/4 v2, 0x0

    #@18
    const/4 v3, 0x0

    #@19
    invoke-interface {v1, p1, v2, v3, v0}, Lcom/android/internal/telephony/CommandsInterface;->queryCallForwardStatus(IILjava/lang/String;Landroid/os/Message;)V

    #@1c
    .line 1586
    .end local v0           #resp:Landroid/os/Message;
    :cond_1c
    return-void

    #@1d
    .line 1582
    :cond_1d
    move-object v0, p2

    #@1e
    .restart local v0       #resp:Landroid/os/Message;
    goto :goto_15
.end method

.method protected getCallForwardingPreference()Z
    .registers 5

    #@0
    .prologue
    .line 1917
    const-string v2, "GSM"

    #@2
    const-string v3, "Get callforwarding info from perferences"

    #@4
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1919
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@9
    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@c
    move-result-object v1

    #@d
    .line 1920
    .local v1, sp:Landroid/content/SharedPreferences;
    const-string v2, "cf_enabled_key"

    #@f
    const/4 v3, 0x0

    #@10
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    #@13
    move-result v0

    #@14
    .line 1921
    .local v0, cf:Z
    return v0
.end method

.method public getCallTracker()Lcom/android/internal/telephony/CallTracker;
    .registers 2

    #@0
    .prologue
    .line 625
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2
    return-object v0
.end method

.method public getCallWaiting(Landroid/os/Message;)V
    .registers 4
    .parameter "onComplete"

    #@0
    .prologue
    .line 1652
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-interface {v0, v1, p1}, Lcom/android/internal/telephony/CommandsInterface;->queryCallWaiting(ILandroid/os/Message;)V

    #@6
    .line 1653
    return-void
.end method

.method public getCdmaLteEhrpdForced()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 772
    const-string v0, "GSM"

    #@2
    const-string v1, "[GSMPhone] getCdmaLteEhrpdForced() is a CDMA method"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 773
    const/4 v0, 0x0

    #@8
    return-object v0
.end method

.method public getCellBroadcastSmsConfig(Landroid/os/Message;)V
    .registers 4
    .parameter "response"

    #@0
    .prologue
    .line 2829
    const-string v0, "GSM"

    #@2
    const-string v1, "[GSMPhone] getCellBroadcastSmsConfig() is obsolete; use SmsManager"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2830
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    #@a
    .line 2831
    return-void
.end method

.method public getCellLocation()Landroid/telephony/CellLocation;
    .registers 2

    #@0
    .prologue
    .line 605
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSST:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@2
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->cellLoc:Landroid/telephony/gsm/GsmCellLocation;

    #@4
    return-object v0
.end method

.method public getDataActivityState()Lcom/android/internal/telephony/Phone$DataActivityState;
    .registers 4

    #@0
    .prologue
    .line 745
    sget-object v0, Lcom/android/internal/telephony/Phone$DataActivityState;->NONE:Lcom/android/internal/telephony/Phone$DataActivityState;

    #@2
    .line 747
    .local v0, ret:Lcom/android/internal/telephony/Phone$DataActivityState;
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSST:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@4
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->getCurrentGprsState()I

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_1b

    #@a
    .line 748
    sget-object v1, Lcom/android/internal/telephony/gsm/GSMPhone$4;->$SwitchMap$com$android$internal$telephony$DctConstants$Activity:[I

    #@c
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@e
    invoke-virtual {v2}, Lcom/android/internal/telephony/DataConnectionTracker;->getActivity()Lcom/android/internal/telephony/DctConstants$Activity;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Lcom/android/internal/telephony/DctConstants$Activity;->ordinal()I

    #@15
    move-result v2

    #@16
    aget v1, v1, v2

    #@18
    packed-switch v1, :pswitch_data_28

    #@1b
    .line 767
    :cond_1b
    :goto_1b
    return-object v0

    #@1c
    .line 750
    :pswitch_1c
    sget-object v0, Lcom/android/internal/telephony/Phone$DataActivityState;->DATAIN:Lcom/android/internal/telephony/Phone$DataActivityState;

    #@1e
    .line 751
    goto :goto_1b

    #@1f
    .line 754
    :pswitch_1f
    sget-object v0, Lcom/android/internal/telephony/Phone$DataActivityState;->DATAOUT:Lcom/android/internal/telephony/Phone$DataActivityState;

    #@21
    .line 755
    goto :goto_1b

    #@22
    .line 758
    :pswitch_22
    sget-object v0, Lcom/android/internal/telephony/Phone$DataActivityState;->DATAINANDOUT:Lcom/android/internal/telephony/Phone$DataActivityState;

    #@24
    .line 759
    goto :goto_1b

    #@25
    .line 762
    :pswitch_25
    sget-object v0, Lcom/android/internal/telephony/Phone$DataActivityState;->DORMANT:Lcom/android/internal/telephony/Phone$DataActivityState;

    #@27
    goto :goto_1b

    #@28
    .line 748
    :pswitch_data_28
    .packed-switch 0x1
        :pswitch_1c
        :pswitch_1f
        :pswitch_22
        :pswitch_25
    .end packed-switch
.end method

.method public getDataCallList(Landroid/os/Message;)V
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 1860
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->getDataCallList(Landroid/os/Message;)V

    #@5
    .line 1861
    return-void
.end method

.method public getDataConnectionState(Ljava/lang/String;)Lcom/android/internal/telephony/PhoneConstants$DataState;
    .registers 8
    .parameter "apnType"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 669
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$DataState;->DISCONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@3
    .line 672
    .local v2, ret:Lcom/android/internal/telephony/PhoneConstants$DataState;
    const/4 v1, 0x0

    #@4
    .line 676
    .local v1, mConnectivityManager:Landroid/net/ConnectivityManager;
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@6
    invoke-interface {v3}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@9
    move-result-object v3

    #@a
    iget-boolean v3, v3, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_TCP_SOCKET_CONN_IN_OOS_DCM:Z

    #@c
    if-eqz v3, :cond_10

    #@e
    .line 677
    iput-boolean v5, p0, Lcom/android/internal/telephony/PhoneBase;->mOosIsDisconnect:Z

    #@10
    .line 681
    :cond_10
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSST:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@12
    if-nez v3, :cond_17

    #@14
    .line 685
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$DataState;->DISCONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@16
    .line 741
    :cond_16
    :goto_16
    return-object v2

    #@17
    .line 686
    :cond_17
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSST:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@19
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->getCurrentGprsState()I

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_3c

    #@1f
    iget-boolean v3, p0, Lcom/android/internal/telephony/PhoneBase;->mOosIsDisconnect:Z

    #@21
    if-eqz v3, :cond_3c

    #@23
    .line 691
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$DataState;->DISCONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@25
    .line 692
    new-instance v3, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v4, "getDataConnectionState: Data is Out of Service. ret = "

    #@2c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v3

    #@38
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/gsm/GSMPhone;->log(Ljava/lang/String;)V

    #@3b
    goto :goto_16

    #@3c
    .line 693
    :cond_3c
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@3e
    invoke-virtual {v3, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->isApnTypeEnabled(Ljava/lang/String;)Z

    #@41
    move-result v3

    #@42
    if-eqz v3, :cond_4c

    #@44
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@46
    invoke-virtual {v3, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->isApnTypeActive(Ljava/lang/String;)Z

    #@49
    move-result v3

    #@4a
    if-nez v3, :cond_4f

    #@4c
    .line 698
    :cond_4c
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$DataState;->DISCONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@4e
    goto :goto_16

    #@4f
    .line 700
    :cond_4f
    sget-object v3, Lcom/android/internal/telephony/gsm/GSMPhone$4;->$SwitchMap$com$android$internal$telephony$DctConstants$State:[I

    #@51
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@53
    invoke-virtual {v4, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->getState(Ljava/lang/String;)Lcom/android/internal/telephony/DctConstants$State;

    #@56
    move-result-object v4

    #@57
    invoke-virtual {v4}, Lcom/android/internal/telephony/DctConstants$State;->ordinal()I

    #@5a
    move-result v4

    #@5b
    aget v3, v3, v4

    #@5d
    packed-switch v3, :pswitch_data_b2

    #@60
    goto :goto_16

    #@61
    .line 706
    :pswitch_61
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$DataState;->DISCONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@63
    .line 707
    goto :goto_16

    #@64
    .line 711
    :pswitch_64
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@66
    iget-object v3, v3, Lcom/android/internal/telephony/gsm/GsmCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@68
    sget-object v4, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@6a
    if-eq v3, v4, :cond_ab

    #@6c
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSST:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@6e
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->isConcurrentVoiceAndDataAllowed()Z

    #@71
    move-result v3

    #@72
    if-nez v3, :cond_ab

    #@74
    .line 713
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$DataState;->SUSPENDED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@76
    .line 719
    :goto_76
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@78
    invoke-interface {v3}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@7b
    move-result-object v3

    #@7c
    iget-boolean v3, v3, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_DUAL_CONNECTIVITY_DCM:Z

    #@7e
    if-eqz v3, :cond_16

    #@80
    const-string v3, "default"

    #@82
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@85
    move-result v3

    #@86
    if-eqz v3, :cond_16

    #@88
    .line 720
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@8b
    move-result-object v3

    #@8c
    const-string v4, "connectivity"

    #@8e
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@91
    move-result-object v1

    #@92
    .end local v1           #mConnectivityManager:Landroid/net/ConnectivityManager;
    check-cast v1, Landroid/net/ConnectivityManager;

    #@94
    .line 721
    .restart local v1       #mConnectivityManager:Landroid/net/ConnectivityManager;
    if-eqz v1, :cond_16

    #@96
    .line 722
    invoke-virtual {v1, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@99
    move-result-object v0

    #@9a
    .line 723
    .local v0, info:Landroid/net/NetworkInfo;
    if-eqz v0, :cond_16

    #@9c
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    #@9f
    move-result v3

    #@a0
    if-ne v3, v5, :cond_16

    #@a2
    .line 724
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$DataState;->DISCONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@a4
    .line 725
    const-string v3, "getDataConnectionState: Mobile disconnect during WiFi. "

    #@a6
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/gsm/GSMPhone;->log(Ljava/lang/String;)V

    #@a9
    goto/16 :goto_16

    #@ab
    .line 715
    .end local v0           #info:Landroid/net/NetworkInfo;
    :cond_ab
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$DataState;->CONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@ad
    goto :goto_76

    #@ae
    .line 735
    :pswitch_ae
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$DataState;->CONNECTING:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@b0
    goto/16 :goto_16

    #@b2
    .line 700
    :pswitch_data_b2
    .packed-switch 0x1
        :pswitch_61
        :pswitch_61
        :pswitch_61
        :pswitch_61
        :pswitch_64
        :pswitch_ae
        :pswitch_ae
    .end packed-switch
.end method

.method public getDataRoamingEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 1876
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnectionTracker;->getDataOnRoamingEnabled()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getDesiredPowerState()Z
    .registers 2

    #@0
    .prologue
    .line 3187
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSST:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->getDesiredPowerState()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 1442
    const-string v1, "VZW"

    #@2
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_25

    #@8
    .line 1443
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getImei()Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    .line 1444
    .local v0, mDeviceId:Ljava/lang/String;
    if-eqz v0, :cond_25

    #@e
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@11
    move-result v1

    #@12
    const/16 v2, 0xf

    #@14
    if-ne v1, v2, :cond_25

    #@16
    .line 1445
    const-string v1, "GSM"

    #@18
    const-string v2, "getDeviceId(): returns IMEI(14) in CDMA-LTE Phone"

    #@1a
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 1446
    const/4 v1, 0x0

    #@1e
    const/16 v2, 0xe

    #@20
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    .line 1451
    .end local v0           #mDeviceId:Ljava/lang/String;
    :goto_24
    return-object v1

    #@25
    :cond_25
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mImei:Ljava/lang/String;

    #@27
    goto :goto_24
.end method

.method public getDeviceId(I)Ljava/lang/String;
    .registers 10
    .parameter "type"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/16 v6, 0xe

    #@3
    .line 3091
    const/4 v1, 0x0

    #@4
    .line 3092
    .local v1, mDeviceId:Ljava/lang/String;
    const-string v3, "gsm.sim.state"

    #@6
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    .line 3093
    .local v2, prop:Ljava/lang/String;
    const-string v3, "READY"

    #@c
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    .line 3095
    .local v0, isSIMReady:Z
    sget-boolean v3, Lcom/android/internal/telephony/gsm/GSMPhone;->ENABLE_PRIVACY_LOG_CALL:Z

    #@12
    if-eqz v3, :cond_52

    #@14
    .line 3096
    const-string v3, "GSM"

    #@16
    new-instance v4, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v5, "mImei = "

    #@1d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v4

    #@21
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mImei:Ljava/lang/String;

    #@23
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v4

    #@27
    const-string v5, "   mImeiSv = "

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mImeiSv:Ljava/lang/String;

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    const-string v5, "   mEsn = "

    #@35
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mEsn:Ljava/lang/String;

    #@3b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v4

    #@3f
    const-string v5, "   mMeid = "

    #@41
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v4

    #@45
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mMeid:Ljava/lang/String;

    #@47
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v4

    #@4b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v4

    #@4f
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 3099
    :cond_52
    packed-switch p1, :pswitch_data_bc

    #@55
    .line 3123
    :cond_55
    :goto_55
    sget-boolean v3, Lcom/android/internal/telephony/gsm/GSMPhone;->ENABLE_PRIVACY_LOG_CALL:Z

    #@57
    if-eqz v3, :cond_7b

    #@59
    .line 3124
    const-string v3, "GSM"

    #@5b
    new-instance v4, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string v5, "getDeviceId("

    #@62
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v4

    #@66
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@69
    move-result-object v4

    #@6a
    const-string v5, ") = "

    #@6c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v4

    #@70
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v4

    #@74
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v4

    #@78
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7b
    .line 3127
    :cond_7b
    return-object v1

    #@7c
    .line 3101
    :pswitch_7c
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mImei:Ljava/lang/String;

    #@7e
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@81
    move-result v3

    #@82
    if-le v3, v6, :cond_8a

    #@84
    .line 3102
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mImei:Ljava/lang/String;

    #@86
    invoke-virtual {v3, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@89
    move-result-object v1

    #@8a
    .line 3104
    :cond_8a
    if-eqz v1, :cond_94

    #@8c
    const-string v3, "^0*$"

    #@8e
    invoke-virtual {v1, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    #@91
    move-result v3

    #@92
    if-eqz v3, :cond_55

    #@94
    .line 3105
    :cond_94
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getEsn()Ljava/lang/String;

    #@97
    move-result-object v1

    #@98
    goto :goto_55

    #@99
    .line 3109
    :pswitch_99
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mImei:Ljava/lang/String;

    #@9b
    .line 3110
    goto :goto_55

    #@9c
    .line 3112
    :pswitch_9c
    if-nez v0, :cond_a0

    #@9e
    .line 3113
    const/4 v1, 0x0

    #@9f
    goto :goto_55

    #@a0
    .line 3114
    :cond_a0
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mImei:Ljava/lang/String;

    #@a2
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@a5
    move-result v3

    #@a6
    if-lt v3, v6, :cond_b9

    #@a8
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mImei:Ljava/lang/String;

    #@aa
    invoke-virtual {v3, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@ad
    move-result-object v3

    #@ae
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mMeid:Ljava/lang/String;

    #@b0
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b3
    move-result v3

    #@b4
    if-nez v3, :cond_b9

    #@b6
    .line 3115
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mMeid:Ljava/lang/String;

    #@b8
    goto :goto_55

    #@b9
    .line 3117
    :cond_b9
    const/4 v1, 0x0

    #@ba
    goto :goto_55

    #@bb
    .line 3099
    nop

    #@bc
    :pswitch_data_bc
    .packed-switch 0x1
        :pswitch_7c
        :pswitch_99
        :pswitch_9c
    .end packed-switch
.end method

.method public getDeviceSvn()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 1457
    const-string v0, "US"

    #@2
    const-string v1, "SPR"

    #@4
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_13

    #@a
    .line 1458
    const-string v0, "ro.lge.swversion"

    #@c
    const-string v1, "0"

    #@e
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    .line 1461
    :goto_12
    return-object v0

    #@13
    :cond_13
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mImeiSv:Ljava/lang/String;

    #@15
    goto :goto_12
.end method

.method public getEmodeInfoPage(I)Ljava/lang/String;
    .registers 5
    .parameter "EngIndex"

    #@0
    .prologue
    .line 2965
    const-string v0, "[GSMPhone]"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[getEmodeInfo()] EngIndex = "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 2967
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mEmodeInfoforAndroid:[Ljava/lang/String;

    #@1a
    aget-object v0, v0, p1

    #@1c
    return-object v0
.end method

.method public getEngineeringModeInfo(ILandroid/os/Message;)I
    .registers 8
    .parameter "data"
    .parameter "result"

    #@0
    .prologue
    .line 2954
    const/16 v2, 0x25

    #@2
    invoke-virtual {p0, v2, p2}, Lcom/android/internal/telephony/gsm/GSMPhone;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    .line 2956
    .local v0, response:Landroid/os/Message;
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@8
    invoke-interface {v2, p1, v0}, Lcom/android/internal/telephony/CommandsInterface;->getEngineeringModeInfo(ILandroid/os/Message;)I

    #@b
    move-result v1

    #@c
    .line 2958
    .local v1, ret:I
    const-string v2, "GSM"

    #@e
    new-instance v3, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v4, "hjlog_ gsm getEngineeringModeInfo ret = "

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 2960
    const/4 v2, 0x0

    #@25
    return v2
.end method

.method public getEsn()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 1469
    const-string v0, "GSM"

    #@2
    const-string v1, "[GSMPhone] getEsn() is a CDMA method"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1470
    const-string v0, "0"

    #@9
    return-object v0
.end method

.method public bridge synthetic getForegroundCall()Lcom/android/internal/telephony/Call;
    .registers 2

    #@0
    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getForegroundCall()Lcom/android/internal/telephony/gsm/GsmCall;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getForegroundCall()Lcom/android/internal/telephony/gsm/GsmCall;
    .registers 2

    #@0
    .prologue
    .line 898
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->foregroundCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@4
    return-object v0
.end method

.method public getIccPhoneBookInterfaceManager()Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;
    .registers 2

    #@0
    .prologue
    .line 2809
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSimPhoneBookIntManager:Lcom/android/internal/telephony/gsm/SimPhoneBookInterfaceManager;

    #@2
    return-object v0
.end method

.method public getImei()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1465
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mImei:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getInternationalMdnVoiceMailNumberForVZW()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 1402
    const/4 v1, 0x0

    #@1
    .line 1404
    .local v1, mdnVmNumber:Ljava/lang/String;
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mAssistDialPhoneNumberUtils:Landroid/telephony/AssistDialPhoneNumberUtils;

    #@3
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@6
    move-result-object v3

    #@7
    invoke-virtual {v2, v3}, Landroid/telephony/AssistDialPhoneNumberUtils;->getAssistedDialCurrentCountry(Landroid/content/Context;)Landroid/provider/ReferenceCountry;

    #@a
    move-result-object v0

    #@b
    .line 1406
    .local v0, currentCountry:Landroid/provider/ReferenceCountry;
    if-eqz v0, :cond_24

    #@d
    .line 1407
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "+1"

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getLine1Number()Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    .line 1410
    :cond_24
    return-object v1
.end method

.method public getIsimRecords()Lcom/android/internal/telephony/uicc/IsimRecords;
    .registers 3

    #@0
    .prologue
    .line 3179
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@6
    const/4 v1, 0x3

    #@7
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/uicc/UiccController;->getIccRecords(I)Lcom/android/internal/telephony/uicc/IccRecords;

    #@a
    move-result-object v0

    #@b
    check-cast v0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;

    #@d
    :goto_d
    return-object v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public getLine1AlphaTag()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 1521
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@2
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/uicc/IccRecords;

    #@8
    .line 1522
    .local v0, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v0, :cond_f

    #@a
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/IccRecords;->getMsisdnAlphaTag()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    :goto_e
    return-object v1

    #@f
    :cond_f
    const-string v1, ""

    #@11
    goto :goto_e
.end method

.method public getLine1Number()Ljava/lang/String;
    .registers 10

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1484
    iget-object v6, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@3
    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@6
    move-result-object v4

    #@7
    check-cast v4, Lcom/android/internal/telephony/uicc/IccRecords;

    #@9
    .line 1486
    .local v4, r:Lcom/android/internal/telephony/uicc/IccRecords;
    const-string v6, "VZW"

    #@b
    invoke-static {v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@e
    move-result v6

    #@f
    if-eqz v6, :cond_a8

    #@11
    .line 1487
    if-eqz v4, :cond_5f

    #@13
    invoke-virtual {v4}, Lcom/android/internal/telephony/uicc/IccRecords;->getMsisdnNumber()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    .line 1488
    .local v2, mdnNumber:Ljava/lang/String;
    :goto_17
    if-eqz v2, :cond_61

    #@19
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@1c
    move-result v3

    #@1d
    .line 1489
    .local v3, mdnNumberLength:I
    :goto_1d
    const/16 v6, 0xa

    #@1f
    if-lt v3, v6, :cond_63

    #@21
    .line 1490
    add-int/lit8 v6, v3, -0xa

    #@23
    invoke-virtual {v2, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    .line 1496
    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    #@2a
    move-result-object v0

    #@2b
    .line 1497
    .local v0, chArr:[C
    const/4 v1, 0x0

    #@2c
    .local v1, i:I
    :goto_2c
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@2f
    move-result v6

    #@30
    if-ge v1, v6, :cond_85

    #@32
    .line 1498
    aget-char v6, v0, v1

    #@34
    invoke-static {v6}, Landroid/telephony/PhoneNumberUtils;->isISODigit(C)Z

    #@37
    move-result v6

    #@38
    if-nez v6, :cond_82

    #@3a
    .line 1499
    const-string v6, "GSM"

    #@3c
    new-instance v7, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v8, "chArr["

    #@43
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v7

    #@47
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v7

    #@4b
    const-string v8, "]"

    #@4d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v7

    #@51
    aget-char v8, v0, v1

    #@53
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@56
    move-result-object v7

    #@57
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v7

    #@5b
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 1511
    .end local v0           #chArr:[C
    .end local v1           #i:I
    .end local v2           #mdnNumber:Ljava/lang/String;
    .end local v3           #mdnNumberLength:I
    :cond_5e
    :goto_5e
    return-object v5

    #@5f
    :cond_5f
    move-object v2, v5

    #@60
    .line 1487
    goto :goto_17

    #@61
    .line 1488
    .restart local v2       #mdnNumber:Ljava/lang/String;
    :cond_61
    const/4 v3, 0x0

    #@62
    goto :goto_1d

    #@63
    .line 1492
    .restart local v3       #mdnNumberLength:I
    :cond_63
    const-string v6, "GSM"

    #@65
    new-instance v7, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v8, "MDN length is "

    #@6c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v7

    #@70
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@73
    move-result-object v7

    #@74
    const-string v8, ", so return null"

    #@76
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v7

    #@7a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v7

    #@7e
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    goto :goto_5e

    #@82
    .line 1497
    .restart local v0       #chArr:[C
    .restart local v1       #i:I
    :cond_82
    add-int/lit8 v1, v1, 0x1

    #@84
    goto :goto_2c

    #@85
    .line 1504
    :cond_85
    const-string v6, "0000000000"

    #@87
    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8a
    move-result v6

    #@8b
    if-eqz v6, :cond_a6

    #@8d
    .line 1505
    const-string v6, "GSM"

    #@8f
    new-instance v7, Ljava/lang/StringBuilder;

    #@91
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@94
    const-string v8, "mdnNumber = "

    #@96
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v7

    #@9a
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v7

    #@9e
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a1
    move-result-object v7

    #@a2
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a5
    goto :goto_5e

    #@a6
    :cond_a6
    move-object v5, v2

    #@a7
    .line 1508
    goto :goto_5e

    #@a8
    .line 1511
    .end local v0           #chArr:[C
    .end local v1           #i:I
    .end local v2           #mdnNumber:Ljava/lang/String;
    .end local v3           #mdnNumberLength:I
    :cond_a8
    if-eqz v4, :cond_5e

    #@aa
    invoke-virtual {v4}, Lcom/android/internal/telephony/uicc/IccRecords;->getMsisdnNumber()Ljava/lang/String;

    #@ad
    move-result-object v5

    #@ae
    goto :goto_5e
.end method

.method public getMSIN()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 3165
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@3
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@6
    move-result-object v1

    #@7
    check-cast v1, Lcom/android/internal/telephony/uicc/IccRecords;

    #@9
    .line 3167
    .local v1, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v1, :cond_11

    #@b
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/IccRecords;->getOperatorNumeric()Ljava/lang/String;

    #@e
    move-result-object v3

    #@f
    if-nez v3, :cond_12

    #@11
    .line 3170
    :cond_11
    :goto_11
    return-object v2

    #@12
    .line 3169
    :cond_12
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/IccRecords;->getIMSI()Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    .line 3170
    .local v0, imsi:Ljava/lang/String;
    if-eqz v0, :cond_11

    #@18
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/IccRecords;->getOperatorNumeric()Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@1f
    move-result v2

    #@20
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@23
    move-result v3

    #@24
    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    goto :goto_11
.end method

.method public getMeid()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 1474
    const-string v0, "GSM"

    #@2
    const-string v1, "[GSMPhone] getMeid() is a CDMA method"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1475
    const-string v0, "0"

    #@9
    return-object v0
.end method

.method public getMipErrorCode(Landroid/os/Message;)V
    .registers 4
    .parameter "result"

    #@0
    .prologue
    .line 3158
    const-string v0, "GSM"

    #@2
    const-string v1, "[GSMPhone,bycho]getMipErrorCode() is a CDMA method"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 3159
    return-void
.end method

.method public getMsisdn()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 1516
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@2
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/uicc/IccRecords;

    #@8
    .line 1517
    .local v0, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v0, :cond_f

    #@a
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/IccRecords;->getMsisdnNumber()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    :goto_e
    return-object v1

    #@f
    :cond_f
    const-string v1, ""

    #@11
    goto :goto_e
.end method

.method public getMute()Z
    .registers 2

    #@0
    .prologue
    .line 1856
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->getMute()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getNeighboringCids(Landroid/os/Message;)V
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 1844
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->getNeighboringCids(Landroid/os/Message;)V

    #@5
    .line 1845
    return-void
.end method

.method public getOutgoingCallerIdDisplay(Landroid/os/Message;)V
    .registers 3
    .parameter "onComplete"

    #@0
    .prologue
    .line 1640
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->getCLIR(Landroid/os/Message;)V

    #@5
    .line 1641
    return-void
.end method

.method public getPendingMmiCodes()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/android/internal/telephony/MmiCode;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 665
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mPendingMMIs:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method public getPhoneName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 613
    const-string v0, "GSM"

    #@2
    return-object v0
.end method

.method public getPhoneSubInfo()Lcom/android/internal/telephony/PhoneSubInfo;
    .registers 2

    #@0
    .prologue
    .line 2802
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSubInfo:Lcom/android/internal/telephony/PhoneSubInfo;

    #@2
    return-object v0
.end method

.method public getPhoneType()I
    .registers 2

    #@0
    .prologue
    .line 617
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public bridge synthetic getRingingCall()Lcom/android/internal/telephony/Call;
    .registers 2

    #@0
    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getRingingCall()Lcom/android/internal/telephony/gsm/GsmCall;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getRingingCall()Lcom/android/internal/telephony/gsm/GsmCall;
    .registers 2

    #@0
    .prologue
    .line 908
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->ringingCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@4
    return-object v0
.end method

.method public getSIMRecords()Lcom/android/internal/telephony/uicc/SIMRecords;
    .registers 3

    #@0
    .prologue
    .line 3183
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@6
    const/4 v1, 0x1

    #@7
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/uicc/UiccController;->getIccRecords(I)Lcom/android/internal/telephony/uicc/IccRecords;

    #@a
    move-result-object v0

    #@b
    check-cast v0, Lcom/android/internal/telephony/uicc/SIMRecords;

    #@d
    :goto_d
    return-object v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public getSearchStatus(ILandroid/os/Message;)V
    .registers 4
    .parameter "ReqType"
    .parameter "response"

    #@0
    .prologue
    .line 1835
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/CommandsInterface;->getSearchStatus(ILandroid/os/Message;)V

    #@5
    .line 1836
    return-void
.end method

.method public getSearchStatus(Landroid/os/Message;)V
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 1838
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->getSearchStatus(Landroid/os/Message;)V

    #@5
    .line 1839
    return-void
.end method

.method public getServiceState()Landroid/telephony/ServiceState;
    .registers 4

    #@0
    .prologue
    .line 593
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSST:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@2
    if-eqz v1, :cond_f

    #@4
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSST:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@6
    iget-object v1, v1, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@8
    if-eqz v1, :cond_f

    #@a
    .line 594
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSST:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@c
    iget-object v0, v1, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@e
    .line 599
    :goto_e
    return-object v0

    #@f
    .line 596
    :cond_f
    const-string v1, "GSM"

    #@11
    const-string v2, "Current ServiceState is null, return default state"

    #@13
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 597
    new-instance v0, Landroid/telephony/ServiceState;

    #@18
    invoke-direct {v0}, Landroid/telephony/ServiceState;-><init>()V

    #@1b
    .line 598
    .local v0, DefaultSs:Landroid/telephony/ServiceState;
    invoke-virtual {v0}, Landroid/telephony/ServiceState;->setStateOutOfService()V

    #@1e
    goto :goto_e
.end method

.method public getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;
    .registers 2

    #@0
    .prologue
    .line 621
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSST:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@2
    return-object v0
.end method

.method public getState()Lcom/android/internal/telephony/PhoneConstants$State;
    .registers 2

    #@0
    .prologue
    .line 609
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@4
    return-object v0
.end method

.method public getStatusId()I
    .registers 2

    #@0
    .prologue
    .line 3134
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSST:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->bManualSelectionAvailable()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_a

    #@8
    .line 3135
    const/4 v0, 0x0

    #@9
    .line 3136
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x1

    #@b
    goto :goto_9
.end method

.method public getSubscriberId()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 1479
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@2
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/uicc/IccRecords;

    #@8
    .line 1480
    .local v0, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v0, :cond_f

    #@a
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/IccRecords;->getIMSI()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    :goto_e
    return-object v1

    #@f
    :cond_f
    const/4 v1, 0x0

    #@10
    goto :goto_e
.end method

.method protected getUiccCardApplication()Lcom/android/internal/telephony/uicc/UiccCardApplication;
    .registers 3

    #@0
    .prologue
    .line 2622
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/uicc/UiccController;->getUiccCardApplication(I)Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getVoiceMailAlphaTag()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 1428
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@2
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/uicc/IccRecords;

    #@8
    .line 1430
    .local v0, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v0, :cond_24

    #@a
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/IccRecords;->getVoiceMailAlphaTag()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    .line 1432
    .local v1, ret:Ljava/lang/String;
    :goto_e
    if-eqz v1, :cond_16

    #@10
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@13
    move-result v2

    #@14
    if-nez v2, :cond_23

    #@16
    .line 1433
    :cond_16
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@18
    const v3, 0x1040004

    #@1b
    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    .line 1437
    .end local v1           #ret:Ljava/lang/String;
    :cond_23
    return-object v1

    #@24
    .line 1430
    :cond_24
    const-string v1, ""

    #@26
    goto :goto_e
.end method

.method public getVoiceMailNumber()Ljava/lang/String;
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 1338
    iget-object v5, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@3
    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@6
    move-result-object v2

    #@7
    check-cast v2, Lcom/android/internal/telephony/uicc/IccRecords;

    #@9
    .line 1339
    .local v2, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v2, :cond_e0

    #@b
    invoke-virtual {v2}, Lcom/android/internal/telephony/uicc/IccRecords;->getVoiceMailNumber()Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    .line 1345
    .local v1, number:Ljava/lang/String;
    :goto_f
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getSimInfo()Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;

    #@12
    move-result-object v3

    #@13
    .line 1346
    .local v3, simInfo:Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@16
    move-result-object v5

    #@17
    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    #@1a
    move-result v0

    #@1b
    .line 1348
    .local v0, is_roaming:Z
    const-string v5, "GSM"

    #@1d
    new-instance v6, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v7, "getVoiceMailNumber() - isNetworkRoaming: "

    #@24
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v6

    #@28
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v6

    #@2c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v6

    #@30
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 1350
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@36
    move-result v5

    #@37
    if-nez v5, :cond_83

    #@39
    invoke-virtual {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMcc()Ljava/lang/String;

    #@3c
    move-result-object v5

    #@3d
    if-eqz v5, :cond_83

    #@3f
    invoke-virtual {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMcc()Ljava/lang/String;

    #@42
    move-result-object v5

    #@43
    const-string v6, "214"

    #@45
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@48
    move-result v5

    #@49
    if-eqz v5, :cond_83

    #@4b
    .line 1351
    invoke-virtual {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMnc()Ljava/lang/String;

    #@4e
    move-result-object v5

    #@4f
    if-eqz v5, :cond_83

    #@51
    invoke-virtual {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMnc()Ljava/lang/String;

    #@54
    move-result-object v5

    #@55
    const-string v6, "08"

    #@57
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5a
    move-result v5

    #@5b
    if-nez v5, :cond_69

    #@5d
    invoke-virtual {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMnc()Ljava/lang/String;

    #@60
    move-result-object v5

    #@61
    const-string v6, "06"

    #@63
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@66
    move-result v5

    #@67
    if-eqz v5, :cond_83

    #@69
    .line 1352
    :cond_69
    const-string v5, "242"

    #@6b
    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6e
    move-result v5

    #@6f
    if-nez v5, :cond_79

    #@71
    const-string v5, "277"

    #@73
    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@76
    move-result v5

    #@77
    if-eqz v5, :cond_e4

    #@79
    :cond_79
    if-nez v0, :cond_e4

    #@7b
    .line 1353
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@7e
    move-result-object v5

    #@7f
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getVMS(Landroid/content/Context;)Ljava/lang/String;

    #@82
    move-result-object v1

    #@83
    .line 1362
    :cond_83
    :goto_83
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@86
    move-result v5

    #@87
    if-eqz v5, :cond_df

    #@89
    .line 1363
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@8c
    move-result-object v5

    #@8d
    invoke-static {v5}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@90
    move-result-object v4

    #@91
    .line 1365
    .local v4, sp:Landroid/content/SharedPreferences;
    const-string v5, "VZW"

    #@93
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@96
    move-result v5

    #@97
    if-eqz v5, :cond_ef

    #@99
    .line 1366
    const-string v5, "vm_number_key"

    #@9b
    const-string v6, "*86"

    #@9d
    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@a0
    move-result-object v1

    #@a1
    .line 1374
    :goto_a1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@a4
    move-result v5

    #@a5
    if-eqz v5, :cond_bf

    #@a7
    .line 1375
    if-eqz v0, :cond_f6

    #@a9
    .line 1376
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@ac
    move-result-object v5

    #@ad
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getRVMS(Landroid/content/Context;)Ljava/lang/String;

    #@b0
    move-result-object v1

    #@b1
    .line 1377
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@b4
    move-result v5

    #@b5
    if-eqz v5, :cond_bf

    #@b7
    .line 1378
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@ba
    move-result-object v5

    #@bb
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getVMS(Landroid/content/Context;)Ljava/lang/String;

    #@be
    move-result-object v1

    #@bf
    .line 1389
    :cond_bf
    :goto_bf
    const-string v5, "support_smart_dialing"

    #@c1
    invoke-static {v8, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@c4
    move-result v5

    #@c5
    if-eqz v5, :cond_df

    #@c7
    .line 1390
    const-string v5, "gsm.sim.type"

    #@c9
    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@cc
    move-result-object v5

    #@cd
    const-string v6, "spr"

    #@cf
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d2
    move-result v5

    #@d3
    if-eqz v5, :cond_df

    #@d5
    .line 1391
    const-string v5, "vm_number_key"

    #@d7
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getLine1Number()Ljava/lang/String;

    #@da
    move-result-object v6

    #@db
    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@de
    move-result-object v1

    #@df
    .line 1396
    .end local v4           #sp:Landroid/content/SharedPreferences;
    :cond_df
    return-object v1

    #@e0
    .line 1339
    .end local v0           #is_roaming:Z
    .end local v1           #number:Ljava/lang/String;
    .end local v3           #simInfo:Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
    :cond_e0
    const-string v1, ""

    #@e2
    goto/16 :goto_f

    #@e4
    .line 1355
    .restart local v0       #is_roaming:Z
    .restart local v1       #number:Ljava/lang/String;
    .restart local v3       #simInfo:Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
    :cond_e4
    if-eqz v0, :cond_83

    #@e6
    .line 1356
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@e9
    move-result-object v5

    #@ea
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getRVMS(Landroid/content/Context;)Ljava/lang/String;

    #@ed
    move-result-object v1

    #@ee
    goto :goto_83

    #@ef
    .line 1369
    .restart local v4       #sp:Landroid/content/SharedPreferences;
    :cond_ef
    const-string v5, "vm_number_key"

    #@f1
    invoke-interface {v4, v5, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@f4
    move-result-object v1

    #@f5
    goto :goto_a1

    #@f6
    .line 1382
    :cond_f6
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@f9
    move-result-object v5

    #@fa
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getVMS(Landroid/content/Context;)Ljava/lang/String;

    #@fd
    move-result-object v1

    #@fe
    goto :goto_bf
.end method

.method public handleInCallMmiCommands(Ljava/lang/String;)Z
    .registers 6
    .parameter "dialString"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1080
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->isInCall()Z

    #@4
    move-result v3

    #@5
    if-nez v3, :cond_9

    #@7
    move v1, v2

    #@8
    .line 1115
    :goto_8
    return v1

    #@9
    .line 1084
    :cond_9
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@c
    move-result v3

    #@d
    if-eqz v3, :cond_11

    #@f
    move v1, v2

    #@10
    .line 1085
    goto :goto_8

    #@11
    .line 1088
    :cond_11
    const/4 v1, 0x0

    #@12
    .line 1089
    .local v1, result:Z
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    #@15
    move-result v0

    #@16
    .line 1090
    .local v0, ch:C
    packed-switch v0, :pswitch_data_38

    #@19
    goto :goto_8

    #@1a
    .line 1092
    :pswitch_1a
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->handleCallDeflectionIncallSupplementaryService(Ljava/lang/String;)Z

    #@1d
    move-result v1

    #@1e
    .line 1094
    goto :goto_8

    #@1f
    .line 1096
    :pswitch_1f
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->handleCallWaitingIncallSupplementaryService(Ljava/lang/String;)Z

    #@22
    move-result v1

    #@23
    .line 1098
    goto :goto_8

    #@24
    .line 1100
    :pswitch_24
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->handleCallHoldIncallSupplementaryService(Ljava/lang/String;)Z

    #@27
    move-result v1

    #@28
    .line 1101
    goto :goto_8

    #@29
    .line 1103
    :pswitch_29
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->handleMultipartyIncallSupplementaryService(Ljava/lang/String;)Z

    #@2c
    move-result v1

    #@2d
    .line 1104
    goto :goto_8

    #@2e
    .line 1106
    :pswitch_2e
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->handleEctIncallSupplementaryService(Ljava/lang/String;)Z

    #@31
    move-result v1

    #@32
    .line 1107
    goto :goto_8

    #@33
    .line 1109
    :pswitch_33
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->handleCcbsIncallSupplementaryService(Ljava/lang/String;)Z

    #@36
    move-result v1

    #@37
    .line 1110
    goto :goto_8

    #@38
    .line 1090
    :pswitch_data_38
    .packed-switch 0x30
        :pswitch_1a
        :pswitch_1f
        :pswitch_24
        :pswitch_29
        :pswitch_2e
        :pswitch_33
    .end packed-switch
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 24
    .parameter "msg"

    #@0
    .prologue
    .line 2251
    move-object/from16 v0, p0

    #@2
    iget-boolean v0, v0, Lcom/android/internal/telephony/PhoneBase;->mIsTheCurrentActivePhone:Z

    #@4
    move/from16 v19, v0

    #@6
    if-nez v19, :cond_3b

    #@8
    .line 2252
    const-string v19, "GSM"

    #@a
    new-instance v20, Ljava/lang/StringBuilder;

    #@c
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v21, "Received message "

    #@11
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v20

    #@15
    move-object/from16 v0, v20

    #@17
    move-object/from16 v1, p1

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v20

    #@1d
    const-string v21, "["

    #@1f
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v20

    #@23
    move-object/from16 v0, p1

    #@25
    iget v0, v0, Landroid/os/Message;->what:I

    #@27
    move/from16 v21, v0

    #@29
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v20

    #@2d
    const-string v21, "] while being destroyed. Ignoring."

    #@2f
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v20

    #@33
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v20

    #@37
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 2619
    :cond_3a
    :goto_3a
    :pswitch_3a
    return-void

    #@3b
    .line 2256
    :cond_3b
    move-object/from16 v0, p1

    #@3d
    iget v0, v0, Landroid/os/Message;->what:I

    #@3f
    move/from16 v19, v0

    #@41
    packed-switch v19, :pswitch_data_5ec

    #@44
    .line 2617
    :pswitch_44
    invoke-super/range {p0 .. p1}, Lcom/android/internal/telephony/PhoneBase;->handleMessage(Landroid/os/Message;)V

    #@47
    goto :goto_3a

    #@48
    .line 2258
    :pswitch_48
    move-object/from16 v0, p0

    #@4a
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@4c
    move-object/from16 v19, v0

    #@4e
    const/16 v20, 0x6

    #@50
    move-object/from16 v0, p0

    #@52
    move/from16 v1, v20

    #@54
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->obtainMessage(I)Landroid/os/Message;

    #@57
    move-result-object v20

    #@58
    invoke-interface/range {v19 .. v20}, Lcom/android/internal/telephony/CommandsInterface;->getBasebandVersion(Landroid/os/Message;)V

    #@5b
    .line 2261
    move-object/from16 v0, p0

    #@5d
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@5f
    move-object/from16 v19, v0

    #@61
    const/16 v20, 0x9

    #@63
    move-object/from16 v0, p0

    #@65
    move/from16 v1, v20

    #@67
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->obtainMessage(I)Landroid/os/Message;

    #@6a
    move-result-object v20

    #@6b
    invoke-interface/range {v19 .. v20}, Lcom/android/internal/telephony/CommandsInterface;->getIMEI(Landroid/os/Message;)V

    #@6e
    .line 2262
    move-object/from16 v0, p0

    #@70
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@72
    move-object/from16 v19, v0

    #@74
    const/16 v20, 0xa

    #@76
    move-object/from16 v0, p0

    #@78
    move/from16 v1, v20

    #@7a
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->obtainMessage(I)Landroid/os/Message;

    #@7d
    move-result-object v20

    #@7e
    invoke-interface/range {v19 .. v20}, Lcom/android/internal/telephony/CommandsInterface;->getIMEISV(Landroid/os/Message;)V

    #@81
    .line 2265
    move-object/from16 v0, p0

    #@83
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@85
    move-object/from16 v19, v0

    #@87
    const/16 v20, 0x15

    #@89
    move-object/from16 v0, p0

    #@8b
    move/from16 v1, v20

    #@8d
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->obtainMessage(I)Landroid/os/Message;

    #@90
    move-result-object v20

    #@91
    invoke-interface/range {v19 .. v20}, Lcom/android/internal/telephony/CommandsInterface;->getDeviceIdentity(Landroid/os/Message;)V

    #@94
    goto :goto_3a

    #@95
    .line 2274
    :pswitch_95
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->syncClirSetting()V

    #@98
    goto :goto_3a

    #@99
    .line 2278
    :pswitch_99
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->updateCurrentCarrierInProvider()Z

    #@9c
    .line 2282
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getVmSimImsi()Ljava/lang/String;

    #@9f
    move-result-object v9

    #@a0
    .line 2283
    .local v9, imsi:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getSubscriberId()Ljava/lang/String;

    #@a3
    move-result-object v10

    #@a4
    .line 2284
    .local v10, imsiFromSIM:Ljava/lang/String;
    if-eqz v9, :cond_c9

    #@a6
    if-eqz v10, :cond_c9

    #@a8
    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ab
    move-result v19

    #@ac
    if-nez v19, :cond_c9

    #@ae
    .line 2285
    const/16 v19, 0x0

    #@b0
    move-object/from16 v0, p0

    #@b2
    move-object/from16 v1, v19

    #@b4
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->storeVoiceMailNumber(Ljava/lang/String;)V

    #@b7
    .line 2286
    const/16 v19, 0x0

    #@b9
    move-object/from16 v0, p0

    #@bb
    move/from16 v1, v19

    #@bd
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->setCallForwardingPreference(Z)V

    #@c0
    .line 2287
    const/16 v19, 0x0

    #@c2
    move-object/from16 v0, p0

    #@c4
    move-object/from16 v1, v19

    #@c6
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->setVmSimImsi(Ljava/lang/String;)V

    #@c9
    .line 2289
    :cond_c9
    move-object/from16 v0, p0

    #@cb
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mSimRecordsLoadedRegistrants:Landroid/os/RegistrantList;

    #@cd
    move-object/from16 v19, v0

    #@cf
    invoke-virtual/range {v19 .. v19}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@d2
    .line 2290
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->updateVoiceMail()V

    #@d5
    .line 2291
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->updateCallForwardStatus()V

    #@d8
    .line 2293
    move-object/from16 v0, p0

    #@da
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@dc
    move-object/from16 v19, v0

    #@de
    const/16 v20, 0x15

    #@e0
    move-object/from16 v0, p0

    #@e2
    move/from16 v1, v20

    #@e4
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->obtainMessage(I)Landroid/os/Message;

    #@e7
    move-result-object v20

    #@e8
    invoke-interface/range {v19 .. v20}, Lcom/android/internal/telephony/CommandsInterface;->getDeviceIdentity(Landroid/os/Message;)V

    #@eb
    goto/16 :goto_3a

    #@ed
    .line 2298
    .end local v9           #imsi:Ljava/lang/String;
    .end local v10           #imsiFromSIM:Ljava/lang/String;
    :pswitch_ed
    move-object/from16 v0, p1

    #@ef
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@f1
    check-cast v4, Landroid/os/AsyncResult;

    #@f3
    .line 2300
    .local v4, ar:Landroid/os/AsyncResult;
    iget-object v0, v4, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@f5
    move-object/from16 v19, v0

    #@f7
    if-nez v19, :cond_3a

    #@f9
    .line 2305
    sget-boolean v19, Lcom/android/internal/telephony/gsm/GSMPhone;->ENABLE_PRIVACY_LOG_CALL:Z

    #@fb
    if-eqz v19, :cond_119

    #@fd
    .line 2306
    const-string v19, "GSM"

    #@ff
    new-instance v20, Ljava/lang/StringBuilder;

    #@101
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@104
    const-string v21, "Baseband version: "

    #@106
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@109
    move-result-object v20

    #@10a
    iget-object v0, v4, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@10c
    move-object/from16 v21, v0

    #@10e
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@111
    move-result-object v20

    #@112
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@115
    move-result-object v20

    #@116
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@119
    .line 2309
    :cond_119
    const-string v20, "gsm.version.baseband"

    #@11b
    iget-object v0, v4, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@11d
    move-object/from16 v19, v0

    #@11f
    check-cast v19, Ljava/lang/String;

    #@121
    move-object/from16 v0, p0

    #@123
    move-object/from16 v1, v20

    #@125
    move-object/from16 v2, v19

    #@127
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@12a
    goto/16 :goto_3a

    #@12c
    .line 2314
    .end local v4           #ar:Landroid/os/AsyncResult;
    :pswitch_12c
    move-object/from16 v0, p1

    #@12e
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@130
    check-cast v4, Landroid/os/AsyncResult;

    #@132
    .line 2316
    .restart local v4       #ar:Landroid/os/AsyncResult;
    iget-object v0, v4, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@134
    move-object/from16 v19, v0

    #@136
    if-eqz v19, :cond_155

    #@138
    .line 2320
    move-object/from16 v0, p0

    #@13a
    iget v0, v0, Lcom/android/internal/telephony/gsm/GSMPhone;->retryNum:I

    #@13c
    move/from16 v19, v0

    #@13e
    const/16 v20, 0xa

    #@140
    move/from16 v0, v19

    #@142
    move/from16 v1, v20

    #@144
    if-gt v0, v1, :cond_3a

    #@146
    .line 2321
    const/16 v19, 0x38

    #@148
    const-wide/16 v20, 0x3e8

    #@14a
    move-object/from16 v0, p0

    #@14c
    move/from16 v1, v19

    #@14e
    move-wide/from16 v2, v20

    #@150
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/telephony/gsm/GSMPhone;->sendEmptyMessageDelayed(IJ)Z

    #@153
    goto/16 :goto_3a

    #@155
    .line 2326
    :cond_155
    iget-object v0, v4, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@157
    move-object/from16 v19, v0

    #@159
    check-cast v19, [Ljava/lang/String;

    #@15b
    move-object/from16 v16, v19

    #@15d
    check-cast v16, [Ljava/lang/String;

    #@15f
    .line 2327
    .local v16, respId:[Ljava/lang/String;
    const/16 v19, 0x0

    #@161
    aget-object v19, v16, v19

    #@163
    move-object/from16 v0, v19

    #@165
    move-object/from16 v1, p0

    #@167
    iput-object v0, v1, Lcom/android/internal/telephony/gsm/GSMPhone;->mImei:Ljava/lang/String;

    #@169
    .line 2328
    const/16 v19, 0x1

    #@16b
    aget-object v19, v16, v19

    #@16d
    move-object/from16 v0, v19

    #@16f
    move-object/from16 v1, p0

    #@171
    iput-object v0, v1, Lcom/android/internal/telephony/gsm/GSMPhone;->mImeiSv:Ljava/lang/String;

    #@173
    .line 2329
    const/16 v19, 0x2

    #@175
    aget-object v19, v16, v19

    #@177
    move-object/from16 v0, v19

    #@179
    move-object/from16 v1, p0

    #@17b
    iput-object v0, v1, Lcom/android/internal/telephony/gsm/GSMPhone;->mEsn:Ljava/lang/String;

    #@17d
    .line 2330
    const/16 v19, 0x3

    #@17f
    aget-object v19, v16, v19

    #@181
    move-object/from16 v0, v19

    #@183
    move-object/from16 v1, p0

    #@185
    iput-object v0, v1, Lcom/android/internal/telephony/gsm/GSMPhone;->mMeid:Ljava/lang/String;

    #@187
    .line 2336
    const/16 v19, 0x0

    #@189
    move/from16 v0, v19

    #@18b
    move-object/from16 v1, p0

    #@18d
    iput v0, v1, Lcom/android/internal/telephony/gsm/GSMPhone;->retryNum:I

    #@18f
    goto/16 :goto_3a

    #@191
    .line 2345
    .end local v4           #ar:Landroid/os/AsyncResult;
    .end local v16           #respId:[Ljava/lang/String;
    :pswitch_191
    const-string v19, "GSM"

    #@193
    new-instance v20, Ljava/lang/StringBuilder;

    #@195
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@198
    const-string v21, "EVENT_RETRY_GET_DEVICE_IDENTITY : retryNum = "

    #@19a
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19d
    move-result-object v20

    #@19e
    move-object/from16 v0, p0

    #@1a0
    iget v0, v0, Lcom/android/internal/telephony/gsm/GSMPhone;->retryNum:I

    #@1a2
    move/from16 v21, v0

    #@1a4
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a7
    move-result-object v20

    #@1a8
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ab
    move-result-object v20

    #@1ac
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1af
    .line 2346
    move-object/from16 v0, p0

    #@1b1
    iget v0, v0, Lcom/android/internal/telephony/gsm/GSMPhone;->retryNum:I

    #@1b3
    move/from16 v19, v0

    #@1b5
    add-int/lit8 v19, v19, 0x1

    #@1b7
    move/from16 v0, v19

    #@1b9
    move-object/from16 v1, p0

    #@1bb
    iput v0, v1, Lcom/android/internal/telephony/gsm/GSMPhone;->retryNum:I

    #@1bd
    .line 2347
    move-object/from16 v0, p0

    #@1bf
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1c1
    move-object/from16 v19, v0

    #@1c3
    const/16 v20, 0x15

    #@1c5
    move-object/from16 v0, p0

    #@1c7
    move/from16 v1, v20

    #@1c9
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->obtainMessage(I)Landroid/os/Message;

    #@1cc
    move-result-object v20

    #@1cd
    invoke-interface/range {v19 .. v20}, Lcom/android/internal/telephony/CommandsInterface;->getDeviceIdentity(Landroid/os/Message;)V

    #@1d0
    goto/16 :goto_3a

    #@1d2
    .line 2353
    :pswitch_1d2
    move-object/from16 v0, p1

    #@1d4
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1d6
    check-cast v4, Landroid/os/AsyncResult;

    #@1d8
    .line 2355
    .restart local v4       #ar:Landroid/os/AsyncResult;
    iget-object v0, v4, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1da
    move-object/from16 v19, v0

    #@1dc
    if-nez v19, :cond_3a

    #@1de
    .line 2359
    iget-object v0, v4, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@1e0
    move-object/from16 v19, v0

    #@1e2
    check-cast v19, Ljava/lang/String;

    #@1e4
    move-object/from16 v0, v19

    #@1e6
    move-object/from16 v1, p0

    #@1e8
    iput-object v0, v1, Lcom/android/internal/telephony/gsm/GSMPhone;->mImei:Ljava/lang/String;

    #@1ea
    .line 2362
    const-string v19, "gsm.baseband.imei"

    #@1ec
    move-object/from16 v0, p0

    #@1ee
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GSMPhone;->mImei:Ljava/lang/String;

    #@1f0
    move-object/from16 v20, v0

    #@1f2
    move-object/from16 v0, p0

    #@1f4
    move-object/from16 v1, v19

    #@1f6
    move-object/from16 v2, v20

    #@1f8
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@1fb
    goto/16 :goto_3a

    #@1fd
    .line 2367
    .end local v4           #ar:Landroid/os/AsyncResult;
    :pswitch_1fd
    move-object/from16 v0, p1

    #@1ff
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@201
    check-cast v4, Landroid/os/AsyncResult;

    #@203
    .line 2369
    .restart local v4       #ar:Landroid/os/AsyncResult;
    iget-object v0, v4, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@205
    move-object/from16 v19, v0

    #@207
    if-nez v19, :cond_3a

    #@209
    .line 2373
    iget-object v0, v4, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@20b
    move-object/from16 v19, v0

    #@20d
    check-cast v19, Ljava/lang/String;

    #@20f
    move-object/from16 v0, v19

    #@211
    move-object/from16 v1, p0

    #@213
    iput-object v0, v1, Lcom/android/internal/telephony/gsm/GSMPhone;->mImeiSv:Ljava/lang/String;

    #@215
    goto/16 :goto_3a

    #@217
    .line 2377
    .end local v4           #ar:Landroid/os/AsyncResult;
    :pswitch_217
    move-object/from16 v0, p1

    #@219
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@21b
    check-cast v4, Landroid/os/AsyncResult;

    #@21d
    .line 2379
    .restart local v4       #ar:Landroid/os/AsyncResult;
    iget-object v0, v4, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@21f
    move-object/from16 v19, v0

    #@221
    check-cast v19, [Ljava/lang/String;

    #@223
    move-object/from16 v18, v19

    #@225
    check-cast v18, [Ljava/lang/String;

    #@227
    .line 2381
    .local v18, ussdResult:[Ljava/lang/String;
    move-object/from16 v0, v18

    #@229
    array-length v0, v0

    #@22a
    move/from16 v19, v0

    #@22c
    const/16 v20, 0x1

    #@22e
    move/from16 v0, v19

    #@230
    move/from16 v1, v20

    #@232
    if-le v0, v1, :cond_3a

    #@234
    .line 2384
    :try_start_234
    const-string v19, "KR"

    #@236
    const-string v20, "KT"

    #@238
    invoke-static/range {v19 .. v20}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@23b
    move-result v19

    #@23c
    if-eqz v19, :cond_275

    #@23e
    .line 2386
    move-object/from16 v0, v18

    #@240
    array-length v0, v0

    #@241
    move/from16 v19, v0

    #@243
    const/16 v20, 0x2

    #@245
    move/from16 v0, v19

    #@247
    move/from16 v1, v20

    #@249
    if-le v0, v1, :cond_25e

    #@24b
    .line 2387
    move-object/from16 v0, p0

    #@24d
    move-object/from16 v1, v18

    #@24f
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->onIncomingUSSD_KT([Ljava/lang/String;)V
    :try_end_252
    .catch Ljava/lang/NumberFormatException; {:try_start_234 .. :try_end_252} :catch_254

    #@252
    goto/16 :goto_3a

    #@254
    .line 2394
    :catch_254
    move-exception v6

    #@255
    .line 2395
    .local v6, e:Ljava/lang/NumberFormatException;
    const-string v19, "GSM"

    #@257
    const-string v20, "error parsing USSD"

    #@259
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@25c
    goto/16 :goto_3a

    #@25e
    .line 2389
    .end local v6           #e:Ljava/lang/NumberFormatException;
    :cond_25e
    const/16 v19, 0x0

    #@260
    :try_start_260
    aget-object v19, v18, v19

    #@262
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@265
    move-result v19

    #@266
    const/16 v20, 0x1

    #@268
    aget-object v20, v18, v20

    #@26a
    move-object/from16 v0, p0

    #@26c
    move/from16 v1, v19

    #@26e
    move-object/from16 v2, v20

    #@270
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->onIncomingUSSD(ILjava/lang/String;)V

    #@273
    goto/16 :goto_3a

    #@275
    .line 2393
    :cond_275
    const/16 v19, 0x0

    #@277
    aget-object v19, v18, v19

    #@279
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@27c
    move-result v19

    #@27d
    const/16 v20, 0x1

    #@27f
    aget-object v20, v18, v20

    #@281
    move-object/from16 v0, p0

    #@283
    move/from16 v1, v19

    #@285
    move-object/from16 v2, v20

    #@287
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->onIncomingUSSD(ILjava/lang/String;)V
    :try_end_28a
    .catch Ljava/lang/NumberFormatException; {:try_start_260 .. :try_end_28a} :catch_254

    #@28a
    goto/16 :goto_3a

    #@28c
    .line 2418
    .end local v4           #ar:Landroid/os/AsyncResult;
    .end local v18           #ussdResult:[Ljava/lang/String;
    :pswitch_28c
    const-string v19, "ims"

    #@28e
    const-string v20, "ro.product.ims"

    #@290
    invoke-static/range {v20 .. v20}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@293
    move-result-object v20

    #@294
    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@297
    move-result v19

    #@298
    if-nez v19, :cond_3a

    #@29a
    .line 2425
    const/4 v7, 0x0

    #@29b
    .local v7, i:I
    move-object/from16 v0, p0

    #@29d
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GSMPhone;->mPendingMMIs:Ljava/util/ArrayList;

    #@29f
    move-object/from16 v19, v0

    #@2a1
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    #@2a4
    move-result v17

    #@2a5
    .local v17, s:I
    :goto_2a5
    move/from16 v0, v17

    #@2a7
    if-ge v7, v0, :cond_3a

    #@2a9
    .line 2426
    move-object/from16 v0, p0

    #@2ab
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GSMPhone;->mPendingMMIs:Ljava/util/ArrayList;

    #@2ad
    move-object/from16 v19, v0

    #@2af
    move-object/from16 v0, v19

    #@2b1
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2b4
    move-result-object v19

    #@2b5
    check-cast v19, Lcom/android/internal/telephony/gsm/GsmMmiCode;

    #@2b7
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isPendingUSSD()Z

    #@2ba
    move-result v19

    #@2bb
    if-eqz v19, :cond_2ce

    #@2bd
    .line 2427
    move-object/from16 v0, p0

    #@2bf
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GSMPhone;->mPendingMMIs:Ljava/util/ArrayList;

    #@2c1
    move-object/from16 v19, v0

    #@2c3
    move-object/from16 v0, v19

    #@2c5
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2c8
    move-result-object v19

    #@2c9
    check-cast v19, Lcom/android/internal/telephony/gsm/GsmMmiCode;

    #@2cb
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->onUssdFinishedError()V

    #@2ce
    .line 2425
    :cond_2ce
    add-int/lit8 v7, v7, 0x1

    #@2d0
    goto :goto_2a5

    #@2d1
    .line 2439
    .end local v7           #i:I
    .end local v17           #s:I
    :pswitch_2d1
    move-object/from16 v0, p1

    #@2d3
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2d5
    check-cast v4, Landroid/os/AsyncResult;

    #@2d7
    .line 2440
    .restart local v4       #ar:Landroid/os/AsyncResult;
    iget-object v12, v4, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@2d9
    check-cast v12, Lcom/android/internal/telephony/gsm/SuppServiceNotification;

    #@2db
    .line 2441
    .local v12, not:Lcom/android/internal/telephony/gsm/SuppServiceNotification;
    move-object/from16 v0, p0

    #@2dd
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSsnRegistrants:Landroid/os/RegistrantList;

    #@2df
    move-object/from16 v19, v0

    #@2e1
    move-object/from16 v0, v19

    #@2e3
    invoke-virtual {v0, v4}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@2e6
    goto/16 :goto_3a

    #@2e8
    .line 2445
    .end local v4           #ar:Landroid/os/AsyncResult;
    .end local v12           #not:Lcom/android/internal/telephony/gsm/SuppServiceNotification;
    :pswitch_2e8
    move-object/from16 v0, p1

    #@2ea
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2ec
    check-cast v4, Landroid/os/AsyncResult;

    #@2ee
    .line 2446
    .restart local v4       #ar:Landroid/os/AsyncResult;
    move-object/from16 v0, p0

    #@2f0
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@2f2
    move-object/from16 v19, v0

    #@2f4
    invoke-virtual/range {v19 .. v19}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@2f7
    move-result-object v15

    #@2f8
    check-cast v15, Lcom/android/internal/telephony/uicc/IccRecords;

    #@2fa
    .line 2447
    .local v15, r:Lcom/android/internal/telephony/uicc/IccRecords;
    iget-object v0, v4, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@2fc
    move-object/from16 v19, v0

    #@2fe
    if-nez v19, :cond_33a

    #@300
    if-eqz v15, :cond_33a

    #@302
    .line 2448
    const/16 v20, 0x1

    #@304
    move-object/from16 v0, p1

    #@306
    iget v0, v0, Landroid/os/Message;->arg1:I

    #@308
    move/from16 v19, v0

    #@30a
    const/16 v21, 0x1

    #@30c
    move/from16 v0, v19

    #@30e
    move/from16 v1, v21

    #@310
    if-ne v0, v1, :cond_354

    #@312
    const/16 v19, 0x1

    #@314
    :goto_314
    move-object/from16 v0, p0

    #@316
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSetCfNumber:Ljava/lang/String;

    #@318
    move-object/from16 v21, v0

    #@31a
    move/from16 v0, v20

    #@31c
    move/from16 v1, v19

    #@31e
    move-object/from16 v2, v21

    #@320
    invoke-virtual {v15, v0, v1, v2}, Lcom/android/internal/telephony/uicc/IccRecords;->setVoiceCallForwardingFlag(IZLjava/lang/String;)V

    #@323
    .line 2449
    move-object/from16 v0, p1

    #@325
    iget v0, v0, Landroid/os/Message;->arg1:I

    #@327
    move/from16 v19, v0

    #@329
    const/16 v20, 0x1

    #@32b
    move/from16 v0, v19

    #@32d
    move/from16 v1, v20

    #@32f
    if-ne v0, v1, :cond_357

    #@331
    const/16 v19, 0x1

    #@333
    :goto_333
    move-object/from16 v0, p0

    #@335
    move/from16 v1, v19

    #@337
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->setCallForwardingPreference(Z)V

    #@33a
    .line 2451
    :cond_33a
    iget-object v13, v4, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@33c
    check-cast v13, Landroid/os/Message;

    #@33e
    .line 2452
    .local v13, onComplete:Landroid/os/Message;
    if-eqz v13, :cond_3a

    #@340
    .line 2453
    iget-object v0, v4, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@342
    move-object/from16 v19, v0

    #@344
    iget-object v0, v4, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@346
    move-object/from16 v20, v0

    #@348
    move-object/from16 v0, v19

    #@34a
    move-object/from16 v1, v20

    #@34c
    invoke-static {v13, v0, v1}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@34f
    .line 2454
    invoke-virtual {v13}, Landroid/os/Message;->sendToTarget()V

    #@352
    goto/16 :goto_3a

    #@354
    .line 2448
    .end local v13           #onComplete:Landroid/os/Message;
    :cond_354
    const/16 v19, 0x0

    #@356
    goto :goto_314

    #@357
    .line 2449
    :cond_357
    const/16 v19, 0x0

    #@359
    goto :goto_333

    #@35a
    .line 2459
    .end local v4           #ar:Landroid/os/AsyncResult;
    .end local v15           #r:Lcom/android/internal/telephony/uicc/IccRecords;
    :pswitch_35a
    move-object/from16 v0, p1

    #@35c
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@35e
    check-cast v4, Landroid/os/AsyncResult;

    #@360
    .line 2460
    .restart local v4       #ar:Landroid/os/AsyncResult;
    const-class v19, Lcom/android/internal/telephony/uicc/IccVmNotSupportedException;

    #@362
    iget-object v0, v4, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@364
    move-object/from16 v20, v0

    #@366
    invoke-virtual/range {v19 .. v20}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    #@369
    move-result v19

    #@36a
    if-eqz v19, :cond_37f

    #@36c
    .line 2461
    move-object/from16 v0, p0

    #@36e
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GSMPhone;->mVmNumber:Ljava/lang/String;

    #@370
    move-object/from16 v19, v0

    #@372
    move-object/from16 v0, p0

    #@374
    move-object/from16 v1, v19

    #@376
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->storeVoiceMailNumber(Ljava/lang/String;)V

    #@379
    .line 2462
    const/16 v19, 0x0

    #@37b
    move-object/from16 v0, v19

    #@37d
    iput-object v0, v4, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@37f
    .line 2464
    :cond_37f
    iget-object v13, v4, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@381
    check-cast v13, Landroid/os/Message;

    #@383
    .line 2465
    .restart local v13       #onComplete:Landroid/os/Message;
    if-eqz v13, :cond_3a

    #@385
    .line 2466
    iget-object v0, v4, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@387
    move-object/from16 v19, v0

    #@389
    iget-object v0, v4, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@38b
    move-object/from16 v20, v0

    #@38d
    move-object/from16 v0, v19

    #@38f
    move-object/from16 v1, v20

    #@391
    invoke-static {v13, v0, v1}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@394
    .line 2467
    invoke-virtual {v13}, Landroid/os/Message;->sendToTarget()V

    #@397
    goto/16 :goto_3a

    #@399
    .line 2473
    .end local v4           #ar:Landroid/os/AsyncResult;
    .end local v13           #onComplete:Landroid/os/Message;
    :pswitch_399
    move-object/from16 v0, p1

    #@39b
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@39d
    check-cast v4, Landroid/os/AsyncResult;

    #@39f
    .line 2474
    .restart local v4       #ar:Landroid/os/AsyncResult;
    iget-object v0, v4, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@3a1
    move-object/from16 v19, v0

    #@3a3
    if-nez v19, :cond_3b4

    #@3a5
    .line 2475
    iget-object v0, v4, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@3a7
    move-object/from16 v19, v0

    #@3a9
    check-cast v19, [Lcom/android/internal/telephony/CallForwardInfo;

    #@3ab
    check-cast v19, [Lcom/android/internal/telephony/CallForwardInfo;

    #@3ad
    move-object/from16 v0, p0

    #@3af
    move-object/from16 v1, v19

    #@3b1
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->handleCfuQueryResult([Lcom/android/internal/telephony/CallForwardInfo;)V

    #@3b4
    .line 2477
    :cond_3b4
    iget-object v13, v4, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@3b6
    check-cast v13, Landroid/os/Message;

    #@3b8
    .line 2478
    .restart local v13       #onComplete:Landroid/os/Message;
    if-eqz v13, :cond_3a

    #@3ba
    .line 2479
    iget-object v0, v4, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@3bc
    move-object/from16 v19, v0

    #@3be
    iget-object v0, v4, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@3c0
    move-object/from16 v20, v0

    #@3c2
    move-object/from16 v0, v19

    #@3c4
    move-object/from16 v1, v20

    #@3c6
    invoke-static {v13, v0, v1}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@3c9
    .line 2480
    invoke-virtual {v13}, Landroid/os/Message;->sendToTarget()V

    #@3cc
    goto/16 :goto_3a

    #@3ce
    .line 2486
    .end local v4           #ar:Landroid/os/AsyncResult;
    .end local v13           #onComplete:Landroid/os/Message;
    :pswitch_3ce
    move-object/from16 v0, p1

    #@3d0
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3d2
    check-cast v4, Landroid/os/AsyncResult;

    #@3d4
    .line 2487
    .restart local v4       #ar:Landroid/os/AsyncResult;
    move-object/from16 v0, p0

    #@3d6
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSST:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@3d8
    move-object/from16 v19, v0

    #@3da
    move-object/from16 v0, v19

    #@3dc
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@3de
    move-object/from16 v19, v0

    #@3e0
    invoke-virtual/range {v19 .. v19}, Landroid/telephony/ServiceState;->getIsManualSelection()Z

    #@3e3
    move-result v19

    #@3e4
    if-eqz v19, :cond_3f5

    #@3e6
    .line 2488
    iget-object v0, v4, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@3e8
    move-object/from16 v19, v0

    #@3ea
    check-cast v19, Landroid/os/Message;

    #@3ec
    move-object/from16 v0, p0

    #@3ee
    move-object/from16 v1, v19

    #@3f0
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->setNetworkSelectionModeAutomatic(Landroid/os/Message;)V

    #@3f3
    goto/16 :goto_3a

    #@3f5
    .line 2492
    :cond_3f5
    const-string v19, "GSM"

    #@3f7
    const-string v20, "Stop duplicate SET_NETWORK_SELECTION_AUTOMATIC to Ril "

    #@3f9
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3fc
    goto/16 :goto_3a

    #@3fe
    .line 2497
    .end local v4           #ar:Landroid/os/AsyncResult;
    :pswitch_3fe
    move-object/from16 v0, p1

    #@400
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@402
    check-cast v4, Landroid/os/AsyncResult;

    #@404
    .line 2498
    .restart local v4       #ar:Landroid/os/AsyncResult;
    iget-object v0, v4, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@406
    move-object/from16 v19, v0

    #@408
    check-cast v19, Ljava/lang/Integer;

    #@40a
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    #@40d
    move-result v19

    #@40e
    move-object/from16 v0, p0

    #@410
    move/from16 v1, v19

    #@412
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->processIccRecordEvents(I)V

    #@415
    goto/16 :goto_3a

    #@417
    .line 2507
    .end local v4           #ar:Landroid/os/AsyncResult;
    :pswitch_417
    move-object/from16 v0, p1

    #@419
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@41b
    move-object/from16 v19, v0

    #@41d
    check-cast v19, Landroid/os/AsyncResult;

    #@41f
    move-object/from16 v0, p0

    #@421
    move-object/from16 v1, v19

    #@423
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->handleSetSelectNetwork(Landroid/os/AsyncResult;)V

    #@426
    goto/16 :goto_3a

    #@428
    .line 2511
    :pswitch_428
    move-object/from16 v0, p1

    #@42a
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@42c
    check-cast v4, Landroid/os/AsyncResult;

    #@42e
    .line 2512
    .restart local v4       #ar:Landroid/os/AsyncResult;
    iget-object v0, v4, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@430
    move-object/from16 v19, v0

    #@432
    if-nez v19, :cond_441

    #@434
    .line 2513
    move-object/from16 v0, p1

    #@436
    iget v0, v0, Landroid/os/Message;->arg1:I

    #@438
    move/from16 v19, v0

    #@43a
    move-object/from16 v0, p0

    #@43c
    move/from16 v1, v19

    #@43e
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->saveClirSetting(I)V

    #@441
    .line 2515
    :cond_441
    iget-object v13, v4, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@443
    check-cast v13, Landroid/os/Message;

    #@445
    .line 2516
    .restart local v13       #onComplete:Landroid/os/Message;
    if-eqz v13, :cond_3a

    #@447
    .line 2517
    iget-object v0, v4, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@449
    move-object/from16 v19, v0

    #@44b
    iget-object v0, v4, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@44d
    move-object/from16 v20, v0

    #@44f
    move-object/from16 v0, v19

    #@451
    move-object/from16 v1, v20

    #@453
    invoke-static {v13, v0, v1}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@456
    .line 2518
    invoke-virtual {v13}, Landroid/os/Message;->sendToTarget()V

    #@459
    goto/16 :goto_3a

    #@45b
    .line 2523
    .end local v4           #ar:Landroid/os/AsyncResult;
    .end local v13           #onComplete:Landroid/os/Message;
    :pswitch_45b
    move-object/from16 v0, p1

    #@45d
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@45f
    check-cast v4, Landroid/os/AsyncResult;

    #@461
    .line 2524
    .restart local v4       #ar:Landroid/os/AsyncResult;
    const-string v19, "GSM"

    #@463
    const-string v20, "Event EVENT_SS received"

    #@465
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@468
    .line 2528
    new-instance v11, Lcom/android/internal/telephony/gsm/GsmMmiCode;

    #@46a
    move-object/from16 v0, p0

    #@46c
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mUiccApplication:Ljava/util/concurrent/atomic/AtomicReference;

    #@46e
    move-object/from16 v19, v0

    #@470
    invoke-virtual/range {v19 .. v19}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@473
    move-result-object v19

    #@474
    check-cast v19, Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@476
    move-object/from16 v0, p0

    #@478
    move-object/from16 v1, v19

    #@47a
    invoke-direct {v11, v0, v1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;-><init>(Lcom/android/internal/telephony/gsm/GSMPhone;Lcom/android/internal/telephony/uicc/UiccCardApplication;)V

    #@47d
    .line 2529
    .local v11, mmi:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    invoke-virtual {v11, v4}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->processSsData(Landroid/os/AsyncResult;)V

    #@480
    goto/16 :goto_3a

    #@482
    .line 2533
    .end local v4           #ar:Landroid/os/AsyncResult;
    .end local v11           #mmi:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    :pswitch_482
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getCallForwardingPreference()Z

    #@485
    move-result v5

    #@486
    .line 2534
    .local v5, cfEnabled:Z
    const-string v19, "GSM"

    #@488
    new-instance v20, Ljava/lang/StringBuilder;

    #@48a
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@48d
    const-string v21, "Callforwarding is "

    #@48f
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@492
    move-result-object v20

    #@493
    move-object/from16 v0, v20

    #@495
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@498
    move-result-object v20

    #@499
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49c
    move-result-object v20

    #@49d
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4a0
    .line 2535
    if-eqz v5, :cond_3a

    #@4a2
    .line 2536
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifyCallForwardingIndicator()V

    #@4a5
    goto/16 :goto_3a

    #@4a7
    .line 2542
    .end local v5           #cfEnabled:Z
    :pswitch_4a7
    move-object/from16 v0, p1

    #@4a9
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4ab
    check-cast v4, Landroid/os/AsyncResult;

    #@4ad
    .line 2543
    .restart local v4       #ar:Landroid/os/AsyncResult;
    iget-object v0, v4, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@4af
    move-object/from16 v19, v0

    #@4b1
    if-nez v19, :cond_3a

    #@4b3
    .line 2547
    iget-object v0, v4, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@4b5
    move-object/from16 v19, v0

    #@4b7
    if-eqz v19, :cond_4d1

    #@4b9
    .line 2548
    const-string v19, "[GSMPhone]"

    #@4bb
    const-string v20, "[EVENT_GET_ENGINEERING_MODE_INFO_DONE] EmodeInfo = not NULL"

    #@4bd
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@4c0
    .line 2549
    iget-object v0, v4, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@4c2
    move-object/from16 v19, v0

    #@4c4
    check-cast v19, Ljava/lang/String;

    #@4c6
    move-object/from16 v0, v19

    #@4c8
    move-object/from16 v1, p0

    #@4ca
    iput-object v0, v1, Lcom/android/internal/telephony/gsm/GSMPhone;->mEmodeModemInfo:Ljava/lang/String;

    #@4cc
    .line 2550
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->makeEngineeringModeInfo()V

    #@4cf
    goto/16 :goto_3a

    #@4d1
    .line 2553
    :cond_4d1
    const-string v19, "[GSMPhone]"

    #@4d3
    const-string v20, "[EVENT_GET_ENGINEERING_MODE_INFO_DONE] EmodeInfo = NULL"

    #@4d5
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@4d8
    goto/16 :goto_3a

    #@4da
    .line 2560
    .end local v4           #ar:Landroid/os/AsyncResult;
    :pswitch_4da
    invoke-direct/range {p0 .. p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->handleEnterEmergencyCallbackMode(Landroid/os/Message;)V

    #@4dd
    goto/16 :goto_3a

    #@4df
    .line 2565
    :pswitch_4df
    invoke-direct/range {p0 .. p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->handleExitEmergencyCallbackMode(Landroid/os/Message;)V

    #@4e2
    goto/16 :goto_3a

    #@4e4
    .line 2573
    :pswitch_4e4
    const-string v19, "ims"

    #@4e6
    const-string v20, "ro.product.ims"

    #@4e8
    invoke-static/range {v20 .. v20}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@4eb
    move-result-object v20

    #@4ec
    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4ef
    move-result v19

    #@4f0
    if-eqz v19, :cond_3a

    #@4f2
    .line 2574
    move-object/from16 v0, p1

    #@4f4
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4f6
    check-cast v4, Landroid/os/AsyncResult;

    #@4f8
    .line 2576
    .restart local v4       #ar:Landroid/os/AsyncResult;
    iget-object v0, v4, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@4fa
    move-object/from16 v19, v0

    #@4fc
    if-nez v19, :cond_558

    #@4fe
    .line 2577
    iget-object v0, v4, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@500
    move-object/from16 v19, v0

    #@502
    check-cast v19, [I

    #@504
    check-cast v19, [I

    #@506
    const/16 v20, 0x0

    #@508
    aget v19, v19, v20

    #@50a
    move/from16 v0, v19

    #@50c
    move-object/from16 v1, p0

    #@50e
    iput v0, v1, Lcom/android/internal/telephony/gsm/GSMPhone;->mISimSessionId:I

    #@510
    .line 2578
    sget-boolean v19, Lcom/android/internal/telephony/gsm/GSMPhone;->ENABLE_PRIVACY_LOG:Z

    #@512
    if-eqz v19, :cond_532

    #@514
    .line 2579
    const-string v19, "GSM"

    #@516
    new-instance v20, Ljava/lang/StringBuilder;

    #@518
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@51b
    const-string v21, "ISIM session started with Id = "

    #@51d
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@520
    move-result-object v20

    #@521
    move-object/from16 v0, p0

    #@523
    iget v0, v0, Lcom/android/internal/telephony/gsm/GSMPhone;->mISimSessionId:I

    #@525
    move/from16 v21, v0

    #@527
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@52a
    move-result-object v20

    #@52b
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52e
    move-result-object v20

    #@52f
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@532
    .line 2586
    :cond_532
    :goto_532
    move-object/from16 v0, p0

    #@534
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GSMPhone;->mPendingOperation:Ljava/util/List;

    #@536
    move-object/from16 v19, v0

    #@538
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@53b
    move-result-object v8

    #@53c
    .local v8, i$:Ljava/util/Iterator;
    :goto_53c
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@53f
    move-result v19

    #@540
    if-eqz v19, :cond_57d

    #@542
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@545
    move-result-object v14

    #@546
    check-cast v14, Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;

    #@548
    .line 2587
    .local v14, op:Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;
    iget-object v0, v4, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@54a
    move-object/from16 v19, v0

    #@54c
    iget-object v0, v4, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@54e
    move-object/from16 v20, v0

    #@550
    move-object/from16 v0, v19

    #@552
    move-object/from16 v1, v20

    #@554
    invoke-virtual {v14, v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;->onSessionStarted(Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@557
    goto :goto_53c

    #@558
    .line 2582
    .end local v8           #i$:Ljava/util/Iterator;
    .end local v14           #op:Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;
    :cond_558
    const/16 v19, -0x1

    #@55a
    move/from16 v0, v19

    #@55c
    move-object/from16 v1, p0

    #@55e
    iput v0, v1, Lcom/android/internal/telephony/gsm/GSMPhone;->mISimSessionId:I

    #@560
    .line 2583
    const-string v19, "GSM"

    #@562
    new-instance v20, Ljava/lang/StringBuilder;

    #@564
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@567
    const-string v21, "Failed to select ISIM app: "

    #@569
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56c
    move-result-object v20

    #@56d
    iget-object v0, v4, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@56f
    move-object/from16 v21, v0

    #@571
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@574
    move-result-object v20

    #@575
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@578
    move-result-object v20

    #@579
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@57c
    goto :goto_532

    #@57d
    .line 2589
    .restart local v8       #i$:Ljava/util/Iterator;
    :cond_57d
    move-object/from16 v0, p0

    #@57f
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GSMPhone;->mLogicalChannelSetupLock:Ljava/util/concurrent/Semaphore;

    #@581
    move-object/from16 v19, v0

    #@583
    invoke-virtual/range {v19 .. v19}, Ljava/util/concurrent/Semaphore;->release()V

    #@586
    .line 2590
    move-object/from16 v0, p0

    #@588
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GSMPhone;->mPendingOperation:Ljava/util/List;

    #@58a
    move-object/from16 v19, v0

    #@58c
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->clear()V

    #@58f
    goto/16 :goto_3a

    #@591
    .line 2595
    .end local v4           #ar:Landroid/os/AsyncResult;
    .end local v8           #i$:Ljava/util/Iterator;
    :pswitch_591
    const-string v19, "ims"

    #@593
    const-string v20, "ro.product.ims"

    #@595
    invoke-static/range {v20 .. v20}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@598
    move-result-object v20

    #@599
    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@59c
    move-result v19

    #@59d
    if-eqz v19, :cond_3a

    #@59f
    .line 2596
    const/16 v19, -0x1

    #@5a1
    move/from16 v0, v19

    #@5a3
    move-object/from16 v1, p0

    #@5a5
    iput v0, v1, Lcom/android/internal/telephony/gsm/GSMPhone;->mISimSessionId:I

    #@5a7
    .line 2597
    const-string v19, "GSM"

    #@5a9
    const-string v20, "ISIM session stopped"

    #@5ab
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5ae
    .line 2598
    move-object/from16 v0, p0

    #@5b0
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GSMPhone;->mLogicalChannelSetupLock:Ljava/util/concurrent/Semaphore;

    #@5b2
    move-object/from16 v19, v0

    #@5b4
    invoke-virtual/range {v19 .. v19}, Ljava/util/concurrent/Semaphore;->release()V

    #@5b7
    goto/16 :goto_3a

    #@5b9
    .line 2605
    :pswitch_5b9
    const-string v19, "ims"

    #@5bb
    const-string v20, "ro.product.ims"

    #@5bd
    invoke-static/range {v20 .. v20}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5c0
    move-result-object v20

    #@5c1
    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5c4
    move-result v19

    #@5c5
    if-eqz v19, :cond_3a

    #@5c7
    .line 2606
    const-string v19, "GSM"

    #@5c9
    const-string v20, "ISIM Authentication done"

    #@5cb
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5ce
    .line 2607
    move-object/from16 v0, p1

    #@5d0
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@5d2
    check-cast v4, Landroid/os/AsyncResult;

    #@5d4
    .line 2608
    .restart local v4       #ar:Landroid/os/AsyncResult;
    iget-object v13, v4, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@5d6
    check-cast v13, Landroid/os/Message;

    #@5d8
    .line 2609
    .restart local v13       #onComplete:Landroid/os/Message;
    iget-object v0, v4, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@5da
    move-object/from16 v19, v0

    #@5dc
    iget-object v0, v4, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@5de
    move-object/from16 v20, v0

    #@5e0
    move-object/from16 v0, v19

    #@5e2
    move-object/from16 v1, v20

    #@5e4
    invoke-static {v13, v0, v1}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@5e7
    .line 2610
    invoke-virtual {v13}, Landroid/os/Message;->sendToTarget()V

    #@5ea
    goto/16 :goto_3a

    #@5ec
    .line 2256
    :pswitch_data_5ec
    .packed-switch 0x1
        :pswitch_48
        :pswitch_2d1
        :pswitch_99
        :pswitch_44
        :pswitch_3a
        :pswitch_ed
        :pswitch_217
        :pswitch_28c
        :pswitch_1d2
        :pswitch_1fd
        :pswitch_44
        :pswitch_2e8
        :pswitch_399
        :pswitch_44
        :pswitch_44
        :pswitch_417
        :pswitch_417
        :pswitch_428
        :pswitch_95
        :pswitch_35a
        :pswitch_12c
        :pswitch_44
        :pswitch_44
        :pswitch_44
        :pswitch_4da
        :pswitch_4df
        :pswitch_44
        :pswitch_3ce
        :pswitch_3fe
        :pswitch_44
        :pswitch_45b
        :pswitch_44
        :pswitch_44
        :pswitch_44
        :pswitch_44
        :pswitch_44
        :pswitch_4a7
        :pswitch_5b9
        :pswitch_4e4
        :pswitch_591
        :pswitch_5b9
        :pswitch_5b9
        :pswitch_44
        :pswitch_44
        :pswitch_44
        :pswitch_44
        :pswitch_44
        :pswitch_44
        :pswitch_44
        :pswitch_44
        :pswitch_44
        :pswitch_44
        :pswitch_44
        :pswitch_44
        :pswitch_44
        :pswitch_191
        :pswitch_44
        :pswitch_44
        :pswitch_44
        :pswitch_44
        :pswitch_417
        :pswitch_44
        :pswitch_44
        :pswitch_44
        :pswitch_44
        :pswitch_44
        :pswitch_44
        :pswitch_44
        :pswitch_44
        :pswitch_44
        :pswitch_44
        :pswitch_44
        :pswitch_44
        :pswitch_44
        :pswitch_482
    .end packed-switch
.end method

.method public handlePinMmi(Ljava/lang/String;)Z
    .registers 6
    .parameter "dialString"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1272
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccApplication:Ljava/util/concurrent/atomic/AtomicReference;

    #@3
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@6
    move-result-object v1

    #@7
    check-cast v1, Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@9
    invoke-static {p1, p0, v1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->newFromDialString(Ljava/lang/String;Lcom/android/internal/telephony/gsm/GSMPhone;Lcom/android/internal/telephony/uicc/UiccCardApplication;)Lcom/android/internal/telephony/gsm/GsmMmiCode;

    #@c
    move-result-object v0

    #@d
    .line 1274
    .local v0, mmi:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    if-eqz v0, :cond_29

    #@f
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isPinCommand()Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_29

    #@15
    .line 1275
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mPendingMMIs:Ljava/util/ArrayList;

    #@17
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1a
    .line 1276
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mMmiRegistrants:Landroid/os/RegistrantList;

    #@1c
    new-instance v2, Landroid/os/AsyncResult;

    #@1e
    invoke-direct {v2, v3, v0, v3}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@21
    invoke-virtual {v1, v2}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@24
    .line 1277
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->processCode()V

    #@27
    .line 1278
    const/4 v1, 0x1

    #@28
    .line 1281
    :goto_28
    return v1

    #@29
    :cond_29
    const/4 v1, 0x0

    #@2a
    goto :goto_28
.end method

.method handleTimerInEmergencyCallbackMode(I)V
    .registers 7
    .parameter "action"

    #@0
    .prologue
    .line 2215
    packed-switch p1, :pswitch_data_40

    #@3
    .line 2227
    const-string v2, "GSM"

    #@5
    new-instance v3, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v4, "handleTimerInEmergencyCallbackMode, unsupported action "

    #@c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v3

    #@10
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v3

    #@18
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 2229
    :goto_1b
    return-void

    #@1c
    .line 2217
    :pswitch_1c
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mExitEcmRunnable:Ljava/lang/Runnable;

    #@1e
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->removeCallbacks(Ljava/lang/Runnable;)V

    #@21
    .line 2218
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mEcmTimerResetRegistrants:Landroid/os/RegistrantList;

    #@23
    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    #@25
    invoke-virtual {v2, v3}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@28
    goto :goto_1b

    #@29
    .line 2221
    :pswitch_29
    const-string v2, "ro.cdma.ecmexittimer"

    #@2b
    const-wide/32 v3, 0x493e0

    #@2e
    invoke-static {v2, v3, v4}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    #@31
    move-result-wide v0

    #@32
    .line 2223
    .local v0, delayInMillis:J
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mExitEcmRunnable:Ljava/lang/Runnable;

    #@34
    invoke-virtual {p0, v2, v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->postDelayed(Ljava/lang/Runnable;J)Z

    #@37
    .line 2224
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mEcmTimerResetRegistrants:Landroid/os/RegistrantList;

    #@39
    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    #@3b
    invoke-virtual {v2, v3}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@3e
    goto :goto_1b

    #@3f
    .line 2215
    nop

    #@40
    :pswitch_data_40
    .packed-switch 0x0
        :pswitch_29
        :pswitch_1c
    .end packed-switch
.end method

.method public hangupGsmCall()V
    .registers 1

    #@0
    .prologue
    .line 3445
    return-void
.end method

.method public hasIsim()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 3192
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@3
    if-eqz v1, :cond_e

    #@5
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@7
    const/4 v2, 0x3

    #@8
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/uicc/UiccController;->getUiccCardApplication(I)Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@b
    move-result-object v1

    #@c
    if-nez v1, :cond_f

    #@e
    :cond_e
    :goto_e
    return v0

    #@f
    :cond_f
    const/4 v0, 0x1

    #@10
    goto :goto_e
.end method

.method protected initSubscriptionSpecifics()V
    .registers 2

    #@0
    .prologue
    .line 508
    new-instance v0, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@2
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;-><init>(Lcom/android/internal/telephony/gsm/GSMPhone;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSST:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@7
    .line 509
    new-instance v0, Lcom/android/internal/telephony/gsm/GsmDataConnectionTracker;

    #@9
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/gsm/GsmDataConnectionTracker;-><init>(Lcom/android/internal/telephony/PhoneBase;)V

    #@c
    iput-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@e
    .line 510
    return-void
.end method

.method protected isCfEnable(I)Z
    .registers 4
    .parameter "action"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 1572
    if-eq p1, v0, :cond_6

    #@3
    const/4 v1, 0x3

    #@4
    if-ne p1, v1, :cond_7

    #@6
    :cond_6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public isCspPlmnEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 2844
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@2
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/uicc/IccRecords;

    #@8
    .line 2845
    .local v0, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v0, :cond_f

    #@a
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/IccRecords;->isCspPlmnEnabled()Z

    #@d
    move-result v1

    #@e
    :goto_e
    return v1

    #@f
    :cond_f
    const/4 v1, 0x0

    #@10
    goto :goto_e
.end method

.method isInCall()Z
    .registers 5

    #@0
    .prologue
    .line 1119
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getForegroundCall()Lcom/android/internal/telephony/gsm/GsmCall;

    #@3
    move-result-object v3

    #@4
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/GsmCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@7
    move-result-object v1

    #@8
    .line 1120
    .local v1, foregroundCallState:Lcom/android/internal/telephony/Call$State;
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getBackgroundCall()Lcom/android/internal/telephony/gsm/GsmCall;

    #@b
    move-result-object v3

    #@c
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/GsmCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@f
    move-result-object v0

    #@10
    .line 1121
    .local v0, backgroundCallState:Lcom/android/internal/telephony/Call$State;
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getRingingCall()Lcom/android/internal/telephony/gsm/GsmCall;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3}, Lcom/android/internal/telephony/gsm/GsmCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@17
    move-result-object v2

    #@18
    .line 1123
    .local v2, ringingCallState:Lcom/android/internal/telephony/Call$State;
    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@1b
    move-result v3

    #@1c
    if-nez v3, :cond_2a

    #@1e
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@21
    move-result v3

    #@22
    if-nez v3, :cond_2a

    #@24
    invoke-virtual {v2}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@27
    move-result v3

    #@28
    if-eqz v3, :cond_2c

    #@2a
    :cond_2a
    const/4 v3, 0x1

    #@2b
    :goto_2b
    return v3

    #@2c
    :cond_2c
    const/4 v3, 0x0

    #@2d
    goto :goto_2b
.end method

.method public isInEcm()Z
    .registers 2

    #@0
    .prologue
    .line 2095
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mIsPhoneInEcmState:Z

    #@2
    return v0
.end method

.method public isInEcmExitDelay()Z
    .registers 2

    #@0
    .prologue
    .line 2100
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mIsPhoneInEcmExitDelayState:Z

    #@2
    return v0
.end method

.method public isManualNetSelAllowed()Z
    .registers 6

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 2850
    sget v0, Lcom/android/internal/telephony/Phone;->PREFERRED_NT_MODE:I

    #@3
    .line 2852
    .local v0, nwMode:I
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@5
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@8
    move-result-object v2

    #@9
    const-string v3, "preferred_network_mode"

    #@b
    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@e
    move-result v0

    #@f
    .line 2855
    const-string v2, "GSM"

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "isManualNetSelAllowed in mode = "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 2860
    const-string v2, "ro.config.multimode_cdma"

    #@29
    invoke-static {v2, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@2c
    move-result v2

    #@2d
    if-eqz v2, :cond_4f

    #@2f
    const/16 v2, 0xa

    #@31
    if-eq v0, v2, :cond_36

    #@33
    const/4 v2, 0x7

    #@34
    if-ne v0, v2, :cond_4f

    #@36
    .line 2863
    :cond_36
    const-string v2, "GSM"

    #@38
    new-instance v3, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v4, "Manual selection not supported in mode = "

    #@3f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v3

    #@47
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v3

    #@4b
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 2874
    :goto_4e
    return v1

    #@4f
    :cond_4f
    const/4 v1, 0x1

    #@50
    goto :goto_4e
.end method

.method protected log(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 2913
    const-string v0, "GSM"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[GSMPhone] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 2914
    return-void
.end method

.method public makeEngineeringModeInfo()V
    .registers 6

    #@0
    .prologue
    .line 2971
    const/4 v1, 0x0

    #@1
    .local v1, initi:I
    const/4 v0, 0x0

    #@2
    .line 2972
    .local v0, TokenIndex:I
    new-instance v2, Ljava/util/StringTokenizer;

    #@4
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mEmodeModemInfo:Ljava/lang/String;

    #@6
    const-string v4, "!"

    #@8
    invoke-direct {v2, v3, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 2974
    .local v2, st:Ljava/util/StringTokenizer;
    :goto_b
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_1c

    #@11
    .line 2975
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mEmodeInfoforAndroid:[Ljava/lang/String;

    #@13
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@16
    move-result-object v4

    #@17
    aput-object v4, v3, v0

    #@19
    .line 2976
    add-int/lit8 v0, v0, 0x1

    #@1b
    goto :goto_b

    #@1c
    .line 2979
    :cond_1c
    move v1, v0

    #@1d
    :goto_1d
    const/16 v3, 0x10

    #@1f
    if-ge v1, v3, :cond_2a

    #@21
    .line 2980
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mEmodeInfoforAndroid:[Ljava/lang/String;

    #@23
    const-string v4, " "

    #@25
    aput-object v4, v3, v1

    #@27
    .line 2979
    add-int/lit8 v1, v1, 0x1

    #@29
    goto :goto_1d

    #@2a
    .line 2982
    :cond_2a
    return-void
.end method

.method public notifyCallForwardingIndicator()V
    .registers 2

    #@0
    .prologue
    .line 824
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@2
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/PhoneNotifier;->notifyCallForwardingChanged(Lcom/android/internal/telephony/Phone;)V

    #@5
    .line 825
    return-void
.end method

.method notifyDisconnect(Lcom/android/internal/telephony/Connection;)V
    .registers 3
    .parameter "cn"

    #@0
    .prologue
    .line 801
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDisconnectRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@5
    .line 802
    return-void
.end method

.method public notifyFakeCallStateChanged()V
    .registers 1

    #@0
    .prologue
    .line 3448
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->notifyPreciseCallStateChanged()V

    #@3
    .line 3449
    return-void
.end method

.method notifyLocationChanged()V
    .registers 2

    #@0
    .prologue
    .line 819
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@2
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/PhoneNotifier;->notifyCellLocation(Lcom/android/internal/telephony/Phone;)V

    #@5
    .line 820
    return-void
.end method

.method notifyNewRingingConnection(Lcom/android/internal/telephony/Connection;)V
    .registers 2
    .parameter "c"

    #@0
    .prologue
    .line 796
    invoke-super {p0, p1}, Lcom/android/internal/telephony/PhoneBase;->notifyNewRingingConnectionP(Lcom/android/internal/telephony/Connection;)V

    #@3
    .line 797
    return-void
.end method

.method notifyPhoneStateChanged()V
    .registers 2

    #@0
    .prologue
    .line 781
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@2
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/PhoneNotifier;->notifyPhoneState(Lcom/android/internal/telephony/Phone;)V

    #@5
    .line 782
    return-void
.end method

.method notifyPreciseCallStateChanged()V
    .registers 1

    #@0
    .prologue
    .line 790
    invoke-super {p0}, Lcom/android/internal/telephony/PhoneBase;->notifyPreciseCallStateChangedP()V

    #@3
    .line 791
    return-void
.end method

.method notifyServiceStateChanged(Landroid/telephony/ServiceState;)V
    .registers 2
    .parameter "ss"

    #@0
    .prologue
    .line 814
    invoke-super {p0, p1}, Lcom/android/internal/telephony/PhoneBase;->notifyServiceStateChangedP(Landroid/telephony/ServiceState;)V

    #@3
    .line 815
    return-void
.end method

.method notifySuppServiceFailed(Lcom/android/internal/telephony/Phone$SuppService;)V
    .registers 3
    .parameter "code"

    #@0
    .prologue
    .line 809
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mSuppServiceFailedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@5
    .line 810
    return-void
.end method

.method notifyUnknownConnection()V
    .registers 2

    #@0
    .prologue
    .line 805
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mUnknownConnectionRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p0}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@5
    .line 806
    return-void
.end method

.method onMMIDone(Lcom/android/internal/telephony/gsm/GsmMmiCode;)V
    .registers 5
    .parameter "mmi"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1895
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mPendingMMIs:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_15

    #@9
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isUssdRequest()Z

    #@c
    move-result v0

    #@d
    if-nez v0, :cond_15

    #@f
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->isSsInfo()Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_1f

    #@15
    .line 1896
    :cond_15
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mMmiCompleteRegistrants:Landroid/os/RegistrantList;

    #@17
    new-instance v1, Landroid/os/AsyncResult;

    #@19
    invoke-direct {v1, v2, p1, v2}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@1c
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@1f
    .line 1899
    :cond_1f
    return-void
.end method

.method protected onUpdateIccAvailability()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 2636
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 2663
    :cond_5
    :goto_5
    return-void

    #@6
    .line 2642
    :cond_6
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->setCardInPhoneBook()V

    #@9
    .line 2644
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getUiccCardApplication()Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@c
    move-result-object v1

    #@d
    .line 2646
    .local v1, newUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccApplication:Ljava/util/concurrent/atomic/AtomicReference;

    #@f
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@15
    .line 2647
    .local v0, app:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    if-eq v0, v1, :cond_5

    #@17
    .line 2648
    if-eqz v0, :cond_33

    #@19
    .line 2649
    const-string v2, "Removing stale icc objects."

    #@1b
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->log(Ljava/lang/String;)V

    #@1e
    .line 2650
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@20
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@23
    move-result-object v2

    #@24
    if-eqz v2, :cond_29

    #@26
    .line 2651
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->unregisterForSimRecordEvents()V

    #@29
    .line 2653
    :cond_29
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@2b
    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    #@2e
    .line 2654
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccApplication:Ljava/util/concurrent/atomic/AtomicReference;

    #@30
    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    #@33
    .line 2656
    :cond_33
    if-eqz v1, :cond_5

    #@35
    .line 2657
    const-string v2, "New Uicc application found"

    #@37
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->log(Ljava/lang/String;)V

    #@3a
    .line 2658
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccApplication:Ljava/util/concurrent/atomic/AtomicReference;

    #@3c
    invoke-virtual {v2, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    #@3f
    .line 2659
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@41
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getIccRecords()Lcom/android/internal/telephony/uicc/IccRecords;

    #@44
    move-result-object v3

    #@45
    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    #@48
    .line 2660
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->registerForSimRecordEvents()V

    #@4b
    goto :goto_5
.end method

.method peekSession()I
    .registers 2

    #@0
    .prologue
    .line 3303
    iget v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mISimSessionId:I

    #@2
    return v0
.end method

.method public processECCNotiRep(Lcom/android/internal/telephony/MmiCode;)Z
    .registers 7
    .parameter "mmicode"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 3029
    invoke-static {}, Lcom/android/internal/telephony/gsm/EccNoti;->getProcessECCNoti()Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_81

    #@8
    .line 3030
    invoke-static {}, Lcom/android/internal/telephony/gsm/EccNoti;->isSENDING_ECN()Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_55

    #@e
    invoke-static {}, Lcom/android/internal/telephony/gsm/EccNoti;->isSENDING_UVR()Z

    #@11
    move-result v1

    #@12
    if-nez v1, :cond_55

    #@14
    .line 3031
    const-string v1, "GSM"

    #@16
    const-string v3, "processECCNotiRep ==> Sending UVR "

    #@18
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 3032
    invoke-static {p1}, Lcom/android/internal/telephony/gsm/EccNoti;->processUNR(Lcom/android/internal/telephony/MmiCode;)Z

    #@1e
    move-result v1

    #@1f
    if-eqz v1, :cond_4a

    #@21
    .line 3034
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccApplication:Ljava/util/concurrent/atomic/AtomicReference;

    #@23
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@26
    move-result-object v1

    #@27
    check-cast v1, Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@29
    invoke-static {p0, v1}, Lcom/android/internal/telephony/gsm/EccNoti;->setMMICode(Lcom/android/internal/telephony/gsm/GSMPhone;Lcom/android/internal/telephony/uicc/UiccCardApplication;)Lcom/android/internal/telephony/gsm/GsmMmiCode;

    #@2c
    move-result-object v0

    #@2d
    .line 3035
    .local v0, mmi:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    if-eqz v0, :cond_46

    #@2f
    .line 3036
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mPendingMMIs:Ljava/util/ArrayList;

    #@31
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@34
    .line 3037
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mMmiRegistrants:Landroid/os/RegistrantList;

    #@36
    new-instance v3, Landroid/os/AsyncResult;

    #@38
    invoke-direct {v3, v4, v0, v4}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@3b
    invoke-virtual {v1, v3}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@3e
    .line 3038
    invoke-static {}, Lcom/android/internal/telephony/gsm/EccNoti;->processUVR()V

    #@41
    .line 3056
    .end local v0           #mmi:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    :cond_41
    :goto_41
    invoke-static {v2}, Lcom/android/internal/telephony/gsm/EccNoti;->setNotCloseCallScreenECCNoti(Z)V

    #@44
    move v1, v2

    #@45
    .line 3062
    :goto_45
    return v1

    #@46
    .line 3041
    .restart local v0       #mmi:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    :cond_46
    invoke-static {}, Lcom/android/internal/telephony/gsm/EccNoti;->CompleteECCNoti()V

    #@49
    goto :goto_41

    #@4a
    .line 3045
    .end local v0           #mmi:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    :cond_4a
    const-string v1, "GSM"

    #@4c
    const-string v3, "processECCNotiRep ==> CannNot Sending UVR "

    #@4e
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    .line 3046
    invoke-static {}, Lcom/android/internal/telephony/gsm/EccNoti;->CompleteECCNoti()V

    #@54
    goto :goto_41

    #@55
    .line 3050
    :cond_55
    invoke-static {}, Lcom/android/internal/telephony/gsm/EccNoti;->isSENDING_UVR()Z

    #@58
    move-result v1

    #@59
    if-eqz v1, :cond_41

    #@5b
    invoke-static {}, Lcom/android/internal/telephony/gsm/EccNoti;->isSENDING_ECN()Z

    #@5e
    move-result v1

    #@5f
    if-nez v1, :cond_41

    #@61
    .line 3051
    const-string v1, "GSM"

    #@63
    new-instance v3, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string v4, "processECCNotiRep ==> After Received Release Compleate of UVR [finish] ==> "

    #@6a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v3

    #@6e
    invoke-interface {p1}, Lcom/android/internal/telephony/MmiCode;->getState()Lcom/android/internal/telephony/MmiCode$State;

    #@71
    move-result-object v4

    #@72
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v3

    #@76
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v3

    #@7a
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7d
    .line 3053
    invoke-static {}, Lcom/android/internal/telephony/gsm/EccNoti;->CompleteECCNoti()V

    #@80
    goto :goto_41

    #@81
    .line 3061
    :cond_81
    const-string v1, "GSM"

    #@83
    const-string v2, "processECCNotiRep ==> Do Not Processing ECC Noti"

    #@85
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@88
    .line 3062
    const/4 v1, 0x0

    #@89
    goto :goto_45
.end method

.method public processECCNotiUSSD(Ljava/lang/String;)V
    .registers 8
    .parameter "dialString"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x0

    #@2
    .line 2993
    invoke-static {}, Lcom/android/internal/telephony/gsm/EccNoti;->getProcessECCNoti()Z

    #@5
    move-result v2

    #@6
    if-eqz v2, :cond_67

    #@8
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumber(Ljava/lang/String;)Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_67

    #@e
    .line 2995
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@10
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@13
    move-result-object v2

    #@14
    invoke-static {v2}, Lcom/android/internal/telephony/gsm/EccNoti;->setResolver(Landroid/content/ContentResolver;)V

    #@17
    .line 2997
    const/4 v1, 0x0

    #@18
    .line 2998
    .local v1, r:Lcom/android/internal/telephony/uicc/IccRecords;
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@1a
    if-eqz v2, :cond_24

    #@1c
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@1e
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@21
    move-result-object v1

    #@22
    .end local v1           #r:Lcom/android/internal/telephony/uicc/IccRecords;
    check-cast v1, Lcom/android/internal/telephony/uicc/IccRecords;

    #@24
    .line 3000
    .restart local v1       #r:Lcom/android/internal/telephony/uicc/IccRecords;
    :cond_24
    if-eqz v1, :cond_5c

    #@26
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/IccRecords;->getOperatorNumeric()Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    invoke-static {v2}, Lcom/android/internal/telephony/gsm/EccNoti;->On(Ljava/lang/String;)Z

    #@2d
    move-result v2

    #@2e
    if-eqz v2, :cond_5c

    #@30
    .line 3001
    const-string v2, "GSM"

    #@32
    const-string v3, "==== Go Ecc Notification ==="

    #@34
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 3003
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccApplication:Ljava/util/concurrent/atomic/AtomicReference;

    #@39
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@3c
    move-result-object v2

    #@3d
    check-cast v2, Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@3f
    invoke-static {p0, v2}, Lcom/android/internal/telephony/gsm/EccNoti;->setMMICode(Lcom/android/internal/telephony/gsm/GSMPhone;Lcom/android/internal/telephony/uicc/UiccCardApplication;)Lcom/android/internal/telephony/gsm/GsmMmiCode;

    #@42
    move-result-object v0

    #@43
    .line 3004
    .local v0, mmi:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    if-eqz v0, :cond_58

    #@45
    .line 3005
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mPendingMMIs:Ljava/util/ArrayList;

    #@47
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4a
    .line 3006
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mMmiRegistrants:Landroid/os/RegistrantList;

    #@4c
    new-instance v3, Landroid/os/AsyncResult;

    #@4e
    invoke-direct {v3, v5, v0, v5}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@51
    invoke-virtual {v2, v3}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@54
    .line 3007
    invoke-static {}, Lcom/android/internal/telephony/gsm/EccNoti;->processECN()V

    #@57
    .line 3020
    .end local v0           #mmi:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    .end local v1           #r:Lcom/android/internal/telephony/uicc/IccRecords;
    :goto_57
    return-void

    #@58
    .line 3009
    .restart local v0       #mmi:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    .restart local v1       #r:Lcom/android/internal/telephony/uicc/IccRecords;
    :cond_58
    invoke-static {v4}, Lcom/android/internal/telephony/gsm/EccNoti;->setProcessECCNoti(Z)V

    #@5b
    goto :goto_57

    #@5c
    .line 3013
    .end local v0           #mmi:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    :cond_5c
    invoke-static {v4}, Lcom/android/internal/telephony/gsm/EccNoti;->setProcessECCNoti(Z)V

    #@5f
    .line 3014
    const-string v2, "GSM"

    #@61
    const-string v3, "Ecc Noti Cannot be Sent"

    #@63
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    goto :goto_57

    #@67
    .line 3018
    .end local v1           #r:Lcom/android/internal/telephony/uicc/IccRecords;
    :cond_67
    const-string v2, "GSM"

    #@69
    new-instance v3, Ljava/lang/StringBuilder;

    #@6b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@6e
    const-string v4, "Already Sent Ecc Noti -> processing : "

    #@70
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v3

    #@74
    invoke-static {}, Lcom/android/internal/telephony/gsm/EccNoti;->getProcessECCNoti()Z

    #@77
    move-result v4

    #@78
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v3

    #@7c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f
    move-result-object v3

    #@80
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@83
    goto :goto_57
.end method

.method public registerForEcmTimerReset(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 2238
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mEcmTimerResetRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 2239
    return-void
.end method

.method public registerForSimRecordsLoaded(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 849
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mSimRecordsLoadedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 850
    return-void
.end method

.method public registerForSuppServiceNotification(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 7
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 838
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSsnRegistrants:Landroid/os/RegistrantList;

    #@3
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@6
    .line 839
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSsnRegistrants:Landroid/os/RegistrantList;

    #@8
    invoke-virtual {v0}, Landroid/os/RegistrantList;->size()I

    #@b
    move-result v0

    #@c
    if-ne v0, v2, :cond_14

    #@e
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@10
    const/4 v1, 0x0

    #@11
    invoke-interface {v0, v2, v1}, Lcom/android/internal/telephony/CommandsInterface;->setSuppServiceNotifications(ZLandroid/os/Message;)V

    #@14
    .line 840
    :cond_14
    return-void
.end method

.method public rejectCall()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 864
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->rejectCall()V

    #@5
    .line 865
    return-void
.end method

.method public removeReferences()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 560
    const-string v0, "GSM"

    #@3
    const-string v1, "removeReferences"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 561
    iput-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mSimulatedRadioControl:Lcom/android/internal/telephony/test/SimulatedRadioControl;

    #@a
    .line 562
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSimPhoneBookIntManager:Lcom/android/internal/telephony/gsm/SimPhoneBookInterfaceManager;

    #@c
    .line 563
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSubInfo:Lcom/android/internal/telephony/PhoneSubInfo;

    #@e
    .line 564
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@10
    .line 565
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSST:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@12
    .line 567
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mExitEcmRunnable:Ljava/lang/Runnable;

    #@14
    .line 569
    invoke-super {p0}, Lcom/android/internal/telephony/PhoneBase;->removeReferences()V

    #@17
    .line 572
    const-string v0, "vzw_gfit"

    #@19
    invoke-static {v2, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1c
    move-result v0

    #@1d
    if-eqz v0, :cond_21

    #@1f
    .line 573
    iput-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->gfUtils:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@21
    .line 576
    :cond_21
    return-void
.end method

.method public saveClirSetting(I)V
    .registers 6
    .parameter "commandInterfaceCLIRMode"

    #@0
    .prologue
    .line 2755
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@3
    move-result-object v2

    #@4
    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@7
    move-result-object v1

    #@8
    .line 2756
    .local v1, sp:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@b
    move-result-object v0

    #@c
    .line 2757
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "clir_key"

    #@e
    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    #@11
    .line 2760
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    #@14
    move-result v2

    #@15
    if-nez v2, :cond_1e

    #@17
    .line 2761
    const-string v2, "GSM"

    #@19
    const-string v3, "failed to commit CLIR preference"

    #@1b
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 2763
    :cond_1e
    return-void
.end method

.method public selectNetworkManually(Lcom/android/internal/telephony/OperatorInfo;Landroid/os/Message;)V
    .registers 10
    .parameter "network"
    .parameter "response"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 1753
    new-instance v2, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;

    #@3
    invoke-direct {v2, v6}, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;-><init>(Lcom/android/internal/telephony/gsm/GSMPhone$1;)V

    #@6
    .line 1754
    .local v2, nsm:Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;
    iput-object p2, v2, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->message:Landroid/os/Message;

    #@8
    .line 1755
    invoke-virtual {p1}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorNumeric()Ljava/lang/String;

    #@b
    move-result-object v4

    #@c
    iput-object v4, v2, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->operatorNumeric:Ljava/lang/String;

    #@e
    .line 1756
    invoke-virtual {p1}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorAlphaLong()Ljava/lang/String;

    #@11
    move-result-object v4

    #@12
    iput-object v4, v2, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->operatorAlphaLong:Ljava/lang/String;

    #@14
    .line 1758
    invoke-virtual {p1}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorAlphaShort()Ljava/lang/String;

    #@17
    move-result-object v4

    #@18
    iput-object v4, v2, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->operatorAlphaShort:Ljava/lang/String;

    #@1a
    .line 1761
    invoke-virtual {p1}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorRAT()Ljava/lang/String;

    #@1d
    move-result-object v4

    #@1e
    iput-object v4, v2, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->operatorRat:Ljava/lang/String;

    #@20
    .line 1765
    const-string v4, "SHOW_NETWORK_NAME_WHEN_MANUAL_NETWORK_SETTING_FAIL"

    #@22
    invoke-static {v6, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@25
    move-result v4

    #@26
    if-eqz v4, :cond_76

    #@28
    .line 1769
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@2b
    move-result-object v4

    #@2c
    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@2f
    move-result-object v3

    #@30
    .line 1770
    .local v3, sp:Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@33
    move-result-object v0

    #@34
    .line 1771
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v4, "network_selection_key"

    #@36
    iget-object v5, v2, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->operatorNumeric:Ljava/lang/String;

    #@38
    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@3b
    .line 1774
    const-string v4, "SAVE_NETWORK_OPERATOR_SHORT_NAME"

    #@3d
    invoke-static {v6, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@40
    move-result v4

    #@41
    if-eqz v4, :cond_94

    #@43
    .line 1775
    const-string v4, "network_selection_name_key"

    #@45
    iget-object v5, v2, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->operatorAlphaShort:Ljava/lang/String;

    #@47
    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@4a
    .line 1776
    const-string v4, "GSM"

    #@4c
    new-instance v5, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v6, "Writing NETWORK_SELECTION_NAME_KEY "

    #@53
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v5

    #@57
    iget-object v6, v2, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->operatorAlphaShort:Ljava/lang/String;

    #@59
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v5

    #@5d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v5

    #@61
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    .line 1777
    iget-object v4, v2, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->operatorAlphaShort:Ljava/lang/String;

    #@66
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@69
    move-result v4

    #@6a
    if-eqz v4, :cond_73

    #@6c
    .line 1778
    const-string v4, "network_selection_name_key"

    #@6e
    iget-object v5, v2, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->operatorAlphaLong:Ljava/lang/String;

    #@70
    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@73
    .line 1789
    :cond_73
    :goto_73
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    #@76
    .line 1794
    .end local v0           #editor:Landroid/content/SharedPreferences$Editor;
    .end local v3           #sp:Landroid/content/SharedPreferences;
    :cond_76
    const/16 v4, 0x10

    #@78
    invoke-virtual {p0, v4, v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@7b
    move-result-object v1

    #@7c
    .line 1797
    .local v1, msg:Landroid/os/Message;
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@7e
    const-string v5, "MANUAL_SELECTION_WITH_RAT"

    #@80
    invoke-static {v4, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@83
    move-result v4

    #@84
    if-eqz v4, :cond_c5

    #@86
    .line 1798
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@88
    invoke-virtual {p1}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorNumeric()Ljava/lang/String;

    #@8b
    move-result-object v5

    #@8c
    invoke-virtual {p1}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorRAT()Ljava/lang/String;

    #@8f
    move-result-object v6

    #@90
    invoke-interface {v4, v5, v6, v1}, Lcom/android/internal/telephony/CommandsInterface;->setNetworkSelectionModeManual(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@93
    .line 1805
    :goto_93
    return-void

    #@94
    .line 1781
    .end local v1           #msg:Landroid/os/Message;
    .restart local v0       #editor:Landroid/content/SharedPreferences$Editor;
    .restart local v3       #sp:Landroid/content/SharedPreferences;
    :cond_94
    const-string v4, "network_selection_name_key"

    #@96
    iget-object v5, v2, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->operatorAlphaLong:Ljava/lang/String;

    #@98
    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@9b
    .line 1782
    const-string v4, "GSM"

    #@9d
    new-instance v5, Ljava/lang/StringBuilder;

    #@9f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@a2
    const-string v6, "Writing NETWORK_SELECTION_NAME_KEY "

    #@a4
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v5

    #@a8
    iget-object v6, v2, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->operatorAlphaLong:Ljava/lang/String;

    #@aa
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v5

    #@ae
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b1
    move-result-object v5

    #@b2
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b5
    .line 1784
    iget-object v4, v2, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->operatorAlphaLong:Ljava/lang/String;

    #@b7
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@ba
    move-result v4

    #@bb
    if-eqz v4, :cond_73

    #@bd
    .line 1785
    const-string v4, "network_selection_name_key"

    #@bf
    iget-object v5, v2, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->operatorAlphaShort:Ljava/lang/String;

    #@c1
    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@c4
    goto :goto_73

    #@c5
    .line 1803
    .end local v0           #editor:Landroid/content/SharedPreferences$Editor;
    .end local v3           #sp:Landroid/content/SharedPreferences;
    .restart local v1       #msg:Landroid/os/Message;
    :cond_c5
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@c7
    invoke-virtual {p1}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorNumeric()Ljava/lang/String;

    #@ca
    move-result-object v5

    #@cb
    invoke-interface {v4, v5, v1}, Lcom/android/internal/telephony/CommandsInterface;->setNetworkSelectionModeManual(Ljava/lang/String;Landroid/os/Message;)V

    #@ce
    goto :goto_93
.end method

.method public selectPreviousNetworkManually(Lcom/android/internal/telephony/OperatorInfo;Landroid/os/Message;)V
    .registers 8
    .parameter "network"
    .parameter "response"

    #@0
    .prologue
    .line 1822
    new-instance v1, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;

    #@2
    const/4 v2, 0x0

    #@3
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;-><init>(Lcom/android/internal/telephony/gsm/GSMPhone$1;)V

    #@6
    .line 1823
    .local v1, nsm:Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;
    iput-object p2, v1, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->message:Landroid/os/Message;

    #@8
    .line 1824
    invoke-virtual {p1}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorNumeric()Ljava/lang/String;

    #@b
    move-result-object v2

    #@c
    iput-object v2, v1, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->operatorNumeric:Ljava/lang/String;

    #@e
    .line 1827
    const/16 v2, 0x3d

    #@10
    invoke-virtual {p0, v2, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@13
    move-result-object v0

    #@14
    .line 1829
    .local v0, msg:Landroid/os/Message;
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@16
    invoke-virtual {p1}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorNumeric()Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {p1}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorRAT()Ljava/lang/String;

    #@1d
    move-result-object v4

    #@1e
    invoke-interface {v2, v3, v4, v0}, Lcom/android/internal/telephony/CommandsInterface;->setPreviousNetworkSelectionModeManual(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@21
    .line 1830
    return-void
.end method

.method public sendBurstDtmf(Ljava/lang/String;)V
    .registers 4
    .parameter "dtmfString"

    #@0
    .prologue
    .line 1320
    const-string v0, "GSM"

    #@2
    const-string v1, "[GSMPhone] sendBurstDtmf() is a CDMA method"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1321
    return-void
.end method

.method public sendDtmf(C)V
    .registers 5
    .parameter "c"

    #@0
    .prologue
    .line 1293
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->is12Key(C)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_25

    #@6
    .line 1294
    const-string v0, "GSM"

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "sendDtmf called with invalid character \'"

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, "\'"

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 1301
    :cond_24
    :goto_24
    return-void

    #@25
    .line 1297
    :cond_25
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@27
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@29
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$State;->OFFHOOK:Lcom/android/internal/telephony/PhoneConstants$State;

    #@2b
    if-ne v0, v1, :cond_24

    #@2d
    .line 1298
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2f
    const/4 v1, 0x0

    #@30
    invoke-interface {v0, p1, v1}, Lcom/android/internal/telephony/CommandsInterface;->sendDtmf(CLandroid/os/Message;)V

    #@33
    goto :goto_24
.end method

.method sendEmergencyCallbackModeChange()V
    .registers 4

    #@0
    .prologue
    .line 2106
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.intent.action.EMERGENCY_CALLBACK_MODE_CHANGED"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 2107
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "phoneinECMState"

    #@9
    iget-boolean v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mIsPhoneInEcmState:Z

    #@b
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@e
    .line 2108
    const/4 v1, 0x0

    #@f
    const/4 v2, -0x1

    #@10
    invoke-static {v0, v1, v2}, Landroid/app/ActivityManagerNative;->broadcastStickyIntent(Landroid/content/Intent;Ljava/lang/String;I)V

    #@13
    .line 2109
    const-string v1, "GSM"

    #@15
    const-string v2, "sendEmergencyCallbackModeChange"

    #@17
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 2110
    return-void
.end method

.method public sendUssdResponse(Ljava/lang/String;)V
    .registers 6
    .parameter "ussdMessge"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1285
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccApplication:Ljava/util/concurrent/atomic/AtomicReference;

    #@3
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@6
    move-result-object v1

    #@7
    check-cast v1, Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@9
    invoke-static {p1, p0, v1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->newFromUssdUserInput(Ljava/lang/String;Lcom/android/internal/telephony/gsm/GSMPhone;Lcom/android/internal/telephony/uicc/UiccCardApplication;)Lcom/android/internal/telephony/gsm/GsmMmiCode;

    #@c
    move-result-object v0

    #@d
    .line 1286
    .local v0, mmi:Lcom/android/internal/telephony/gsm/GsmMmiCode;
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mPendingMMIs:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@12
    .line 1287
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mMmiRegistrants:Landroid/os/RegistrantList;

    #@14
    new-instance v2, Landroid/os/AsyncResult;

    #@16
    invoke-direct {v2, v3, v0, v3}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@19
    invoke-virtual {v1, v2}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@1c
    .line 1288
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/gsm/GsmMmiCode;->sendUssd(Ljava/lang/String;)V

    #@1f
    .line 1289
    return-void
.end method

.method public setCallBarringOption(ILjava/lang/String;ILjava/lang/String;Landroid/os/Message;)V
    .registers 12
    .parameter "commandInterfaceCBAction"
    .parameter "commandInterfaceCBReason"
    .parameter "serviceClass"
    .parameter "password"
    .parameter "onComplete"

    #@0
    .prologue
    .line 1675
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->isCbEnable(I)Z

    #@5
    move-result v2

    #@6
    move-object v1, p2

    #@7
    move-object v3, p4

    #@8
    move v4, p3

    #@9
    move-object v5, p5

    #@a
    invoke-interface/range {v0 .. v5}, Lcom/android/internal/telephony/CommandsInterface;->setFacilityLock(Ljava/lang/String;ZLjava/lang/String;ILandroid/os/Message;)V

    #@d
    .line 1676
    return-void
.end method

.method public setCallBarringPass(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 6
    .parameter "oldPwd"
    .parameter "newPwd"
    .parameter "onComplete"

    #@0
    .prologue
    .line 1683
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    const-string v1, "AB"

    #@4
    invoke-interface {v0, v1, p1, p2, p3}, Lcom/android/internal/telephony/CommandsInterface;->changeBarringPassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@7
    .line 1684
    return-void
.end method

.method public setCallForwardingOption(IILjava/lang/String;IILandroid/os/Message;)V
    .registers 14
    .parameter "commandInterfaceCFAction"
    .parameter "commandInterfaceCFReason"
    .parameter "dialingNumber"
    .parameter "serviceClass"
    .parameter "timerSeconds"
    .parameter "onComplete"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1620
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->isValidCommandInterfaceCFAction(I)Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_26

    #@7
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/gsm/GSMPhone;->isValidCommandInterfaceCFReason(I)Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_26

    #@d
    .line 1624
    if-nez p2, :cond_29

    #@f
    .line 1625
    const/16 v2, 0xc

    #@11
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->isCfEnable(I)Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_27

    #@17
    const/4 v0, 0x1

    #@18
    :goto_18
    invoke-virtual {p0, v2, v0, v1, p6}, Lcom/android/internal/telephony/gsm/GSMPhone;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@1b
    move-result-object v6

    #@1c
    .line 1630
    .local v6, resp:Landroid/os/Message;
    :goto_1c
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1e
    move v1, p1

    #@1f
    move v2, p2

    #@20
    move v3, p4

    #@21
    move-object v4, p3

    #@22
    move v5, p5

    #@23
    invoke-interface/range {v0 .. v6}, Lcom/android/internal/telephony/CommandsInterface;->setCallForward(IIILjava/lang/String;ILandroid/os/Message;)V

    #@26
    .line 1637
    .end local v6           #resp:Landroid/os/Message;
    :cond_26
    return-void

    #@27
    :cond_27
    move v0, v1

    #@28
    .line 1625
    goto :goto_18

    #@29
    .line 1628
    :cond_29
    move-object v6, p6

    #@2a
    .restart local v6       #resp:Landroid/os/Message;
    goto :goto_1c
.end method

.method public setCallForwardingOption(IILjava/lang/String;ILandroid/os/Message;)V
    .registers 13
    .parameter "commandInterfaceCFAction"
    .parameter "commandInterfaceCFReason"
    .parameter "dialingNumber"
    .parameter "timerSeconds"
    .parameter "onComplete"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1593
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->isValidCommandInterfaceCFAction(I)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_28

    #@8
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/gsm/GSMPhone;->isValidCommandInterfaceCFReason(I)Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_28

    #@e
    .line 1597
    if-nez p2, :cond_2b

    #@10
    .line 1598
    iput-object p3, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSetCfNumber:Ljava/lang/String;

    #@12
    .line 1599
    const/16 v2, 0xc

    #@14
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/gsm/GSMPhone;->isCfEnable(I)Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_29

    #@1a
    move v0, v3

    #@1b
    :goto_1b
    invoke-virtual {p0, v2, v0, v1, p5}, Lcom/android/internal/telephony/gsm/GSMPhone;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@1e
    move-result-object v6

    #@1f
    .line 1604
    .local v6, resp:Landroid/os/Message;
    :goto_1f
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@21
    move v1, p1

    #@22
    move v2, p2

    #@23
    move-object v4, p3

    #@24
    move v5, p4

    #@25
    invoke-interface/range {v0 .. v6}, Lcom/android/internal/telephony/CommandsInterface;->setCallForward(IIILjava/lang/String;ILandroid/os/Message;)V

    #@28
    .line 1611
    .end local v6           #resp:Landroid/os/Message;
    :cond_28
    return-void

    #@29
    :cond_29
    move v0, v1

    #@2a
    .line 1599
    goto :goto_1b

    #@2b
    .line 1602
    :cond_2b
    move-object v6, p5

    #@2c
    .restart local v6       #resp:Landroid/os/Message;
    goto :goto_1f
.end method

.method protected setCallForwardingPreference(Z)V
    .registers 6
    .parameter "enabled"

    #@0
    .prologue
    .line 1906
    const-string v2, "GSM"

    #@2
    const-string v3, "Set callforwarding info to perferences"

    #@4
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1907
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@9
    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@c
    move-result-object v1

    #@d
    .line 1908
    .local v1, sp:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@10
    move-result-object v0

    #@11
    .line 1909
    .local v0, edit:Landroid/content/SharedPreferences$Editor;
    const-string v2, "cf_enabled_key"

    #@13
    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    #@16
    .line 1910
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    #@19
    .line 1913
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getSubscriberId()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->setVmSimImsi(Ljava/lang/String;)V

    #@20
    .line 1914
    return-void
.end method

.method public setCallWaiting(ZLandroid/os/Message;)V
    .registers 5
    .parameter "enable"
    .parameter "onComplete"

    #@0
    .prologue
    .line 1656
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-interface {v0, p1, v1, p2}, Lcom/android/internal/telephony/CommandsInterface;->setCallWaiting(ZILandroid/os/Message;)V

    #@6
    .line 1657
    return-void
.end method

.method protected setCardInPhoneBook()V
    .registers 3

    #@0
    .prologue
    .line 2627
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 2632
    :goto_4
    return-void

    #@5
    .line 2631
    :cond_5
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSimPhoneBookIntManager:Lcom/android/internal/telephony/gsm/SimPhoneBookInterfaceManager;

    #@7
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@9
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccController;->getUiccCard()Lcom/android/internal/telephony/uicc/UiccCard;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gsm/SimPhoneBookInterfaceManager;->setIccCard(Lcom/android/internal/telephony/uicc/UiccCard;)V

    #@10
    goto :goto_4
.end method

.method public setCdmaFactoryReset(Landroid/os/Message;)V
    .registers 4
    .parameter "onComplete"

    #@0
    .prologue
    .line 3068
    const-string v0, "GSM"

    #@2
    const-string v1, "[GSMPhone] setCdmaFactoryReset() is a CDMA method"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 3070
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@9
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->setCdmaFactoryReset(Landroid/os/Message;)V

    #@c
    .line 3071
    return-void
.end method

.method public setCellBroadcastSmsConfig([ILandroid/os/Message;)V
    .registers 5
    .parameter "configValuesArray"
    .parameter "response"

    #@0
    .prologue
    .line 2839
    const-string v0, "GSM"

    #@2
    const-string v1, "[GSMPhone] setCellBroadcastSmsConfig() is obsolete; use SmsManager"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2840
    invoke-virtual {p2}, Landroid/os/Message;->sendToTarget()V

    #@a
    .line 2841
    return-void
.end method

.method public setDataRoamingEnabled(Z)V
    .registers 3
    .parameter "enable"

    #@0
    .prologue
    .line 1880
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->setDataOnRoamingEnabled(Z)V

    #@5
    .line 1881
    return-void
.end method

.method public setGbaBootstrappingParams([BLjava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 9
    .parameter "rand"
    .parameter "btid"
    .parameter "keyLifetime"
    .parameter "onComplete"

    #@0
    .prologue
    .line 3405
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getIsimRecords()Lcom/android/internal/telephony/uicc/IsimRecords;

    #@3
    move-result-object v1

    #@4
    if-eqz v1, :cond_6c

    #@6
    .line 3407
    if-eqz p1, :cond_f

    #@8
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getIsimRecords()Lcom/android/internal/telephony/uicc/IsimRecords;

    #@b
    move-result-object v1

    #@c
    invoke-interface {v1, p1}, Lcom/android/internal/telephony/uicc/IsimRecords;->setIsimRand([B)V

    #@f
    .line 3408
    :cond_f
    if-eqz p2, :cond_18

    #@11
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getIsimRecords()Lcom/android/internal/telephony/uicc/IsimRecords;

    #@14
    move-result-object v1

    #@15
    invoke-interface {v1, p2}, Lcom/android/internal/telephony/uicc/IsimRecords;->setIsimBtid(Ljava/lang/String;)V

    #@18
    .line 3409
    :cond_18
    if-eqz p3, :cond_21

    #@1a
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getIsimRecords()Lcom/android/internal/telephony/uicc/IsimRecords;

    #@1d
    move-result-object v1

    #@1e
    invoke-interface {v1, p3}, Lcom/android/internal/telephony/uicc/IsimRecords;->setIsimKeyLifetime(Ljava/lang/String;)V

    #@21
    .line 3417
    :cond_21
    :goto_21
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    #@23
    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@26
    .line 3420
    .local v0, os:Ljava/io/ByteArrayOutputStream;
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSubInfo:Lcom/android/internal/telephony/PhoneSubInfo;

    #@28
    if-eqz v1, :cond_7c

    #@2a
    .line 3422
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSubInfo:Lcom/android/internal/telephony/PhoneSubInfo;

    #@2c
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneSubInfo;->getBtid()Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    if-eqz v1, :cond_74

    #@32
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSubInfo:Lcom/android/internal/telephony/PhoneSubInfo;

    #@34
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneSubInfo;->getKeyLifetime()Ljava/lang/String;

    #@37
    move-result-object v1

    #@38
    if-eqz v1, :cond_74

    #@3a
    .line 3424
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSubInfo:Lcom/android/internal/telephony/PhoneSubInfo;

    #@3c
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneSubInfo;->getRand()[B

    #@3f
    move-result-object v1

    #@40
    invoke-direct {p0, v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->appendGbaParameter(Ljava/io/ByteArrayOutputStream;[B)V

    #@43
    .line 3425
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSubInfo:Lcom/android/internal/telephony/PhoneSubInfo;

    #@45
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneSubInfo;->getBtid()Ljava/lang/String;

    #@48
    move-result-object v1

    #@49
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    #@4c
    move-result-object v1

    #@4d
    invoke-direct {p0, v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->appendGbaParameter(Ljava/io/ByteArrayOutputStream;[B)V

    #@50
    .line 3426
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSubInfo:Lcom/android/internal/telephony/PhoneSubInfo;

    #@52
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneSubInfo;->getKeyLifetime()Ljava/lang/String;

    #@55
    move-result-object v1

    #@56
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    #@59
    move-result-object v1

    #@5a
    invoke-direct {p0, v0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->appendGbaParameter(Ljava/io/ByteArrayOutputStream;[B)V

    #@5d
    .line 3439
    :goto_5d
    new-instance v1, Lcom/android/internal/telephony/gsm/GSMPhone$PendingWrite;

    #@5f
    const/16 v2, 0x6fd5

    #@61
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@64
    move-result-object v3

    #@65
    invoke-direct {v1, p0, v2, v3, p4}, Lcom/android/internal/telephony/gsm/GSMPhone$PendingWrite;-><init>(Lcom/android/internal/telephony/gsm/GSMPhone;I[BLandroid/os/Message;)V

    #@68
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->ensureISimSession(Lcom/android/internal/telephony/gsm/GSMPhone$PendingOperation;)V

    #@6b
    .line 3440
    return-void

    #@6c
    .line 3413
    .end local v0           #os:Ljava/io/ByteArrayOutputStream;
    :cond_6c
    const-string v1, "GSM"

    #@6e
    const-string v2, "NullPointerException occur - getIsimRecords() == null"

    #@70
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@73
    goto :goto_21

    #@74
    .line 3430
    .restart local v0       #os:Ljava/io/ByteArrayOutputStream;
    :cond_74
    const-string v1, "GSM"

    #@76
    const-string v2, "NullPointerException occur - mSubInfo.getBtid() == null or mSubInfo.getKeyLifetime() == null"

    #@78
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7b
    goto :goto_5d

    #@7c
    .line 3435
    :cond_7c
    const-string v1, "GSM"

    #@7e
    const-string v2, "NullPointerException occur - mSubInfo == null"

    #@80
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@83
    goto :goto_5d
.end method

.method public setLine1Number(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 6
    .parameter "alphaTag"
    .parameter "number"
    .parameter "onComplete"

    #@0
    .prologue
    .line 1526
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@2
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/uicc/IccRecords;

    #@8
    .line 1527
    .local v0, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v0, :cond_d

    #@a
    .line 1528
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/internal/telephony/uicc/IccRecords;->setMsisdnNumber(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@d
    .line 1530
    :cond_d
    return-void
.end method

.method public setMute(Z)V
    .registers 3
    .parameter "muted"

    #@0
    .prologue
    .line 1852
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->setMute(Z)V

    #@5
    .line 1853
    return-void
.end method

.method public setNetworkSelectionModeAutomatic(Landroid/os/Message;)V
    .registers 8
    .parameter "response"

    #@0
    .prologue
    .line 1715
    new-instance v2, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;

    #@2
    const/4 v4, 0x0

    #@3
    invoke-direct {v2, v4}, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;-><init>(Lcom/android/internal/telephony/gsm/GSMPhone$1;)V

    #@6
    .line 1716
    .local v2, nsm:Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;
    iput-object p1, v2, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->message:Landroid/os/Message;

    #@8
    .line 1717
    const-string v4, ""

    #@a
    iput-object v4, v2, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->operatorNumeric:Ljava/lang/String;

    #@c
    .line 1718
    const-string v4, ""

    #@e
    iput-object v4, v2, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->operatorAlphaLong:Ljava/lang/String;

    #@10
    .line 1720
    const-string v4, ""

    #@12
    iput-object v4, v2, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->operatorAlphaShort:Ljava/lang/String;

    #@14
    .line 1724
    const-string v4, ""

    #@16
    iput-object v4, v2, Lcom/android/internal/telephony/gsm/GSMPhone$NetworkSelectMessage;->operatorRat:Ljava/lang/String;

    #@18
    .line 1728
    const-string v4, "KR"

    #@1a
    const-string v5, "LGU"

    #@1c
    invoke-static {v4, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@1f
    move-result v4

    #@20
    if-eqz v4, :cond_49

    #@22
    .line 1729
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@25
    move-result-object v4

    #@26
    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@29
    move-result-object v3

    #@2a
    .line 1730
    .local v3, sp:Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@2d
    move-result-object v0

    #@2e
    .line 1731
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v4, "network_selection_key"

    #@30
    const-string v5, ""

    #@32
    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@35
    .line 1732
    const-string v4, "network_selection_name_key"

    #@37
    const-string v5, ""

    #@39
    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@3c
    .line 1734
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    #@3f
    move-result v4

    #@40
    if-nez v4, :cond_49

    #@42
    .line 1735
    const-string v4, "GSM"

    #@44
    const-string v5, "failed to commit automatic network selection preference"

    #@46
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    .line 1741
    .end local v0           #editor:Landroid/content/SharedPreferences$Editor;
    .end local v3           #sp:Landroid/content/SharedPreferences;
    :cond_49
    const/16 v4, 0x11

    #@4b
    invoke-virtual {p0, v4, v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@4e
    move-result-object v1

    #@4f
    .line 1743
    .local v1, msg:Landroid/os/Message;
    const-string v4, "GSM"

    #@51
    const-string v5, "wrapping and sending message to connect automatically"

    #@53
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 1745
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@58
    invoke-interface {v4, v1}, Lcom/android/internal/telephony/CommandsInterface;->setNetworkSelectionModeAutomatic(Landroid/os/Message;)V

    #@5b
    .line 1746
    return-void
.end method

.method public setOnEcbModeExitResponse(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 2087
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mEcmExitRespRegistrant:Landroid/os/Registrant;

    #@7
    .line 2088
    return-void
.end method

.method public setOnPostDialCharacter(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1848
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mPostDialHandler:Landroid/os/Registrant;

    #@7
    .line 1849
    return-void
.end method

.method public setOutgoingCallerIdDisplay(ILandroid/os/Message;)V
    .registers 6
    .parameter "commandInterfaceCLIRMode"
    .parameter "onComplete"

    #@0
    .prologue
    .line 1645
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    const/16 v1, 0x12

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {p0, v1, p1, v2, p2}, Lcom/android/internal/telephony/gsm/GSMPhone;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@8
    move-result-object v1

    #@9
    invoke-interface {v0, p1, v1}, Lcom/android/internal/telephony/CommandsInterface;->setCLIR(ILandroid/os/Message;)V

    #@c
    .line 1647
    return-void
.end method

.method protected setProperties()V
    .registers 4

    #@0
    .prologue
    .line 439
    const-string v0, "gsm.current.phone-type"

    #@2
    new-instance v1, Ljava/lang/Integer;

    #@4
    const/4 v2, 0x1

    #@5
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@8
    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@f
    .line 441
    return-void
.end method

.method public setRadioPower(Z)V
    .registers 3
    .parameter "power"

    #@0
    .prologue
    .line 1325
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSST:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setRadioPower(Z)V

    #@5
    .line 1326
    return-void
.end method

.method public setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "property"
    .parameter "value"

    #@0
    .prologue
    .line 833
    invoke-super {p0, p1, p2}, Lcom/android/internal/telephony/PhoneBase;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@3
    .line 834
    return-void
.end method

.method protected setVmSimImsi(Ljava/lang/String;)V
    .registers 5
    .parameter "imsi"

    #@0
    .prologue
    .line 1420
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@3
    move-result-object v2

    #@4
    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@7
    move-result-object v1

    #@8
    .line 1421
    .local v1, sp:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@b
    move-result-object v0

    #@c
    .line 1422
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "vm_sim_imsi_key"

    #@e
    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@11
    .line 1423
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    #@14
    .line 1424
    return-void
.end method

.method public setVoiceMailNumber(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 8
    .parameter "alphaTag"
    .parameter "voiceMailNumber"
    .parameter "onComplete"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1537
    iput-object p2, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mVmNumber:Ljava/lang/String;

    #@3
    .line 1538
    const/16 v2, 0x14

    #@5
    invoke-virtual {p0, v2, v3, v3, p3}, Lcom/android/internal/telephony/gsm/GSMPhone;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@8
    move-result-object v1

    #@9
    .line 1539
    .local v1, resp:Landroid/os/Message;
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@b
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Lcom/android/internal/telephony/uicc/IccRecords;

    #@11
    .line 1540
    .local v0, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v0, :cond_18

    #@13
    .line 1541
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mVmNumber:Ljava/lang/String;

    #@15
    invoke-virtual {v0, p1, v2, v1}, Lcom/android/internal/telephony/uicc/IccRecords;->setVoiceMailNumber(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@18
    .line 1543
    :cond_18
    return-void
.end method

.method public startDtmf(C)V
    .registers 5
    .parameter "c"

    #@0
    .prologue
    .line 1305
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->is12Key(C)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_25

    #@6
    .line 1306
    const-string v0, "GSM"

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "startDtmf called with invalid character \'"

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, "\'"

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 1311
    :goto_24
    return-void

    #@25
    .line 1309
    :cond_25
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@27
    const/4 v1, 0x0

    #@28
    invoke-interface {v0, p1, v1}, Lcom/android/internal/telephony/CommandsInterface;->startDtmf(CLandroid/os/Message;)V

    #@2b
    goto :goto_24
.end method

.method public stopDtmf()V
    .registers 3

    #@0
    .prologue
    .line 1315
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->stopDtmf(Landroid/os/Message;)V

    #@6
    .line 1316
    return-void
.end method

.method protected storeVoiceMailNumber(Ljava/lang/String;)V
    .registers 5
    .parameter "number"

    #@0
    .prologue
    .line 1329
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@3
    move-result-object v2

    #@4
    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@7
    move-result-object v1

    #@8
    .line 1330
    .local v1, sp:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@b
    move-result-object v0

    #@c
    .line 1331
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "vm_number_key"

    #@e
    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@11
    .line 1332
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    #@14
    .line 1333
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getSubscriberId()Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->setVmSimImsi(Ljava/lang/String;)V

    #@1b
    .line 1334
    return-void
.end method

.method public switchHoldingAndActive()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 869
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mCT:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->switchWaitingOrHoldingAndActive()V

    #@5
    .line 870
    return-void
.end method

.method protected syncClirSetting()V
    .registers 7

    #@0
    .prologue
    .line 2056
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GSMPhone;->getContext()Landroid/content/Context;

    #@3
    move-result-object v3

    #@4
    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@7
    move-result-object v2

    #@8
    .line 2058
    .local v2, sp:Landroid/content/SharedPreferences;
    const/4 v0, 0x0

    #@9
    .line 2059
    .local v0, clirSetting:I
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@b
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getClirSettingValue(Landroid/content/Context;)Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    .line 2061
    .local v1, clirSettingValue:Ljava/lang/String;
    const-string v3, "-1"

    #@11
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v3

    #@15
    if-nez v3, :cond_3f

    #@17
    .line 2062
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1a
    move-result v0

    #@1b
    .line 2063
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/GSMPhone;->saveClirSetting(I)V

    #@1e
    .line 2078
    :goto_1e
    const-string v3, "GSM"

    #@20
    new-instance v4, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v5, "syncClirSetting :: clirSetting = "

    #@27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v4

    #@33
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 2080
    if-ltz v0, :cond_3e

    #@38
    .line 2081
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@3a
    const/4 v4, 0x0

    #@3b
    invoke-interface {v3, v0, v4}, Lcom/android/internal/telephony/CommandsInterface;->setCLIR(ILandroid/os/Message;)V

    #@3e
    .line 2083
    :cond_3e
    return-void

    #@3f
    .line 2065
    :cond_3f
    const-string v3, "clir_key"

    #@41
    const/4 v4, -0x1

    #@42
    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    #@45
    move-result v0

    #@46
    goto :goto_1e
.end method

.method public unregisterForEcmTimerReset(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 2242
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mEcmTimerResetRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 2243
    return-void
.end method

.method public unregisterForSimRecordsLoaded(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 854
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mSimRecordsLoadedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 855
    return-void
.end method

.method public unregisterForSuppServiceNotification(Landroid/os/Handler;)V
    .registers 5
    .parameter "h"

    #@0
    .prologue
    .line 843
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSsnRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 844
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSsnRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v0}, Landroid/os/RegistrantList;->size()I

    #@a
    move-result v0

    #@b
    if-nez v0, :cond_14

    #@d
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@f
    const/4 v1, 0x0

    #@10
    const/4 v2, 0x0

    #@11
    invoke-interface {v0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->setSuppServiceNotifications(ZLandroid/os/Message;)V

    #@14
    .line 845
    :cond_14
    return-void
.end method

.method public unsetOnEcbModeExitResponse(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 2091
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mEcmExitRespRegistrant:Landroid/os/Registrant;

    #@2
    invoke-virtual {v0}, Landroid/os/Registrant;->clear()V

    #@5
    .line 2092
    return-void
.end method

.method updateCurrentCarrierInProvider()Z
    .registers 7

    #@0
    .prologue
    .line 2682
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@2
    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v2

    #@6
    check-cast v2, Lcom/android/internal/telephony/uicc/IccRecords;

    #@8
    .line 2683
    .local v2, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v2, :cond_33

    #@a
    .line 2685
    :try_start_a
    sget-object v4, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    #@c
    const-string v5, "current"

    #@e
    invoke-static {v4, v5}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@11
    move-result-object v3

    #@12
    .line 2686
    .local v3, uri:Landroid/net/Uri;
    new-instance v1, Landroid/content/ContentValues;

    #@14
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@17
    .line 2687
    .local v1, map:Landroid/content/ContentValues;
    const-string v4, "numeric"

    #@19
    invoke-virtual {v2}, Lcom/android/internal/telephony/uicc/IccRecords;->getOperatorNumeric()Ljava/lang/String;

    #@1c
    move-result-object v5

    #@1d
    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@20
    .line 2688
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@22
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@25
    move-result-object v4

    #@26
    invoke-virtual {v4, v3, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_29
    .catch Landroid/database/SQLException; {:try_start_a .. :try_end_29} :catch_2b

    #@29
    .line 2689
    const/4 v4, 0x1

    #@2a
    .line 2694
    .end local v1           #map:Landroid/content/ContentValues;
    .end local v3           #uri:Landroid/net/Uri;
    :goto_2a
    return v4

    #@2b
    .line 2690
    :catch_2b
    move-exception v0

    #@2c
    .line 2691
    .local v0, e:Landroid/database/SQLException;
    const-string v4, "GSM"

    #@2e
    const-string v5, "Can\'t store current operator"

    #@30
    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@33
    .line 2694
    .end local v0           #e:Landroid/database/SQLException;
    :cond_33
    const/4 v4, 0x0

    #@34
    goto :goto_2a
.end method

.method public updateServiceLocation()V
    .registers 2

    #@0
    .prologue
    .line 1864
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GSMPhone;->mSST:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->enableSingleLocationUpdate()V

    #@5
    .line 1865
    return-void
.end method
