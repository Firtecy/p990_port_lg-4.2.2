.class public Lcom/android/internal/telephony/cat/CatService;
.super Landroid/os/Handler;
.source "CatService.java"

# interfaces
.implements Lcom/android/internal/telephony/cat/AppInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/cat/CatService$1;
    }
.end annotation


# static fields
.field private static final DEV_ID_DISPLAY:I = 0x2

.field private static final DEV_ID_EARPIECE:I = 0x3

.field private static final DEV_ID_KEYPAD:I = 0x1

.field private static final DEV_ID_NETWORK:I = 0x83

.field private static final DEV_ID_TERMINAL:I = 0x82

.field private static final DEV_ID_UICC:I = 0x81

.field private static ENABLE_PRIVACY_LOG:Z = false

.field static final MSG_ID_ALPHA_NOTIFY:I = 0x8

.field protected static final MSG_ID_CALL_SETUP:I = 0x4

.field protected static final MSG_ID_EVENT_NOTIFY:I = 0x3

.field private static final MSG_ID_ICC_CHANGED:I = 0x15

.field protected static final MSG_ID_ICC_RECORDS_LOADED:I = 0x14

.field private static final MSG_ID_ICC_REFRESH:I = 0x1e

.field protected static final MSG_ID_PROACTIVE_COMMAND:I = 0x2

.field static final MSG_ID_REFRESH:I = 0x5

.field static final MSG_ID_RESPONSE:I = 0x6

.field static final MSG_ID_RIL_MSG_DECODED:I = 0xa

.field protected static final MSG_ID_SESSION_END:I = 0x1

.field protected static final MSG_ID_SIM_READY:I = 0x7

.field static final STK_DEFAULT:Ljava/lang/String; = "Default Message"

.field protected static mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

.field protected static mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

.field private static mhandlerThread:Landroid/os/HandlerThread;

.field private static sInstance:Lcom/android/internal/telephony/cat/CatService;

.field protected static final sInstanceLock:Ljava/lang/Object;


# instance fields
.field private mCardState:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

.field protected mCmdIf:Lcom/android/internal/telephony/CommandsInterface;

.field protected mContext:Landroid/content/Context;

.field protected mCurrntCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

.field protected mMenuCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

.field protected mMsgDecoder:Lcom/android/internal/telephony/cat/RilMessageDecoder;

.field protected mStkAppInstalled:Z

.field private mUiccController:Lcom/android/internal/telephony/uicc/UiccController;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 78
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Lcom/android/internal/telephony/cat/CatService;->sInstanceLock:Ljava/lang/Object;

    #@7
    .line 89
    const-string v1, "persist.service.privacy.enable"

    #@9
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    const-string v2, "ATT"

    #@f
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v0

    #@13
    if-nez v0, :cond_21

    #@15
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    const-string v2, "TMO"

    #@1b
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v0

    #@1f
    if-eqz v0, :cond_35

    #@21
    :cond_21
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    const-string v2, "US"

    #@27
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a
    move-result v0

    #@2b
    if-eqz v0, :cond_35

    #@2d
    const/4 v0, 0x0

    #@2e
    :goto_2e
    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@31
    move-result v0

    #@32
    sput-boolean v0, Lcom/android/internal/telephony/cat/CatService;->ENABLE_PRIVACY_LOG:Z

    #@34
    return-void

    #@35
    :cond_35
    const/4 v0, 0x1

    #@36
    goto :goto_2e
.end method

.method protected constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 168
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@4
    .line 83
    iput-object v0, p0, Lcom/android/internal/telephony/cat/CatService;->mCurrntCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    #@6
    .line 84
    iput-object v0, p0, Lcom/android/internal/telephony/cat/CatService;->mMenuCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    #@8
    .line 86
    iput-object v0, p0, Lcom/android/internal/telephony/cat/CatService;->mMsgDecoder:Lcom/android/internal/telephony/cat/RilMessageDecoder;

    #@a
    .line 87
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Lcom/android/internal/telephony/cat/CatService;->mStkAppInstalled:Z

    #@d
    .line 169
    return-void
.end method

.method private constructor <init>(Lcom/android/internal/telephony/CommandsInterface;Lcom/android/internal/telephony/uicc/UiccCardApplication;Lcom/android/internal/telephony/uicc/IccRecords;Landroid/content/Context;Lcom/android/internal/telephony/uicc/IccFileHandler;Lcom/android/internal/telephony/uicc/UiccCard;)V
    .registers 10
    .parameter "ci"
    .parameter "ca"
    .parameter "ir"
    .parameter "context"
    .parameter "fh"
    .parameter "ic"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 124
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@4
    .line 83
    iput-object v2, p0, Lcom/android/internal/telephony/cat/CatService;->mCurrntCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    #@6
    .line 84
    iput-object v2, p0, Lcom/android/internal/telephony/cat/CatService;->mMenuCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    #@8
    .line 86
    iput-object v2, p0, Lcom/android/internal/telephony/cat/CatService;->mMsgDecoder:Lcom/android/internal/telephony/cat/RilMessageDecoder;

    #@a
    .line 87
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Lcom/android/internal/telephony/cat/CatService;->mStkAppInstalled:Z

    #@d
    .line 125
    if-eqz p1, :cond_19

    #@f
    if-eqz p2, :cond_19

    #@11
    if-eqz p3, :cond_19

    #@13
    if-eqz p4, :cond_19

    #@15
    if-eqz p5, :cond_19

    #@17
    if-nez p6, :cond_21

    #@19
    .line 127
    :cond_19
    new-instance v0, Ljava/lang/NullPointerException;

    #@1b
    const-string v1, "Service: Input parameters must not be null"

    #@1d
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@20
    throw v0

    #@21
    .line 130
    :cond_21
    iput-object p1, p0, Lcom/android/internal/telephony/cat/CatService;->mCmdIf:Lcom/android/internal/telephony/CommandsInterface;

    #@23
    .line 131
    iput-object p4, p0, Lcom/android/internal/telephony/cat/CatService;->mContext:Landroid/content/Context;

    #@25
    .line 134
    invoke-static {p0, p5}, Lcom/android/internal/telephony/cat/RilMessageDecoder;->getInstance(Landroid/os/Handler;Lcom/android/internal/telephony/uicc/IccFileHandler;)Lcom/android/internal/telephony/cat/RilMessageDecoder;

    #@28
    move-result-object v0

    #@29
    iput-object v0, p0, Lcom/android/internal/telephony/cat/CatService;->mMsgDecoder:Lcom/android/internal/telephony/cat/RilMessageDecoder;

    #@2b
    .line 137
    iget-object v0, p0, Lcom/android/internal/telephony/cat/CatService;->mCmdIf:Lcom/android/internal/telephony/CommandsInterface;

    #@2d
    const/4 v1, 0x1

    #@2e
    invoke-interface {v0, p0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->setOnCatSessionEnd(Landroid/os/Handler;ILjava/lang/Object;)V

    #@31
    .line 138
    iget-object v0, p0, Lcom/android/internal/telephony/cat/CatService;->mCmdIf:Lcom/android/internal/telephony/CommandsInterface;

    #@33
    const/4 v1, 0x2

    #@34
    invoke-interface {v0, p0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->setOnCatProactiveCmd(Landroid/os/Handler;ILjava/lang/Object;)V

    #@37
    .line 139
    iget-object v0, p0, Lcom/android/internal/telephony/cat/CatService;->mCmdIf:Lcom/android/internal/telephony/CommandsInterface;

    #@39
    const/4 v1, 0x3

    #@3a
    invoke-interface {v0, p0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->setOnCatEvent(Landroid/os/Handler;ILjava/lang/Object;)V

    #@3d
    .line 140
    iget-object v0, p0, Lcom/android/internal/telephony/cat/CatService;->mCmdIf:Lcom/android/internal/telephony/CommandsInterface;

    #@3f
    const/4 v1, 0x4

    #@40
    invoke-interface {v0, p0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->setOnCatCallSetUp(Landroid/os/Handler;ILjava/lang/Object;)V

    #@43
    .line 143
    iget-object v0, p0, Lcom/android/internal/telephony/cat/CatService;->mCmdIf:Lcom/android/internal/telephony/CommandsInterface;

    #@45
    const/16 v1, 0x1e

    #@47
    invoke-interface {v0, p0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->registerForIccRefresh(Landroid/os/Handler;ILjava/lang/Object;)V

    #@4a
    .line 145
    iget-object v0, p0, Lcom/android/internal/telephony/cat/CatService;->mCmdIf:Lcom/android/internal/telephony/CommandsInterface;

    #@4c
    const/16 v1, 0x8

    #@4e
    invoke-interface {v0, p0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->setOnCatCcAlphaNotify(Landroid/os/Handler;ILjava/lang/Object;)V

    #@51
    .line 146
    sput-object p3, Lcom/android/internal/telephony/cat/CatService;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@53
    .line 147
    sput-object p2, Lcom/android/internal/telephony/cat/CatService;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@55
    .line 150
    sget-object v0, Lcom/android/internal/telephony/cat/CatService;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@57
    const/4 v1, 0x7

    #@58
    invoke-virtual {v0, p0, v1, v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->registerForReady(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5b
    .line 151
    sget-object v0, Lcom/android/internal/telephony/cat/CatService;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@5d
    const/16 v1, 0x14

    #@5f
    invoke-virtual {v0, p0, v1, v2}, Lcom/android/internal/telephony/uicc/IccRecords;->registerForRecordsLoaded(Landroid/os/Handler;ILjava/lang/Object;)V

    #@62
    .line 154
    invoke-static {}, Lcom/android/internal/telephony/uicc/UiccController;->getInstance()Lcom/android/internal/telephony/uicc/UiccController;

    #@65
    move-result-object v0

    #@66
    iput-object v0, p0, Lcom/android/internal/telephony/cat/CatService;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@68
    .line 155
    iget-object v0, p0, Lcom/android/internal/telephony/cat/CatService;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@6a
    if-eqz v0, :cond_92

    #@6c
    .line 156
    iget-object v0, p0, Lcom/android/internal/telephony/cat/CatService;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@6e
    const/16 v1, 0x15

    #@70
    invoke-virtual {v0, p0, v1, v2}, Lcom/android/internal/telephony/uicc/UiccController;->registerForIccChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@73
    .line 162
    :goto_73
    invoke-virtual {p0}, Lcom/android/internal/telephony/cat/CatService;->isStkAppInstalled()Z

    #@76
    move-result v0

    #@77
    iput-boolean v0, p0, Lcom/android/internal/telephony/cat/CatService;->mStkAppInstalled:Z

    #@79
    .line 164
    new-instance v0, Ljava/lang/StringBuilder;

    #@7b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7e
    const-string v1, "Running CAT service. STK app installed:"

    #@80
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v0

    #@84
    iget-boolean v1, p0, Lcom/android/internal/telephony/cat/CatService;->mStkAppInstalled:Z

    #@86
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@89
    move-result-object v0

    #@8a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8d
    move-result-object v0

    #@8e
    invoke-static {p0, v0}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@91
    .line 165
    return-void

    #@92
    .line 158
    :cond_92
    const-string v0, "UiccController instance is null"

    #@94
    invoke-static {p0, v0}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@97
    goto :goto_73
.end method

.method private broadcastAlphaMessage(Ljava/lang/String;)V
    .registers 5
    .parameter "alphaString"

    #@0
    .prologue
    .line 882
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "Broadcasting STK Alpha message from card: "

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-static {p0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@16
    .line 883
    new-instance v0, Landroid/content/Intent;

    #@18
    const-string v1, "qualcomm.intent.action.stk.alpha_notify"

    #@1a
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1d
    .line 884
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "alpha_string"

    #@1f
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@22
    .line 885
    iget-object v1, p0, Lcom/android/internal/telephony/cat/CatService;->mContext:Landroid/content/Context;

    #@24
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@27
    .line 886
    return-void
.end method

.method private broadcastCardStateAndIccRefreshResp(Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;Lcom/android/internal/telephony/uicc/IccRefreshResponse;)V
    .registers 7
    .parameter "cardState"
    .parameter "IccRefreshState"

    #@0
    .prologue
    .line 863
    new-instance v1, Landroid/content/Intent;

    #@2
    const-string v2, "qualcomm.intent.action.stk.icc_status_change"

    #@4
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 864
    .local v1, intent:Landroid/content/Intent;
    sget-object v2, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_PRESENT:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@9
    if-ne p1, v2, :cond_5e

    #@b
    const/4 v0, 0x1

    #@c
    .line 866
    .local v0, cardStatus:Z
    :goto_c
    if-eqz p2, :cond_2d

    #@e
    .line 868
    const-string v2, "refresh_result"

    #@10
    iget v3, p2, Lcom/android/internal/telephony/uicc/IccRefreshResponse;->refreshResult:I

    #@12
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@15
    .line 869
    new-instance v2, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v3, "Sending IccResult with Result: "

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    iget v3, p2, Lcom/android/internal/telephony/uicc/IccRefreshResponse;->refreshResult:I

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    invoke-static {p0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@2d
    .line 874
    :cond_2d
    const-string v2, "card_status"

    #@2f
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@32
    .line 875
    new-instance v2, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v3, "Sending Card Status: "

    #@39
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v2

    #@41
    const-string v3, " "

    #@43
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    const-string v3, "cardStatus: "

    #@49
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v2

    #@4d
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v2

    #@55
    invoke-static {p0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@58
    .line 878
    iget-object v2, p0, Lcom/android/internal/telephony/cat/CatService;->mContext:Landroid/content/Context;

    #@5a
    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@5d
    .line 879
    return-void

    #@5e
    .line 864
    .end local v0           #cardStatus:Z
    :cond_5e
    const/4 v0, 0x0

    #@5f
    goto :goto_c
.end method

.method private encodeOptionalTags(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/ResultCode;Lcom/android/internal/telephony/cat/Input;Ljava/io/ByteArrayOutputStream;)V
    .registers 8
    .parameter "cmdDet"
    .parameter "resultCode"
    .parameter "cmdInput"
    .parameter "buf"

    #@0
    .prologue
    .line 542
    iget v1, p1, Lcom/android/internal/telephony/cat/CommandDetails;->typeOfCommand:I

    #@2
    invoke-static {v1}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->fromInt(I)Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@5
    move-result-object v0

    #@6
    .line 543
    .local v0, cmdType:Lcom/android/internal/telephony/cat/AppInterface$CommandType;
    if-eqz v0, :cond_55

    #@8
    .line 544
    sget-object v1, Lcom/android/internal/telephony/cat/CatService$1;->$SwitchMap$com$android$internal$telephony$cat$AppInterface$CommandType:[I

    #@a
    invoke-virtual {v0}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->ordinal()I

    #@d
    move-result v2

    #@e
    aget v1, v1, v2

    #@10
    sparse-switch v1, :sswitch_data_6c

    #@13
    .line 561
    new-instance v1, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v2, "encodeOptionalTags() Unsupported Cmd details="

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v1

    #@26
    invoke-static {p0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@29
    .line 567
    :cond_29
    :goto_29
    return-void

    #@2a
    .line 549
    :sswitch_2a
    invoke-virtual {p2}, Lcom/android/internal/telephony/cat/ResultCode;->value()I

    #@2d
    move-result v1

    #@2e
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->NO_RESPONSE_FROM_USER:Lcom/android/internal/telephony/cat/ResultCode;

    #@30
    invoke-virtual {v2}, Lcom/android/internal/telephony/cat/ResultCode;->value()I

    #@33
    move-result v2

    #@34
    if-ne v1, v2, :cond_29

    #@36
    if-eqz p3, :cond_29

    #@38
    iget-object v1, p3, Lcom/android/internal/telephony/cat/Input;->duration:Lcom/android/internal/telephony/cat/Duration;

    #@3a
    if-eqz v1, :cond_29

    #@3c
    .line 551
    invoke-direct {p0, p4, p3}, Lcom/android/internal/telephony/cat/CatService;->getInKeyResponse(Ljava/io/ByteArrayOutputStream;Lcom/android/internal/telephony/cat/Input;)V

    #@3f
    goto :goto_29

    #@40
    .line 555
    :sswitch_40
    iget v1, p1, Lcom/android/internal/telephony/cat/CommandDetails;->commandQualifier:I

    #@42
    const/4 v2, 0x4

    #@43
    if-ne v1, v2, :cond_29

    #@45
    invoke-virtual {p2}, Lcom/android/internal/telephony/cat/ResultCode;->value()I

    #@48
    move-result v1

    #@49
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    #@4b
    invoke-virtual {v2}, Lcom/android/internal/telephony/cat/ResultCode;->value()I

    #@4e
    move-result v2

    #@4f
    if-ne v1, v2, :cond_29

    #@51
    .line 557
    invoke-direct {p0, p4}, Lcom/android/internal/telephony/cat/CatService;->getPliResponse(Ljava/io/ByteArrayOutputStream;)V

    #@54
    goto :goto_29

    #@55
    .line 565
    :cond_55
    new-instance v1, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v2, "encodeOptionalTags() bad Cmd details="

    #@5c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v1

    #@60
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v1

    #@64
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v1

    #@68
    invoke-static {p0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@6b
    goto :goto_29

    #@6c
    .line 544
    :sswitch_data_6c
    .sparse-switch
        0x6 -> :sswitch_40
        0xa -> :sswitch_2a
    .end sparse-switch
.end method

.method private eventDownload(III[BZ)V
    .registers 17
    .parameter "event"
    .parameter "sourceId"
    .parameter "destinationId"
    .parameter "additionalInfo"
    .parameter "oneShot"

    #@0
    .prologue
    .line 637
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    #@2
    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@5
    .line 640
    .local v2, buf:Ljava/io/ByteArrayOutputStream;
    const/16 v8, 0xd6

    #@7
    .line 641
    .local v8, tag:I
    invoke-virtual {v2, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@a
    .line 644
    const/4 v9, 0x0

    #@b
    invoke-virtual {v2, v9}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@e
    .line 647
    sget-object v9, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->EVENT_LIST:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@10
    invoke-virtual {v9}, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->value()I

    #@13
    move-result v9

    #@14
    or-int/lit16 v8, v9, 0x80

    #@16
    .line 648
    invoke-virtual {v2, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@19
    .line 649
    const/4 v9, 0x1

    #@1a
    invoke-virtual {v2, v9}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@1d
    .line 650
    invoke-virtual {v2, p1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@20
    .line 653
    sget-object v9, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->DEVICE_IDENTITIES:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@22
    invoke-virtual {v9}, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->value()I

    #@25
    move-result v9

    #@26
    or-int/lit16 v8, v9, 0x80

    #@28
    .line 654
    invoke-virtual {v2, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@2b
    .line 655
    const/4 v9, 0x2

    #@2c
    invoke-virtual {v2, v9}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@2f
    .line 656
    invoke-virtual {v2, p2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@32
    .line 657
    invoke-virtual {v2, p3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@35
    .line 673
    packed-switch p1, :pswitch_data_ae

    #@38
    .line 689
    :goto_38
    :pswitch_38
    if-eqz p4, :cond_66

    #@3a
    .line 690
    move-object v0, p4

    #@3b
    .local v0, arr$:[B
    array-length v6, v0

    #@3c
    .local v6, len$:I
    const/4 v4, 0x0

    #@3d
    .local v4, i$:I
    :goto_3d
    if-ge v4, v6, :cond_66

    #@3f
    aget-byte v1, v0, v4

    #@41
    .line 691
    .local v1, b:B
    invoke-virtual {v2, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@44
    .line 690
    add-int/lit8 v4, v4, 0x1

    #@46
    goto :goto_3d

    #@47
    .line 675
    .end local v0           #arr$:[B
    .end local v1           #b:B
    .end local v4           #i$:I
    .end local v6           #len$:I
    :pswitch_47
    sget-object v9, Lcom/android/internal/telephony/cat/CatService;->sInstance:Lcom/android/internal/telephony/cat/CatService;

    #@49
    const-string v10, " Sending Idle Screen Available event download to ICC"

    #@4b
    invoke-static {v9, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@4e
    goto :goto_38

    #@4f
    .line 678
    :pswitch_4f
    sget-object v9, Lcom/android/internal/telephony/cat/CatService;->sInstance:Lcom/android/internal/telephony/cat/CatService;

    #@51
    const-string v10, " Sending Language Selection event download to ICC"

    #@53
    invoke-static {v9, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@56
    .line 679
    sget-object v9, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->LANGUAGE:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@58
    invoke-virtual {v9}, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->value()I

    #@5b
    move-result v9

    #@5c
    or-int/lit16 v8, v9, 0x80

    #@5e
    .line 680
    invoke-virtual {v2, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@61
    .line 682
    const/4 v9, 0x2

    #@62
    invoke-virtual {v2, v9}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@65
    goto :goto_38

    #@66
    .line 695
    :cond_66
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@69
    move-result-object v7

    #@6a
    .line 698
    .local v7, rawData:[B
    array-length v9, v7

    #@6b
    add-int/lit8 v5, v9, -0x2

    #@6d
    .line 699
    .local v5, len:I
    const/4 v9, 0x1

    #@6e
    int-to-byte v10, v5

    #@6f
    aput-byte v10, v7, v9

    #@71
    .line 701
    invoke-static {v7}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@74
    move-result-object v3

    #@75
    .line 703
    .local v3, hexString:Ljava/lang/String;
    sget-boolean v9, Lcom/android/internal/telephony/cat/CatService;->ENABLE_PRIVACY_LOG:Z

    #@77
    if-eqz v9, :cond_96

    #@79
    .line 705
    new-instance v9, Ljava/lang/StringBuilder;

    #@7b
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@7e
    const-string v10, "ENVELOPE COMMAND: "

    #@80
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v9

    #@84
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v9

    #@88
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8b
    move-result-object v9

    #@8c
    invoke-static {p0, v9}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@8f
    .line 712
    :goto_8f
    iget-object v9, p0, Lcom/android/internal/telephony/cat/CatService;->mCmdIf:Lcom/android/internal/telephony/CommandsInterface;

    #@91
    const/4 v10, 0x0

    #@92
    invoke-interface {v9, v3, v10}, Lcom/android/internal/telephony/CommandsInterface;->sendEnvelope(Ljava/lang/String;Landroid/os/Message;)V

    #@95
    .line 713
    return-void

    #@96
    .line 709
    :cond_96
    new-instance v9, Ljava/lang/StringBuilder;

    #@98
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@9b
    const-string v10, "ENVELOPE COMMAND: "

    #@9d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v9

    #@a1
    const/4 v10, 0x0

    #@a2
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v9

    #@a6
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v9

    #@aa
    invoke-static {p0, v9}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@ad
    goto :goto_8f

    #@ae
    .line 673
    :pswitch_data_ae
    .packed-switch 0x5
        :pswitch_47
        :pswitch_38
        :pswitch_4f
    .end packed-switch
.end method

.method private getInKeyResponse(Ljava/io/ByteArrayOutputStream;Lcom/android/internal/telephony/cat/Input;)V
    .registers 5
    .parameter "buf"
    .parameter "cmdInput"

    #@0
    .prologue
    .line 570
    sget-object v1, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->DURATION:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->value()I

    #@5
    move-result v0

    #@6
    .line 572
    .local v0, tag:I
    invoke-virtual {p1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@9
    .line 573
    const/4 v1, 0x2

    #@a
    invoke-virtual {p1, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@d
    .line 574
    iget-object v1, p2, Lcom/android/internal/telephony/cat/Input;->duration:Lcom/android/internal/telephony/cat/Duration;

    #@f
    iget-object v1, v1, Lcom/android/internal/telephony/cat/Duration;->timeUnit:Lcom/android/internal/telephony/cat/Duration$TimeUnit;

    #@11
    sget-object v1, Lcom/android/internal/telephony/cat/Duration$TimeUnit;->SECOND:Lcom/android/internal/telephony/cat/Duration$TimeUnit;

    #@13
    invoke-virtual {v1}, Lcom/android/internal/telephony/cat/Duration$TimeUnit;->value()I

    #@16
    move-result v1

    #@17
    invoke-virtual {p1, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@1a
    .line 575
    iget-object v1, p2, Lcom/android/internal/telephony/cat/Input;->duration:Lcom/android/internal/telephony/cat/Duration;

    #@1c
    iget v1, v1, Lcom/android/internal/telephony/cat/Duration;->timeInterval:I

    #@1e
    invoke-virtual {p1, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@21
    .line 576
    return-void
.end method

.method public static getInstance()Lcom/android/internal/telephony/cat/AppInterface;
    .registers 1

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 782
    invoke-static {v0, v0, v0}, Lcom/android/internal/telephony/cat/CatService;->getInstance(Lcom/android/internal/telephony/CommandsInterface;Landroid/content/Context;Lcom/android/internal/telephony/uicc/UiccCard;)Lcom/android/internal/telephony/cat/CatService;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static getInstance(Lcom/android/internal/telephony/CommandsInterface;Landroid/content/Context;Lcom/android/internal/telephony/uicc/UiccCard;)Lcom/android/internal/telephony/cat/CatService;
    .registers 11
    .parameter "ci"
    .parameter "context"
    .parameter "ic"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 727
    const/4 v2, 0x0

    #@2
    .line 728
    .local v2, ca:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    const/4 v5, 0x0

    #@3
    .line 729
    .local v5, fh:Lcom/android/internal/telephony/uicc/IccFileHandler;
    const/4 v3, 0x0

    #@4
    .line 730
    .local v3, ir:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz p2, :cond_15

    #@6
    .line 734
    const/4 v1, 0x0

    #@7
    invoke-virtual {p2, v1}, Lcom/android/internal/telephony/uicc/UiccCard;->getApplicationIndex(I)Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@a
    move-result-object v2

    #@b
    .line 735
    if-eqz v2, :cond_15

    #@d
    .line 736
    invoke-virtual {v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getIccFileHandler()Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@10
    move-result-object v5

    #@11
    .line 737
    invoke-virtual {v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getIccRecords()Lcom/android/internal/telephony/uicc/IccRecords;

    #@14
    move-result-object v3

    #@15
    .line 740
    :cond_15
    sget-object v7, Lcom/android/internal/telephony/cat/CatService;->sInstanceLock:Ljava/lang/Object;

    #@17
    monitor-enter v7

    #@18
    .line 741
    :try_start_18
    sget-object v1, Lcom/android/internal/telephony/cat/CatService;->sInstance:Lcom/android/internal/telephony/cat/CatService;

    #@1a
    if-nez v1, :cond_50

    #@1c
    .line 742
    if-eqz p0, :cond_28

    #@1e
    if-eqz v2, :cond_28

    #@20
    if-eqz v3, :cond_28

    #@22
    if-eqz p1, :cond_28

    #@24
    if-eqz v5, :cond_28

    #@26
    if-nez p2, :cond_2a

    #@28
    .line 744
    :cond_28
    monitor-exit v7

    #@29
    .line 772
    :goto_29
    return-object v0

    #@2a
    .line 746
    :cond_2a
    new-instance v0, Landroid/os/HandlerThread;

    #@2c
    const-string v1, "Cat Telephony service"

    #@2e
    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@31
    sput-object v0, Lcom/android/internal/telephony/cat/CatService;->mhandlerThread:Landroid/os/HandlerThread;

    #@33
    .line 747
    sget-object v0, Lcom/android/internal/telephony/cat/CatService;->mhandlerThread:Landroid/os/HandlerThread;

    #@35
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    #@38
    .line 748
    new-instance v0, Lcom/android/internal/telephony/cat/CatService;

    #@3a
    move-object v1, p0

    #@3b
    move-object v4, p1

    #@3c
    move-object v6, p2

    #@3d
    invoke-direct/range {v0 .. v6}, Lcom/android/internal/telephony/cat/CatService;-><init>(Lcom/android/internal/telephony/CommandsInterface;Lcom/android/internal/telephony/uicc/UiccCardApplication;Lcom/android/internal/telephony/uicc/IccRecords;Landroid/content/Context;Lcom/android/internal/telephony/uicc/IccFileHandler;Lcom/android/internal/telephony/uicc/UiccCard;)V

    #@40
    sput-object v0, Lcom/android/internal/telephony/cat/CatService;->sInstance:Lcom/android/internal/telephony/cat/CatService;

    #@42
    .line 749
    sget-object v0, Lcom/android/internal/telephony/cat/CatService;->sInstance:Lcom/android/internal/telephony/cat/CatService;

    #@44
    const-string v1, "NEW sInstance"

    #@46
    invoke-static {v0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@49
    .line 772
    :goto_49
    sget-object v0, Lcom/android/internal/telephony/cat/CatService;->sInstance:Lcom/android/internal/telephony/cat/CatService;

    #@4b
    monitor-exit v7

    #@4c
    goto :goto_29

    #@4d
    .line 773
    :catchall_4d
    move-exception v0

    #@4e
    monitor-exit v7
    :try_end_4f
    .catchall {:try_start_18 .. :try_end_4f} :catchall_4d

    #@4f
    throw v0

    #@50
    .line 750
    :cond_50
    if-eqz v3, :cond_92

    #@52
    :try_start_52
    sget-object v0, Lcom/android/internal/telephony/cat/CatService;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@54
    if-eq v0, v3, :cond_92

    #@56
    .line 751
    sget-object v0, Lcom/android/internal/telephony/cat/CatService;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@58
    if-eqz v0, :cond_61

    #@5a
    .line 752
    sget-object v0, Lcom/android/internal/telephony/cat/CatService;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@5c
    sget-object v1, Lcom/android/internal/telephony/cat/CatService;->sInstance:Lcom/android/internal/telephony/cat/CatService;

    #@5e
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/uicc/IccRecords;->unregisterForRecordsLoaded(Landroid/os/Handler;)V

    #@61
    .line 755
    :cond_61
    sget-object v0, Lcom/android/internal/telephony/cat/CatService;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@63
    if-eqz v0, :cond_6c

    #@65
    .line 756
    sget-object v0, Lcom/android/internal/telephony/cat/CatService;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@67
    sget-object v1, Lcom/android/internal/telephony/cat/CatService;->sInstance:Lcom/android/internal/telephony/cat/CatService;

    #@69
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->unregisterForReady(Landroid/os/Handler;)V

    #@6c
    .line 759
    :cond_6c
    sget-object v0, Lcom/android/internal/telephony/cat/CatService;->sInstance:Lcom/android/internal/telephony/cat/CatService;

    #@6e
    const-string v1, "Reinitialize the Service with SIMRecords and UiccCardApplication"

    #@70
    invoke-static {v0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@73
    .line 761
    sput-object v3, Lcom/android/internal/telephony/cat/CatService;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@75
    .line 762
    sput-object v2, Lcom/android/internal/telephony/cat/CatService;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@77
    .line 765
    sget-object v0, Lcom/android/internal/telephony/cat/CatService;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@79
    sget-object v1, Lcom/android/internal/telephony/cat/CatService;->sInstance:Lcom/android/internal/telephony/cat/CatService;

    #@7b
    const/16 v4, 0x14

    #@7d
    const/4 v6, 0x0

    #@7e
    invoke-virtual {v0, v1, v4, v6}, Lcom/android/internal/telephony/uicc/IccRecords;->registerForRecordsLoaded(Landroid/os/Handler;ILjava/lang/Object;)V

    #@81
    .line 766
    sget-object v0, Lcom/android/internal/telephony/cat/CatService;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@83
    sget-object v1, Lcom/android/internal/telephony/cat/CatService;->sInstance:Lcom/android/internal/telephony/cat/CatService;

    #@85
    const/4 v4, 0x7

    #@86
    const/4 v6, 0x0

    #@87
    invoke-virtual {v0, v1, v4, v6}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->registerForReady(Landroid/os/Handler;ILjava/lang/Object;)V

    #@8a
    .line 768
    sget-object v0, Lcom/android/internal/telephony/cat/CatService;->sInstance:Lcom/android/internal/telephony/cat/CatService;

    #@8c
    const-string v1, "sr changed reinitialize and return current sInstance"

    #@8e
    invoke-static {v0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@91
    goto :goto_49

    #@92
    .line 770
    :cond_92
    sget-object v0, Lcom/android/internal/telephony/cat/CatService;->sInstance:Lcom/android/internal/telephony/cat/CatService;

    #@94
    const-string v1, "Return current sInstance"

    #@96
    invoke-static {v0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_99
    .catchall {:try_start_52 .. :try_end_99} :catchall_4d

    #@99
    goto :goto_49
.end method

.method private getPliResponse(Ljava/io/ByteArrayOutputStream;)V
    .registers 7
    .parameter "buf"

    #@0
    .prologue
    .line 581
    const-string v2, "persist.sys.language"

    #@2
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 583
    .local v0, lang:Ljava/lang/String;
    if-eqz v0, :cond_24

    #@8
    .line 585
    sget-object v2, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->LANGUAGE:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@a
    invoke-virtual {v2}, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->value()I

    #@d
    move-result v1

    #@e
    .line 586
    .local v1, tag:I
    invoke-virtual {p1, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@11
    .line 587
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@14
    move-result v2

    #@15
    invoke-static {p1, v2}, Lcom/android/internal/telephony/cat/ResponseData;->writeLength(Ljava/io/ByteArrayOutputStream;I)V

    #@18
    .line 588
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    #@1b
    move-result-object v2

    #@1c
    const/4 v3, 0x0

    #@1d
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@20
    move-result v4

    #@21
    invoke-virtual {p1, v2, v3, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@24
    .line 590
    .end local v1           #tag:I
    :cond_24
    return-void
.end method

.method private handleCmdResponse(Lcom/android/internal/telephony/cat/CatResponseMessage;)V
    .registers 13
    .parameter "resMsg"

    #@0
    .prologue
    const/16 v3, 0x81

    #@2
    const/4 v0, 0x1

    #@3
    const/4 v5, 0x0

    #@4
    const/4 v10, 0x0

    #@5
    .line 939
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cat/CatService;->validateResponse(Lcom/android/internal/telephony/cat/CatResponseMessage;)Z

    #@8
    move-result v2

    #@9
    if-nez v2, :cond_c

    #@b
    .line 1038
    :goto_b
    return-void

    #@c
    .line 942
    :cond_c
    const/4 v8, 0x0

    #@d
    .line 943
    .local v8, resp:Lcom/android/internal/telephony/cat/ResponseData;
    const/4 v6, 0x0

    #@e
    .line 944
    .local v6, helpRequired:Z
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatResponseMessage;->getCmdDetails()Lcom/android/internal/telephony/cat/CommandDetails;

    #@11
    move-result-object v1

    #@12
    .line 945
    .local v1, cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;
    iget v2, v1, Lcom/android/internal/telephony/cat/CommandDetails;->typeOfCommand:I

    #@14
    invoke-static {v2}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->fromInt(I)Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@17
    move-result-object v9

    #@18
    .line 947
    .local v9, type:Lcom/android/internal/telephony/cat/AppInterface$CommandType;
    sget-object v2, Lcom/android/internal/telephony/cat/CatService$1;->$SwitchMap$com$android$internal$telephony$cat$ResultCode:[I

    #@1a
    iget-object v4, p1, Lcom/android/internal/telephony/cat/CatResponseMessage;->resCode:Lcom/android/internal/telephony/cat/ResultCode;

    #@1c
    invoke-virtual {v4}, Lcom/android/internal/telephony/cat/ResultCode;->ordinal()I

    #@1f
    move-result v4

    #@20
    aget v2, v2, v4

    #@22
    packed-switch v2, :pswitch_data_ba

    #@25
    goto :goto_b

    #@26
    .line 949
    :pswitch_26
    const/4 v6, 0x1

    #@27
    .line 962
    :pswitch_27
    sget-object v2, Lcom/android/internal/telephony/cat/CatService$1;->$SwitchMap$com$android$internal$telephony$cat$AppInterface$CommandType:[I

    #@29
    invoke-virtual {v9}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->ordinal()I

    #@2c
    move-result v4

    #@2d
    aget v2, v2, v4

    #@2f
    packed-switch v2, :pswitch_data_e0

    #@32
    :cond_32
    :pswitch_32
    move-object v5, v8

    #@33
    .line 1035
    .end local v8           #resp:Lcom/android/internal/telephony/cat/ResponseData;
    .local v5, resp:Lcom/android/internal/telephony/cat/ResponseData;
    :goto_33
    iget-object v2, p1, Lcom/android/internal/telephony/cat/CatResponseMessage;->resCode:Lcom/android/internal/telephony/cat/ResultCode;

    #@35
    iget-boolean v3, p1, Lcom/android/internal/telephony/cat/CatResponseMessage;->includeAdditionalInfo:Z

    #@37
    iget v4, p1, Lcom/android/internal/telephony/cat/CatResponseMessage;->additionalInfo:I

    #@39
    move-object v0, p0

    #@3a
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/telephony/cat/CatService;->sendTerminalResponse(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/ResultCode;ZILcom/android/internal/telephony/cat/ResponseData;)V

    #@3d
    .line 1037
    iput-object v10, p0, Lcom/android/internal/telephony/cat/CatService;->mCurrntCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    #@3f
    goto :goto_b

    #@40
    .line 964
    .end local v5           #resp:Lcom/android/internal/telephony/cat/ResponseData;
    .restart local v8       #resp:Lcom/android/internal/telephony/cat/ResponseData;
    :pswitch_40
    iget-object v2, p1, Lcom/android/internal/telephony/cat/CatResponseMessage;->resCode:Lcom/android/internal/telephony/cat/ResultCode;

    #@42
    sget-object v3, Lcom/android/internal/telephony/cat/ResultCode;->HELP_INFO_REQUIRED:Lcom/android/internal/telephony/cat/ResultCode;

    #@44
    if-ne v2, v3, :cond_4d

    #@46
    move v6, v0

    #@47
    .line 965
    :goto_47
    iget v0, p1, Lcom/android/internal/telephony/cat/CatResponseMessage;->usersMenuSelection:I

    #@49
    invoke-direct {p0, v0, v6}, Lcom/android/internal/telephony/cat/CatService;->sendMenuSelection(IZ)V

    #@4c
    goto :goto_b

    #@4d
    :cond_4d
    move v6, v5

    #@4e
    .line 964
    goto :goto_47

    #@4f
    .line 968
    :pswitch_4f
    new-instance v5, Lcom/android/internal/telephony/cat/SelectItemResponseData;

    #@51
    iget v0, p1, Lcom/android/internal/telephony/cat/CatResponseMessage;->usersMenuSelection:I

    #@53
    invoke-direct {v5, v0}, Lcom/android/internal/telephony/cat/SelectItemResponseData;-><init>(I)V

    #@56
    .line 969
    .end local v8           #resp:Lcom/android/internal/telephony/cat/ResponseData;
    .restart local v5       #resp:Lcom/android/internal/telephony/cat/ResponseData;
    goto :goto_33

    #@57
    .line 972
    .end local v5           #resp:Lcom/android/internal/telephony/cat/ResponseData;
    .restart local v8       #resp:Lcom/android/internal/telephony/cat/ResponseData;
    :pswitch_57
    iget-object v0, p0, Lcom/android/internal/telephony/cat/CatService;->mCurrntCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    #@59
    invoke-virtual {v0}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geInput()Lcom/android/internal/telephony/cat/Input;

    #@5c
    move-result-object v7

    #@5d
    .line 973
    .local v7, input:Lcom/android/internal/telephony/cat/Input;
    iget-boolean v0, v7, Lcom/android/internal/telephony/cat/Input;->yesNo:Z

    #@5f
    if-nez v0, :cond_6f

    #@61
    .line 976
    if-nez v6, :cond_32

    #@63
    .line 977
    new-instance v5, Lcom/android/internal/telephony/cat/GetInkeyInputResponseData;

    #@65
    iget-object v0, p1, Lcom/android/internal/telephony/cat/CatResponseMessage;->usersInput:Ljava/lang/String;

    #@67
    iget-boolean v2, v7, Lcom/android/internal/telephony/cat/Input;->ucs2:Z

    #@69
    iget-boolean v3, v7, Lcom/android/internal/telephony/cat/Input;->packed:Z

    #@6b
    invoke-direct {v5, v0, v2, v3}, Lcom/android/internal/telephony/cat/GetInkeyInputResponseData;-><init>(Ljava/lang/String;ZZ)V

    #@6e
    .end local v8           #resp:Lcom/android/internal/telephony/cat/ResponseData;
    .restart local v5       #resp:Lcom/android/internal/telephony/cat/ResponseData;
    goto :goto_33

    #@6f
    .line 981
    .end local v5           #resp:Lcom/android/internal/telephony/cat/ResponseData;
    .restart local v8       #resp:Lcom/android/internal/telephony/cat/ResponseData;
    :cond_6f
    new-instance v5, Lcom/android/internal/telephony/cat/GetInkeyInputResponseData;

    #@71
    iget-boolean v0, p1, Lcom/android/internal/telephony/cat/CatResponseMessage;->usersYesNoSelection:Z

    #@73
    invoke-direct {v5, v0}, Lcom/android/internal/telephony/cat/GetInkeyInputResponseData;-><init>(Z)V

    #@76
    .line 984
    .end local v8           #resp:Lcom/android/internal/telephony/cat/ResponseData;
    .restart local v5       #resp:Lcom/android/internal/telephony/cat/ResponseData;
    goto :goto_33

    #@77
    .end local v5           #resp:Lcom/android/internal/telephony/cat/ResponseData;
    .end local v7           #input:Lcom/android/internal/telephony/cat/Input;
    .restart local v8       #resp:Lcom/android/internal/telephony/cat/ResponseData;
    :pswitch_77
    move-object v5, v8

    #@78
    .line 987
    .end local v8           #resp:Lcom/android/internal/telephony/cat/ResponseData;
    .restart local v5       #resp:Lcom/android/internal/telephony/cat/ResponseData;
    goto :goto_33

    #@79
    .line 991
    .end local v5           #resp:Lcom/android/internal/telephony/cat/ResponseData;
    .restart local v8       #resp:Lcom/android/internal/telephony/cat/ResponseData;
    :pswitch_79
    iget-object v0, p0, Lcom/android/internal/telephony/cat/CatService;->mCmdIf:Lcom/android/internal/telephony/CommandsInterface;

    #@7b
    iget-boolean v2, p1, Lcom/android/internal/telephony/cat/CatResponseMessage;->usersConfirm:Z

    #@7d
    invoke-interface {v0, v2, v10}, Lcom/android/internal/telephony/CommandsInterface;->handleCallSetupRequestFromSim(ZLandroid/os/Message;)V

    #@80
    .line 995
    iput-object v10, p0, Lcom/android/internal/telephony/cat/CatService;->mCurrntCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    #@82
    goto :goto_b

    #@83
    .line 998
    :pswitch_83
    const/4 v0, 0x5

    #@84
    iget v2, p1, Lcom/android/internal/telephony/cat/CatResponseMessage;->eventValue:I

    #@86
    if-ne v0, v2, :cond_93

    #@88
    .line 999
    iget v1, p1, Lcom/android/internal/telephony/cat/CatResponseMessage;->eventValue:I

    #@8a
    .end local v1           #cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;
    const/4 v2, 0x2

    #@8b
    iget-object v4, p1, Lcom/android/internal/telephony/cat/CatResponseMessage;->addedInfo:[B

    #@8d
    move-object v0, p0

    #@8e
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/telephony/cat/CatService;->eventDownload(III[BZ)V

    #@91
    goto/16 :goto_b

    #@93
    .line 1002
    .restart local v1       #cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;
    :cond_93
    iget v1, p1, Lcom/android/internal/telephony/cat/CatResponseMessage;->eventValue:I

    #@95
    .end local v1           #cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;
    const/16 v2, 0x82

    #@97
    iget-object v4, p1, Lcom/android/internal/telephony/cat/CatResponseMessage;->addedInfo:[B

    #@99
    move-object v0, p0

    #@9a
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/telephony/cat/CatService;->eventDownload(III[BZ)V

    #@9d
    goto/16 :goto_b

    #@9f
    .line 1015
    .restart local v1       #cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;
    :pswitch_9f
    sget-object v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SET_UP_CALL:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@a1
    if-eq v9, v0, :cond_a7

    #@a3
    sget-object v0, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->OPEN_CHANNEL:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@a5
    if-ne v9, v0, :cond_b0

    #@a7
    .line 1016
    :cond_a7
    iget-object v0, p0, Lcom/android/internal/telephony/cat/CatService;->mCmdIf:Lcom/android/internal/telephony/CommandsInterface;

    #@a9
    invoke-interface {v0, v5, v10}, Lcom/android/internal/telephony/CommandsInterface;->handleCallSetupRequestFromSim(ZLandroid/os/Message;)V

    #@ac
    .line 1017
    iput-object v10, p0, Lcom/android/internal/telephony/cat/CatService;->mCurrntCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    #@ae
    goto/16 :goto_b

    #@b0
    .line 1020
    :cond_b0
    const/4 v5, 0x0

    #@b1
    .line 1022
    .end local v8           #resp:Lcom/android/internal/telephony/cat/ResponseData;
    .restart local v5       #resp:Lcom/android/internal/telephony/cat/ResponseData;
    goto :goto_33

    #@b2
    .line 1026
    .end local v5           #resp:Lcom/android/internal/telephony/cat/ResponseData;
    .restart local v8       #resp:Lcom/android/internal/telephony/cat/ResponseData;
    :pswitch_b2
    iput-boolean v0, p1, Lcom/android/internal/telephony/cat/CatResponseMessage;->includeAdditionalInfo:Z

    #@b4
    .line 1027
    iput v0, p1, Lcom/android/internal/telephony/cat/CatResponseMessage;->additionalInfo:I

    #@b6
    .line 1030
    :pswitch_b6
    const/4 v5, 0x0

    #@b7
    .line 1031
    .end local v8           #resp:Lcom/android/internal/telephony/cat/ResponseData;
    .restart local v5       #resp:Lcom/android/internal/telephony/cat/ResponseData;
    goto/16 :goto_33

    #@b9
    .line 947
    nop

    #@ba
    :pswitch_data_ba
    .packed-switch 0x1
        :pswitch_26
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_9f
        :pswitch_9f
        :pswitch_b2
        :pswitch_b6
        :pswitch_b6
    .end packed-switch

    #@e0
    .line 962
    :pswitch_data_e0
    .packed-switch 0x1
        :pswitch_40
        :pswitch_77
        :pswitch_32
        :pswitch_32
        :pswitch_83
        :pswitch_32
        :pswitch_77
        :pswitch_4f
        :pswitch_57
        :pswitch_57
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_79
        :pswitch_79
    .end packed-switch
.end method

.method private handleCommand(Lcom/android/internal/telephony/cat/CommandParams;Z)V
    .registers 24
    .parameter "cmdParams"
    .parameter "isProactiveCmd"

    #@0
    .prologue
    .line 290
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/cat/CommandParams;->getCommandType()Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@3
    move-result-object v2

    #@4
    invoke-virtual {v2}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->name()Ljava/lang/String;

    #@7
    move-result-object v2

    #@8
    move-object/from16 v0, p0

    #@a
    invoke-static {v0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@d
    .line 294
    new-instance v19, Lcom/android/internal/telephony/cat/CatCmdMessage;

    #@f
    move-object/from16 v0, v19

    #@11
    move-object/from16 v1, p1

    #@13
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/cat/CatCmdMessage;-><init>(Lcom/android/internal/telephony/cat/CommandParams;)V

    #@16
    .line 295
    .local v19, cmdMsg:Lcom/android/internal/telephony/cat/CatCmdMessage;
    sget-object v2, Lcom/android/internal/telephony/cat/CatService$1;->$SwitchMap$com$android$internal$telephony$cat$AppInterface$CommandType:[I

    #@18
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/cat/CommandParams;->getCommandType()Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->ordinal()I

    #@1f
    move-result v3

    #@20
    aget v2, v2, v3

    #@22
    packed-switch v2, :pswitch_data_2c6

    #@25
    .line 439
    const-string v2, "Unsupported command"

    #@27
    move-object/from16 v0, p0

    #@29
    invoke-static {v0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@2c
    .line 444
    .end local p1
    :cond_2c
    :goto_2c
    return-void

    #@2d
    .line 297
    .restart local p1
    :pswitch_2d
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getMenu()Lcom/android/internal/telephony/cat/Menu;

    #@30
    move-result-object v2

    #@31
    move-object/from16 v0, p0

    #@33
    invoke-direct {v0, v2}, Lcom/android/internal/telephony/cat/CatService;->removeMenu(Lcom/android/internal/telephony/cat/Menu;)Z

    #@36
    move-result v2

    #@37
    if-eqz v2, :cond_60

    #@39
    .line 298
    const/4 v2, 0x0

    #@3a
    move-object/from16 v0, p0

    #@3c
    iput-object v2, v0, Lcom/android/internal/telephony/cat/CatService;->mMenuCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    #@3e
    .line 308
    :cond_3e
    :goto_3e
    move-object/from16 v0, p1

    #@40
    iget-boolean v2, v0, Lcom/android/internal/telephony/cat/CommandParams;->loadIconFailed:Z

    #@42
    if-eqz v2, :cond_82

    #@44
    sget-object v4, Lcom/android/internal/telephony/cat/ResultCode;->PRFRMD_ICON_NOT_DISPLAYED:Lcom/android/internal/telephony/cat/ResultCode;

    #@46
    .line 309
    .local v4, resultCode:Lcom/android/internal/telephony/cat/ResultCode;
    :goto_46
    move-object/from16 v0, p1

    #@48
    iget-object v3, v0, Lcom/android/internal/telephony/cat/CommandParams;->cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;

    #@4a
    const/4 v5, 0x0

    #@4b
    const/4 v6, 0x0

    #@4c
    const/4 v7, 0x0

    #@4d
    move-object/from16 v2, p0

    #@4f
    invoke-direct/range {v2 .. v7}, Lcom/android/internal/telephony/cat/CatService;->sendTerminalResponse(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/ResultCode;ZILcom/android/internal/telephony/cat/ResponseData;)V

    #@52
    .line 442
    .end local v4           #resultCode:Lcom/android/internal/telephony/cat/ResultCode;
    .end local p1
    :cond_52
    :goto_52
    :pswitch_52
    move-object/from16 v0, v19

    #@54
    move-object/from16 v1, p0

    #@56
    iput-object v0, v1, Lcom/android/internal/telephony/cat/CatService;->mCurrntCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    #@58
    .line 443
    move-object/from16 v0, p0

    #@5a
    move-object/from16 v1, v19

    #@5c
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cat/CatService;->broadcastCatCmdIntent(Lcom/android/internal/telephony/cat/CatCmdMessage;)V

    #@5f
    goto :goto_2c

    #@60
    .line 300
    .restart local p1
    :cond_60
    move-object/from16 v0, v19

    #@62
    move-object/from16 v1, p0

    #@64
    iput-object v0, v1, Lcom/android/internal/telephony/cat/CatService;->mMenuCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    #@66
    .line 303
    const-string v2, "ro.build.target_operator"

    #@68
    const-string v3, "OPEN"

    #@6a
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@6d
    move-result-object v2

    #@6e
    const-string v3, "VDF"

    #@70
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@73
    move-result v2

    #@74
    if-eqz v2, :cond_3e

    #@76
    .line 304
    const-string v2, "persist.radio.stk_title"

    #@78
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getMenu()Lcom/android/internal/telephony/cat/Menu;

    #@7b
    move-result-object v3

    #@7c
    iget-object v3, v3, Lcom/android/internal/telephony/cat/Menu;->title:Ljava/lang/String;

    #@7e
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@81
    goto :goto_3e

    #@82
    .line 308
    :cond_82
    sget-object v4, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    #@84
    goto :goto_46

    #@85
    .line 313
    :pswitch_85
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geTextMessage()Lcom/android/internal/telephony/cat/TextMessage;

    #@88
    move-result-object v2

    #@89
    iget-boolean v2, v2, Lcom/android/internal/telephony/cat/TextMessage;->responseNeeded:Z

    #@8b
    if-nez v2, :cond_52

    #@8d
    .line 314
    move-object/from16 v0, p1

    #@8f
    iget-boolean v2, v0, Lcom/android/internal/telephony/cat/CommandParams;->loadIconFailed:Z

    #@91
    if-eqz v2, :cond_a2

    #@93
    sget-object v4, Lcom/android/internal/telephony/cat/ResultCode;->PRFRMD_ICON_NOT_DISPLAYED:Lcom/android/internal/telephony/cat/ResultCode;

    #@95
    .line 315
    .restart local v4       #resultCode:Lcom/android/internal/telephony/cat/ResultCode;
    :goto_95
    move-object/from16 v0, p1

    #@97
    iget-object v3, v0, Lcom/android/internal/telephony/cat/CommandParams;->cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;

    #@99
    const/4 v5, 0x0

    #@9a
    const/4 v6, 0x0

    #@9b
    const/4 v7, 0x0

    #@9c
    move-object/from16 v2, p0

    #@9e
    invoke-direct/range {v2 .. v7}, Lcom/android/internal/telephony/cat/CatService;->sendTerminalResponse(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/ResultCode;ZILcom/android/internal/telephony/cat/ResponseData;)V

    #@a1
    goto :goto_52

    #@a2
    .line 314
    .end local v4           #resultCode:Lcom/android/internal/telephony/cat/ResultCode;
    :cond_a2
    sget-object v4, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    #@a4
    goto :goto_95

    #@a5
    .line 321
    :pswitch_a5
    move-object/from16 v0, p1

    #@a7
    iget-object v2, v0, Lcom/android/internal/telephony/cat/CommandParams;->cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;

    #@a9
    sget-object v3, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SET_UP_IDLE_MODE_TEXT:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@ab
    invoke-virtual {v3}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->value()I

    #@ae
    move-result v3

    #@af
    iput v3, v2, Lcom/android/internal/telephony/cat/CommandDetails;->typeOfCommand:I

    #@b1
    goto :goto_52

    #@b2
    .line 324
    :pswitch_b2
    move-object/from16 v0, p1

    #@b4
    iget-boolean v2, v0, Lcom/android/internal/telephony/cat/CommandParams;->loadIconFailed:Z

    #@b6
    if-eqz v2, :cond_c7

    #@b8
    sget-object v4, Lcom/android/internal/telephony/cat/ResultCode;->PRFRMD_ICON_NOT_DISPLAYED:Lcom/android/internal/telephony/cat/ResultCode;

    #@ba
    .line 325
    .restart local v4       #resultCode:Lcom/android/internal/telephony/cat/ResultCode;
    :goto_ba
    move-object/from16 v0, p1

    #@bc
    iget-object v3, v0, Lcom/android/internal/telephony/cat/CommandParams;->cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;

    #@be
    const/4 v5, 0x0

    #@bf
    const/4 v6, 0x0

    #@c0
    const/4 v7, 0x0

    #@c1
    move-object/from16 v2, p0

    #@c3
    invoke-direct/range {v2 .. v7}, Lcom/android/internal/telephony/cat/CatService;->sendTerminalResponse(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/ResultCode;ZILcom/android/internal/telephony/cat/ResponseData;)V

    #@c6
    goto :goto_52

    #@c7
    .line 324
    .end local v4           #resultCode:Lcom/android/internal/telephony/cat/ResultCode;
    :cond_c7
    sget-object v4, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    #@c9
    goto :goto_ba

    #@ca
    .line 328
    :pswitch_ca
    move-object/from16 v0, p0

    #@cc
    move-object/from16 v1, v19

    #@ce
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/cat/CatService;->isSupportedSetupEventCommand(Lcom/android/internal/telephony/cat/CatCmdMessage;)Z

    #@d1
    move-result v2

    #@d2
    if-eqz v2, :cond_e4

    #@d4
    .line 329
    move-object/from16 v0, p1

    #@d6
    iget-object v6, v0, Lcom/android/internal/telephony/cat/CommandParams;->cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;

    #@d8
    sget-object v7, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    #@da
    const/4 v8, 0x0

    #@db
    const/4 v9, 0x0

    #@dc
    const/4 v10, 0x0

    #@dd
    move-object/from16 v5, p0

    #@df
    invoke-direct/range {v5 .. v10}, Lcom/android/internal/telephony/cat/CatService;->sendTerminalResponse(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/ResultCode;ZILcom/android/internal/telephony/cat/ResponseData;)V

    #@e2
    goto/16 :goto_52

    #@e4
    .line 331
    :cond_e4
    move-object/from16 v0, p1

    #@e6
    iget-object v6, v0, Lcom/android/internal/telephony/cat/CommandParams;->cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;

    #@e8
    sget-object v7, Lcom/android/internal/telephony/cat/ResultCode;->BEYOND_TERMINAL_CAPABILITY:Lcom/android/internal/telephony/cat/ResultCode;

    #@ea
    const/4 v8, 0x0

    #@eb
    const/4 v9, 0x0

    #@ec
    const/4 v10, 0x0

    #@ed
    move-object/from16 v5, p0

    #@ef
    invoke-direct/range {v5 .. v10}, Lcom/android/internal/telephony/cat/CatService;->sendTerminalResponse(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/ResultCode;ZILcom/android/internal/telephony/cat/ResponseData;)V

    #@f2
    goto/16 :goto_52

    #@f4
    .line 337
    :pswitch_f4
    move-object/from16 v0, p1

    #@f6
    iget-object v2, v0, Lcom/android/internal/telephony/cat/CommandParams;->cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;

    #@f8
    iget v2, v2, Lcom/android/internal/telephony/cat/CommandDetails;->commandQualifier:I

    #@fa
    packed-switch v2, :pswitch_data_2f4

    #@fd
    .line 347
    move-object/from16 v0, p1

    #@ff
    iget-object v12, v0, Lcom/android/internal/telephony/cat/CommandParams;->cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;

    #@101
    sget-object v13, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    #@103
    const/4 v14, 0x0

    #@104
    const/4 v15, 0x0

    #@105
    const/16 v16, 0x0

    #@107
    move-object/from16 v11, p0

    #@109
    invoke-direct/range {v11 .. v16}, Lcom/android/internal/telephony/cat/CatService;->sendTerminalResponse(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/ResultCode;ZILcom/android/internal/telephony/cat/ResponseData;)V

    #@10c
    goto/16 :goto_2c

    #@10e
    .line 339
    :pswitch_10e
    new-instance v10, Lcom/android/internal/telephony/cat/DTTZResponseData;

    #@110
    const/4 v2, 0x0

    #@111
    invoke-direct {v10, v2}, Lcom/android/internal/telephony/cat/DTTZResponseData;-><init>(Ljava/util/Calendar;)V

    #@114
    .line 340
    .local v10, resp:Lcom/android/internal/telephony/cat/ResponseData;
    move-object/from16 v0, p1

    #@116
    iget-object v6, v0, Lcom/android/internal/telephony/cat/CommandParams;->cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;

    #@118
    sget-object v7, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    #@11a
    const/4 v8, 0x0

    #@11b
    const/4 v9, 0x0

    #@11c
    move-object/from16 v5, p0

    #@11e
    invoke-direct/range {v5 .. v10}, Lcom/android/internal/telephony/cat/CatService;->sendTerminalResponse(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/ResultCode;ZILcom/android/internal/telephony/cat/ResponseData;)V

    #@121
    goto/16 :goto_2c

    #@123
    .line 343
    .end local v10           #resp:Lcom/android/internal/telephony/cat/ResponseData;
    :pswitch_123
    new-instance v10, Lcom/android/internal/telephony/cat/LanguageResponseData;

    #@125
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@128
    move-result-object v2

    #@129
    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    #@12c
    move-result-object v2

    #@12d
    invoke-direct {v10, v2}, Lcom/android/internal/telephony/cat/LanguageResponseData;-><init>(Ljava/lang/String;)V

    #@130
    .line 344
    .restart local v10       #resp:Lcom/android/internal/telephony/cat/ResponseData;
    move-object/from16 v0, p1

    #@132
    iget-object v6, v0, Lcom/android/internal/telephony/cat/CommandParams;->cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;

    #@134
    sget-object v7, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    #@136
    const/4 v8, 0x0

    #@137
    const/4 v9, 0x0

    #@138
    move-object/from16 v5, p0

    #@13a
    invoke-direct/range {v5 .. v10}, Lcom/android/internal/telephony/cat/CatService;->sendTerminalResponse(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/ResultCode;ZILcom/android/internal/telephony/cat/ResponseData;)V

    #@13d
    goto/16 :goto_2c

    #@13f
    .end local v10           #resp:Lcom/android/internal/telephony/cat/ResponseData;
    :pswitch_13f
    move-object/from16 v2, p1

    #@141
    .line 352
    check-cast v2, Lcom/android/internal/telephony/cat/LaunchBrowserParams;

    #@143
    iget-object v2, v2, Lcom/android/internal/telephony/cat/LaunchBrowserParams;->confirmMsg:Lcom/android/internal/telephony/cat/TextMessage;

    #@145
    iget-object v2, v2, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    #@147
    if-eqz v2, :cond_52

    #@149
    move-object/from16 v2, p1

    #@14b
    check-cast v2, Lcom/android/internal/telephony/cat/LaunchBrowserParams;

    #@14d
    iget-object v2, v2, Lcom/android/internal/telephony/cat/LaunchBrowserParams;->confirmMsg:Lcom/android/internal/telephony/cat/TextMessage;

    #@14f
    iget-object v2, v2, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    #@151
    const-string v3, "Default Message"

    #@153
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@156
    move-result v2

    #@157
    if-eqz v2, :cond_52

    #@159
    .line 354
    move-object/from16 v0, p0

    #@15b
    iget-object v2, v0, Lcom/android/internal/telephony/cat/CatService;->mContext:Landroid/content/Context;

    #@15d
    const v3, 0x1040533

    #@160
    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@163
    move-result-object v20

    #@164
    .line 355
    .local v20, message:Ljava/lang/CharSequence;
    check-cast p1, Lcom/android/internal/telephony/cat/LaunchBrowserParams;

    #@166
    .end local p1
    move-object/from16 v0, p1

    #@168
    iget-object v2, v0, Lcom/android/internal/telephony/cat/LaunchBrowserParams;->confirmMsg:Lcom/android/internal/telephony/cat/TextMessage;

    #@16a
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@16d
    move-result-object v3

    #@16e
    iput-object v3, v2, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    #@170
    goto/16 :goto_52

    #@172
    .end local v20           #message:Ljava/lang/CharSequence;
    .restart local p1
    :pswitch_172
    move-object/from16 v2, p1

    #@174
    .line 366
    check-cast v2, Lcom/android/internal/telephony/cat/DisplayTextParams;

    #@176
    iget-object v2, v2, Lcom/android/internal/telephony/cat/DisplayTextParams;->textMsg:Lcom/android/internal/telephony/cat/TextMessage;

    #@178
    iget-object v2, v2, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    #@17a
    if-eqz v2, :cond_52

    #@17c
    move-object/from16 v2, p1

    #@17e
    check-cast v2, Lcom/android/internal/telephony/cat/DisplayTextParams;

    #@180
    iget-object v2, v2, Lcom/android/internal/telephony/cat/DisplayTextParams;->textMsg:Lcom/android/internal/telephony/cat/TextMessage;

    #@182
    iget-object v2, v2, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    #@184
    const-string v3, "Default Message"

    #@186
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@189
    move-result v2

    #@18a
    if-eqz v2, :cond_52

    #@18c
    .line 368
    move-object/from16 v0, p0

    #@18e
    iget-object v2, v0, Lcom/android/internal/telephony/cat/CatService;->mContext:Landroid/content/Context;

    #@190
    const v3, 0x1040532

    #@193
    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@196
    move-result-object v20

    #@197
    .line 369
    .restart local v20       #message:Ljava/lang/CharSequence;
    check-cast p1, Lcom/android/internal/telephony/cat/DisplayTextParams;

    #@199
    .end local p1
    move-object/from16 v0, p1

    #@19b
    iget-object v2, v0, Lcom/android/internal/telephony/cat/DisplayTextParams;->textMsg:Lcom/android/internal/telephony/cat/TextMessage;

    #@19d
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1a0
    move-result-object v3

    #@1a1
    iput-object v3, v2, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    #@1a3
    goto/16 :goto_52

    #@1a5
    .end local v20           #message:Ljava/lang/CharSequence;
    .restart local p1
    :pswitch_1a5
    move-object/from16 v2, p1

    #@1a7
    .line 375
    check-cast v2, Lcom/android/internal/telephony/cat/CallSetupParams;

    #@1a9
    iget-object v2, v2, Lcom/android/internal/telephony/cat/CallSetupParams;->confirmMsg:Lcom/android/internal/telephony/cat/TextMessage;

    #@1ab
    iget-object v2, v2, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    #@1ad
    if-eqz v2, :cond_52

    #@1af
    move-object/from16 v2, p1

    #@1b1
    check-cast v2, Lcom/android/internal/telephony/cat/CallSetupParams;

    #@1b3
    iget-object v2, v2, Lcom/android/internal/telephony/cat/CallSetupParams;->confirmMsg:Lcom/android/internal/telephony/cat/TextMessage;

    #@1b5
    iget-object v2, v2, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    #@1b7
    const-string v3, "Default Message"

    #@1b9
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1bc
    move-result v2

    #@1bd
    if-eqz v2, :cond_52

    #@1bf
    .line 377
    move-object/from16 v0, p0

    #@1c1
    iget-object v2, v0, Lcom/android/internal/telephony/cat/CatService;->mContext:Landroid/content/Context;

    #@1c3
    const v3, 0x1040534

    #@1c6
    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@1c9
    move-result-object v20

    #@1ca
    .line 378
    .restart local v20       #message:Ljava/lang/CharSequence;
    check-cast p1, Lcom/android/internal/telephony/cat/CallSetupParams;

    #@1cc
    .end local p1
    move-object/from16 v0, p1

    #@1ce
    iget-object v2, v0, Lcom/android/internal/telephony/cat/CallSetupParams;->confirmMsg:Lcom/android/internal/telephony/cat/TextMessage;

    #@1d0
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1d3
    move-result-object v3

    #@1d4
    iput-object v3, v2, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    #@1d6
    goto/16 :goto_52

    #@1d8
    .end local v20           #message:Ljava/lang/CharSequence;
    .restart local p1
    :pswitch_1d8
    move-object/from16 v18, p1

    #@1da
    .line 385
    check-cast v18, Lcom/android/internal/telephony/cat/BIPClientParams;

    #@1dc
    .line 393
    .local v18, cmd:Lcom/android/internal/telephony/cat/BIPClientParams;
    const-string v2, "persist.atel.noalpha.usrcnf"

    #@1de
    const/4 v3, 0x0

    #@1df
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@1e2
    move-result v17

    #@1e3
    .line 395
    .local v17, alphaUsrCnf:Z
    new-instance v2, Ljava/lang/StringBuilder;

    #@1e5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1e8
    const-string v3, "alphaUsrCnf: "

    #@1ea
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ed
    move-result-object v2

    #@1ee
    move/from16 v0, v17

    #@1f0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1f3
    move-result-object v2

    #@1f4
    const-string v3, ", bHasAlphaId: "

    #@1f6
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f9
    move-result-object v2

    #@1fa
    move-object/from16 v0, v18

    #@1fc
    iget-boolean v3, v0, Lcom/android/internal/telephony/cat/BIPClientParams;->bHasAlphaId:Z

    #@1fe
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@201
    move-result-object v2

    #@202
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@205
    move-result-object v2

    #@206
    move-object/from16 v0, p0

    #@208
    invoke-static {v0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@20b
    .line 397
    move-object/from16 v0, v18

    #@20d
    iget-object v2, v0, Lcom/android/internal/telephony/cat/BIPClientParams;->textMsg:Lcom/android/internal/telephony/cat/TextMessage;

    #@20f
    iget-object v2, v2, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    #@211
    if-nez v2, :cond_263

    #@213
    move-object/from16 v0, v18

    #@215
    iget-boolean v2, v0, Lcom/android/internal/telephony/cat/BIPClientParams;->bHasAlphaId:Z

    #@217
    if-nez v2, :cond_21b

    #@219
    if-nez v17, :cond_263

    #@21b
    .line 398
    :cond_21b
    new-instance v2, Ljava/lang/StringBuilder;

    #@21d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@220
    const-string v3, "cmd "

    #@222
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@225
    move-result-object v2

    #@226
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/cat/CommandParams;->getCommandType()Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@229
    move-result-object v3

    #@22a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22d
    move-result-object v2

    #@22e
    const-string v3, " with null alpha id"

    #@230
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@233
    move-result-object v2

    #@234
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@237
    move-result-object v2

    #@238
    move-object/from16 v0, p0

    #@23a
    invoke-static {v0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@23d
    .line 400
    if-eqz p2, :cond_250

    #@23f
    .line 401
    move-object/from16 v0, p1

    #@241
    iget-object v12, v0, Lcom/android/internal/telephony/cat/CommandParams;->cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;

    #@243
    sget-object v13, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    #@245
    const/4 v14, 0x0

    #@246
    const/4 v15, 0x0

    #@247
    const/16 v16, 0x0

    #@249
    move-object/from16 v11, p0

    #@24b
    invoke-direct/range {v11 .. v16}, Lcom/android/internal/telephony/cat/CatService;->sendTerminalResponse(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/ResultCode;ZILcom/android/internal/telephony/cat/ResponseData;)V

    #@24e
    goto/16 :goto_2c

    #@250
    .line 402
    :cond_250
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/cat/CommandParams;->getCommandType()Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@253
    move-result-object v2

    #@254
    sget-object v3, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->OPEN_CHANNEL:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@256
    if-ne v2, v3, :cond_2c

    #@258
    .line 403
    move-object/from16 v0, p0

    #@25a
    iget-object v2, v0, Lcom/android/internal/telephony/cat/CatService;->mCmdIf:Lcom/android/internal/telephony/CommandsInterface;

    #@25c
    const/4 v3, 0x1

    #@25d
    const/4 v5, 0x0

    #@25e
    invoke-interface {v2, v3, v5}, Lcom/android/internal/telephony/CommandsInterface;->handleCallSetupRequestFromSim(ZLandroid/os/Message;)V

    #@261
    goto/16 :goto_2c

    #@263
    .line 408
    :cond_263
    move-object/from16 v0, p0

    #@265
    iget-boolean v2, v0, Lcom/android/internal/telephony/cat/CatService;->mStkAppInstalled:Z

    #@267
    if-nez v2, :cond_283

    #@269
    .line 409
    const-string v2, "No STK application found."

    #@26b
    move-object/from16 v0, p0

    #@26d
    invoke-static {v0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@270
    .line 410
    if-eqz p2, :cond_283

    #@272
    .line 411
    move-object/from16 v0, p1

    #@274
    iget-object v12, v0, Lcom/android/internal/telephony/cat/CommandParams;->cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;

    #@276
    sget-object v13, Lcom/android/internal/telephony/cat/ResultCode;->BEYOND_TERMINAL_CAPABILITY:Lcom/android/internal/telephony/cat/ResultCode;

    #@278
    const/4 v14, 0x0

    #@279
    const/4 v15, 0x0

    #@27a
    const/16 v16, 0x0

    #@27c
    move-object/from16 v11, p0

    #@27e
    invoke-direct/range {v11 .. v16}, Lcom/android/internal/telephony/cat/CatService;->sendTerminalResponse(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/ResultCode;ZILcom/android/internal/telephony/cat/ResponseData;)V

    #@281
    goto/16 :goto_2c

    #@283
    .line 423
    :cond_283
    if-eqz p2, :cond_52

    #@285
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/cat/CommandParams;->getCommandType()Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@288
    move-result-object v2

    #@289
    sget-object v3, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->CLOSE_CHANNEL:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@28b
    if-eq v2, v3, :cond_29d

    #@28d
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/cat/CommandParams;->getCommandType()Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@290
    move-result-object v2

    #@291
    sget-object v3, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->RECEIVE_DATA:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@293
    if-eq v2, v3, :cond_29d

    #@295
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/cat/CommandParams;->getCommandType()Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@298
    move-result-object v2

    #@299
    sget-object v3, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SEND_DATA:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@29b
    if-ne v2, v3, :cond_52

    #@29d
    .line 427
    :cond_29d
    move-object/from16 v0, p1

    #@29f
    iget-object v12, v0, Lcom/android/internal/telephony/cat/CommandParams;->cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;

    #@2a1
    sget-object v13, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    #@2a3
    const/4 v14, 0x0

    #@2a4
    const/4 v15, 0x0

    #@2a5
    const/16 v16, 0x0

    #@2a7
    move-object/from16 v11, p0

    #@2a9
    invoke-direct/range {v11 .. v16}, Lcom/android/internal/telephony/cat/CatService;->sendTerminalResponse(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/ResultCode;ZILcom/android/internal/telephony/cat/ResponseData;)V

    #@2ac
    goto/16 :goto_52

    #@2ae
    .line 433
    .end local v17           #alphaUsrCnf:Z
    .end local v18           #cmd:Lcom/android/internal/telephony/cat/BIPClientParams;
    :pswitch_2ae
    const-string v2, "Received ACTIVATE cmd"

    #@2b0
    move-object/from16 v0, p0

    #@2b2
    invoke-static {v0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@2b5
    .line 434
    move-object/from16 v0, p1

    #@2b7
    iget-object v12, v0, Lcom/android/internal/telephony/cat/CommandParams;->cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;

    #@2b9
    sget-object v13, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    #@2bb
    const/4 v14, 0x0

    #@2bc
    const/4 v15, 0x0

    #@2bd
    const/16 v16, 0x0

    #@2bf
    move-object/from16 v11, p0

    #@2c1
    invoke-direct/range {v11 .. v16}, Lcom/android/internal/telephony/cat/CatService;->sendTerminalResponse(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/ResultCode;ZILcom/android/internal/telephony/cat/ResponseData;)V

    #@2c4
    goto/16 :goto_52

    #@2c6
    .line 295
    :pswitch_data_2c6
    .packed-switch 0x1
        :pswitch_2d
        :pswitch_85
        :pswitch_a5
        :pswitch_b2
        :pswitch_ca
        :pswitch_f4
        :pswitch_13f
        :pswitch_52
        :pswitch_52
        :pswitch_52
        :pswitch_172
        :pswitch_172
        :pswitch_172
        :pswitch_172
        :pswitch_52
        :pswitch_1a5
        :pswitch_1d8
        :pswitch_1d8
        :pswitch_1d8
        :pswitch_1d8
        :pswitch_2ae
    .end packed-switch

    #@2f4
    .line 337
    :pswitch_data_2f4
    .packed-switch 0x3
        :pswitch_10e
        :pswitch_123
    .end packed-switch
.end method

.method private handleRilMsg(Lcom/android/internal/telephony/cat/RilMessage;)V
    .registers 11
    .parameter "rilMsg"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v4, 0x0

    #@2
    .line 199
    if-nez p1, :cond_5

    #@4
    .line 253
    :cond_4
    :goto_4
    return-void

    #@5
    .line 204
    :cond_5
    const/4 v7, 0x0

    #@6
    .line 205
    .local v7, cmdParams:Lcom/android/internal/telephony/cat/CommandParams;
    iget v1, p1, Lcom/android/internal/telephony/cat/RilMessage;->mId:I

    #@8
    packed-switch v1, :pswitch_data_5e

    #@b
    :pswitch_b
    goto :goto_4

    #@c
    .line 246
    :pswitch_c
    invoke-direct {p0}, Lcom/android/internal/telephony/cat/CatService;->handleSessionEnd()V

    #@f
    goto :goto_4

    #@10
    .line 207
    :pswitch_10
    iget-object v1, p1, Lcom/android/internal/telephony/cat/RilMessage;->mResCode:Lcom/android/internal/telephony/cat/ResultCode;

    #@12
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    #@14
    if-ne v1, v2, :cond_4

    #@16
    .line 208
    iget-object v7, p1, Lcom/android/internal/telephony/cat/RilMessage;->mData:Ljava/lang/Object;

    #@18
    .end local v7           #cmdParams:Lcom/android/internal/telephony/cat/CommandParams;
    check-cast v7, Lcom/android/internal/telephony/cat/CommandParams;

    #@1a
    .line 209
    .restart local v7       #cmdParams:Lcom/android/internal/telephony/cat/CommandParams;
    if-eqz v7, :cond_4

    #@1c
    .line 210
    invoke-direct {p0, v7, v4}, Lcom/android/internal/telephony/cat/CatService;->handleCommand(Lcom/android/internal/telephony/cat/CommandParams;Z)V

    #@1f
    goto :goto_4

    #@20
    .line 216
    :pswitch_20
    :try_start_20
    iget-object v1, p1, Lcom/android/internal/telephony/cat/RilMessage;->mData:Ljava/lang/Object;

    #@22
    move-object v0, v1

    #@23
    check-cast v0, Lcom/android/internal/telephony/cat/CommandParams;

    #@25
    move-object v7, v0
    :try_end_26
    .catch Ljava/lang/ClassCastException; {:try_start_20 .. :try_end_26} :catch_33

    #@26
    .line 227
    if-eqz v7, :cond_4

    #@28
    .line 228
    iget-object v1, p1, Lcom/android/internal/telephony/cat/RilMessage;->mResCode:Lcom/android/internal/telephony/cat/ResultCode;

    #@2a
    sget-object v2, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    #@2c
    if-ne v1, v2, :cond_49

    #@2e
    .line 229
    const/4 v1, 0x1

    #@2f
    invoke-direct {p0, v7, v1}, Lcom/android/internal/telephony/cat/CatService;->handleCommand(Lcom/android/internal/telephony/cat/CommandParams;Z)V

    #@32
    goto :goto_4

    #@33
    .line 217
    :catch_33
    move-exception v8

    #@34
    .line 219
    .local v8, e:Ljava/lang/ClassCastException;
    const-string v1, "Fail to parse proactive command"

    #@36
    invoke-static {p0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@39
    .line 221
    iget-object v1, p0, Lcom/android/internal/telephony/cat/CatService;->mCurrntCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    #@3b
    if-eqz v1, :cond_4

    #@3d
    .line 222
    iget-object v1, p0, Lcom/android/internal/telephony/cat/CatService;->mCurrntCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    #@3f
    iget-object v2, v1, Lcom/android/internal/telephony/cat/CatCmdMessage;->mCmdDet:Lcom/android/internal/telephony/cat/CommandDetails;

    #@41
    sget-object v3, Lcom/android/internal/telephony/cat/ResultCode;->CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@43
    move-object v1, p0

    #@44
    move v5, v4

    #@45
    invoke-direct/range {v1 .. v6}, Lcom/android/internal/telephony/cat/CatService;->sendTerminalResponse(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/ResultCode;ZILcom/android/internal/telephony/cat/ResponseData;)V

    #@48
    goto :goto_4

    #@49
    .line 234
    .end local v8           #e:Ljava/lang/ClassCastException;
    :cond_49
    iget-object v2, v7, Lcom/android/internal/telephony/cat/CommandParams;->cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;

    #@4b
    iget-object v3, p1, Lcom/android/internal/telephony/cat/RilMessage;->mResCode:Lcom/android/internal/telephony/cat/ResultCode;

    #@4d
    move-object v1, p0

    #@4e
    move v5, v4

    #@4f
    invoke-direct/range {v1 .. v6}, Lcom/android/internal/telephony/cat/CatService;->sendTerminalResponse(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/ResultCode;ZILcom/android/internal/telephony/cat/ResponseData;)V

    #@52
    goto :goto_4

    #@53
    .line 240
    :pswitch_53
    iget-object v7, p1, Lcom/android/internal/telephony/cat/RilMessage;->mData:Ljava/lang/Object;

    #@55
    .end local v7           #cmdParams:Lcom/android/internal/telephony/cat/CommandParams;
    check-cast v7, Lcom/android/internal/telephony/cat/CommandParams;

    #@57
    .line 241
    .restart local v7       #cmdParams:Lcom/android/internal/telephony/cat/CommandParams;
    if-eqz v7, :cond_4

    #@59
    .line 242
    invoke-direct {p0, v7, v4}, Lcom/android/internal/telephony/cat/CatService;->handleCommand(Lcom/android/internal/telephony/cat/CommandParams;Z)V

    #@5c
    goto :goto_4

    #@5d
    .line 205
    nop

    #@5e
    :pswitch_data_5e
    .packed-switch 0x1
        :pswitch_c
        :pswitch_20
        :pswitch_10
        :pswitch_b
        :pswitch_53
    .end packed-switch
.end method

.method private handleSessionEnd()V
    .registers 3

    #@0
    .prologue
    .line 457
    const-string v1, "SESSION END"

    #@2
    invoke-static {p0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@5
    .line 459
    iget-object v1, p0, Lcom/android/internal/telephony/cat/CatService;->mMenuCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    #@7
    iput-object v1, p0, Lcom/android/internal/telephony/cat/CatService;->mCurrntCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    #@9
    .line 460
    new-instance v0, Landroid/content/Intent;

    #@b
    const-string v1, "android.intent.action.stk.session_end"

    #@d
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@10
    .line 461
    .local v0, intent:Landroid/content/Intent;
    iget-object v1, p0, Lcom/android/internal/telephony/cat/CatService;->mContext:Landroid/content/Context;

    #@12
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@15
    .line 462
    return-void
.end method

.method private isSupportedSetupEventCommand(Lcom/android/internal/telephony/cat/CatCmdMessage;)Z
    .registers 7
    .parameter "cmdMsg"

    #@0
    .prologue
    .line 261
    const/4 v1, 0x1

    #@1
    .line 264
    .local v1, flag:Z
    const/4 v2, 0x0

    #@2
    .local v2, i:I
    :goto_2
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getSetEventList()Lcom/android/internal/telephony/cat/CatCmdMessage$SetupEventListSettings;

    #@5
    move-result-object v3

    #@6
    iget-object v3, v3, Lcom/android/internal/telephony/cat/CatCmdMessage$SetupEventListSettings;->eventList:[I

    #@8
    array-length v3, v3

    #@9
    if-ge v2, v3, :cond_30

    #@b
    .line 265
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getSetEventList()Lcom/android/internal/telephony/cat/CatCmdMessage$SetupEventListSettings;

    #@e
    move-result-object v3

    #@f
    iget-object v3, v3, Lcom/android/internal/telephony/cat/CatCmdMessage$SetupEventListSettings;->eventList:[I

    #@11
    aget v0, v3, v2

    #@13
    .line 266
    .local v0, eventval:I
    new-instance v3, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v4, "Event: "

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    invoke-static {p0, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@29
    .line 267
    packed-switch v0, :pswitch_data_32

    #@2c
    .line 276
    :pswitch_2c
    const/4 v1, 0x0

    #@2d
    .line 264
    :pswitch_2d
    add-int/lit8 v2, v2, 0x1

    #@2f
    goto :goto_2

    #@30
    .line 279
    .end local v0           #eventval:I
    :cond_30
    return v1

    #@31
    .line 267
    nop

    #@32
    :pswitch_data_32
    .packed-switch 0x5
        :pswitch_2d
        :pswitch_2c
        :pswitch_2d
    .end packed-switch
.end method

.method private removeMenu(Lcom/android/internal/telephony/cat/Menu;)Z
    .registers 7
    .parameter "menu"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 912
    :try_start_2
    iget-object v3, p1, Lcom/android/internal/telephony/cat/Menu;->items:Ljava/util/List;

    #@4
    invoke-interface {v3}, Ljava/util/List;->size()I

    #@7
    move-result v3

    #@8
    if-ne v3, v1, :cond_1b

    #@a
    iget-object v3, p1, Lcom/android/internal/telephony/cat/Menu;->items:Ljava/util/List;

    #@c
    const/4 v4, 0x0

    #@d
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;
    :try_end_10
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_10} :catch_14

    #@10
    move-result-object v3

    #@11
    if-nez v3, :cond_1b

    #@13
    .line 919
    :goto_13
    return v1

    #@14
    .line 915
    :catch_14
    move-exception v0

    #@15
    .line 916
    .local v0, e:Ljava/lang/NullPointerException;
    const-string v2, "Unable to get Menu\'s items size"

    #@17
    invoke-static {p0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@1a
    goto :goto_13

    #@1b
    .end local v0           #e:Ljava/lang/NullPointerException;
    :cond_1b
    move v1, v2

    #@1c
    .line 919
    goto :goto_13
.end method

.method private sendMenuSelection(IZ)V
    .registers 11
    .parameter "menuId"
    .parameter "helpRequired"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 594
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    #@4
    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@7
    .line 597
    .local v0, buf:Ljava/io/ByteArrayOutputStream;
    const/16 v4, 0xd3

    #@9
    .line 598
    .local v4, tag:I
    invoke-virtual {v0, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@c
    .line 601
    invoke-virtual {v0, v7}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@f
    .line 604
    sget-object v5, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->DEVICE_IDENTITIES:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@11
    invoke-virtual {v5}, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->value()I

    #@14
    move-result v5

    #@15
    or-int/lit16 v4, v5, 0x80

    #@17
    .line 605
    invoke-virtual {v0, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@1a
    .line 606
    const/4 v5, 0x2

    #@1b
    invoke-virtual {v0, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@1e
    .line 607
    invoke-virtual {v0, v6}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@21
    .line 608
    const/16 v5, 0x81

    #@23
    invoke-virtual {v0, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@26
    .line 611
    sget-object v5, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->ITEM_ID:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@28
    invoke-virtual {v5}, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->value()I

    #@2b
    move-result v5

    #@2c
    or-int/lit16 v4, v5, 0x80

    #@2e
    .line 612
    invoke-virtual {v0, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@31
    .line 613
    invoke-virtual {v0, v6}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@34
    .line 614
    invoke-virtual {v0, p1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@37
    .line 617
    if-eqz p2, :cond_45

    #@39
    .line 618
    sget-object v5, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->HELP_REQUEST:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@3b
    invoke-virtual {v5}, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->value()I

    #@3e
    move-result v4

    #@3f
    .line 619
    invoke-virtual {v0, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@42
    .line 620
    invoke-virtual {v0, v7}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@45
    .line 623
    :cond_45
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@48
    move-result-object v3

    #@49
    .line 626
    .local v3, rawData:[B
    array-length v5, v3

    #@4a
    add-int/lit8 v2, v5, -0x2

    #@4c
    .line 627
    .local v2, len:I
    int-to-byte v5, v2

    #@4d
    aput-byte v5, v3, v6

    #@4f
    .line 629
    invoke-static {v3}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@52
    move-result-object v1

    #@53
    .line 631
    .local v1, hexString:Ljava/lang/String;
    iget-object v5, p0, Lcom/android/internal/telephony/cat/CatService;->mCmdIf:Lcom/android/internal/telephony/CommandsInterface;

    #@55
    const/4 v6, 0x0

    #@56
    invoke-interface {v5, v1, v6}, Lcom/android/internal/telephony/CommandsInterface;->sendEnvelope(Ljava/lang/String;Landroid/os/Message;)V

    #@59
    .line 632
    return-void
.end method

.method private sendTerminalResponse(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/ResultCode;ZILcom/android/internal/telephony/cat/ResponseData;)V
    .registers 14
    .parameter "cmdDet"
    .parameter "resultCode"
    .parameter "includeAdditionalInfo"
    .parameter "additionalInfo"
    .parameter "resp"

    #@0
    .prologue
    const/4 v3, 0x2

    #@1
    .line 468
    if-nez p1, :cond_4

    #@3
    .line 538
    :goto_3
    return-void

    #@4
    .line 471
    :cond_4
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    #@6
    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@9
    .line 473
    .local v0, buf:Ljava/io/ByteArrayOutputStream;
    const/4 v1, 0x0

    #@a
    .line 474
    .local v1, cmdInput:Lcom/android/internal/telephony/cat/Input;
    iget-object v6, p0, Lcom/android/internal/telephony/cat/CatService;->mCurrntCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    #@c
    if-eqz v6, :cond_14

    #@e
    .line 475
    iget-object v6, p0, Lcom/android/internal/telephony/cat/CatService;->mCurrntCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    #@10
    invoke-virtual {v6}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geInput()Lcom/android/internal/telephony/cat/Input;

    #@13
    move-result-object v1

    #@14
    .line 479
    :cond_14
    sget-object v6, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->COMMAND_DETAILS:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@16
    invoke-virtual {v6}, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->value()I

    #@19
    move-result v5

    #@1a
    .line 480
    .local v5, tag:I
    iget-boolean v6, p1, Lcom/android/internal/telephony/cat/CommandDetails;->compRequired:Z

    #@1c
    if-eqz v6, :cond_20

    #@1e
    .line 481
    or-int/lit16 v5, v5, 0x80

    #@20
    .line 483
    :cond_20
    invoke-virtual {v0, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@23
    .line 484
    const/4 v6, 0x3

    #@24
    invoke-virtual {v0, v6}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@27
    .line 485
    iget v6, p1, Lcom/android/internal/telephony/cat/CommandDetails;->commandNumber:I

    #@29
    invoke-virtual {v0, v6}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@2c
    .line 486
    iget v6, p1, Lcom/android/internal/telephony/cat/CommandDetails;->typeOfCommand:I

    #@2e
    invoke-virtual {v0, v6}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@31
    .line 487
    iget v6, p1, Lcom/android/internal/telephony/cat/CommandDetails;->commandQualifier:I

    #@33
    invoke-virtual {v0, v6}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@36
    .line 496
    sget-object v6, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->DEVICE_IDENTITIES:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@38
    invoke-virtual {v6}, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->value()I

    #@3b
    move-result v5

    #@3c
    .line 497
    invoke-virtual {v0, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@3f
    .line 498
    invoke-virtual {v0, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@42
    .line 499
    const/16 v6, 0x82

    #@44
    invoke-virtual {v0, v6}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@47
    .line 500
    const/16 v6, 0x81

    #@49
    invoke-virtual {v0, v6}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@4c
    .line 503
    sget-object v6, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->RESULT:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@4e
    invoke-virtual {v6}, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->value()I

    #@51
    move-result v5

    #@52
    .line 504
    iget-boolean v6, p1, Lcom/android/internal/telephony/cat/CommandDetails;->compRequired:Z

    #@54
    if-eqz v6, :cond_58

    #@56
    .line 505
    or-int/lit16 v5, v5, 0x80

    #@58
    .line 507
    :cond_58
    invoke-virtual {v0, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@5b
    .line 508
    if-eqz p3, :cond_80

    #@5d
    .line 509
    .local v3, length:I
    :goto_5d
    invoke-virtual {v0, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@60
    .line 510
    invoke-virtual {p2}, Lcom/android/internal/telephony/cat/ResultCode;->value()I

    #@63
    move-result v6

    #@64
    invoke-virtual {v0, v6}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@67
    .line 513
    if-eqz p3, :cond_6c

    #@69
    .line 514
    invoke-virtual {v0, p4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@6c
    .line 518
    :cond_6c
    if-eqz p5, :cond_82

    #@6e
    .line 519
    invoke-virtual {p5, v0}, Lcom/android/internal/telephony/cat/ResponseData;->format(Ljava/io/ByteArrayOutputStream;)V

    #@71
    .line 524
    :goto_71
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@74
    move-result-object v4

    #@75
    .line 525
    .local v4, rawData:[B
    invoke-static {v4}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@78
    move-result-object v2

    #@79
    .line 537
    .local v2, hexString:Ljava/lang/String;
    iget-object v6, p0, Lcom/android/internal/telephony/cat/CatService;->mCmdIf:Lcom/android/internal/telephony/CommandsInterface;

    #@7b
    const/4 v7, 0x0

    #@7c
    invoke-interface {v6, v2, v7}, Lcom/android/internal/telephony/CommandsInterface;->sendTerminalResponse(Ljava/lang/String;Landroid/os/Message;)V

    #@7f
    goto :goto_3

    #@80
    .line 508
    .end local v2           #hexString:Ljava/lang/String;
    .end local v3           #length:I
    .end local v4           #rawData:[B
    :cond_80
    const/4 v3, 0x1

    #@81
    goto :goto_5d

    #@82
    .line 521
    .restart local v3       #length:I
    :cond_82
    invoke-direct {p0, p1, p2, v1, v0}, Lcom/android/internal/telephony/cat/CatService;->encodeOptionalTags(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/ResultCode;Lcom/android/internal/telephony/cat/Input;Ljava/io/ByteArrayOutputStream;)V

    #@85
    goto :goto_71
.end method

.method private validateResponse(Lcom/android/internal/telephony/cat/CatResponseMessage;)Z
    .registers 5
    .parameter "resMsg"

    #@0
    .prologue
    .line 898
    const/4 v0, 0x0

    #@1
    .line 899
    .local v0, validResponse:Z
    iget-object v1, p1, Lcom/android/internal/telephony/cat/CatResponseMessage;->cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;

    #@3
    iget v1, v1, Lcom/android/internal/telephony/cat/CommandDetails;->typeOfCommand:I

    #@5
    sget-object v2, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SET_UP_EVENT_LIST:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@7
    invoke-virtual {v2}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->value()I

    #@a
    move-result v2

    #@b
    if-eq v1, v2, :cond_19

    #@d
    iget-object v1, p1, Lcom/android/internal/telephony/cat/CatResponseMessage;->cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;

    #@f
    iget v1, v1, Lcom/android/internal/telephony/cat/CommandDetails;->typeOfCommand:I

    #@11
    sget-object v2, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SET_UP_MENU:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@13
    invoke-virtual {v2}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->value()I

    #@16
    move-result v2

    #@17
    if-ne v1, v2, :cond_35

    #@19
    .line 901
    :cond_19
    new-instance v1, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v2, "CmdType: "

    #@20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    iget-object v2, p1, Lcom/android/internal/telephony/cat/CatResponseMessage;->cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;

    #@26
    iget v2, v2, Lcom/android/internal/telephony/cat/CommandDetails;->typeOfCommand:I

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    invoke-static {p0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@33
    .line 902
    const/4 v0, 0x1

    #@34
    .line 907
    :cond_34
    :goto_34
    return v0

    #@35
    .line 903
    :cond_35
    iget-object v1, p0, Lcom/android/internal/telephony/cat/CatService;->mCurrntCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    #@37
    if-eqz v1, :cond_34

    #@39
    .line 904
    iget-object v1, p1, Lcom/android/internal/telephony/cat/CatResponseMessage;->cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;

    #@3b
    iget-object v2, p0, Lcom/android/internal/telephony/cat/CatService;->mCurrntCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    #@3d
    iget-object v2, v2, Lcom/android/internal/telephony/cat/CatCmdMessage;->mCmdDet:Lcom/android/internal/telephony/cat/CommandDetails;

    #@3f
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/cat/CommandDetails;->compareTo(Lcom/android/internal/telephony/cat/CommandDetails;)Z

    #@42
    move-result v0

    #@43
    .line 905
    new-instance v1, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v2, "isResponse for last valid cmd: "

    #@4a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v1

    #@4e
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@51
    move-result-object v1

    #@52
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v1

    #@56
    invoke-static {p0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@59
    goto :goto_34
.end method


# virtual methods
.method protected broadcastCatCmdIntent(Lcom/android/internal/telephony/cat/CatCmdMessage;)V
    .registers 4
    .parameter "cmdMsg"

    #@0
    .prologue
    .line 447
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.intent.action.stk.command"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 448
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "STK CMD"

    #@9
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@c
    .line 449
    iget-object v1, p0, Lcom/android/internal/telephony/cat/CatService;->mContext:Landroid/content/Context;

    #@e
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@11
    .line 450
    return-void
.end method

.method public dispose()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 172
    sget-object v0, Lcom/android/internal/telephony/cat/CatService;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@3
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/uicc/IccRecords;->unregisterForRecordsLoaded(Landroid/os/Handler;)V

    #@6
    .line 175
    sget-object v0, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_ABSENT:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@8
    invoke-direct {p0, v0, v1}, Lcom/android/internal/telephony/cat/CatService;->broadcastCardStateAndIccRefreshResp(Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;Lcom/android/internal/telephony/uicc/IccRefreshResponse;)V

    #@b
    .line 177
    iget-object v0, p0, Lcom/android/internal/telephony/cat/CatService;->mCmdIf:Lcom/android/internal/telephony/CommandsInterface;

    #@d
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unSetOnCatSessionEnd(Landroid/os/Handler;)V

    #@10
    .line 178
    iget-object v0, p0, Lcom/android/internal/telephony/cat/CatService;->mCmdIf:Lcom/android/internal/telephony/CommandsInterface;

    #@12
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unSetOnCatProactiveCmd(Landroid/os/Handler;)V

    #@15
    .line 179
    iget-object v0, p0, Lcom/android/internal/telephony/cat/CatService;->mCmdIf:Lcom/android/internal/telephony/CommandsInterface;

    #@17
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unSetOnCatEvent(Landroid/os/Handler;)V

    #@1a
    .line 180
    iget-object v0, p0, Lcom/android/internal/telephony/cat/CatService;->mCmdIf:Lcom/android/internal/telephony/CommandsInterface;

    #@1c
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unSetOnCatCallSetUp(Landroid/os/Handler;)V

    #@1f
    .line 181
    iget-object v0, p0, Lcom/android/internal/telephony/cat/CatService;->mCmdIf:Lcom/android/internal/telephony/CommandsInterface;

    #@21
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForIccRefresh(Landroid/os/Handler;)V

    #@24
    .line 182
    iget-object v0, p0, Lcom/android/internal/telephony/cat/CatService;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@26
    if-eqz v0, :cond_2f

    #@28
    .line 183
    iget-object v0, p0, Lcom/android/internal/telephony/cat/CatService;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@2a
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/uicc/UiccController;->unregisterForIccChanged(Landroid/os/Handler;)V

    #@2d
    .line 184
    iput-object v1, p0, Lcom/android/internal/telephony/cat/CatService;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@2f
    .line 186
    :cond_2f
    sput-object v1, Lcom/android/internal/telephony/cat/CatService;->sInstance:Lcom/android/internal/telephony/cat/CatService;

    #@31
    .line 187
    sget-object v0, Lcom/android/internal/telephony/cat/CatService;->mhandlerThread:Landroid/os/HandlerThread;

    #@33
    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    #@36
    .line 188
    sput-object v1, Lcom/android/internal/telephony/cat/CatService;->mhandlerThread:Landroid/os/HandlerThread;

    #@38
    .line 189
    iget-object v0, p0, Lcom/android/internal/telephony/cat/CatService;->mCmdIf:Lcom/android/internal/telephony/CommandsInterface;

    #@3a
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unSetOnCatCcAlphaNotify(Landroid/os/Handler;)V

    #@3d
    .line 191
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cat/CatService;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    #@40
    .line 192
    return-void
.end method

.method protected finalize()V
    .registers 2

    #@0
    .prologue
    .line 195
    const-string v0, "Service finalized"

    #@2
    invoke-static {p0, v0}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@5
    .line 196
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 9
    .parameter "msg"

    #@0
    .prologue
    .line 788
    iget v3, p1, Landroid/os/Message;->what:I

    #@2
    sparse-switch v3, :sswitch_data_ce

    #@5
    .line 851
    new-instance v3, Ljava/lang/AssertionError;

    #@7
    new-instance v4, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v5, "Unrecognized CAT command: "

    #@e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v4

    #@12
    iget v5, p1, Landroid/os/Message;->what:I

    #@14
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v4

    #@1c
    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    #@1f
    throw v3

    #@20
    .line 793
    :sswitch_20
    const-string v3, "ril message arrived"

    #@22
    invoke-static {p0, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@25
    .line 794
    const/4 v1, 0x0

    #@26
    .line 795
    .local v1, data:Ljava/lang/String;
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@28
    if-eqz v3, :cond_38

    #@2a
    .line 796
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2c
    check-cast v0, Landroid/os/AsyncResult;

    #@2e
    .line 797
    .local v0, ar:Landroid/os/AsyncResult;
    if-eqz v0, :cond_38

    #@30
    iget-object v3, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@32
    if-eqz v3, :cond_38

    #@34
    .line 799
    :try_start_34
    iget-object v1, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@36
    .end local v1           #data:Ljava/lang/String;
    check-cast v1, Ljava/lang/String;
    :try_end_38
    .catch Ljava/lang/ClassCastException; {:try_start_34 .. :try_end_38} :catch_45

    #@38
    .line 805
    .end local v0           #ar:Landroid/os/AsyncResult;
    .restart local v1       #data:Ljava/lang/String;
    :cond_38
    iget-object v3, p0, Lcom/android/internal/telephony/cat/CatService;->mMsgDecoder:Lcom/android/internal/telephony/cat/RilMessageDecoder;

    #@3a
    new-instance v4, Lcom/android/internal/telephony/cat/RilMessage;

    #@3c
    iget v5, p1, Landroid/os/Message;->what:I

    #@3e
    invoke-direct {v4, v5, v1}, Lcom/android/internal/telephony/cat/RilMessage;-><init>(ILjava/lang/String;)V

    #@41
    invoke-virtual {v3, v4}, Lcom/android/internal/telephony/cat/RilMessageDecoder;->sendStartDecodingMessageParams(Lcom/android/internal/telephony/cat/RilMessage;)V

    #@44
    .line 853
    .end local v1           #data:Ljava/lang/String;
    :goto_44
    :sswitch_44
    return-void

    #@45
    .line 800
    .restart local v0       #ar:Landroid/os/AsyncResult;
    :catch_45
    move-exception v2

    #@46
    .line 801
    .local v2, e:Ljava/lang/ClassCastException;
    goto :goto_44

    #@47
    .line 808
    .end local v0           #ar:Landroid/os/AsyncResult;
    .end local v2           #e:Ljava/lang/ClassCastException;
    :sswitch_47
    iget-object v3, p0, Lcom/android/internal/telephony/cat/CatService;->mMsgDecoder:Lcom/android/internal/telephony/cat/RilMessageDecoder;

    #@49
    new-instance v4, Lcom/android/internal/telephony/cat/RilMessage;

    #@4b
    iget v5, p1, Landroid/os/Message;->what:I

    #@4d
    const/4 v6, 0x0

    #@4e
    invoke-direct {v4, v5, v6}, Lcom/android/internal/telephony/cat/RilMessage;-><init>(ILjava/lang/String;)V

    #@51
    invoke-virtual {v3, v4}, Lcom/android/internal/telephony/cat/RilMessageDecoder;->sendStartDecodingMessageParams(Lcom/android/internal/telephony/cat/RilMessage;)V

    #@54
    goto :goto_44

    #@55
    .line 813
    :sswitch_55
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@57
    check-cast v3, Lcom/android/internal/telephony/cat/RilMessage;

    #@59
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/cat/CatService;->handleRilMsg(Lcom/android/internal/telephony/cat/RilMessage;)V

    #@5c
    goto :goto_44

    #@5d
    .line 816
    :sswitch_5d
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@5f
    check-cast v3, Lcom/android/internal/telephony/cat/CatResponseMessage;

    #@61
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/cat/CatService;->handleCmdResponse(Lcom/android/internal/telephony/cat/CatResponseMessage;)V

    #@64
    goto :goto_44

    #@65
    .line 821
    :sswitch_65
    const-string v3, "MSG_ID_ICC_CHANGED"

    #@67
    invoke-static {p0, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@6a
    .line 822
    invoke-virtual {p0}, Lcom/android/internal/telephony/cat/CatService;->updateIccAvailability()V

    #@6d
    goto :goto_44

    #@6e
    .line 825
    :sswitch_6e
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@70
    if-eqz v3, :cond_9f

    #@72
    .line 826
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@74
    check-cast v0, Landroid/os/AsyncResult;

    #@76
    .line 827
    .restart local v0       #ar:Landroid/os/AsyncResult;
    if-eqz v0, :cond_86

    #@78
    iget-object v3, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@7a
    if-eqz v3, :cond_86

    #@7c
    .line 828
    sget-object v4, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_PRESENT:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@7e
    iget-object v3, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@80
    check-cast v3, Lcom/android/internal/telephony/uicc/IccRefreshResponse;

    #@82
    invoke-direct {p0, v4, v3}, Lcom/android/internal/telephony/cat/CatService;->broadcastCardStateAndIccRefreshResp(Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;Lcom/android/internal/telephony/uicc/IccRefreshResponse;)V

    #@85
    goto :goto_44

    #@86
    .line 831
    :cond_86
    new-instance v3, Ljava/lang/StringBuilder;

    #@88
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@8b
    const-string v4, "Icc REFRESH with exception: "

    #@8d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v3

    #@91
    iget-object v4, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@93
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v3

    #@97
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9a
    move-result-object v3

    #@9b
    invoke-static {p0, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@9e
    goto :goto_44

    #@9f
    .line 834
    .end local v0           #ar:Landroid/os/AsyncResult;
    :cond_9f
    const-string v3, "IccRefresh Message is null"

    #@a1
    invoke-static {p0, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@a4
    goto :goto_44

    #@a5
    .line 838
    :sswitch_a5
    const-string v3, "Received STK CC Alpha message from card"

    #@a7
    invoke-static {p0, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@aa
    .line 839
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@ac
    if-eqz v3, :cond_c7

    #@ae
    .line 840
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@b0
    check-cast v0, Landroid/os/AsyncResult;

    #@b2
    .line 841
    .restart local v0       #ar:Landroid/os/AsyncResult;
    if-eqz v0, :cond_c0

    #@b4
    iget-object v3, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@b6
    if-eqz v3, :cond_c0

    #@b8
    .line 842
    iget-object v3, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@ba
    check-cast v3, Ljava/lang/String;

    #@bc
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/cat/CatService;->broadcastAlphaMessage(Ljava/lang/String;)V

    #@bf
    goto :goto_44

    #@c0
    .line 844
    :cond_c0
    const-string v3, "STK Alpha message: ar.result is null"

    #@c2
    invoke-static {p0, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@c5
    goto/16 :goto_44

    #@c7
    .line 847
    .end local v0           #ar:Landroid/os/AsyncResult;
    :cond_c7
    const-string v3, "STK Alpha message: msg.obj is null"

    #@c9
    invoke-static {p0, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@cc
    goto/16 :goto_44

    #@ce
    .line 788
    :sswitch_data_ce
    .sparse-switch
        0x1 -> :sswitch_20
        0x2 -> :sswitch_20
        0x3 -> :sswitch_20
        0x4 -> :sswitch_47
        0x5 -> :sswitch_20
        0x6 -> :sswitch_5d
        0x7 -> :sswitch_44
        0x8 -> :sswitch_a5
        0xa -> :sswitch_55
        0x14 -> :sswitch_44
        0x15 -> :sswitch_65
        0x1e -> :sswitch_6e
    .end sparse-switch
.end method

.method protected isStkAppInstalled()Z
    .registers 7

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1041
    new-instance v1, Landroid/content/Intent;

    #@3
    const-string v5, "android.intent.action.stk.command"

    #@5
    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@8
    .line 1042
    .local v1, intent:Landroid/content/Intent;
    iget-object v5, p0, Lcom/android/internal/telephony/cat/CatService;->mContext:Landroid/content/Context;

    #@a
    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@d
    move-result-object v3

    #@e
    .line 1043
    .local v3, pm:Landroid/content/pm/PackageManager;
    const/16 v5, 0x80

    #@10
    invoke-virtual {v3, v1, v5}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    #@13
    move-result-object v0

    #@14
    .line 1045
    .local v0, broadcastReceivers:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-nez v0, :cond_1b

    #@16
    move v2, v4

    #@17
    .line 1047
    .local v2, numReceiver:I
    :goto_17
    if-lez v2, :cond_1a

    #@19
    const/4 v4, 0x1

    #@1a
    :cond_1a
    return v4

    #@1b
    .line 1045
    .end local v2           #numReceiver:I
    :cond_1b
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@1e
    move-result v2

    #@1f
    goto :goto_17
.end method

.method public declared-synchronized onCmdResponse(Lcom/android/internal/telephony/cat/CatResponseMessage;)V
    .registers 4
    .parameter "resMsg"

    #@0
    .prologue
    .line 889
    monitor-enter p0

    #@1
    if-nez p1, :cond_5

    #@3
    .line 895
    :goto_3
    monitor-exit p0

    #@4
    return-void

    #@5
    .line 893
    :cond_5
    const/4 v1, 0x6

    #@6
    :try_start_6
    invoke-virtual {p0, v1, p1}, Lcom/android/internal/telephony/cat/CatService;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@9
    move-result-object v0

    #@a
    .line 894
    .local v0, msg:Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_d
    .catchall {:try_start_6 .. :try_end_d} :catchall_e

    #@d
    goto :goto_3

    #@e
    .line 889
    .end local v0           #msg:Landroid/os/Message;
    :catchall_e
    move-exception v1

    #@f
    monitor-exit p0

    #@10
    throw v1
.end method

.method updateIccAvailability()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1051
    sget-object v1, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_ABSENT:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@3
    .line 1052
    .local v1, newState:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;
    iget-object v3, p0, Lcom/android/internal/telephony/cat/CatService;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@5
    if-nez v3, :cond_8

    #@7
    .line 1071
    :cond_7
    :goto_7
    return-void

    #@8
    .line 1055
    :cond_8
    iget-object v3, p0, Lcom/android/internal/telephony/cat/CatService;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@a
    invoke-virtual {v3}, Lcom/android/internal/telephony/uicc/UiccController;->getUiccCard()Lcom/android/internal/telephony/uicc/UiccCard;

    #@d
    move-result-object v0

    #@e
    .line 1056
    .local v0, newCard:Lcom/android/internal/telephony/uicc/UiccCard;
    if-eqz v0, :cond_14

    #@10
    .line 1057
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/UiccCard;->getCardState()Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@13
    move-result-object v1

    #@14
    .line 1059
    :cond_14
    iget-object v2, p0, Lcom/android/internal/telephony/cat/CatService;->mCardState:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@16
    .line 1060
    .local v2, oldState:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;
    iput-object v1, p0, Lcom/android/internal/telephony/cat/CatService;->mCardState:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@18
    .line 1061
    new-instance v3, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v4, "New Card State = "

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    const-string v4, " "

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    const-string v4, "Old Card State = "

    #@2f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v3

    #@3b
    invoke-static {p0, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@3e
    .line 1062
    sget-object v3, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_PRESENT:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@40
    if-ne v2, v3, :cond_4a

    #@42
    sget-object v3, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_PRESENT:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@44
    if-eq v1, v3, :cond_4a

    #@46
    .line 1064
    invoke-direct {p0, v1, v5}, Lcom/android/internal/telephony/cat/CatService;->broadcastCardStateAndIccRefreshResp(Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;Lcom/android/internal/telephony/uicc/IccRefreshResponse;)V

    #@49
    goto :goto_7

    #@4a
    .line 1065
    :cond_4a
    sget-object v3, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_PRESENT:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@4c
    if-eq v2, v3, :cond_7

    #@4e
    sget-object v3, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_PRESENT:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@50
    if-ne v1, v3, :cond_7

    #@52
    .line 1068
    iget-object v3, p0, Lcom/android/internal/telephony/cat/CatService;->mCmdIf:Lcom/android/internal/telephony/CommandsInterface;

    #@54
    invoke-interface {v3, v5}, Lcom/android/internal/telephony/CommandsInterface;->reportStkServiceIsRunning(Landroid/os/Message;)V

    #@57
    goto :goto_7
.end method
