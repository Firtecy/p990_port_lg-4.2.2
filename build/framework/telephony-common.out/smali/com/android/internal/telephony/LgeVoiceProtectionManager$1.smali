.class Lcom/android/internal/telephony/LgeVoiceProtectionManager$1;
.super Landroid/content/BroadcastReceiver;
.source "LgeVoiceProtectionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/LgeVoiceProtectionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/LgeVoiceProtectionManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 112
    iput-object p1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$1;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public declared-synchronized onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 12
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 115
    monitor-enter p0

    #@3
    :try_start_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 117
    .local v0, action:Ljava/lang/String;
    iget-object v6, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$1;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@9
    new-instance v7, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v8, "intent received :"

    #@10
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v7

    #@14
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v7

    #@18
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v7

    #@1c
    invoke-static {v6, v7}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$000(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Ljava/lang/String;)V

    #@1f
    .line 119
    const-string v6, "android.intent.action.SCREEN_ON"

    #@21
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v6

    #@25
    if-eqz v6, :cond_52

    #@27
    .line 120
    iget-object v4, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$1;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@29
    const/16 v5, 0x8

    #@2b
    invoke-static {v4, v5}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$100(Lcom/android/internal/telephony/LgeVoiceProtectionManager;I)V

    #@2e
    .line 145
    :cond_2e
    :goto_2e
    iget-object v4, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$1;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@30
    iget-object v4, v4, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    #@32
    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@35
    move-result v4

    #@36
    if-eqz v4, :cond_3f

    #@38
    .line 146
    iget-object v4, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$1;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@3a
    iget-object v4, v4, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    #@3c
    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->release()V

    #@3f
    .line 149
    :cond_3f
    iget-object v4, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$1;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@41
    iget-object v4, v4, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    #@43
    const-wide/16 v5, 0x3e8

    #@45
    invoke-virtual {v4, v5, v6}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    #@48
    .line 150
    iget-object v4, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$1;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@4a
    const v5, 0x25ceb

    #@4d
    invoke-virtual {v4, v5}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->sendMessage(I)V
    :try_end_50
    .catchall {:try_start_3 .. :try_end_50} :catchall_62

    #@50
    .line 151
    monitor-exit p0

    #@51
    return-void

    #@52
    .line 122
    :cond_52
    :try_start_52
    const-string v6, "android.intent.action.SCREEN_OFF"

    #@54
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@57
    move-result v6

    #@58
    if-eqz v6, :cond_65

    #@5a
    .line 123
    iget-object v4, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$1;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@5c
    const/16 v5, 0x8

    #@5e
    invoke-static {v4, v5}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$200(Lcom/android/internal/telephony/LgeVoiceProtectionManager;I)V
    :try_end_61
    .catchall {:try_start_52 .. :try_end_61} :catchall_62

    #@61
    goto :goto_2e

    #@62
    .line 115
    .end local v0           #action:Ljava/lang/String;
    :catchall_62
    move-exception v4

    #@63
    monitor-exit p0

    #@64
    throw v4

    #@65
    .line 125
    .restart local v0       #action:Ljava/lang/String;
    :cond_65
    :try_start_65
    const-string v6, "android.intent.action.SERVICE_STATE"

    #@67
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6a
    move-result v6

    #@6b
    if-eqz v6, :cond_83

    #@6d
    .line 127
    iget-object v4, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$1;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@6f
    invoke-virtual {v4}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->isUMTS()Z

    #@72
    move-result v4

    #@73
    if-eqz v4, :cond_7c

    #@75
    iget-object v4, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$1;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@77
    const/4 v5, 0x1

    #@78
    invoke-static {v4, v5}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$100(Lcom/android/internal/telephony/LgeVoiceProtectionManager;I)V

    #@7b
    goto :goto_2e

    #@7c
    .line 128
    :cond_7c
    iget-object v4, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$1;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@7e
    const/4 v5, 0x1

    #@7f
    invoke-static {v4, v5}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$200(Lcom/android/internal/telephony/LgeVoiceProtectionManager;I)V

    #@82
    goto :goto_2e

    #@83
    .line 130
    :cond_83
    const-string v6, "com.lge.internal.telephony.SIGNAL_REPORT"

    #@85
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@88
    move-result v6

    #@89
    if-eqz v6, :cond_a4

    #@8b
    .line 132
    const-string v4, "enabled"

    #@8d
    const/4 v5, 0x0

    #@8e
    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@91
    move-result v3

    #@92
    .line 133
    .local v3, vpEnabled:Z
    if-eqz v3, :cond_9c

    #@94
    iget-object v4, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$1;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@96
    const/16 v5, 0x10

    #@98
    invoke-static {v4, v5}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$100(Lcom/android/internal/telephony/LgeVoiceProtectionManager;I)V

    #@9b
    goto :goto_2e

    #@9c
    .line 134
    :cond_9c
    iget-object v4, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$1;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@9e
    const/16 v5, 0x10

    #@a0
    invoke-static {v4, v5}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$200(Lcom/android/internal/telephony/LgeVoiceProtectionManager;I)V

    #@a3
    goto :goto_2e

    #@a4
    .line 136
    .end local v3           #vpEnabled:Z
    :cond_a4
    const-string v6, "android.net.conn.TETHER_STATE_CHANGED"

    #@a6
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a9
    move-result v6

    #@aa
    if-eqz v6, :cond_2e

    #@ac
    .line 137
    const-string v6, "activeArray"

    #@ae
    invoke-virtual {p2, v6}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    #@b1
    move-result-object v1

    #@b2
    .line 138
    .local v1, active:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    #@b3
    .line 139
    .local v2, isTetheringOn:Z
    if-eqz v1, :cond_bc

    #@b5
    .line 140
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@b8
    move-result v6

    #@b9
    if-lez v6, :cond_c7

    #@bb
    move v2, v4

    #@bc
    .line 142
    :cond_bc
    :goto_bc
    if-eqz v2, :cond_c9

    #@be
    iget-object v4, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$1;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@c0
    const/16 v5, 0x20

    #@c2
    invoke-static {v4, v5}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$100(Lcom/android/internal/telephony/LgeVoiceProtectionManager;I)V

    #@c5
    goto/16 :goto_2e

    #@c7
    :cond_c7
    move v2, v5

    #@c8
    .line 140
    goto :goto_bc

    #@c9
    .line 143
    :cond_c9
    iget-object v4, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager$1;->this$0:Lcom/android/internal/telephony/LgeVoiceProtectionManager;

    #@cb
    const/16 v5, 0x20

    #@cd
    invoke-static {v4, v5}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->access$200(Lcom/android/internal/telephony/LgeVoiceProtectionManager;I)V
    :try_end_d0
    .catchall {:try_start_65 .. :try_end_d0} :catchall_62

    #@d0
    goto/16 :goto_2e
.end method
