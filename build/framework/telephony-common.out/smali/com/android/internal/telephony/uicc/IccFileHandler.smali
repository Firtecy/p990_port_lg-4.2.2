.class public abstract Lcom/android/internal/telephony/uicc/IccFileHandler;
.super Landroid/os/Handler;
.source "IccFileHandler.java"

# interfaces
.implements Lcom/android/internal/telephony/uicc/IccConstants;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;
    }
.end annotation


# static fields
.field protected static final COMMAND_GET_RESPONSE:I = 0xc0

.field protected static final COMMAND_READ_BINARY:I = 0xb0

.field protected static final COMMAND_READ_RECORD:I = 0xb2

.field protected static final COMMAND_SEEK:I = 0xa2

.field protected static final COMMAND_UPDATE_BINARY:I = 0xd6

.field protected static final COMMAND_UPDATE_RECORD:I = 0xdc

.field protected static final EF_TYPE_CYCLIC:I = 0x3

.field protected static final EF_TYPE_LINEAR_FIXED:I = 0x1

.field protected static final EF_TYPE_TRANSPARENT:I = 0x0

.field protected static final EVENT_GET_BINARY_SIZE_DONE:I = 0x4

.field protected static final EVENT_GET_EF_LINEAR_RECORD_SIZE_DONE:I = 0x8

.field protected static final EVENT_GET_RECORD_SIZE_DONE:I = 0x6

.field protected static final EVENT_GET_RECORD_SIZE_IMG_DONE:I = 0xb

.field protected static final EVENT_READ_BINARY_DONE:I = 0x5

.field protected static final EVENT_READ_ICON_DONE:I = 0xa

.field protected static final EVENT_READ_IMG_DONE:I = 0x9

.field protected static final EVENT_READ_RECORD_DONE:I = 0x7

.field protected static final GET_RESPONSE_EF_IMG_SIZE_BYTES:I = 0xa

.field protected static final GET_RESPONSE_EF_SIZE_BYTES:I = 0xf

.field protected static final READ_RECORD_MODE_ABSOLUTE:I = 0x4

.field protected static final RESPONSE_DATA_ACCESS_CONDITION_1:I = 0x8

.field protected static final RESPONSE_DATA_ACCESS_CONDITION_2:I = 0x9

.field protected static final RESPONSE_DATA_ACCESS_CONDITION_3:I = 0xa

.field protected static final RESPONSE_DATA_FILE_ID_1:I = 0x4

.field protected static final RESPONSE_DATA_FILE_ID_2:I = 0x5

.field protected static final RESPONSE_DATA_FILE_SIZE_1:I = 0x2

.field protected static final RESPONSE_DATA_FILE_SIZE_2:I = 0x3

.field protected static final RESPONSE_DATA_FILE_STATUS:I = 0xb

.field protected static final RESPONSE_DATA_FILE_TYPE:I = 0x6

.field protected static final RESPONSE_DATA_LENGTH:I = 0xc

.field protected static final RESPONSE_DATA_RECORD_LENGTH:I = 0xe

.field protected static final RESPONSE_DATA_RFU_1:I = 0x0

.field protected static final RESPONSE_DATA_RFU_2:I = 0x1

.field protected static final RESPONSE_DATA_RFU_3:I = 0x7

.field protected static final RESPONSE_DATA_STRUCTURE:I = 0xd

.field protected static final TYPE_DF:I = 0x2

.field protected static final TYPE_EF:I = 0x4

.field protected static final TYPE_MF:I = 0x1

.field protected static final TYPE_RFU:I


# instance fields
.field protected final mAid:Ljava/lang/String;

.field protected final mCi:Lcom/android/internal/telephony/CommandsInterface;

.field protected final mParentApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;


# direct methods
.method protected constructor <init>(Lcom/android/internal/telephony/uicc/UiccCardApplication;Ljava/lang/String;Lcom/android/internal/telephony/CommandsInterface;)V
    .registers 4
    .parameter "app"
    .parameter "aid"
    .parameter "ci"

    #@0
    .prologue
    .line 155
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@3
    .line 156
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mParentApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@5
    .line 157
    iput-object p2, p0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mAid:Ljava/lang/String;

    #@7
    .line 158
    iput-object p3, p0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@9
    .line 159
    return-void
.end method

.method private processException(Landroid/os/Message;Landroid/os/AsyncResult;)Z
    .registers 8
    .parameter "response"
    .parameter "ar"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 406
    const/4 v0, 0x0

    #@2
    .line 407
    .local v0, flag:Z
    iget-object v2, p2, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@4
    check-cast v2, Lcom/android/internal/telephony/uicc/IccIoResult;

    #@6
    .line 409
    .local v2, result:Lcom/android/internal/telephony/uicc/IccIoResult;
    iget-object v3, p2, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@8
    if-eqz v3, :cond_11

    #@a
    .line 410
    iget-object v3, p2, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@c
    invoke-direct {p0, p1, v4, v3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->sendResult(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@f
    .line 411
    const/4 v0, 0x1

    #@10
    .line 419
    :cond_10
    :goto_10
    return v0

    #@11
    .line 413
    :cond_11
    invoke-virtual {v2}, Lcom/android/internal/telephony/uicc/IccIoResult;->getException()Lcom/android/internal/telephony/uicc/IccException;

    #@14
    move-result-object v1

    #@15
    .line 414
    .local v1, iccException:Lcom/android/internal/telephony/uicc/IccException;
    if-eqz v1, :cond_10

    #@17
    .line 415
    invoke-direct {p0, p1, v4, v1}, Lcom/android/internal/telephony/uicc/IccFileHandler;->sendResult(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@1a
    .line 416
    const/4 v0, 0x1

    #@1b
    goto :goto_10
.end method

.method private sendResult(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)V
    .registers 4
    .parameter "response"
    .parameter "result"
    .parameter "ex"

    #@0
    .prologue
    .line 395
    if-nez p1, :cond_3

    #@2
    .line 402
    :goto_2
    return-void

    #@3
    .line 399
    :cond_3
    invoke-static {p1, p2, p3}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@6
    .line 401
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    #@9
    goto :goto_2
.end method


# virtual methods
.method public dispose()V
    .registers 1

    #@0
    .prologue
    .line 162
    return-void
.end method

.method protected getCommonIccEFPath(I)Ljava/lang/String;
    .registers 3
    .parameter "efid"

    #@0
    .prologue
    .line 697
    sparse-switch p1, :sswitch_data_14

    #@3
    .line 742
    const/4 v0, 0x0

    #@4
    :goto_4
    return-object v0

    #@5
    .line 708
    :sswitch_5
    const-string v0, "3F007F10"

    #@7
    goto :goto_4

    #@8
    .line 731
    :sswitch_8
    const-string v0, "3F00"

    #@a
    goto :goto_4

    #@b
    .line 734
    :sswitch_b
    const-string v0, "3F007F105F3A"

    #@d
    goto :goto_4

    #@e
    .line 736
    :sswitch_e
    const-string v0, "3F007F105F50"

    #@10
    goto :goto_4

    #@11
    .line 739
    :sswitch_11
    const-string v0, "3F007FFF5F1F"

    #@13
    goto :goto_4

    #@14
    .line 697
    :sswitch_data_14
    .sparse-switch
        0x2f05 -> :sswitch_8
        0x2f30 -> :sswitch_8
        0x2f32 -> :sswitch_8
        0x2f33 -> :sswitch_8
        0x2f37 -> :sswitch_8
        0x2f38 -> :sswitch_8
        0x2f39 -> :sswitch_8
        0x2f40 -> :sswitch_8
        0x2f41 -> :sswitch_8
        0x2f42 -> :sswitch_8
        0x2f43 -> :sswitch_8
        0x2f50 -> :sswitch_8
        0x2fe2 -> :sswitch_8
        0x2fe7 -> :sswitch_8
        0x2ff0 -> :sswitch_8
        0x4f1c -> :sswitch_11
        0x4f20 -> :sswitch_e
        0x4f30 -> :sswitch_b
        0x6f3a -> :sswitch_5
        0x6f3b -> :sswitch_5
        0x6f40 -> :sswitch_5
        0x6f49 -> :sswitch_5
        0x6f4a -> :sswitch_5
        0x6f4b -> :sswitch_5
        0x6f4c -> :sswitch_5
        0x6fe5 -> :sswitch_5
    .end sparse-switch
.end method

.method public getEFLinearRecordSize(ILandroid/os/Message;)V
    .registers 4
    .parameter "fileid"
    .parameter "onLoaded"

    #@0
    .prologue
    .line 248
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/uicc/IccFileHandler;->getEFPath(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, p1, v0, p2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->getEFLinearRecordSize(ILjava/lang/String;Landroid/os/Message;)V

    #@7
    .line 249
    return-void
.end method

.method public getEFLinearRecordSize(ILjava/lang/String;Landroid/os/Message;)V
    .registers 15
    .parameter "fileid"
    .parameter "path"
    .parameter "onLoaded"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v4, 0x0

    #@2
    .line 231
    const/16 v0, 0x8

    #@4
    new-instance v1, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;

    #@6
    invoke-direct {v1, p1, p2, p3}, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;-><init>(ILjava/lang/String;Landroid/os/Message;)V

    #@9
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/uicc/IccFileHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@c
    move-result-object v10

    #@d
    .line 234
    .local v10, response:Landroid/os/Message;
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@f
    const/16 v1, 0xc0

    #@11
    const/16 v6, 0xf

    #@13
    iget-object v9, p0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mAid:Ljava/lang/String;

    #@15
    move v2, p1

    #@16
    move-object v3, p2

    #@17
    move v5, v4

    #@18
    move-object v8, v7

    #@19
    invoke-interface/range {v0 .. v10}, Lcom/android/internal/telephony/CommandsInterface;->iccIOForApp(IILjava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@1c
    .line 236
    return-void
.end method

.method protected abstract getEFPath(I)Ljava/lang/String;
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 37
    .parameter "msg"

    #@0
    .prologue
    .line 427
    const/16 v33, 0x0

    #@2
    .line 437
    .local v33, response:Landroid/os/Message;
    const/16 v30, 0x0

    #@4
    .line 440
    .local v30, path:Ljava/lang/String;
    :try_start_4
    move-object/from16 v0, p1

    #@6
    iget v3, v0, Landroid/os/Message;->what:I

    #@8
    packed-switch v3, :pswitch_data_4de

    #@b
    :cond_b
    :goto_b
    move-object/from16 v6, v30

    #@d
    .line 684
    .end local v30           #path:Ljava/lang/String;
    .local v6, path:Ljava/lang/String;
    :cond_d
    :goto_d
    return-void

    #@e
    .line 442
    .end local v6           #path:Ljava/lang/String;
    .restart local v30       #path:Ljava/lang/String;
    :pswitch_e
    const-string v3, "IccFileHandler: get record size img done"

    #@10
    move-object/from16 v0, p0

    #@12
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->logd(Ljava/lang/String;)V

    #@15
    .line 443
    move-object/from16 v0, p1

    #@17
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@19
    move-object/from16 v25, v0

    #@1b
    check-cast v25, Landroid/os/AsyncResult;

    #@1d
    .line 444
    .local v25, ar:Landroid/os/AsyncResult;
    move-object/from16 v0, v25

    #@1f
    iget-object v0, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@21
    move-object/from16 v29, v0

    #@23
    check-cast v29, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;

    #@25
    .line 445
    .local v29, lc:Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;
    move-object/from16 v0, v25

    #@27
    iget-object v0, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@29
    move-object/from16 v34, v0

    #@2b
    check-cast v34, Lcom/android/internal/telephony/uicc/IccIoResult;

    #@2d
    .line 446
    .local v34, result:Lcom/android/internal/telephony/uicc/IccIoResult;
    move-object/from16 v0, v29

    #@2f
    iget-object v0, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->onLoaded:Landroid/os/Message;

    #@31
    move-object/from16 v33, v0

    #@33
    .line 448
    move-object/from16 v0, v25

    #@35
    iget-object v3, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@37
    if-eqz v3, :cond_48

    #@39
    .line 449
    const/4 v3, 0x0

    #@3a
    move-object/from16 v0, v25

    #@3c
    iget-object v4, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@3e
    move-object/from16 v0, p0

    #@40
    move-object/from16 v1, v33

    #@42
    invoke-direct {v0, v1, v3, v4}, Lcom/android/internal/telephony/uicc/IccFileHandler;->sendResult(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@45
    move-object/from16 v6, v30

    #@47
    .line 450
    .end local v30           #path:Ljava/lang/String;
    .restart local v6       #path:Ljava/lang/String;
    goto :goto_d

    #@48
    .line 453
    .end local v6           #path:Ljava/lang/String;
    .restart local v30       #path:Ljava/lang/String;
    :cond_48
    invoke-virtual/range {v34 .. v34}, Lcom/android/internal/telephony/uicc/IccIoResult;->getException()Lcom/android/internal/telephony/uicc/IccException;

    #@4b
    move-result-object v28

    #@4c
    .line 455
    .local v28, iccException:Lcom/android/internal/telephony/uicc/IccException;
    if-eqz v28, :cond_5b

    #@4e
    .line 456
    const/4 v3, 0x0

    #@4f
    move-object/from16 v0, p0

    #@51
    move-object/from16 v1, v33

    #@53
    move-object/from16 v2, v28

    #@55
    invoke-direct {v0, v1, v3, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->sendResult(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@58
    move-object/from16 v6, v30

    #@5a
    .line 457
    .end local v30           #path:Ljava/lang/String;
    .restart local v6       #path:Ljava/lang/String;
    goto :goto_d

    #@5b
    .line 460
    .end local v6           #path:Ljava/lang/String;
    .restart local v30       #path:Ljava/lang/String;
    :cond_5b
    move-object/from16 v0, v34

    #@5d
    iget-object v0, v0, Lcom/android/internal/telephony/uicc/IccIoResult;->payload:[B

    #@5f
    move-object/from16 v26, v0

    #@61
    .line 461
    .local v26, data:[B
    const/16 v3, 0xe

    #@63
    aget-byte v3, v26, v3

    #@65
    and-int/lit16 v3, v3, 0xff

    #@67
    move-object/from16 v0, v29

    #@69
    iput v3, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->recordSize:I

    #@6b
    .line 463
    const/4 v3, 0x4

    #@6c
    const/4 v4, 0x6

    #@6d
    aget-byte v4, v26, v4

    #@6f
    if-ne v3, v4, :cond_78

    #@71
    const/4 v3, 0x1

    #@72
    const/16 v4, 0xd

    #@74
    aget-byte v4, v26, v4

    #@76
    if-eq v3, v4, :cond_96

    #@78
    .line 465
    :cond_78
    const-string v3, "IccFileHandler: File type mismatch: Throw Exception"

    #@7a
    move-object/from16 v0, p0

    #@7c
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loge(Ljava/lang/String;)V

    #@7f
    .line 466
    new-instance v3, Lcom/android/internal/telephony/uicc/IccFileTypeMismatch;

    #@81
    invoke-direct {v3}, Lcom/android/internal/telephony/uicc/IccFileTypeMismatch;-><init>()V

    #@84
    throw v3
    :try_end_85
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_85} :catch_85

    #@85
    .line 677
    .end local v25           #ar:Landroid/os/AsyncResult;
    .end local v26           #data:[B
    .end local v28           #iccException:Lcom/android/internal/telephony/uicc/IccException;
    .end local v29           #lc:Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;
    .end local v34           #result:Lcom/android/internal/telephony/uicc/IccIoResult;
    :catch_85
    move-exception v27

    #@86
    move-object/from16 v6, v30

    #@88
    .line 678
    .end local v30           #path:Ljava/lang/String;
    .restart local v6       #path:Ljava/lang/String;
    .local v27, exc:Ljava/lang/Exception;
    :goto_88
    if-eqz v33, :cond_4c1

    #@8a
    .line 679
    const/4 v3, 0x0

    #@8b
    move-object/from16 v0, p0

    #@8d
    move-object/from16 v1, v33

    #@8f
    move-object/from16 v2, v27

    #@91
    invoke-direct {v0, v1, v3, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->sendResult(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@94
    goto/16 :goto_d

    #@96
    .line 469
    .end local v6           #path:Ljava/lang/String;
    .end local v27           #exc:Ljava/lang/Exception;
    .restart local v25       #ar:Landroid/os/AsyncResult;
    .restart local v26       #data:[B
    .restart local v28       #iccException:Lcom/android/internal/telephony/uicc/IccException;
    .restart local v29       #lc:Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;
    .restart local v30       #path:Ljava/lang/String;
    .restart local v34       #result:Lcom/android/internal/telephony/uicc/IccIoResult;
    :cond_96
    :try_start_96
    const-string v3, "IccFileHandler: read EF IMG"

    #@98
    move-object/from16 v0, p0

    #@9a
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->logd(Ljava/lang/String;)V

    #@9d
    .line 470
    move-object/from16 v0, p0

    #@9f
    iget-object v3, v0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@a1
    const/16 v4, 0xb2

    #@a3
    move-object/from16 v0, v29

    #@a5
    iget v5, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->efid:I

    #@a7
    move-object/from16 v0, v29

    #@a9
    iget v7, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->efid:I

    #@ab
    move-object/from16 v0, p0

    #@ad
    invoke-virtual {v0, v7}, Lcom/android/internal/telephony/uicc/IccFileHandler;->getEFPath(I)Ljava/lang/String;

    #@b0
    move-result-object v6

    #@b1
    move-object/from16 v0, v29

    #@b3
    iget v7, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->recordNum:I

    #@b5
    const/4 v8, 0x4

    #@b6
    move-object/from16 v0, v29

    #@b8
    iget v9, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->recordSize:I

    #@ba
    const/4 v10, 0x0

    #@bb
    const/4 v11, 0x0

    #@bc
    move-object/from16 v0, p0

    #@be
    iget-object v12, v0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mAid:Ljava/lang/String;

    #@c0
    const/16 v14, 0x9

    #@c2
    const/16 v15, 0x4f20

    #@c4
    const/16 v16, 0x0

    #@c6
    move-object/from16 v0, p0

    #@c8
    move/from16 v1, v16

    #@ca
    move-object/from16 v2, v33

    #@cc
    invoke-virtual {v0, v14, v15, v1, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@cf
    move-result-object v13

    #@d0
    invoke-interface/range {v3 .. v13}, Lcom/android/internal/telephony/CommandsInterface;->iccIOForApp(IILjava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@d3
    move-object/from16 v6, v30

    #@d5
    .line 475
    .end local v30           #path:Ljava/lang/String;
    .restart local v6       #path:Ljava/lang/String;
    goto/16 :goto_d

    #@d7
    .line 478
    .end local v6           #path:Ljava/lang/String;
    .end local v25           #ar:Landroid/os/AsyncResult;
    .end local v26           #data:[B
    .end local v28           #iccException:Lcom/android/internal/telephony/uicc/IccException;
    .end local v29           #lc:Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;
    .end local v34           #result:Lcom/android/internal/telephony/uicc/IccIoResult;
    .restart local v30       #path:Ljava/lang/String;
    :pswitch_d7
    const-string v3, "read IMG done"

    #@d9
    move-object/from16 v0, p0

    #@db
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->logd(Ljava/lang/String;)V

    #@de
    .line 479
    move-object/from16 v0, p1

    #@e0
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@e2
    move-object/from16 v25, v0

    #@e4
    check-cast v25, Landroid/os/AsyncResult;

    #@e6
    .line 480
    .restart local v25       #ar:Landroid/os/AsyncResult;
    move-object/from16 v0, v25

    #@e8
    iget-object v3, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@ea
    move-object v0, v3

    #@eb
    check-cast v0, Landroid/os/Message;

    #@ed
    move-object/from16 v33, v0

    #@ef
    .line 481
    move-object/from16 v0, v25

    #@f1
    iget-object v0, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@f3
    move-object/from16 v34, v0

    #@f5
    check-cast v34, Lcom/android/internal/telephony/uicc/IccIoResult;

    #@f7
    .line 483
    .restart local v34       #result:Lcom/android/internal/telephony/uicc/IccIoResult;
    move-object/from16 v0, p1

    #@f9
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@fb
    check-cast v3, Landroid/os/AsyncResult;

    #@fd
    move-object/from16 v0, p0

    #@ff
    move-object/from16 v1, v33

    #@101
    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->processException(Landroid/os/Message;Landroid/os/AsyncResult;)Z

    #@104
    move-result v3

    #@105
    if-eqz v3, :cond_10b

    #@107
    move-object/from16 v6, v30

    #@109
    .line 484
    .end local v30           #path:Ljava/lang/String;
    .restart local v6       #path:Ljava/lang/String;
    goto/16 :goto_d

    #@10b
    .line 486
    .end local v6           #path:Ljava/lang/String;
    .restart local v30       #path:Ljava/lang/String;
    :cond_10b
    const-string v3, "read img success"

    #@10d
    move-object/from16 v0, p0

    #@10f
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->logd(Ljava/lang/String;)V

    #@112
    .line 487
    move-object/from16 v0, v34

    #@114
    iget-object v3, v0, Lcom/android/internal/telephony/uicc/IccIoResult;->payload:[B

    #@116
    const/4 v4, 0x0

    #@117
    move-object/from16 v0, p0

    #@119
    move-object/from16 v1, v33

    #@11b
    invoke-direct {v0, v1, v3, v4}, Lcom/android/internal/telephony/uicc/IccFileHandler;->sendResult(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@11e
    .line 488
    invoke-virtual/range {v34 .. v34}, Lcom/android/internal/telephony/uicc/IccIoResult;->getException()Lcom/android/internal/telephony/uicc/IccException;

    #@121
    move-result-object v28

    #@122
    .line 489
    .restart local v28       #iccException:Lcom/android/internal/telephony/uicc/IccException;
    if-eqz v28, :cond_b

    #@124
    .line 490
    move-object/from16 v0, v34

    #@126
    iget-object v3, v0, Lcom/android/internal/telephony/uicc/IccIoResult;->payload:[B

    #@128
    move-object/from16 v0, v25

    #@12a
    iget-object v4, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@12c
    move-object/from16 v0, p0

    #@12e
    move-object/from16 v1, v33

    #@130
    invoke-direct {v0, v1, v3, v4}, Lcom/android/internal/telephony/uicc/IccFileHandler;->sendResult(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@133
    move-object/from16 v6, v30

    #@135
    .end local v30           #path:Ljava/lang/String;
    .restart local v6       #path:Ljava/lang/String;
    goto/16 :goto_d

    #@137
    .line 495
    .end local v6           #path:Ljava/lang/String;
    .end local v25           #ar:Landroid/os/AsyncResult;
    .end local v28           #iccException:Lcom/android/internal/telephony/uicc/IccException;
    .end local v34           #result:Lcom/android/internal/telephony/uicc/IccIoResult;
    .restart local v30       #path:Ljava/lang/String;
    :pswitch_137
    const-string v3, "read icon done"

    #@139
    move-object/from16 v0, p0

    #@13b
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->logd(Ljava/lang/String;)V

    #@13e
    .line 496
    move-object/from16 v0, p1

    #@140
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@142
    move-object/from16 v25, v0

    #@144
    check-cast v25, Landroid/os/AsyncResult;

    #@146
    .line 497
    .restart local v25       #ar:Landroid/os/AsyncResult;
    move-object/from16 v0, v25

    #@148
    iget-object v3, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@14a
    move-object v0, v3

    #@14b
    check-cast v0, Landroid/os/Message;

    #@14d
    move-object/from16 v33, v0

    #@14f
    .line 498
    move-object/from16 v0, v25

    #@151
    iget-object v0, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@153
    move-object/from16 v34, v0

    #@155
    check-cast v34, Lcom/android/internal/telephony/uicc/IccIoResult;

    #@157
    .line 500
    .restart local v34       #result:Lcom/android/internal/telephony/uicc/IccIoResult;
    move-object/from16 v0, p1

    #@159
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@15b
    check-cast v3, Landroid/os/AsyncResult;

    #@15d
    move-object/from16 v0, p0

    #@15f
    move-object/from16 v1, v33

    #@161
    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->processException(Landroid/os/Message;Landroid/os/AsyncResult;)Z

    #@164
    move-result v3

    #@165
    if-eqz v3, :cond_16b

    #@167
    move-object/from16 v6, v30

    #@169
    .line 501
    .end local v30           #path:Ljava/lang/String;
    .restart local v6       #path:Ljava/lang/String;
    goto/16 :goto_d

    #@16b
    .line 503
    .end local v6           #path:Ljava/lang/String;
    .restart local v30       #path:Ljava/lang/String;
    :cond_16b
    const-string v3, "read icon success"

    #@16d
    move-object/from16 v0, p0

    #@16f
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->logd(Ljava/lang/String;)V

    #@172
    .line 504
    move-object/from16 v0, v34

    #@174
    iget-object v3, v0, Lcom/android/internal/telephony/uicc/IccIoResult;->payload:[B

    #@176
    const/4 v4, 0x0

    #@177
    move-object/from16 v0, p0

    #@179
    move-object/from16 v1, v33

    #@17b
    invoke-direct {v0, v1, v3, v4}, Lcom/android/internal/telephony/uicc/IccFileHandler;->sendResult(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@17e
    move-object/from16 v6, v30

    #@180
    .line 505
    .end local v30           #path:Ljava/lang/String;
    .restart local v6       #path:Ljava/lang/String;
    goto/16 :goto_d

    #@182
    .line 507
    .end local v6           #path:Ljava/lang/String;
    .end local v25           #ar:Landroid/os/AsyncResult;
    .end local v34           #result:Lcom/android/internal/telephony/uicc/IccIoResult;
    .restart local v30       #path:Ljava/lang/String;
    :pswitch_182
    move-object/from16 v0, p1

    #@184
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@186
    move-object/from16 v25, v0

    #@188
    check-cast v25, Landroid/os/AsyncResult;

    #@18a
    .line 508
    .restart local v25       #ar:Landroid/os/AsyncResult;
    move-object/from16 v0, v25

    #@18c
    iget-object v0, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@18e
    move-object/from16 v29, v0

    #@190
    check-cast v29, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;

    #@192
    .line 509
    .restart local v29       #lc:Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;
    move-object/from16 v0, v25

    #@194
    iget-object v0, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@196
    move-object/from16 v34, v0

    #@198
    check-cast v34, Lcom/android/internal/telephony/uicc/IccIoResult;

    #@19a
    .line 510
    .restart local v34       #result:Lcom/android/internal/telephony/uicc/IccIoResult;
    move-object/from16 v0, v29

    #@19c
    iget-object v0, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->onLoaded:Landroid/os/Message;

    #@19e
    move-object/from16 v33, v0

    #@1a0
    .line 512
    move-object/from16 v0, p1

    #@1a2
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1a4
    check-cast v3, Landroid/os/AsyncResult;

    #@1a6
    move-object/from16 v0, p0

    #@1a8
    move-object/from16 v1, v33

    #@1aa
    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->processException(Landroid/os/Message;Landroid/os/AsyncResult;)Z

    #@1ad
    move-result v3

    #@1ae
    if-eqz v3, :cond_1b4

    #@1b0
    move-object/from16 v6, v30

    #@1b2
    .line 513
    .end local v30           #path:Ljava/lang/String;
    .restart local v6       #path:Ljava/lang/String;
    goto/16 :goto_d

    #@1b4
    .line 516
    .end local v6           #path:Ljava/lang/String;
    .restart local v30       #path:Ljava/lang/String;
    :cond_1b4
    move-object/from16 v0, v34

    #@1b6
    iget-object v0, v0, Lcom/android/internal/telephony/uicc/IccIoResult;->payload:[B

    #@1b8
    move-object/from16 v26, v0

    #@1ba
    .line 518
    .restart local v26       #data:[B
    const/4 v3, 0x4

    #@1bb
    const/4 v4, 0x6

    #@1bc
    aget-byte v4, v26, v4

    #@1be
    if-ne v3, v4, :cond_1c7

    #@1c0
    const/4 v3, 0x1

    #@1c1
    const/16 v4, 0xd

    #@1c3
    aget-byte v4, v26, v4

    #@1c5
    if-eq v3, v4, :cond_1cd

    #@1c7
    .line 520
    :cond_1c7
    new-instance v3, Lcom/android/internal/telephony/uicc/IccFileTypeMismatch;

    #@1c9
    invoke-direct {v3}, Lcom/android/internal/telephony/uicc/IccFileTypeMismatch;-><init>()V

    #@1cc
    throw v3

    #@1cd
    .line 523
    :cond_1cd
    const/4 v3, 0x3

    #@1ce
    new-array v0, v3, [I

    #@1d0
    move-object/from16 v32, v0

    #@1d2
    .line 524
    .local v32, recordSize:[I
    const/4 v3, 0x0

    #@1d3
    const/16 v4, 0xe

    #@1d5
    aget-byte v4, v26, v4

    #@1d7
    and-int/lit16 v4, v4, 0xff

    #@1d9
    aput v4, v32, v3

    #@1db
    .line 525
    const/4 v3, 0x1

    #@1dc
    const/4 v4, 0x2

    #@1dd
    aget-byte v4, v26, v4

    #@1df
    and-int/lit16 v4, v4, 0xff

    #@1e1
    shl-int/lit8 v4, v4, 0x8

    #@1e3
    const/4 v5, 0x3

    #@1e4
    aget-byte v5, v26, v5

    #@1e6
    and-int/lit16 v5, v5, 0xff

    #@1e8
    add-int/2addr v4, v5

    #@1e9
    aput v4, v32, v3

    #@1eb
    .line 527
    const/4 v3, 0x2

    #@1ec
    const/4 v4, 0x1

    #@1ed
    aget v4, v32, v4

    #@1ef
    const/4 v5, 0x0

    #@1f0
    aget v5, v32, v5

    #@1f2
    div-int/2addr v4, v5

    #@1f3
    aput v4, v32, v3

    #@1f5
    .line 529
    const/4 v3, 0x0

    #@1f6
    move-object/from16 v0, p0

    #@1f8
    move-object/from16 v1, v33

    #@1fa
    move-object/from16 v2, v32

    #@1fc
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->sendResult(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@1ff
    move-object/from16 v6, v30

    #@201
    .line 530
    .end local v30           #path:Ljava/lang/String;
    .restart local v6       #path:Ljava/lang/String;
    goto/16 :goto_d

    #@203
    .line 532
    .end local v6           #path:Ljava/lang/String;
    .end local v25           #ar:Landroid/os/AsyncResult;
    .end local v26           #data:[B
    .end local v29           #lc:Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;
    .end local v32           #recordSize:[I
    .end local v34           #result:Lcom/android/internal/telephony/uicc/IccIoResult;
    .restart local v30       #path:Ljava/lang/String;
    :pswitch_203
    move-object/from16 v0, p1

    #@205
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@207
    move-object/from16 v25, v0

    #@209
    check-cast v25, Landroid/os/AsyncResult;

    #@20b
    .line 533
    .restart local v25       #ar:Landroid/os/AsyncResult;
    move-object/from16 v0, v25

    #@20d
    iget-object v0, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@20f
    move-object/from16 v29, v0

    #@211
    check-cast v29, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;

    #@213
    .line 534
    .restart local v29       #lc:Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;
    move-object/from16 v0, v25

    #@215
    iget-object v0, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@217
    move-object/from16 v34, v0

    #@219
    check-cast v34, Lcom/android/internal/telephony/uicc/IccIoResult;

    #@21b
    .line 535
    .restart local v34       #result:Lcom/android/internal/telephony/uicc/IccIoResult;
    move-object/from16 v0, v29

    #@21d
    iget-object v0, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->onLoaded:Landroid/os/Message;

    #@21f
    move-object/from16 v33, v0

    #@221
    .line 537
    move-object/from16 v0, p1

    #@223
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@225
    check-cast v3, Landroid/os/AsyncResult;

    #@227
    move-object/from16 v0, p0

    #@229
    move-object/from16 v1, v33

    #@22b
    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->processException(Landroid/os/Message;Landroid/os/AsyncResult;)Z

    #@22e
    move-result v3

    #@22f
    if-eqz v3, :cond_235

    #@231
    move-object/from16 v6, v30

    #@233
    .line 538
    .end local v30           #path:Ljava/lang/String;
    .restart local v6       #path:Ljava/lang/String;
    goto/16 :goto_d

    #@235
    .line 541
    .end local v6           #path:Ljava/lang/String;
    .restart local v30       #path:Ljava/lang/String;
    :cond_235
    move-object/from16 v0, v34

    #@237
    iget-object v0, v0, Lcom/android/internal/telephony/uicc/IccIoResult;->payload:[B

    #@239
    move-object/from16 v26, v0

    #@23b
    .line 542
    .restart local v26       #data:[B
    move-object/from16 v0, v29

    #@23d
    iget v9, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->efid:I

    #@23f
    .line 543
    .local v9, fileid:I
    move-object/from16 v0, v29

    #@241
    iget v0, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->recordNum:I

    #@243
    move/from16 v31, v0

    #@245
    .line 544
    .local v31, recordNum:I
    move-object/from16 v0, v29

    #@247
    iget-object v6, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->path:Ljava/lang/String;
    :try_end_249
    .catch Ljava/lang/Exception; {:try_start_96 .. :try_end_249} :catch_85

    #@249
    .line 546
    .end local v30           #path:Ljava/lang/String;
    .restart local v6       #path:Ljava/lang/String;
    const/4 v3, 0x4

    #@24a
    const/4 v4, 0x6

    #@24b
    :try_start_24b
    aget-byte v4, v26, v4

    #@24d
    if-eq v3, v4, :cond_258

    #@24f
    .line 547
    new-instance v3, Lcom/android/internal/telephony/uicc/IccFileTypeMismatch;

    #@251
    invoke-direct {v3}, Lcom/android/internal/telephony/uicc/IccFileTypeMismatch;-><init>()V

    #@254
    throw v3

    #@255
    .line 677
    .end local v9           #fileid:I
    .end local v26           #data:[B
    .end local v31           #recordNum:I
    :catch_255
    move-exception v27

    #@256
    goto/16 :goto_88

    #@258
    .line 550
    .restart local v9       #fileid:I
    .restart local v26       #data:[B
    .restart local v31       #recordNum:I
    :cond_258
    const/4 v3, 0x1

    #@259
    const/16 v4, 0xd

    #@25b
    aget-byte v4, v26, v4

    #@25d
    if-eq v3, v4, :cond_265

    #@25f
    .line 551
    new-instance v3, Lcom/android/internal/telephony/uicc/IccFileTypeMismatch;

    #@261
    invoke-direct {v3}, Lcom/android/internal/telephony/uicc/IccFileTypeMismatch;-><init>()V

    #@264
    throw v3

    #@265
    .line 554
    :cond_265
    const/16 v3, 0xe

    #@267
    aget-byte v3, v26, v3

    #@269
    and-int/lit16 v3, v3, 0xff

    #@26b
    move-object/from16 v0, v29

    #@26d
    iput v3, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->recordSize:I

    #@26f
    .line 556
    const/4 v3, 0x2

    #@270
    aget-byte v3, v26, v3

    #@272
    and-int/lit16 v3, v3, 0xff

    #@274
    shl-int/lit8 v3, v3, 0x8

    #@276
    const/4 v4, 0x3

    #@277
    aget-byte v4, v26, v4

    #@279
    and-int/lit16 v4, v4, 0xff

    #@27b
    add-int v13, v3, v4

    #@27d
    .line 559
    .local v13, size:I
    move-object/from16 v0, v29

    #@27f
    iget v3, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->recordSize:I

    #@281
    div-int v3, v13, v3

    #@283
    move-object/from16 v0, v29

    #@285
    iput v3, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->countRecords:I

    #@287
    .line 561
    move-object/from16 v0, v29

    #@289
    iget-boolean v3, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->loadAll:Z

    #@28b
    if-eqz v3, :cond_29a

    #@28d
    .line 562
    new-instance v3, Ljava/util/ArrayList;

    #@28f
    move-object/from16 v0, v29

    #@291
    iget v4, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->countRecords:I

    #@293
    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    #@296
    move-object/from16 v0, v29

    #@298
    iput-object v3, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->results:Ljava/util/ArrayList;

    #@29a
    .line 565
    :cond_29a
    if-nez v6, :cond_2a6

    #@29c
    .line 566
    move-object/from16 v0, v29

    #@29e
    iget v3, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->efid:I

    #@2a0
    move-object/from16 v0, p0

    #@2a2
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->getEFPath(I)Ljava/lang/String;

    #@2a5
    move-result-object v6

    #@2a6
    .line 570
    :cond_2a6
    move-object/from16 v0, v29

    #@2a8
    iget v3, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->efid:I

    #@2aa
    const/16 v4, 0x6f3b

    #@2ac
    if-ne v3, v4, :cond_2f0

    #@2ae
    .line 572
    move-object/from16 v0, v29

    #@2b0
    iget v3, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->countRecords:I

    #@2b2
    sput v3, Lcom/android/internal/telephony/uicc/SIMRecords;->FDN_REC_NUM:I

    #@2b4
    .line 573
    const-string v3, "IccFileHandler"

    #@2b6
    new-instance v4, Ljava/lang/StringBuilder;

    #@2b8
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2bb
    const-string v5, "FDN_REC_NUM = "

    #@2bd
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c0
    move-result-object v4

    #@2c1
    sget v5, Lcom/android/internal/telephony/uicc/SIMRecords;->FDN_REC_NUM:I

    #@2c3
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c6
    move-result-object v4

    #@2c7
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2ca
    move-result-object v4

    #@2cb
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2ce
    .line 574
    move-object/from16 v0, v29

    #@2d0
    iget v3, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->recordSize:I

    #@2d2
    add-int/lit8 v3, v3, -0xe

    #@2d4
    sput v3, Lcom/android/internal/telephony/uicc/SIMRecords;->FDN_NAME_MAX:I

    #@2d6
    .line 575
    const-string v3, "IccFileHandler"

    #@2d8
    new-instance v4, Ljava/lang/StringBuilder;

    #@2da
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2dd
    const-string v5, "FDN_NAME_MAX = "

    #@2df
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e2
    move-result-object v4

    #@2e3
    sget v5, Lcom/android/internal/telephony/uicc/SIMRecords;->FDN_NAME_MAX:I

    #@2e5
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e8
    move-result-object v4

    #@2e9
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2ec
    move-result-object v4

    #@2ed
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2f0
    .line 580
    :cond_2f0
    move-object/from16 v0, v29

    #@2f2
    iget v3, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->efid:I

    #@2f4
    const/16 v4, 0x6f04

    #@2f6
    if-ne v3, v4, :cond_314

    #@2f8
    .line 581
    move-object/from16 v0, v29

    #@2fa
    iget v3, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->recordSize:I

    #@2fc
    const/16 v4, 0x50

    #@2fe
    if-le v3, v4, :cond_306

    #@300
    .line 582
    const/16 v3, 0x50

    #@302
    move-object/from16 v0, v29

    #@304
    iput v3, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->recordSize:I

    #@306
    .line 583
    :cond_306
    move-object/from16 v0, v29

    #@308
    iget v3, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->countRecords:I

    #@30a
    const/16 v4, 0x14

    #@30c
    if-le v3, v4, :cond_314

    #@30e
    .line 584
    const/16 v3, 0x14

    #@310
    move-object/from16 v0, v29

    #@312
    iput v3, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->countRecords:I

    #@314
    .line 587
    :cond_314
    move-object/from16 v0, p0

    #@316
    iget-object v3, v0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@318
    const/16 v4, 0xb2

    #@31a
    move-object/from16 v0, v29

    #@31c
    iget v5, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->efid:I

    #@31e
    move-object/from16 v0, v29

    #@320
    iget v7, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->recordNum:I

    #@322
    const/4 v8, 0x4

    #@323
    move-object/from16 v0, v29

    #@325
    iget v9, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->recordSize:I

    #@327
    .end local v9           #fileid:I
    const/4 v10, 0x0

    #@328
    const/4 v11, 0x0

    #@329
    move-object/from16 v0, p0

    #@32b
    iget-object v12, v0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mAid:Ljava/lang/String;

    #@32d
    const/4 v14, 0x7

    #@32e
    move-object/from16 v0, p0

    #@330
    move-object/from16 v1, v29

    #@332
    invoke-virtual {v0, v14, v1}, Lcom/android/internal/telephony/uicc/IccFileHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@335
    move-result-object v13

    #@336
    .end local v13           #size:I
    invoke-interface/range {v3 .. v13}, Lcom/android/internal/telephony/CommandsInterface;->iccIOForApp(IILjava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    :try_end_339
    .catch Ljava/lang/Exception; {:try_start_24b .. :try_end_339} :catch_255

    #@339
    goto/16 :goto_d

    #@33b
    .line 594
    .end local v6           #path:Ljava/lang/String;
    .end local v25           #ar:Landroid/os/AsyncResult;
    .end local v26           #data:[B
    .end local v29           #lc:Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;
    .end local v31           #recordNum:I
    .end local v34           #result:Lcom/android/internal/telephony/uicc/IccIoResult;
    .restart local v30       #path:Ljava/lang/String;
    :pswitch_33b
    :try_start_33b
    move-object/from16 v0, p1

    #@33d
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@33f
    move-object/from16 v25, v0

    #@341
    check-cast v25, Landroid/os/AsyncResult;

    #@343
    .line 595
    .restart local v25       #ar:Landroid/os/AsyncResult;
    move-object/from16 v0, v25

    #@345
    iget-object v3, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@347
    move-object v0, v3

    #@348
    check-cast v0, Landroid/os/Message;

    #@34a
    move-object/from16 v33, v0

    #@34c
    .line 596
    move-object/from16 v0, v25

    #@34e
    iget-object v0, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@350
    move-object/from16 v34, v0

    #@352
    check-cast v34, Lcom/android/internal/telephony/uicc/IccIoResult;

    #@354
    .line 598
    .restart local v34       #result:Lcom/android/internal/telephony/uicc/IccIoResult;
    move-object/from16 v0, p1

    #@356
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@358
    check-cast v3, Landroid/os/AsyncResult;

    #@35a
    move-object/from16 v0, p0

    #@35c
    move-object/from16 v1, v33

    #@35e
    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->processException(Landroid/os/Message;Landroid/os/AsyncResult;)Z

    #@361
    move-result v3

    #@362
    if-eqz v3, :cond_368

    #@364
    move-object/from16 v6, v30

    #@366
    .line 599
    .end local v30           #path:Ljava/lang/String;
    .restart local v6       #path:Ljava/lang/String;
    goto/16 :goto_d

    #@368
    .line 602
    .end local v6           #path:Ljava/lang/String;
    .restart local v30       #path:Ljava/lang/String;
    :cond_368
    move-object/from16 v0, v34

    #@36a
    iget-object v0, v0, Lcom/android/internal/telephony/uicc/IccIoResult;->payload:[B

    #@36c
    move-object/from16 v26, v0

    #@36e
    .line 604
    .restart local v26       #data:[B
    move-object/from16 v0, p1

    #@370
    iget v9, v0, Landroid/os/Message;->arg1:I

    #@372
    .line 606
    .restart local v9       #fileid:I
    const/4 v3, 0x4

    #@373
    const/4 v4, 0x6

    #@374
    aget-byte v4, v26, v4

    #@376
    if-eq v3, v4, :cond_37e

    #@378
    .line 607
    new-instance v3, Lcom/android/internal/telephony/uicc/IccFileTypeMismatch;

    #@37a
    invoke-direct {v3}, Lcom/android/internal/telephony/uicc/IccFileTypeMismatch;-><init>()V

    #@37d
    throw v3

    #@37e
    .line 610
    :cond_37e
    const/16 v3, 0xd

    #@380
    aget-byte v3, v26, v3

    #@382
    if-eqz v3, :cond_38a

    #@384
    .line 611
    new-instance v3, Lcom/android/internal/telephony/uicc/IccFileTypeMismatch;

    #@386
    invoke-direct {v3}, Lcom/android/internal/telephony/uicc/IccFileTypeMismatch;-><init>()V

    #@389
    throw v3

    #@38a
    .line 614
    :cond_38a
    const/4 v3, 0x2

    #@38b
    aget-byte v3, v26, v3

    #@38d
    and-int/lit16 v3, v3, 0xff

    #@38f
    shl-int/lit8 v3, v3, 0x8

    #@391
    const/4 v4, 0x3

    #@392
    aget-byte v4, v26, v4

    #@394
    and-int/lit16 v4, v4, 0xff

    #@396
    add-int v13, v3, v4

    #@398
    .line 618
    .restart local v13       #size:I
    const/16 v3, 0x6f02

    #@39a
    if-ne v9, v3, :cond_3a2

    #@39c
    const/16 v3, 0x64

    #@39e
    if-le v13, v3, :cond_3a2

    #@3a0
    .line 619
    const/16 v13, 0x64

    #@3a2
    .line 620
    :cond_3a2
    const/16 v3, 0x6f03

    #@3a4
    if-ne v9, v3, :cond_3ac

    #@3a6
    const/16 v3, 0x4b

    #@3a8
    if-le v13, v3, :cond_3ac

    #@3aa
    .line 621
    const/16 v13, 0x4b

    #@3ac
    .line 623
    :cond_3ac
    move-object/from16 v0, p0

    #@3ae
    iget-object v7, v0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@3b0
    const/16 v8, 0xb0

    #@3b2
    move-object/from16 v0, p0

    #@3b4
    invoke-virtual {v0, v9}, Lcom/android/internal/telephony/uicc/IccFileHandler;->getEFPath(I)Ljava/lang/String;

    #@3b7
    move-result-object v10

    #@3b8
    const/4 v11, 0x0

    #@3b9
    const/4 v12, 0x0

    #@3ba
    const/4 v14, 0x0

    #@3bb
    const/4 v15, 0x0

    #@3bc
    move-object/from16 v0, p0

    #@3be
    iget-object v0, v0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mAid:Ljava/lang/String;

    #@3c0
    move-object/from16 v16, v0

    #@3c2
    const/4 v3, 0x5

    #@3c3
    const/4 v4, 0x0

    #@3c4
    move-object/from16 v0, p0

    #@3c6
    move-object/from16 v1, v33

    #@3c8
    invoke-virtual {v0, v3, v9, v4, v1}, Lcom/android/internal/telephony/uicc/IccFileHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@3cb
    move-result-object v17

    #@3cc
    invoke-interface/range {v7 .. v17}, Lcom/android/internal/telephony/CommandsInterface;->iccIOForApp(IILjava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@3cf
    move-object/from16 v6, v30

    #@3d1
    .line 627
    .end local v30           #path:Ljava/lang/String;
    .restart local v6       #path:Ljava/lang/String;
    goto/16 :goto_d

    #@3d3
    .line 631
    .end local v6           #path:Ljava/lang/String;
    .end local v9           #fileid:I
    .end local v13           #size:I
    .end local v25           #ar:Landroid/os/AsyncResult;
    .end local v26           #data:[B
    .end local v34           #result:Lcom/android/internal/telephony/uicc/IccIoResult;
    .restart local v30       #path:Ljava/lang/String;
    :pswitch_3d3
    move-object/from16 v0, p1

    #@3d5
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3d7
    move-object/from16 v25, v0

    #@3d9
    check-cast v25, Landroid/os/AsyncResult;

    #@3db
    .line 632
    .restart local v25       #ar:Landroid/os/AsyncResult;
    move-object/from16 v0, v25

    #@3dd
    iget-object v0, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@3df
    move-object/from16 v29, v0

    #@3e1
    check-cast v29, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;

    #@3e3
    .line 633
    .restart local v29       #lc:Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;
    move-object/from16 v0, v25

    #@3e5
    iget-object v0, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@3e7
    move-object/from16 v34, v0

    #@3e9
    check-cast v34, Lcom/android/internal/telephony/uicc/IccIoResult;

    #@3eb
    .line 634
    .restart local v34       #result:Lcom/android/internal/telephony/uicc/IccIoResult;
    move-object/from16 v0, v29

    #@3ed
    iget-object v0, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->onLoaded:Landroid/os/Message;

    #@3ef
    move-object/from16 v33, v0

    #@3f1
    .line 635
    move-object/from16 v0, v29

    #@3f3
    iget-object v6, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->path:Ljava/lang/String;
    :try_end_3f5
    .catch Ljava/lang/Exception; {:try_start_33b .. :try_end_3f5} :catch_85

    #@3f5
    .line 637
    .end local v30           #path:Ljava/lang/String;
    .restart local v6       #path:Ljava/lang/String;
    :try_start_3f5
    move-object/from16 v0, p1

    #@3f7
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3f9
    check-cast v3, Landroid/os/AsyncResult;

    #@3fb
    move-object/from16 v0, p0

    #@3fd
    move-object/from16 v1, v33

    #@3ff
    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->processException(Landroid/os/Message;Landroid/os/AsyncResult;)Z

    #@402
    move-result v3

    #@403
    if-nez v3, :cond_d

    #@405
    .line 641
    move-object/from16 v0, v29

    #@407
    iget-boolean v3, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->loadAll:Z

    #@409
    if-nez v3, :cond_419

    #@40b
    .line 642
    move-object/from16 v0, v34

    #@40d
    iget-object v3, v0, Lcom/android/internal/telephony/uicc/IccIoResult;->payload:[B

    #@40f
    const/4 v4, 0x0

    #@410
    move-object/from16 v0, p0

    #@412
    move-object/from16 v1, v33

    #@414
    invoke-direct {v0, v1, v3, v4}, Lcom/android/internal/telephony/uicc/IccFileHandler;->sendResult(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@417
    goto/16 :goto_d

    #@419
    .line 644
    :cond_419
    move-object/from16 v0, v29

    #@41b
    iget-object v3, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->results:Ljava/util/ArrayList;

    #@41d
    move-object/from16 v0, v34

    #@41f
    iget-object v4, v0, Lcom/android/internal/telephony/uicc/IccIoResult;->payload:[B

    #@421
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@424
    .line 646
    move-object/from16 v0, v29

    #@426
    iget v3, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->recordNum:I

    #@428
    add-int/lit8 v3, v3, 0x1

    #@42a
    move-object/from16 v0, v29

    #@42c
    iput v3, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->recordNum:I

    #@42e
    .line 648
    move-object/from16 v0, v29

    #@430
    iget v3, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->recordNum:I

    #@432
    move-object/from16 v0, v29

    #@434
    iget v4, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->countRecords:I

    #@436
    if-le v3, v4, :cond_446

    #@438
    .line 649
    move-object/from16 v0, v29

    #@43a
    iget-object v3, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->results:Ljava/util/ArrayList;

    #@43c
    const/4 v4, 0x0

    #@43d
    move-object/from16 v0, p0

    #@43f
    move-object/from16 v1, v33

    #@441
    invoke-direct {v0, v1, v3, v4}, Lcom/android/internal/telephony/uicc/IccFileHandler;->sendResult(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@444
    goto/16 :goto_d

    #@446
    .line 651
    :cond_446
    if-nez v6, :cond_452

    #@448
    .line 652
    move-object/from16 v0, v29

    #@44a
    iget v3, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->efid:I

    #@44c
    move-object/from16 v0, p0

    #@44e
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->getEFPath(I)Ljava/lang/String;

    #@451
    move-result-object v6

    #@452
    .line 655
    :cond_452
    move-object/from16 v0, p0

    #@454
    iget-object v14, v0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@456
    const/16 v15, 0xb2

    #@458
    move-object/from16 v0, v29

    #@45a
    iget v0, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->efid:I

    #@45c
    move/from16 v16, v0

    #@45e
    move-object/from16 v0, v29

    #@460
    iget v0, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->recordNum:I

    #@462
    move/from16 v18, v0

    #@464
    const/16 v19, 0x4

    #@466
    move-object/from16 v0, v29

    #@468
    iget v0, v0, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;->recordSize:I

    #@46a
    move/from16 v20, v0

    #@46c
    const/16 v21, 0x0

    #@46e
    const/16 v22, 0x0

    #@470
    move-object/from16 v0, p0

    #@472
    iget-object v0, v0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mAid:Ljava/lang/String;

    #@474
    move-object/from16 v23, v0

    #@476
    const/4 v3, 0x7

    #@477
    move-object/from16 v0, p0

    #@479
    move-object/from16 v1, v29

    #@47b
    invoke-virtual {v0, v3, v1}, Lcom/android/internal/telephony/uicc/IccFileHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@47e
    move-result-object v24

    #@47f
    move-object/from16 v17, v6

    #@481
    invoke-interface/range {v14 .. v24}, Lcom/android/internal/telephony/CommandsInterface;->iccIOForApp(IILjava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    :try_end_484
    .catch Ljava/lang/Exception; {:try_start_3f5 .. :try_end_484} :catch_255

    #@484
    goto/16 :goto_d

    #@486
    .line 666
    .end local v6           #path:Ljava/lang/String;
    .end local v25           #ar:Landroid/os/AsyncResult;
    .end local v29           #lc:Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;
    .end local v34           #result:Lcom/android/internal/telephony/uicc/IccIoResult;
    .restart local v30       #path:Ljava/lang/String;
    :pswitch_486
    :try_start_486
    move-object/from16 v0, p1

    #@488
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@48a
    move-object/from16 v25, v0

    #@48c
    check-cast v25, Landroid/os/AsyncResult;

    #@48e
    .line 667
    .restart local v25       #ar:Landroid/os/AsyncResult;
    move-object/from16 v0, v25

    #@490
    iget-object v3, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@492
    move-object v0, v3

    #@493
    check-cast v0, Landroid/os/Message;

    #@495
    move-object/from16 v33, v0

    #@497
    .line 668
    move-object/from16 v0, v25

    #@499
    iget-object v0, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@49b
    move-object/from16 v34, v0

    #@49d
    check-cast v34, Lcom/android/internal/telephony/uicc/IccIoResult;

    #@49f
    .line 670
    .restart local v34       #result:Lcom/android/internal/telephony/uicc/IccIoResult;
    move-object/from16 v0, p1

    #@4a1
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4a3
    check-cast v3, Landroid/os/AsyncResult;

    #@4a5
    move-object/from16 v0, p0

    #@4a7
    move-object/from16 v1, v33

    #@4a9
    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->processException(Landroid/os/Message;Landroid/os/AsyncResult;)Z

    #@4ac
    move-result v3

    #@4ad
    if-eqz v3, :cond_4b3

    #@4af
    move-object/from16 v6, v30

    #@4b1
    .line 671
    .end local v30           #path:Ljava/lang/String;
    .restart local v6       #path:Ljava/lang/String;
    goto/16 :goto_d

    #@4b3
    .line 674
    .end local v6           #path:Ljava/lang/String;
    .restart local v30       #path:Ljava/lang/String;
    :cond_4b3
    move-object/from16 v0, v34

    #@4b5
    iget-object v3, v0, Lcom/android/internal/telephony/uicc/IccIoResult;->payload:[B

    #@4b7
    const/4 v4, 0x0

    #@4b8
    move-object/from16 v0, p0

    #@4ba
    move-object/from16 v1, v33

    #@4bc
    invoke-direct {v0, v1, v3, v4}, Lcom/android/internal/telephony/uicc/IccFileHandler;->sendResult(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)V
    :try_end_4bf
    .catch Ljava/lang/Exception; {:try_start_486 .. :try_end_4bf} :catch_85

    #@4bf
    goto/16 :goto_b

    #@4c1
    .line 681
    .end local v25           #ar:Landroid/os/AsyncResult;
    .end local v30           #path:Ljava/lang/String;
    .end local v34           #result:Lcom/android/internal/telephony/uicc/IccIoResult;
    .restart local v6       #path:Ljava/lang/String;
    .restart local v27       #exc:Ljava/lang/Exception;
    :cond_4c1
    new-instance v3, Ljava/lang/StringBuilder;

    #@4c3
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4c6
    const-string v4, "uncaught exception"

    #@4c8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4cb
    move-result-object v3

    #@4cc
    move-object/from16 v0, v27

    #@4ce
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4d1
    move-result-object v3

    #@4d2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d5
    move-result-object v3

    #@4d6
    move-object/from16 v0, p0

    #@4d8
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loge(Ljava/lang/String;)V

    #@4db
    goto/16 :goto_d

    #@4dd
    .line 440
    nop

    #@4de
    :pswitch_data_4de
    .packed-switch 0x4
        :pswitch_33b
        :pswitch_486
        :pswitch_203
        :pswitch_3d3
        :pswitch_182
        :pswitch_d7
        :pswitch_137
        :pswitch_e
    .end packed-switch
.end method

.method public loadEFImgLinearFixed(ILandroid/os/Message;)V
    .registers 14
    .parameter "recordNum"
    .parameter "onLoaded"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/16 v2, 0x4f20

    #@3
    .line 210
    const/16 v0, 0xb

    #@5
    new-instance v1, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;

    #@7
    invoke-direct {v1, v2, p1, p2}, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;-><init>(IILandroid/os/Message;)V

    #@a
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/uicc/IccFileHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@d
    move-result-object v10

    #@e
    .line 214
    .local v10, response:Landroid/os/Message;
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@10
    const/16 v1, 0xc0

    #@12
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->getEFPath(I)Ljava/lang/String;

    #@15
    move-result-object v3

    #@16
    const/4 v5, 0x4

    #@17
    const/16 v6, 0xa

    #@19
    iget-object v9, p0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mAid:Ljava/lang/String;

    #@1b
    move v4, p1

    #@1c
    move-object v8, v7

    #@1d
    invoke-interface/range {v0 .. v10}, Lcom/android/internal/telephony/CommandsInterface;->iccIOForApp(IILjava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@20
    .line 218
    return-void
.end method

.method public loadEFImgTransparent(IIIILandroid/os/Message;)V
    .registers 18
    .parameter "fileid"
    .parameter "highOffset"
    .parameter "lowOffset"
    .parameter "length"
    .parameter "onLoaded"

    #@0
    .prologue
    .line 331
    const/16 v1, 0xa

    #@2
    const/4 v2, 0x0

    #@3
    move-object/from16 v0, p5

    #@5
    invoke-virtual {p0, v1, p1, v2, v0}, Lcom/android/internal/telephony/uicc/IccFileHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@8
    move-result-object v11

    #@9
    .line 334
    .local v11, response:Landroid/os/Message;
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v2, "IccFileHandler: loadEFImgTransparent fileid = "

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    const-string v2, " filePath = "

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    const/16 v2, 0x4f20

    #@20
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->getEFPath(I)Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    const-string v2, " highOffset = "

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    const-string v2, " lowOffset = "

    #@34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v1

    #@3c
    const-string v2, " length = "

    #@3e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v1

    #@42
    move/from16 v0, p4

    #@44
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@47
    move-result-object v1

    #@48
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v1

    #@4c
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/IccFileHandler;->logd(Ljava/lang/String;)V

    #@4f
    .line 340
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@51
    const/16 v2, 0xb0

    #@53
    const/16 v3, 0x4f20

    #@55
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->getEFPath(I)Ljava/lang/String;

    #@58
    move-result-object v4

    #@59
    const/4 v8, 0x0

    #@5a
    const/4 v9, 0x0

    #@5b
    iget-object v10, p0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mAid:Ljava/lang/String;

    #@5d
    move v3, p1

    #@5e
    move v5, p2

    #@5f
    move v6, p3

    #@60
    move/from16 v7, p4

    #@62
    invoke-interface/range {v1 .. v11}, Lcom/android/internal/telephony/CommandsInterface;->iccIOForApp(IILjava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@65
    .line 342
    return-void
.end method

.method public loadEFLinearFixed(IILandroid/os/Message;)V
    .registers 5
    .parameter "fileid"
    .parameter "recordNum"
    .parameter "onLoaded"

    #@0
    .prologue
    .line 197
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/uicc/IccFileHandler;->getEFPath(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixed(ILjava/lang/String;ILandroid/os/Message;)V

    #@7
    .line 198
    return-void
.end method

.method public loadEFLinearFixed(ILjava/lang/String;ILandroid/os/Message;)V
    .registers 16
    .parameter "fileid"
    .parameter "path"
    .parameter "recordNum"
    .parameter "onLoaded"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v4, 0x0

    #@2
    .line 178
    const/4 v0, 0x6

    #@3
    new-instance v1, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;

    #@5
    invoke-direct {v1, p1, p3, p2, p4}, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;-><init>(IILjava/lang/String;Landroid/os/Message;)V

    #@8
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/uicc/IccFileHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@b
    move-result-object v10

    #@c
    .line 182
    .local v10, response:Landroid/os/Message;
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@e
    const/16 v1, 0xc0

    #@10
    const/16 v6, 0xf

    #@12
    iget-object v9, p0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mAid:Ljava/lang/String;

    #@14
    move v2, p1

    #@15
    move-object v3, p2

    #@16
    move v5, v4

    #@17
    move-object v8, v7

    #@18
    invoke-interface/range {v0 .. v10}, Lcom/android/internal/telephony/CommandsInterface;->iccIOForApp(IILjava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@1b
    .line 184
    return-void
.end method

.method public loadEFLinearFixedAll(ILandroid/os/Message;)V
    .registers 4
    .parameter "fileid"
    .parameter "onLoaded"

    #@0
    .prologue
    .line 261
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/uicc/IccFileHandler;->getEFPath(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, p1, v0, p2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixedAll(ILjava/lang/String;Landroid/os/Message;)V

    #@7
    .line 262
    return-void
.end method

.method public loadEFLinearFixedAll(ILjava/lang/String;Landroid/os/Message;)V
    .registers 15
    .parameter "fileid"
    .parameter "path"
    .parameter "onLoaded"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v4, 0x0

    #@2
    .line 275
    const/4 v0, 0x6

    #@3
    new-instance v1, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;

    #@5
    invoke-direct {v1, p1, p2, p3}, Lcom/android/internal/telephony/uicc/IccFileHandler$LoadLinearFixedContext;-><init>(ILjava/lang/String;Landroid/os/Message;)V

    #@8
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/uicc/IccFileHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@b
    move-result-object v10

    #@c
    .line 278
    .local v10, response:Landroid/os/Message;
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@e
    const/16 v1, 0xc0

    #@10
    const/16 v6, 0xf

    #@12
    iget-object v9, p0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mAid:Ljava/lang/String;

    #@14
    move v2, p1

    #@15
    move-object v3, p2

    #@16
    move v5, v4

    #@17
    move-object v8, v7

    #@18
    invoke-interface/range {v0 .. v10}, Lcom/android/internal/telephony/CommandsInterface;->iccIOForApp(IILjava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@1b
    .line 280
    return-void
.end method

.method public loadEFTransparent(IILandroid/os/Message;)V
    .registers 15
    .parameter "fileid"
    .parameter "size"
    .parameter "onLoaded"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v4, 0x0

    #@2
    .line 312
    const/4 v0, 0x5

    #@3
    invoke-virtual {p0, v0, p1, v4, p3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@6
    move-result-object v10

    #@7
    .line 315
    .local v10, response:Landroid/os/Message;
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@9
    const/16 v1, 0xb0

    #@b
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/uicc/IccFileHandler;->getEFPath(I)Ljava/lang/String;

    #@e
    move-result-object v3

    #@f
    iget-object v9, p0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mAid:Ljava/lang/String;

    #@11
    move v2, p1

    #@12
    move v5, v4

    #@13
    move v6, p2

    #@14
    move-object v8, v7

    #@15
    invoke-interface/range {v0 .. v10}, Lcom/android/internal/telephony/CommandsInterface;->iccIOForApp(IILjava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@18
    .line 317
    return-void
.end method

.method public loadEFTransparent(ILandroid/os/Message;)V
    .registers 14
    .parameter "fileid"
    .parameter "onLoaded"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v4, 0x0

    #@2
    .line 294
    const/4 v0, 0x4

    #@3
    invoke-virtual {p0, v0, p1, v4, p2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@6
    move-result-object v10

    #@7
    .line 297
    .local v10, response:Landroid/os/Message;
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@9
    const/16 v1, 0xc0

    #@b
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/uicc/IccFileHandler;->getEFPath(I)Ljava/lang/String;

    #@e
    move-result-object v3

    #@f
    const/16 v6, 0xf

    #@11
    iget-object v9, p0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mAid:Ljava/lang/String;

    #@13
    move v2, p1

    #@14
    move v5, v4

    #@15
    move-object v8, v7

    #@16
    invoke-interface/range {v0 .. v10}, Lcom/android/internal/telephony/CommandsInterface;->iccIOForApp(IILjava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@19
    .line 299
    return-void
.end method

.method protected abstract logd(Ljava/lang/String;)V
.end method

.method protected abstract loge(Ljava/lang/String;)V
.end method

.method public updateEFLinearFixed(II[BLjava/lang/String;Landroid/os/Message;)V
    .registers 17
    .parameter "fileid"
    .parameter "recordNum"
    .parameter "data"
    .parameter "pin2"
    .parameter "onComplete"

    #@0
    .prologue
    .line 372
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    const/16 v1, 0xdc

    #@4
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/uicc/IccFileHandler;->getEFPath(I)Ljava/lang/String;

    #@7
    move-result-object v3

    #@8
    const/4 v5, 0x4

    #@9
    array-length v6, p3

    #@a
    invoke-static {p3}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@d
    move-result-object v7

    #@e
    iget-object v9, p0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mAid:Ljava/lang/String;

    #@10
    move v2, p1

    #@11
    move v4, p2

    #@12
    move-object v8, p4

    #@13
    move-object/from16 v10, p5

    #@15
    invoke-interface/range {v0 .. v10}, Lcom/android/internal/telephony/CommandsInterface;->iccIOForApp(IILjava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@18
    .line 375
    return-void
.end method

.method public updateEFLinearFixed(ILjava/lang/String;I[BLjava/lang/String;Landroid/os/Message;)V
    .registers 18
    .parameter "fileid"
    .parameter "path"
    .parameter "recordNum"
    .parameter "data"
    .parameter "pin2"
    .parameter "onComplete"

    #@0
    .prologue
    .line 356
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    const/16 v1, 0xdc

    #@4
    const/4 v5, 0x4

    #@5
    array-length v6, p4

    #@6
    invoke-static {p4}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@9
    move-result-object v7

    #@a
    iget-object v9, p0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mAid:Ljava/lang/String;

    #@c
    move v2, p1

    #@d
    move-object v3, p2

    #@e
    move v4, p3

    #@f
    move-object/from16 v8, p5

    #@11
    move-object/from16 v10, p6

    #@13
    invoke-interface/range {v0 .. v10}, Lcom/android/internal/telephony/CommandsInterface;->iccIOForApp(IILjava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@16
    .line 359
    return-void
.end method

.method public updateEFTransparent(I[BLandroid/os/Message;)V
    .registers 15
    .parameter "fileid"
    .parameter "data"
    .parameter "onComplete"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 383
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@3
    const/16 v1, 0xd6

    #@5
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/uicc/IccFileHandler;->getEFPath(I)Ljava/lang/String;

    #@8
    move-result-object v3

    #@9
    array-length v6, p2

    #@a
    invoke-static {p2}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@d
    move-result-object v7

    #@e
    const/4 v8, 0x0

    #@f
    iget-object v9, p0, Lcom/android/internal/telephony/uicc/IccFileHandler;->mAid:Ljava/lang/String;

    #@11
    move v2, p1

    #@12
    move v5, v4

    #@13
    move-object v10, p3

    #@14
    invoke-interface/range {v0 .. v10}, Lcom/android/internal/telephony/CommandsInterface;->iccIOForApp(IILjava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@17
    .line 386
    return-void
.end method
