.class Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimCdmaHomeLoaded;
.super Ljava/lang/Object;
.source "RuimRecords.java"

# interfaces
.implements Lcom/android/internal/telephony/uicc/IccRecords$IccRecordLoaded;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/uicc/RuimRecords;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EfCsimCdmaHomeLoaded"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/uicc/RuimRecords;


# direct methods
.method private constructor <init>(Lcom/android/internal/telephony/uicc/RuimRecords;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 491
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimCdmaHomeLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/telephony/uicc/RuimRecords;Lcom/android/internal/telephony/uicc/RuimRecords$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 491
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimCdmaHomeLoaded;-><init>(Lcom/android/internal/telephony/uicc/RuimRecords;)V

    #@3
    return-void
.end method


# virtual methods
.method public getEfName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 493
    const-string v0, "EF_CSIM_CDMAHOME"

    #@2
    return-object v0
.end method

.method public onRecordLoaded(Landroid/os/AsyncResult;)V
    .registers 13
    .parameter "ar"

    #@0
    .prologue
    const/16 v10, 0x2c

    #@2
    .line 498
    iget-object v1, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@4
    check-cast v1, Ljava/util/ArrayList;

    #@6
    .line 499
    .local v1, dataList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    iget-object v7, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimCdmaHomeLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@8
    new-instance v8, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v9, "CSIM_CDMAHOME data size="

    #@f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v8

    #@13
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@16
    move-result v9

    #@17
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v8

    #@1b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v8

    #@1f
    invoke-virtual {v7, v8}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@22
    .line 500
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    #@25
    move-result v7

    #@26
    if-eqz v7, :cond_29

    #@28
    .line 520
    :goto_28
    return-void

    #@29
    .line 503
    :cond_29
    new-instance v6, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    .line 504
    .local v6, sidBuf:Ljava/lang/StringBuilder;
    new-instance v4, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    .line 506
    .local v4, nidBuf:Ljava/lang/StringBuilder;
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@36
    move-result-object v2

    #@37
    .local v2, i$:Ljava/util/Iterator;
    :cond_37
    :goto_37
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@3a
    move-result v7

    #@3b
    if-eqz v7, :cond_72

    #@3d
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@40
    move-result-object v0

    #@41
    check-cast v0, [B

    #@43
    .line 507
    .local v0, data:[B
    array-length v7, v0

    #@44
    const/4 v8, 0x5

    #@45
    if-ne v7, v8, :cond_37

    #@47
    .line 508
    const/4 v7, 0x1

    #@48
    aget-byte v7, v0, v7

    #@4a
    and-int/lit16 v7, v7, 0xff

    #@4c
    shl-int/lit8 v7, v7, 0x8

    #@4e
    const/4 v8, 0x0

    #@4f
    aget-byte v8, v0, v8

    #@51
    and-int/lit16 v8, v8, 0xff

    #@53
    or-int v5, v7, v8

    #@55
    .line 509
    .local v5, sid:I
    const/4 v7, 0x3

    #@56
    aget-byte v7, v0, v7

    #@58
    and-int/lit16 v7, v7, 0xff

    #@5a
    shl-int/lit8 v7, v7, 0x8

    #@5c
    const/4 v8, 0x2

    #@5d
    aget-byte v8, v0, v8

    #@5f
    and-int/lit16 v8, v8, 0xff

    #@61
    or-int v3, v7, v8

    #@63
    .line 510
    .local v3, nid:I
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@66
    move-result-object v7

    #@67
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@6a
    .line 511
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v7

    #@6e
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@71
    goto :goto_37

    #@72
    .line 515
    .end local v0           #data:[B
    .end local v3           #nid:I
    .end local v5           #sid:I
    :cond_72
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    #@75
    move-result v7

    #@76
    add-int/lit8 v7, v7, -0x1

    #@78
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    #@7b
    .line 516
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    #@7e
    move-result v7

    #@7f
    add-int/lit8 v7, v7, -0x1

    #@81
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    #@84
    .line 518
    iget-object v7, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimCdmaHomeLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@86
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v8

    #@8a
    invoke-static {v7, v8}, Lcom/android/internal/telephony/uicc/RuimRecords;->access$502(Lcom/android/internal/telephony/uicc/RuimRecords;Ljava/lang/String;)Ljava/lang/String;

    #@8d
    .line 519
    iget-object v7, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimCdmaHomeLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@8f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@92
    move-result-object v8

    #@93
    invoke-static {v7, v8}, Lcom/android/internal/telephony/uicc/RuimRecords;->access$602(Lcom/android/internal/telephony/uicc/RuimRecords;Ljava/lang/String;)Ljava/lang/String;

    #@96
    goto :goto_28
.end method
