.class public final enum Lcom/android/internal/telephony/DriverCall$State;
.super Ljava/lang/Enum;
.source "DriverCall.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/DriverCall;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/DriverCall$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/DriverCall$State;

.field public static final enum ACTIVE:Lcom/android/internal/telephony/DriverCall$State;

.field public static final enum ALERTING:Lcom/android/internal/telephony/DriverCall$State;

.field public static final enum DIALING:Lcom/android/internal/telephony/DriverCall$State;

.field public static final enum HOLDING:Lcom/android/internal/telephony/DriverCall$State;

.field public static final enum INCOMING:Lcom/android/internal/telephony/DriverCall$State;

.field public static final enum WAITING:Lcom/android/internal/telephony/DriverCall$State;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 36
    new-instance v0, Lcom/android/internal/telephony/DriverCall$State;

    #@7
    const-string v1, "ACTIVE"

    #@9
    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/DriverCall$State;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/android/internal/telephony/DriverCall$State;->ACTIVE:Lcom/android/internal/telephony/DriverCall$State;

    #@e
    .line 37
    new-instance v0, Lcom/android/internal/telephony/DriverCall$State;

    #@10
    const-string v1, "HOLDING"

    #@12
    invoke-direct {v0, v1, v4}, Lcom/android/internal/telephony/DriverCall$State;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/android/internal/telephony/DriverCall$State;->HOLDING:Lcom/android/internal/telephony/DriverCall$State;

    #@17
    .line 38
    new-instance v0, Lcom/android/internal/telephony/DriverCall$State;

    #@19
    const-string v1, "DIALING"

    #@1b
    invoke-direct {v0, v1, v5}, Lcom/android/internal/telephony/DriverCall$State;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/android/internal/telephony/DriverCall$State;->DIALING:Lcom/android/internal/telephony/DriverCall$State;

    #@20
    .line 39
    new-instance v0, Lcom/android/internal/telephony/DriverCall$State;

    #@22
    const-string v1, "ALERTING"

    #@24
    invoke-direct {v0, v1, v6}, Lcom/android/internal/telephony/DriverCall$State;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/android/internal/telephony/DriverCall$State;->ALERTING:Lcom/android/internal/telephony/DriverCall$State;

    #@29
    .line 40
    new-instance v0, Lcom/android/internal/telephony/DriverCall$State;

    #@2b
    const-string v1, "INCOMING"

    #@2d
    invoke-direct {v0, v1, v7}, Lcom/android/internal/telephony/DriverCall$State;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/android/internal/telephony/DriverCall$State;->INCOMING:Lcom/android/internal/telephony/DriverCall$State;

    #@32
    .line 41
    new-instance v0, Lcom/android/internal/telephony/DriverCall$State;

    #@34
    const-string v1, "WAITING"

    #@36
    const/4 v2, 0x5

    #@37
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/DriverCall$State;-><init>(Ljava/lang/String;I)V

    #@3a
    sput-object v0, Lcom/android/internal/telephony/DriverCall$State;->WAITING:Lcom/android/internal/telephony/DriverCall$State;

    #@3c
    .line 35
    const/4 v0, 0x6

    #@3d
    new-array v0, v0, [Lcom/android/internal/telephony/DriverCall$State;

    #@3f
    sget-object v1, Lcom/android/internal/telephony/DriverCall$State;->ACTIVE:Lcom/android/internal/telephony/DriverCall$State;

    #@41
    aput-object v1, v0, v3

    #@43
    sget-object v1, Lcom/android/internal/telephony/DriverCall$State;->HOLDING:Lcom/android/internal/telephony/DriverCall$State;

    #@45
    aput-object v1, v0, v4

    #@47
    sget-object v1, Lcom/android/internal/telephony/DriverCall$State;->DIALING:Lcom/android/internal/telephony/DriverCall$State;

    #@49
    aput-object v1, v0, v5

    #@4b
    sget-object v1, Lcom/android/internal/telephony/DriverCall$State;->ALERTING:Lcom/android/internal/telephony/DriverCall$State;

    #@4d
    aput-object v1, v0, v6

    #@4f
    sget-object v1, Lcom/android/internal/telephony/DriverCall$State;->INCOMING:Lcom/android/internal/telephony/DriverCall$State;

    #@51
    aput-object v1, v0, v7

    #@53
    const/4 v1, 0x5

    #@54
    sget-object v2, Lcom/android/internal/telephony/DriverCall$State;->WAITING:Lcom/android/internal/telephony/DriverCall$State;

    #@56
    aput-object v2, v0, v1

    #@58
    sput-object v0, Lcom/android/internal/telephony/DriverCall$State;->$VALUES:[Lcom/android/internal/telephony/DriverCall$State;

    #@5a
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/DriverCall$State;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 35
    const-class v0, Lcom/android/internal/telephony/DriverCall$State;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/DriverCall$State;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/DriverCall$State;
    .registers 1

    #@0
    .prologue
    .line 35
    sget-object v0, Lcom/android/internal/telephony/DriverCall$State;->$VALUES:[Lcom/android/internal/telephony/DriverCall$State;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/DriverCall$State;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/DriverCall$State;

    #@8
    return-object v0
.end method
