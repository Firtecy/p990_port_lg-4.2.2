.class public Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;
.super Landroid/os/Handler;
.source "UsimPhoneBookManager.java"

# interfaces
.implements Lcom/android/internal/telephony/uicc/IccConstants;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/gsm/UsimPhoneBookManager$PbrFile;
    }
.end annotation


# static fields
.field private static final DBG:Z = true

.field private static final EVENT_EMAIL_LOAD_DONE:I = 0x4

.field private static final EVENT_IAP_LOAD_DONE:I = 0x3

.field private static final EVENT_PBR_LOAD_DONE:I = 0x1

.field private static final EVENT_USIM_ADN_LOAD_DONE:I = 0x2

.field private static final LOG_TAG:Ljava/lang/String; = "GSM"

.field private static final USIM_EFAAS_TAG:I = 0xc7

.field private static final USIM_EFADN_TAG:I = 0xc0

.field private static final USIM_EFANR_TAG:I = 0xc4

.field private static final USIM_EFCCP1_TAG:I = 0xcb

.field private static final USIM_EFEMAIL_TAG:I = 0xca

.field private static final USIM_EFEXT1_TAG:I = 0xc2

.field private static final USIM_EFGRP_TAG:I = 0xc6

.field private static final USIM_EFGSD_TAG:I = 0xc8

.field private static final USIM_EFIAP_TAG:I = 0xc1

.field private static final USIM_EFPBC_TAG:I = 0xc5

.field private static final USIM_EFSNE_TAG:I = 0xc3

.field private static final USIM_EFUID_TAG:I = 0xc9

.field private static final USIM_TYPE1_TAG:I = 0xa8

.field private static final USIM_TYPE2_TAG:I = 0xa9

.field private static final USIM_TYPE3_TAG:I = 0xaa


# instance fields
.field private mAdnCache:Lcom/android/internal/telephony/uicc/AdnRecordCache;

.field private mEmailFileRecord:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[B>;"
        }
    .end annotation
.end field

.field private mEmailPresentInIap:Z

.field private mEmailTagNumberInIap:I

.field private mEmailsForAdnRec:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

.field private mIapFileRecord:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[B>;"
        }
    .end annotation
.end field

.field private mIsPbrPresent:Ljava/lang/Boolean;

.field private mLock:Ljava/lang/Object;

.field private mPbrFile:Lcom/android/internal/telephony/gsm/UsimPhoneBookManager$PbrFile;

.field private mPhoneBookRecords:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/uicc/AdnRecord;",
            ">;"
        }
    .end annotation
.end field

.field private mRefreshCache:Z


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/uicc/IccFileHandler;Lcom/android/internal/telephony/uicc/AdnRecordCache;)V
    .registers 5
    .parameter "fh"
    .parameter "cache"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 80
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@4
    .line 50
    new-instance v0, Ljava/lang/Object;

    #@6
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@9
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mLock:Ljava/lang/Object;

    #@b
    .line 52
    iput-boolean v1, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mEmailPresentInIap:Z

    #@d
    .line 53
    iput v1, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mEmailTagNumberInIap:I

    #@f
    .line 57
    iput-boolean v1, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mRefreshCache:Z

    #@11
    .line 81
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@13
    .line 82
    new-instance v0, Ljava/util/ArrayList;

    #@15
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@18
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mPhoneBookRecords:Ljava/util/ArrayList;

    #@1a
    .line 83
    const/4 v0, 0x0

    #@1b
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mPbrFile:Lcom/android/internal/telephony/gsm/UsimPhoneBookManager$PbrFile;

    #@1d
    .line 86
    const/4 v0, 0x1

    #@1e
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@21
    move-result-object v0

    #@22
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mIsPbrPresent:Ljava/lang/Boolean;

    #@24
    .line 87
    iput-object p2, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mAdnCache:Lcom/android/internal/telephony/uicc/AdnRecordCache;

    #@26
    .line 88
    return-void
.end method

.method static synthetic access$002(Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mEmailPresentInIap:Z

    #@2
    return p1
.end method

.method static synthetic access$102(Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 43
    iput p1, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mEmailTagNumberInIap:I

    #@2
    return p1
.end method

.method private createPbrFile(Ljava/util/ArrayList;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[B>;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 397
    .local p1, records:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    if-nez p1, :cond_d

    #@2
    .line 398
    const/4 v0, 0x0

    #@3
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mPbrFile:Lcom/android/internal/telephony/gsm/UsimPhoneBookManager$PbrFile;

    #@5
    .line 399
    const/4 v0, 0x0

    #@6
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mIsPbrPresent:Ljava/lang/Boolean;

    #@c
    .line 403
    :goto_c
    return-void

    #@d
    .line 402
    :cond_d
    new-instance v0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager$PbrFile;

    #@f
    invoke-direct {v0, p0, p1}, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager$PbrFile;-><init>(Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;Ljava/util/ArrayList;)V

    #@12
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mPbrFile:Lcom/android/internal/telephony/gsm/UsimPhoneBookManager$PbrFile;

    #@14
    goto :goto_c
.end method

.method private log(Ljava/lang/String;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 532
    const-string v0, "GSM"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 533
    return-void
.end method

.method private readAdnFileAndWait(I)V
    .registers 8
    .parameter "recNum"

    #@0
    .prologue
    const/16 v5, 0xc2

    #@2
    .line 373
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mPbrFile:Lcom/android/internal/telephony/gsm/UsimPhoneBookManager$PbrFile;

    #@4
    if-nez v3, :cond_7

    #@6
    .line 394
    :cond_6
    :goto_6
    return-void

    #@7
    .line 376
    :cond_7
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mPbrFile:Lcom/android/internal/telephony/gsm/UsimPhoneBookManager$PbrFile;

    #@9
    iget-object v3, v3, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager$PbrFile;->mFileIds:Ljava/util/HashMap;

    #@b
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@12
    move-result-object v2

    #@13
    check-cast v2, Ljava/util/Map;

    #@15
    .line 378
    .local v2, fileIds:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    if-eqz v2, :cond_6

    #@17
    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    #@1a
    move-result v3

    #@1b
    if-nez v3, :cond_6

    #@1d
    .line 381
    const/4 v1, 0x0

    #@1e
    .line 383
    .local v1, extEf:I
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@21
    move-result-object v3

    #@22
    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    #@25
    move-result v3

    #@26
    if-eqz v3, :cond_36

    #@28
    .line 384
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2b
    move-result-object v3

    #@2c
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2f
    move-result-object v3

    #@30
    check-cast v3, Ljava/lang/Integer;

    #@32
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@35
    move-result v1

    #@36
    .line 387
    :cond_36
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mAdnCache:Lcom/android/internal/telephony/uicc/AdnRecordCache;

    #@38
    const/16 v3, 0xc0

    #@3a
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3d
    move-result-object v3

    #@3e
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@41
    move-result-object v3

    #@42
    check-cast v3, Ljava/lang/Integer;

    #@44
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@47
    move-result v3

    #@48
    const/4 v5, 0x2

    #@49
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->obtainMessage(I)Landroid/os/Message;

    #@4c
    move-result-object v5

    #@4d
    invoke-virtual {v4, v3, v1, v5}, Lcom/android/internal/telephony/uicc/AdnRecordCache;->requestLoadAllAdnLike(IILandroid/os/Message;)V

    #@50
    .line 390
    :try_start_50
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mLock:Ljava/lang/Object;

    #@52
    invoke-virtual {v3}, Ljava/lang/Object;->wait()V
    :try_end_55
    .catch Ljava/lang/InterruptedException; {:try_start_50 .. :try_end_55} :catch_56

    #@55
    goto :goto_6

    #@56
    .line 391
    :catch_56
    move-exception v0

    #@57
    .line 392
    .local v0, e:Ljava/lang/InterruptedException;
    const-string v3, "GSM"

    #@59
    const-string v4, "Interrupted Exception in readAdnFileAndWait"

    #@5b
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    goto :goto_6
.end method

.method private readEmailFileAndWait(I)V
    .registers 8
    .parameter "recNum"

    #@0
    .prologue
    const/16 v5, 0xca

    #@2
    .line 154
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mPbrFile:Lcom/android/internal/telephony/gsm/UsimPhoneBookManager$PbrFile;

    #@4
    if-nez v3, :cond_7

    #@6
    .line 190
    :cond_6
    :goto_6
    return-void

    #@7
    .line 157
    :cond_7
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mPbrFile:Lcom/android/internal/telephony/gsm/UsimPhoneBookManager$PbrFile;

    #@9
    iget-object v3, v3, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager$PbrFile;->mFileIds:Ljava/util/HashMap;

    #@b
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@12
    move-result-object v2

    #@13
    check-cast v2, Ljava/util/Map;

    #@15
    .line 159
    .local v2, fileIds:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    if-eqz v2, :cond_6

    #@17
    .line 161
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v3

    #@1b
    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_6

    #@21
    .line 162
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@24
    move-result-object v3

    #@25
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@28
    move-result-object v3

    #@29
    check-cast v3, Ljava/lang/Integer;

    #@2b
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@2e
    move-result v1

    #@2f
    .line 167
    .local v1, efid:I
    iget-boolean v3, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mEmailPresentInIap:Z

    #@31
    if-eqz v3, :cond_52

    #@33
    .line 168
    const/16 v3, 0xc1

    #@35
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@38
    move-result-object v3

    #@39
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3c
    move-result-object v3

    #@3d
    check-cast v3, Ljava/lang/Integer;

    #@3f
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@42
    move-result v3

    #@43
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->readIapFileAndWait(I)V

    #@46
    .line 169
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mIapFileRecord:Ljava/util/ArrayList;

    #@48
    if-nez v3, :cond_52

    #@4a
    .line 170
    const-string v3, "GSM"

    #@4c
    const-string v4, "Error: IAP file is empty"

    #@4e
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    goto :goto_6

    #@52
    .line 175
    :cond_52
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@54
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@57
    move-result-object v3

    #@58
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5b
    move-result-object v3

    #@5c
    check-cast v3, Ljava/lang/Integer;

    #@5e
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@61
    move-result v3

    #@62
    const/4 v5, 0x4

    #@63
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->obtainMessage(I)Landroid/os/Message;

    #@66
    move-result-object v5

    #@67
    invoke-virtual {v4, v3, v5}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixedAll(ILandroid/os/Message;)V

    #@6a
    .line 178
    :try_start_6a
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mLock:Ljava/lang/Object;

    #@6c
    invoke-virtual {v3}, Ljava/lang/Object;->wait()V
    :try_end_6f
    .catch Ljava/lang/InterruptedException; {:try_start_6a .. :try_end_6f} :catch_7b

    #@6f
    .line 183
    :goto_6f
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mEmailFileRecord:Ljava/util/ArrayList;

    #@71
    if-nez v3, :cond_84

    #@73
    .line 184
    const-string v3, "GSM"

    #@75
    const-string v4, "Error: Email file is empty"

    #@77
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    goto :goto_6

    #@7b
    .line 179
    :catch_7b
    move-exception v0

    #@7c
    .line 180
    .local v0, e:Ljava/lang/InterruptedException;
    const-string v3, "GSM"

    #@7e
    const-string v4, "Interrupted Exception in readEmailFileAndWait"

    #@80
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@83
    goto :goto_6f

    #@84
    .line 187
    .end local v0           #e:Ljava/lang/InterruptedException;
    :cond_84
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->updatePhoneAdnRecord()V

    #@87
    goto/16 :goto_6
.end method

.method private readEmailRecord(I)Ljava/lang/String;
    .registers 10
    .parameter "recNum"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 343
    const/4 v3, 0x0

    #@2
    .line 345
    .local v3, emailRec:[B
    const/4 v4, 0x0

    #@3
    .line 348
    .local v4, email_len:I
    :try_start_3
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mEmailFileRecord:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@8
    move-result-object v5

    #@9
    move-object v0, v5

    #@a
    check-cast v0, [B

    #@c
    move-object v3, v0
    :try_end_d
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 .. :try_end_d} :catch_25

    #@d
    .line 356
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@10
    move-result-object v5

    #@11
    const-string v6, "DCM"

    #@13
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v5

    #@17
    if-eqz v5, :cond_2a

    #@19
    .line 357
    iget-boolean v5, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mEmailPresentInIap:Z

    #@1b
    if-eqz v5, :cond_28

    #@1d
    .line 358
    array-length v5, v3

    #@1e
    add-int/lit8 v4, v5, -0x2

    #@20
    .line 362
    :goto_20
    invoke-static {v3, v7, v4}, Lcom/android/internal/telephony/uicc/IccUtils;->adnStringFieldToString([BII)Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    .line 368
    :goto_24
    return-object v2

    #@25
    .line 349
    :catch_25
    move-exception v1

    #@26
    .line 350
    .local v1, e:Ljava/lang/IndexOutOfBoundsException;
    const/4 v2, 0x0

    #@27
    goto :goto_24

    #@28
    .line 360
    .end local v1           #e:Ljava/lang/IndexOutOfBoundsException;
    :cond_28
    array-length v4, v3

    #@29
    goto :goto_20

    #@2a
    .line 365
    :cond_2a
    array-length v5, v3

    #@2b
    add-int/lit8 v5, v5, -0x2

    #@2d
    invoke-static {v3, v7, v5}, Lcom/android/internal/telephony/uicc/IccUtils;->adnStringFieldToString([BII)Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    .local v2, email:Ljava/lang/String;
    goto :goto_24
.end method

.method private readIapFileAndWait(I)V
    .registers 5
    .parameter "efid"

    #@0
    .prologue
    .line 193
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@2
    const/4 v2, 0x3

    #@3
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->obtainMessage(I)Landroid/os/Message;

    #@6
    move-result-object v2

    #@7
    invoke-virtual {v1, p1, v2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixedAll(ILandroid/os/Message;)V

    #@a
    .line 195
    :try_start_a
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mLock:Ljava/lang/Object;

    #@c
    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_f} :catch_10

    #@f
    .line 199
    :goto_f
    return-void

    #@10
    .line 196
    :catch_10
    move-exception v0

    #@11
    .line 197
    .local v0, e:Ljava/lang/InterruptedException;
    const-string v1, "GSM"

    #@13
    const-string v2, "Interrupted Exception in readIapFileAndWait"

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    goto :goto_f
.end method

.method private readPbrFileAndWait()V
    .registers 5

    #@0
    .prologue
    .line 144
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@2
    const/16 v2, 0x4f30

    #@4
    const/4 v3, 0x1

    #@5
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->obtainMessage(I)Landroid/os/Message;

    #@8
    move-result-object v3

    #@9
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFLinearFixedAll(ILandroid/os/Message;)V

    #@c
    .line 146
    :try_start_c
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mLock:Ljava/lang/Object;

    #@e
    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_11
    .catch Ljava/lang/InterruptedException; {:try_start_c .. :try_end_11} :catch_12

    #@11
    .line 150
    :goto_11
    return-void

    #@12
    .line 147
    :catch_12
    move-exception v0

    #@13
    .line 148
    .local v0, e:Ljava/lang/InterruptedException;
    const-string v1, "GSM"

    #@15
    const-string v2, "Interrupted Exception in readAdnFileAndWait"

    #@17
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    goto :goto_11
.end method

.method private refreshCache()V
    .registers 4

    #@0
    .prologue
    .line 130
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mPbrFile:Lcom/android/internal/telephony/gsm/UsimPhoneBookManager$PbrFile;

    #@2
    if-nez v2, :cond_5

    #@4
    .line 137
    :cond_4
    return-void

    #@5
    .line 131
    :cond_5
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mPhoneBookRecords:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@a
    .line 133
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mPbrFile:Lcom/android/internal/telephony/gsm/UsimPhoneBookManager$PbrFile;

    #@c
    iget-object v2, v2, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager$PbrFile;->mFileIds:Ljava/util/HashMap;

    #@e
    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    #@11
    move-result v1

    #@12
    .line 134
    .local v1, numRecs:I
    const/4 v0, 0x0

    #@13
    .local v0, i:I
    :goto_13
    if-ge v0, v1, :cond_4

    #@15
    .line 135
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->readAdnFileAndWait(I)V

    #@18
    .line 134
    add-int/lit8 v0, v0, 0x1

    #@1a
    goto :goto_13
.end method

.method private updatePhoneAdnRecord()V
    .registers 15

    #@0
    .prologue
    const/4 v13, -0x1

    #@1
    const/4 v12, 0x1

    #@2
    const/4 v11, 0x0

    #@3
    .line 202
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mEmailFileRecord:Ljava/util/ArrayList;

    #@5
    if-nez v9, :cond_8

    #@7
    .line 296
    :cond_7
    :goto_7
    return-void

    #@8
    .line 203
    :cond_8
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mPhoneBookRecords:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@d
    move-result v5

    #@e
    .line 205
    .local v5, numAdnRecs:I
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@11
    move-result-object v9

    #@12
    const-string v10, "DCM"

    #@14
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v9

    #@18
    if-eqz v9, :cond_87

    #@1a
    .line 206
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mIapFileRecord:Ljava/util/ArrayList;

    #@1c
    if-eqz v9, :cond_5b

    #@1e
    iget-boolean v9, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mEmailPresentInIap:Z

    #@20
    if-ne v9, v12, :cond_5b

    #@22
    .line 212
    const/4 v3, 0x0

    #@23
    .local v3, i:I
    :goto_23
    if-ge v3, v5, :cond_5b

    #@25
    .line 213
    const/4 v8, 0x0

    #@26
    .line 215
    .local v8, record:[B
    :try_start_26
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mIapFileRecord:Ljava/util/ArrayList;

    #@28
    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2b
    move-result-object v8

    #@2c
    .end local v8           #record:[B
    check-cast v8, [B
    :try_end_2e
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_26 .. :try_end_2e} :catch_53

    #@2e
    .line 220
    .restart local v8       #record:[B
    iget v9, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mEmailTagNumberInIap:I

    #@30
    aget-byte v7, v8, v9

    #@32
    .line 222
    .local v7, recNum:I
    if-eq v7, v13, :cond_50

    #@34
    .line 223
    new-array v2, v12, [Ljava/lang/String;

    #@36
    .line 225
    .local v2, emails:[Ljava/lang/String;
    add-int/lit8 v9, v7, -0x1

    #@38
    invoke-direct {p0, v9}, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->readEmailRecord(I)Ljava/lang/String;

    #@3b
    move-result-object v9

    #@3c
    aput-object v9, v2, v11

    #@3e
    .line 226
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mPhoneBookRecords:Ljava/util/ArrayList;

    #@40
    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@43
    move-result-object v6

    #@44
    check-cast v6, Lcom/android/internal/telephony/uicc/AdnRecord;

    #@46
    .line 227
    .local v6, rec:Lcom/android/internal/telephony/uicc/AdnRecord;
    if-eqz v6, :cond_7d

    #@48
    .line 228
    invoke-virtual {v6, v2}, Lcom/android/internal/telephony/uicc/AdnRecord;->setEmails([Ljava/lang/String;)V

    #@4b
    .line 233
    :goto_4b
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mPhoneBookRecords:Ljava/util/ArrayList;

    #@4d
    invoke-virtual {v9, v3, v6}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@50
    .line 212
    .end local v2           #emails:[Ljava/lang/String;
    .end local v6           #rec:Lcom/android/internal/telephony/uicc/AdnRecord;
    :cond_50
    add-int/lit8 v3, v3, 0x1

    #@52
    goto :goto_23

    #@53
    .line 216
    .end local v7           #recNum:I
    .end local v8           #record:[B
    :catch_53
    move-exception v0

    #@54
    .line 217
    .local v0, e:Ljava/lang/IndexOutOfBoundsException;
    const-string v9, "GSM"

    #@56
    const-string v10, "Error: Improper ICC card: No IAP record for ADN, continuing"

    #@58
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    .line 274
    .end local v0           #e:Ljava/lang/IndexOutOfBoundsException;
    .end local v3           #i:I
    :cond_5b
    :goto_5b
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mPhoneBookRecords:Ljava/util/ArrayList;

    #@5d
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@60
    move-result v4

    #@61
    .line 277
    .local v4, len:I
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mEmailsForAdnRec:Ljava/util/Map;

    #@63
    if-nez v9, :cond_68

    #@65
    .line 278
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->parseType1EmailFile(I)V

    #@68
    .line 280
    :cond_68
    const/4 v3, 0x0

    #@69
    .restart local v3       #i:I
    :goto_69
    if-ge v3, v5, :cond_7

    #@6b
    .line 281
    const/4 v1, 0x0

    #@6c
    .line 283
    .local v1, emailList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_6c
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mEmailsForAdnRec:Ljava/util/Map;

    #@6e
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@71
    move-result-object v10

    #@72
    invoke-interface {v9, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@75
    move-result-object v1

    #@76
    .end local v1           #emailList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    check-cast v1, Ljava/util/ArrayList;
    :try_end_78
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_6c .. :try_end_78} :catch_cf

    #@78
    .line 287
    .restart local v1       #emailList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v1, :cond_d2

    #@7a
    .line 280
    :goto_7a
    add-int/lit8 v3, v3, 0x1

    #@7c
    goto :goto_69

    #@7d
    .line 231
    .end local v1           #emailList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v4           #len:I
    .restart local v2       #emails:[Ljava/lang/String;
    .restart local v6       #rec:Lcom/android/internal/telephony/uicc/AdnRecord;
    .restart local v7       #recNum:I
    .restart local v8       #record:[B
    :cond_7d
    new-instance v6, Lcom/android/internal/telephony/uicc/AdnRecord;

    #@7f
    .end local v6           #rec:Lcom/android/internal/telephony/uicc/AdnRecord;
    const-string v9, ""

    #@81
    const-string v10, ""

    #@83
    invoke-direct {v6, v9, v10, v2}, Lcom/android/internal/telephony/uicc/AdnRecord;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    #@86
    .restart local v6       #rec:Lcom/android/internal/telephony/uicc/AdnRecord;
    goto :goto_4b

    #@87
    .line 238
    .end local v2           #emails:[Ljava/lang/String;
    .end local v3           #i:I
    .end local v6           #rec:Lcom/android/internal/telephony/uicc/AdnRecord;
    .end local v7           #recNum:I
    .end local v8           #record:[B
    :cond_87
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mIapFileRecord:Ljava/util/ArrayList;

    #@89
    if-eqz v9, :cond_5b

    #@8b
    .line 244
    const/4 v3, 0x0

    #@8c
    .restart local v3       #i:I
    :goto_8c
    if-ge v3, v5, :cond_5b

    #@8e
    .line 245
    const/4 v8, 0x0

    #@8f
    .line 247
    .restart local v8       #record:[B
    :try_start_8f
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mIapFileRecord:Ljava/util/ArrayList;

    #@91
    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@94
    move-result-object v8

    #@95
    .end local v8           #record:[B
    check-cast v8, [B
    :try_end_97
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_8f .. :try_end_97} :catch_bc

    #@97
    .line 252
    .restart local v8       #record:[B
    iget v9, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mEmailTagNumberInIap:I

    #@99
    aget-byte v7, v8, v9

    #@9b
    .line 254
    .restart local v7       #recNum:I
    if-eq v7, v13, :cond_b9

    #@9d
    .line 255
    new-array v2, v12, [Ljava/lang/String;

    #@9f
    .line 257
    .restart local v2       #emails:[Ljava/lang/String;
    add-int/lit8 v9, v7, -0x1

    #@a1
    invoke-direct {p0, v9}, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->readEmailRecord(I)Ljava/lang/String;

    #@a4
    move-result-object v9

    #@a5
    aput-object v9, v2, v11

    #@a7
    .line 258
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mPhoneBookRecords:Ljava/util/ArrayList;

    #@a9
    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@ac
    move-result-object v6

    #@ad
    check-cast v6, Lcom/android/internal/telephony/uicc/AdnRecord;

    #@af
    .line 259
    .restart local v6       #rec:Lcom/android/internal/telephony/uicc/AdnRecord;
    if-eqz v6, :cond_c5

    #@b1
    .line 260
    invoke-virtual {v6, v2}, Lcom/android/internal/telephony/uicc/AdnRecord;->setEmails([Ljava/lang/String;)V

    #@b4
    .line 265
    :goto_b4
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mPhoneBookRecords:Ljava/util/ArrayList;

    #@b6
    invoke-virtual {v9, v3, v6}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@b9
    .line 244
    .end local v2           #emails:[Ljava/lang/String;
    .end local v6           #rec:Lcom/android/internal/telephony/uicc/AdnRecord;
    :cond_b9
    add-int/lit8 v3, v3, 0x1

    #@bb
    goto :goto_8c

    #@bc
    .line 248
    .end local v7           #recNum:I
    .end local v8           #record:[B
    :catch_bc
    move-exception v0

    #@bd
    .line 249
    .restart local v0       #e:Ljava/lang/IndexOutOfBoundsException;
    const-string v9, "GSM"

    #@bf
    const-string v10, "Error: Improper ICC card: No IAP record for ADN, continuing"

    #@c1
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c4
    goto :goto_5b

    #@c5
    .line 263
    .end local v0           #e:Ljava/lang/IndexOutOfBoundsException;
    .restart local v2       #emails:[Ljava/lang/String;
    .restart local v6       #rec:Lcom/android/internal/telephony/uicc/AdnRecord;
    .restart local v7       #recNum:I
    .restart local v8       #record:[B
    :cond_c5
    new-instance v6, Lcom/android/internal/telephony/uicc/AdnRecord;

    #@c7
    .end local v6           #rec:Lcom/android/internal/telephony/uicc/AdnRecord;
    const-string v9, ""

    #@c9
    const-string v10, ""

    #@cb
    invoke-direct {v6, v9, v10, v2}, Lcom/android/internal/telephony/uicc/AdnRecord;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    #@ce
    .restart local v6       #rec:Lcom/android/internal/telephony/uicc/AdnRecord;
    goto :goto_b4

    #@cf
    .line 284
    .end local v2           #emails:[Ljava/lang/String;
    .end local v6           #rec:Lcom/android/internal/telephony/uicc/AdnRecord;
    .end local v7           #recNum:I
    .end local v8           #record:[B
    .restart local v4       #len:I
    :catch_cf
    move-exception v0

    #@d0
    .line 285
    .restart local v0       #e:Ljava/lang/IndexOutOfBoundsException;
    goto/16 :goto_7

    #@d2
    .line 289
    .end local v0           #e:Ljava/lang/IndexOutOfBoundsException;
    .restart local v1       #emailList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_d2
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mPhoneBookRecords:Ljava/util/ArrayList;

    #@d4
    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@d7
    move-result-object v6

    #@d8
    check-cast v6, Lcom/android/internal/telephony/uicc/AdnRecord;

    #@da
    .line 291
    .restart local v6       #rec:Lcom/android/internal/telephony/uicc/AdnRecord;
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@dd
    move-result v9

    #@de
    new-array v2, v9, [Ljava/lang/String;

    #@e0
    .line 292
    .restart local v2       #emails:[Ljava/lang/String;
    invoke-virtual {v1}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    #@e3
    move-result-object v9

    #@e4
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@e7
    move-result v10

    #@e8
    invoke-static {v9, v11, v2, v11, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@eb
    .line 293
    invoke-virtual {v6, v2}, Lcom/android/internal/telephony/uicc/AdnRecord;->setEmails([Ljava/lang/String;)V

    #@ee
    .line 294
    iget-object v9, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mPhoneBookRecords:Ljava/util/ArrayList;

    #@f0
    invoke-virtual {v9, v3, v6}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@f3
    goto :goto_7a
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 409
    iget v1, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v1, :pswitch_data_86

    #@5
    .line 451
    :goto_5
    return-void

    #@6
    .line 411
    :pswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8
    check-cast v0, Landroid/os/AsyncResult;

    #@a
    .line 412
    .local v0, ar:Landroid/os/AsyncResult;
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@c
    if-nez v1, :cond_15

    #@e
    .line 413
    iget-object v1, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@10
    check-cast v1, Ljava/util/ArrayList;

    #@12
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->createPbrFile(Ljava/util/ArrayList;)V

    #@15
    .line 415
    :cond_15
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mLock:Ljava/lang/Object;

    #@17
    monitor-enter v2

    #@18
    .line 416
    :try_start_18
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mLock:Ljava/lang/Object;

    #@1a
    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    #@1d
    .line 417
    monitor-exit v2

    #@1e
    goto :goto_5

    #@1f
    :catchall_1f
    move-exception v1

    #@20
    monitor-exit v2
    :try_end_21
    .catchall {:try_start_18 .. :try_end_21} :catchall_1f

    #@21
    throw v1

    #@22
    .line 420
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_22
    const-string v1, "Loading USIM ADN records done"

    #@24
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->log(Ljava/lang/String;)V

    #@27
    .line 421
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@29
    check-cast v0, Landroid/os/AsyncResult;

    #@2b
    .line 422
    .restart local v0       #ar:Landroid/os/AsyncResult;
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@2d
    if-nez v1, :cond_38

    #@2f
    .line 423
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mPhoneBookRecords:Ljava/util/ArrayList;

    #@31
    iget-object v1, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@33
    check-cast v1, Ljava/util/ArrayList;

    #@35
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    #@38
    .line 425
    :cond_38
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mLock:Ljava/lang/Object;

    #@3a
    monitor-enter v2

    #@3b
    .line 426
    :try_start_3b
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mLock:Ljava/lang/Object;

    #@3d
    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    #@40
    .line 427
    monitor-exit v2

    #@41
    goto :goto_5

    #@42
    :catchall_42
    move-exception v1

    #@43
    monitor-exit v2
    :try_end_44
    .catchall {:try_start_3b .. :try_end_44} :catchall_42

    #@44
    throw v1

    #@45
    .line 430
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_45
    const-string v1, "Loading USIM IAP records done"

    #@47
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->log(Ljava/lang/String;)V

    #@4a
    .line 431
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4c
    check-cast v0, Landroid/os/AsyncResult;

    #@4e
    .line 432
    .restart local v0       #ar:Landroid/os/AsyncResult;
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@50
    if-nez v1, :cond_58

    #@52
    .line 433
    iget-object v1, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@54
    check-cast v1, Ljava/util/ArrayList;

    #@56
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mIapFileRecord:Ljava/util/ArrayList;

    #@58
    .line 435
    :cond_58
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mLock:Ljava/lang/Object;

    #@5a
    monitor-enter v2

    #@5b
    .line 436
    :try_start_5b
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mLock:Ljava/lang/Object;

    #@5d
    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    #@60
    .line 437
    monitor-exit v2

    #@61
    goto :goto_5

    #@62
    :catchall_62
    move-exception v1

    #@63
    monitor-exit v2
    :try_end_64
    .catchall {:try_start_5b .. :try_end_64} :catchall_62

    #@64
    throw v1

    #@65
    .line 440
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_65
    const-string v1, "Loading USIM Email records done"

    #@67
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->log(Ljava/lang/String;)V

    #@6a
    .line 441
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@6c
    check-cast v0, Landroid/os/AsyncResult;

    #@6e
    .line 442
    .restart local v0       #ar:Landroid/os/AsyncResult;
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@70
    if-nez v1, :cond_78

    #@72
    .line 443
    iget-object v1, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@74
    check-cast v1, Ljava/util/ArrayList;

    #@76
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mEmailFileRecord:Ljava/util/ArrayList;

    #@78
    .line 446
    :cond_78
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mLock:Ljava/lang/Object;

    #@7a
    monitor-enter v2

    #@7b
    .line 447
    :try_start_7b
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mLock:Ljava/lang/Object;

    #@7d
    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    #@80
    .line 448
    monitor-exit v2

    #@81
    goto :goto_5

    #@82
    :catchall_82
    move-exception v1

    #@83
    monitor-exit v2
    :try_end_84
    .catchall {:try_start_7b .. :try_end_84} :catchall_82

    #@84
    throw v1

    #@85
    .line 409
    nop

    #@86
    :pswitch_data_86
    .packed-switch 0x1
        :pswitch_6
        :pswitch_22
        :pswitch_45
        :pswitch_65
    .end packed-switch
.end method

.method public invalidateCache()V
    .registers 2

    #@0
    .prologue
    .line 140
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mRefreshCache:Z

    #@3
    .line 141
    return-void
.end method

.method public loadEfFilesFromUsim()Ljava/util/ArrayList;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/uicc/AdnRecord;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 100
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mLock:Ljava/lang/Object;

    #@3
    monitor-enter v3

    #@4
    .line 101
    :try_start_4
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mPhoneBookRecords:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    #@9
    move-result v4

    #@a
    if-nez v4, :cond_1a

    #@c
    .line 102
    iget-boolean v2, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mRefreshCache:Z

    #@e
    if-eqz v2, :cond_16

    #@10
    .line 103
    const/4 v2, 0x0

    #@11
    iput-boolean v2, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mRefreshCache:Z

    #@13
    .line 104
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->refreshCache()V

    #@16
    .line 106
    :cond_16
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mPhoneBookRecords:Ljava/util/ArrayList;

    #@18
    monitor-exit v3

    #@19
    .line 126
    :goto_19
    return-object v2

    #@1a
    .line 109
    :cond_1a
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mIsPbrPresent:Ljava/lang/Boolean;

    #@1c
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    #@1f
    move-result v4

    #@20
    if-nez v4, :cond_27

    #@22
    monitor-exit v3

    #@23
    goto :goto_19

    #@24
    .line 125
    :catchall_24
    move-exception v2

    #@25
    monitor-exit v3
    :try_end_26
    .catchall {:try_start_4 .. :try_end_26} :catchall_24

    #@26
    throw v2

    #@27
    .line 113
    :cond_27
    :try_start_27
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mPbrFile:Lcom/android/internal/telephony/gsm/UsimPhoneBookManager$PbrFile;

    #@29
    if-nez v4, :cond_2e

    #@2b
    .line 114
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->readPbrFileAndWait()V

    #@2e
    .line 117
    :cond_2e
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mPbrFile:Lcom/android/internal/telephony/gsm/UsimPhoneBookManager$PbrFile;

    #@30
    if-nez v4, :cond_34

    #@32
    monitor-exit v3

    #@33
    goto :goto_19

    #@34
    .line 119
    :cond_34
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mPbrFile:Lcom/android/internal/telephony/gsm/UsimPhoneBookManager$PbrFile;

    #@36
    iget-object v2, v2, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager$PbrFile;->mFileIds:Ljava/util/HashMap;

    #@38
    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    #@3b
    move-result v1

    #@3c
    .line 120
    .local v1, numRecs:I
    const/4 v0, 0x0

    #@3d
    .local v0, i:I
    :goto_3d
    if-ge v0, v1, :cond_48

    #@3f
    .line 121
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->readAdnFileAndWait(I)V

    #@42
    .line 122
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->readEmailFileAndWait(I)V

    #@45
    .line 120
    add-int/lit8 v0, v0, 0x1

    #@47
    goto :goto_3d

    #@48
    .line 125
    :cond_48
    monitor-exit v3
    :try_end_49
    .catchall {:try_start_27 .. :try_end_49} :catchall_24

    #@49
    .line 126
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mPhoneBookRecords:Ljava/util/ArrayList;

    #@4b
    goto :goto_19
.end method

.method parseType1EmailFile(I)V
    .registers 10
    .parameter "numRecs"

    #@0
    .prologue
    .line 299
    new-instance v6, Ljava/util/HashMap;

    #@2
    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    #@5
    iput-object v6, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mEmailsForAdnRec:Ljava/util/Map;

    #@7
    .line 300
    const/4 v3, 0x0

    #@8
    .line 301
    .local v3, emailRec:[B
    const/4 v4, 0x0

    #@9
    .local v4, i:I
    :goto_9
    if-ge v4, p1, :cond_36

    #@b
    .line 303
    :try_start_b
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mEmailFileRecord:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v3

    #@11
    .end local v3           #emailRec:[B
    check-cast v3, [B
    :try_end_13
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_b .. :try_end_13} :catch_2e

    #@13
    .line 310
    .restart local v3       #emailRec:[B
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@16
    move-result-object v6

    #@17
    const-string v7, "DCM"

    #@19
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v6

    #@1d
    if-eqz v6, :cond_3a

    #@1f
    .line 312
    iget-boolean v6, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mEmailPresentInIap:Z

    #@21
    if-eqz v6, :cond_37

    #@23
    .line 313
    array-length v6, v3

    #@24
    add-int/lit8 v6, v6, -0x1

    #@26
    aget-byte v0, v3, v6

    #@28
    .line 321
    .local v0, adnRecNum:I
    :goto_28
    const/4 v6, -0x1

    #@29
    if-ne v0, v6, :cond_40

    #@2b
    .line 301
    :cond_2b
    :goto_2b
    add-int/lit8 v4, v4, 0x1

    #@2d
    goto :goto_9

    #@2e
    .line 304
    .end local v0           #adnRecNum:I
    .end local v3           #emailRec:[B
    :catch_2e
    move-exception v1

    #@2f
    .line 305
    .local v1, e:Ljava/lang/IndexOutOfBoundsException;
    const-string v6, "GSM"

    #@31
    const-string v7, "Error: Improper ICC card: No email record for ADN, continuing"

    #@33
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 340
    .end local v1           #e:Ljava/lang/IndexOutOfBoundsException;
    :cond_36
    return-void

    #@37
    .line 315
    .restart local v3       #emailRec:[B
    :cond_37
    add-int/lit8 v0, v4, 0x1

    #@39
    .restart local v0       #adnRecNum:I
    goto :goto_28

    #@3a
    .line 317
    .end local v0           #adnRecNum:I
    :cond_3a
    array-length v6, v3

    #@3b
    add-int/lit8 v6, v6, -0x1

    #@3d
    aget-byte v0, v3, v6

    #@3f
    .restart local v0       #adnRecNum:I
    goto :goto_28

    #@40
    .line 325
    :cond_40
    invoke-direct {p0, v4}, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->readEmailRecord(I)Ljava/lang/String;

    #@43
    move-result-object v2

    #@44
    .line 327
    .local v2, email:Ljava/lang/String;
    if-eqz v2, :cond_2b

    #@46
    const-string v6, ""

    #@48
    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4b
    move-result v6

    #@4c
    if-nez v6, :cond_2b

    #@4e
    .line 332
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mEmailsForAdnRec:Ljava/util/Map;

    #@50
    add-int/lit8 v7, v0, -0x1

    #@52
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@55
    move-result-object v7

    #@56
    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@59
    move-result-object v5

    #@5a
    check-cast v5, Ljava/util/ArrayList;

    #@5c
    .line 333
    .local v5, val:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v5, :cond_63

    #@5e
    .line 334
    new-instance v5, Ljava/util/ArrayList;

    #@60
    .end local v5           #val:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    #@63
    .line 336
    .restart local v5       #val:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_63
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@66
    .line 338
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mEmailsForAdnRec:Ljava/util/Map;

    #@68
    add-int/lit8 v7, v0, -0x1

    #@6a
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6d
    move-result-object v7

    #@6e
    invoke-interface {v6, v7, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@71
    goto :goto_2b
.end method

.method public reset()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 91
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mPhoneBookRecords:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@6
    .line 92
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mIapFileRecord:Ljava/util/ArrayList;

    #@8
    .line 93
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mEmailFileRecord:Ljava/util/ArrayList;

    #@a
    .line 94
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mPbrFile:Lcom/android/internal/telephony/gsm/UsimPhoneBookManager$PbrFile;

    #@c
    .line 95
    const/4 v0, 0x1

    #@d
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@10
    move-result-object v0

    #@11
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mIsPbrPresent:Ljava/lang/Boolean;

    #@13
    .line 96
    const/4 v0, 0x0

    #@14
    iput-boolean v0, p0, Lcom/android/internal/telephony/gsm/UsimPhoneBookManager;->mRefreshCache:Z

    #@16
    .line 97
    return-void
.end method
