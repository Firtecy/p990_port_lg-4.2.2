.class final Lcom/android/internal/telephony/DataConnectionTracker$4;
.super Ljava/lang/Object;
.source "DataConnectionTracker.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/DataConnectionTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/android/internal/telephony/ApnContext;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 2303
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public compare(Lcom/android/internal/telephony/ApnContext;Lcom/android/internal/telephony/ApnContext;)I
    .registers 4
    .parameter "lhs"
    .parameter "rhs"

    #@0
    .prologue
    .line 2308
    invoke-virtual {p1, p2}, Lcom/android/internal/telephony/ApnContext;->isEqualPriority(Lcom/android/internal/telephony/ApnContext;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x0

    #@7
    .line 2309
    :goto_7
    return v0

    #@8
    :cond_8
    invoke-virtual {p1, p2}, Lcom/android/internal/telephony/ApnContext;->isLowerPriority(Lcom/android/internal/telephony/ApnContext;)Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_10

    #@e
    const/4 v0, -0x1

    #@f
    goto :goto_7

    #@10
    :cond_10
    const/4 v0, 0x1

    #@11
    goto :goto_7
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2303
    check-cast p1, Lcom/android/internal/telephony/ApnContext;

    #@2
    .end local p1
    check-cast p2, Lcom/android/internal/telephony/ApnContext;

    #@4
    .end local p2
    invoke-virtual {p0, p1, p2}, Lcom/android/internal/telephony/DataConnectionTracker$4;->compare(Lcom/android/internal/telephony/ApnContext;Lcom/android/internal/telephony/ApnContext;)I

    #@7
    move-result v0

    #@8
    return v0
.end method
