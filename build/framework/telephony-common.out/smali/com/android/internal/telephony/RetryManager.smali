.class public Lcom/android/internal/telephony/RetryManager;
.super Ljava/lang/Object;
.source "RetryManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/RetryManager$RetryRec;
    }
.end annotation


# static fields
.field public static final DBG:Z = true

.field public static final LOG_TAG:Ljava/lang/String; = "GSM"

.field public static final VDBG:Z


# instance fields
.field private mConfig:Ljava/lang/String;

.field protected mLGfeature:Lcom/android/internal/telephony/LGfeature;

.field private mMaxRetryCount:I

.field private mRetryArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/RetryManager$RetryRec;",
            ">;"
        }
    .end annotation
.end field

.field private mRetryCount:I

.field private mRetryForever:Z

.field private rng:Ljava/util/Random;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 122
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 102
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/internal/telephony/RetryManager;->mRetryArray:Ljava/util/ArrayList;

    #@a
    .line 117
    new-instance v0, Ljava/util/Random;

    #@c
    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    #@f
    iput-object v0, p0, Lcom/android/internal/telephony/RetryManager;->rng:Ljava/util/Random;

    #@11
    .line 124
    return-void
.end method

.method private log(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 419
    const-string v0, "GSM"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[RM] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 420
    return-void
.end method

.method private nextRandomizationTime(I)I
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 410
    iget-object v1, p0, Lcom/android/internal/telephony/RetryManager;->mRetryArray:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Lcom/android/internal/telephony/RetryManager$RetryRec;

    #@8
    iget v0, v1, Lcom/android/internal/telephony/RetryManager$RetryRec;->mRandomizationTime:I

    #@a
    .line 411
    .local v0, randomTime:I
    if-nez v0, :cond_e

    #@c
    .line 412
    const/4 v1, 0x0

    #@d
    .line 414
    :goto_d
    return v1

    #@e
    :cond_e
    iget-object v1, p0, Lcom/android/internal/telephony/RetryManager;->rng:Ljava/util/Random;

    #@10
    invoke-virtual {v1, v0}, Ljava/util/Random;->nextInt(I)I

    #@13
    move-result v1

    #@14
    goto :goto_d
.end method

.method private parseNonNegativeInt(Ljava/lang/String;Ljava/lang/String;)Landroid/util/Pair;
    .registers 10
    .parameter "name"
    .parameter "stringValue"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 376
    :try_start_1
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@4
    move-result v2

    #@5
    .line 377
    .local v2, value:I
    new-instance v1, Landroid/util/Pair;

    #@7
    invoke-direct {p0, p1, v2}, Lcom/android/internal/telephony/RetryManager;->validateNonNegativeInt(Ljava/lang/String;I)Z

    #@a
    move-result v3

    #@b
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@e
    move-result-object v3

    #@f
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@12
    move-result-object v4

    #@13
    invoke-direct {v1, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_16
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_16} :catch_17

    #@16
    .line 384
    .end local v2           #value:I
    .local v1, retVal:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Boolean;Ljava/lang/Integer;>;"
    :goto_16
    return-object v1

    #@17
    .line 378
    .end local v1           #retVal:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Boolean;Ljava/lang/Integer;>;"
    :catch_17
    move-exception v0

    #@18
    .line 379
    .local v0, e:Ljava/lang/NumberFormatException;
    const-string v3, "GSM"

    #@1a
    new-instance v4, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    const-string v5, " bad value: "

    #@25
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v4

    #@31
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@34
    .line 380
    new-instance v1, Landroid/util/Pair;

    #@36
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@39
    move-result-object v3

    #@3a
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3d
    move-result-object v4

    #@3e
    invoke-direct {v1, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    #@41
    .restart local v1       #retVal:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Boolean;Ljava/lang/Integer;>;"
    goto :goto_16
.end method

.method private validateNonNegativeInt(Ljava/lang/String;I)Z
    .registers 7
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 396
    if-gez p2, :cond_1c

    #@2
    .line 397
    const-string v1, "GSM"

    #@4
    new-instance v2, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    const-string v3, " bad value: is < 0"

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 398
    const/4 v0, 0x0

    #@1b
    .line 403
    .local v0, retVal:Z
    :goto_1b
    return v0

    #@1c
    .line 400
    .end local v0           #retVal:Z
    :cond_1c
    const/4 v0, 0x1

    #@1d
    .restart local v0       #retVal:Z
    goto :goto_1b
.end method


# virtual methods
.method public configure(III)Z
    .registers 6
    .parameter "maxRetryCount"
    .parameter "retryTime"
    .parameter "randomizationTime"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 152
    const-string v1, "maxRetryCount"

    #@3
    invoke-direct {p0, v1, p1}, Lcom/android/internal/telephony/RetryManager;->validateNonNegativeInt(Ljava/lang/String;I)Z

    #@6
    move-result v1

    #@7
    if-nez v1, :cond_a

    #@9
    .line 169
    :cond_9
    :goto_9
    return v0

    #@a
    .line 156
    :cond_a
    const-string v1, "retryTime"

    #@c
    invoke-direct {p0, v1, p2}, Lcom/android/internal/telephony/RetryManager;->validateNonNegativeInt(Ljava/lang/String;I)Z

    #@f
    move-result v1

    #@10
    if-eqz v1, :cond_9

    #@12
    .line 160
    const-string v1, "randomizationTime"

    #@14
    invoke-direct {p0, v1, p3}, Lcom/android/internal/telephony/RetryManager;->validateNonNegativeInt(Ljava/lang/String;I)Z

    #@17
    move-result v1

    #@18
    if-eqz v1, :cond_9

    #@1a
    .line 164
    iput p1, p0, Lcom/android/internal/telephony/RetryManager;->mMaxRetryCount:I

    #@1c
    .line 165
    invoke-virtual {p0}, Lcom/android/internal/telephony/RetryManager;->resetRetryCount()V

    #@1f
    .line 166
    iget-object v0, p0, Lcom/android/internal/telephony/RetryManager;->mRetryArray:Ljava/util/ArrayList;

    #@21
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@24
    .line 167
    iget-object v0, p0, Lcom/android/internal/telephony/RetryManager;->mRetryArray:Ljava/util/ArrayList;

    #@26
    new-instance v1, Lcom/android/internal/telephony/RetryManager$RetryRec;

    #@28
    invoke-direct {v1, p2, p3}, Lcom/android/internal/telephony/RetryManager$RetryRec;-><init>(II)V

    #@2b
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2e
    .line 169
    const/4 v0, 0x1

    #@2f
    goto :goto_9
.end method

.method public configure(Ljava/lang/String;)Z
    .registers 13
    .parameter "configStr"

    #@0
    .prologue
    const/4 v10, 0x2

    #@1
    const/4 v8, 0x1

    #@2
    const/4 v7, 0x0

    #@3
    .line 181
    const-string v6, "\""

    #@5
    invoke-virtual {p1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@8
    move-result v6

    #@9
    if-eqz v6, :cond_1d

    #@b
    const-string v6, "\""

    #@d
    invoke-virtual {p1, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@10
    move-result v6

    #@11
    if-eqz v6, :cond_1d

    #@13
    .line 182
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@16
    move-result v6

    #@17
    add-int/lit8 v6, v6, -0x1

    #@19
    invoke-virtual {p1, v8, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1c
    move-result-object p1

    #@1d
    .line 185
    :cond_1d
    iput-object p1, p0, Lcom/android/internal/telephony/RetryManager;->mConfig:Ljava/lang/String;

    #@1f
    .line 187
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@22
    move-result v6

    #@23
    if-nez v6, :cond_14f

    #@25
    .line 188
    const/4 v0, 0x0

    #@26
    .line 192
    .local v0, defaultRandomization:I
    iput v7, p0, Lcom/android/internal/telephony/RetryManager;->mMaxRetryCount:I

    #@28
    .line 193
    invoke-virtual {p0}, Lcom/android/internal/telephony/RetryManager;->resetRetryCount()V

    #@2b
    .line 194
    iget-object v6, p0, Lcom/android/internal/telephony/RetryManager;->mRetryArray:Ljava/util/ArrayList;

    #@2d
    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    #@30
    .line 196
    const-string v6, ","

    #@32
    invoke-virtual {p1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@35
    move-result-object v4

    #@36
    .line 197
    .local v4, strArray:[Ljava/lang/String;
    const/4 v1, 0x0

    #@37
    .local v1, i:I
    :goto_37
    array-length v6, v4

    #@38
    if-ge v1, v6, :cond_13a

    #@3a
    .line 200
    aget-object v6, v4, v1

    #@3c
    const-string v9, "="

    #@3e
    invoke-virtual {v6, v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    #@41
    move-result-object v3

    #@42
    .line 201
    .local v3, splitStr:[Ljava/lang/String;
    aget-object v6, v3, v7

    #@44
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@47
    move-result-object v6

    #@48
    aput-object v6, v3, v7

    #@4a
    .line 203
    array-length v6, v3

    #@4b
    if-le v6, v8, :cond_d2

    #@4d
    .line 204
    aget-object v6, v3, v8

    #@4f
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@52
    move-result-object v6

    #@53
    aput-object v6, v3, v8

    #@55
    .line 206
    aget-object v6, v3, v7

    #@57
    const-string v9, "default_randomization"

    #@59
    invoke-static {v6, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@5c
    move-result v6

    #@5d
    if-eqz v6, :cond_7e

    #@5f
    .line 207
    aget-object v6, v3, v7

    #@61
    aget-object v9, v3, v8

    #@63
    invoke-direct {p0, v6, v9}, Lcom/android/internal/telephony/RetryManager;->parseNonNegativeInt(Ljava/lang/String;Ljava/lang/String;)Landroid/util/Pair;

    #@66
    move-result-object v5

    #@67
    .line 208
    .local v5, value:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Boolean;Ljava/lang/Integer;>;"
    iget-object v6, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@69
    check-cast v6, Ljava/lang/Boolean;

    #@6b
    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    #@6e
    move-result v6

    #@6f
    if-nez v6, :cond_73

    #@71
    move v6, v7

    #@72
    .line 259
    .end local v0           #defaultRandomization:I
    .end local v1           #i:I
    .end local v3           #splitStr:[Ljava/lang/String;
    .end local v4           #strArray:[Ljava/lang/String;
    .end local v5           #value:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Boolean;Ljava/lang/Integer;>;"
    :goto_72
    return v6

    #@73
    .line 209
    .restart local v0       #defaultRandomization:I
    .restart local v1       #i:I
    .restart local v3       #splitStr:[Ljava/lang/String;
    .restart local v4       #strArray:[Ljava/lang/String;
    .restart local v5       #value:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Boolean;Ljava/lang/Integer;>;"
    :cond_73
    iget-object v6, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@75
    check-cast v6, Ljava/lang/Integer;

    #@77
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    #@7a
    move-result v0

    #@7b
    .line 197
    .end local v5           #value:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Boolean;Ljava/lang/Integer;>;"
    :goto_7b
    add-int/lit8 v1, v1, 0x1

    #@7d
    goto :goto_37

    #@7e
    .line 210
    :cond_7e
    aget-object v6, v3, v7

    #@80
    const-string v9, "max_retries"

    #@82
    invoke-static {v6, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@85
    move-result v6

    #@86
    if-eqz v6, :cond_b6

    #@88
    .line 211
    const-string v6, "infinite"

    #@8a
    aget-object v9, v3, v8

    #@8c
    invoke-static {v6, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@8f
    move-result v6

    #@90
    if-eqz v6, :cond_95

    #@92
    .line 212
    iput-boolean v8, p0, Lcom/android/internal/telephony/RetryManager;->mRetryForever:Z

    #@94
    goto :goto_7b

    #@95
    .line 215
    :cond_95
    iput-boolean v7, p0, Lcom/android/internal/telephony/RetryManager;->mRetryForever:Z

    #@97
    .line 217
    aget-object v6, v3, v7

    #@99
    aget-object v9, v3, v8

    #@9b
    invoke-direct {p0, v6, v9}, Lcom/android/internal/telephony/RetryManager;->parseNonNegativeInt(Ljava/lang/String;Ljava/lang/String;)Landroid/util/Pair;

    #@9e
    move-result-object v5

    #@9f
    .line 218
    .restart local v5       #value:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Boolean;Ljava/lang/Integer;>;"
    iget-object v6, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@a1
    check-cast v6, Ljava/lang/Boolean;

    #@a3
    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    #@a6
    move-result v6

    #@a7
    if-nez v6, :cond_ab

    #@a9
    move v6, v7

    #@aa
    goto :goto_72

    #@ab
    .line 219
    :cond_ab
    iget-object v6, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@ad
    check-cast v6, Ljava/lang/Integer;

    #@af
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    #@b2
    move-result v6

    #@b3
    iput v6, p0, Lcom/android/internal/telephony/RetryManager;->mMaxRetryCount:I

    #@b5
    goto :goto_7b

    #@b6
    .line 222
    .end local v5           #value:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Boolean;Ljava/lang/Integer;>;"
    :cond_b6
    const-string v6, "GSM"

    #@b8
    new-instance v8, Ljava/lang/StringBuilder;

    #@ba
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@bd
    const-string v9, "Unrecognized configuration name value pair: "

    #@bf
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v8

    #@c3
    aget-object v9, v4, v1

    #@c5
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v8

    #@c9
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cc
    move-result-object v8

    #@cd
    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d0
    move v6, v7

    #@d1
    .line 224
    goto :goto_72

    #@d2
    .line 231
    :cond_d2
    aget-object v6, v4, v1

    #@d4
    const-string v9, ":"

    #@d6
    invoke-virtual {v6, v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    #@d9
    move-result-object v3

    #@da
    .line 232
    aget-object v6, v3, v7

    #@dc
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@df
    move-result-object v6

    #@e0
    aput-object v6, v3, v7

    #@e2
    .line 233
    new-instance v2, Lcom/android/internal/telephony/RetryManager$RetryRec;

    #@e4
    invoke-direct {v2, v7, v7}, Lcom/android/internal/telephony/RetryManager$RetryRec;-><init>(II)V

    #@e7
    .line 234
    .local v2, rr:Lcom/android/internal/telephony/RetryManager$RetryRec;
    const-string v6, "delayTime"

    #@e9
    aget-object v9, v3, v7

    #@eb
    invoke-direct {p0, v6, v9}, Lcom/android/internal/telephony/RetryManager;->parseNonNegativeInt(Ljava/lang/String;Ljava/lang/String;)Landroid/util/Pair;

    #@ee
    move-result-object v5

    #@ef
    .line 235
    .restart local v5       #value:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Boolean;Ljava/lang/Integer;>;"
    iget-object v6, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@f1
    check-cast v6, Ljava/lang/Boolean;

    #@f3
    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    #@f6
    move-result v6

    #@f7
    if-nez v6, :cond_fc

    #@f9
    move v6, v7

    #@fa
    goto/16 :goto_72

    #@fc
    .line 236
    :cond_fc
    iget-object v6, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@fe
    check-cast v6, Ljava/lang/Integer;

    #@100
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    #@103
    move-result v6

    #@104
    iput v6, v2, Lcom/android/internal/telephony/RetryManager$RetryRec;->mDelayTime:I

    #@106
    .line 239
    array-length v6, v3

    #@107
    if-le v6, v8, :cond_137

    #@109
    .line 240
    aget-object v6, v3, v8

    #@10b
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@10e
    move-result-object v6

    #@10f
    aput-object v6, v3, v8

    #@111
    .line 242
    const-string v6, "randomizationTime"

    #@113
    aget-object v9, v3, v8

    #@115
    invoke-direct {p0, v6, v9}, Lcom/android/internal/telephony/RetryManager;->parseNonNegativeInt(Ljava/lang/String;Ljava/lang/String;)Landroid/util/Pair;

    #@118
    move-result-object v5

    #@119
    .line 243
    iget-object v6, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@11b
    check-cast v6, Ljava/lang/Boolean;

    #@11d
    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    #@120
    move-result v6

    #@121
    if-nez v6, :cond_126

    #@123
    move v6, v7

    #@124
    goto/16 :goto_72

    #@126
    .line 244
    :cond_126
    iget-object v6, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@128
    check-cast v6, Ljava/lang/Integer;

    #@12a
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    #@12d
    move-result v6

    #@12e
    iput v6, v2, Lcom/android/internal/telephony/RetryManager$RetryRec;->mRandomizationTime:I

    #@130
    .line 248
    :goto_130
    iget-object v6, p0, Lcom/android/internal/telephony/RetryManager;->mRetryArray:Ljava/util/ArrayList;

    #@132
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@135
    goto/16 :goto_7b

    #@137
    .line 246
    :cond_137
    iput v0, v2, Lcom/android/internal/telephony/RetryManager$RetryRec;->mRandomizationTime:I

    #@139
    goto :goto_130

    #@13a
    .line 251
    .end local v2           #rr:Lcom/android/internal/telephony/RetryManager$RetryRec;
    .end local v3           #splitStr:[Ljava/lang/String;
    .end local v5           #value:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Boolean;Ljava/lang/Integer;>;"
    :cond_13a
    iget-object v6, p0, Lcom/android/internal/telephony/RetryManager;->mRetryArray:Ljava/util/ArrayList;

    #@13c
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@13f
    move-result v6

    #@140
    iget v7, p0, Lcom/android/internal/telephony/RetryManager;->mMaxRetryCount:I

    #@142
    if-le v6, v7, :cond_14c

    #@144
    .line 252
    iget-object v6, p0, Lcom/android/internal/telephony/RetryManager;->mRetryArray:Ljava/util/ArrayList;

    #@146
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@149
    move-result v6

    #@14a
    iput v6, p0, Lcom/android/internal/telephony/RetryManager;->mMaxRetryCount:I

    #@14c
    :cond_14c
    move v6, v8

    #@14d
    .line 256
    goto/16 :goto_72

    #@14f
    .end local v0           #defaultRandomization:I
    .end local v1           #i:I
    .end local v4           #strArray:[Ljava/lang/String;
    :cond_14f
    move v6, v7

    #@150
    .line 259
    goto/16 :goto_72
.end method

.method public getRetryCount()I
    .registers 3

    #@0
    .prologue
    .line 301
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "getRetryCount: "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Lcom/android/internal/telephony/RetryManager;->mRetryCount:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/RetryManager;->log(Ljava/lang/String;)V

    #@18
    .line 302
    iget v0, p0, Lcom/android/internal/telephony/RetryManager;->mRetryCount:I

    #@1a
    return v0
.end method

.method public getRetryTimer()I
    .registers 5

    #@0
    .prologue
    .line 280
    iget v2, p0, Lcom/android/internal/telephony/RetryManager;->mRetryCount:I

    #@2
    iget-object v3, p0, Lcom/android/internal/telephony/RetryManager;->mRetryArray:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v3

    #@8
    if-ge v2, v3, :cond_3d

    #@a
    .line 281
    iget v0, p0, Lcom/android/internal/telephony/RetryManager;->mRetryCount:I

    #@c
    .line 287
    .local v0, index:I
    :goto_c
    if-ltz v0, :cond_46

    #@e
    iget-object v2, p0, Lcom/android/internal/telephony/RetryManager;->mRetryArray:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@13
    move-result v2

    #@14
    if-ge v0, v2, :cond_46

    #@16
    .line 288
    iget-object v2, p0, Lcom/android/internal/telephony/RetryManager;->mRetryArray:Ljava/util/ArrayList;

    #@18
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1b
    move-result-object v2

    #@1c
    check-cast v2, Lcom/android/internal/telephony/RetryManager$RetryRec;

    #@1e
    iget v2, v2, Lcom/android/internal/telephony/RetryManager$RetryRec;->mDelayTime:I

    #@20
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/RetryManager;->nextRandomizationTime(I)I

    #@23
    move-result v3

    #@24
    add-int v1, v2, v3

    #@26
    .line 293
    .local v1, retVal:I
    :goto_26
    new-instance v2, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v3, "getRetryTimer: "

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v2

    #@39
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/RetryManager;->log(Ljava/lang/String;)V

    #@3c
    .line 294
    return v1

    #@3d
    .line 283
    .end local v0           #index:I
    .end local v1           #retVal:I
    :cond_3d
    iget-object v2, p0, Lcom/android/internal/telephony/RetryManager;->mRetryArray:Ljava/util/ArrayList;

    #@3f
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@42
    move-result v2

    #@43
    add-int/lit8 v0, v2, -0x1

    #@45
    .restart local v0       #index:I
    goto :goto_c

    #@46
    .line 290
    :cond_46
    const/4 v1, 0x0

    #@47
    .restart local v1       #retVal:I
    goto :goto_26
.end method

.method public increaseRetryCount()V
    .registers 3

    #@0
    .prologue
    .line 309
    iget v0, p0, Lcom/android/internal/telephony/RetryManager;->mRetryCount:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Lcom/android/internal/telephony/RetryManager;->mRetryCount:I

    #@6
    .line 310
    iget v0, p0, Lcom/android/internal/telephony/RetryManager;->mRetryCount:I

    #@8
    iget v1, p0, Lcom/android/internal/telephony/RetryManager;->mMaxRetryCount:I

    #@a
    if-le v0, v1, :cond_10

    #@c
    .line 311
    iget v0, p0, Lcom/android/internal/telephony/RetryManager;->mMaxRetryCount:I

    #@e
    iput v0, p0, Lcom/android/internal/telephony/RetryManager;->mRetryCount:I

    #@10
    .line 313
    :cond_10
    new-instance v0, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v1, "increaseRetryCount: "

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    iget v1, p0, Lcom/android/internal/telephony/RetryManager;->mRetryCount:I

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/RetryManager;->log(Ljava/lang/String;)V

    #@28
    .line 314
    return-void
.end method

.method public isRetryForever()Z
    .registers 3

    #@0
    .prologue
    .line 361
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "isRetryForever: "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-boolean v1, p0, Lcom/android/internal/telephony/RetryManager;->mRetryForever:Z

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/RetryManager;->log(Ljava/lang/String;)V

    #@18
    .line 362
    iget-boolean v0, p0, Lcom/android/internal/telephony/RetryManager;->mRetryForever:Z

    #@1a
    return v0
.end method

.method public isRetryNeeded()Z
    .registers 4

    #@0
    .prologue
    .line 270
    iget-boolean v1, p0, Lcom/android/internal/telephony/RetryManager;->mRetryForever:Z

    #@2
    if-nez v1, :cond_a

    #@4
    iget v1, p0, Lcom/android/internal/telephony/RetryManager;->mRetryCount:I

    #@6
    iget v2, p0, Lcom/android/internal/telephony/RetryManager;->mMaxRetryCount:I

    #@8
    if-ge v1, v2, :cond_22

    #@a
    :cond_a
    const/4 v0, 0x1

    #@b
    .line 271
    .local v0, retVal:Z
    :goto_b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "isRetryNeeded: "

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/RetryManager;->log(Ljava/lang/String;)V

    #@21
    .line 272
    return v0

    #@22
    .line 270
    .end local v0           #retVal:Z
    :cond_22
    const/4 v0, 0x0

    #@23
    goto :goto_b
.end method

.method public resetRetryCount()V
    .registers 3

    #@0
    .prologue
    .line 344
    const/4 v0, 0x0

    #@1
    iput v0, p0, Lcom/android/internal/telephony/RetryManager;->mRetryCount:I

    #@3
    .line 345
    new-instance v0, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v1, "resetRetryCount: "

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v0

    #@e
    iget v1, p0, Lcom/android/internal/telephony/RetryManager;->mRetryCount:I

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v0

    #@14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/RetryManager;->log(Ljava/lang/String;)V

    #@1b
    .line 346
    return-void
.end method

.method public retryForeverUsingLastTimeout()V
    .registers 3

    #@0
    .prologue
    .line 352
    iget v0, p0, Lcom/android/internal/telephony/RetryManager;->mMaxRetryCount:I

    #@2
    iput v0, p0, Lcom/android/internal/telephony/RetryManager;->mRetryCount:I

    #@4
    .line 353
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Lcom/android/internal/telephony/RetryManager;->mRetryForever:Z

    #@7
    .line 354
    new-instance v0, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v1, "retryForeverUsingLastTimeout: "

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v0

    #@12
    iget-boolean v1, p0, Lcom/android/internal/telephony/RetryManager;->mRetryForever:Z

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    const-string v1, ", "

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    iget v1, p0, Lcom/android/internal/telephony/RetryManager;->mRetryCount:I

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v0

    #@24
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v0

    #@28
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/RetryManager;->log(Ljava/lang/String;)V

    #@2b
    .line 355
    return-void
.end method

.method public setRetryCount(I)V
    .registers 4
    .parameter "count"

    #@0
    .prologue
    .line 320
    iput p1, p0, Lcom/android/internal/telephony/RetryManager;->mRetryCount:I

    #@2
    .line 321
    iget v0, p0, Lcom/android/internal/telephony/RetryManager;->mRetryCount:I

    #@4
    iget v1, p0, Lcom/android/internal/telephony/RetryManager;->mMaxRetryCount:I

    #@6
    if-le v0, v1, :cond_c

    #@8
    .line 322
    iget v0, p0, Lcom/android/internal/telephony/RetryManager;->mMaxRetryCount:I

    #@a
    iput v0, p0, Lcom/android/internal/telephony/RetryManager;->mRetryCount:I

    #@c
    .line 325
    :cond_c
    iget v0, p0, Lcom/android/internal/telephony/RetryManager;->mRetryCount:I

    #@e
    if-gez v0, :cond_13

    #@10
    .line 326
    const/4 v0, 0x0

    #@11
    iput v0, p0, Lcom/android/internal/telephony/RetryManager;->mRetryCount:I

    #@13
    .line 329
    :cond_13
    new-instance v0, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v1, "setRetryCount: "

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    iget v1, p0, Lcom/android/internal/telephony/RetryManager;->mRetryCount:I

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v0

    #@24
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v0

    #@28
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/RetryManager;->log(Ljava/lang/String;)V

    #@2b
    .line 330
    return-void
.end method

.method public setRetryForever(Z)V
    .registers 4
    .parameter "retryForever"

    #@0
    .prologue
    .line 336
    iput-boolean p1, p0, Lcom/android/internal/telephony/RetryManager;->mRetryForever:Z

    #@2
    .line 337
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v1, "setRetryForever: "

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    iget-boolean v1, p0, Lcom/android/internal/telephony/RetryManager;->mRetryForever:Z

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/RetryManager;->log(Ljava/lang/String;)V

    #@1a
    .line 338
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 127
    new-instance v3, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v4, "RetryManager: forever="

    #@7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v3

    #@b
    iget-boolean v4, p0, Lcom/android/internal/telephony/RetryManager;->mRetryForever:Z

    #@d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    const-string v4, ", maxRetry="

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    iget v4, p0, Lcom/android/internal/telephony/RetryManager;->mMaxRetryCount:I

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    const-string v4, ", retry="

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    iget v4, p0, Lcom/android/internal/telephony/RetryManager;->mRetryCount:I

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    const-string v4, ",\n    "

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    iget-object v4, p0, Lcom/android/internal/telephony/RetryManager;->mConfig:Ljava/lang/String;

    #@31
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v2

    #@39
    .line 129
    .local v2, ret:Ljava/lang/String;
    iget-object v3, p0, Lcom/android/internal/telephony/RetryManager;->mRetryArray:Ljava/util/ArrayList;

    #@3b
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@3e
    move-result-object v0

    #@3f
    .local v0, i$:Ljava/util/Iterator;
    :goto_3f
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@42
    move-result v3

    #@43
    if-eqz v3, :cond_71

    #@45
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@48
    move-result-object v1

    #@49
    check-cast v1, Lcom/android/internal/telephony/RetryManager$RetryRec;

    #@4b
    .line 130
    .local v1, r:Lcom/android/internal/telephony/RetryManager$RetryRec;
    new-instance v3, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v3

    #@54
    const-string v4, "\n    "

    #@56
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v3

    #@5a
    iget v4, v1, Lcom/android/internal/telephony/RetryManager$RetryRec;->mDelayTime:I

    #@5c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v3

    #@60
    const-string v4, ":"

    #@62
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v3

    #@66
    iget v4, v1, Lcom/android/internal/telephony/RetryManager$RetryRec;->mRandomizationTime:I

    #@68
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v3

    #@6c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v2

    #@70
    goto :goto_3f

    #@71
    .line 132
    .end local v1           #r:Lcom/android/internal/telephony/RetryManager$RetryRec;
    :cond_71
    return-object v2
.end method
