.class Lcom/android/internal/telephony/LgeTimeZoneMonitor$2;
.super Landroid/database/ContentObserver;
.source "LgeTimeZoneMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/LgeTimeZoneMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/LgeTimeZoneMonitor;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/LgeTimeZoneMonitor;Landroid/os/Handler;)V
    .registers 3
    .parameter
    .parameter "x0"

    #@0
    .prologue
    .line 91
    iput-object p1, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor$2;->this$0:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@2
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    #@5
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .registers 6
    .parameter "selfChange"

    #@0
    .prologue
    .line 94
    iget-object v0, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor$2;->this$0:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@2
    const-string v1, "Auto time zone state changed"

    #@4
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->log(Ljava/lang/String;)V

    #@7
    .line 96
    iget-object v0, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor$2;->this$0:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@9
    const/4 v1, 0x6

    #@a
    const-wide/16 v2, 0x3e8

    #@c
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->sendEmptyMessageDelayed(IJ)Z

    #@f
    .line 97
    return-void
.end method
