.class public interface abstract Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoSlice$Column;
.super Ljava/lang/Object;
.source "IccPhonebookProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoSlice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Column"
.end annotation


# static fields
.field public static final ADN_CAPACITY:Ljava/lang/String;

.field public static final ANR_CAPACITY:Ljava/lang/String;

.field public static final ANR_TYPE:Ljava/lang/String;

.field public static final EMAIL_CAPACITY:Ljava/lang/String;

.field public static final EMAIL_TYPE:Ljava/lang/String;

.field public static final RESERVED1:Ljava/lang/String;

.field public static final RESERVED10:Ljava/lang/String;

.field public static final RESERVED11:Ljava/lang/String;

.field public static final RESERVED2:Ljava/lang/String;

.field public static final RESERVED3:Ljava/lang/String;

.field public static final RESERVED4:Ljava/lang/String;

.field public static final RESERVED5:Ljava/lang/String;

.field public static final RESERVED6:Ljava/lang/String;

.field public static final RESERVED7:Ljava/lang/String;

.field public static final RESERVED8:Ljava/lang/String;

.field public static final RESERVED9:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 126
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@2
    const/4 v1, 0x0

    #@3
    aget-object v0, v0, v1

    #@5
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoSlice$Column;->ADN_CAPACITY:Ljava/lang/String;

    #@7
    .line 127
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@9
    const/4 v1, 0x1

    #@a
    aget-object v0, v0, v1

    #@c
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoSlice$Column;->ANR_TYPE:Ljava/lang/String;

    #@e
    .line 128
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@10
    const/4 v1, 0x2

    #@11
    aget-object v0, v0, v1

    #@13
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoSlice$Column;->ANR_CAPACITY:Ljava/lang/String;

    #@15
    .line 129
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@17
    const/4 v1, 0x3

    #@18
    aget-object v0, v0, v1

    #@1a
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoSlice$Column;->EMAIL_TYPE:Ljava/lang/String;

    #@1c
    .line 130
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@1e
    const/4 v1, 0x4

    #@1f
    aget-object v0, v0, v1

    #@21
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoSlice$Column;->EMAIL_CAPACITY:Ljava/lang/String;

    #@23
    .line 131
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@25
    const/4 v1, 0x5

    #@26
    aget-object v0, v0, v1

    #@28
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoSlice$Column;->RESERVED1:Ljava/lang/String;

    #@2a
    .line 132
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@2c
    const/4 v1, 0x6

    #@2d
    aget-object v0, v0, v1

    #@2f
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoSlice$Column;->RESERVED2:Ljava/lang/String;

    #@31
    .line 133
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@33
    const/4 v1, 0x7

    #@34
    aget-object v0, v0, v1

    #@36
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoSlice$Column;->RESERVED3:Ljava/lang/String;

    #@38
    .line 134
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@3a
    const/16 v1, 0x8

    #@3c
    aget-object v0, v0, v1

    #@3e
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoSlice$Column;->RESERVED4:Ljava/lang/String;

    #@40
    .line 135
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@42
    const/16 v1, 0x9

    #@44
    aget-object v0, v0, v1

    #@46
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoSlice$Column;->RESERVED5:Ljava/lang/String;

    #@48
    .line 136
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@4a
    const/16 v1, 0xa

    #@4c
    aget-object v0, v0, v1

    #@4e
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoSlice$Column;->RESERVED6:Ljava/lang/String;

    #@50
    .line 137
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@52
    const/16 v1, 0xb

    #@54
    aget-object v0, v0, v1

    #@56
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoSlice$Column;->RESERVED7:Ljava/lang/String;

    #@58
    .line 138
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@5a
    const/16 v1, 0xc

    #@5c
    aget-object v0, v0, v1

    #@5e
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoSlice$Column;->RESERVED8:Ljava/lang/String;

    #@60
    .line 139
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@62
    const/16 v1, 0xd

    #@64
    aget-object v0, v0, v1

    #@66
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoSlice$Column;->RESERVED9:Ljava/lang/String;

    #@68
    .line 140
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@6a
    const/16 v1, 0xe

    #@6c
    aget-object v0, v0, v1

    #@6e
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoSlice$Column;->RESERVED10:Ljava/lang/String;

    #@70
    .line 141
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@72
    const/16 v1, 0xf

    #@74
    aget-object v0, v0, v1

    #@76
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoSlice$Column;->RESERVED11:Ljava/lang/String;

    #@78
    return-void
.end method
