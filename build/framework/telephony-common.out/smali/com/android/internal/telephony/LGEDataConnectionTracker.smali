.class public Lcom/android/internal/telephony/LGEDataConnectionTracker;
.super Landroid/os/Handler;
.source "LGEDataConnectionTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/LGEDataConnectionTracker$2;,
        Lcom/android/internal/telephony/LGEDataConnectionTracker$LTEDataRoamingSettingObserver;
    }
.end annotation


# static fields
.field public static final ACTION_GW_ROAMING_DATA_CONNECTION_LGU:Ljava/lang/String; = "lge.intent.action.GW_ROAMING_DATA_CONNECTION_LGU"

.field public static final ACTION_LTE_ROAMING_DATA_CONNECTION_LGU:Ljava/lang/String; = "lge.intent.action.LTE_ROAMING_DATA_CONNECTION_LGU"

.field static final ApplyToprotectionVoiceInMobie:I = 0x1

.field protected static final BASE:I = 0x42000

.field public static final CMD_SET_DEPENDENCY_MET:I = 0x4201f

.field public static final CMD_SET_POLICY_DATA_ENABLE:I = 0x42020

.field public static final CMD_SET_USER_DATA_ENABLE:I = 0x4201d

.field public static final DATA_LTE_ROAMING:Ljava/lang/String; = "data_lte_roaming"

.field protected static final EVENT_APN_CHANGED:I = 0x42013

.field protected static final EVENT_CDMA_DATA_DETACHED:I = 0x42014

.field protected static final EVENT_CDMA_OTA_PROVISION:I = 0x42019

.field protected static final EVENT_CDMA_SUBSCRIPTION_SOURCE_CHANGED:I = 0x42015

.field public static final EVENT_CLEAN_UP_ALL_CONNECTIONS:I = 0x4201e

.field public static final EVENT_CLEAN_UP_CONNECTION:I = 0x42018

.field protected static final EVENT_CPA_PACKAGE_CHECK:I = 0x4203c

.field protected static final EVENT_DATA_CONNECTION_ATTACHED:I = 0x42010

.field protected static final EVENT_DATA_CONNECTION_DETACHED:I = 0x42009

.field protected static final EVENT_DATA_ERROR_FAIL_CAUSE:I = 0x4202c

.field protected static final EVENT_DATA_LOCK_STATE_CHANGED:I = 0x42032

.field public static final EVENT_DATA_PROFILE_NV:I = 0x42037

.field public static final EVENT_DATA_RADIO_ON:I = 0x42038

.field protected static final EVENT_DATA_SETUP_COMPLETE:I = 0x42000

.field protected static final EVENT_DATA_STALL_ALARM:I = 0x42011

.field protected static final EVENT_DATA_STATE_CHANGED:I = 0x42004

.field protected static final EVENT_DEFAULT_SETUP:I = 0x4203e

.field protected static final EVENT_DISCONNECT_DONE:I = 0x4200f

.field protected static final EVENT_DO_RECOVERY:I = 0x42012

.field protected static final EVENT_ENABLE_NEW_APN:I = 0x4200d

.field protected static final EVENT_FAKE_DATACONNECTION_EVENT:I = 0x4203d

.field protected static final EVENT_GET_DATA_CALL_PROFILE_DONE:I = 0x42024

.field protected static final EVENT_GET_W_MODEM_INFO_CS_PROTECTION:I = 0x4202e

.field protected static final EVENT_GO_FASTDORMANCY_DELAYED:I = 0x42035

.field protected static final EVENT_ICC_CHANGED:I = 0x42021

.field protected static final EVENT_LINK_STATE_CHANGED:I = 0x4200a

.field protected static final EVENT_MODEM_DATA_PROFILE_READY:I = 0x42025

.field public static final EVENT_OMADM_LOCK_NV:I = 0x42036

.field protected static final EVENT_PACKET_PAGING_RECEIVED:I = 0x42034

.field protected static final EVENT_PCSCF_ADDR_CHANGED:I = 0x4202b

.field protected static final EVENT_POLL_PDP:I = 0x42005

.field protected static final EVENT_PREFERRED_NETWORK_TYPE_CHANGED:I = 0x42027

.field protected static final EVENT_PS_RESTRICT_DISABLED:I = 0x42017

.field protected static final EVENT_PS_RESTRICT_ENABLED:I = 0x42016

.field protected static final EVENT_PS_RETRY_RESET:I = 0x42026

.field protected static final EVENT_RADIO_AVAILABLE:I = 0x42001

.field protected static final EVENT_RADIO_OFF_OR_NOT_AVAILABLE:I = 0x42006

.field public static final EVENT_RADIO_REGISTERED_TO_NETWORK:I = 0x42031

.field protected static final EVENT_RAT_CHANGED:I = 0x4202d

.field protected static final EVENT_READ_MODEM_PROFILES:I = 0x42023

.field protected static final EVENT_RECORDS_LOADED:I = 0x42002

.field protected static final EVENT_RESET_DONE:I = 0x4201c

.field protected static final EVENT_RESTART_RADIO:I = 0x4201a

.field protected static final EVENT_RESTORE_DEFAULT_APN:I = 0x4200e

.field public static final EVENT_RESYNC:I = 0x42039

.field protected static final EVENT_ROAMING_OFF:I = 0x4200c

.field protected static final EVENT_ROAMING_ON:I = 0x4200b

.field protected static final EVENT_SETDEFAULT_TOCHANGE_AFTER_DELAY:I = 0x4202f

.field protected static final EVENT_SET_INTERNAL_DATA_ENABLE:I = 0x4201b

.field protected static final EVENT_TETHERED_MODE_STATE_CHANGED:I = 0x42022

.field protected static final EVENT_TRY_SETUP_DATA:I = 0x42003

.field protected static final EVENT_VOICE_CALL_ENDED:I = 0x42008

.field protected static final EVENT_VOICE_CALL_STARTED:I = 0x42007

.field public static final EVENT_WAIT_PENDING:I = 0x42033

.field private static final LOG_TAG:Ljava/lang/String; = "[LGE_DATA][LGEDCT] "

.field protected static final MESSAGE_GET_PREFERRED_NETWORK_TYPE:I = 0x42028

.field protected static final MESSAGE_SET_PREFERRED_NETWORK_TYPE:I = 0x42029

.field static final SKT_mms_apn:Ljava/lang/String; = "mmsonly.sktelecom.com"

.field public static mJustIsBTConnected:Z = false

.field public static mJustIsWifiConnected:Z = false

.field public static final sConnectionStatus:Ljava/lang/String; = "Connection_Status"

.field private static setTeardownRequested:[Z

.field private static voice_call_ing:Z


# instance fields
.field protected APN_ID_FOR_IMS:I

.field protected APN_ID_FOR_LTE_Roaming:I

.field public bConnectionStatus:Z

.field protected exist_ims_type_in_mpdn:Z

.field public isAvailableLTEDataRoaming:Z

.field isGsm:Z

.field protected is_single_apn_flow:Z

.field protected mConnMgr:Landroid/net/ConnectivityManager;

.field private mDct:Lcom/android/internal/telephony/DataConnectionTracker;

.field mGsst:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

.field protected mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lcom/android/internal/telephony/uicc/IccRecords;",
            ">;"
        }
    .end annotation
.end field

.field protected mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private final mLTEDataRoamingSettingObserver:Lcom/android/internal/telephony/LGEDataConnectionTracker$LTEDataRoamingSettingObserver;

.field private mNotification:Landroid/app/Notification;

.field private mPhone:Lcom/android/internal/telephony/PhoneBase;

.field protected mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

.field final notificationId:I

.field private original_apn_formms:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 195
    sput-boolean v1, Lcom/android/internal/telephony/LGEDataConnectionTracker;->voice_call_ing:Z

    #@3
    .line 198
    const/16 v0, 0x12

    #@5
    new-array v0, v0, [Z

    #@7
    sput-object v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->setTeardownRequested:[Z

    #@9
    .line 199
    sput-boolean v1, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mJustIsWifiConnected:Z

    #@b
    .line 201
    sput-boolean v1, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mJustIsBTConnected:Z

    #@d
    return-void
.end method

.method public constructor <init>(Lcom/android/internal/telephony/DataConnectionTracker;Lcom/android/internal/telephony/PhoneBase;)V
    .registers 15
    .parameter "mmdct"
    .parameter "p"

    #@0
    .prologue
    const/16 v11, 0x9f6

    #@2
    const/4 v6, 0x1

    #@3
    const/4 v10, 0x0

    #@4
    const/4 v7, 0x0

    #@5
    .line 640
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@8
    .line 189
    iput-object v10, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->original_apn_formms:Ljava/lang/String;

    #@a
    .line 193
    iput-boolean v7, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->exist_ims_type_in_mpdn:Z

    #@c
    .line 211
    new-instance v5, Ljava/util/concurrent/atomic/AtomicReference;

    #@e
    invoke-direct {v5}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    #@11
    iput-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@13
    .line 213
    iput-boolean v7, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->is_single_apn_flow:Z

    #@15
    .line 223
    iput v7, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->APN_ID_FOR_LTE_Roaming:I

    #@17
    .line 224
    iput v7, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->APN_ID_FOR_IMS:I

    #@19
    .line 229
    iput-boolean v7, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->isAvailableLTEDataRoaming:Z

    #@1b
    .line 236
    iput-boolean v7, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->bConnectionStatus:Z

    #@1d
    .line 242
    iput v11, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->notificationId:I

    #@1f
    .line 245
    new-instance v5, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;

    #@21
    invoke-direct {v5, p0}, Lcom/android/internal/telephony/LGEDataConnectionTracker$1;-><init>(Lcom/android/internal/telephony/LGEDataConnectionTracker;)V

    #@24
    iput-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@26
    .line 2394
    new-instance v5, Lcom/android/internal/telephony/LGEDataConnectionTracker$LTEDataRoamingSettingObserver;

    #@28
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2a
    invoke-direct {v5, p0, v8}, Lcom/android/internal/telephony/LGEDataConnectionTracker$LTEDataRoamingSettingObserver;-><init>(Lcom/android/internal/telephony/LGEDataConnectionTracker;Landroid/os/Handler;)V

    #@2d
    iput-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mLTEDataRoamingSettingObserver:Lcom/android/internal/telephony/LGEDataConnectionTracker$LTEDataRoamingSettingObserver;

    #@2f
    .line 642
    const-string v5, "[LGE_DATA][LGEDCT] "

    #@31
    const-string v8, "LGEDataConnectionTracker() has created"

    #@33
    invoke-static {v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 643
    iput-object p1, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@38
    .line 644
    iput-object p2, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@3a
    .line 646
    iget-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@3c
    iget-object v5, v5, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@3e
    invoke-interface {v5}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@41
    move-result-object v5

    #@42
    iget-object v5, v5, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@44
    const-string v8, "KTBASE"

    #@46
    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@49
    move-result v5

    #@4a
    if-nez v5, :cond_78

    #@4c
    iget-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@4e
    iget-object v5, v5, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@50
    invoke-interface {v5}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@53
    move-result-object v5

    #@54
    iget-object v5, v5, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@56
    const-string v8, "SKTBASE"

    #@58
    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5b
    move-result v5

    #@5c
    if-nez v5, :cond_78

    #@5e
    iget-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@60
    iget-object v5, v5, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@62
    invoke-interface {v5}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@65
    move-result-object v5

    #@66
    iget-object v5, v5, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@68
    const-string v8, "LGTBASE"

    #@6a
    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6d
    move-result v5

    #@6e
    if-nez v5, :cond_78

    #@70
    .line 650
    const-string v5, "[LGE_DATA][LGEDCT] "

    #@72
    const-string v6, "other country do not use this function. so return."

    #@74
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@77
    .line 787
    :goto_77
    return-void

    #@78
    .line 654
    :cond_78
    new-instance v1, Landroid/content/IntentFilter;

    #@7a
    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    #@7d
    .line 655
    .local v1, filter:Landroid/content/IntentFilter;
    const-string v5, "android.intent.action.SCREEN_ON"

    #@7f
    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@82
    .line 657
    const-string v5, "lge.test.limit_data_usage"

    #@84
    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@87
    .line 659
    const-string v5, "com.skt.CALL_PROTECTION_STATUS_CHANGED"

    #@89
    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@8c
    .line 660
    const-string v5, "com.skt.CALL_PROTECTION_MENU_OFF"

    #@8e
    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@91
    .line 661
    const-string v5, "com.skt.CALL_PROTECTION_MENU_ON"

    #@93
    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@96
    .line 662
    const-string v5, "com.skt.test_intent"

    #@98
    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@9b
    .line 663
    const-string v5, "com.kt.CALL_PROTECTION_CALL_START"

    #@9d
    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@a0
    .line 664
    const-string v5, "com.kt.CALL_PROTECTION_MENU_OFF"

    #@a2
    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@a5
    .line 665
    const-string v5, "com.kt.CALL_PROTECTION_MENU_ON"

    #@a7
    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@aa
    .line 666
    const-string v5, "com.lge.GprsAttachedIsTrue"

    #@ac
    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@af
    .line 667
    const-string v5, "android.intent.action.ANY_DATA_STATE"

    #@b1
    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@b4
    .line 668
    const-string v5, "android.intent.action.DATA_CONNECTION_FAILED"

    #@b6
    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@b9
    .line 669
    const-string v5, "lge.android.telephony.dataflow"

    #@bb
    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@be
    .line 671
    const-string v5, "lge.intent.action.LGU_LTE_ROAMING_IS_AVAILABLE"

    #@c0
    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@c3
    .line 674
    iget-object v5, p2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@c5
    const v8, 0x42001

    #@c8
    invoke-interface {v5, p0, v8, v10}, Lcom/android/internal/telephony/CommandsInterface;->registerForAvailable(Landroid/os/Handler;ILjava/lang/Object;)V

    #@cb
    .line 676
    invoke-virtual {p2}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@ce
    move-result-object v5

    #@cf
    if-eqz v5, :cond_e5

    #@d1
    .line 677
    invoke-virtual {p2}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@d4
    move-result-object v5

    #@d5
    const v8, 0x42010

    #@d8
    invoke-virtual {v5, p0, v8, v10}, Lcom/android/internal/telephony/ServiceStateTracker;->registerForDataConnectionAttached(Landroid/os/Handler;ILjava/lang/Object;)V

    #@db
    .line 678
    invoke-virtual {p2}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@de
    move-result-object v5

    #@df
    const v8, 0x42031

    #@e2
    invoke-virtual {v5, p0, v8, v10}, Lcom/android/internal/telephony/ServiceStateTracker;->registerForNetworkAttached(Landroid/os/Handler;ILjava/lang/Object;)V

    #@e5
    .line 684
    :cond_e5
    iget-object v5, p2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@e7
    const v8, 0x42034

    #@ea
    invoke-interface {v5, p0, v8, v10}, Lcom/android/internal/telephony/CommandsInterface;->registorForPacketPaging(Landroid/os/Handler;ILjava/lang/Object;)V

    #@ed
    .line 686
    iget-object v5, p2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@ef
    const v8, 0x42004

    #@f2
    invoke-interface {v5, p0, v8, v10}, Lcom/android/internal/telephony/CommandsInterface;->registerForDataNetworkStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@f5
    .line 688
    invoke-virtual {p2}, Lcom/android/internal/telephony/PhoneBase;->getCallTracker()Lcom/android/internal/telephony/CallTracker;

    #@f8
    move-result-object v5

    #@f9
    if-eqz v5, :cond_10f

    #@fb
    .line 689
    invoke-virtual {p2}, Lcom/android/internal/telephony/PhoneBase;->getCallTracker()Lcom/android/internal/telephony/CallTracker;

    #@fe
    move-result-object v5

    #@ff
    const v8, 0x42008

    #@102
    invoke-virtual {v5, p0, v8, v10}, Lcom/android/internal/telephony/CallTracker;->registerForVoiceCallEnded(Landroid/os/Handler;ILjava/lang/Object;)V

    #@105
    .line 690
    invoke-virtual {p2}, Lcom/android/internal/telephony/PhoneBase;->getCallTracker()Lcom/android/internal/telephony/CallTracker;

    #@108
    move-result-object v5

    #@109
    const v8, 0x42007

    #@10c
    invoke-virtual {v5, p0, v8, v10}, Lcom/android/internal/telephony/CallTracker;->registerForVoiceCallStarted(Landroid/os/Handler;ILjava/lang/Object;)V

    #@10f
    .line 694
    :cond_10f
    const v5, 0x4203d

    #@112
    invoke-virtual {p1, p0, v5, v10}, Lcom/android/internal/telephony/DataConnectionTracker;->registerForDataConnectEvent(Landroid/os/Handler;ILjava/lang/Object;)V

    #@115
    .line 696
    invoke-static {}, Lcom/android/internal/telephony/uicc/UiccController;->getInstance()Lcom/android/internal/telephony/uicc/UiccController;

    #@118
    move-result-object v5

    #@119
    iput-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@11b
    .line 697
    iget-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@11d
    const v8, 0x42021

    #@120
    invoke-virtual {v5, p0, v8, v10}, Lcom/android/internal/telephony/uicc/UiccController;->registerForIccChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@123
    .line 699
    iget-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@125
    invoke-virtual {v5}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@128
    move-result-object v5

    #@129
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@12b
    iget-object v9, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@12d
    invoke-virtual {v5, v8, v1, v10, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@130
    .line 700
    iget-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@132
    invoke-virtual {v5}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@135
    move-result-object v5

    #@136
    const-string v8, "connectivity"

    #@138
    invoke-virtual {v5, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@13b
    move-result-object v5

    #@13c
    check-cast v5, Landroid/net/ConnectivityManager;

    #@13e
    iput-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mConnMgr:Landroid/net/ConnectivityManager;

    #@140
    .line 704
    iget-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@142
    iget-object v5, v5, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@144
    invoke-interface {v5}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@147
    move-result-object v5

    #@148
    iget-boolean v5, v5, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_VOICE_PROTECTION_KR:Z

    #@14a
    if-ne v5, v6, :cond_169

    #@14c
    .line 706
    iget-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@14e
    invoke-virtual {v5}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@151
    move-result-object v5

    #@152
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@155
    move-result-object v5

    #@156
    const-string v8, "enable_call_protect_when_calling"

    #@158
    invoke-static {v5, v8, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@15b
    .line 708
    const-string v5, "net.is_dropping_packet"

    #@15d
    const-string v8, "false"

    #@15f
    invoke-static {v5, v8}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@162
    .line 709
    iget-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@164
    iget-object v5, v5, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@166
    invoke-virtual {v5, v7}, Lcom/android/internal/telephony/DataConnectionManager;->functionForPacketDrop(Z)V

    #@169
    .line 721
    :cond_169
    if-eqz p2, :cond_183

    #@16b
    invoke-virtual {p2}, Lcom/android/internal/telephony/PhoneBase;->getPhoneType()I

    #@16e
    move-result v5

    #@16f
    if-ne v5, v6, :cond_183

    #@171
    .line 723
    invoke-virtual {p2}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@174
    move-result-object v5

    #@175
    instance-of v5, v5, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@177
    if-eqz v5, :cond_183

    #@179
    .line 724
    invoke-virtual {p2}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@17c
    move-result-object v5

    #@17d
    check-cast v5, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@17f
    iput-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mGsst:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@181
    .line 725
    iput-boolean v6, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->isGsm:Z

    #@183
    .line 732
    :cond_183
    iget-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@185
    iget-object v5, v5, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@187
    invoke-interface {v5}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@18a
    move-result-object v5

    #@18b
    iget-boolean v5, v5, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_NOTI_USERDATADISABLE_KR:Z

    #@18d
    if-ne v5, v6, :cond_25b

    #@18f
    .line 733
    new-instance v5, Landroid/app/Notification;

    #@191
    invoke-direct {v5}, Landroid/app/Notification;-><init>()V

    #@194
    iput-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mNotification:Landroid/app/Notification;

    #@196
    .line 734
    iget-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mNotification:Landroid/app/Notification;

    #@198
    const-wide/16 v8, 0x0

    #@19a
    iput-wide v8, v5, Landroid/app/Notification;->when:J

    #@19c
    .line 735
    iget-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mNotification:Landroid/app/Notification;

    #@19e
    const/16 v8, 0x10

    #@1a0
    iput v8, v5, Landroid/app/Notification;->flags:I

    #@1a2
    .line 736
    iget-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mNotification:Landroid/app/Notification;

    #@1a4
    const v8, 0x108008a

    #@1a7
    iput v8, v5, Landroid/app/Notification;->icon:I

    #@1a9
    .line 737
    new-instance v2, Landroid/content/Intent;

    #@1ab
    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    #@1ae
    .line 738
    .local v2, intent:Landroid/content/Intent;
    iget-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mNotification:Landroid/app/Notification;

    #@1b0
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1b2
    invoke-virtual {v8}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@1b5
    move-result-object v8

    #@1b6
    const/high16 v9, 0x1000

    #@1b8
    invoke-static {v8, v7, v2, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@1bb
    move-result-object v8

    #@1bc
    iput-object v8, v5, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    #@1be
    .line 741
    iget-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1c0
    invoke-virtual {v5}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@1c3
    move-result-object v5

    #@1c4
    const v8, 0x209030c

    #@1c7
    invoke-virtual {v5, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@1ca
    move-result-object v0

    #@1cb
    .line 742
    .local v0, details:Ljava/lang/CharSequence;
    iget-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1cd
    invoke-virtual {v5}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@1d0
    move-result-object v5

    #@1d1
    const v8, 0x209030b

    #@1d4
    invoke-virtual {v5, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@1d7
    move-result-object v4

    #@1d8
    .line 743
    .local v4, title:Ljava/lang/CharSequence;
    iget-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mNotification:Landroid/app/Notification;

    #@1da
    iput-object v4, v5, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@1dc
    .line 745
    const-string v5, "SKTBASE"

    #@1de
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1e0
    iget-object v8, v8, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1e2
    invoke-interface {v8}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1e5
    move-result-object v8

    #@1e6
    iget-object v8, v8, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@1e8
    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1eb
    move-result v5

    #@1ec
    if-eqz v5, :cond_2ae

    #@1ee
    .line 746
    const-string v5, "com.android.settings"

    #@1f0
    const-string v6, "com.android.settings.lgesetting.wireless.DataEnabledSettingBootableSKT"

    #@1f2
    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1f5
    .line 757
    :goto_1f5
    iget-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mNotification:Landroid/app/Notification;

    #@1f7
    iget-object v6, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1f9
    invoke-virtual {v6}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@1fc
    move-result-object v6

    #@1fd
    invoke-static {v6, v7, v2, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@200
    move-result-object v6

    #@201
    iput-object v6, v5, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    #@203
    .line 759
    iget-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mNotification:Landroid/app/Notification;

    #@205
    iget-object v6, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@207
    invoke-virtual {v6}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@20a
    move-result-object v6

    #@20b
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mNotification:Landroid/app/Notification;

    #@20d
    iget-object v8, v8, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    #@20f
    invoke-virtual {v5, v6, v4, v0, v8}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@212
    .line 762
    iget-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@214
    invoke-virtual {v5}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@217
    move-result-object v5

    #@218
    const-string v6, "notification"

    #@21a
    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@21d
    move-result-object v3

    #@21e
    check-cast v3, Landroid/app/NotificationManager;

    #@220
    .line 765
    .local v3, notificationManager:Landroid/app/NotificationManager;
    iget-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mConnMgr:Landroid/net/ConnectivityManager;

    #@222
    invoke-virtual {v5}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z

    #@225
    move-result v5

    #@226
    if-nez v5, :cond_25b

    #@228
    iget-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@22a
    invoke-virtual {v5}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@22d
    move-result-object v5

    #@22e
    invoke-virtual {v5}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@231
    move-result v5

    #@232
    if-nez v5, :cond_25b

    #@234
    .line 766
    const-string v5, "[LGE_DATA][LGEDCT] "

    #@236
    new-instance v6, Ljava/lang/StringBuilder;

    #@238
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@23b
    const-string v8, "[LGEDataConnectionTracker]setNotification: put notification "

    #@23d
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@240
    move-result-object v6

    #@241
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@244
    move-result-object v6

    #@245
    const-string v8, " / "

    #@247
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24a
    move-result-object v6

    #@24b
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24e
    move-result-object v6

    #@24f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@252
    move-result-object v6

    #@253
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@256
    .line 767
    iget-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mNotification:Landroid/app/Notification;

    #@258
    invoke-virtual {v3, v11, v5}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@25b
    .line 773
    .end local v0           #details:Ljava/lang/CharSequence;
    .end local v2           #intent:Landroid/content/Intent;
    .end local v3           #notificationManager:Landroid/app/NotificationManager;
    .end local v4           #title:Ljava/lang/CharSequence;
    :cond_25b
    iget-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@25d
    iget-object v5, v5, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@25f
    invoke-interface {v5}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@262
    move-result-object v5

    #@263
    iget-boolean v5, v5, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_LTE_ROAMING_LGU:Z

    #@265
    if-eqz v5, :cond_2a1

    #@267
    .line 774
    const-string v5, "net.Is_phone_booted"

    #@269
    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@26c
    move-result-object v5

    #@26d
    const-string v6, "true"

    #@26f
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@272
    move-result v5

    #@273
    if-eqz v5, :cond_2a1

    #@275
    .line 775
    const-string v5, "[LGE_DATA][LGEDCT] "

    #@277
    new-instance v6, Ljava/lang/StringBuilder;

    #@279
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@27c
    const-string v8, "LGEDataConnectionTracker(): in Is_phone_booted, getLTEDataRoamingEnable() : "

    #@27e
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@281
    move-result-object v6

    #@282
    invoke-virtual {p0}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->getLTEDataRoamingEnable()Z

    #@285
    move-result v8

    #@286
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@289
    move-result-object v6

    #@28a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28d
    move-result-object v6

    #@28e
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@291
    .line 776
    invoke-virtual {p0}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->getLTEDataRoamingEnable()Z

    #@294
    move-result v5

    #@295
    if-eqz v5, :cond_2a1

    #@297
    .line 777
    const-string v5, "[LGE_DATA][LGEDCT] "

    #@299
    const-string v6, "LGEDataConnectionTracker(): when only booting case and data_lte_roaming is 1, forcely set to 0"

    #@29b
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29e
    .line 778
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->setLTEDataRoamingEnable(Z)V

    #@2a1
    .line 784
    :cond_2a1
    iget-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mLTEDataRoamingSettingObserver:Lcom/android/internal/telephony/LGEDataConnectionTracker$LTEDataRoamingSettingObserver;

    #@2a3
    iget-object v6, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2a5
    invoke-virtual {v6}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@2a8
    move-result-object v6

    #@2a9
    invoke-virtual {v5, v6}, Lcom/android/internal/telephony/LGEDataConnectionTracker$LTEDataRoamingSettingObserver;->register(Landroid/content/Context;)V

    #@2ac
    goto/16 :goto_77

    #@2ae
    .line 747
    .restart local v0       #details:Ljava/lang/CharSequence;
    .restart local v2       #intent:Landroid/content/Intent;
    .restart local v4       #title:Ljava/lang/CharSequence;
    :cond_2ae
    const-string v5, "KTBASE"

    #@2b0
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2b2
    iget-object v8, v8, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2b4
    invoke-interface {v8}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@2b7
    move-result-object v8

    #@2b8
    iget-object v8, v8, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@2ba
    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2bd
    move-result v5

    #@2be
    if-eqz v5, :cond_2dd

    #@2c0
    .line 748
    const-string v5, "com.android.settings"

    #@2c2
    const-string v8, "com.android.settings.lgesetting.wireless.DataNetworkModePayPopupKT"

    #@2c4
    invoke-virtual {v2, v5, v8}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@2c7
    .line 749
    const-string v8, "isRoaming"

    #@2c9
    iget-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2cb
    invoke-virtual {v5}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@2ce
    move-result-object v5

    #@2cf
    invoke-virtual {v5}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@2d2
    move-result v5

    #@2d3
    if-ne v5, v6, :cond_2db

    #@2d5
    move v5, v6

    #@2d6
    :goto_2d6
    invoke-virtual {v2, v8, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@2d9
    goto/16 :goto_1f5

    #@2db
    :cond_2db
    move v5, v7

    #@2dc
    goto :goto_2d6

    #@2dd
    .line 750
    :cond_2dd
    const-string v5, "LGTBASE"

    #@2df
    iget-object v6, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2e1
    iget-object v6, v6, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2e3
    invoke-interface {v6}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@2e6
    move-result-object v6

    #@2e7
    iget-object v6, v6, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@2e9
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2ec
    move-result v5

    #@2ed
    if-eqz v5, :cond_2f8

    #@2ef
    .line 751
    const-string v5, "com.android.settings"

    #@2f1
    const-string v6, "com.android.settings.lgesetting.wireless.DataNetworkModePayPopupLGT"

    #@2f3
    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@2f6
    goto/16 :goto_1f5

    #@2f8
    .line 754
    :cond_2f8
    const-string v5, "[LGE_DATA][LGEDCT] "

    #@2fa
    const-string v6, "[LGEDataConnectionTracker] it\'s abnormal case"

    #@2fc
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2ff
    goto/16 :goto_1f5
.end method

.method static synthetic access$000(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/DataConnectionTracker;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 120
    iget-object v0, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/internal/telephony/LGEDataConnectionTracker;)Lcom/android/internal/telephony/PhoneBase;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 120
    iget-object v0, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2
    return-object v0
.end method

.method static synthetic access$200()[Z
    .registers 1

    #@0
    .prologue
    .line 120
    sget-object v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->setTeardownRequested:[Z

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/internal/telephony/LGEDataConnectionTracker;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 120
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->deleteDefualt_DNS(Z)V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/internal/telephony/LGEDataConnectionTracker;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 120
    invoke-direct {p0}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->handleLTEDataOnRoamingChange()V

    #@3
    return-void
.end method

.method private deleteDefualt_DNS(Z)V
    .registers 12
    .parameter "is_mpdn_issue"

    #@0
    .prologue
    .line 1724
    const/4 v0, 0x0

    #@1
    .line 1725
    .local v0, checkInfo:Landroid/net/NetworkInfo;
    const/4 v6, 0x0

    #@2
    .line 1727
    .local v6, p:Landroid/net/LinkProperties;
    if-eqz p1, :cond_40

    #@4
    .line 1728
    iget-object v7, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mConnMgr:Landroid/net/ConnectivityManager;

    #@6
    const/16 v8, 0xb

    #@8
    invoke-virtual {v7, v8}, Landroid/net/ConnectivityManager;->getLinkProperties(I)Landroid/net/LinkProperties;

    #@b
    move-result-object v6

    #@c
    .line 1729
    iget-object v7, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mConnMgr:Landroid/net/ConnectivityManager;

    #@e
    const/4 v8, 0x5

    #@f
    invoke-virtual {v7, v8}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@12
    move-result-object v0

    #@13
    .line 1736
    :goto_13
    const-string v7, "[LGE_DATA][LGEDCT] "

    #@15
    new-instance v8, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v9, "deleteDefualt_DNS for check (is_mpdn_issue) :: "

    #@1c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v8

    #@20
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@23
    move-result-object v8

    #@24
    const-string v9, "checkInfo = "

    #@26
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v8

    #@2a
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v8

    #@2e
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v8

    #@32
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 1738
    if-eqz v0, :cond_3f

    #@37
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    #@3a
    move-result v7

    #@3b
    if-eqz v7, :cond_3f

    #@3d
    .line 1741
    if-nez v6, :cond_4f

    #@3f
    .line 1759
    :cond_3f
    return-void

    #@40
    .line 1732
    :cond_40
    iget-object v7, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mConnMgr:Landroid/net/ConnectivityManager;

    #@42
    const/4 v8, 0x2

    #@43
    invoke-virtual {v7, v8}, Landroid/net/ConnectivityManager;->getLinkProperties(I)Landroid/net/LinkProperties;

    #@46
    move-result-object v6

    #@47
    .line 1733
    iget-object v7, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mConnMgr:Landroid/net/ConnectivityManager;

    #@49
    const/4 v8, 0x1

    #@4a
    invoke-virtual {v7, v8}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@4d
    move-result-object v0

    #@4e
    goto :goto_13

    #@4f
    .line 1743
    :cond_4f
    invoke-virtual {v6}, Landroid/net/LinkProperties;->getDnses()Ljava/util/Collection;

    #@52
    move-result-object v2

    #@53
    .line 1745
    .local v2, dnses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    invoke-interface {v2}, Ljava/util/Collection;->size()I

    #@56
    move-result v7

    #@57
    if-eqz v7, :cond_3f

    #@59
    .line 1747
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@5c
    move-result-object v5

    #@5d
    .local v5, i$:Ljava/util/Iterator;
    :cond_5d
    :goto_5d
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@60
    move-result v7

    #@61
    if-eqz v7, :cond_3f

    #@63
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@66
    move-result-object v1

    #@67
    check-cast v1, Ljava/net/InetAddress;

    #@69
    .line 1749
    .local v1, dns:Ljava/net/InetAddress;
    const/4 v3, 0x1

    #@6a
    .line 1751
    .local v3, i:I
    if-eqz v1, :cond_5d

    #@6c
    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@6f
    move-result-object v7

    #@70
    const-string v8, "0.0.0.0"

    #@72
    invoke-static {v7, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@75
    move-result v7

    #@76
    if-nez v7, :cond_5d

    #@78
    .line 1753
    const-string v7, "[LGE_DATA][LGEDCT] "

    #@7a
    new-instance v8, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    const-string v9, "delete dns "

    #@81
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v8

    #@85
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v8

    #@89
    const-string v9, " for "

    #@8b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v8

    #@8f
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    #@92
    move-result-object v9

    #@93
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v8

    #@97
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9a
    move-result-object v8

    #@9b
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9e
    .line 1754
    new-instance v7, Ljava/lang/StringBuilder;

    #@a0
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@a3
    const-string v8, "net.dns"

    #@a5
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v7

    #@a9
    add-int/lit8 v4, v3, 0x1

    #@ab
    .end local v3           #i:I
    .local v4, i:I
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v7

    #@af
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b2
    move-result-object v7

    #@b3
    const-string v8, ""

    #@b5
    invoke-static {v7, v8}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@b8
    move v3, v4

    #@b9
    .end local v4           #i:I
    .restart local v3       #i:I
    goto :goto_5d
.end method

.method private handleGetPreferredNetworkTypeResponse(Landroid/os/Message;)V
    .registers 8
    .parameter "msg"

    #@0
    .prologue
    .line 2233
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2
    check-cast v0, Landroid/os/AsyncResult;

    #@4
    .line 2235
    .local v0, ar:Landroid/os/AsyncResult;
    iget-object v3, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@6
    if-nez v3, :cond_b0

    #@8
    .line 2236
    iget-object v3, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@a
    check-cast v3, [I

    #@c
    check-cast v3, [I

    #@e
    const/4 v4, 0x0

    #@f
    aget v2, v3, v4

    #@11
    .line 2238
    .local v2, modemNetworkMode:I
    const-string v3, "[LGE_DATA][LGEDCT] "

    #@13
    new-instance v4, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v5, "handleGetPreferredNetworkTypeResponse: modemNetworkMode = "

    #@1a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v4

    #@1e
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v4

    #@26
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 2240
    iget-object v3, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2b
    invoke-virtual {v3}, Lcom/android/internal/telephony/DataConnectionTracker;->getPreferredNetworkMode()I

    #@2e
    move-result v1

    #@2f
    .line 2241
    .local v1, curPreferMode:I
    const-string v3, "[LGE_DATA][LGEDCT] "

    #@31
    new-instance v4, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v5, "handleGetPreferredNetworkTypeReponse: curPreferMode = "

    #@38
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v4

    #@3c
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v4

    #@40
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v4

    #@44
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 2246
    if-eqz v2, :cond_72

    #@49
    const/4 v3, 0x1

    #@4a
    if-eq v2, v3, :cond_72

    #@4c
    const/4 v3, 0x2

    #@4d
    if-eq v2, v3, :cond_72

    #@4f
    const/4 v3, 0x3

    #@50
    if-eq v2, v3, :cond_72

    #@52
    const/16 v3, 0xc

    #@54
    if-eq v2, v3, :cond_72

    #@56
    const/4 v3, 0x4

    #@57
    if-eq v2, v3, :cond_72

    #@59
    const/4 v3, 0x5

    #@5a
    if-eq v2, v3, :cond_72

    #@5c
    const/4 v3, 0x6

    #@5d
    if-eq v2, v3, :cond_72

    #@5f
    const/4 v3, 0x7

    #@60
    if-eq v2, v3, :cond_72

    #@62
    const/16 v3, 0x8

    #@64
    if-eq v2, v3, :cond_72

    #@66
    const/16 v3, 0x9

    #@68
    if-eq v2, v3, :cond_72

    #@6a
    const/16 v3, 0xa

    #@6c
    if-eq v2, v3, :cond_72

    #@6e
    const/16 v3, 0xb

    #@70
    if-ne v2, v3, :cond_b1

    #@72
    .line 2261
    :cond_72
    const-string v3, "[LGE_DATA][LGEDCT] "

    #@74
    new-instance v4, Ljava/lang/StringBuilder;

    #@76
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@79
    const-string v5, "handleGetPreferredNetworkTypeResponse: if 1: modemNetworkMode = "

    #@7b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v4

    #@7f
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@82
    move-result-object v4

    #@83
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@86
    move-result-object v4

    #@87
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8a
    .line 2265
    if-eq v2, v1, :cond_b0

    #@8c
    .line 2266
    const-string v3, "[LGE_DATA][LGEDCT] "

    #@8e
    const-string v4, "handleGetPreferredNetworkTypeResponse: if 2: modemNetworkMode != curPreferMode"

    #@90
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@93
    .line 2282
    const-string v3, "[LGE_DATA][LGEDCT] "

    #@95
    new-instance v4, Ljava/lang/StringBuilder;

    #@97
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9a
    const-string v5, "handleGetPreferredNetworkTypeResponse: setPreferredNetworkMode set to = "

    #@9c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v4

    #@a0
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v4

    #@a4
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a7
    move-result-object v4

    #@a8
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ab
    .line 2284
    iget-object v3, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@ad
    invoke-virtual {v3, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->setPreferredNetworkMode(I)V

    #@b0
    .line 2292
    .end local v1           #curPreferMode:I
    .end local v2           #modemNetworkMode:I
    :cond_b0
    :goto_b0
    return-void

    #@b1
    .line 2288
    .restart local v1       #curPreferMode:I
    .restart local v2       #modemNetworkMode:I
    :cond_b1
    const-string v3, "[LGE_DATA][LGEDCT] "

    #@b3
    const-string v4, "handleGetPreferredNetworkTypeResponse: else: reset to default"

    #@b5
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b8
    .line 2289
    iget-object v3, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@ba
    invoke-virtual {v3}, Lcom/android/internal/telephony/DataConnectionTracker;->resetNetworkModeToDefault()V

    #@bd
    goto :goto_b0
.end method

.method private handleLTEDataOnRoamingChange()V
    .registers 5

    #@0
    .prologue
    .line 2420
    invoke-virtual {p0}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->getLTEDataRoamingEnable()Z

    #@3
    move-result v0

    #@4
    .line 2421
    .local v0, enableDataLteRoaming:Z
    const-string v1, "[LGE_DATA][LGEDCT] "

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "handleLTEDataOnRoamingChange(), enableDataLteRoaming : "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 2423
    iget-object v1, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@1e
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnectionTracker;->apnSelectionHdlr:Lcom/android/internal/telephony/ApnSelectionHandler;

    #@20
    invoke-virtual {v1, v0}, Lcom/android/internal/telephony/ApnSelectionHandler;->selectApnForLteRoamingOfUplus(Z)V

    #@23
    .line 2424
    invoke-virtual {p0}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->sendPdnTable()V

    #@26
    .line 2425
    return-void
.end method

.method private handleSetPreferredNetworkTypeResponse(Landroid/os/Message;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    .line 2295
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2
    check-cast v0, Landroid/os/AsyncResult;

    #@4
    .line 2297
    .local v0, ar:Landroid/os/AsyncResult;
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@6
    if-nez v1, :cond_10

    #@8
    .line 2298
    const-string v1, "[LGE_DATA][LGEDCT] "

    #@a
    const-string v2, "SetPreferredNetworkType is success"

    #@c
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 2303
    :goto_f
    return-void

    #@10
    .line 2300
    :cond_10
    const-string v1, "[LGE_DATA][LGEDCT] "

    #@12
    new-instance v2, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v3, "SetPreferredNetworkType is failed, exception="

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    iget-object v3, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 2301
    iget-object v1, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2c
    const v2, 0x42028

    #@2f
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->obtainMessage(I)Landroid/os/Message;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/PhoneBase;->getPreferredNetworkType(Landroid/os/Message;)V

    #@36
    goto :goto_f
.end method

.method private retryAfterDisconnected(Ljava/lang/String;)Z
    .registers 6
    .parameter "reason"

    #@0
    .prologue
    .line 1789
    const/4 v1, 0x1

    #@1
    .line 1790
    .local v1, retry:Z
    const-string v2, "persist.telephony.mpdn"

    #@3
    const/4 v3, 0x1

    #@4
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@7
    move-result v0

    #@8
    .line 1791
    .local v0, SUPPORT_MPDN:Z
    const-string v2, "radioTurnedOff"

    #@a
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v2

    #@e
    if-nez v2, :cond_1a

    #@10
    if-nez v0, :cond_1b

    #@12
    const-string v2, "SinglePdnArbitration"

    #@14
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v2

    #@18
    if-eqz v2, :cond_1b

    #@1a
    .line 1793
    :cond_1a
    const/4 v1, 0x0

    #@1b
    .line 1795
    :cond_1b
    return v1
.end method


# virtual methods
.method public changePreferrredNetworkMode(Z)V
    .registers 15
    .parameter "enabled"

    #@0
    .prologue
    const v12, 0x42029

    #@3
    const/4 v11, 0x1

    #@4
    .line 2091
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@6
    const/4 v5, -0x1

    #@7
    .line 2092
    .local v5, newPreferMode:I
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@9
    invoke-virtual {v8}, Lcom/android/internal/telephony/DataConnectionTracker;->getPreferredNetworkMode()I

    #@c
    move-result v1

    #@d
    .line 2093
    .local v1, curPreferMode:I
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@f
    invoke-virtual {v8}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@12
    move-result-object v8

    #@13
    invoke-virtual {v8}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@16
    move-result v2

    #@17
    .line 2095
    .local v2, curRadioTech:I
    const/4 v0, 0x0

    #@18
    .line 2098
    .local v0, Is_LWmode_selected:Z
    const-string v8, "[LGE_DATA][LGEDCT] "

    #@1a
    new-instance v9, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v10, "[changePreferrredNetworkMode] enabled:"

    #@21
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v9

    #@25
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@28
    move-result-object v9

    #@29
    const-string v10, ", curPreferMode:"

    #@2b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v9

    #@2f
    iget-object v10, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@31
    invoke-static {v1}, Lcom/android/internal/telephony/DataConnectionTracker;->networkModeToString(I)Ljava/lang/String;

    #@34
    move-result-object v10

    #@35
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v9

    #@39
    const-string v10, ", curRadioTech:"

    #@3b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v9

    #@3f
    iget-object v10, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@41
    invoke-virtual {v10}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@44
    invoke-static {v2}, Landroid/telephony/ServiceState;->rilRadioTechnologyToString(I)Ljava/lang/String;

    #@47
    move-result-object v10

    #@48
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v9

    #@4c
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v9

    #@50
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    .line 2102
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@55
    invoke-virtual {v8}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@58
    move-result-object v8

    #@59
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5c
    move-result-object v8

    #@5d
    const-string v9, "mobile_data"

    #@5f
    invoke-static {v8, v9, v11}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@62
    move-result v3

    #@63
    .line 2103
    .local v3, dataNetwork:I
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@65
    invoke-virtual {v8}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@68
    move-result-object v8

    #@69
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6c
    move-result-object v8

    #@6d
    const-string v9, "data_roaming"

    #@6f
    invoke-static {v8, v9, v11}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@72
    move-result v7

    #@73
    .line 2104
    .local v7, roamingData:I
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@75
    invoke-virtual {v8}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@78
    move-result-object v8

    #@79
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7c
    move-result-object v8

    #@7d
    const-string v9, "lte_roaming"

    #@7f
    const/4 v10, 0x0

    #@80
    invoke-static {v8, v9, v10}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@83
    move-result v4

    #@84
    .line 2105
    .local v4, lteRoaming:I
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@86
    invoke-virtual {v8}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@89
    move-result-object v8

    #@8a
    invoke-virtual {v8}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@8d
    move-result v6

    #@8e
    .line 2107
    .local v6, roaming:Z
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@90
    iget-object v8, v8, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@92
    invoke-interface {v8}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@95
    move-result-object v8

    #@96
    iget-boolean v8, v8, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_LTE_ROAMING_KT:Z

    #@98
    if-eqz v8, :cond_cd

    #@9a
    if-eqz v6, :cond_cd

    #@9c
    .line 2109
    const-string v8, "[LGE_DATA][LGEDCT] "

    #@9e
    new-instance v9, Ljava/lang/StringBuilder;

    #@a0
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@a3
    const-string v10, "[sehyun] dataNetwork = "

    #@a5
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v9

    #@a9
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v9

    #@ad
    const-string v10, ", roamingData = "

    #@af
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v9

    #@b3
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v9

    #@b7
    const-string v10, ", lteRoaming = "

    #@b9
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v9

    #@bd
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v9

    #@c1
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c4
    move-result-object v9

    #@c5
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c8
    .line 2110
    if-eqz v4, :cond_cc

    #@ca
    if-nez v7, :cond_cd

    #@cc
    .line 2229
    :cond_cc
    :goto_cc
    return-void

    #@cd
    .line 2114
    :cond_cd
    if-eqz p1, :cond_176

    #@cf
    .line 2115
    sparse-switch v1, :sswitch_data_22a

    #@d2
    .line 2211
    :cond_d2
    :goto_d2
    const-string v8, "[LGE_DATA][LGEDCT] "

    #@d4
    new-instance v9, Ljava/lang/StringBuilder;

    #@d6
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@d9
    const-string v10, "[changePreferrredNetworkMode] newPreferMode : "

    #@db
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v9

    #@df
    iget-object v10, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@e1
    invoke-static {v5}, Lcom/android/internal/telephony/DataConnectionTracker;->networkModeToString(I)Ljava/lang/String;

    #@e4
    move-result-object v10

    #@e5
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v9

    #@e9
    const-string v10, " / curPreferMode : "

    #@eb
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v9

    #@ef
    iget-object v10, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@f1
    invoke-static {v1}, Lcom/android/internal/telephony/DataConnectionTracker;->networkModeToString(I)Ljava/lang/String;

    #@f4
    move-result-object v10

    #@f5
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v9

    #@f9
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fc
    move-result-object v9

    #@fd
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@100
    .line 2214
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@102
    const/4 v8, -0x1

    #@103
    if-eq v5, v8, :cond_cc

    #@105
    if-eq v5, v1, :cond_cc

    #@107
    .line 2215
    const-string v8, "[LGE_DATA][LGEDCT] "

    #@109
    new-instance v9, Ljava/lang/StringBuilder;

    #@10b
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@10e
    const-string v10, "[changePreferrredNetworkMode] change to newPreferMode:"

    #@110
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@113
    move-result-object v9

    #@114
    iget-object v10, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@116
    invoke-static {v5}, Lcom/android/internal/telephony/DataConnectionTracker;->networkModeToString(I)Ljava/lang/String;

    #@119
    move-result-object v10

    #@11a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v9

    #@11e
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@121
    move-result-object v9

    #@122
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@125
    .line 2218
    if-eqz v0, :cond_21a

    #@127
    .line 2220
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@129
    invoke-virtual {p0, v12}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->obtainMessage(I)Landroid/os/Message;

    #@12c
    move-result-object v9

    #@12d
    invoke-virtual {v8, v5, v9}, Lcom/android/internal/telephony/PhoneBase;->setPreferredNetworkType(ILandroid/os/Message;)V

    #@130
    goto :goto_cc

    #@131
    .line 2119
    :sswitch_131
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@133
    iget-object v8, v8, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@135
    invoke-interface {v8}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@138
    move-result-object v8

    #@139
    iget-boolean v8, v8, Lcom/android/internal/telephony/LGfeature;->bypassforuplus:Z

    #@13b
    if-nez v8, :cond_140

    #@13d
    .line 2120
    const/16 v5, 0x9

    #@13f
    .line 2121
    goto :goto_d2

    #@140
    .line 2126
    :cond_140
    :sswitch_140
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@142
    iget-object v8, v8, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@144
    invoke-interface {v8}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@147
    move-result-object v8

    #@148
    iget-boolean v8, v8, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MODE_CHANGE_USER3G_KR:Z

    #@14a
    if-eqz v8, :cond_166

    #@14c
    .line 2128
    const-string v8, "[LGE_DATA][LGEDCT] "

    #@14e
    new-instance v9, Ljava/lang/StringBuilder;

    #@150
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@153
    const-string v10, "[changePreferrredNetworkMode] User_3g_mode_maintain : "

    #@155
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@158
    move-result-object v9

    #@159
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15c
    move-result-object v9

    #@15d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@160
    move-result-object v9

    #@161
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@164
    goto/16 :goto_d2

    #@166
    .line 2136
    :cond_166
    :sswitch_166
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@168
    iget-object v8, v8, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@16a
    invoke-interface {v8}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@16d
    move-result-object v8

    #@16e
    iget-boolean v8, v8, Lcom/android/internal/telephony/LGfeature;->bypassforuplus:Z

    #@170
    if-nez v8, :cond_d2

    #@172
    .line 2137
    const/16 v5, 0xc

    #@174
    .line 2138
    goto/16 :goto_d2

    #@176
    .line 2150
    :cond_176
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@178
    iget-object v8, v8, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@17a
    invoke-interface {v8}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@17d
    move-result-object v8

    #@17e
    iget-boolean v8, v8, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DEREGISTRATION:Z

    #@180
    if-eqz v8, :cond_187

    #@182
    .line 2151
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@184
    invoke-virtual {v8}, Lcom/android/internal/telephony/DataConnectionTracker;->isCompleteIMSforDelayTime()Z

    #@187
    .line 2155
    :cond_187
    sparse-switch v1, :sswitch_data_240

    #@18a
    .line 2201
    :cond_18a
    :goto_18a
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@18c
    iget-object v8, v8, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@18e
    invoke-interface {v8}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@191
    move-result-object v8

    #@192
    iget-boolean v8, v8, Lcom/android/internal/telephony/LGfeature;->bypassforuplus:Z

    #@194
    if-eqz v8, :cond_d2

    #@196
    const/4 v8, 0x6

    #@197
    if-eq v1, v8, :cond_d2

    #@199
    const/4 v8, 0x4

    #@19a
    if-eq v1, v8, :cond_d2

    #@19c
    const/16 v8, 0xa

    #@19e
    if-ne v1, v8, :cond_d2

    #@1a0
    const/16 v8, 0xd

    #@1a2
    if-eq v2, v8, :cond_d2

    #@1a4
    goto/16 :goto_d2

    #@1a6
    .line 2159
    :sswitch_1a6
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1a8
    iget-object v8, v8, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1aa
    invoke-interface {v8}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1ad
    move-result-object v8

    #@1ae
    iget-boolean v8, v8, Lcom/android/internal/telephony/LGfeature;->bypassforuplus:Z

    #@1b0
    if-nez v8, :cond_1c1

    #@1b2
    .line 2160
    const/4 v5, 0x3

    #@1b3
    .line 2163
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1b5
    iget-object v8, v8, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1b7
    invoke-interface {v8}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1ba
    move-result-object v8

    #@1bb
    iget-boolean v8, v8, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_PROFILE_SETTING_SINGLE_PDN:Z

    #@1bd
    if-eqz v8, :cond_18a

    #@1bf
    .line 2164
    const/4 v5, 0x0

    #@1c0
    goto :goto_18a

    #@1c1
    .line 2173
    :cond_1c1
    :sswitch_1c1
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1c3
    iget-object v8, v8, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1c5
    invoke-interface {v8}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1c8
    move-result-object v8

    #@1c9
    iget-boolean v8, v8, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MODE_CHANGE_USER3G_KR:Z

    #@1cb
    if-eqz v8, :cond_1e6

    #@1cd
    .line 2175
    const-string v8, "[LGE_DATA][LGEDCT] "

    #@1cf
    new-instance v9, Ljava/lang/StringBuilder;

    #@1d1
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1d4
    const-string v10, "[changePreferrredNetworkMode] User_3g_mode_maintain : "

    #@1d6
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d9
    move-result-object v9

    #@1da
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1dd
    move-result-object v9

    #@1de
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e1
    move-result-object v9

    #@1e2
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e5
    goto :goto_18a

    #@1e6
    .line 2183
    :cond_1e6
    :sswitch_1e6
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1e8
    iget-object v8, v8, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1ea
    invoke-interface {v8}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1ed
    move-result-object v8

    #@1ee
    iget-boolean v8, v8, Lcom/android/internal/telephony/LGfeature;->bypassforuplus:Z

    #@1f0
    if-nez v8, :cond_1f3

    #@1f2
    .line 2184
    const/4 v5, 0x2

    #@1f3
    .line 2188
    :cond_1f3
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1f5
    iget-object v8, v8, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1f7
    invoke-interface {v8}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1fa
    move-result-object v8

    #@1fb
    iget-boolean v8, v8, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MODECHANGE_KT:Z

    #@1fd
    if-eqz v8, :cond_18a

    #@1ff
    .line 2190
    const/4 v0, 0x1

    #@200
    .line 2191
    const-string v8, "[LGE_DATA][LGEDCT] "

    #@202
    new-instance v9, Ljava/lang/StringBuilder;

    #@204
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@207
    const-string v10, "[changePreferrredNetworkMode] Is_LWmode_selected : "

    #@209
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20c
    move-result-object v9

    #@20d
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@210
    move-result-object v9

    #@211
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@214
    move-result-object v9

    #@215
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@218
    goto/16 :goto_18a

    #@21a
    .line 2225
    :cond_21a
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@21c
    invoke-virtual {p0, v12}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->obtainMessage(I)Landroid/os/Message;

    #@21f
    move-result-object v9

    #@220
    invoke-virtual {v8, v5, v9}, Lcom/android/internal/telephony/PhoneBase;->setPreferredNetworkType(ILandroid/os/Message;)V

    #@223
    .line 2226
    iget-object v8, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@225
    invoke-virtual {v8, v5}, Lcom/android/internal/telephony/DataConnectionTracker;->setPreferredNetworkMode(I)V

    #@228
    goto/16 :goto_cc

    #@22a
    .line 2115
    :sswitch_data_22a
    .sparse-switch
        0x0 -> :sswitch_131
        0x2 -> :sswitch_140
        0x3 -> :sswitch_131
        0x9 -> :sswitch_131
        0xc -> :sswitch_166
    .end sparse-switch

    #@240
    .line 2155
    :sswitch_data_240
    .sparse-switch
        0x0 -> :sswitch_1a6
        0x2 -> :sswitch_1c1
        0x3 -> :sswitch_1a6
        0x9 -> :sswitch_1a6
        0xc -> :sswitch_1e6
    .end sparse-switch
.end method

.method public dispose()V
    .registers 4

    #@0
    .prologue
    .line 2309
    iget-object v0, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@4
    invoke-interface {v0}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@7
    move-result-object v0

    #@8
    iget-object v0, v0, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@a
    const-string v1, "KTBASE"

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    if-nez v0, :cond_3e

    #@12
    iget-object v0, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@14
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@16
    invoke-interface {v0}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@19
    move-result-object v0

    #@1a
    iget-object v0, v0, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@1c
    const-string v1, "SKTBASE"

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v0

    #@22
    if-nez v0, :cond_3e

    #@24
    iget-object v0, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@26
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@28
    invoke-interface {v0}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@2b
    move-result-object v0

    #@2c
    iget-object v0, v0, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@2e
    const-string v1, "LGTBASE"

    #@30
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v0

    #@34
    if-nez v0, :cond_3e

    #@36
    .line 2313
    const-string v0, "[LGE_DATA][LGEDCT] "

    #@38
    const-string v1, "other country do not use this function. so return."

    #@3a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 2348
    :goto_3d
    return-void

    #@3e
    .line 2319
    :cond_3e
    iget-object v0, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@40
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@42
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForAvailable(Landroid/os/Handler;)V

    #@45
    .line 2325
    iget-object v0, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@47
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@49
    invoke-interface {v0}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@4c
    move-result-object v0

    #@4d
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_GPRS_REJECTED_KT:Z

    #@4f
    if-eqz v0, :cond_5f

    #@51
    .line 2326
    iget-boolean v0, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->isGsm:Z

    #@53
    if-eqz v0, :cond_5f

    #@55
    .line 2327
    iget-object v0, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mGsst:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@57
    iget-object v1, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mGsst:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@59
    const/16 v1, 0x3ef

    #@5b
    const/4 v2, 0x0

    #@5c
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setPdpRejectedNotification(II)V

    #@5f
    .line 2332
    :cond_5f
    iget-object v0, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@61
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@64
    move-result-object v0

    #@65
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/ServiceStateTracker;->unregisterForDataConnectionAttached(Landroid/os/Handler;)V

    #@68
    .line 2333
    iget-object v0, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@6a
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@6d
    move-result-object v0

    #@6e
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/ServiceStateTracker;->unregisterForNetworkAttached(Landroid/os/Handler;)V

    #@71
    .line 2335
    iget-object v0, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@73
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@75
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregistorForPacketPaging(Landroid/os/Handler;)V

    #@78
    .line 2336
    iget-object v0, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@7a
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@7c
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForDataNetworkStateChanged(Landroid/os/Handler;)V

    #@7f
    .line 2338
    iget-object v0, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@81
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getCallTracker()Lcom/android/internal/telephony/CallTracker;

    #@84
    move-result-object v0

    #@85
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/CallTracker;->unregisterForVoiceCallEnded(Landroid/os/Handler;)V

    #@88
    .line 2339
    iget-object v0, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@8a
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getCallTracker()Lcom/android/internal/telephony/CallTracker;

    #@8d
    move-result-object v0

    #@8e
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/CallTracker;->unregisterForVoiceCallStarted(Landroid/os/Handler;)V

    #@91
    .line 2341
    iget-object v0, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@93
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/DataConnectionTracker;->unregisterForDataConnectEvent(Landroid/os/Handler;)V

    #@96
    .line 2342
    iget-object v0, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@98
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/uicc/UiccController;->unregisterForIccChanged(Landroid/os/Handler;)V

    #@9b
    .line 2343
    iget-object v0, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@9d
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@a0
    move-result-object v0

    #@a1
    iget-object v1, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@a3
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@a6
    .line 2346
    iget-object v0, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mLTEDataRoamingSettingObserver:Lcom/android/internal/telephony/LGEDataConnectionTracker$LTEDataRoamingSettingObserver;

    #@a8
    iget-object v1, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@aa
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@ad
    move-result-object v1

    #@ae
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/LGEDataConnectionTracker$LTEDataRoamingSettingObserver;->unregister(Landroid/content/Context;)V

    #@b1
    goto :goto_3d
.end method

.method public dprilmsg(Lcom/android/internal/telephony/DataProfile;I)I
    .registers 20
    .parameter "dp"
    .parameter "nodbID"

    #@0
    .prologue
    .line 1874
    move-object/from16 v14, p1

    #@2
    check-cast v14, Lcom/android/internal/telephony/ApnSetting;

    #@4
    .line 1876
    .local v14, apnSetting:Lcom/android/internal/telephony/ApnSetting;
    invoke-virtual/range {p0 .. p1}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->lgDatagetMainApnType(Lcom/android/internal/telephony/DataProfile;)Ljava/lang/String;

    #@7
    move-result-object v15

    #@8
    .line 1877
    .local v15, maintype:Ljava/lang/String;
    move-object/from16 v0, p0

    #@a
    iget-object v1, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@c
    invoke-virtual {v1, v15}, Lcom/android/internal/telephony/DataConnectionTracker;->apnTypeToId(Ljava/lang/String;)I

    #@f
    move-result v13

    #@10
    .line 1878
    .local v13, AFWID:I
    move-object/from16 v0, p0

    #@12
    iget-object v1, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@14
    move-object/from16 v0, p0

    #@16
    iget-object v7, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@18
    iget-object v7, v7, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1a
    invoke-interface {v7}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1d
    move-result-object v7

    #@1e
    iget v7, v7, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@20
    invoke-virtual {v1, v13, v7}, Lcom/android/internal/telephony/DataConnectionTracker;->switchingFixedtype(II)I

    #@23
    move-result v2

    #@24
    .line 1880
    .local v2, pdnId:I
    move-object/from16 v0, p0

    #@26
    iget-object v1, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@28
    const/4 v1, -0x1

    #@29
    if-ne v2, v1, :cond_33

    #@2b
    .line 1881
    const-string v1, "[LGE_DATA][LGEDCT] "

    #@2d
    const-string v7, "APN_INVALID_ID"

    #@2f
    invoke-static {v1, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 1968
    .end local p2
    :goto_32
    return p2

    #@33
    .line 1885
    .restart local p2
    :cond_33
    const/4 v3, 0x0

    #@34
    .line 1887
    .local v3, apnLength:I
    iget-object v4, v14, Lcom/android/internal/telephony/DataProfile;->apn:Ljava/lang/String;

    #@36
    .line 1888
    .local v4, apn:Ljava/lang/String;
    const-string v1, ""

    #@38
    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v1

    #@3c
    if-eqz v1, :cond_46

    #@3e
    .line 1889
    const-string v1, "[LGE_DATA][LGEDCT] "

    #@40
    const-string v7, "apn is null"

    #@42
    invoke-static {v1, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    goto :goto_32

    #@46
    .line 1892
    :cond_46
    const-string v1, "[LGE_DATA][LGEDCT] "

    #@48
    new-instance v7, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v8, "apn is "

    #@4f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v7

    #@53
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v7

    #@57
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v7

    #@5b
    invoke-static {v1, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 1893
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@61
    move-result v3

    #@62
    .line 1896
    const/4 v5, 0x0

    #@63
    .line 1898
    .local v5, ipType:I
    move-object/from16 v0, p0

    #@65
    iget-object v7, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@67
    move-object/from16 v1, p1

    #@69
    check-cast v1, Lcom/android/internal/telephony/ApnSetting;

    #@6b
    invoke-virtual {v7, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->getIpTypeFromDataProfile(Lcom/android/internal/telephony/ApnSetting;)I

    #@6e
    move-result v5

    #@6f
    .line 1900
    move-object/from16 v0, p0

    #@71
    iget-object v1, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@73
    const/4 v1, -0x1

    #@74
    if-ne v5, v1, :cond_7e

    #@76
    .line 1901
    const-string v1, "[LGE_DATA][LGEDCT] "

    #@78
    const-string v7, "IP_VERSION_SUPPORT_TYPE_NOT_AVAILABLE"

    #@7a
    invoke-static {v1, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7d
    goto :goto_32

    #@7e
    .line 1905
    :cond_7e
    const/4 v6, 0x0

    #@7f
    .line 1906
    .local v6, inactivityTime:I
    const/4 v9, 0x0

    #@80
    .line 1908
    .local v9, esminfo:I
    move-object/from16 v0, p0

    #@82
    iget-object v1, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@84
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@86
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@89
    move-result-object v1

    #@8a
    iget-boolean v1, v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_PAYPOPUP_ROAMING_SKT:Z

    #@8c
    if-eqz v1, :cond_b5

    #@8e
    .line 1910
    const-string v1, "[LGE_DATA][LGEDCT] "

    #@90
    new-instance v7, Ljava/lang/StringBuilder;

    #@92
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@95
    const-string v8, "APN ID is  "

    #@97
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v7

    #@9b
    move-object/from16 v0, p1

    #@9d
    iget v8, v0, Lcom/android/internal/telephony/DataProfile;->id:I

    #@9f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v7

    #@a3
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a6
    move-result-object v7

    #@a7
    invoke-static {v1, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@aa
    .line 1911
    move-object/from16 v0, p1

    #@ac
    iget v1, v0, Lcom/android/internal/telephony/DataProfile;->id:I

    #@ae
    move-object/from16 v0, p0

    #@b0
    iget v7, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->APN_ID_FOR_LTE_Roaming:I

    #@b2
    if-le v1, v7, :cond_153

    #@b4
    .line 1913
    const/4 v9, 0x1

    #@b5
    .line 1923
    :cond_b5
    :goto_b5
    const-string v1, "[LGE_DATA][LGEDCT] "

    #@b7
    new-instance v7, Ljava/lang/StringBuilder;

    #@b9
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@bc
    const-string v8, "esminfo  : "

    #@be
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v7

    #@c2
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v7

    #@c6
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c9
    move-result-object v7

    #@ca
    invoke-static {v1, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@cd
    .line 1927
    move-object/from16 v0, p0

    #@cf
    iget-object v1, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@d1
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@d3
    const/4 v7, 0x0

    #@d4
    const/4 v8, 0x0

    #@d5
    invoke-interface {v1, v7, v2, v8}, Lcom/android/internal/telephony/CommandsInterface;->getOrSetDB(ZILjava/lang/String;)Ljava/lang/String;

    #@d8
    move-result-object v1

    #@d9
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/DataProfile;->toHash()Ljava/lang/String;

    #@dc
    move-result-object v7

    #@dd
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e0
    move-result v1

    #@e1
    if-nez v1, :cond_16f

    #@e3
    .line 1929
    move-object/from16 v0, p0

    #@e5
    iget-object v1, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@e7
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@e9
    const/4 v7, 0x1

    #@ea
    iget v8, v14, Lcom/android/internal/telephony/DataProfile;->authType:I

    #@ec
    iget-object v10, v14, Lcom/android/internal/telephony/DataProfile;->user:Ljava/lang/String;

    #@ee
    iget-object v11, v14, Lcom/android/internal/telephony/DataProfile;->password:Ljava/lang/String;

    #@f0
    const/4 v12, 0x0

    #@f1
    invoke-interface/range {v1 .. v12}, Lcom/android/internal/telephony/CommandsInterface;->sendPdnTable(IILjava/lang/String;IIIIILjava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@f4
    .line 1932
    move-object/from16 v0, p0

    #@f6
    iget-object v1, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@f8
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@fa
    const/4 v7, 0x1

    #@fb
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/DataProfile;->toHash()Ljava/lang/String;

    #@fe
    move-result-object v8

    #@ff
    invoke-interface {v1, v7, v2, v8}, Lcom/android/internal/telephony/CommandsInterface;->getOrSetDB(ZILjava/lang/String;)Ljava/lang/String;

    #@102
    .line 1934
    move-object/from16 v0, p0

    #@104
    iget-object v1, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@106
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@108
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@10b
    move-result-object v1

    #@10c
    iget-boolean v1, v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APNSYNC:Z

    #@10e
    if-eqz v1, :cond_14f

    #@110
    .line 1936
    invoke-virtual/range {p0 .. p1}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->lgDatagetMainApnType(Lcom/android/internal/telephony/DataProfile;)Ljava/lang/String;

    #@113
    move-result-object v16

    #@114
    .line 1939
    .local v16, pdntype:Ljava/lang/String;
    move-object/from16 v0, p0

    #@116
    iget-object v1, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@118
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@11a
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@11d
    move-result-object v1

    #@11e
    iget-boolean v1, v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APNSYNC_KR:Z

    #@120
    if-eqz v1, :cond_146

    #@122
    .line 1941
    const-string v1, "[LGE_DATA][LGEDCT] "

    #@124
    new-instance v7, Ljava/lang/StringBuilder;

    #@126
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@129
    const-string v8, "[LGE_DATA_APN] Used the single APN in roaming network : "

    #@12b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12e
    move-result-object v7

    #@12f
    move-object/from16 v0, p0

    #@131
    iget-boolean v8, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->is_single_apn_flow:Z

    #@133
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@136
    move-result-object v7

    #@137
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13a
    move-result-object v7

    #@13b
    invoke-static {v1, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@13e
    .line 1942
    move-object/from16 v0, p0

    #@140
    iget-boolean v1, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->is_single_apn_flow:Z

    #@142
    if-eqz v1, :cond_146

    #@144
    .line 1943
    const-string v16, "default"

    #@146
    .line 1948
    :cond_146
    move-object/from16 v0, p0

    #@148
    iget-object v1, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@14a
    move-object/from16 v0, v16

    #@14c
    invoke-virtual {v1, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->disconnectonlyChangedPDN(Ljava/lang/String;)V

    #@14f
    .end local v16           #pdntype:Ljava/lang/String;
    :cond_14f
    :goto_14f
    move/from16 p2, v2

    #@151
    .line 1968
    goto/16 :goto_32

    #@153
    .line 1916
    :cond_153
    move-object/from16 v0, p0

    #@155
    iget-object v1, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@157
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@159
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@15c
    move-result-object v1

    #@15d
    iget v1, v1, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@15f
    const/4 v7, 0x6

    #@160
    if-ne v1, v7, :cond_b5

    #@162
    move-object/from16 v0, p1

    #@164
    iget v1, v0, Lcom/android/internal/telephony/DataProfile;->id:I

    #@166
    move-object/from16 v0, p0

    #@168
    iget v7, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->APN_ID_FOR_IMS:I

    #@16a
    if-ne v1, v7, :cond_b5

    #@16c
    .line 1918
    const/4 v9, 0x1

    #@16d
    goto/16 :goto_b5

    #@16f
    .line 1956
    :cond_16f
    move-object/from16 v0, p0

    #@171
    iget-object v1, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@173
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@175
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@178
    move-result-object v1

    #@179
    iget-boolean v1, v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_SELECT_KR:Z

    #@17b
    if-eqz v1, :cond_1a3

    #@17d
    .line 1957
    invoke-virtual/range {p0 .. p1}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->lgDatagetMainApnType(Lcom/android/internal/telephony/DataProfile;)Ljava/lang/String;

    #@180
    move-result-object v16

    #@181
    .line 1958
    .restart local v16       #pdntype:Ljava/lang/String;
    move-object/from16 v0, p0

    #@183
    iget-object v1, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@185
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mPreferredApn:Lcom/android/internal/telephony/DataProfile;

    #@187
    if-nez v1, :cond_1a3

    #@189
    const-string v1, "default"

    #@18b
    move-object/from16 v0, v16

    #@18d
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@190
    move-result v1

    #@191
    if-eqz v1, :cond_1a3

    #@193
    .line 1959
    const-string v1, "[LGE_DATA][LGEDCT] "

    #@195
    const-string v7, "[LGE_DATA_APN] disconnect default for set preferredAPN"

    #@197
    invoke-static {v1, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19a
    .line 1960
    move-object/from16 v0, p0

    #@19c
    iget-object v1, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@19e
    move-object/from16 v0, v16

    #@1a0
    invoke-virtual {v1, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->disconnectonlyChangedPDN(Ljava/lang/String;)V

    #@1a3
    .line 1965
    .end local v16           #pdntype:Ljava/lang/String;
    :cond_1a3
    const-string v1, "[LGE_DATA][LGEDCT] "

    #@1a5
    new-instance v7, Ljava/lang/StringBuilder;

    #@1a7
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1aa
    const-string v8, "sending apn is same with old one id= "

    #@1ac
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1af
    move-result-object v7

    #@1b0
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b3
    move-result-object v7

    #@1b4
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b7
    move-result-object v7

    #@1b8
    invoke-static {v1, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1bb
    goto :goto_14f
.end method

.method public getLTEDataRoamingEnable()Z
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2385
    :try_start_1
    iget-object v3, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@3
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@6
    move-result-object v3

    #@7
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a
    move-result-object v0

    #@b
    .line 2386
    .local v0, resolver:Landroid/content/ContentResolver;
    const-string v3, "data_lte_roaming"

    #@d
    invoke-static {v0, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_10
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_10} :catch_15

    #@10
    move-result v3

    #@11
    if-eqz v3, :cond_14

    #@13
    const/4 v2, 0x1

    #@14
    .line 2388
    .end local v0           #resolver:Landroid/content/ContentResolver;
    :cond_14
    :goto_14
    return v2

    #@15
    .line 2387
    :catch_15
    move-exception v1

    #@16
    .line 2388
    .local v1, snfe:Landroid/provider/Settings$SettingNotFoundException;
    goto :goto_14
.end method

.method public getOverallState()Lcom/android/internal/telephony/DctConstants$State;
    .registers 8

    #@0
    .prologue
    .line 1829
    const/4 v3, 0x0

    #@1
    .line 1830
    .local v3, isConnecting:Z
    const/4 v4, 0x1

    #@2
    .line 1831
    .local v4, isFailed:Z
    const/4 v2, 0x0

    #@3
    .line 1833
    .local v2, isAnyEnabled:Z
    iget-object v5, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@5
    iget-object v5, v5, Lcom/android/internal/telephony/DataConnectionTracker;->mApnContexts:Ljava/util/concurrent/ConcurrentHashMap;

    #@7
    invoke-virtual {v5}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    #@a
    move-result-object v5

    #@b
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@e
    move-result-object v1

    #@f
    .local v1, i$:Ljava/util/Iterator;
    :cond_f
    :goto_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@12
    move-result v5

    #@13
    if-eqz v5, :cond_41

    #@15
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@18
    move-result-object v0

    #@19
    check-cast v0, Lcom/android/internal/telephony/ApnContext;

    #@1b
    .line 1834
    .local v0, apnContext:Lcom/android/internal/telephony/ApnContext;
    invoke-virtual {v0}, Lcom/android/internal/telephony/ApnContext;->isEnabled()Z

    #@1e
    move-result v5

    #@1f
    if-eqz v5, :cond_f

    #@21
    .line 1835
    const/4 v2, 0x1

    #@22
    .line 1836
    sget-object v5, Lcom/android/internal/telephony/LGEDataConnectionTracker$2;->$SwitchMap$com$android$internal$telephony$DctConstants$State:[I

    #@24
    invoke-virtual {v0}, Lcom/android/internal/telephony/ApnContext;->getState()Lcom/android/internal/telephony/DctConstants$State;

    #@27
    move-result-object v6

    #@28
    invoke-virtual {v6}, Lcom/android/internal/telephony/DctConstants$State;->ordinal()I

    #@2b
    move-result v6

    #@2c
    aget v5, v5, v6

    #@2e
    packed-switch v5, :pswitch_data_70

    #@31
    goto :goto_f

    #@32
    .line 1839
    :pswitch_32
    const-string v5, "[LGE_DATA][LGEDCT] "

    #@34
    const-string v6, "overall state is CONNECTED"

    #@36
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 1840
    sget-object v5, Lcom/android/internal/telephony/DctConstants$State;->CONNECTED:Lcom/android/internal/telephony/DctConstants$State;

    #@3b
    .line 1867
    .end local v0           #apnContext:Lcom/android/internal/telephony/ApnContext;
    :goto_3b
    return-object v5

    #@3c
    .line 1843
    .restart local v0       #apnContext:Lcom/android/internal/telephony/ApnContext;
    :pswitch_3c
    const/4 v3, 0x1

    #@3d
    .line 1844
    const/4 v4, 0x0

    #@3e
    .line 1845
    goto :goto_f

    #@3f
    .line 1848
    :pswitch_3f
    const/4 v4, 0x0

    #@40
    goto :goto_f

    #@41
    .line 1854
    .end local v0           #apnContext:Lcom/android/internal/telephony/ApnContext;
    :cond_41
    if-nez v2, :cond_4d

    #@43
    .line 1855
    const-string v5, "[LGE_DATA][LGEDCT] "

    #@45
    const-string v6, "overall state is IDLE"

    #@47
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    .line 1856
    sget-object v5, Lcom/android/internal/telephony/DctConstants$State;->IDLE:Lcom/android/internal/telephony/DctConstants$State;

    #@4c
    goto :goto_3b

    #@4d
    .line 1859
    :cond_4d
    if-eqz v3, :cond_59

    #@4f
    .line 1860
    const-string v5, "[LGE_DATA][LGEDCT] "

    #@51
    const-string v6, "overall state is CONNECTING"

    #@53
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 1861
    sget-object v5, Lcom/android/internal/telephony/DctConstants$State;->CONNECTING:Lcom/android/internal/telephony/DctConstants$State;

    #@58
    goto :goto_3b

    #@59
    .line 1862
    :cond_59
    if-nez v4, :cond_65

    #@5b
    .line 1863
    const-string v5, "[LGE_DATA][LGEDCT] "

    #@5d
    const-string v6, "overall state is IDLE"

    #@5f
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    .line 1864
    sget-object v5, Lcom/android/internal/telephony/DctConstants$State;->IDLE:Lcom/android/internal/telephony/DctConstants$State;

    #@64
    goto :goto_3b

    #@65
    .line 1866
    :cond_65
    const-string v5, "[LGE_DATA][LGEDCT] "

    #@67
    const-string v6, "overall state is FAILED"

    #@69
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6c
    .line 1867
    sget-object v5, Lcom/android/internal/telephony/DctConstants$State;->FAILED:Lcom/android/internal/telephony/DctConstants$State;

    #@6e
    goto :goto_3b

    #@6f
    .line 1836
    nop

    #@70
    :pswitch_data_70
    .packed-switch 0x1
        :pswitch_32
        :pswitch_32
        :pswitch_3c
        :pswitch_3c
        :pswitch_3f
        :pswitch_3f
    .end packed-switch
.end method

.method protected getUiccCardApplication()Lcom/android/internal/telephony/uicc/IccRecords;
    .registers 3

    #@0
    .prologue
    .line 1799
    iget-object v0, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/uicc/UiccController;->getIccRecords(I)Lcom/android/internal/telephony/uicc/IccRecords;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public handleCSProtection(Landroid/os/AsyncResult;)V
    .registers 8
    .parameter "ar"

    #@0
    .prologue
    .line 1701
    iget-object v2, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@2
    if-nez v2, :cond_4c

    #@4
    .line 1702
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@6
    const-string v3, "handleCSProtection"

    #@8
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 1704
    iget-object v0, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@d
    check-cast v0, Ljava/lang/String;

    #@f
    .line 1705
    .local v0, ModemInfo:Ljava/lang/String;
    if-eqz v0, :cond_49

    #@11
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@14
    move-result v2

    #@15
    if-lez v2, :cond_49

    #@17
    .line 1706
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@19
    new-instance v3, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v4, "handleCSProtection ModemInfo = "

    #@20
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v3

    #@2c
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 1708
    const-string v2, "1"

    #@31
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@34
    move-result v1

    #@35
    .line 1709
    .local v1, bEnabled:Z
    iget-object v2, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@37
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@39
    if-eqz v2, :cond_49

    #@3b
    .line 1710
    iget-object v2, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@3d
    iget-object v3, v2, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@3f
    sget-object v4, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->setBlockPacketMenuProcess:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@41
    const-string v5, ""

    #@43
    if-eqz v1, :cond_4a

    #@45
    const/4 v2, 0x1

    #@46
    :goto_46
    invoke-virtual {v3, v4, v5, v2}, Lcom/android/internal/telephony/DataConnectionManager;->IntegrationAPI(Lcom/android/internal/telephony/DataConnectionManager$FunctionName;Ljava/lang/String;I)I

    #@49
    .line 1718
    .end local v0           #ModemInfo:Ljava/lang/String;
    .end local v1           #bEnabled:Z
    :cond_49
    :goto_49
    return-void

    #@4a
    .line 1710
    .restart local v0       #ModemInfo:Ljava/lang/String;
    .restart local v1       #bEnabled:Z
    :cond_4a
    const/4 v2, 0x0

    #@4b
    goto :goto_46

    #@4c
    .line 1715
    .end local v0           #ModemInfo:Ljava/lang/String;
    .end local v1           #bEnabled:Z
    :cond_4c
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@4e
    new-instance v3, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v4, "handleCSProtection, exception="

    #@55
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v3

    #@59
    iget-object v4, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@5b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v3

    #@5f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v3

    #@63
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    goto :goto_49
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 85
    .parameter "msg"

    #@0
    .prologue
    .line 793
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "handleMessage msg="

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    move-object/from16 v0, p1

    #@f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v3

    #@17
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 795
    move-object/from16 v0, p1

    #@1c
    iget v2, v0, Landroid/os/Message;->what:I

    #@1e
    sparse-switch v2, :sswitch_data_1362

    #@21
    .line 1694
    :cond_21
    :goto_21
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@23
    new-instance v3, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v4, "Unidentified event msg="

    #@2a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    move-object/from16 v0, p1

    #@30
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v3

    #@38
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 1697
    :cond_3b
    :goto_3b
    return-void

    #@3c
    .line 797
    :sswitch_3c
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@3e
    const-string v3, "EVENT_RADIO_AVAILABLE"

    #@40
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 799
    const-string v2, "upgrade.mpdn.db"

    #@45
    const/4 v3, 0x0

    #@46
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@49
    move-result v2

    #@4a
    if-eqz v2, :cond_74

    #@4c
    move-object/from16 v0, p0

    #@4e
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@50
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@53
    move-result-object v2

    #@54
    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@57
    move-result v2

    #@58
    if-nez v2, :cond_74

    #@5a
    .line 803
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@5c
    const-string v3, "Netowrk mode change from gw to gwl when upgrade APN DB"

    #@5e
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@61
    .line 804
    move-object/from16 v0, p0

    #@63
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@65
    const/16 v3, 0x9

    #@67
    const/4 v4, 0x0

    #@68
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/telephony/PhoneBase;->setPreferredNetworkType(ILandroid/os/Message;)V

    #@6b
    .line 805
    move-object/from16 v0, p0

    #@6d
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@6f
    const/16 v3, 0x9

    #@71
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/DataConnectionTracker;->setPreferredNetworkMode(I)V

    #@74
    .line 807
    :cond_74
    const-string v2, "upgrade.mpdn.db"

    #@76
    const-string v3, "false"

    #@78
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@7b
    .line 810
    move-object/from16 v0, p0

    #@7d
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@7f
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@81
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@84
    move-result-object v2

    #@85
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_VOICE_PROTECTION_KR:Z

    #@87
    const/4 v3, 0x1

    #@88
    if-ne v2, v3, :cond_3b

    #@8a
    .line 812
    const-string v2, "net.Is_phone_booted"

    #@8c
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@8f
    move-result-object v2

    #@90
    const-string v3, "true"

    #@92
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@95
    move-result v67

    #@96
    .line 814
    .local v67, mbooting_phone:Z
    if-eqz v67, :cond_3b

    #@98
    .line 815
    const-string v2, "ro.product.model"

    #@9a
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@9d
    move-result-object v68

    #@9e
    .line 817
    .local v68, model:Ljava/lang/String;
    const-string v2, "LG-F160S"

    #@a0
    move-object/from16 v0, v68

    #@a2
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a5
    move-result v2

    #@a6
    if-nez v2, :cond_bc

    #@a8
    const-string v2, "LG-F180S"

    #@aa
    move-object/from16 v0, v68

    #@ac
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@af
    move-result v2

    #@b0
    if-nez v2, :cond_bc

    #@b2
    const-string v2, "LG-F200S"

    #@b4
    move-object/from16 v0, v68

    #@b6
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b9
    move-result v2

    #@ba
    if-eqz v2, :cond_d8

    #@bc
    .line 822
    :cond_bc
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@be
    new-instance v3, Ljava/lang/StringBuilder;

    #@c0
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@c3
    const-string v4, "[LGE_DATA] NV model = "

    #@c5
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v3

    #@c9
    move-object/from16 v0, v68

    #@cb
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v3

    #@cf
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d2
    move-result-object v3

    #@d3
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d6
    goto/16 :goto_3b

    #@d8
    .line 825
    :cond_d8
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@da
    new-instance v3, Ljava/lang/StringBuilder;

    #@dc
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@df
    const-string v4, "[LGE_DATA] Non NV model = "

    #@e1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v3

    #@e5
    move-object/from16 v0, v68

    #@e7
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v3

    #@eb
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ee
    move-result-object v3

    #@ef
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f2
    .line 826
    move-object/from16 v0, p0

    #@f4
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@f6
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@f9
    move-result-object v66

    #@fa
    .line 827
    .local v66, mContext:Landroid/content/Context;
    invoke-virtual/range {v66 .. v66}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@fd
    move-result-object v2

    #@fe
    const-string v3, "multi_rab_setting"

    #@100
    const/4 v4, 0x1

    #@101
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@104
    move-result v2

    #@105
    const/4 v3, 0x1

    #@106
    if-ne v2, v3, :cond_13e

    #@108
    const/16 v41, 0x1

    #@10a
    .line 828
    .local v41, bEnabled:Z
    :goto_10a
    move-object/from16 v0, p0

    #@10c
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@10e
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@110
    if-eqz v2, :cond_143

    #@112
    .line 829
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@114
    new-instance v3, Ljava/lang/StringBuilder;

    #@116
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@119
    const-string v4, "[LGE_DATA] bEnabled = "

    #@11b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11e
    move-result-object v3

    #@11f
    move/from16 v0, v41

    #@121
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@124
    move-result-object v3

    #@125
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@128
    move-result-object v3

    #@129
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12c
    .line 830
    move-object/from16 v0, p0

    #@12e
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@130
    iget-object v3, v2, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@132
    sget-object v4, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->setBlockPacketMenuProcess:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@134
    const-string v7, ""

    #@136
    if-eqz v41, :cond_141

    #@138
    const/4 v2, 0x1

    #@139
    :goto_139
    invoke-virtual {v3, v4, v7, v2}, Lcom/android/internal/telephony/DataConnectionManager;->IntegrationAPI(Lcom/android/internal/telephony/DataConnectionManager$FunctionName;Ljava/lang/String;I)I

    #@13c
    goto/16 :goto_3b

    #@13e
    .line 827
    .end local v41           #bEnabled:Z
    :cond_13e
    const/16 v41, 0x0

    #@140
    goto :goto_10a

    #@141
    .line 830
    .restart local v41       #bEnabled:Z
    :cond_141
    const/4 v2, 0x0

    #@142
    goto :goto_139

    #@143
    .line 833
    :cond_143
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@145
    const-string v3, "[LGE_DATA]dataMgr Null"

    #@147
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@14a
    goto/16 :goto_3b

    #@14c
    .line 842
    .end local v41           #bEnabled:Z
    .end local v66           #mContext:Landroid/content/Context;
    .end local v67           #mbooting_phone:Z
    .end local v68           #model:Ljava/lang/String;
    :sswitch_14c
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@14e
    const-string v3, "onDataConnectionAttached"

    #@150
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@153
    .line 846
    move-object/from16 v0, p0

    #@155
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@157
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@159
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@15c
    move-result-object v2

    #@15d
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_SELECT_KR:Z

    #@15f
    if-eqz v2, :cond_3b

    #@161
    .line 848
    move-object/from16 v0, p0

    #@163
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@165
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->apnSelectionHdlr:Lcom/android/internal/telephony/ApnSelectionHandler;

    #@167
    move-object/from16 v0, p0

    #@169
    iget-object v3, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@16b
    iget-object v3, v3, Lcom/android/internal/telephony/DataConnectionTracker;->apnSelectionHdlr:Lcom/android/internal/telephony/ApnSelectionHandler;

    #@16d
    const-string v3, "Select_default_APN_between_domestic_and_roaming"

    #@16f
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/ApnSelectionHandler;->selectApn(Ljava/lang/String;)V

    #@172
    goto/16 :goto_3b

    #@174
    .line 855
    :sswitch_174
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@176
    const-string v3, "EVENT_RADIO_REGISTERED_TO_NETWORK"

    #@178
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17b
    .line 857
    move-object/from16 v0, p0

    #@17d
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@17f
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@181
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@184
    move-result-object v2

    #@185
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_SELECT_KR:Z

    #@187
    if-eqz v2, :cond_3b

    #@189
    .line 859
    move-object/from16 v0, p0

    #@18b
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@18d
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->apnSelectionHdlr:Lcom/android/internal/telephony/ApnSelectionHandler;

    #@18f
    move-object/from16 v0, p0

    #@191
    iget-object v3, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@193
    iget-object v3, v3, Lcom/android/internal/telephony/DataConnectionTracker;->apnSelectionHdlr:Lcom/android/internal/telephony/ApnSelectionHandler;

    #@195
    const-string v3, "Select_default_APN_between_domestic_and_roaming"

    #@197
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/ApnSelectionHandler;->selectApn(Ljava/lang/String;)V

    #@19a
    goto/16 :goto_3b

    #@19c
    .line 865
    :sswitch_19c
    move-object/from16 v0, p0

    #@19e
    iget-object v3, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@1a0
    move-object/from16 v0, p1

    #@1a2
    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1a4
    check-cast v2, Landroid/os/AsyncResult;

    #@1a6
    invoke-virtual {v3, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->onLockStateChanged(Landroid/os/AsyncResult;)V

    #@1a9
    goto/16 :goto_3b

    #@1ab
    .line 871
    :sswitch_1ab
    move-object/from16 v0, p0

    #@1ad
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1af
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@1b2
    move-result-object v66

    #@1b3
    .line 873
    .restart local v66       #mContext:Landroid/content/Context;
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@1b5
    const-string v3, "EVENT_PACKET_PAGING_RECEIVED"

    #@1b7
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1ba
    .line 874
    move-object/from16 v0, p0

    #@1bc
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1be
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1c0
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1c3
    move-result-object v2

    #@1c4
    iget-object v2, v2, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@1c6
    const-string v3, "KTBASE"

    #@1c8
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1cb
    move-result v2

    #@1cc
    if-eqz v2, :cond_1ed

    #@1ce
    .line 875
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@1d0
    const-string v3, "mFDTimeoutMaxCount 10sec"

    #@1d2
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d5
    .line 876
    new-instance v56, Landroid/content/Intent;

    #@1d7
    const-string v2, "com.lge.ACTION_FD_TRIGGER_TIME_VAL_CHANGED"

    #@1d9
    move-object/from16 v0, v56

    #@1db
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1de
    .line 877
    .local v56, intent:Landroid/content/Intent;
    const-string v2, "value"

    #@1e0
    const/4 v3, 0x1

    #@1e1
    move-object/from16 v0, v56

    #@1e3
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@1e6
    .line 878
    move-object/from16 v0, v66

    #@1e8
    move-object/from16 v1, v56

    #@1ea
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    #@1ed
    .line 881
    .end local v56           #intent:Landroid/content/Intent;
    :cond_1ed
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@1ef
    const-string v3, "Packet Paing Drop"

    #@1f1
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f4
    .line 882
    move-object/from16 v0, p0

    #@1f6
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@1f8
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@1fa
    const/4 v3, 0x0

    #@1fb
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/DataConnectionManager;->functionForPacketDrop(Z)V

    #@1fe
    goto/16 :goto_3b

    #@200
    .line 888
    .end local v66           #mContext:Landroid/content/Context;
    :sswitch_200
    move-object/from16 v0, p1

    #@202
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@204
    move-object/from16 v40, v0

    #@206
    check-cast v40, Landroid/os/AsyncResult;

    #@208
    .line 890
    .local v40, ar:Landroid/os/AsyncResult;
    move-object/from16 v0, p0

    #@20a
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mConnMgr:Landroid/net/ConnectivityManager;

    #@20c
    const/4 v3, 0x0

    #@20d
    invoke-virtual {v2, v3}, Landroid/net/ConnectivityManager;->getLinkProperties(I)Landroid/net/LinkProperties;

    #@210
    move-result-object v74

    #@211
    .line 892
    .local v74, p:Landroid/net/LinkProperties;
    move-object/from16 v0, v40

    #@213
    iget-object v2, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@215
    check-cast v2, Ljava/util/ArrayList;

    #@217
    move-object/from16 v47, v2

    #@219
    check-cast v47, Ljava/util/ArrayList;

    #@21b
    .line 894
    .local v47, dataCallStates:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/DataCallState;>;"
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@21d
    new-instance v3, Ljava/lang/StringBuilder;

    #@21f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@222
    const-string v4, "EVENT_DATA_STATE_CHANGED = "

    #@224
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@227
    move-result-object v3

    #@228
    move-object/from16 v0, v47

    #@22a
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22d
    move-result-object v3

    #@22e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@231
    move-result-object v3

    #@232
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@235
    .line 896
    move-object/from16 v0, v40

    #@237
    iget-object v2, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@239
    if-eqz v2, :cond_244

    #@23b
    .line 898
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@23d
    const-string v3, "onDataStateChanged(ar): exception; likely radio not available, ignore"

    #@23f
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@242
    goto/16 :goto_3b

    #@244
    .line 901
    :cond_244
    move-object/from16 v0, p0

    #@246
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@248
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->mApnContexts:Ljava/util/concurrent/ConcurrentHashMap;

    #@24a
    const-string v3, "default"

    #@24c
    invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@24f
    move-result-object v31

    #@250
    check-cast v31, Lcom/android/internal/telephony/ApnContext;

    #@252
    .line 904
    .local v31, apnContext:Lcom/android/internal/telephony/ApnContext;
    if-eqz v31, :cond_263

    #@254
    .line 905
    invoke-virtual/range {v31 .. v31}, Lcom/android/internal/telephony/ApnContext;->getState()Lcom/android/internal/telephony/DctConstants$State;

    #@257
    move-result-object v2

    #@258
    sget-object v3, Lcom/android/internal/telephony/DctConstants$State;->CONNECTED:Lcom/android/internal/telephony/DctConstants$State;

    #@25a
    if-ne v2, v3, :cond_263

    #@25c
    .line 906
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@25e
    const-string v3, "[LGE_DATA_STATE]Default is connected"

    #@260
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@263
    .line 911
    :cond_263
    const/16 v60, 0x0

    #@265
    .line 912
    .local v60, isAnyDataCallDormant:Z
    const/16 v59, 0x0

    #@267
    .line 915
    .local v59, isAnyDataCallActive:Z
    if-eqz v74, :cond_2cc

    #@269
    if-eqz v47, :cond_2cc

    #@26b
    .line 917
    invoke-virtual/range {v47 .. v47}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@26e
    move-result-object v54

    #@26f
    .local v54, i$:Ljava/util/Iterator;
    :cond_26f
    :goto_26f
    invoke-interface/range {v54 .. v54}, Ljava/util/Iterator;->hasNext()Z

    #@272
    move-result v2

    #@273
    if-eqz v2, :cond_2cc

    #@275
    invoke-interface/range {v54 .. v54}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@278
    move-result-object v70

    #@279
    check-cast v70, Lcom/android/internal/telephony/DataCallState;

    #@27b
    .line 918
    .local v70, newState:Lcom/android/internal/telephony/DataCallState;
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@27d
    new-instance v3, Ljava/lang/StringBuilder;

    #@27f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@282
    const-string v4, "newState = "

    #@284
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@287
    move-result-object v3

    #@288
    move-object/from16 v0, v70

    #@28a
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28d
    move-result-object v3

    #@28e
    const-string v4, "iface = "

    #@290
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@293
    move-result-object v3

    #@294
    invoke-virtual/range {v74 .. v74}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@297
    move-result-object v4

    #@298
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29b
    move-result-object v3

    #@29c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29f
    move-result-object v3

    #@2a0
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a3
    .line 919
    move-object/from16 v0, v70

    #@2a5
    iget-object v2, v0, Lcom/android/internal/telephony/DataCallState;->ifname:Ljava/lang/String;

    #@2a7
    invoke-virtual/range {v74 .. v74}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@2aa
    move-result-object v3

    #@2ab
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2ae
    move-result v2

    #@2af
    if-eqz v2, :cond_26f

    #@2b1
    .line 920
    move-object/from16 v0, v70

    #@2b3
    iget v2, v0, Lcom/android/internal/telephony/DataCallState;->active:I

    #@2b5
    move-object/from16 v0, p0

    #@2b7
    iget-object v3, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2b9
    const/4 v3, 0x2

    #@2ba
    if-ne v2, v3, :cond_2be

    #@2bc
    const/16 v59, 0x1

    #@2be
    .line 921
    :cond_2be
    move-object/from16 v0, v70

    #@2c0
    iget v2, v0, Lcom/android/internal/telephony/DataCallState;->active:I

    #@2c2
    move-object/from16 v0, p0

    #@2c4
    iget-object v3, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2c6
    const/4 v3, 0x1

    #@2c7
    if-ne v2, v3, :cond_26f

    #@2c9
    const/16 v60, 0x1

    #@2cb
    goto :goto_26f

    #@2cc
    .line 927
    .end local v54           #i$:Ljava/util/Iterator;
    .end local v70           #newState:Lcom/android/internal/telephony/DataCallState;
    :cond_2cc
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@2ce
    new-instance v3, Ljava/lang/StringBuilder;

    #@2d0
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2d3
    const-string v4, "isAnyDataCallActive = "

    #@2d5
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d8
    move-result-object v3

    #@2d9
    move/from16 v0, v59

    #@2db
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2de
    move-result-object v3

    #@2df
    const-string v4, "  isAnyDataCallDormant = "

    #@2e1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e4
    move-result-object v3

    #@2e5
    move/from16 v0, v60

    #@2e7
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2ea
    move-result-object v3

    #@2eb
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2ee
    move-result-object v3

    #@2ef
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f2
    .line 929
    if-eqz v60, :cond_3b

    #@2f4
    if-nez v59, :cond_3b

    #@2f6
    .line 931
    move-object/from16 v0, p0

    #@2f8
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2fa
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2fc
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@2ff
    move-result-object v2

    #@300
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_VOICE_PROTECTION_KR:Z

    #@302
    const/4 v3, 0x1

    #@303
    if-ne v2, v3, :cond_3b

    #@305
    .line 934
    move-object/from16 v0, p0

    #@307
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@309
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@30c
    move-result-object v2

    #@30d
    const-string v3, "activity"

    #@30f
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@312
    move-result-object v30

    #@313
    check-cast v30, Landroid/app/ActivityManager;

    #@315
    .line 936
    .local v30, am:Landroid/app/ActivityManager;
    const/4 v2, 0x1

    #@316
    move-object/from16 v0, v30

    #@318
    invoke-virtual {v0, v2}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    #@31b
    move-result-object v78

    #@31c
    .line 937
    .local v78, taskInfo:Ljava/util/List;,"Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-eqz v78, :cond_3b

    #@31e
    .line 940
    const/4 v2, 0x0

    #@31f
    move-object/from16 v0, v78

    #@321
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@324
    move-result-object v2

    #@325
    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    #@327
    iget-object v0, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    #@329
    move-object/from16 v81, v0

    #@32b
    .line 941
    .local v81, topActivity:Landroid/content/ComponentName;
    invoke-virtual/range {v81 .. v81}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@32e
    move-result-object v69

    #@32f
    .line 942
    .local v69, name:Ljava/lang/String;
    invoke-virtual/range {v81 .. v81}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@332
    move-result-object v82

    #@333
    .line 943
    .local v82, topclassname:Ljava/lang/String;
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@335
    new-instance v3, Ljava/lang/StringBuilder;

    #@337
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@33a
    const-string v4, "topActivity.getPackageName(); = "

    #@33c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33f
    move-result-object v3

    #@340
    move-object/from16 v0, v69

    #@342
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@345
    move-result-object v3

    #@346
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@349
    move-result-object v3

    #@34a
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@34d
    .line 944
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@34f
    new-instance v3, Ljava/lang/StringBuilder;

    #@351
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@354
    const-string v4, "topActivity.getClassName(); = "

    #@356
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@359
    move-result-object v3

    #@35a
    move-object/from16 v0, v82

    #@35c
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35f
    move-result-object v3

    #@360
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@363
    move-result-object v3

    #@364
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@367
    .line 945
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@369
    new-instance v3, Ljava/lang/StringBuilder;

    #@36b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@36e
    const-string v4, "VOICE CALL\t::"

    #@370
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@373
    move-result-object v3

    #@374
    sget-boolean v4, Lcom/android/internal/telephony/LGEDataConnectionTracker;->voice_call_ing:Z

    #@376
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@379
    move-result-object v3

    #@37a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37d
    move-result-object v3

    #@37e
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@381
    .line 947
    const-string v2, "com.android.phone.InCallScreen"

    #@383
    move-object/from16 v0, v82

    #@385
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@388
    move-result v2

    #@389
    if-eqz v2, :cond_3b

    #@38b
    sget-boolean v2, Lcom/android/internal/telephony/LGEDataConnectionTracker;->voice_call_ing:Z

    #@38d
    if-eqz v2, :cond_3b

    #@38f
    .line 949
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@391
    new-instance v3, Ljava/lang/StringBuilder;

    #@393
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@396
    const-string v4, "(InCallScreen) topActivity.getClassName(); = "

    #@398
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39b
    move-result-object v3

    #@39c
    move-object/from16 v0, v82

    #@39e
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a1
    move-result-object v3

    #@3a2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a5
    move-result-object v3

    #@3a6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a9
    .line 951
    move-object/from16 v0, p0

    #@3ab
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@3ad
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@3af
    sget-object v3, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->getAlreadyAppUsedPacket:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@3b1
    const-string v4, ""

    #@3b3
    const/4 v7, 0x0

    #@3b4
    invoke-virtual {v2, v3, v4, v7}, Lcom/android/internal/telephony/DataConnectionManager;->IntegrationAPI(Lcom/android/internal/telephony/DataConnectionManager$FunctionName;Ljava/lang/String;I)I

    #@3b7
    move-result v2

    #@3b8
    if-nez v2, :cond_3df

    #@3ba
    .line 953
    move-object/from16 v0, p0

    #@3bc
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@3be
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@3c0
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@3c3
    move-result-object v2

    #@3c4
    iget-object v2, v2, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@3c6
    const-string v3, "SKTBASE"

    #@3c8
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3cb
    move-result v2

    #@3cc
    if-eqz v2, :cond_413

    #@3ce
    .line 955
    move-object/from16 v0, p0

    #@3d0
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@3d2
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@3d4
    const/4 v3, 0x1

    #@3d5
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/DataConnectionManager;->functionForPacketDrop(Z)V

    #@3d8
    .line 956
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@3da
    const-string v3, "functionForPacketDrop is called"

    #@3dc
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3df
    .line 975
    :cond_3df
    :goto_3df
    move-object/from16 v0, p0

    #@3e1
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@3e3
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@3e5
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@3e8
    move-result-object v2

    #@3e9
    iget-object v2, v2, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@3eb
    const-string v3, "KTBASE"

    #@3ed
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3f0
    move-result v2

    #@3f1
    if-eqz v2, :cond_3b

    #@3f3
    .line 976
    new-instance v56, Landroid/content/Intent;

    #@3f5
    const-string v2, "com.lge.ACTION_FD_TRIGGER_TIME_VAL_CHANGED"

    #@3f7
    move-object/from16 v0, v56

    #@3f9
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3fc
    .line 977
    .restart local v56       #intent:Landroid/content/Intent;
    const-string v2, "value"

    #@3fe
    const/4 v3, 0x0

    #@3ff
    move-object/from16 v0, v56

    #@401
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@404
    .line 978
    move-object/from16 v0, p0

    #@406
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@408
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@40b
    move-result-object v2

    #@40c
    move-object/from16 v0, v56

    #@40e
    invoke-virtual {v2, v0}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    #@411
    goto/16 :goto_3b

    #@413
    .line 958
    .end local v56           #intent:Landroid/content/Intent;
    :cond_413
    move-object/from16 v0, p0

    #@415
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@417
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@419
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@41c
    move-result-object v2

    #@41d
    iget-object v2, v2, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@41f
    const-string v3, "KTBASE"

    #@421
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@424
    move-result v2

    #@425
    if-eqz v2, :cond_3df

    #@427
    .line 961
    move-object/from16 v0, p0

    #@429
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mConnMgr:Landroid/net/ConnectivityManager;

    #@42b
    const/4 v3, 0x1

    #@42c
    invoke-virtual {v2, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@42f
    move-result-object v55

    #@430
    .line 963
    .local v55, info:Landroid/net/NetworkInfo;
    if-eqz v55, :cond_44a

    #@432
    invoke-virtual/range {v55 .. v55}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    #@435
    move-result v2

    #@436
    if-nez v2, :cond_44a

    #@438
    .line 965
    move-object/from16 v0, p0

    #@43a
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@43c
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@43e
    const/4 v3, 0x1

    #@43f
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/DataConnectionManager;->functionForPacketDrop(Z)V

    #@442
    .line 966
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@444
    const-string v3, "functionForPacketDrop is called"

    #@446
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@449
    goto :goto_3df

    #@44a
    .line 970
    :cond_44a
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@44c
    const-string v3, "functionForPacketDrop is not called (WIFI connected)"

    #@44e
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@451
    goto :goto_3df

    #@452
    .line 987
    .end local v30           #am:Landroid/app/ActivityManager;
    .end local v31           #apnContext:Lcom/android/internal/telephony/ApnContext;
    .end local v40           #ar:Landroid/os/AsyncResult;
    .end local v47           #dataCallStates:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/DataCallState;>;"
    .end local v55           #info:Landroid/net/NetworkInfo;
    .end local v59           #isAnyDataCallActive:Z
    .end local v60           #isAnyDataCallDormant:Z
    .end local v69           #name:Ljava/lang/String;
    .end local v74           #p:Landroid/net/LinkProperties;
    .end local v78           #taskInfo:Ljava/util/List;,"Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .end local v81           #topActivity:Landroid/content/ComponentName;
    .end local v82           #topclassname:Ljava/lang/String;
    :sswitch_452
    const/4 v2, 0x1

    #@453
    sput-boolean v2, Lcom/android/internal/telephony/LGEDataConnectionTracker;->voice_call_ing:Z

    #@455
    goto/16 :goto_3b

    #@457
    .line 993
    :sswitch_457
    const/4 v2, 0x0

    #@458
    sput-boolean v2, Lcom/android/internal/telephony/LGEDataConnectionTracker;->voice_call_ing:Z

    #@45a
    .line 995
    move-object/from16 v0, p0

    #@45c
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@45e
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@460
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@463
    move-result-object v2

    #@464
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_VOICE_PROTECTION_KR:Z

    #@466
    const/4 v3, 0x1

    #@467
    if-ne v2, v3, :cond_3b

    #@469
    .line 997
    move-object/from16 v0, p0

    #@46b
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@46d
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@470
    move-result-object v2

    #@471
    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getState()I

    #@474
    move-result v2

    #@475
    const/4 v3, 0x1

    #@476
    if-ne v2, v3, :cond_48b

    #@478
    .line 999
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@47a
    const-string v3, "[LGE_DATA] functionForResetDrop for STATE_OUT_OF_SERVICE"

    #@47c
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@47f
    .line 1000
    move-object/from16 v0, p0

    #@481
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@483
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@485
    const/4 v3, 0x0

    #@486
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/DataConnectionManager;->functionForPacketDrop(Z)V

    #@489
    goto/16 :goto_3b

    #@48b
    .line 1004
    :cond_48b
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@48d
    const-string v3, "[LGE_DATA] functionForPacketDrop for ACTION_VOICE_CALL_ENDED"

    #@48f
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@492
    .line 1005
    move-object/from16 v0, p0

    #@494
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@496
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@498
    const/4 v3, 0x0

    #@499
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/DataConnectionManager;->functionForPacketDrop(Z)V

    #@49c
    goto/16 :goto_3b

    #@49e
    .line 1014
    :sswitch_49e
    move-object/from16 v0, p0

    #@4a0
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@4a2
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@4a4
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@4a7
    move-result-object v2

    #@4a8
    iget-object v2, v2, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@4aa
    const-string v3, "KTBASE"

    #@4ac
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4af
    move-result v2

    #@4b0
    if-eqz v2, :cond_3b

    #@4b2
    .line 1015
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@4b4
    const-string v3, "############### calling goFastDormancy"

    #@4b6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4b9
    .line 1016
    move-object/from16 v0, p0

    #@4bb
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@4bd
    if-eqz v2, :cond_4d6

    #@4bf
    .line 1019
    const/16 v2, 0x10

    #@4c1
    new-array v0, v2, [B

    #@4c3
    move-object/from16 v45, v0

    #@4c5
    fill-array-data v45, :array_13ac

    #@4c8
    .line 1034
    .local v45, cmd:[B
    move-object/from16 v0, p0

    #@4ca
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@4cc
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@4ce
    const/4 v3, 0x0

    #@4cf
    move-object/from16 v0, v45

    #@4d1
    invoke-interface {v2, v0, v3}, Lcom/android/internal/telephony/CommandsInterface;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    #@4d4
    goto/16 :goto_3b

    #@4d6
    .line 1036
    .end local v45           #cmd:[B
    :cond_4d6
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@4d8
    const-string v3, "############### don\'t calling goFastDormancy"

    #@4da
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4dd
    goto/16 :goto_3b

    #@4df
    .line 1045
    :sswitch_4df
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@4e1
    new-instance v3, Ljava/lang/StringBuilder;

    #@4e3
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4e6
    const-string v4, "EVENT_SETDEFAULT_TOCHANGE_AFTER_DELAY complete : "

    #@4e8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4eb
    move-result-object v3

    #@4ec
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->getOverallState()Lcom/android/internal/telephony/DctConstants$State;

    #@4ef
    move-result-object v4

    #@4f0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4f3
    move-result-object v3

    #@4f4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f7
    move-result-object v3

    #@4f8
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4fb
    .line 1046
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->getOverallState()Lcom/android/internal/telephony/DctConstants$State;

    #@4fe
    move-result-object v2

    #@4ff
    sget-object v3, Lcom/android/internal/telephony/DctConstants$State;->IDLE:Lcom/android/internal/telephony/DctConstants$State;

    #@501
    if-ne v2, v3, :cond_3b

    #@503
    .line 1048
    move-object/from16 v0, p0

    #@505
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@507
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->apnSelectionHdlr:Lcom/android/internal/telephony/ApnSelectionHandler;

    #@509
    move-object/from16 v0, p0

    #@50b
    iget-object v3, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@50d
    iget-object v3, v3, Lcom/android/internal/telephony/DataConnectionTracker;->apnSelectionHdlr:Lcom/android/internal/telephony/ApnSelectionHandler;

    #@50f
    const-string v3, "Added_APN_failed"

    #@511
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/ApnSelectionHandler;->selectApn(Ljava/lang/String;)V

    #@514
    goto/16 :goto_3b

    #@516
    .line 1054
    :sswitch_516
    move-object/from16 v0, p0

    #@518
    iget-object v3, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@51a
    move-object/from16 v0, p1

    #@51c
    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@51e
    check-cast v2, Lcom/android/internal/telephony/ApnContext;

    #@520
    invoke-virtual {v3, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->onTrySetupData(Lcom/android/internal/telephony/ApnContext;)Z

    #@523
    goto/16 :goto_3b

    #@525
    .line 1059
    :sswitch_525
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@527
    const-string v3, "EVENT_DATA_ERROR_FAIL_CAUSE"

    #@529
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@52c
    .line 1060
    move-object/from16 v0, p1

    #@52e
    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@530
    check-cast v2, Landroid/os/AsyncResult;

    #@532
    move-object/from16 v0, p0

    #@534
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->pdpreject_causecode(Landroid/os/AsyncResult;)V

    #@537
    goto/16 :goto_3b

    #@539
    .line 1065
    :sswitch_539
    move-object/from16 v0, p1

    #@53b
    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@53d
    check-cast v2, Landroid/os/AsyncResult;

    #@53f
    move-object/from16 v0, p0

    #@541
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->handleCSProtection(Landroid/os/AsyncResult;)V

    #@544
    goto/16 :goto_3b

    #@546
    .line 1069
    :sswitch_546
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@548
    const-string v3, "EVENT_ICC_CHANGED"

    #@54a
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@54d
    .line 1070
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->updateIccAvailability()V

    #@550
    goto/16 :goto_3b

    #@552
    .line 1074
    :sswitch_552
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@554
    const-string v3, "EVENT_RECORDS_LOADED"

    #@556
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@559
    .line 1076
    move-object/from16 v0, p0

    #@55b
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@55d
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@55f
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@562
    move-result-object v2

    #@563
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_SELECT_KR:Z

    #@565
    if-eqz v2, :cond_65c

    #@567
    .line 1080
    move-object/from16 v0, p0

    #@569
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@56b
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->apnSelectionHdlr:Lcom/android/internal/telephony/ApnSelectionHandler;

    #@56d
    const/4 v3, 0x0

    #@56e
    iput v3, v2, Lcom/android/internal/telephony/ApnSelectionHandler;->Domestic_APN_ID:I

    #@570
    .line 1081
    move-object/from16 v0, p0

    #@572
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@574
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->apnSelectionHdlr:Lcom/android/internal/telephony/ApnSelectionHandler;

    #@576
    const/4 v3, 0x0

    #@577
    iput v3, v2, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_3G_APN_ID:I

    #@579
    .line 1082
    move-object/from16 v0, p0

    #@57b
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@57d
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->apnSelectionHdlr:Lcom/android/internal/telephony/ApnSelectionHandler;

    #@57f
    const/4 v3, 0x0

    #@580
    iput v3, v2, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_LTE_APN_ID:I

    #@582
    .line 1084
    move-object/from16 v0, p0

    #@584
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@586
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@589
    move-result-object v75

    #@58a
    check-cast v75, Lcom/android/internal/telephony/uicc/IccRecords;

    #@58c
    .line 1085
    .local v75, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v75, :cond_676

    #@58e
    invoke-virtual/range {v75 .. v75}, Lcom/android/internal/telephony/uicc/IccRecords;->getOperatorNumeric()Ljava/lang/String;

    #@591
    move-result-object v73

    #@592
    .line 1086
    .local v73, operator:Ljava/lang/String;
    :goto_592
    if-eqz v73, :cond_64b

    #@594
    .line 1087
    new-instance v2, Ljava/lang/StringBuilder;

    #@596
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@599
    const-string v3, "numeric = \'"

    #@59b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59e
    move-result-object v2

    #@59f
    move-object/from16 v0, v73

    #@5a1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a4
    move-result-object v2

    #@5a5
    const-string v3, "\'"

    #@5a7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5aa
    move-result-object v2

    #@5ab
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5ae
    move-result-object v5

    #@5af
    .line 1090
    .local v5, selection:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    #@5b1
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5b4
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b7
    move-result-object v2

    #@5b8
    const-string v3, " and carrier_enabled = 1"

    #@5ba
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5bd
    move-result-object v2

    #@5be
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c1
    move-result-object v5

    #@5c2
    .line 1092
    move-object/from16 v0, p0

    #@5c4
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@5c6
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@5c9
    move-result-object v2

    #@5ca
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5cd
    move-result-object v2

    #@5ce
    sget-object v3, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    #@5d0
    const/4 v4, 0x0

    #@5d1
    const/4 v6, 0x0

    #@5d2
    const/4 v7, 0x0

    #@5d3
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@5d6
    move-result-object v46

    #@5d7
    .line 1095
    .local v46, cursor:Landroid/database/Cursor;
    if-eqz v46, :cond_64b

    #@5d9
    .line 1096
    invoke-interface/range {v46 .. v46}, Landroid/database/Cursor;->getCount()I

    #@5dc
    move-result v2

    #@5dd
    if-lez v2, :cond_64b

    #@5df
    .line 1097
    invoke-interface/range {v46 .. v46}, Landroid/database/Cursor;->moveToFirst()Z

    #@5e2
    move-result v2

    #@5e3
    if-eqz v2, :cond_64b

    #@5e5
    .line 1099
    :cond_5e5
    new-instance v6, Lcom/android/internal/telephony/ApnSetting;

    #@5e7
    const-string v2, "_id"

    #@5e9
    move-object/from16 v0, v46

    #@5eb
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@5ee
    move-result v2

    #@5ef
    move-object/from16 v0, v46

    #@5f1
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    #@5f4
    move-result v7

    #@5f5
    const-string v8, "12345"

    #@5f7
    const-string v9, "Name"

    #@5f9
    const-string v2, "apn"

    #@5fb
    move-object/from16 v0, v46

    #@5fd
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@600
    move-result v2

    #@601
    move-object/from16 v0, v46

    #@603
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@606
    move-result-object v10

    #@607
    const-string v11, ""

    #@609
    const-string v12, ""

    #@60b
    const-string v13, ""

    #@60d
    const-string v14, ""

    #@60f
    const-string v15, ""

    #@611
    const-string v16, ""

    #@613
    const-string v17, ""

    #@615
    const/16 v18, 0x0

    #@617
    const/16 v19, 0x0

    #@619
    const-string v20, ""

    #@61b
    const-string v21, ""

    #@61d
    const/16 v22, 0x1

    #@61f
    const/16 v23, 0x0

    #@621
    invoke-direct/range {v6 .. v23}, Lcom/android/internal/telephony/ApnSetting;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V

    #@624
    .line 1104
    .local v6, apn:Lcom/android/internal/telephony/ApnSetting;
    move-object/from16 v0, p0

    #@626
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@628
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->apnSelectionHdlr:Lcom/android/internal/telephony/ApnSelectionHandler;

    #@62a
    invoke-virtual {v2, v6}, Lcom/android/internal/telephony/ApnSelectionHandler;->findAllOperatorApnID(Lcom/android/internal/telephony/ApnSetting;)V

    #@62d
    .line 1106
    move-object/from16 v0, p0

    #@62f
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@631
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->apnSelectionHdlr:Lcom/android/internal/telephony/ApnSelectionHandler;

    #@633
    iget v2, v2, Lcom/android/internal/telephony/ApnSelectionHandler;->Roaming_LTE_APN_ID:I

    #@635
    move-object/from16 v0, p0

    #@637
    iput v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->APN_ID_FOR_LTE_Roaming:I

    #@639
    .line 1108
    move-object/from16 v0, p0

    #@63b
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@63d
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->apnSelectionHdlr:Lcom/android/internal/telephony/ApnSelectionHandler;

    #@63f
    iget v2, v2, Lcom/android/internal/telephony/ApnSelectionHandler;->IMS_APN_ID:I

    #@641
    move-object/from16 v0, p0

    #@643
    iput v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->APN_ID_FOR_IMS:I

    #@645
    .line 1109
    invoke-interface/range {v46 .. v46}, Landroid/database/Cursor;->moveToNext()Z

    #@648
    move-result v2

    #@649
    if-nez v2, :cond_5e5

    #@64b
    .line 1115
    .end local v5           #selection:Ljava/lang/String;
    .end local v6           #apn:Lcom/android/internal/telephony/ApnSetting;
    .end local v46           #cursor:Landroid/database/Cursor;
    :cond_64b
    move-object/from16 v0, p0

    #@64d
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@64f
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->apnSelectionHdlr:Lcom/android/internal/telephony/ApnSelectionHandler;

    #@651
    move-object/from16 v0, p0

    #@653
    iget-object v3, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@655
    iget-object v3, v3, Lcom/android/internal/telephony/DataConnectionTracker;->apnSelectionHdlr:Lcom/android/internal/telephony/ApnSelectionHandler;

    #@657
    const-string v3, "Select_default_APN_between_domestic_and_roaming"

    #@659
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/ApnSelectionHandler;->selectApn(Ljava/lang/String;)V

    #@65c
    .line 1119
    .end local v73           #operator:Ljava/lang/String;
    .end local v75           #r:Lcom/android/internal/telephony/uicc/IccRecords;
    :cond_65c
    move-object/from16 v0, p0

    #@65e
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@660
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@663
    move-result-object v2

    #@664
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@667
    move-result-object v2

    #@668
    const-string v3, "SENDPDNTABLE_ENABLE_SAVE"

    #@66a
    const/4 v4, 0x0

    #@66b
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@66e
    move-result v2

    #@66f
    if-nez v2, :cond_67a

    #@671
    .line 1121
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->sendPdnTable()V

    #@674
    goto/16 :goto_3b

    #@676
    .line 1085
    .restart local v75       #r:Lcom/android/internal/telephony/uicc/IccRecords;
    :cond_676
    const-string v73, ""

    #@678
    goto/16 :goto_592

    #@67a
    .line 1124
    .end local v75           #r:Lcom/android/internal/telephony/uicc/IccRecords;
    :cond_67a
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@67c
    const-string v3, "NoDBSync is Enable"

    #@67e
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@681
    goto/16 :goto_3b

    #@683
    .line 1129
    :sswitch_683
    invoke-direct/range {p0 .. p1}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->handleGetPreferredNetworkTypeResponse(Landroid/os/Message;)V

    #@686
    goto/16 :goto_3b

    #@688
    .line 1133
    :sswitch_688
    invoke-direct/range {p0 .. p1}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->handleSetPreferredNetworkTypeResponse(Landroid/os/Message;)V

    #@68b
    goto/16 :goto_3b

    #@68d
    .line 1137
    :sswitch_68d
    move-object/from16 v0, p1

    #@68f
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@691
    move-object/from16 v71, v0

    #@693
    check-cast v71, Landroid/os/AsyncResult;

    #@695
    .line 1138
    .local v71, new_ar:Landroid/os/AsyncResult;
    move-object/from16 v0, v71

    #@697
    iget-object v2, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@699
    check-cast v2, Lcom/android/internal/telephony/LGDataConnectionTrackerMsg;

    #@69b
    move-object/from16 v24, v2

    #@69d
    check-cast v24, Lcom/android/internal/telephony/LGDataConnectionTrackerMsg;

    #@69f
    .line 1140
    .local v24, LGDataMsg:Lcom/android/internal/telephony/LGDataConnectionTrackerMsg;
    if-eqz v24, :cond_3b

    #@6a1
    move-object/from16 v0, v24

    #@6a3
    iget-boolean v2, v0, Lcom/android/internal/telephony/LGDataConnectionTrackerMsg;->valid:Z

    #@6a5
    if-eqz v2, :cond_3b

    #@6a7
    .line 1142
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@6a9
    new-instance v3, Ljava/lang/StringBuilder;

    #@6ab
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@6ae
    const-string v4, "[LGE_DATA] EVENT_FAKE_DATACONNECTION_EVENT  valid : "

    #@6b0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b3
    move-result-object v3

    #@6b4
    move-object/from16 v0, v24

    #@6b6
    iget v4, v0, Lcom/android/internal/telephony/LGDataConnectionTrackerMsg;->what:I

    #@6b8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6bb
    move-result-object v3

    #@6bc
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6bf
    move-result-object v3

    #@6c0
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6c3
    .line 1144
    move-object/from16 v0, v24

    #@6c5
    iget v2, v0, Lcom/android/internal/telephony/LGDataConnectionTrackerMsg;->what:I

    #@6c7
    sparse-switch v2, :sswitch_data_13b8

    #@6ca
    goto/16 :goto_21

    #@6cc
    .line 1267
    :sswitch_6cc
    move-object/from16 v0, v24

    #@6ce
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGDataConnectionTrackerMsg;->success:Z

    #@6d0
    move/from16 v65, v0

    #@6d2
    .line 1268
    .local v65, issucess:Z
    move-object/from16 v0, v24

    #@6d4
    iget-object v0, v0, Lcom/android/internal/telephony/LGDataConnectionTrackerMsg;->type:Ljava/lang/String;

    #@6d6
    move-object/from16 v36, v0

    #@6d8
    .line 1270
    .local v36, apnType:Ljava/lang/String;
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@6da
    new-instance v3, Ljava/lang/StringBuilder;

    #@6dc
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@6df
    const-string v4, "[LGE_DATA] apnType = "

    #@6e1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e4
    move-result-object v3

    #@6e5
    move-object/from16 v0, v36

    #@6e7
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6ea
    move-result-object v3

    #@6eb
    const-string v4, "issucess = "

    #@6ed
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f0
    move-result-object v3

    #@6f1
    move/from16 v0, v65

    #@6f3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@6f6
    move-result-object v3

    #@6f7
    const-string v4, " valid ="

    #@6f9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6fc
    move-result-object v3

    #@6fd
    move-object/from16 v0, v24

    #@6ff
    iget-boolean v4, v0, Lcom/android/internal/telephony/LGDataConnectionTrackerMsg;->valid:Z

    #@701
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@704
    move-result-object v3

    #@705
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@708
    move-result-object v3

    #@709
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@70c
    .line 1272
    const/16 v38, 0x0

    #@70e
    .line 1273
    .local v38, apn_info:Lcom/android/internal/telephony/ApnSetting;
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@710
    new-instance v3, Ljava/lang/StringBuilder;

    #@712
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@715
    const-string v4, "[LGE_DATA] EVENT_DATA_SETUP_COMPLETE Connected?? = "

    #@717
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71a
    move-result-object v3

    #@71b
    move/from16 v0, v65

    #@71d
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@720
    move-result-object v3

    #@721
    const-string v4, " apnType = "

    #@723
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@726
    move-result-object v3

    #@727
    move-object/from16 v0, v36

    #@729
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72c
    move-result-object v3

    #@72d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@730
    move-result-object v3

    #@731
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@734
    .line 1274
    move-object/from16 v0, p0

    #@736
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@738
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->mApnContexts:Ljava/util/concurrent/ConcurrentHashMap;

    #@73a
    move-object/from16 v0, v36

    #@73c
    invoke-virtual {v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@73f
    move-result-object v31

    #@740
    check-cast v31, Lcom/android/internal/telephony/ApnContext;

    #@742
    .line 1277
    .restart local v31       #apnContext:Lcom/android/internal/telephony/ApnContext;
    if-nez v31, :cond_9b7

    #@744
    .line 1278
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@746
    const-string v3, "[LGE_DATA] EVENT_DATA_SETUP_COMPLETE apnContext is NULL!"

    #@748
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@74b
    goto/16 :goto_3b

    #@74d
    .line 1147
    .end local v31           #apnContext:Lcom/android/internal/telephony/ApnContext;
    .end local v36           #apnType:Ljava/lang/String;
    .end local v38           #apn_info:Lcom/android/internal/telephony/ApnSetting;
    .end local v65           #issucess:Z
    :sswitch_74d
    move-object/from16 v0, v24

    #@74f
    iget v0, v0, Lcom/android/internal/telephony/LGDataConnectionTrackerMsg;->apntype_n:I

    #@751
    move/from16 v39, v0

    #@753
    .line 1148
    .local v39, apntype:I
    move-object/from16 v0, v24

    #@755
    iget v0, v0, Lcom/android/internal/telephony/LGDataConnectionTrackerMsg;->enable:I

    #@757
    move/from16 v51, v0

    #@759
    .line 1150
    .local v51, enable:I
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@75b
    new-instance v3, Ljava/lang/StringBuilder;

    #@75d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@760
    const-string v4, "[LGE_DATA] EVENT_ENABLE_NEW_APN type =  "

    #@762
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@765
    move-result-object v3

    #@766
    move/from16 v0, v39

    #@768
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@76b
    move-result-object v3

    #@76c
    const-string v4, " enable = "

    #@76e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@771
    move-result-object v3

    #@772
    move/from16 v0, v51

    #@774
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@777
    move-result-object v3

    #@778
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77b
    move-result-object v3

    #@77c
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@77f
    .line 1152
    move-object/from16 v0, p0

    #@781
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@783
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->mApnContexts:Ljava/util/concurrent/ConcurrentHashMap;

    #@785
    invoke-static/range {v39 .. v39}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@788
    move-result-object v3

    #@789
    invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@78c
    move-result-object v34

    #@78d
    check-cast v34, Lcom/android/internal/telephony/ApnContext;

    #@78f
    .line 1153
    .local v34, apnContext_new:Lcom/android/internal/telephony/ApnContext;
    const/4 v2, 0x1

    #@790
    move/from16 v0, v51

    #@792
    if-ne v0, v2, :cond_79b

    #@794
    .line 1155
    sget-object v2, Lcom/android/internal/telephony/LGEDataConnectionTracker;->setTeardownRequested:[Z

    #@796
    const/4 v3, 0x0

    #@797
    aput-boolean v3, v2, v39

    #@799
    goto/16 :goto_21

    #@79b
    .line 1160
    :cond_79b
    sget-object v2, Lcom/android/internal/telephony/LGEDataConnectionTracker;->setTeardownRequested:[Z

    #@79d
    const/4 v3, 0x1

    #@79e
    aput-boolean v3, v2, v39

    #@7a0
    .line 1162
    move-object/from16 v0, p0

    #@7a2
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@7a4
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@7a6
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@7a9
    move-result-object v2

    #@7aa
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MMS_ON_DATA_DISABLED_SKT:Z

    #@7ac
    if-eqz v2, :cond_21

    #@7ae
    .line 1164
    if-eqz v34, :cond_21

    #@7b0
    .line 1166
    move-object/from16 v0, p0

    #@7b2
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@7b4
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@7b7
    move-result-object v2

    #@7b8
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7bb
    move-result-object v2

    #@7bc
    const-string v3, "mobile_data"

    #@7be
    const/4 v4, 0x1

    #@7bf
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@7c2
    move-result v2

    #@7c3
    if-nez v2, :cond_21

    #@7c5
    invoke-virtual/range {v34 .. v34}, Lcom/android/internal/telephony/ApnContext;->isEnabled()Z

    #@7c8
    move-result v2

    #@7c9
    if-eqz v2, :cond_21

    #@7cb
    move-object/from16 v0, p0

    #@7cd
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@7cf
    const/4 v2, 0x1

    #@7d0
    move/from16 v0, v39

    #@7d2
    if-ne v0, v2, :cond_21

    #@7d4
    .line 1171
    invoke-virtual/range {v34 .. v34}, Lcom/android/internal/telephony/ApnContext;->getApnSetting()Lcom/android/internal/telephony/DataProfile;

    #@7d7
    move-result-object v6

    #@7d8
    check-cast v6, Lcom/android/internal/telephony/ApnSetting;

    #@7da
    .line 1173
    .restart local v6       #apn:Lcom/android/internal/telephony/ApnSetting;
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@7dc
    new-instance v3, Ljava/lang/StringBuilder;

    #@7de
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7e1
    const-string v4, "[LGE_DATA] <disableApnType()> apn : "

    #@7e3
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e6
    move-result-object v3

    #@7e7
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7ea
    move-result-object v3

    #@7eb
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7ee
    move-result-object v3

    #@7ef
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7f2
    .line 1174
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@7f4
    new-instance v3, Ljava/lang/StringBuilder;

    #@7f6
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7f9
    const-string v4, "[LGE_DATA] <disableApnType()> original_apn_formms : "

    #@7fb
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7fe
    move-result-object v3

    #@7ff
    move-object/from16 v0, p0

    #@801
    iget-object v4, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->original_apn_formms:Ljava/lang/String;

    #@803
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@806
    move-result-object v3

    #@807
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80a
    move-result-object v3

    #@80b
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@80e
    .line 1176
    if-eqz v6, :cond_823

    #@810
    move-object/from16 v0, p0

    #@812
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->original_apn_formms:Ljava/lang/String;

    #@814
    if-eqz v2, :cond_823

    #@816
    .line 1178
    move-object/from16 v0, p0

    #@818
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->original_apn_formms:Ljava/lang/String;

    #@81a
    iput-object v2, v6, Lcom/android/internal/telephony/DataProfile;->apn:Ljava/lang/String;

    #@81c
    .line 1179
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@81e
    const-string v3, "[LGE_DATA] <disableApnType()> APN changes from \'mmsonly\' to \'default\'."

    #@820
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@823
    .line 1182
    :cond_823
    const/4 v2, 0x0

    #@824
    move-object/from16 v0, p0

    #@826
    iput-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->original_apn_formms:Ljava/lang/String;

    #@828
    goto/16 :goto_21

    #@82a
    .line 1193
    .end local v6           #apn:Lcom/android/internal/telephony/ApnSetting;
    .end local v34           #apnContext_new:Lcom/android/internal/telephony/ApnContext;
    .end local v39           #apntype:I
    .end local v51           #enable:I
    :sswitch_82a
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@82c
    const-string v3, "[LGE_DATA] EVENT_TRY_SETUP_DATA = "

    #@82e
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@831
    .line 1194
    move-object/from16 v0, v24

    #@833
    iget-object v0, v0, Lcom/android/internal/telephony/LGDataConnectionTrackerMsg;->reason:Ljava/lang/String;

    #@835
    move-object/from16 v76, v0

    #@837
    .line 1195
    .local v76, reason:Ljava/lang/String;
    goto/16 :goto_21

    #@839
    .line 1202
    .end local v76           #reason:Ljava/lang/String;
    :sswitch_839
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@83b
    const-string v3, "[LGE_DATA] EVENT_ROAMING_OFF = "

    #@83d
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@840
    .line 1204
    move-object/from16 v0, p0

    #@842
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@844
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@847
    move-result-object v2

    #@848
    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@84b
    move-result v2

    #@84c
    if-nez v2, :cond_21

    #@84e
    move-object/from16 v0, p0

    #@850
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@852
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@854
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@857
    move-result-object v2

    #@858
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_NOTI_USERDATADISABLE_KR:Z

    #@85a
    const/4 v3, 0x1

    #@85b
    if-ne v2, v3, :cond_21

    #@85d
    .line 1205
    move-object/from16 v0, p0

    #@85f
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@861
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@864
    move-result-object v2

    #@865
    const-string v3, "notification"

    #@867
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@86a
    move-result-object v72

    #@86b
    check-cast v72, Landroid/app/NotificationManager;

    #@86d
    .line 1208
    .local v72, notificationManager:Landroid/app/NotificationManager;
    move-object/from16 v0, p0

    #@86f
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@871
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@874
    move-result-object v2

    #@875
    const v3, 0x209030c

    #@878
    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@87b
    move-result-object v50

    #@87c
    .line 1209
    .local v50, details:Ljava/lang/CharSequence;
    move-object/from16 v0, p0

    #@87e
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@880
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@883
    move-result-object v2

    #@884
    const v3, 0x209030b

    #@887
    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@88a
    move-result-object v79

    #@88b
    .line 1210
    .local v79, title:Ljava/lang/CharSequence;
    move-object/from16 v0, p0

    #@88d
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mNotification:Landroid/app/Notification;

    #@88f
    move-object/from16 v0, v79

    #@891
    iput-object v0, v2, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@893
    .line 1212
    move-object/from16 v0, p0

    #@895
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mNotification:Landroid/app/Notification;

    #@897
    move-object/from16 v0, p0

    #@899
    iget-object v3, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@89b
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@89e
    move-result-object v3

    #@89f
    move-object/from16 v0, p0

    #@8a1
    iget-object v4, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mNotification:Landroid/app/Notification;

    #@8a3
    iget-object v4, v4, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    #@8a5
    move-object/from16 v0, v79

    #@8a7
    move-object/from16 v1, v50

    #@8a9
    invoke-virtual {v2, v3, v0, v1, v4}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@8ac
    .line 1214
    move-object/from16 v0, p0

    #@8ae
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@8b0
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@8b3
    move-result-object v2

    #@8b4
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@8b7
    move-result-object v2

    #@8b8
    const-string v3, "mobile_data"

    #@8ba
    const/4 v4, 0x1

    #@8bb
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@8be
    move-result v48

    #@8bf
    .line 1215
    .local v48, dataNetwork:I
    const/4 v2, 0x1

    #@8c0
    move/from16 v0, v48

    #@8c2
    if-ne v0, v2, :cond_8d4

    #@8c4
    .line 1216
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@8c6
    const-string v3, "[EVENT_ROAMING_OFF] clean Notification"

    #@8c8
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8cb
    .line 1217
    const/16 v2, 0x9f6

    #@8cd
    move-object/from16 v0, v72

    #@8cf
    invoke-virtual {v0, v2}, Landroid/app/NotificationManager;->cancel(I)V

    #@8d2
    goto/16 :goto_21

    #@8d4
    .line 1219
    :cond_8d4
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@8d6
    const-string v3, "[EVENT_ROAMING_OFF] put notification"

    #@8d8
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8db
    .line 1220
    const/16 v2, 0x9f6

    #@8dd
    move-object/from16 v0, p0

    #@8df
    iget-object v3, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mNotification:Landroid/app/Notification;

    #@8e1
    move-object/from16 v0, v72

    #@8e3
    invoke-virtual {v0, v2, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@8e6
    goto/16 :goto_21

    #@8e8
    .line 1227
    .end local v48           #dataNetwork:I
    .end local v50           #details:Ljava/lang/CharSequence;
    .end local v72           #notificationManager:Landroid/app/NotificationManager;
    .end local v79           #title:Ljava/lang/CharSequence;
    :sswitch_8e8
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@8ea
    const-string v3, "[LGE_DATA] EVENT_ROAMING_ON = "

    #@8ec
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8ef
    .line 1229
    move-object/from16 v0, p0

    #@8f1
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@8f3
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@8f6
    move-result-object v2

    #@8f7
    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@8fa
    move-result v63

    #@8fb
    .line 1230
    .local v63, isRoaming:Z
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@8fd
    const-string v3, "onRoamingOn: setup data on roaming"

    #@8ff
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@902
    .line 1232
    if-eqz v63, :cond_97a

    #@904
    move-object/from16 v0, p0

    #@906
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@908
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@90a
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@90d
    move-result-object v2

    #@90e
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_PAYPOPUP_ROAMING_SKT:Z

    #@910
    if-eqz v2, :cond_97a

    #@912
    move-object/from16 v0, p0

    #@914
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@916
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@918
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@91b
    move-result-object v2

    #@91c
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_SELECT_KR:Z

    #@91e
    if-eqz v2, :cond_97a

    #@920
    .line 1235
    move-object/from16 v0, p0

    #@922
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@924
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->mAllApns:Ljava/util/ArrayList;

    #@926
    if-eqz v2, :cond_97a

    #@928
    .line 1236
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@92a
    const-string v3, "onRoamingOn: sendPdnTable"

    #@92c
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@92f
    .line 1237
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->sendPdnTable()V

    #@932
    .line 1239
    move-object/from16 v0, p0

    #@934
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@936
    invoke-virtual {v2}, Lcom/android/internal/telephony/DataConnectionTracker;->getDataOnRoamingEnabled()Z

    #@939
    move-result v2

    #@93a
    if-nez v2, :cond_97a

    #@93c
    move-object/from16 v0, p0

    #@93e
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@940
    invoke-virtual {v2}, Lcom/android/internal/telephony/DataConnectionTracker;->getPreferredNetworkMode()I

    #@943
    move-result v2

    #@944
    const/16 v3, 0x9

    #@946
    if-ne v2, v3, :cond_97a

    #@948
    .line 1241
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@94a
    const-string v3, "Roaming Disable and mode=NT_MODE_GSM_UMTS"

    #@94c
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@94f
    .line 1242
    move-object/from16 v0, p0

    #@951
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@953
    const/4 v3, 0x3

    #@954
    const v4, 0x42029

    #@957
    move-object/from16 v0, p0

    #@959
    invoke-virtual {v0, v4}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->obtainMessage(I)Landroid/os/Message;

    #@95c
    move-result-object v4

    #@95d
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/telephony/PhoneBase;->setPreferredNetworkType(ILandroid/os/Message;)V

    #@960
    .line 1243
    move-object/from16 v0, p0

    #@962
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@964
    const/4 v3, 0x3

    #@965
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/DataConnectionTracker;->setPreferredNetworkMode(I)V

    #@968
    .line 1245
    move-object/from16 v0, p0

    #@96a
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@96c
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@96f
    move-result-object v2

    #@970
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@973
    move-result-object v2

    #@974
    const-string v3, "preferred_network_mode"

    #@976
    const/4 v4, 0x0

    #@977
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@97a
    .line 1252
    :cond_97a
    move-object/from16 v0, p0

    #@97c
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@97e
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@981
    move-result-object v2

    #@982
    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@985
    move-result v2

    #@986
    if-eqz v2, :cond_21

    #@988
    move-object/from16 v0, p0

    #@98a
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@98c
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@98e
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@991
    move-result-object v2

    #@992
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_NOTI_USERDATADISABLE_KR:Z

    #@994
    const/4 v3, 0x1

    #@995
    if-ne v2, v3, :cond_21

    #@997
    .line 1253
    move-object/from16 v0, p0

    #@999
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@99b
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@99e
    move-result-object v2

    #@99f
    const-string v3, "notification"

    #@9a1
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@9a4
    move-result-object v72

    #@9a5
    check-cast v72, Landroid/app/NotificationManager;

    #@9a7
    .line 1255
    .restart local v72       #notificationManager:Landroid/app/NotificationManager;
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@9a9
    const-string v3, "[EVENT_ROAMING_ON]this case is on roaming, so clean Notification"

    #@9ab
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9ae
    .line 1256
    const/16 v2, 0x9f6

    #@9b0
    move-object/from16 v0, v72

    #@9b2
    invoke-virtual {v0, v2}, Landroid/app/NotificationManager;->cancel(I)V

    #@9b5
    goto/16 :goto_21

    #@9b7
    .line 1283
    .end local v63           #isRoaming:Z
    .end local v72           #notificationManager:Landroid/app/NotificationManager;
    .restart local v31       #apnContext:Lcom/android/internal/telephony/ApnContext;
    .restart local v36       #apnType:Ljava/lang/String;
    .restart local v38       #apn_info:Lcom/android/internal/telephony/ApnSetting;
    .restart local v65       #issucess:Z
    :cond_9b7
    if-eqz v31, :cond_9bf

    #@9b9
    .line 1284
    invoke-virtual/range {v31 .. v31}, Lcom/android/internal/telephony/ApnContext;->getApnSetting()Lcom/android/internal/telephony/DataProfile;

    #@9bc
    move-result-object v38

    #@9bd
    .end local v38           #apn_info:Lcom/android/internal/telephony/ApnSetting;
    check-cast v38, Lcom/android/internal/telephony/ApnSetting;

    #@9bf
    .line 1286
    .restart local v38       #apn_info:Lcom/android/internal/telephony/ApnSetting;
    :cond_9bf
    if-eqz v65, :cond_e05

    #@9c1
    .line 1291
    move-object/from16 v0, p0

    #@9c3
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@9c5
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@9c7
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@9ca
    move-result-object v2

    #@9cb
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_GPRS_REJECTED_KT:Z

    #@9cd
    if-eqz v2, :cond_9e3

    #@9cf
    .line 1293
    move-object/from16 v0, p0

    #@9d1
    iget-boolean v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->isGsm:Z

    #@9d3
    if-eqz v2, :cond_9e3

    #@9d5
    .line 1294
    move-object/from16 v0, p0

    #@9d7
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mGsst:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@9d9
    move-object/from16 v0, p0

    #@9db
    iget-object v3, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mGsst:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@9dd
    const/16 v3, 0x3ef

    #@9df
    const/4 v4, 0x0

    #@9e0
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setPdpRejectedNotification(II)V

    #@9e3
    .line 1300
    :cond_9e3
    move-object/from16 v0, p0

    #@9e5
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@9e7
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@9e9
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@9ec
    move-result-object v2

    #@9ed
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MMS_ON_DATA_DISABLED_SKT:Z

    #@9ef
    if-eqz v2, :cond_a30

    #@9f1
    .line 1303
    if-eqz v31, :cond_a30

    #@9f3
    invoke-virtual/range {v31 .. v31}, Lcom/android/internal/telephony/ApnContext;->isEnabled()Z

    #@9f6
    move-result v2

    #@9f7
    if-eqz v2, :cond_a30

    #@9f9
    invoke-virtual/range {v31 .. v31}, Lcom/android/internal/telephony/ApnContext;->getApnType()Ljava/lang/String;

    #@9fc
    move-result-object v2

    #@9fd
    const-string v3, "mms"

    #@9ff
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@a02
    move-result v2

    #@a03
    if-eqz v2, :cond_a30

    #@a05
    move-object/from16 v0, p0

    #@a07
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@a09
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@a0c
    move-result-object v2

    #@a0d
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a10
    move-result-object v2

    #@a11
    const-string v3, "mobile_data"

    #@a13
    const/4 v4, 0x1

    #@a14
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@a17
    move-result v2

    #@a18
    if-nez v2, :cond_a30

    #@a1a
    .line 1306
    if-eqz v38, :cond_a24

    #@a1c
    .line 1307
    move-object/from16 v0, p0

    #@a1e
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->original_apn_formms:Ljava/lang/String;

    #@a20
    move-object/from16 v0, v38

    #@a22
    iput-object v2, v0, Lcom/android/internal/telephony/DataProfile;->apn:Ljava/lang/String;

    #@a24
    .line 1309
    :cond_a24
    const/4 v2, 0x0

    #@a25
    move-object/from16 v0, p0

    #@a27
    iput-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->original_apn_formms:Ljava/lang/String;

    #@a29
    .line 1313
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@a2b
    const-string v3, "[onDataSetupComplete] APN changes from \'mmsonly\' to \'default\'."

    #@a2d
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a30
    .line 1321
    :cond_a30
    const-string v35, ""

    #@a32
    .line 1322
    .local v35, apnStr:Ljava/lang/String;
    if-eqz v38, :cond_a3a

    #@a34
    move-object/from16 v0, v38

    #@a36
    iget-object v0, v0, Lcom/android/internal/telephony/DataProfile;->apn:Ljava/lang/String;

    #@a38
    move-object/from16 v35, v0

    #@a3a
    .line 1325
    :cond_a3a
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@a3c
    new-instance v3, Ljava/lang/StringBuilder;

    #@a3e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@a41
    const-string v4, "[LGE_DATA] onDataSetupComplete: success apn name="

    #@a43
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a46
    move-result-object v3

    #@a47
    move-object/from16 v0, v35

    #@a49
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4c
    move-result-object v3

    #@a4d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a50
    move-result-object v3

    #@a51
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a54
    .line 1328
    move-object/from16 v0, p0

    #@a56
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@a58
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@a5a
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@a5d
    move-result-object v2

    #@a5e
    iget-object v2, v2, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@a60
    const-string v3, "LGTBASE"

    #@a62
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a65
    move-result v2

    #@a66
    if-eqz v2, :cond_bfa

    #@a68
    invoke-virtual/range {v31 .. v31}, Lcom/android/internal/telephony/ApnContext;->getApnType()Ljava/lang/String;

    #@a6b
    move-result-object v2

    #@a6c
    const-string v3, "ims"

    #@a6e
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@a71
    move-result v2

    #@a72
    if-eqz v2, :cond_bfa

    #@a74
    .line 1330
    move-object/from16 v0, p0

    #@a76
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@a78
    sget-object v3, Lcom/android/internal/telephony/IPVersion;->INET:Lcom/android/internal/telephony/IPVersion;

    #@a7a
    invoke-virtual {v3}, Lcom/android/internal/telephony/IPVersion;->toString()Ljava/lang/String;

    #@a7d
    move-result-object v3

    #@a7e
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/PhoneBase;->getPcscfAddress(Ljava/lang/String;)[Ljava/lang/String;

    #@a81
    move-result-object v2

    #@a82
    if-eqz v2, :cond_b37

    #@a84
    .line 1331
    const/16 v53, 0x0

    #@a86
    .local v53, i:I
    :goto_a86
    move-object/from16 v0, p0

    #@a88
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@a8a
    sget-object v3, Lcom/android/internal/telephony/IPVersion;->INET:Lcom/android/internal/telephony/IPVersion;

    #@a8c
    invoke-virtual {v3}, Lcom/android/internal/telephony/IPVersion;->toString()Ljava/lang/String;

    #@a8f
    move-result-object v3

    #@a90
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/PhoneBase;->getPcscfAddress(Ljava/lang/String;)[Ljava/lang/String;

    #@a93
    move-result-object v2

    #@a94
    array-length v2, v2

    #@a95
    move/from16 v0, v53

    #@a97
    if-ge v0, v2, :cond_b37

    #@a99
    const/4 v2, 0x2

    #@a9a
    move/from16 v0, v53

    #@a9c
    if-ge v0, v2, :cond_b37

    #@a9e
    .line 1332
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@aa0
    new-instance v3, Ljava/lang/StringBuilder;

    #@aa2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@aa5
    const-string v4, "[LGE_DATA][pcscf]4-2 getPcscfAddress"

    #@aa7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aaa
    move-result-object v3

    #@aab
    move/from16 v0, v53

    #@aad
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ab0
    move-result-object v3

    #@ab1
    const-string v4, "["

    #@ab3
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab6
    move-result-object v3

    #@ab7
    move-object/from16 v0, p0

    #@ab9
    iget-object v4, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@abb
    sget-object v7, Lcom/android/internal/telephony/IPVersion;->INET:Lcom/android/internal/telephony/IPVersion;

    #@abd
    invoke-virtual {v7}, Lcom/android/internal/telephony/IPVersion;->toString()Ljava/lang/String;

    #@ac0
    move-result-object v7

    #@ac1
    invoke-virtual {v4, v7}, Lcom/android/internal/telephony/PhoneBase;->getPcscfAddress(Ljava/lang/String;)[Ljava/lang/String;

    #@ac4
    move-result-object v4

    #@ac5
    aget-object v4, v4, v53

    #@ac7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aca
    move-result-object v3

    #@acb
    const-string v4, "]"

    #@acd
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad0
    move-result-object v3

    #@ad1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ad4
    move-result-object v3

    #@ad5
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ad8
    .line 1335
    move-object/from16 v0, p0

    #@ada
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@adc
    sget-object v3, Lcom/android/internal/telephony/IPVersion;->INET:Lcom/android/internal/telephony/IPVersion;

    #@ade
    invoke-virtual {v3}, Lcom/android/internal/telephony/IPVersion;->toString()Ljava/lang/String;

    #@ae1
    move-result-object v3

    #@ae2
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/PhoneBase;->getPcscfAddress(Ljava/lang/String;)[Ljava/lang/String;

    #@ae5
    move-result-object v2

    #@ae6
    aget-object v2, v2, v53

    #@ae8
    if-eqz v2, :cond_b33

    #@aea
    .line 1336
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@aec
    new-instance v3, Ljava/lang/StringBuilder;

    #@aee
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@af1
    const-string v4, "[LGE_DATA][pcscf] set v4 property["

    #@af3
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af6
    move-result-object v3

    #@af7
    move/from16 v0, v53

    #@af9
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@afc
    move-result-object v3

    #@afd
    const-string v4, "]"

    #@aff
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b02
    move-result-object v3

    #@b03
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b06
    move-result-object v3

    #@b07
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b0a
    .line 1337
    new-instance v2, Ljava/lang/StringBuilder;

    #@b0c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b0f
    const-string v3, "net.pcscf"

    #@b11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b14
    move-result-object v2

    #@b15
    move/from16 v0, v53

    #@b17
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b1a
    move-result-object v2

    #@b1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b1e
    move-result-object v2

    #@b1f
    move-object/from16 v0, p0

    #@b21
    iget-object v3, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@b23
    sget-object v4, Lcom/android/internal/telephony/IPVersion;->INET:Lcom/android/internal/telephony/IPVersion;

    #@b25
    invoke-virtual {v4}, Lcom/android/internal/telephony/IPVersion;->toString()Ljava/lang/String;

    #@b28
    move-result-object v4

    #@b29
    invoke-virtual {v3, v4}, Lcom/android/internal/telephony/PhoneBase;->getPcscfAddress(Ljava/lang/String;)[Ljava/lang/String;

    #@b2c
    move-result-object v3

    #@b2d
    const/4 v4, 0x0

    #@b2e
    aget-object v3, v3, v4

    #@b30
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@b33
    .line 1331
    :cond_b33
    add-int/lit8 v53, v53, 0x1

    #@b35
    goto/16 :goto_a86

    #@b37
    .line 1343
    .end local v53           #i:I
    :cond_b37
    move-object/from16 v0, p0

    #@b39
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@b3b
    sget-object v3, Lcom/android/internal/telephony/IPVersion;->INET6:Lcom/android/internal/telephony/IPVersion;

    #@b3d
    invoke-virtual {v3}, Lcom/android/internal/telephony/IPVersion;->toString()Ljava/lang/String;

    #@b40
    move-result-object v3

    #@b41
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/PhoneBase;->getPcscfAddress(Ljava/lang/String;)[Ljava/lang/String;

    #@b44
    move-result-object v2

    #@b45
    if-eqz v2, :cond_bfa

    #@b47
    .line 1344
    const/16 v53, 0x0

    #@b49
    .restart local v53       #i:I
    :goto_b49
    move-object/from16 v0, p0

    #@b4b
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@b4d
    sget-object v3, Lcom/android/internal/telephony/IPVersion;->INET6:Lcom/android/internal/telephony/IPVersion;

    #@b4f
    invoke-virtual {v3}, Lcom/android/internal/telephony/IPVersion;->toString()Ljava/lang/String;

    #@b52
    move-result-object v3

    #@b53
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/PhoneBase;->getPcscfAddress(Ljava/lang/String;)[Ljava/lang/String;

    #@b56
    move-result-object v2

    #@b57
    array-length v2, v2

    #@b58
    move/from16 v0, v53

    #@b5a
    if-ge v0, v2, :cond_bfa

    #@b5c
    const/4 v2, 0x2

    #@b5d
    move/from16 v0, v53

    #@b5f
    if-ge v0, v2, :cond_bfa

    #@b61
    .line 1345
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@b63
    new-instance v3, Ljava/lang/StringBuilder;

    #@b65
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b68
    const-string v4, "[LGE_DATA][pcscf]6-2 getPcscfAddress"

    #@b6a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6d
    move-result-object v3

    #@b6e
    move/from16 v0, v53

    #@b70
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b73
    move-result-object v3

    #@b74
    const-string v4, "["

    #@b76
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b79
    move-result-object v3

    #@b7a
    move-object/from16 v0, p0

    #@b7c
    iget-object v4, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@b7e
    sget-object v7, Lcom/android/internal/telephony/IPVersion;->INET6:Lcom/android/internal/telephony/IPVersion;

    #@b80
    invoke-virtual {v7}, Lcom/android/internal/telephony/IPVersion;->toString()Ljava/lang/String;

    #@b83
    move-result-object v7

    #@b84
    invoke-virtual {v4, v7}, Lcom/android/internal/telephony/PhoneBase;->getPcscfAddress(Ljava/lang/String;)[Ljava/lang/String;

    #@b87
    move-result-object v4

    #@b88
    aget-object v4, v4, v53

    #@b8a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8d
    move-result-object v3

    #@b8e
    const-string v4, "]"

    #@b90
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b93
    move-result-object v3

    #@b94
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b97
    move-result-object v3

    #@b98
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b9b
    .line 1348
    move-object/from16 v0, p0

    #@b9d
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@b9f
    sget-object v3, Lcom/android/internal/telephony/IPVersion;->INET6:Lcom/android/internal/telephony/IPVersion;

    #@ba1
    invoke-virtual {v3}, Lcom/android/internal/telephony/IPVersion;->toString()Ljava/lang/String;

    #@ba4
    move-result-object v3

    #@ba5
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/PhoneBase;->getPcscfAddress(Ljava/lang/String;)[Ljava/lang/String;

    #@ba8
    move-result-object v2

    #@ba9
    aget-object v2, v2, v53

    #@bab
    if-eqz v2, :cond_bf6

    #@bad
    .line 1349
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@baf
    new-instance v3, Ljava/lang/StringBuilder;

    #@bb1
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@bb4
    const-string v4, "[LGE_DATA][pcscf] set v6 property["

    #@bb6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb9
    move-result-object v3

    #@bba
    move/from16 v0, v53

    #@bbc
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bbf
    move-result-object v3

    #@bc0
    const-string v4, "]"

    #@bc2
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc5
    move-result-object v3

    #@bc6
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bc9
    move-result-object v3

    #@bca
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@bcd
    .line 1350
    new-instance v2, Ljava/lang/StringBuilder;

    #@bcf
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@bd2
    const-string v3, "net.pcscf"

    #@bd4
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd7
    move-result-object v2

    #@bd8
    move/from16 v0, v53

    #@bda
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bdd
    move-result-object v2

    #@bde
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@be1
    move-result-object v2

    #@be2
    move-object/from16 v0, p0

    #@be4
    iget-object v3, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@be6
    sget-object v4, Lcom/android/internal/telephony/IPVersion;->INET6:Lcom/android/internal/telephony/IPVersion;

    #@be8
    invoke-virtual {v4}, Lcom/android/internal/telephony/IPVersion;->toString()Ljava/lang/String;

    #@beb
    move-result-object v4

    #@bec
    invoke-virtual {v3, v4}, Lcom/android/internal/telephony/PhoneBase;->getPcscfAddress(Ljava/lang/String;)[Ljava/lang/String;

    #@bef
    move-result-object v3

    #@bf0
    const/4 v4, 0x0

    #@bf1
    aget-object v3, v3, v4

    #@bf3
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@bf6
    .line 1344
    :cond_bf6
    add-int/lit8 v53, v53, 0x1

    #@bf8
    goto/16 :goto_b49

    #@bfa
    .line 1359
    .end local v53           #i:I
    :cond_bfa
    move-object/from16 v0, p0

    #@bfc
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@bfe
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@c00
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@c03
    move-result-object v2

    #@c04
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_FAST_CONNECT_DEFAULT_PDN_KR:Z

    #@c06
    if-eqz v2, :cond_cfd

    #@c08
    invoke-virtual/range {v31 .. v31}, Lcom/android/internal/telephony/ApnContext;->getApnType()Ljava/lang/String;

    #@c0b
    move-result-object v2

    #@c0c
    const-string v3, "ims"

    #@c0e
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@c11
    move-result v2

    #@c12
    if-eqz v2, :cond_cfd

    #@c14
    move-object/from16 v0, p0

    #@c16
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@c18
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@c1b
    move-result-object v2

    #@c1c
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c1f
    move-result-object v2

    #@c20
    const-string v3, "mobile_data"

    #@c22
    const/4 v4, 0x1

    #@c23
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@c26
    move-result v2

    #@c27
    const/4 v3, 0x1

    #@c28
    if-ne v2, v3, :cond_cfd

    #@c2a
    .line 1363
    const/16 v49, 0x0

    #@c2c
    .line 1364
    .local v49, dcacForAlarmIntent:Lcom/android/internal/telephony/DataConnectionAc;
    const/16 v29, 0x0

    #@c2e
    .line 1366
    .local v29, alarmIntent:Landroid/app/PendingIntent;
    move-object/from16 v0, p0

    #@c30
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@c32
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->mApnContexts:Ljava/util/concurrent/ConcurrentHashMap;

    #@c34
    const-string v3, "default"

    #@c36
    invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@c39
    move-result-object v32

    #@c3a
    check-cast v32, Lcom/android/internal/telephony/ApnContext;

    #@c3c
    .line 1368
    .local v32, apnContext_default:Lcom/android/internal/telephony/ApnContext;
    if-eqz v32, :cond_c42

    #@c3e
    .line 1369
    invoke-virtual/range {v32 .. v32}, Lcom/android/internal/telephony/ApnContext;->getDataConnectionAc()Lcom/android/internal/telephony/DataConnectionAc;

    #@c41
    move-result-object v49

    #@c42
    .line 1372
    :cond_c42
    if-eqz v49, :cond_c48

    #@c44
    .line 1373
    invoke-virtual/range {v49 .. v49}, Lcom/android/internal/telephony/DataConnectionAc;->getReconnectIntentSync()Landroid/app/PendingIntent;

    #@c47
    move-result-object v29

    #@c48
    .line 1376
    :cond_c48
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@c4a
    new-instance v3, Ljava/lang/StringBuilder;

    #@c4c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@c4f
    const-string v4, "[onDataSetupComplete] : alarmIntent  :"

    #@c51
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c54
    move-result-object v3

    #@c55
    move-object/from16 v0, v29

    #@c57
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c5a
    move-result-object v3

    #@c5b
    const-string v4, "  default  :"

    #@c5d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c60
    move-result-object v3

    #@c61
    move-object/from16 v0, v32

    #@c63
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c66
    move-result-object v3

    #@c67
    const-string v4, " dcac :"

    #@c69
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6c
    move-result-object v3

    #@c6d
    move-object/from16 v0, v49

    #@c6f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c72
    move-result-object v3

    #@c73
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c76
    move-result-object v3

    #@c77
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c7a
    .line 1381
    if-eqz v29, :cond_cf6

    #@c7c
    invoke-virtual/range {v32 .. v32}, Lcom/android/internal/telephony/ApnContext;->getState()Lcom/android/internal/telephony/DctConstants$State;

    #@c7f
    move-result-object v2

    #@c80
    sget-object v3, Lcom/android/internal/telephony/DctConstants$State;->CONNECTED:Lcom/android/internal/telephony/DctConstants$State;

    #@c82
    if-eq v2, v3, :cond_cf6

    #@c84
    .line 1383
    move-object/from16 v0, p0

    #@c86
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@c88
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->mReconnectIntentForDefaultType:Landroid/content/Intent;

    #@c8a
    if-eqz v2, :cond_cf6

    #@c8c
    .line 1385
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@c8e
    new-instance v3, Ljava/lang/StringBuilder;

    #@c90
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@c93
    const-string v4, "onDataSetupComplete: cancel alarmIntent  :"

    #@c95
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c98
    move-result-object v3

    #@c99
    move-object/from16 v0, v29

    #@c9b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c9e
    move-result-object v3

    #@c9f
    const-string v4, "\tdefault  :"

    #@ca1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca4
    move-result-object v3

    #@ca5
    move-object/from16 v0, v32

    #@ca7
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@caa
    move-result-object v3

    #@cab
    const-string v4, " dcac :"

    #@cad
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb0
    move-result-object v3

    #@cb1
    move-object/from16 v0, v49

    #@cb3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@cb6
    move-result-object v3

    #@cb7
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cba
    move-result-object v3

    #@cbb
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@cbe
    .line 1386
    move-object/from16 v0, p0

    #@cc0
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@cc2
    move-object/from16 v0, v49

    #@cc4
    invoke-virtual {v2, v0}, Lcom/android/internal/telephony/DataConnectionTracker;->cancelReconnectAlarm(Lcom/android/internal/telephony/DataConnectionAc;)V

    #@cc7
    .line 1388
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@cc9
    new-instance v3, Ljava/lang/StringBuilder;

    #@ccb
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@cce
    const-string v4, "onDataSetupComplete: fast reconnect Default with Intent "

    #@cd0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd3
    move-result-object v3

    #@cd4
    move-object/from16 v0, p0

    #@cd6
    iget-object v4, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@cd8
    iget-object v4, v4, Lcom/android/internal/telephony/DataConnectionTracker;->mReconnectIntentForDefaultType:Landroid/content/Intent;

    #@cda
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@cdd
    move-result-object v3

    #@cde
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ce1
    move-result-object v3

    #@ce2
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ce5
    .line 1389
    move-object/from16 v0, p0

    #@ce7
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@ce9
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@cec
    move-result-object v2

    #@ced
    move-object/from16 v0, p0

    #@cef
    iget-object v3, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@cf1
    iget-object v3, v3, Lcom/android/internal/telephony/DataConnectionTracker;->mReconnectIntentForDefaultType:Landroid/content/Intent;

    #@cf3
    invoke-virtual {v2, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@cf6
    .line 1393
    :cond_cf6
    move-object/from16 v0, p0

    #@cf8
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@cfa
    const/4 v3, 0x0

    #@cfb
    iput-object v3, v2, Lcom/android/internal/telephony/DataConnectionTracker;->mReconnectIntentForDefaultType:Landroid/content/Intent;

    #@cfd
    .line 1399
    .end local v29           #alarmIntent:Landroid/app/PendingIntent;
    .end local v32           #apnContext_default:Lcom/android/internal/telephony/ApnContext;
    .end local v49           #dcacForAlarmIntent:Lcom/android/internal/telephony/DataConnectionAc;
    :cond_cfd
    move-object/from16 v0, p0

    #@cff
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@d01
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@d03
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@d06
    move-result-object v2

    #@d07
    iget-object v2, v2, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@d09
    const-string v3, "LGTBASE"

    #@d0b
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d0e
    move-result v2

    #@d0f
    if-eqz v2, :cond_d42

    #@d11
    .line 1400
    invoke-virtual/range {v31 .. v31}, Lcom/android/internal/telephony/ApnContext;->getApnType()Ljava/lang/String;

    #@d14
    move-result-object v2

    #@d15
    const-string v3, "default"

    #@d17
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d1a
    move-result v2

    #@d1b
    if-eqz v2, :cond_d42

    #@d1d
    .line 1401
    move-object/from16 v0, p0

    #@d1f
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@d21
    const/4 v3, 0x1

    #@d22
    iput-boolean v3, v2, Lcom/android/internal/telephony/DataConnectionTracker;->internetPDNconnected:Z

    #@d24
    .line 1402
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@d26
    new-instance v3, Ljava/lang/StringBuilder;

    #@d28
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d2b
    const-string v4, "[LGE_DATA] Default PDN Connected, internetPDNconnected = "

    #@d2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d30
    move-result-object v3

    #@d31
    move-object/from16 v0, p0

    #@d33
    iget-object v4, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@d35
    iget-boolean v4, v4, Lcom/android/internal/telephony/DataConnectionTracker;->internetPDNconnected:Z

    #@d37
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@d3a
    move-result-object v3

    #@d3b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d3e
    move-result-object v3

    #@d3f
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d42
    .line 1408
    :cond_d42
    move-object/from16 v0, p0

    #@d44
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@d46
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@d48
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@d4b
    move-result-object v2

    #@d4c
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_LTE_ROAMING_LGU:Z

    #@d4e
    if-eqz v2, :cond_21

    #@d50
    .line 1409
    move-object/from16 v0, p0

    #@d52
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@d54
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@d57
    move-result-object v2

    #@d58
    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@d5b
    move-result v2

    #@d5c
    if-eqz v2, :cond_21

    #@d5e
    .line 1410
    move-object/from16 v0, p0

    #@d60
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@d62
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@d65
    move-result-object v2

    #@d66
    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@d69
    move-result v27

    #@d6a
    .line 1411
    .local v27, aRadioTech:I
    const/4 v2, 0x1

    #@d6b
    move-object/from16 v0, p0

    #@d6d
    iput-boolean v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->bConnectionStatus:Z

    #@d6f
    .line 1414
    invoke-virtual/range {v31 .. v31}, Lcom/android/internal/telephony/ApnContext;->getApnSetting()Lcom/android/internal/telephony/DataProfile;

    #@d72
    move-result-object v26

    #@d73
    check-cast v26, Lcom/android/internal/telephony/ApnSetting;

    #@d75
    .line 1415
    .local v26, aApnSettingOfApnContext:Lcom/android/internal/telephony/ApnSetting;
    const-string v25, ""

    #@d77
    .line 1417
    .local v25, aApnOfApnContext:Ljava/lang/String;
    if-eqz v26, :cond_d7f

    #@d79
    move-object/from16 v0, v26

    #@d7b
    iget-object v0, v0, Lcom/android/internal/telephony/DataProfile;->apn:Ljava/lang/String;

    #@d7d
    move-object/from16 v25, v0

    #@d7f
    .line 1420
    :cond_d7f
    const/16 v2, 0xe

    #@d81
    move/from16 v0, v27

    #@d83
    if-eq v0, v2, :cond_d95

    #@d85
    move-object/from16 v0, p0

    #@d87
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@d89
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->apnSelectionHdlr:Lcom/android/internal/telephony/ApnSelectionHandler;

    #@d8b
    const-string v2, "lte-roaming.lguplus.co.kr"

    #@d8d
    move-object/from16 v0, v25

    #@d8f
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d92
    move-result v2

    #@d93
    if-eqz v2, :cond_dd4

    #@d95
    .line 1421
    :cond_d95
    new-instance v57, Landroid/content/Intent;

    #@d97
    const-string v2, "lge.intent.action.LTE_ROAMING_DATA_CONNECTION_LGU"

    #@d99
    move-object/from16 v0, v57

    #@d9b
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@d9e
    .line 1422
    .local v57, intentForRoaming:Landroid/content/Intent;
    const-string v2, "Connection_Status"

    #@da0
    move-object/from16 v0, p0

    #@da2
    iget-boolean v3, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->bConnectionStatus:Z

    #@da4
    move-object/from16 v0, v57

    #@da6
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@da9
    .line 1423
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@dab
    new-instance v3, Ljava/lang/StringBuilder;

    #@dad
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@db0
    const-string v4, "[LGE_DATA] send intent LTE_ROAMING_DATA_CONNECTION_LGU, Connection_Status="

    #@db2
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@db5
    move-result-object v3

    #@db6
    move-object/from16 v0, p0

    #@db8
    iget-boolean v4, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->bConnectionStatus:Z

    #@dba
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@dbd
    move-result-object v3

    #@dbe
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dc1
    move-result-object v3

    #@dc2
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@dc5
    .line 1431
    :goto_dc5
    move-object/from16 v0, p0

    #@dc7
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@dc9
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@dcc
    move-result-object v2

    #@dcd
    move-object/from16 v0, v57

    #@dcf
    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@dd2
    goto/16 :goto_21

    #@dd4
    .line 1426
    .end local v57           #intentForRoaming:Landroid/content/Intent;
    :cond_dd4
    new-instance v57, Landroid/content/Intent;

    #@dd6
    const-string v2, "lge.intent.action.GW_ROAMING_DATA_CONNECTION_LGU"

    #@dd8
    move-object/from16 v0, v57

    #@dda
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@ddd
    .line 1427
    .restart local v57       #intentForRoaming:Landroid/content/Intent;
    const-string v2, "Connection_Status"

    #@ddf
    move-object/from16 v0, p0

    #@de1
    iget-boolean v3, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->bConnectionStatus:Z

    #@de3
    move-object/from16 v0, v57

    #@de5
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@de8
    .line 1428
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@dea
    new-instance v3, Ljava/lang/StringBuilder;

    #@dec
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@def
    const-string v4, "[LGE_DATA] send intent GW_ROAMING_DATA_CONNECTION_LGU, Connection_Status="

    #@df1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@df4
    move-result-object v3

    #@df5
    move-object/from16 v0, p0

    #@df7
    iget-boolean v4, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->bConnectionStatus:Z

    #@df9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@dfc
    move-result-object v3

    #@dfd
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e00
    move-result-object v3

    #@e01
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e04
    goto :goto_dc5

    #@e05
    .line 1441
    .end local v25           #aApnOfApnContext:Ljava/lang/String;
    .end local v26           #aApnSettingOfApnContext:Lcom/android/internal/telephony/ApnSetting;
    .end local v27           #aRadioTech:I
    .end local v35           #apnStr:Ljava/lang/String;
    .end local v57           #intentForRoaming:Landroid/content/Intent;
    :cond_e05
    move-object/from16 v0, p0

    #@e07
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@e09
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@e0b
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@e0e
    move-result-object v2

    #@e0f
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_SELECT_KR:Z

    #@e11
    if-eqz v2, :cond_e41

    #@e13
    move-object/from16 v0, p0

    #@e15
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@e17
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@e19
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@e1c
    move-result-object v2

    #@e1d
    iget-object v2, v2, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@e1f
    const-string v3, "SKTBASE"

    #@e21
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e24
    move-result v2

    #@e25
    if-eqz v2, :cond_e41

    #@e27
    .line 1443
    const v2, 0x4202f

    #@e2a
    move-object/from16 v0, p0

    #@e2c
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->hasMessages(I)Z

    #@e2f
    move-result v2

    #@e30
    if-eqz v2, :cond_e41

    #@e32
    .line 1445
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@e34
    const-string v3, "[onDataSetupComplete] : Remove EVENT_SETDEFAULT_TOCHANGE_AFTER_DELAY"

    #@e36
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e39
    .line 1446
    const v2, 0x4202f

    #@e3c
    move-object/from16 v0, p0

    #@e3e
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->removeMessages(I)V

    #@e41
    .line 1452
    :cond_e41
    move-object/from16 v0, p0

    #@e43
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@e45
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@e47
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@e4a
    move-result-object v2

    #@e4b
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_GPRS_REJECTED_KT:Z

    #@e4d
    if-eqz v2, :cond_f2c

    #@e4f
    .line 1454
    move-object/from16 v0, p0

    #@e51
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@e53
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@e55
    const v3, 0x4202c

    #@e58
    move-object/from16 v0, p0

    #@e5a
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->obtainMessage(I)Landroid/os/Message;

    #@e5d
    move-result-object v3

    #@e5e
    invoke-interface {v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->getLastPdpFailCause(Landroid/os/Message;)V

    #@e61
    .line 1455
    move-object/from16 v0, p0

    #@e63
    iget-boolean v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->isGsm:Z

    #@e65
    if-eqz v2, :cond_f2c

    #@e67
    .line 1456
    move-object/from16 v0, p0

    #@e69
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@e6b
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@e6e
    move-result-object v2

    #@e6f
    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@e72
    move-result v64

    #@e73
    .line 1457
    .local v64, isRoamingValue:Z
    move-object/from16 v0, p0

    #@e75
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@e77
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@e7a
    move-result-object v2

    #@e7b
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@e7e
    move-result-object v2

    #@e7f
    const-string v3, "airplane_mode_on"

    #@e81
    const/4 v4, 0x0

    #@e82
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@e85
    move-result v28

    #@e86
    .line 1459
    .local v28, airplaneMode:I
    sget-object v42, Lcom/android/internal/telephony/DataConnection$FailCause;->UNKNOWN:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@e88
    .line 1460
    .local v42, cause:Lcom/android/internal/telephony/DataConnection$FailCause;
    move-object/from16 v0, v24

    #@e8a
    iget-object v0, v0, Lcom/android/internal/telephony/LGDataConnectionTrackerMsg;->cause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@e8c
    move-object/from16 v42, v0

    #@e8e
    .line 1461
    new-instance v2, Ljava/lang/StringBuilder;

    #@e90
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e93
    const-string v3, "("

    #@e95
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e98
    move-result-object v2

    #@e99
    invoke-virtual/range {v42 .. v42}, Lcom/android/internal/telephony/DataConnection$FailCause;->getErrorCode()I

    #@e9c
    move-result v3

    #@e9d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ea0
    move-result-object v2

    #@ea1
    const-string v3, ") "

    #@ea3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea6
    move-result-object v2

    #@ea7
    invoke-virtual/range {v42 .. v42}, Lcom/android/internal/telephony/DataConnection$FailCause;->toString()Ljava/lang/String;

    #@eaa
    move-result-object v3

    #@eab
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eae
    move-result-object v2

    #@eaf
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@eb2
    move-result-object v43

    #@eb3
    .line 1462
    .local v43, causeValue:Ljava/lang/String;
    if-eqz v31, :cond_f2c

    #@eb5
    .line 1463
    invoke-virtual/range {v31 .. v31}, Lcom/android/internal/telephony/ApnContext;->getApnType()Ljava/lang/String;

    #@eb8
    move-result-object v2

    #@eb9
    move-object/from16 v0, p0

    #@ebb
    iget-object v3, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@ebd
    const-string v3, "ims"

    #@ebf
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ec2
    move-result v2

    #@ec3
    if-nez v2, :cond_100a

    #@ec5
    invoke-virtual/range {v42 .. v42}, Lcom/android/internal/telephony/DataConnection$FailCause;->isPermanentFail()Z

    #@ec8
    move-result v2

    #@ec9
    if-eqz v2, :cond_100a

    #@ecb
    move-object/from16 v0, p0

    #@ecd
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@ecf
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@ed1
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@ed4
    move-result-object v2

    #@ed5
    iget-object v2, v2, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@ed7
    const-string v3, "KTBASE"

    #@ed9
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@edc
    move-result v2

    #@edd
    if-eqz v2, :cond_100a

    #@edf
    .line 1465
    move-object/from16 v0, p0

    #@ee1
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mGsst:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@ee3
    move-object/from16 v0, p0

    #@ee5
    iget-object v3, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mGsst:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@ee7
    const/16 v3, 0x3f0

    #@ee9
    invoke-virtual/range {v42 .. v42}, Lcom/android/internal/telephony/DataConnection$FailCause;->getErrorCode()I

    #@eec
    move-result v4

    #@eed
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setPdpRejectedNotification(II)V

    #@ef0
    .line 1468
    move-object/from16 v0, p0

    #@ef2
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@ef4
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@ef7
    move-result-object v2

    #@ef8
    const-string v3, "connectivity"

    #@efa
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@efd
    move-result-object v44

    #@efe
    check-cast v44, Landroid/net/ConnectivityManager;

    #@f00
    .line 1471
    .local v44, cm:Landroid/net/ConnectivityManager;
    move-object/from16 v0, p0

    #@f02
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@f04
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@f07
    move-result-object v2

    #@f08
    move-object/from16 v0, p0

    #@f0a
    iget-object v3, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@f0c
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@f0f
    move-result-object v3

    #@f10
    const v4, 0x1040599

    #@f13
    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@f16
    move-result-object v3

    #@f17
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@f1a
    move-result-object v3

    #@f1b
    const/4 v4, 0x1

    #@f1c
    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@f1f
    move-result-object v80

    #@f20
    .line 1472
    .local v80, toast:Landroid/widget/Toast;
    const/16 v2, 0x50

    #@f22
    const/4 v3, 0x0

    #@f23
    const/4 v4, 0x0

    #@f24
    move-object/from16 v0, v80

    #@f26
    invoke-virtual {v0, v2, v3, v4}, Landroid/widget/Toast;->setGravity(III)V

    #@f29
    .line 1473
    invoke-virtual/range {v80 .. v80}, Landroid/widget/Toast;->show()V

    #@f2c
    .line 1488
    .end local v28           #airplaneMode:I
    .end local v42           #cause:Lcom/android/internal/telephony/DataConnection$FailCause;
    .end local v43           #causeValue:Ljava/lang/String;
    .end local v44           #cm:Landroid/net/ConnectivityManager;
    .end local v64           #isRoamingValue:Z
    .end local v80           #toast:Landroid/widget/Toast;
    :cond_f2c
    :goto_f2c
    move-object/from16 v0, p0

    #@f2e
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@f30
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@f32
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@f35
    move-result-object v2

    #@f36
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_SELECT_KR:Z

    #@f38
    if-eqz v2, :cond_f68

    #@f3a
    .line 1490
    move-object/from16 v0, p0

    #@f3c
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@f3e
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@f40
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@f43
    move-result-object v2

    #@f44
    iget-object v2, v2, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@f46
    const-string v3, "SKTBASE"

    #@f48
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f4b
    move-result v2

    #@f4c
    if-eqz v2, :cond_f68

    #@f4e
    .line 1492
    move-object/from16 v0, p0

    #@f50
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@f52
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->apnSelectionHdlr:Lcom/android/internal/telephony/ApnSelectionHandler;

    #@f54
    move-object/from16 v0, p0

    #@f56
    iget-object v3, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@f58
    iget-object v3, v3, Lcom/android/internal/telephony/DataConnectionTracker;->apnSelectionHdlr:Lcom/android/internal/telephony/ApnSelectionHandler;

    #@f5a
    const-string v3, "Added_APN_failed"

    #@f5c
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/ApnSelectionHandler;->selectApn(Ljava/lang/String;)V

    #@f5f
    .line 1494
    move-object/from16 v0, p0

    #@f61
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@f63
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->apnSelectionHdlr:Lcom/android/internal/telephony/ApnSelectionHandler;

    #@f65
    const/4 v3, 0x1

    #@f66
    iput-boolean v3, v2, Lcom/android/internal/telephony/ApnSelectionHandler;->APN_FAIL_Flag:Z

    #@f68
    .line 1498
    :cond_f68
    invoke-virtual/range {v31 .. v31}, Lcom/android/internal/telephony/ApnContext;->getWaitingApns()Ljava/util/ArrayList;

    #@f6b
    move-result-object v2

    #@f6c
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    #@f6f
    move-result v2

    #@f70
    if-eqz v2, :cond_21

    #@f72
    .line 1499
    invoke-virtual/range {v31 .. v31}, Lcom/android/internal/telephony/ApnContext;->getWaitingApnsPermFailCount()I

    #@f75
    move-result v2

    #@f76
    if-nez v2, :cond_1076

    #@f78
    .line 1501
    move-object/from16 v0, p0

    #@f7a
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@f7c
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@f7e
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@f81
    move-result-object v2

    #@f82
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_LTE_ROAMING_LGU:Z

    #@f84
    if-eqz v2, :cond_21

    #@f86
    .line 1502
    move-object/from16 v0, p0

    #@f88
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@f8a
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@f8d
    move-result-object v2

    #@f8e
    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@f91
    move-result v2

    #@f92
    if-eqz v2, :cond_21

    #@f94
    .line 1503
    move-object/from16 v0, p0

    #@f96
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@f98
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@f9b
    move-result-object v2

    #@f9c
    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@f9f
    move-result v27

    #@fa0
    .line 1504
    .restart local v27       #aRadioTech:I
    const/4 v2, 0x0

    #@fa1
    move-object/from16 v0, p0

    #@fa3
    iput-boolean v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->bConnectionStatus:Z

    #@fa5
    .line 1507
    invoke-virtual/range {v31 .. v31}, Lcom/android/internal/telephony/ApnContext;->getApnSetting()Lcom/android/internal/telephony/DataProfile;

    #@fa8
    move-result-object v26

    #@fa9
    check-cast v26, Lcom/android/internal/telephony/ApnSetting;

    #@fab
    .line 1508
    .restart local v26       #aApnSettingOfApnContext:Lcom/android/internal/telephony/ApnSetting;
    const-string v25, ""

    #@fad
    .line 1510
    .restart local v25       #aApnOfApnContext:Ljava/lang/String;
    if-eqz v26, :cond_fb5

    #@faf
    move-object/from16 v0, v26

    #@fb1
    iget-object v0, v0, Lcom/android/internal/telephony/DataProfile;->apn:Ljava/lang/String;

    #@fb3
    move-object/from16 v25, v0

    #@fb5
    .line 1514
    :cond_fb5
    const/16 v2, 0xe

    #@fb7
    move/from16 v0, v27

    #@fb9
    if-eq v0, v2, :cond_fcb

    #@fbb
    move-object/from16 v0, p0

    #@fbd
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@fbf
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->apnSelectionHdlr:Lcom/android/internal/telephony/ApnSelectionHandler;

    #@fc1
    const-string v2, "lte-roaming.lguplus.co.kr"

    #@fc3
    move-object/from16 v0, v25

    #@fc5
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@fc8
    move-result v2

    #@fc9
    if-eqz v2, :cond_1045

    #@fcb
    .line 1515
    :cond_fcb
    new-instance v57, Landroid/content/Intent;

    #@fcd
    const-string v2, "lge.intent.action.LTE_ROAMING_DATA_CONNECTION_LGU"

    #@fcf
    move-object/from16 v0, v57

    #@fd1
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@fd4
    .line 1516
    .restart local v57       #intentForRoaming:Landroid/content/Intent;
    const-string v2, "Connection_Status"

    #@fd6
    move-object/from16 v0, p0

    #@fd8
    iget-boolean v3, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->bConnectionStatus:Z

    #@fda
    move-object/from16 v0, v57

    #@fdc
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@fdf
    .line 1517
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@fe1
    new-instance v3, Ljava/lang/StringBuilder;

    #@fe3
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@fe6
    const-string v4, "[LGE_DATA] send intent LTE_ROAMING_DATA_CONNECTION_LGU, Connection_Status="

    #@fe8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@feb
    move-result-object v3

    #@fec
    move-object/from16 v0, p0

    #@fee
    iget-boolean v4, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->bConnectionStatus:Z

    #@ff0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@ff3
    move-result-object v3

    #@ff4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ff7
    move-result-object v3

    #@ff8
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ffb
    .line 1525
    :goto_ffb
    move-object/from16 v0, p0

    #@ffd
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@fff
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@1002
    move-result-object v2

    #@1003
    move-object/from16 v0, v57

    #@1005
    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@1008
    goto/16 :goto_21

    #@100a
    .line 1474
    .end local v25           #aApnOfApnContext:Ljava/lang/String;
    .end local v26           #aApnSettingOfApnContext:Lcom/android/internal/telephony/ApnSetting;
    .end local v27           #aRadioTech:I
    .end local v57           #intentForRoaming:Landroid/content/Intent;
    .restart local v28       #airplaneMode:I
    .restart local v42       #cause:Lcom/android/internal/telephony/DataConnection$FailCause;
    .restart local v43       #causeValue:Ljava/lang/String;
    .restart local v64       #isRoamingValue:Z
    :cond_100a
    if-eqz v64, :cond_f2c

    #@100c
    const/4 v2, 0x1

    #@100d
    move/from16 v0, v28

    #@100f
    if-eq v0, v2, :cond_f2c

    #@1011
    move-object/from16 v0, p0

    #@1013
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1015
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1017
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@101a
    move-result-object v2

    #@101b
    iget-object v2, v2, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@101d
    const-string v3, "LGTBASE"

    #@101f
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1022
    move-result v2

    #@1023
    if-eqz v2, :cond_f2c

    #@1025
    .line 1476
    invoke-virtual/range {v31 .. v31}, Lcom/android/internal/telephony/ApnContext;->isEnabled()Z

    #@1028
    move-result v2

    #@1029
    if-eqz v2, :cond_f2c

    #@102b
    .line 1477
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@102d
    const-string v3, "[MIN]setNorification!!"

    #@102f
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1032
    .line 1478
    move-object/from16 v0, p0

    #@1034
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mGsst:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@1036
    move-object/from16 v0, p0

    #@1038
    iget-object v3, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mGsst:Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;

    #@103a
    const/16 v3, 0x3f0

    #@103c
    invoke-virtual/range {v42 .. v42}, Lcom/android/internal/telephony/DataConnection$FailCause;->getErrorCode()I

    #@103f
    move-result v4

    #@1040
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/telephony/gsm/GsmServiceStateTracker;->setPdpRejectedNotification(II)V

    #@1043
    goto/16 :goto_f2c

    #@1045
    .line 1520
    .end local v28           #airplaneMode:I
    .end local v42           #cause:Lcom/android/internal/telephony/DataConnection$FailCause;
    .end local v43           #causeValue:Ljava/lang/String;
    .end local v64           #isRoamingValue:Z
    .restart local v25       #aApnOfApnContext:Ljava/lang/String;
    .restart local v26       #aApnSettingOfApnContext:Lcom/android/internal/telephony/ApnSetting;
    .restart local v27       #aRadioTech:I
    :cond_1045
    new-instance v57, Landroid/content/Intent;

    #@1047
    const-string v2, "lge.intent.action.GW_ROAMING_DATA_CONNECTION_LGU"

    #@1049
    move-object/from16 v0, v57

    #@104b
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@104e
    .line 1521
    .restart local v57       #intentForRoaming:Landroid/content/Intent;
    const-string v2, "Connection_Status"

    #@1050
    move-object/from16 v0, p0

    #@1052
    iget-boolean v3, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->bConnectionStatus:Z

    #@1054
    move-object/from16 v0, v57

    #@1056
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@1059
    .line 1522
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@105b
    new-instance v3, Ljava/lang/StringBuilder;

    #@105d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1060
    const-string v4, "[LGE_DATA] send intent GW_ROAMING_DATA_CONNECTION_LGU, Connection_Status="

    #@1062
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1065
    move-result-object v3

    #@1066
    move-object/from16 v0, p0

    #@1068
    iget-boolean v4, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->bConnectionStatus:Z

    #@106a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@106d
    move-result-object v3

    #@106e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1071
    move-result-object v3

    #@1072
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1075
    goto :goto_ffb

    #@1076
    .line 1533
    .end local v25           #aApnOfApnContext:Ljava/lang/String;
    .end local v26           #aApnSettingOfApnContext:Lcom/android/internal/telephony/ApnSetting;
    .end local v27           #aRadioTech:I
    .end local v57           #intentForRoaming:Landroid/content/Intent;
    :cond_1076
    move-object/from16 v0, p0

    #@1078
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@107a
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@107c
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@107f
    move-result-object v2

    #@1080
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MMS_ON_DATA_DISABLED_SKT:Z

    #@1082
    if-eqz v2, :cond_21

    #@1084
    .line 1535
    invoke-virtual/range {v31 .. v31}, Lcom/android/internal/telephony/ApnContext;->getApnSetting()Lcom/android/internal/telephony/DataProfile;

    #@1087
    move-result-object v6

    #@1088
    check-cast v6, Lcom/android/internal/telephony/ApnSetting;

    #@108a
    .line 1537
    .restart local v6       #apn:Lcom/android/internal/telephony/ApnSetting;
    invoke-virtual/range {v31 .. v31}, Lcom/android/internal/telephony/ApnContext;->isEnabled()Z

    #@108d
    move-result v2

    #@108e
    if-eqz v2, :cond_21

    #@1090
    invoke-virtual/range {v31 .. v31}, Lcom/android/internal/telephony/ApnContext;->getApnType()Ljava/lang/String;

    #@1093
    move-result-object v2

    #@1094
    const-string v3, "mms"

    #@1096
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@1099
    move-result v2

    #@109a
    if-eqz v2, :cond_21

    #@109c
    move-object/from16 v0, p0

    #@109e
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@10a0
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@10a3
    move-result-object v2

    #@10a4
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@10a7
    move-result-object v2

    #@10a8
    const-string v3, "mobile_data"

    #@10aa
    const/4 v4, 0x1

    #@10ab
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@10ae
    move-result v2

    #@10af
    if-nez v2, :cond_21

    #@10b1
    .line 1540
    move-object/from16 v0, p0

    #@10b3
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->original_apn_formms:Ljava/lang/String;

    #@10b5
    iput-object v2, v6, Lcom/android/internal/telephony/DataProfile;->apn:Ljava/lang/String;

    #@10b7
    .line 1542
    const/4 v2, 0x0

    #@10b8
    move-object/from16 v0, p0

    #@10ba
    iput-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->original_apn_formms:Ljava/lang/String;

    #@10bc
    goto/16 :goto_21

    #@10be
    .line 1555
    .end local v6           #apn:Lcom/android/internal/telephony/ApnSetting;
    .end local v31           #apnContext:Lcom/android/internal/telephony/ApnContext;
    .end local v36           #apnType:Ljava/lang/String;
    .end local v38           #apn_info:Lcom/android/internal/telephony/ApnSetting;
    .end local v65           #issucess:Z
    :sswitch_10be
    move-object/from16 v0, v24

    #@10c0
    iget-object v0, v0, Lcom/android/internal/telephony/LGDataConnectionTrackerMsg;->type:Ljava/lang/String;

    #@10c2
    move-object/from16 v37, v0

    #@10c4
    .line 1556
    .local v37, apnType_done:Ljava/lang/String;
    move-object/from16 v0, p0

    #@10c6
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@10c8
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->mApnContexts:Ljava/util/concurrent/ConcurrentHashMap;

    #@10ca
    move-object/from16 v0, v37

    #@10cc
    invoke-virtual {v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@10cf
    move-result-object v33

    #@10d0
    check-cast v33, Lcom/android/internal/telephony/ApnContext;

    #@10d2
    .line 1559
    .local v33, apnContext_done:Lcom/android/internal/telephony/ApnContext;
    if-eqz v33, :cond_21

    #@10d4
    .line 1561
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@10d6
    new-instance v3, Ljava/lang/StringBuilder;

    #@10d8
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10db
    const-string v4, "[LGE_DATA] EVENT_DISCONNECT_DONE = type : "

    #@10dd
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e0
    move-result-object v3

    #@10e1
    move-object/from16 v0, v37

    #@10e3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e6
    move-result-object v3

    #@10e7
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10ea
    move-result-object v3

    #@10eb
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10ee
    .line 1562
    invoke-virtual/range {v33 .. v33}, Lcom/android/internal/telephony/ApnContext;->isReady()Z

    #@10f1
    move-result v2

    #@10f2
    if-eqz v2, :cond_1166

    #@10f4
    invoke-virtual/range {v33 .. v33}, Lcom/android/internal/telephony/ApnContext;->getReason()Ljava/lang/String;

    #@10f7
    move-result-object v2

    #@10f8
    move-object/from16 v0, p0

    #@10fa
    invoke-direct {v0, v2}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->retryAfterDisconnected(Ljava/lang/String;)Z

    #@10fd
    move-result v2

    #@10fe
    if-eqz v2, :cond_1166

    #@1100
    .line 1576
    :cond_1100
    :goto_1100
    move-object/from16 v0, p0

    #@1102
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1104
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1106
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1109
    move-result-object v2

    #@110a
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_VOICE_PROTECTION_KR:Z

    #@110c
    const/4 v3, 0x1

    #@110d
    if-ne v2, v3, :cond_1125

    #@110f
    .line 1579
    invoke-virtual/range {v33 .. v33}, Lcom/android/internal/telephony/ApnContext;->getApnType()Ljava/lang/String;

    #@1112
    move-result-object v2

    #@1113
    const-string v3, "default"

    #@1115
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@1118
    move-result v2

    #@1119
    if-eqz v2, :cond_1125

    #@111b
    .line 1580
    move-object/from16 v0, p0

    #@111d
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@111f
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@1121
    const/4 v3, 0x0

    #@1122
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/DataConnectionManager;->functionForPacketDrop(Z)V

    #@1125
    .line 1586
    :cond_1125
    move-object/from16 v0, p0

    #@1127
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1129
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@112b
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@112e
    move-result-object v2

    #@112f
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_KR:Z

    #@1131
    if-eqz v2, :cond_21

    #@1133
    .line 1587
    invoke-virtual/range {v33 .. v33}, Lcom/android/internal/telephony/ApnContext;->getApnType()Ljava/lang/String;

    #@1136
    move-result-object v2

    #@1137
    const-string v3, "default"

    #@1139
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@113c
    move-result v2

    #@113d
    if-eqz v2, :cond_21

    #@113f
    .line 1588
    move-object/from16 v0, p0

    #@1141
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@1143
    const/4 v3, 0x0

    #@1144
    iput-boolean v3, v2, Lcom/android/internal/telephony/DataConnectionTracker;->internetPDNconnected:Z

    #@1146
    .line 1589
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@1148
    new-instance v3, Ljava/lang/StringBuilder;

    #@114a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@114d
    const-string v4, "[LGE_DATA] Default PDN Disonnected, internetPDNconnected = "

    #@114f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1152
    move-result-object v3

    #@1153
    move-object/from16 v0, p0

    #@1155
    iget-object v4, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@1157
    iget-boolean v4, v4, Lcom/android/internal/telephony/DataConnectionTracker;->internetPDNconnected:Z

    #@1159
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@115c
    move-result-object v3

    #@115d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1160
    move-result-object v3

    #@1161
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1164
    goto/16 :goto_21

    #@1166
    .line 1566
    :cond_1166
    const-string v2, "1"

    #@1168
    const-string v3, "sys.boot_completed"

    #@116a
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@116d
    move-result-object v3

    #@116e
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1171
    move-result v61

    #@1172
    .line 1567
    .local v61, isBootCompleted:Z
    const-string v2, "KTBASE"

    #@1174
    const-string v3, "ro.afwdata.LGfeatureset"

    #@1176
    const-string v4, "none"

    #@1178
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@117b
    move-result-object v3

    #@117c
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@117f
    move-result v62

    #@1180
    .line 1568
    .local v62, isKTBASE:Z
    if-eqz v61, :cond_1100

    #@1182
    if-eqz v62, :cond_1100

    #@1184
    .line 1569
    new-instance v58, Landroid/content/Intent;

    #@1186
    const-string v2, "com.kt.CALL_PROTECTION_MENU_ON"

    #@1188
    move-object/from16 v0, v58

    #@118a
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@118d
    .line 1570
    .local v58, intent_s:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@118f
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1191
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@1194
    move-result-object v2

    #@1195
    move-object/from16 v0, v58

    #@1197
    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@119a
    goto/16 :goto_1100

    #@119c
    .line 1611
    .end local v33           #apnContext_done:Lcom/android/internal/telephony/ApnContext;
    .end local v37           #apnType_done:Ljava/lang/String;
    .end local v58           #intent_s:Landroid/content/Intent;
    .end local v61           #isBootCompleted:Z
    .end local v62           #isKTBASE:Z
    :sswitch_119c
    move-object/from16 v0, p0

    #@119e
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@11a0
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@11a3
    move-result-object v2

    #@11a4
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@11a7
    move-result-object v2

    #@11a8
    const-string v3, "mobile_data"

    #@11aa
    const/4 v4, 0x1

    #@11ab
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@11ae
    move-result v2

    #@11af
    const/4 v3, 0x1

    #@11b0
    if-ne v2, v3, :cond_12b7

    #@11b2
    const/16 v52, 0x1

    #@11b4
    .line 1612
    .local v52, enabled:Z
    :goto_11b4
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@11b6
    new-instance v3, Ljava/lang/StringBuilder;

    #@11b8
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11bb
    const-string v4, "[LGE_DATA] CMD_SET_USER_DATA_ENABLE = "

    #@11bd
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11c0
    move-result-object v3

    #@11c1
    move/from16 v0, v52

    #@11c3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@11c6
    move-result-object v3

    #@11c7
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11ca
    move-result-object v3

    #@11cb
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11ce
    .line 1614
    move-object/from16 v0, p0

    #@11d0
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@11d2
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@11d4
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@11d7
    move-result-object v2

    #@11d8
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_VOICE_PROTECTION_KR:Z

    #@11da
    if-eqz v2, :cond_11e8

    #@11dc
    .line 1616
    if-nez v52, :cond_11e8

    #@11de
    .line 1617
    move-object/from16 v0, p0

    #@11e0
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@11e2
    iget-object v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@11e4
    const/4 v3, 0x0

    #@11e5
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/DataConnectionManager;->functionForPacketDrop(Z)V

    #@11e8
    .line 1621
    :cond_11e8
    move-object/from16 v0, p0

    #@11ea
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@11ec
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@11ef
    move-result-object v2

    #@11f0
    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@11f3
    move-result v77

    #@11f4
    .line 1622
    .local v77, roaming:Z
    move-object/from16 v0, p0

    #@11f6
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@11f8
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@11fa
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@11fd
    move-result-object v2

    #@11fe
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_KR:Z

    #@1200
    if-eqz v2, :cond_12bb

    #@1202
    if-eqz v77, :cond_12bb

    #@1204
    move-object/from16 v0, p0

    #@1206
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1208
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@120a
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@120d
    move-result-object v2

    #@120e
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_LTE_ROAMING_KT:Z

    #@1210
    if-eqz v2, :cond_12bb

    #@1212
    .line 1625
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@1214
    new-instance v3, Ljava/lang/StringBuilder;

    #@1216
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1219
    const-string v4, "[LGE_DATA] taegyu KT LTE Roaming roaming = "

    #@121b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@121e
    move-result-object v3

    #@121f
    move/from16 v0, v77

    #@1221
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1224
    move-result-object v3

    #@1225
    const-string v4, "supprot ModeChange For POAB"

    #@1227
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@122a
    move-result-object v3

    #@122b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@122e
    move-result-object v3

    #@122f
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1232
    .line 1626
    move-object/from16 v0, p0

    #@1234
    move/from16 v1, v52

    #@1236
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->changePreferrredNetworkMode(Z)V

    #@1239
    .line 1635
    :cond_1239
    :goto_1239
    move-object/from16 v0, p0

    #@123b
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@123d
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@1240
    move-result-object v2

    #@1241
    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@1244
    move-result v2

    #@1245
    if-nez v2, :cond_21

    #@1247
    move-object/from16 v0, p0

    #@1249
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@124b
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@124d
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1250
    move-result-object v2

    #@1251
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_NOTI_USERDATADISABLE_KR:Z

    #@1253
    const/4 v3, 0x1

    #@1254
    if-ne v2, v3, :cond_21

    #@1256
    .line 1636
    move-object/from16 v0, p0

    #@1258
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@125a
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@125d
    move-result-object v2

    #@125e
    const-string v3, "notification"

    #@1260
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1263
    move-result-object v72

    #@1264
    check-cast v72, Landroid/app/NotificationManager;

    #@1266
    .line 1638
    .restart local v72       #notificationManager:Landroid/app/NotificationManager;
    move-object/from16 v0, p0

    #@1268
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@126a
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@126d
    move-result-object v2

    #@126e
    const v3, 0x209030c

    #@1271
    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@1274
    move-result-object v50

    #@1275
    .line 1639
    .restart local v50       #details:Ljava/lang/CharSequence;
    move-object/from16 v0, p0

    #@1277
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1279
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@127c
    move-result-object v2

    #@127d
    const v3, 0x209030b

    #@1280
    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@1283
    move-result-object v79

    #@1284
    .line 1640
    .restart local v79       #title:Ljava/lang/CharSequence;
    move-object/from16 v0, p0

    #@1286
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mNotification:Landroid/app/Notification;

    #@1288
    move-object/from16 v0, v79

    #@128a
    iput-object v0, v2, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@128c
    .line 1642
    move-object/from16 v0, p0

    #@128e
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mNotification:Landroid/app/Notification;

    #@1290
    move-object/from16 v0, p0

    #@1292
    iget-object v3, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1294
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@1297
    move-result-object v3

    #@1298
    move-object/from16 v0, p0

    #@129a
    iget-object v4, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mNotification:Landroid/app/Notification;

    #@129c
    iget-object v4, v4, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    #@129e
    move-object/from16 v0, v79

    #@12a0
    move-object/from16 v1, v50

    #@12a2
    invoke-virtual {v2, v3, v0, v1, v4}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@12a5
    .line 1645
    if-eqz v52, :cond_12d2

    #@12a7
    .line 1646
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@12a9
    const-string v3, "[CMD_SET_USER_DATA_ENABLE]clean Notification"

    #@12ab
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12ae
    .line 1647
    const/16 v2, 0x9f6

    #@12b0
    move-object/from16 v0, v72

    #@12b2
    invoke-virtual {v0, v2}, Landroid/app/NotificationManager;->cancel(I)V

    #@12b5
    goto/16 :goto_21

    #@12b7
    .line 1611
    .end local v50           #details:Ljava/lang/CharSequence;
    .end local v52           #enabled:Z
    .end local v72           #notificationManager:Landroid/app/NotificationManager;
    .end local v77           #roaming:Z
    .end local v79           #title:Ljava/lang/CharSequence;
    :cond_12b7
    const/16 v52, 0x0

    #@12b9
    goto/16 :goto_11b4

    #@12bb
    .line 1630
    .restart local v52       #enabled:Z
    .restart local v77       #roaming:Z
    :cond_12bb
    move-object/from16 v0, p0

    #@12bd
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@12bf
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@12c1
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@12c4
    move-result-object v2

    #@12c5
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_KR:Z

    #@12c7
    if-nez v2, :cond_1239

    #@12c9
    .line 1631
    move-object/from16 v0, p0

    #@12cb
    move/from16 v1, v52

    #@12cd
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->changePreferrredNetworkMode(Z)V

    #@12d0
    goto/16 :goto_1239

    #@12d2
    .line 1649
    .restart local v50       #details:Ljava/lang/CharSequence;
    .restart local v72       #notificationManager:Landroid/app/NotificationManager;
    .restart local v79       #title:Ljava/lang/CharSequence;
    :cond_12d2
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@12d4
    const-string v3, "[CMD_SET_USER_DATA_ENABLE]setNotification: put notification"

    #@12d6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12d9
    .line 1650
    const/16 v2, 0x9f6

    #@12db
    move-object/from16 v0, p0

    #@12dd
    iget-object v3, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mNotification:Landroid/app/Notification;

    #@12df
    move-object/from16 v0, v72

    #@12e1
    invoke-virtual {v0, v2, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@12e4
    goto/16 :goto_21

    #@12e6
    .line 1667
    .end local v50           #details:Ljava/lang/CharSequence;
    .end local v52           #enabled:Z
    .end local v72           #notificationManager:Landroid/app/NotificationManager;
    .end local v77           #roaming:Z
    .end local v79           #title:Ljava/lang/CharSequence;
    :sswitch_12e6
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@12e8
    const-string v3, "[LGE_DATA] EVENT_APN_CHANGED = "

    #@12ea
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12ed
    .line 1670
    move-object/from16 v0, p0

    #@12ef
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@12f1
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@12f3
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@12f6
    move-result-object v2

    #@12f7
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_SELECT_KR:Z

    #@12f9
    if-eqz v2, :cond_132f

    #@12fb
    move-object/from16 v0, p0

    #@12fd
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@12ff
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@1302
    move-result-object v2

    #@1303
    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@1306
    move-result v2

    #@1307
    const/16 v3, 0xe

    #@1309
    if-ne v2, v3, :cond_132f

    #@130b
    move-object/from16 v0, p0

    #@130d
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@130f
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1311
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1314
    move-result-object v2

    #@1315
    iget-object v2, v2, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@1317
    const-string v3, "SKTBASE"

    #@1319
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@131c
    move-result v2

    #@131d
    if-eqz v2, :cond_132f

    #@131f
    .line 1674
    const v2, 0x4202f

    #@1322
    move-object/from16 v0, p0

    #@1324
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->obtainMessage(I)Landroid/os/Message;

    #@1327
    move-result-object v2

    #@1328
    const-wide/16 v3, 0x2ee0

    #@132a
    move-object/from16 v0, p0

    #@132c
    invoke-virtual {v0, v2, v3, v4}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@132f
    .line 1678
    :cond_132f
    move-object/from16 v0, p0

    #@1331
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@1333
    iget-boolean v2, v2, Lcom/android/internal/telephony/DataConnectionTracker;->hasProfileDbChanged:Z

    #@1335
    if-eqz v2, :cond_21

    #@1337
    .line 1679
    move-object/from16 v0, p0

    #@1339
    iget-object v2, v0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@133b
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@133e
    move-result-object v2

    #@133f
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1342
    move-result-object v2

    #@1343
    const-string v3, "SENDPDNTABLE_ENABLE_SAVE"

    #@1345
    const/4 v4, 0x0

    #@1346
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@1349
    move-result v2

    #@134a
    if-nez v2, :cond_1358

    #@134c
    .line 1680
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@134e
    const-string v3, "NoDBSyncDBSync is Not Enable"

    #@1350
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1353
    .line 1681
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->sendPdnTable()V

    #@1356
    goto/16 :goto_21

    #@1358
    .line 1684
    :cond_1358
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@135a
    const-string v3, "NoDBSync is Enable"

    #@135c
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@135f
    goto/16 :goto_21

    #@1361
    .line 795
    nop

    #@1362
    :sswitch_data_1362
    .sparse-switch
        0x42001 -> :sswitch_3c
        0x42002 -> :sswitch_552
        0x42004 -> :sswitch_200
        0x42007 -> :sswitch_452
        0x42008 -> :sswitch_457
        0x42010 -> :sswitch_14c
        0x42021 -> :sswitch_546
        0x42028 -> :sswitch_683
        0x42029 -> :sswitch_688
        0x4202c -> :sswitch_525
        0x4202e -> :sswitch_539
        0x4202f -> :sswitch_4df
        0x42031 -> :sswitch_174
        0x42032 -> :sswitch_19c
        0x42034 -> :sswitch_1ab
        0x42035 -> :sswitch_49e
        0x4203d -> :sswitch_68d
        0x4203e -> :sswitch_516
    .end sparse-switch

    #@13ac
    .line 1019
    :array_13ac
    .array-data 0x1
        0x51t
        0x55t
        0x41t
        0x4ct
        0x43t
        0x4ft
        0x4dt
        0x4dt
        0x3t
        0x0t
        0x8t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    #@13b8
    .line 1144
    :sswitch_data_13b8
    .sparse-switch
        0x42000 -> :sswitch_6cc
        0x42003 -> :sswitch_82a
        0x4200b -> :sswitch_8e8
        0x4200c -> :sswitch_839
        0x4200d -> :sswitch_74d
        0x4200f -> :sswitch_10be
        0x42013 -> :sswitch_12e6
        0x4201e -> :sswitch_119c
    .end sparse-switch
.end method

.method public isLTEDataRoamingAvailable()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 2352
    iget-boolean v1, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->isAvailableLTEDataRoaming:Z

    #@3
    if-ne v1, v0, :cond_6

    #@5
    .line 2355
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public lgDatagetMainApnType(Lcom/android/internal/telephony/DataProfile;)Ljava/lang/String;
    .registers 9
    .parameter "dp"

    #@0
    .prologue
    .line 1976
    iget-object v4, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@4
    invoke-interface {v4}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@7
    move-result-object v4

    #@8
    iget-boolean v4, v4, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APNSYNC_KR:Z

    #@a
    if-eqz v4, :cond_6f

    #@c
    .line 1978
    const-string v4, "[LGE_DATA][LGEDCT] "

    #@e
    new-instance v5, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v6, "[LGE_DATA_APN] single pdn : "

    #@15
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v5

    #@19
    iget-boolean v6, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->is_single_apn_flow:Z

    #@1b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v5

    #@1f
    const-string v6, "  exist_ims_type_in_mpdn : "

    #@21
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v5

    #@25
    iget-boolean v6, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->exist_ims_type_in_mpdn:Z

    #@27
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v5

    #@2b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v5

    #@2f
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 1979
    invoke-virtual {p1}, Lcom/android/internal/telephony/DataProfile;->getServiceTypes()[Ljava/lang/String;

    #@35
    move-result-object v0

    #@36
    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    #@37
    .local v2, len$:I
    const/4 v1, 0x0

    #@38
    .local v1, i$:I
    :goto_38
    if-ge v1, v2, :cond_60

    #@3a
    aget-object v3, v0, v1

    #@3c
    .line 1980
    .local v3, type:Ljava/lang/String;
    const-string v4, "*"

    #@3e
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@41
    move-result v4

    #@42
    if-eqz v4, :cond_5d

    #@44
    .line 1981
    const-string v4, "[LGE_DATA][LGEDCT] "

    #@46
    const-string v5, "[LGE_DATA_APN] Used the single APN"

    #@48
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 1982
    iget-object v4, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@4d
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@4f
    invoke-interface {v4}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@52
    move-result-object v4

    #@53
    iget-boolean v4, v4, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_KR:Z

    #@55
    if-eqz v4, :cond_5a

    #@57
    .line 1983
    const-string v4, "ims"

    #@59
    .line 1996
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #i$:I
    .end local v2           #len$:I
    .end local v3           #type:Ljava/lang/String;
    :goto_59
    return-object v4

    #@5a
    .line 1985
    .restart local v0       #arr$:[Ljava/lang/String;
    .restart local v1       #i$:I
    .restart local v2       #len$:I
    .restart local v3       #type:Ljava/lang/String;
    :cond_5a
    const-string v4, "default"

    #@5c
    goto :goto_59

    #@5d
    .line 1979
    :cond_5d
    add-int/lit8 v1, v1, 0x1

    #@5f
    goto :goto_38

    #@60
    .line 1989
    .end local v3           #type:Ljava/lang/String;
    :cond_60
    const-string v4, "default"

    #@62
    invoke-virtual {p1, v4}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@65
    move-result v4

    #@66
    if-eqz v4, :cond_6f

    #@68
    iget-boolean v4, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->exist_ims_type_in_mpdn:Z

    #@6a
    if-nez v4, :cond_6f

    #@6c
    .line 1991
    const-string v4, "ims"

    #@6e
    goto :goto_59

    #@6f
    .line 1996
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #i$:I
    .end local v2           #len$:I
    :cond_6f
    iget-object v4, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@71
    invoke-virtual {v4, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->getMainApnType(Lcom/android/internal/telephony/DataProfile;)Ljava/lang/String;

    #@74
    move-result-object v4

    #@75
    goto :goto_59
.end method

.method protected pdpreject_causecode(Landroid/os/AsyncResult;)V
    .registers 12
    .parameter "ar"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 1764
    iget-object v6, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@3
    invoke-virtual {v6}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@6
    move-result-object v4

    #@7
    .line 1765
    .local v4, mContext:Landroid/content/Context;
    iget-object v6, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@9
    iget-object v6, v6, Lcom/android/internal/telephony/DataConnectionTracker;->mApnContexts:Ljava/util/concurrent/ConcurrentHashMap;

    #@b
    const-string v7, "default"

    #@d
    invoke-virtual {v6, v7}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    move-result-object v1

    #@11
    check-cast v1, Lcom/android/internal/telephony/ApnContext;

    #@13
    .line 1767
    .local v1, apnContext:Lcom/android/internal/telephony/ApnContext;
    const/4 v5, 0x0

    #@14
    .line 1768
    .local v5, rawPdpRejectCuase:I
    iget-object v6, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@16
    if-eqz v6, :cond_20

    #@18
    .line 1769
    iget-object v6, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@1a
    check-cast v6, [I

    #@1c
    check-cast v6, [I

    #@1e
    aget v5, v6, v8

    #@20
    .line 1771
    :cond_20
    iget-object v6, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@22
    invoke-virtual {v6}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@25
    move-result-object v6

    #@26
    invoke-virtual {v6}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@29
    move-result v3

    #@2a
    .line 1772
    .local v3, isRoaming:Z
    iget-object v6, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@2c
    invoke-virtual {v6}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@2f
    move-result-object v6

    #@30
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@33
    move-result-object v6

    #@34
    const-string v7, "airplane_mode_on"

    #@36
    invoke-static {v6, v7, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@39
    move-result v0

    #@3a
    .line 1773
    .local v0, airplaneMode:I
    new-instance v6, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v7, "("

    #@41
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v6

    #@45
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@48
    move-result-object v6

    #@49
    const-string v7, ") "

    #@4b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v6

    #@4f
    invoke-static {v5}, Lcom/android/internal/telephony/DataConnection$FailCause;->fromInt(I)Lcom/android/internal/telephony/DataConnection$FailCause;

    #@52
    move-result-object v7

    #@53
    invoke-virtual {v7}, Lcom/android/internal/telephony/DataConnection$FailCause;->toString()Ljava/lang/String;

    #@56
    move-result-object v7

    #@57
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v6

    #@5b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v2

    #@5f
    .line 1774
    .local v2, causeValue:Ljava/lang/String;
    const-string v6, "[LGE_DATA][LGEDCT] "

    #@61
    new-instance v7, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    const-string v8, "######## EVENT_DATA_ERROR_FAIL_CAUSE ("

    #@68
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v7

    #@6c
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v7

    #@70
    const-string v8, ")"

    #@72
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v7

    #@76
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v7

    #@7a
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7d
    .line 1776
    iget-object v6, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@7f
    iget-object v6, v6, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@81
    sget-object v7, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->debugFileWrite:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@83
    new-instance v8, Ljava/lang/StringBuilder;

    #@85
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@88
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v8

    #@8c
    const-string v9, ""

    #@8e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v8

    #@92
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@95
    move-result-object v8

    #@96
    const/4 v9, 0x4

    #@97
    invoke-virtual {v6, v7, v8, v9}, Lcom/android/internal/telephony/DataConnectionManager;->IntegrationAPI(Lcom/android/internal/telephony/DataConnectionManager$FunctionName;Ljava/lang/String;I)I

    #@9a
    .line 1779
    iget-object v6, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@9c
    iget-object v6, v6, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@9e
    invoke-interface {v6}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@a1
    move-result-object v6

    #@a2
    iget-object v6, v6, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@a4
    const-string v7, "LGTBASE"

    #@a6
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a9
    move-result v6

    #@aa
    if-eqz v6, :cond_ed

    #@ac
    iget-object v6, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@ae
    invoke-virtual {v6}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@b1
    move-result-object v6

    #@b2
    invoke-virtual {v6}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@b5
    move-result v6

    #@b6
    if-eqz v6, :cond_ed

    #@b8
    const/16 v6, 0x21

    #@ba
    if-ne v5, v6, :cond_ed

    #@bc
    iget-object v6, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@be
    iget-object v6, v6, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@c0
    invoke-interface {v6}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@c3
    move-result-object v6

    #@c4
    iget-boolean v6, v6, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_REJECT_INTENT_UPLUS:Z

    #@c6
    if-eqz v6, :cond_ed

    #@c8
    .line 1780
    const-string v6, "[LGE_DATA][LGEDCT] "

    #@ca
    new-instance v7, Ljava/lang/StringBuilder;

    #@cc
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@cf
    const-string v8, "[LGE_DATA][PDP_reject] EVENT_DATA_ERROR_FAIL_CAUSE ("

    #@d1
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v7

    #@d5
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v7

    #@d9
    const-string v8, ")"

    #@db
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v7

    #@df
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e2
    move-result-object v7

    #@e3
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e6
    .line 1781
    iget-object v6, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@e8
    iget-object v6, v6, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@ea
    invoke-virtual {v6, v5}, Lcom/android/internal/telephony/DataConnectionManager;->SendBroadcastPdpRejectCause(I)V

    #@ed
    .line 1785
    :cond_ed
    return-void
.end method

.method public sendPdnTable()V
    .registers 15

    #@0
    .prologue
    .line 2001
    const/4 v2, 0x0

    #@1
    .line 2002
    .local v2, defaultcount:I
    const/4 v0, 0x0

    #@2
    .line 2003
    .local v0, aleadysenddefault:Z
    const/4 v8, 0x0

    #@3
    .line 2005
    .local v8, pdnId:I
    const/4 v11, 0x6

    #@4
    new-array v6, v11, [Z

    #@6
    fill-array-data v6, :array_10c

    #@9
    .line 2007
    .local v6, isDBEx:[Z
    const/4 v11, 0x0

    #@a
    iput-boolean v11, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->is_single_apn_flow:Z

    #@c
    .line 2008
    const/4 v11, 0x0

    #@d
    iput-boolean v11, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->exist_ims_type_in_mpdn:Z

    #@f
    .line 2012
    const/4 v9, 0x0

    #@10
    .line 2015
    .local v9, tempOTAdp:Lcom/android/internal/telephony/DataProfile;
    iget-object v11, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@12
    iget-object v11, v11, Lcom/android/internal/telephony/DataConnectionTracker;->mAllApns:Ljava/util/ArrayList;

    #@14
    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@17
    move-result-object v5

    #@18
    .local v5, i$:Ljava/util/Iterator;
    :cond_18
    :goto_18
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@1b
    move-result v11

    #@1c
    if-eqz v11, :cond_4a

    #@1e
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@21
    move-result-object v4

    #@22
    check-cast v4, Lcom/android/internal/telephony/DataProfile;

    #@24
    .line 2016
    .local v4, dp:Lcom/android/internal/telephony/DataProfile;
    invoke-virtual {v4}, Lcom/android/internal/telephony/DataProfile;->getDataProfileType()Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@27
    move-result-object v11

    #@28
    sget-object v12, Lcom/android/internal/telephony/DataProfile$DataProfileType;->PROFILE_TYPE_APN:Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@2a
    if-ne v11, v12, :cond_36

    #@2c
    const-string v11, "default"

    #@2e
    invoke-virtual {v4, v11}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@31
    move-result v11

    #@32
    if-eqz v11, :cond_36

    #@34
    .line 2019
    add-int/lit8 v2, v2, 0x1

    #@36
    .line 2022
    :cond_36
    invoke-virtual {v4}, Lcom/android/internal/telephony/DataProfile;->getDataProfileType()Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@39
    move-result-object v11

    #@3a
    sget-object v12, Lcom/android/internal/telephony/DataProfile$DataProfileType;->PROFILE_TYPE_APN:Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@3c
    if-ne v11, v12, :cond_18

    #@3e
    const-string v11, "ims"

    #@40
    invoke-virtual {v4, v11}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@43
    move-result v11

    #@44
    if-eqz v11, :cond_18

    #@46
    .line 2025
    const/4 v11, 0x1

    #@47
    iput-boolean v11, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->exist_ims_type_in_mpdn:Z

    #@49
    goto :goto_18

    #@4a
    .line 2034
    .end local v4           #dp:Lcom/android/internal/telephony/DataProfile;
    :cond_4a
    iget-object v11, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@4c
    iget-object v11, v11, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@4e
    invoke-interface {v11}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@51
    move-result-object v11

    #@52
    iget-boolean v11, v11, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APNSYNC_KR:Z

    #@54
    if-eqz v11, :cond_73

    #@56
    .line 2036
    const/4 v11, 0x1

    #@57
    if-ne v2, v11, :cond_5b

    #@59
    add-int/lit8 v2, v2, 0x1

    #@5b
    .line 2037
    :cond_5b
    const-string v11, "[LGE_DATA][LGEDCT] "

    #@5d
    new-instance v12, Ljava/lang/StringBuilder;

    #@5f
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@62
    const-string v13, "[lge_data_sync]  defaultcount : "

    #@64
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v12

    #@68
    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v12

    #@6c
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v12

    #@70
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@73
    .line 2040
    :cond_73
    const/4 v11, 0x1

    #@74
    if-le v2, v11, :cond_b2

    #@76
    .line 2042
    iget-object v11, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@78
    invoke-virtual {v11}, Lcom/android/internal/telephony/DataConnectionTracker;->getPreferredApn()Lcom/android/internal/telephony/DataProfile;

    #@7b
    move-result-object v3

    #@7c
    .line 2043
    .local v3, defaultdp:Lcom/android/internal/telephony/DataProfile;
    if-eqz v3, :cond_b2

    #@7e
    .line 2046
    iget-object v11, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@80
    iget-object v11, v11, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@82
    invoke-interface {v11}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@85
    move-result-object v11

    #@86
    iget-boolean v11, v11, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APNSYNC_KR:Z

    #@88
    if-eqz v11, :cond_a9

    #@8a
    .line 2048
    invoke-virtual {v3}, Lcom/android/internal/telephony/DataProfile;->getServiceTypes()[Ljava/lang/String;

    #@8d
    move-result-object v1

    #@8e
    .local v1, arr$:[Ljava/lang/String;
    array-length v7, v1

    #@8f
    .local v7, len$:I
    const/4 v5, 0x0

    #@90
    .local v5, i$:I
    :goto_90
    if-ge v5, v7, :cond_a9

    #@92
    aget-object v10, v1, v5

    #@94
    .line 2049
    .local v10, type:Ljava/lang/String;
    const-string v11, "*"

    #@96
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@99
    move-result v11

    #@9a
    if-eqz v11, :cond_a6

    #@9c
    .line 2050
    const-string v11, "[LGE_DATA][LGEDCT] "

    #@9e
    const-string v12, "[LGE_DATA_APN] Used the single APN"

    #@a0
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a3
    .line 2051
    const/4 v11, 0x1

    #@a4
    iput-boolean v11, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->is_single_apn_flow:Z

    #@a6
    .line 2048
    :cond_a6
    add-int/lit8 v5, v5, 0x1

    #@a8
    goto :goto_90

    #@a9
    .line 2056
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v5           #i$:I
    .end local v7           #len$:I
    .end local v10           #type:Ljava/lang/String;
    :cond_a9
    const/4 v11, 0x0

    #@aa
    invoke-virtual {p0, v3, v11}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->dprilmsg(Lcom/android/internal/telephony/DataProfile;I)I

    #@ad
    move-result v8

    #@ae
    .line 2057
    const/4 v11, 0x1

    #@af
    aput-boolean v11, v6, v8

    #@b1
    .line 2058
    const/4 v0, 0x1

    #@b2
    .line 2062
    .end local v3           #defaultdp:Lcom/android/internal/telephony/DataProfile;
    :cond_b2
    iget-object v11, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mDct:Lcom/android/internal/telephony/DataConnectionTracker;

    #@b4
    iget-object v11, v11, Lcom/android/internal/telephony/DataConnectionTracker;->mAllApns:Ljava/util/ArrayList;

    #@b6
    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@b9
    move-result-object v5

    #@ba
    .local v5, i$:Ljava/util/Iterator;
    :cond_ba
    :goto_ba
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@bd
    move-result v11

    #@be
    if-eqz v11, :cond_ed

    #@c0
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@c3
    move-result-object v4

    #@c4
    check-cast v4, Lcom/android/internal/telephony/DataProfile;

    #@c6
    .line 2063
    .restart local v4       #dp:Lcom/android/internal/telephony/DataProfile;
    invoke-virtual {v4}, Lcom/android/internal/telephony/DataProfile;->getDataProfileType()Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@c9
    move-result-object v11

    #@ca
    sget-object v12, Lcom/android/internal/telephony/DataProfile$DataProfileType;->PROFILE_TYPE_APN:Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@cc
    if-eq v11, v12, :cond_d6

    #@ce
    .line 2064
    const-string v11, "[LGE_DATA][LGEDCT] "

    #@d0
    const-string v12, "SEND PDN TABLE:: Data Profile does not belong to 3GPP APN type..."

    #@d2
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d5
    goto :goto_ba

    #@d6
    .line 2068
    :cond_d6
    iget-object v11, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@d8
    iget-object v11, v11, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@da
    invoke-interface {v11}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@dd
    move-result-object v11

    #@de
    iget-boolean v11, v11, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APNSYNC_KR:Z

    #@e0
    if-eqz v11, :cond_ee

    #@e2
    iget-boolean v11, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->is_single_apn_flow:Z

    #@e4
    if-eqz v11, :cond_ee

    #@e6
    .line 2069
    const-string v11, "[LGE_DATA][LGEDCT] "

    #@e8
    const-string v12, "[LGE_DATA_APN] Used the single APN"

    #@ea
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ed
    .line 2088
    .end local v4           #dp:Lcom/android/internal/telephony/DataProfile;
    :cond_ed
    return-void

    #@ee
    .line 2073
    .restart local v4       #dp:Lcom/android/internal/telephony/DataProfile;
    :cond_ee
    const-string v11, "default"

    #@f0
    invoke-virtual {v4, v11}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@f3
    move-result v11

    #@f4
    if-eqz v11, :cond_102

    #@f6
    .line 2075
    if-nez v0, :cond_ba

    #@f8
    .line 2077
    const/4 v11, 0x0

    #@f9
    invoke-virtual {p0, v4, v11}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->dprilmsg(Lcom/android/internal/telephony/DataProfile;I)I

    #@fc
    move-result v8

    #@fd
    .line 2078
    const/4 v11, 0x1

    #@fe
    aput-boolean v11, v6, v8

    #@100
    .line 2079
    const/4 v0, 0x1

    #@101
    goto :goto_ba

    #@102
    .line 2084
    :cond_102
    const/4 v11, 0x0

    #@103
    invoke-virtual {p0, v4, v11}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->dprilmsg(Lcom/android/internal/telephony/DataProfile;I)I

    #@106
    move-result v8

    #@107
    .line 2085
    const/4 v11, 0x1

    #@108
    aput-boolean v11, v6, v8

    #@10a
    goto :goto_ba

    #@10b
    .line 2005
    nop

    #@10c
    :array_10c
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public setLTEDataRoamingEnable(Z)V
    .registers 6
    .parameter "enable"

    #@0
    .prologue
    .line 2369
    invoke-virtual {p0}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->getLTEDataRoamingEnable()Z

    #@3
    move-result v1

    #@4
    if-eq v1, p1, :cond_30

    #@6
    .line 2370
    const-string v1, "[LGE_DATA][LGEDCT] "

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "setLTEDataRoamingEnable, enable="

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 2371
    iget-object v1, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@20
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@27
    move-result-object v0

    #@28
    .line 2372
    .local v0, resolver:Landroid/content/ContentResolver;
    const-string v2, "data_lte_roaming"

    #@2a
    if-eqz p1, :cond_31

    #@2c
    const/4 v1, 0x1

    #@2d
    :goto_2d
    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@30
    .line 2381
    .end local v0           #resolver:Landroid/content/ContentResolver;
    :cond_30
    return-void

    #@31
    .line 2372
    .restart local v0       #resolver:Landroid/content/ContentResolver;
    :cond_31
    const/4 v1, 0x0

    #@32
    goto :goto_2d
.end method

.method public setLTEDataRoamingEnableNotApplyObserver(Z)V
    .registers 6
    .parameter "enable"

    #@0
    .prologue
    .line 2359
    const-string v1, "[LGE_DATA][LGEDCT] "

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "setLTEDataRoamingEnableNotApplyObserver, enable="

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 2360
    iget-object v1, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mLTEDataRoamingSettingObserver:Lcom/android/internal/telephony/LGEDataConnectionTracker$LTEDataRoamingSettingObserver;

    #@1a
    iget-object v2, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1c
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/LGEDataConnectionTracker$LTEDataRoamingSettingObserver;->unregister(Landroid/content/Context;)V

    #@23
    .line 2362
    iget-object v1, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@25
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2c
    move-result-object v0

    #@2d
    .line 2363
    .local v0, resolver:Landroid/content/ContentResolver;
    const-string v2, "data_lte_roaming"

    #@2f
    if-eqz p1, :cond_41

    #@31
    const/4 v1, 0x1

    #@32
    :goto_32
    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@35
    .line 2365
    iget-object v1, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mLTEDataRoamingSettingObserver:Lcom/android/internal/telephony/LGEDataConnectionTracker$LTEDataRoamingSettingObserver;

    #@37
    iget-object v2, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@39
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/LGEDataConnectionTracker$LTEDataRoamingSettingObserver;->register(Landroid/content/Context;)V

    #@40
    .line 2366
    return-void

    #@41
    .line 2363
    :cond_41
    const/4 v1, 0x0

    #@42
    goto :goto_32
.end method

.method protected updateIccAvailability()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1802
    iget-object v2, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 1825
    :cond_5
    :goto_5
    return-void

    #@6
    .line 1805
    :cond_6
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@8
    const-string v3, "updateIccAvailability 1"

    #@a
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 1807
    invoke-virtual {p0}, Lcom/android/internal/telephony/LGEDataConnectionTracker;->getUiccCardApplication()Lcom/android/internal/telephony/uicc/IccRecords;

    #@10
    move-result-object v0

    #@11
    .line 1808
    .local v0, newIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v0, :cond_5

    #@13
    .line 1811
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@15
    const-string v3, "updateIccAvailability 2"

    #@17
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 1812
    iget-object v2, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@1c
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@1f
    move-result-object v1

    #@20
    check-cast v1, Lcom/android/internal/telephony/uicc/IccRecords;

    #@22
    .line 1813
    .local v1, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eq v1, v0, :cond_5

    #@24
    .line 1814
    if-eqz v1, :cond_35

    #@26
    .line 1815
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@28
    const-string v3, "Removing stale icc objects."

    #@2a
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 1816
    invoke-virtual {v1, p0}, Lcom/android/internal/telephony/uicc/IccRecords;->unregisterForRecordsLoaded(Landroid/os/Handler;)V

    #@30
    .line 1817
    iget-object v2, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@32
    invoke-virtual {v2, v4}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    #@35
    .line 1819
    :cond_35
    if-eqz v0, :cond_5

    #@37
    .line 1820
    const-string v2, "[LGE_DATA][LGEDCT] "

    #@39
    const-string v3, "New records found"

    #@3b
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 1821
    iget-object v2, p0, Lcom/android/internal/telephony/LGEDataConnectionTracker;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@40
    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    #@43
    .line 1822
    const v2, 0x42002

    #@46
    invoke-virtual {v0, p0, v2, v4}, Lcom/android/internal/telephony/uicc/IccRecords;->registerForRecordsLoaded(Landroid/os/Handler;ILjava/lang/Object;)V

    #@49
    goto :goto_5
.end method
