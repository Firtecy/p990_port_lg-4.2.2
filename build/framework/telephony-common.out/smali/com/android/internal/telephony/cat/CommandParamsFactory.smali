.class public Lcom/android/internal/telephony/cat/CommandParamsFactory;
.super Landroid/os/Handler;
.source "CommandParamsFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/cat/CommandParamsFactory$1;
    }
.end annotation


# static fields
.field static final DTTZ_SETTING:I = 0x3

.field static final LANGUAGE_SETTING:I = 0x4

.field static final LOAD_MULTI_ICONS:I = 0x2

.field static final LOAD_NO_ICON:I = 0x0

.field static final LOAD_SINGLE_ICON:I = 0x1

.field private static final MAX_GSM7_DEFAULT_CHARS:I = 0xef

.field private static final MAX_UCS2_CHARS:I = 0x76

.field static final MSG_ID_LOAD_ICON_DONE:I = 0x1

.field static final REFRESH_NAA_INIT:I = 0x3

.field static final REFRESH_NAA_INIT_AND_FILE_CHANGE:I = 0x2

.field static final REFRESH_NAA_INIT_AND_FULL_FILE_CHANGE:I = 0x0

.field static final REFRESH_UICC_RESET:I = 0x4

.field private static sInstance:Lcom/android/internal/telephony/cat/CommandParamsFactory;


# instance fields
.field protected mCaller:Lcom/android/internal/telephony/cat/RilMessageDecoder;

.field private mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

.field private mIconLoadState:I

.field protected mIconLoader:Lcom/android/internal/telephony/cat/IconLoader;

.field private mloadIcon:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 40
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->sInstance:Lcom/android/internal/telephony/cat/CommandParamsFactory;

    #@3
    return-void
.end method

.method protected constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 97
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    .line 42
    iput-object v1, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

    #@7
    .line 43
    iput v0, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mIconLoadState:I

    #@9
    .line 44
    iput-object v1, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCaller:Lcom/android/internal/telephony/cat/RilMessageDecoder;

    #@b
    .line 45
    iput-boolean v0, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mloadIcon:Z

    #@d
    .line 98
    return-void
.end method

.method private constructor <init>(Lcom/android/internal/telephony/cat/RilMessageDecoder;Lcom/android/internal/telephony/uicc/IccFileHandler;)V
    .registers 5
    .parameter "caller"
    .parameter "fh"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 92
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    .line 42
    iput-object v1, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

    #@7
    .line 43
    iput v0, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mIconLoadState:I

    #@9
    .line 44
    iput-object v1, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCaller:Lcom/android/internal/telephony/cat/RilMessageDecoder;

    #@b
    .line 45
    iput-boolean v0, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mloadIcon:Z

    #@d
    .line 93
    iput-object p1, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCaller:Lcom/android/internal/telephony/cat/RilMessageDecoder;

    #@f
    .line 94
    invoke-static {p0, p2}, Lcom/android/internal/telephony/cat/IconLoader;->getInstance(Landroid/os/Handler;Lcom/android/internal/telephony/uicc/IccFileHandler;)Lcom/android/internal/telephony/cat/IconLoader;

    #@12
    move-result-object v0

    #@13
    iput-object v0, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mIconLoader:Lcom/android/internal/telephony/cat/IconLoader;

    #@15
    .line 95
    return-void
.end method

.method static declared-synchronized getInstance(Lcom/android/internal/telephony/cat/RilMessageDecoder;Lcom/android/internal/telephony/uicc/IccFileHandler;)Lcom/android/internal/telephony/cat/CommandParamsFactory;
    .registers 4
    .parameter "caller"
    .parameter "fh"

    #@0
    .prologue
    .line 83
    const-class v1, Lcom/android/internal/telephony/cat/CommandParamsFactory;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    sget-object v0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->sInstance:Lcom/android/internal/telephony/cat/CommandParamsFactory;

    #@5
    if-eqz v0, :cond_b

    #@7
    .line 84
    sget-object v0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->sInstance:Lcom/android/internal/telephony/cat/CommandParamsFactory;
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_13

    #@9
    .line 89
    :goto_9
    monitor-exit v1

    #@a
    return-object v0

    #@b
    .line 86
    :cond_b
    if-eqz p1, :cond_16

    #@d
    .line 87
    :try_start_d
    new-instance v0, Lcom/android/internal/telephony/cat/CommandParamsFactory;

    #@f
    invoke-direct {v0, p0, p1}, Lcom/android/internal/telephony/cat/CommandParamsFactory;-><init>(Lcom/android/internal/telephony/cat/RilMessageDecoder;Lcom/android/internal/telephony/uicc/IccFileHandler;)V
    :try_end_12
    .catchall {:try_start_d .. :try_end_12} :catchall_13

    #@12
    goto :goto_9

    #@13
    .line 83
    :catchall_13
    move-exception v0

    #@14
    monitor-exit v1

    #@15
    throw v0

    #@16
    .line 89
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_9
.end method

.method private processActivate(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z
    .registers 10
    .parameter "cmdDet"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/telephony/cat/CommandDetails;",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/cat/ComprehensionTlv;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cat/ResultException;
        }
    .end annotation

    #@0
    .prologue
    .line 1056
    .local p2, ctlvs:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/cat/ComprehensionTlv;>;"
    const-string v5, "process Activate"

    #@2
    invoke-static {p0, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@5
    .line 1058
    const/4 v0, 0x0

    #@6
    .line 1060
    .local v0, ActivateTarget:B
    sget-object v5, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->ACTIVATE_DESCRIPTOR:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@8
    invoke-direct {p0, v5, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@b
    move-result-object v1

    #@c
    .line 1062
    .local v1, ctlv:Lcom/android/internal/telephony/cat/ComprehensionTlv;
    if-eqz v1, :cond_93

    #@e
    .line 1063
    invoke-virtual {v1}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getLength()I

    #@11
    move-result v4

    #@12
    .line 1064
    .local v4, valueLen:I
    invoke-virtual {v1}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getValueIndex()I

    #@15
    move-result v3

    #@16
    .line 1065
    .local v3, valueIndex:I
    invoke-virtual {v1}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getRawValue()[B

    #@19
    move-result-object v2

    #@1a
    .line 1067
    .local v2, rawValue:[B
    new-instance v5, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v6, "Activate ACTIVATE_DESCRIPTOR (valueLen = "

    #@21
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v5

    #@25
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v5

    #@29
    const-string v6, ")"

    #@2b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v5

    #@2f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v5

    #@33
    invoke-static {p0, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@36
    .line 1068
    new-instance v5, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v6, "Activate ACTIVATE_DESCRIPTOR (valueIndex = "

    #@3d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v5

    #@41
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@44
    move-result-object v5

    #@45
    const-string v6, ")"

    #@47
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v5

    #@4b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v5

    #@4f
    invoke-static {p0, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@52
    .line 1070
    const/4 v5, 0x1

    #@53
    if-ne v4, v5, :cond_57

    #@55
    .line 1071
    aget-byte v0, v2, v3

    #@57
    .line 1074
    :cond_57
    packed-switch v0, :pswitch_data_a8

    #@5a
    .line 1081
    new-instance v5, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v6, "Activate (Target = Unknown ["

    #@61
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v5

    #@65
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@68
    move-result-object v5

    #@69
    const-string v6, "] )"

    #@6b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v5

    #@6f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v5

    #@73
    invoke-static {p0, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@76
    .line 1082
    new-instance v5, Lcom/android/internal/telephony/cat/CommandParams;

    #@78
    invoke-direct {v5, p1}, Lcom/android/internal/telephony/cat/CommandParams;-><init>(Lcom/android/internal/telephony/cat/CommandDetails;)V

    #@7b
    iput-object v5, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

    #@7d
    .line 1083
    new-instance v5, Lcom/android/internal/telephony/cat/ResultException;

    #@7f
    sget-object v6, Lcom/android/internal/telephony/cat/ResultCode;->BEYOND_TERMINAL_CAPABILITY:Lcom/android/internal/telephony/cat/ResultCode;

    #@81
    invoke-direct {v5, v6}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@84
    throw v5

    #@85
    .line 1076
    :pswitch_85
    const-string v5, "Activate (Target = CLF [0x01])"

    #@87
    invoke-static {p0, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@8a
    .line 1077
    new-instance v5, Lcom/android/internal/telephony/cat/CommandParams;

    #@8c
    invoke-direct {v5, p1}, Lcom/android/internal/telephony/cat/CommandParams;-><init>(Lcom/android/internal/telephony/cat/CommandDetails;)V

    #@8f
    iput-object v5, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

    #@91
    .line 1093
    const/4 v5, 0x0

    #@92
    return v5

    #@93
    .line 1088
    .end local v2           #rawValue:[B
    .end local v3           #valueIndex:I
    .end local v4           #valueLen:I
    :cond_93
    const-string v5, "Activate (No ACTIVATE_DESCRIPTOR)"

    #@95
    invoke-static {p0, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@98
    .line 1089
    new-instance v5, Lcom/android/internal/telephony/cat/CommandParams;

    #@9a
    invoke-direct {v5, p1}, Lcom/android/internal/telephony/cat/CommandParams;-><init>(Lcom/android/internal/telephony/cat/CommandDetails;)V

    #@9d
    iput-object v5, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

    #@9f
    .line 1090
    new-instance v5, Lcom/android/internal/telephony/cat/ResultException;

    #@a1
    sget-object v6, Lcom/android/internal/telephony/cat/ResultCode;->BEYOND_TERMINAL_CAPABILITY:Lcom/android/internal/telephony/cat/ResultCode;

    #@a3
    invoke-direct {v5, v6}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@a6
    throw v5

    #@a7
    .line 1074
    nop

    #@a8
    :pswitch_data_a8
    .packed-switch 0x1
        :pswitch_85
    .end packed-switch
.end method

.method private processBIPClient(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z
    .registers 12
    .parameter "cmdDet"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/telephony/cat/CommandDetails;",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/cat/ComprehensionTlv;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cat/ResultException;
        }
    .end annotation

    #@0
    .prologue
    .local p2, ctlvs:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/cat/ComprehensionTlv;>;"
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 1014
    iget v7, p1, Lcom/android/internal/telephony/cat/CommandDetails;->typeOfCommand:I

    #@4
    invoke-static {v7}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->fromInt(I)Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@7
    move-result-object v0

    #@8
    .line 1016
    .local v0, commandType:Lcom/android/internal/telephony/cat/AppInterface$CommandType;
    if-eqz v0, :cond_24

    #@a
    .line 1017
    new-instance v7, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v8, "process "

    #@11
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v7

    #@15
    invoke-virtual {v0}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->name()Ljava/lang/String;

    #@18
    move-result-object v8

    #@19
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v7

    #@1d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v7

    #@21
    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@24
    .line 1020
    :cond_24
    new-instance v4, Lcom/android/internal/telephony/cat/TextMessage;

    #@26
    invoke-direct {v4}, Lcom/android/internal/telephony/cat/TextMessage;-><init>()V

    #@29
    .line 1021
    .local v4, textMsg:Lcom/android/internal/telephony/cat/TextMessage;
    const/4 v3, 0x0

    #@2a
    .line 1022
    .local v3, iconId:Lcom/android/internal/telephony/cat/IconId;
    const/4 v1, 0x0

    #@2b
    .line 1023
    .local v1, ctlv:Lcom/android/internal/telephony/cat/ComprehensionTlv;
    const/4 v2, 0x0

    #@2c
    .line 1026
    .local v2, has_alpha_id:Z
    sget-object v7, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->ALPHA_ID:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@2e
    invoke-direct {p0, v7, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@31
    move-result-object v1

    #@32
    .line 1027
    if-eqz v1, :cond_53

    #@34
    .line 1028
    invoke-static {v1}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveAlphaId(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Ljava/lang/String;

    #@37
    move-result-object v7

    #@38
    iput-object v7, v4, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    #@3a
    .line 1029
    new-instance v7, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v8, "alpha TLV text="

    #@41
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v7

    #@45
    iget-object v8, v4, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    #@47
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v7

    #@4b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v7

    #@4f
    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@52
    .line 1030
    const/4 v2, 0x1

    #@53
    .line 1034
    :cond_53
    sget-object v7, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->ICON_ID:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@55
    invoke-direct {p0, v7, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@58
    move-result-object v1

    #@59
    .line 1035
    if-eqz v1, :cond_63

    #@5b
    .line 1036
    invoke-static {v1}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveIconId(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Lcom/android/internal/telephony/cat/IconId;

    #@5e
    move-result-object v3

    #@5f
    .line 1037
    iget-boolean v7, v3, Lcom/android/internal/telephony/cat/IconId;->selfExplanatory:Z

    #@61
    iput-boolean v7, v4, Lcom/android/internal/telephony/cat/TextMessage;->iconSelfExplanatory:Z

    #@63
    .line 1040
    :cond_63
    iput-boolean v6, v4, Lcom/android/internal/telephony/cat/TextMessage;->responseNeeded:Z

    #@65
    .line 1041
    new-instance v7, Lcom/android/internal/telephony/cat/BIPClientParams;

    #@67
    invoke-direct {v7, p1, v4, v2}, Lcom/android/internal/telephony/cat/BIPClientParams;-><init>(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/TextMessage;Z)V

    #@6a
    iput-object v7, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

    #@6c
    .line 1043
    if-eqz v3, :cond_7c

    #@6e
    .line 1044
    iput v5, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mIconLoadState:I

    #@70
    .line 1045
    iget-object v6, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mIconLoader:Lcom/android/internal/telephony/cat/IconLoader;

    #@72
    iget v7, v3, Lcom/android/internal/telephony/cat/IconId;->recordNumber:I

    #@74
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->obtainMessage(I)Landroid/os/Message;

    #@77
    move-result-object v8

    #@78
    invoke-virtual {v6, v7, v8}, Lcom/android/internal/telephony/cat/IconLoader;->loadIcon(ILandroid/os/Message;)V

    #@7b
    .line 1048
    :goto_7b
    return v5

    #@7c
    :cond_7c
    move v5, v6

    #@7d
    goto :goto_7b
.end method

.method private processCommandDetails(Ljava/util/List;)Lcom/android/internal/telephony/cat/CommandDetails;
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/cat/ComprehensionTlv;",
            ">;)",
            "Lcom/android/internal/telephony/cat/CommandDetails;"
        }
    .end annotation

    #@0
    .prologue
    .line 101
    .local p1, ctlvs:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/cat/ComprehensionTlv;>;"
    const/4 v0, 0x0

    #@1
    .line 103
    .local v0, cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;
    if-eqz p1, :cond_f

    #@3
    .line 105
    sget-object v3, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->COMMAND_DETAILS:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@5
    invoke-direct {p0, v3, p1}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@8
    move-result-object v1

    #@9
    .line 107
    .local v1, ctlvCmdDet:Lcom/android/internal/telephony/cat/ComprehensionTlv;
    if-eqz v1, :cond_f

    #@b
    .line 109
    :try_start_b
    invoke-static {v1}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveCommandDetails(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Lcom/android/internal/telephony/cat/CommandDetails;
    :try_end_e
    .catch Lcom/android/internal/telephony/cat/ResultException; {:try_start_b .. :try_end_e} :catch_10

    #@e
    move-result-object v0

    #@f
    .line 116
    .end local v1           #ctlvCmdDet:Lcom/android/internal/telephony/cat/ComprehensionTlv;
    :cond_f
    :goto_f
    return-object v0

    #@10
    .line 110
    .restart local v1       #ctlvCmdDet:Lcom/android/internal/telephony/cat/ComprehensionTlv;
    :catch_10
    move-exception v2

    #@11
    .line 111
    .local v2, e:Lcom/android/internal/telephony/cat/ResultException;
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "processCommandDetails: Failed to procees command details e="

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-static {p0, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@27
    goto :goto_f
.end method

.method private processDisplayText(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z
    .registers 10
    .parameter "cmdDet"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/telephony/cat/CommandDetails;",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/cat/ComprehensionTlv;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cat/ResultException;
        }
    .end annotation

    #@0
    .prologue
    .local p2, ctlvs:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/cat/ComprehensionTlv;>;"
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 328
    const-string v3, "process DisplayText"

    #@4
    invoke-static {p0, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@7
    .line 330
    new-instance v2, Lcom/android/internal/telephony/cat/TextMessage;

    #@9
    invoke-direct {v2}, Lcom/android/internal/telephony/cat/TextMessage;-><init>()V

    #@c
    .line 331
    .local v2, textMsg:Lcom/android/internal/telephony/cat/TextMessage;
    const/4 v1, 0x0

    #@d
    .line 333
    .local v1, iconId:Lcom/android/internal/telephony/cat/IconId;
    sget-object v3, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->TEXT_STRING:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@f
    invoke-direct {p0, v3, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@12
    move-result-object v0

    #@13
    .line 335
    .local v0, ctlv:Lcom/android/internal/telephony/cat/ComprehensionTlv;
    if-eqz v0, :cond_1b

    #@15
    .line 336
    invoke-static {v0}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveTextString(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    iput-object v3, v2, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    #@1b
    .line 340
    :cond_1b
    iget-object v3, v2, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    #@1d
    if-nez v3, :cond_27

    #@1f
    .line 341
    new-instance v3, Lcom/android/internal/telephony/cat/ResultException;

    #@21
    sget-object v4, Lcom/android/internal/telephony/cat/ResultCode;->CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@23
    invoke-direct {v3, v4}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@26
    throw v3

    #@27
    .line 344
    :cond_27
    sget-object v3, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->IMMEDIATE_RESPONSE:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@29
    invoke-direct {p0, v3, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@2c
    move-result-object v0

    #@2d
    .line 345
    if-eqz v0, :cond_31

    #@2f
    .line 346
    iput-boolean v5, v2, Lcom/android/internal/telephony/cat/TextMessage;->responseNeeded:Z

    #@31
    .line 349
    :cond_31
    sget-object v3, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->ICON_ID:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@33
    invoke-direct {p0, v3, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@36
    move-result-object v0

    #@37
    .line 350
    if-eqz v0, :cond_41

    #@39
    .line 351
    invoke-static {v0}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveIconId(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Lcom/android/internal/telephony/cat/IconId;

    #@3c
    move-result-object v1

    #@3d
    .line 352
    iget-boolean v3, v1, Lcom/android/internal/telephony/cat/IconId;->selfExplanatory:Z

    #@3f
    iput-boolean v3, v2, Lcom/android/internal/telephony/cat/TextMessage;->iconSelfExplanatory:Z

    #@41
    .line 355
    :cond_41
    sget-object v3, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->DURATION:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@43
    invoke-direct {p0, v3, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@46
    move-result-object v0

    #@47
    .line 356
    if-eqz v0, :cond_4f

    #@49
    .line 357
    invoke-static {v0}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveDuration(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Lcom/android/internal/telephony/cat/Duration;

    #@4c
    move-result-object v3

    #@4d
    iput-object v3, v2, Lcom/android/internal/telephony/cat/TextMessage;->duration:Lcom/android/internal/telephony/cat/Duration;

    #@4f
    .line 361
    :cond_4f
    iget v3, p1, Lcom/android/internal/telephony/cat/CommandDetails;->commandQualifier:I

    #@51
    and-int/lit8 v3, v3, 0x1

    #@53
    if-eqz v3, :cond_7a

    #@55
    move v3, v4

    #@56
    :goto_56
    iput-boolean v3, v2, Lcom/android/internal/telephony/cat/TextMessage;->isHighPriority:Z

    #@58
    .line 362
    iget v3, p1, Lcom/android/internal/telephony/cat/CommandDetails;->commandQualifier:I

    #@5a
    and-int/lit16 v3, v3, 0x80

    #@5c
    if-eqz v3, :cond_7c

    #@5e
    move v3, v4

    #@5f
    :goto_5f
    iput-boolean v3, v2, Lcom/android/internal/telephony/cat/TextMessage;->userClear:Z

    #@61
    .line 364
    new-instance v3, Lcom/android/internal/telephony/cat/DisplayTextParams;

    #@63
    invoke-direct {v3, p1, v2}, Lcom/android/internal/telephony/cat/DisplayTextParams;-><init>(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/TextMessage;)V

    #@66
    iput-object v3, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

    #@68
    .line 366
    if-eqz v1, :cond_7e

    #@6a
    .line 367
    iput-boolean v4, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mloadIcon:Z

    #@6c
    .line 368
    iput v4, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mIconLoadState:I

    #@6e
    .line 369
    iget-object v3, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mIconLoader:Lcom/android/internal/telephony/cat/IconLoader;

    #@70
    iget v5, v1, Lcom/android/internal/telephony/cat/IconId;->recordNumber:I

    #@72
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->obtainMessage(I)Landroid/os/Message;

    #@75
    move-result-object v6

    #@76
    invoke-virtual {v3, v5, v6}, Lcom/android/internal/telephony/cat/IconLoader;->loadIcon(ILandroid/os/Message;)V

    #@79
    .line 373
    :goto_79
    return v4

    #@7a
    :cond_7a
    move v3, v5

    #@7b
    .line 361
    goto :goto_56

    #@7c
    :cond_7c
    move v3, v5

    #@7d
    .line 362
    goto :goto_5f

    #@7e
    :cond_7e
    move v4, v5

    #@7f
    .line 373
    goto :goto_79
.end method

.method private processEventNotify(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z
    .registers 10
    .parameter "cmdDet"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/telephony/cat/CommandDetails;",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/cat/ComprehensionTlv;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cat/ResultException;
        }
    .end annotation

    #@0
    .prologue
    .local p2, ctlvs:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/cat/ComprehensionTlv;>;"
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 703
    const-string v5, "process EventNotify"

    #@4
    invoke-static {p0, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@7
    .line 705
    new-instance v2, Lcom/android/internal/telephony/cat/TextMessage;

    #@9
    invoke-direct {v2}, Lcom/android/internal/telephony/cat/TextMessage;-><init>()V

    #@c
    .line 706
    .local v2, textMsg:Lcom/android/internal/telephony/cat/TextMessage;
    const/4 v1, 0x0

    #@d
    .line 708
    .local v1, iconId:Lcom/android/internal/telephony/cat/IconId;
    sget-object v5, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->ALPHA_ID:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@f
    invoke-direct {p0, v5, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@12
    move-result-object v0

    #@13
    .line 710
    .local v0, ctlv:Lcom/android/internal/telephony/cat/ComprehensionTlv;
    invoke-static {v0}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveAlphaId(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Ljava/lang/String;

    #@16
    move-result-object v5

    #@17
    iput-object v5, v2, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    #@19
    .line 712
    sget-object v5, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->ICON_ID:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@1b
    invoke-direct {p0, v5, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@1e
    move-result-object v0

    #@1f
    .line 713
    if-eqz v0, :cond_29

    #@21
    .line 714
    invoke-static {v0}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveIconId(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Lcom/android/internal/telephony/cat/IconId;

    #@24
    move-result-object v1

    #@25
    .line 715
    iget-boolean v5, v1, Lcom/android/internal/telephony/cat/IconId;->selfExplanatory:Z

    #@27
    iput-boolean v5, v2, Lcom/android/internal/telephony/cat/TextMessage;->iconSelfExplanatory:Z

    #@29
    .line 718
    :cond_29
    iput-boolean v4, v2, Lcom/android/internal/telephony/cat/TextMessage;->responseNeeded:Z

    #@2b
    .line 719
    new-instance v5, Lcom/android/internal/telephony/cat/DisplayTextParams;

    #@2d
    invoke-direct {v5, p1, v2}, Lcom/android/internal/telephony/cat/DisplayTextParams;-><init>(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/TextMessage;)V

    #@30
    iput-object v5, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

    #@32
    .line 721
    if-eqz v1, :cond_44

    #@34
    .line 722
    iput-boolean v3, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mloadIcon:Z

    #@36
    .line 723
    iput v3, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mIconLoadState:I

    #@38
    .line 724
    iget-object v4, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mIconLoader:Lcom/android/internal/telephony/cat/IconLoader;

    #@3a
    iget v5, v1, Lcom/android/internal/telephony/cat/IconId;->recordNumber:I

    #@3c
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->obtainMessage(I)Landroid/os/Message;

    #@3f
    move-result-object v6

    #@40
    invoke-virtual {v4, v5, v6}, Lcom/android/internal/telephony/cat/IconLoader;->loadIcon(ILandroid/os/Message;)V

    #@43
    .line 728
    :goto_43
    return v3

    #@44
    :cond_44
    move v3, v4

    #@45
    goto :goto_43
.end method

.method private processGetInkey(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z
    .registers 10
    .parameter "cmdDet"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/telephony/cat/CommandDetails;",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/cat/ComprehensionTlv;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cat/ResultException;
        }
    .end annotation

    #@0
    .prologue
    .local p2, ctlvs:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/cat/ComprehensionTlv;>;"
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 440
    const-string v3, "process GetInkey"

    #@4
    invoke-static {p0, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@7
    .line 442
    new-instance v2, Lcom/android/internal/telephony/cat/Input;

    #@9
    invoke-direct {v2}, Lcom/android/internal/telephony/cat/Input;-><init>()V

    #@c
    .line 443
    .local v2, input:Lcom/android/internal/telephony/cat/Input;
    const/4 v1, 0x0

    #@d
    .line 445
    .local v1, iconId:Lcom/android/internal/telephony/cat/IconId;
    sget-object v3, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->TEXT_STRING:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@f
    invoke-direct {p0, v3, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@12
    move-result-object v0

    #@13
    .line 447
    .local v0, ctlv:Lcom/android/internal/telephony/cat/ComprehensionTlv;
    if-eqz v0, :cond_78

    #@15
    .line 448
    invoke-static {v0}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveTextString(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    iput-object v3, v2, Lcom/android/internal/telephony/cat/Input;->text:Ljava/lang/String;

    #@1b
    .line 453
    sget-object v3, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->ICON_ID:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@1d
    invoke-direct {p0, v3, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@20
    move-result-object v0

    #@21
    .line 454
    if-eqz v0, :cond_27

    #@23
    .line 455
    invoke-static {v0}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveIconId(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Lcom/android/internal/telephony/cat/IconId;

    #@26
    move-result-object v1

    #@27
    .line 459
    :cond_27
    sget-object v3, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->DURATION:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@29
    invoke-direct {p0, v3, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@2c
    move-result-object v0

    #@2d
    .line 460
    if-eqz v0, :cond_35

    #@2f
    .line 461
    invoke-static {v0}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveDuration(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Lcom/android/internal/telephony/cat/Duration;

    #@32
    move-result-object v3

    #@33
    iput-object v3, v2, Lcom/android/internal/telephony/cat/Input;->duration:Lcom/android/internal/telephony/cat/Duration;

    #@35
    .line 464
    :cond_35
    iput v4, v2, Lcom/android/internal/telephony/cat/Input;->minLen:I

    #@37
    .line 465
    iput v4, v2, Lcom/android/internal/telephony/cat/Input;->maxLen:I

    #@39
    .line 467
    iget v3, p1, Lcom/android/internal/telephony/cat/CommandDetails;->commandQualifier:I

    #@3b
    and-int/lit8 v3, v3, 0x1

    #@3d
    if-nez v3, :cond_80

    #@3f
    move v3, v4

    #@40
    :goto_40
    iput-boolean v3, v2, Lcom/android/internal/telephony/cat/Input;->digitOnly:Z

    #@42
    .line 468
    iget v3, p1, Lcom/android/internal/telephony/cat/CommandDetails;->commandQualifier:I

    #@44
    and-int/lit8 v3, v3, 0x2

    #@46
    if-eqz v3, :cond_82

    #@48
    move v3, v4

    #@49
    :goto_49
    iput-boolean v3, v2, Lcom/android/internal/telephony/cat/Input;->ucs2:Z

    #@4b
    .line 469
    iget v3, p1, Lcom/android/internal/telephony/cat/CommandDetails;->commandQualifier:I

    #@4d
    and-int/lit8 v3, v3, 0x4

    #@4f
    if-eqz v3, :cond_84

    #@51
    move v3, v4

    #@52
    :goto_52
    iput-boolean v3, v2, Lcom/android/internal/telephony/cat/Input;->yesNo:Z

    #@54
    .line 470
    iget v3, p1, Lcom/android/internal/telephony/cat/CommandDetails;->commandQualifier:I

    #@56
    and-int/lit16 v3, v3, 0x80

    #@58
    if-eqz v3, :cond_86

    #@5a
    move v3, v4

    #@5b
    :goto_5b
    iput-boolean v3, v2, Lcom/android/internal/telephony/cat/Input;->helpAvailable:Z

    #@5d
    .line 471
    iput-boolean v4, v2, Lcom/android/internal/telephony/cat/Input;->echo:Z

    #@5f
    .line 473
    new-instance v3, Lcom/android/internal/telephony/cat/GetInputParams;

    #@61
    invoke-direct {v3, p1, v2}, Lcom/android/internal/telephony/cat/GetInputParams;-><init>(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/Input;)V

    #@64
    iput-object v3, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

    #@66
    .line 475
    if-eqz v1, :cond_88

    #@68
    .line 476
    iput-boolean v4, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mloadIcon:Z

    #@6a
    .line 477
    iput v4, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mIconLoadState:I

    #@6c
    .line 478
    iget-object v3, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mIconLoader:Lcom/android/internal/telephony/cat/IconLoader;

    #@6e
    iget v5, v1, Lcom/android/internal/telephony/cat/IconId;->recordNumber:I

    #@70
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->obtainMessage(I)Landroid/os/Message;

    #@73
    move-result-object v6

    #@74
    invoke-virtual {v3, v5, v6}, Lcom/android/internal/telephony/cat/IconLoader;->loadIcon(ILandroid/os/Message;)V

    #@77
    .line 482
    :goto_77
    return v4

    #@78
    .line 450
    :cond_78
    new-instance v3, Lcom/android/internal/telephony/cat/ResultException;

    #@7a
    sget-object v4, Lcom/android/internal/telephony/cat/ResultCode;->REQUIRED_VALUES_MISSING:Lcom/android/internal/telephony/cat/ResultCode;

    #@7c
    invoke-direct {v3, v4}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@7f
    throw v3

    #@80
    :cond_80
    move v3, v5

    #@81
    .line 467
    goto :goto_40

    #@82
    :cond_82
    move v3, v5

    #@83
    .line 468
    goto :goto_49

    #@84
    :cond_84
    move v3, v5

    #@85
    .line 469
    goto :goto_52

    #@86
    :cond_86
    move v3, v5

    #@87
    .line 470
    goto :goto_5b

    #@88
    :cond_88
    move v4, v5

    #@89
    .line 482
    goto :goto_77
.end method

.method private processGetInput(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z
    .registers 15
    .parameter "cmdDet"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/telephony/cat/CommandDetails;",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/cat/ComprehensionTlv;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cat/ResultException;
        }
    .end annotation

    #@0
    .prologue
    .local p2, ctlvs:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/cat/ComprehensionTlv;>;"
    const/16 v11, 0xef

    #@2
    const/16 v10, 0x76

    #@4
    const/4 v8, 0x0

    #@5
    const/4 v7, 0x1

    #@6
    .line 498
    const-string v6, "process GetInput"

    #@8
    invoke-static {p0, v6}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@b
    .line 500
    new-instance v3, Lcom/android/internal/telephony/cat/Input;

    #@d
    invoke-direct {v3}, Lcom/android/internal/telephony/cat/Input;-><init>()V

    #@10
    .line 501
    .local v3, input:Lcom/android/internal/telephony/cat/Input;
    const/4 v2, 0x0

    #@11
    .line 503
    .local v2, iconId:Lcom/android/internal/telephony/cat/IconId;
    sget-object v6, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->TEXT_STRING:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@13
    invoke-direct {p0, v6, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@16
    move-result-object v0

    #@17
    .line 505
    .local v0, ctlv:Lcom/android/internal/telephony/cat/ComprehensionTlv;
    if-eqz v0, :cond_c9

    #@19
    .line 506
    invoke-static {v0}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveTextString(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Ljava/lang/String;

    #@1c
    move-result-object v6

    #@1d
    iput-object v6, v3, Lcom/android/internal/telephony/cat/Input;->text:Ljava/lang/String;

    #@1f
    .line 511
    sget-object v6, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->RESPONSE_LENGTH:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@21
    invoke-direct {p0, v6, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@24
    move-result-object v0

    #@25
    .line 512
    if-eqz v0, :cond_da

    #@27
    .line 514
    :try_start_27
    invoke-virtual {v0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getRawValue()[B

    #@2a
    move-result-object v4

    #@2b
    .line 515
    .local v4, rawValue:[B
    invoke-virtual {v0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getValueIndex()I

    #@2e
    move-result v5

    #@2f
    .line 516
    .local v5, valueIndex:I
    aget-byte v6, v4, v5

    #@31
    and-int/lit16 v6, v6, 0xff

    #@33
    iput v6, v3, Lcom/android/internal/telephony/cat/Input;->minLen:I

    #@35
    .line 517
    add-int/lit8 v6, v5, 0x1

    #@37
    aget-byte v6, v4, v6

    #@39
    and-int/lit16 v6, v6, 0xff

    #@3b
    iput v6, v3, Lcom/android/internal/telephony/cat/Input;->maxLen:I
    :try_end_3d
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_27 .. :try_end_3d} :catch_d1

    #@3d
    .line 525
    sget-object v6, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->DEFAULT_TEXT:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@3f
    invoke-direct {p0, v6, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@42
    move-result-object v0

    #@43
    .line 526
    if-eqz v0, :cond_4b

    #@45
    .line 527
    invoke-static {v0}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveTextString(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Ljava/lang/String;

    #@48
    move-result-object v6

    #@49
    iput-object v6, v3, Lcom/android/internal/telephony/cat/Input;->defaultText:Ljava/lang/String;

    #@4b
    .line 530
    :cond_4b
    sget-object v6, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->ICON_ID:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@4d
    invoke-direct {p0, v6, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@50
    move-result-object v0

    #@51
    .line 531
    if-eqz v0, :cond_57

    #@53
    .line 532
    invoke-static {v0}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveIconId(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Lcom/android/internal/telephony/cat/IconId;

    #@56
    move-result-object v2

    #@57
    .line 535
    :cond_57
    iget v6, p1, Lcom/android/internal/telephony/cat/CommandDetails;->commandQualifier:I

    #@59
    and-int/lit8 v6, v6, 0x1

    #@5b
    if-nez v6, :cond_e2

    #@5d
    move v6, v7

    #@5e
    :goto_5e
    iput-boolean v6, v3, Lcom/android/internal/telephony/cat/Input;->digitOnly:Z

    #@60
    .line 536
    iget v6, p1, Lcom/android/internal/telephony/cat/CommandDetails;->commandQualifier:I

    #@62
    and-int/lit8 v6, v6, 0x2

    #@64
    if-eqz v6, :cond_e5

    #@66
    move v6, v7

    #@67
    :goto_67
    iput-boolean v6, v3, Lcom/android/internal/telephony/cat/Input;->ucs2:Z

    #@69
    .line 537
    iget v6, p1, Lcom/android/internal/telephony/cat/CommandDetails;->commandQualifier:I

    #@6b
    and-int/lit8 v6, v6, 0x4

    #@6d
    if-nez v6, :cond_e7

    #@6f
    move v6, v7

    #@70
    :goto_70
    iput-boolean v6, v3, Lcom/android/internal/telephony/cat/Input;->echo:Z

    #@72
    .line 538
    iget v6, p1, Lcom/android/internal/telephony/cat/CommandDetails;->commandQualifier:I

    #@74
    and-int/lit8 v6, v6, 0x8

    #@76
    if-eqz v6, :cond_e9

    #@78
    move v6, v7

    #@79
    :goto_79
    iput-boolean v6, v3, Lcom/android/internal/telephony/cat/Input;->packed:Z

    #@7b
    .line 539
    iget v6, p1, Lcom/android/internal/telephony/cat/CommandDetails;->commandQualifier:I

    #@7d
    and-int/lit16 v6, v6, 0x80

    #@7f
    if-eqz v6, :cond_eb

    #@81
    move v6, v7

    #@82
    :goto_82
    iput-boolean v6, v3, Lcom/android/internal/telephony/cat/Input;->helpAvailable:Z

    #@84
    .line 543
    iget-boolean v6, v3, Lcom/android/internal/telephony/cat/Input;->ucs2:Z

    #@86
    if-eqz v6, :cond_ed

    #@88
    iget v6, v3, Lcom/android/internal/telephony/cat/Input;->maxLen:I

    #@8a
    if-le v6, v10, :cond_ed

    #@8c
    .line 544
    new-instance v6, Ljava/lang/StringBuilder;

    #@8e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@91
    const-string v9, "UCS2: received maxLen = "

    #@93
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v6

    #@97
    iget v9, v3, Lcom/android/internal/telephony/cat/Input;->maxLen:I

    #@99
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v6

    #@9d
    const-string v9, ", truncating to "

    #@9f
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v6

    #@a3
    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v6

    #@a7
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@aa
    move-result-object v6

    #@ab
    invoke-static {p0, v6}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@ae
    .line 546
    iput v10, v3, Lcom/android/internal/telephony/cat/Input;->maxLen:I

    #@b0
    .line 553
    :cond_b0
    :goto_b0
    new-instance v6, Lcom/android/internal/telephony/cat/GetInputParams;

    #@b2
    invoke-direct {v6, p1, v3}, Lcom/android/internal/telephony/cat/GetInputParams;-><init>(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/Input;)V

    #@b5
    iput-object v6, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

    #@b7
    .line 555
    if-eqz v2, :cond_11a

    #@b9
    .line 556
    iput-boolean v7, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mloadIcon:Z

    #@bb
    .line 557
    iput v7, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mIconLoadState:I

    #@bd
    .line 558
    iget-object v6, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mIconLoader:Lcom/android/internal/telephony/cat/IconLoader;

    #@bf
    iget v8, v2, Lcom/android/internal/telephony/cat/IconId;->recordNumber:I

    #@c1
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->obtainMessage(I)Landroid/os/Message;

    #@c4
    move-result-object v9

    #@c5
    invoke-virtual {v6, v8, v9}, Lcom/android/internal/telephony/cat/IconLoader;->loadIcon(ILandroid/os/Message;)V

    #@c8
    .line 562
    :goto_c8
    return v7

    #@c9
    .line 508
    .end local v4           #rawValue:[B
    .end local v5           #valueIndex:I
    :cond_c9
    new-instance v6, Lcom/android/internal/telephony/cat/ResultException;

    #@cb
    sget-object v7, Lcom/android/internal/telephony/cat/ResultCode;->REQUIRED_VALUES_MISSING:Lcom/android/internal/telephony/cat/ResultCode;

    #@cd
    invoke-direct {v6, v7}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@d0
    throw v6

    #@d1
    .line 518
    :catch_d1
    move-exception v1

    #@d2
    .line 519
    .local v1, e:Ljava/lang/IndexOutOfBoundsException;
    new-instance v6, Lcom/android/internal/telephony/cat/ResultException;

    #@d4
    sget-object v7, Lcom/android/internal/telephony/cat/ResultCode;->CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@d6
    invoke-direct {v6, v7}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@d9
    throw v6

    #@da
    .line 522
    .end local v1           #e:Ljava/lang/IndexOutOfBoundsException;
    :cond_da
    new-instance v6, Lcom/android/internal/telephony/cat/ResultException;

    #@dc
    sget-object v7, Lcom/android/internal/telephony/cat/ResultCode;->REQUIRED_VALUES_MISSING:Lcom/android/internal/telephony/cat/ResultCode;

    #@de
    invoke-direct {v6, v7}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@e1
    throw v6

    #@e2
    .restart local v4       #rawValue:[B
    .restart local v5       #valueIndex:I
    :cond_e2
    move v6, v8

    #@e3
    .line 535
    goto/16 :goto_5e

    #@e5
    :cond_e5
    move v6, v8

    #@e6
    .line 536
    goto :goto_67

    #@e7
    :cond_e7
    move v6, v8

    #@e8
    .line 537
    goto :goto_70

    #@e9
    :cond_e9
    move v6, v8

    #@ea
    .line 538
    goto :goto_79

    #@eb
    :cond_eb
    move v6, v8

    #@ec
    .line 539
    goto :goto_82

    #@ed
    .line 547
    :cond_ed
    iget-boolean v6, v3, Lcom/android/internal/telephony/cat/Input;->packed:Z

    #@ef
    if-nez v6, :cond_b0

    #@f1
    iget v6, v3, Lcom/android/internal/telephony/cat/Input;->maxLen:I

    #@f3
    if-le v6, v11, :cond_b0

    #@f5
    .line 548
    new-instance v6, Ljava/lang/StringBuilder;

    #@f7
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@fa
    const-string v9, "GSM 7Bit Default: received maxLen = "

    #@fc
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v6

    #@100
    iget v9, v3, Lcom/android/internal/telephony/cat/Input;->maxLen:I

    #@102
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@105
    move-result-object v6

    #@106
    const-string v9, ", truncating to "

    #@108
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10b
    move-result-object v6

    #@10c
    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10f
    move-result-object v6

    #@110
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@113
    move-result-object v6

    #@114
    invoke-static {p0, v6}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@117
    .line 550
    iput v11, v3, Lcom/android/internal/telephony/cat/Input;->maxLen:I

    #@119
    goto :goto_b0

    #@11a
    :cond_11a
    move v7, v8

    #@11b
    .line 562
    goto :goto_c8
.end method

.method private processLaunchBrowser(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z
    .registers 16
    .parameter "cmdDet"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/telephony/cat/CommandDetails;",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/cat/ComprehensionTlv;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cat/ResultException;
        }
    .end annotation

    #@0
    .prologue
    .local p2, ctlvs:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/cat/ComprehensionTlv;>;"
    const/4 v9, 0x1

    #@1
    .line 794
    const-string v10, "process LaunchBrowser"

    #@3
    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@6
    .line 796
    new-instance v0, Lcom/android/internal/telephony/cat/TextMessage;

    #@8
    invoke-direct {v0}, Lcom/android/internal/telephony/cat/TextMessage;-><init>()V

    #@b
    .line 797
    .local v0, confirmMsg:Lcom/android/internal/telephony/cat/TextMessage;
    const/4 v3, 0x0

    #@c
    .line 798
    .local v3, iconId:Lcom/android/internal/telephony/cat/IconId;
    const/4 v6, 0x0

    #@d
    .line 800
    .local v6, url:Ljava/lang/String;
    sget-object v10, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->URL:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@f
    invoke-direct {p0, v10, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@12
    move-result-object v1

    #@13
    .line 801
    .local v1, ctlv:Lcom/android/internal/telephony/cat/ComprehensionTlv;
    if-eqz v1, :cond_27

    #@15
    .line 803
    :try_start_15
    invoke-virtual {v1}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getRawValue()[B

    #@18
    move-result-object v5

    #@19
    .line 804
    .local v5, rawValue:[B
    invoke-virtual {v1}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getValueIndex()I

    #@1c
    move-result v7

    #@1d
    .line 805
    .local v7, valueIndex:I
    invoke-virtual {v1}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getLength()I

    #@20
    move-result v8

    #@21
    .line 806
    .local v8, valueLen:I
    if-lez v8, :cond_61

    #@23
    .line 807
    invoke-static {v5, v7, v8}, Lcom/android/internal/telephony/GsmAlphabet;->gsm8BitUnpackedToString([BII)Ljava/lang/String;
    :try_end_26
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_15 .. :try_end_26} :catch_63

    #@26
    move-result-object v6

    #@27
    .line 818
    .end local v5           #rawValue:[B
    .end local v7           #valueIndex:I
    .end local v8           #valueLen:I
    :cond_27
    :goto_27
    sget-object v10, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->ALPHA_ID:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@29
    invoke-direct {p0, v10, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@2c
    move-result-object v1

    #@2d
    .line 819
    invoke-static {v1}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveAlphaId(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Ljava/lang/String;

    #@30
    move-result-object v10

    #@31
    iput-object v10, v0, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    #@33
    .line 822
    sget-object v10, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->ICON_ID:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@35
    invoke-direct {p0, v10, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@38
    move-result-object v1

    #@39
    .line 823
    if-eqz v1, :cond_43

    #@3b
    .line 824
    invoke-static {v1}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveIconId(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Lcom/android/internal/telephony/cat/IconId;

    #@3e
    move-result-object v3

    #@3f
    .line 825
    iget-boolean v10, v3, Lcom/android/internal/telephony/cat/IconId;->selfExplanatory:Z

    #@41
    iput-boolean v10, v0, Lcom/android/internal/telephony/cat/TextMessage;->iconSelfExplanatory:Z

    #@43
    .line 830
    :cond_43
    iget v10, p1, Lcom/android/internal/telephony/cat/CommandDetails;->commandQualifier:I

    #@45
    packed-switch v10, :pswitch_data_74

    #@48
    .line 833
    sget-object v4, Lcom/android/internal/telephony/cat/LaunchBrowserMode;->LAUNCH_IF_NOT_ALREADY_LAUNCHED:Lcom/android/internal/telephony/cat/LaunchBrowserMode;

    #@4a
    .line 843
    .local v4, mode:Lcom/android/internal/telephony/cat/LaunchBrowserMode;
    :goto_4a
    new-instance v10, Lcom/android/internal/telephony/cat/LaunchBrowserParams;

    #@4c
    invoke-direct {v10, p1, v0, v6, v4}, Lcom/android/internal/telephony/cat/LaunchBrowserParams;-><init>(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/TextMessage;Ljava/lang/String;Lcom/android/internal/telephony/cat/LaunchBrowserMode;)V

    #@4f
    iput-object v10, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

    #@51
    .line 845
    if-eqz v3, :cond_72

    #@53
    .line 846
    iput v9, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mIconLoadState:I

    #@55
    .line 847
    iget-object v10, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mIconLoader:Lcom/android/internal/telephony/cat/IconLoader;

    #@57
    iget v11, v3, Lcom/android/internal/telephony/cat/IconId;->recordNumber:I

    #@59
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->obtainMessage(I)Landroid/os/Message;

    #@5c
    move-result-object v12

    #@5d
    invoke-virtual {v10, v11, v12}, Lcom/android/internal/telephony/cat/IconLoader;->loadIcon(ILandroid/os/Message;)V

    #@60
    .line 851
    :goto_60
    return v9

    #@61
    .line 810
    .end local v4           #mode:Lcom/android/internal/telephony/cat/LaunchBrowserMode;
    .restart local v5       #rawValue:[B
    .restart local v7       #valueIndex:I
    .restart local v8       #valueLen:I
    :cond_61
    const/4 v6, 0x0

    #@62
    goto :goto_27

    #@63
    .line 812
    .end local v5           #rawValue:[B
    .end local v7           #valueIndex:I
    .end local v8           #valueLen:I
    :catch_63
    move-exception v2

    #@64
    .line 813
    .local v2, e:Ljava/lang/IndexOutOfBoundsException;
    new-instance v9, Lcom/android/internal/telephony/cat/ResultException;

    #@66
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@68
    invoke-direct {v9, v10}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@6b
    throw v9

    #@6c
    .line 836
    .end local v2           #e:Ljava/lang/IndexOutOfBoundsException;
    :pswitch_6c
    sget-object v4, Lcom/android/internal/telephony/cat/LaunchBrowserMode;->USE_EXISTING_BROWSER:Lcom/android/internal/telephony/cat/LaunchBrowserMode;

    #@6e
    .line 837
    .restart local v4       #mode:Lcom/android/internal/telephony/cat/LaunchBrowserMode;
    goto :goto_4a

    #@6f
    .line 839
    .end local v4           #mode:Lcom/android/internal/telephony/cat/LaunchBrowserMode;
    :pswitch_6f
    sget-object v4, Lcom/android/internal/telephony/cat/LaunchBrowserMode;->LAUNCH_NEW_BROWSER:Lcom/android/internal/telephony/cat/LaunchBrowserMode;

    #@71
    .restart local v4       #mode:Lcom/android/internal/telephony/cat/LaunchBrowserMode;
    goto :goto_4a

    #@72
    .line 851
    :cond_72
    const/4 v9, 0x0

    #@73
    goto :goto_60

    #@74
    .line 830
    :pswitch_data_74
    .packed-switch 0x2
        :pswitch_6c
        :pswitch_6f
    .end packed-switch
.end method

.method private processPlayTone(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z
    .registers 16
    .parameter "cmdDet"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/telephony/cat/CommandDetails;",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/cat/ComprehensionTlv;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cat/ResultException;
        }
    .end annotation

    #@0
    .prologue
    .line 867
    .local p2, ctlvs:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/cat/ComprehensionTlv;>;"
    const-string v0, "process PlayTone"

    #@2
    invoke-static {p0, v0}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@5
    .line 869
    const/4 v3, 0x0

    #@6
    .line 870
    .local v3, tone:Lcom/android/internal/telephony/cat/Tone;
    new-instance v2, Lcom/android/internal/telephony/cat/TextMessage;

    #@8
    invoke-direct {v2}, Lcom/android/internal/telephony/cat/TextMessage;-><init>()V

    #@b
    .line 871
    .local v2, textMsg:Lcom/android/internal/telephony/cat/TextMessage;
    const/4 v4, 0x0

    #@c
    .line 872
    .local v4, duration:Lcom/android/internal/telephony/cat/Duration;
    const/4 v8, 0x0

    #@d
    .line 874
    .local v8, iconId:Lcom/android/internal/telephony/cat/IconId;
    sget-object v0, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->TONE:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@f
    invoke-direct {p0, v0, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@12
    move-result-object v6

    #@13
    .line 875
    .local v6, ctlv:Lcom/android/internal/telephony/cat/ComprehensionTlv;
    if-eqz v6, :cond_68

    #@15
    .line 877
    invoke-virtual {v6}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getLength()I

    #@18
    move-result v0

    #@19
    if-lez v0, :cond_68

    #@1b
    .line 879
    :try_start_1b
    invoke-virtual {v6}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getRawValue()[B

    #@1e
    move-result-object v9

    #@1f
    .line 880
    .local v9, rawValue:[B
    invoke-virtual {v6}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getValueIndex()I

    #@22
    move-result v11

    #@23
    .line 881
    .local v11, valueIndex:I
    aget-byte v10, v9, v11

    #@25
    .line 882
    .local v10, toneVal:I
    invoke-static {v10}, Lcom/android/internal/telephony/cat/Tone;->fromInt(I)Lcom/android/internal/telephony/cat/Tone;

    #@28
    move-result-object v3

    #@29
    .line 884
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@2c
    move-result-object v0

    #@2d
    const-string v1, "DCM"

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@32
    move-result v0

    #@33
    if-eqz v0, :cond_68

    #@35
    .line 886
    if-eqz v3, :cond_57

    #@37
    const/16 v0, 0x40

    #@39
    if-eq v10, v0, :cond_57

    #@3b
    const/16 v0, 0x41

    #@3d
    if-eq v10, v0, :cond_57

    #@3f
    const/16 v0, 0x42

    #@41
    if-eq v10, v0, :cond_57

    #@43
    const/16 v0, 0x43

    #@45
    if-eq v10, v0, :cond_57

    #@47
    const/16 v0, 0x44

    #@49
    if-eq v10, v0, :cond_57

    #@4b
    const/16 v0, 0x45

    #@4d
    if-eq v10, v0, :cond_57

    #@4f
    const/16 v0, 0x46

    #@51
    if-eq v10, v0, :cond_57

    #@53
    const/16 v0, 0x47

    #@55
    if-ne v10, v0, :cond_68

    #@57
    .line 889
    :cond_57
    new-instance v0, Lcom/android/internal/telephony/cat/ResultException;

    #@59
    sget-object v1, Lcom/android/internal/telephony/cat/ResultCode;->BEYOND_TERMINAL_CAPABILITY:Lcom/android/internal/telephony/cat/ResultCode;

    #@5b
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@5e
    throw v0
    :try_end_5f
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1b .. :try_end_5f} :catch_5f

    #@5f
    .line 893
    .end local v9           #rawValue:[B
    .end local v10           #toneVal:I
    .end local v11           #valueIndex:I
    :catch_5f
    move-exception v7

    #@60
    .line 894
    .local v7, e:Ljava/lang/IndexOutOfBoundsException;
    new-instance v0, Lcom/android/internal/telephony/cat/ResultException;

    #@62
    sget-object v1, Lcom/android/internal/telephony/cat/ResultCode;->CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@64
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@67
    throw v0

    #@68
    .line 900
    .end local v7           #e:Ljava/lang/IndexOutOfBoundsException;
    :cond_68
    sget-object v0, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->ALPHA_ID:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@6a
    invoke-direct {p0, v0, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@6d
    move-result-object v6

    #@6e
    .line 901
    if-eqz v6, :cond_76

    #@70
    .line 902
    invoke-static {v6}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveAlphaId(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Ljava/lang/String;

    #@73
    move-result-object v0

    #@74
    iput-object v0, v2, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    #@76
    .line 905
    :cond_76
    sget-object v0, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->DURATION:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@78
    invoke-direct {p0, v0, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@7b
    move-result-object v6

    #@7c
    .line 906
    if-eqz v6, :cond_82

    #@7e
    .line 907
    invoke-static {v6}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveDuration(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Lcom/android/internal/telephony/cat/Duration;

    #@81
    move-result-object v4

    #@82
    .line 910
    :cond_82
    sget-object v0, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->ICON_ID:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@84
    invoke-direct {p0, v0, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@87
    move-result-object v6

    #@88
    .line 911
    if-eqz v6, :cond_92

    #@8a
    .line 912
    invoke-static {v6}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveIconId(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Lcom/android/internal/telephony/cat/IconId;

    #@8d
    move-result-object v8

    #@8e
    .line 913
    iget-boolean v0, v8, Lcom/android/internal/telephony/cat/IconId;->selfExplanatory:Z

    #@90
    iput-boolean v0, v2, Lcom/android/internal/telephony/cat/TextMessage;->iconSelfExplanatory:Z

    #@92
    .line 916
    :cond_92
    iget v0, p1, Lcom/android/internal/telephony/cat/CommandDetails;->commandQualifier:I

    #@94
    and-int/lit8 v0, v0, 0x1

    #@96
    if-eqz v0, :cond_b7

    #@98
    const/4 v5, 0x1

    #@99
    .line 918
    .local v5, vibrate:Z
    :goto_99
    const/4 v0, 0x0

    #@9a
    iput-boolean v0, v2, Lcom/android/internal/telephony/cat/TextMessage;->responseNeeded:Z

    #@9c
    .line 919
    new-instance v0, Lcom/android/internal/telephony/cat/PlayToneParams;

    #@9e
    move-object v1, p1

    #@9f
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/telephony/cat/PlayToneParams;-><init>(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/TextMessage;Lcom/android/internal/telephony/cat/Tone;Lcom/android/internal/telephony/cat/Duration;Z)V

    #@a2
    iput-object v0, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

    #@a4
    .line 921
    if-eqz v8, :cond_b9

    #@a6
    .line 922
    const/4 v0, 0x1

    #@a7
    iput v0, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mIconLoadState:I

    #@a9
    .line 923
    iget-object v0, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mIconLoader:Lcom/android/internal/telephony/cat/IconLoader;

    #@ab
    iget v1, v8, Lcom/android/internal/telephony/cat/IconId;->recordNumber:I

    #@ad
    const/4 v12, 0x1

    #@ae
    invoke-virtual {p0, v12}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->obtainMessage(I)Landroid/os/Message;

    #@b1
    move-result-object v12

    #@b2
    invoke-virtual {v0, v1, v12}, Lcom/android/internal/telephony/cat/IconLoader;->loadIcon(ILandroid/os/Message;)V

    #@b5
    .line 925
    const/4 v0, 0x1

    #@b6
    .line 927
    :goto_b6
    return v0

    #@b7
    .line 916
    .end local v5           #vibrate:Z
    :cond_b7
    const/4 v5, 0x0

    #@b8
    goto :goto_99

    #@b9
    .line 927
    .restart local v5       #vibrate:Z
    :cond_b9
    const/4 v0, 0x0

    #@ba
    goto :goto_b6
.end method

.method private processProvideLocalInfo(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z
    .registers 5
    .parameter "cmdDet"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/telephony/cat/CommandDetails;",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/cat/ComprehensionTlv;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cat/ResultException;
        }
    .end annotation

    #@0
    .prologue
    .line 994
    .local p2, ctlvs:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/cat/ComprehensionTlv;>;"
    const-string v0, "process ProvideLocalInfo"

    #@2
    invoke-static {p0, v0}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@5
    .line 995
    iget v0, p1, Lcom/android/internal/telephony/cat/CommandDetails;->commandQualifier:I

    #@7
    packed-switch v0, :pswitch_data_52

    #@a
    .line 1005
    new-instance v0, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v1, "PLI["

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    iget v1, p1, Lcom/android/internal/telephony/cat/CommandDetails;->commandQualifier:I

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    const-string v1, "] Command Not Supported"

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    invoke-static {p0, v0}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@28
    .line 1006
    new-instance v0, Lcom/android/internal/telephony/cat/CommandParams;

    #@2a
    invoke-direct {v0, p1}, Lcom/android/internal/telephony/cat/CommandParams;-><init>(Lcom/android/internal/telephony/cat/CommandDetails;)V

    #@2d
    iput-object v0, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

    #@2f
    .line 1007
    new-instance v0, Lcom/android/internal/telephony/cat/ResultException;

    #@31
    sget-object v1, Lcom/android/internal/telephony/cat/ResultCode;->BEYOND_TERMINAL_CAPABILITY:Lcom/android/internal/telephony/cat/ResultCode;

    #@33
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@36
    throw v0

    #@37
    .line 997
    :pswitch_37
    const-string v0, "PLI [DTTZ_SETTING]"

    #@39
    invoke-static {p0, v0}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@3c
    .line 998
    new-instance v0, Lcom/android/internal/telephony/cat/CommandParams;

    #@3e
    invoke-direct {v0, p1}, Lcom/android/internal/telephony/cat/CommandParams;-><init>(Lcom/android/internal/telephony/cat/CommandDetails;)V

    #@41
    iput-object v0, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

    #@43
    .line 1009
    :goto_43
    const/4 v0, 0x0

    #@44
    return v0

    #@45
    .line 1001
    :pswitch_45
    const-string v0, "PLI [LANGUAGE_SETTING]"

    #@47
    invoke-static {p0, v0}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@4a
    .line 1002
    new-instance v0, Lcom/android/internal/telephony/cat/CommandParams;

    #@4c
    invoke-direct {v0, p1}, Lcom/android/internal/telephony/cat/CommandParams;-><init>(Lcom/android/internal/telephony/cat/CommandDetails;)V

    #@4f
    iput-object v0, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

    #@51
    goto :goto_43

    #@52
    .line 995
    :pswitch_data_52
    .packed-switch 0x3
        :pswitch_37
        :pswitch_45
    .end packed-switch
.end method

.method private processRefresh(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z
    .registers 5
    .parameter "cmdDet"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/telephony/cat/CommandDetails;",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/cat/ComprehensionTlv;",
            ">;)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 575
    .local p2, ctlvs:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/cat/ComprehensionTlv;>;"
    const-string v0, "process Refresh"

    #@2
    invoke-static {p0, v0}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@5
    .line 580
    iget v0, p1, Lcom/android/internal/telephony/cat/CommandDetails;->commandQualifier:I

    #@7
    packed-switch v0, :pswitch_data_16

    #@a
    .line 588
    :goto_a
    :pswitch_a
    const/4 v0, 0x0

    #@b
    return v0

    #@c
    .line 585
    :pswitch_c
    new-instance v0, Lcom/android/internal/telephony/cat/DisplayTextParams;

    #@e
    const/4 v1, 0x0

    #@f
    invoke-direct {v0, p1, v1}, Lcom/android/internal/telephony/cat/DisplayTextParams;-><init>(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/TextMessage;)V

    #@12
    iput-object v0, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

    #@14
    goto :goto_a

    #@15
    .line 580
    nop

    #@16
    :pswitch_data_16
    .packed-switch 0x0
        :pswitch_c
        :pswitch_a
        :pswitch_c
        :pswitch_c
        :pswitch_c
    .end packed-switch
.end method

.method private processSelectItem(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z
    .registers 14
    .parameter "cmdDet"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/telephony/cat/CommandDetails;",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/cat/ComprehensionTlv;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cat/ResultException;
        }
    .end annotation

    #@0
    .prologue
    .local p2, ctlvs:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/cat/ComprehensionTlv;>;"
    const/4 v9, 0x0

    #@1
    const/4 v8, 0x1

    #@2
    .line 604
    const-string v7, "process SelectItem"

    #@4
    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@7
    .line 606
    new-instance v3, Lcom/android/internal/telephony/cat/Menu;

    #@9
    invoke-direct {v3}, Lcom/android/internal/telephony/cat/Menu;-><init>()V

    #@c
    .line 607
    .local v3, menu:Lcom/android/internal/telephony/cat/Menu;
    const/4 v6, 0x0

    #@d
    .line 608
    .local v6, titleIconId:Lcom/android/internal/telephony/cat/IconId;
    const/4 v1, 0x0

    #@e
    .line 609
    .local v1, itemsIconId:Lcom/android/internal/telephony/cat/ItemsIconId;
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@11
    move-result-object v2

    #@12
    .line 611
    .local v2, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/internal/telephony/cat/ComprehensionTlv;>;"
    sget-object v7, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->ALPHA_ID:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@14
    invoke-direct {p0, v7, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@17
    move-result-object v0

    #@18
    .line 613
    .local v0, ctlv:Lcom/android/internal/telephony/cat/ComprehensionTlv;
    if-eqz v0, :cond_20

    #@1a
    .line 614
    invoke-static {v0}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveAlphaId(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Ljava/lang/String;

    #@1d
    move-result-object v7

    #@1e
    iput-object v7, v3, Lcom/android/internal/telephony/cat/Menu;->title:Ljava/lang/String;

    #@20
    .line 618
    :cond_20
    :goto_20
    sget-object v7, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->ITEM:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@22
    invoke-direct {p0, v7, v2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForNextTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/Iterator;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@25
    move-result-object v0

    #@26
    .line 619
    if-eqz v0, :cond_32

    #@28
    .line 620
    iget-object v7, v3, Lcom/android/internal/telephony/cat/Menu;->items:Ljava/util/List;

    #@2a
    invoke-static {v0}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveItem(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Lcom/android/internal/telephony/cat/Item;

    #@2d
    move-result-object v10

    #@2e
    invoke-interface {v7, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@31
    goto :goto_20

    #@32
    .line 627
    :cond_32
    iget-object v7, v3, Lcom/android/internal/telephony/cat/Menu;->items:Ljava/util/List;

    #@34
    invoke-interface {v7}, Ljava/util/List;->size()I

    #@37
    move-result v7

    #@38
    if-nez v7, :cond_42

    #@3a
    .line 628
    new-instance v7, Lcom/android/internal/telephony/cat/ResultException;

    #@3c
    sget-object v8, Lcom/android/internal/telephony/cat/ResultCode;->REQUIRED_VALUES_MISSING:Lcom/android/internal/telephony/cat/ResultCode;

    #@3e
    invoke-direct {v7, v8}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@41
    throw v7

    #@42
    .line 631
    :cond_42
    sget-object v7, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->ITEM_ID:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@44
    invoke-direct {p0, v7, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@47
    move-result-object v0

    #@48
    .line 632
    if-eqz v0, :cond_52

    #@4a
    .line 635
    invoke-static {v0}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveItemId(Lcom/android/internal/telephony/cat/ComprehensionTlv;)I

    #@4d
    move-result v7

    #@4e
    add-int/lit8 v7, v7, -0x1

    #@50
    iput v7, v3, Lcom/android/internal/telephony/cat/Menu;->defaultItem:I

    #@52
    .line 638
    :cond_52
    sget-object v7, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->ICON_ID:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@54
    invoke-direct {p0, v7, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@57
    move-result-object v0

    #@58
    .line 639
    if-eqz v0, :cond_64

    #@5a
    .line 640
    iput v8, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mIconLoadState:I

    #@5c
    .line 641
    invoke-static {v0}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveIconId(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Lcom/android/internal/telephony/cat/IconId;

    #@5f
    move-result-object v6

    #@60
    .line 642
    iget-boolean v7, v6, Lcom/android/internal/telephony/cat/IconId;->selfExplanatory:Z

    #@62
    iput-boolean v7, v3, Lcom/android/internal/telephony/cat/Menu;->titleIconSelfExplanatory:Z

    #@64
    .line 645
    :cond_64
    sget-object v7, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->ITEM_ICON_ID_LIST:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@66
    invoke-direct {p0, v7, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@69
    move-result-object v0

    #@6a
    .line 646
    if-eqz v0, :cond_77

    #@6c
    .line 647
    const/4 v7, 0x2

    #@6d
    iput v7, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mIconLoadState:I

    #@6f
    .line 648
    invoke-static {v0}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveItemsIconId(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Lcom/android/internal/telephony/cat/ItemsIconId;

    #@72
    move-result-object v1

    #@73
    .line 649
    iget-boolean v7, v1, Lcom/android/internal/telephony/cat/ItemsIconId;->selfExplanatory:Z

    #@75
    iput-boolean v7, v3, Lcom/android/internal/telephony/cat/Menu;->itemsIconSelfExplanatory:Z

    #@77
    .line 652
    :cond_77
    iget v7, p1, Lcom/android/internal/telephony/cat/CommandDetails;->commandQualifier:I

    #@79
    and-int/lit8 v7, v7, 0x1

    #@7b
    if-eqz v7, :cond_ad

    #@7d
    move v4, v8

    #@7e
    .line 653
    .local v4, presentTypeSpecified:Z
    :goto_7e
    if-eqz v4, :cond_8a

    #@80
    .line 654
    iget v7, p1, Lcom/android/internal/telephony/cat/CommandDetails;->commandQualifier:I

    #@82
    and-int/lit8 v7, v7, 0x2

    #@84
    if-nez v7, :cond_af

    #@86
    .line 655
    sget-object v7, Lcom/android/internal/telephony/cat/PresentationType;->DATA_VALUES:Lcom/android/internal/telephony/cat/PresentationType;

    #@88
    iput-object v7, v3, Lcom/android/internal/telephony/cat/Menu;->presentationType:Lcom/android/internal/telephony/cat/PresentationType;

    #@8a
    .line 660
    :cond_8a
    :goto_8a
    iget v7, p1, Lcom/android/internal/telephony/cat/CommandDetails;->commandQualifier:I

    #@8c
    and-int/lit8 v7, v7, 0x4

    #@8e
    if-eqz v7, :cond_b4

    #@90
    move v7, v8

    #@91
    :goto_91
    iput-boolean v7, v3, Lcom/android/internal/telephony/cat/Menu;->softKeyPreferred:Z

    #@93
    .line 661
    iget v7, p1, Lcom/android/internal/telephony/cat/CommandDetails;->commandQualifier:I

    #@95
    and-int/lit16 v7, v7, 0x80

    #@97
    if-eqz v7, :cond_b6

    #@99
    move v7, v8

    #@9a
    :goto_9a
    iput-boolean v7, v3, Lcom/android/internal/telephony/cat/Menu;->helpAvailable:Z

    #@9c
    .line 663
    new-instance v10, Lcom/android/internal/telephony/cat/SelectItemParams;

    #@9e
    if-eqz v6, :cond_b8

    #@a0
    move v7, v8

    #@a1
    :goto_a1
    invoke-direct {v10, p1, v3, v7}, Lcom/android/internal/telephony/cat/SelectItemParams;-><init>(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/Menu;Z)V

    #@a4
    iput-object v10, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

    #@a6
    .line 666
    iget v7, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mIconLoadState:I

    #@a8
    packed-switch v7, :pswitch_data_ec

    #@ab
    :goto_ab
    move v9, v8

    #@ac
    .line 688
    :pswitch_ac
    return v9

    #@ad
    .end local v4           #presentTypeSpecified:Z
    :cond_ad
    move v4, v9

    #@ae
    .line 652
    goto :goto_7e

    #@af
    .line 657
    .restart local v4       #presentTypeSpecified:Z
    :cond_af
    sget-object v7, Lcom/android/internal/telephony/cat/PresentationType;->NAVIGATION_OPTIONS:Lcom/android/internal/telephony/cat/PresentationType;

    #@b1
    iput-object v7, v3, Lcom/android/internal/telephony/cat/Menu;->presentationType:Lcom/android/internal/telephony/cat/PresentationType;

    #@b3
    goto :goto_8a

    #@b4
    :cond_b4
    move v7, v9

    #@b5
    .line 660
    goto :goto_91

    #@b6
    :cond_b6
    move v7, v9

    #@b7
    .line 661
    goto :goto_9a

    #@b8
    :cond_b8
    move v7, v9

    #@b9
    .line 663
    goto :goto_a1

    #@ba
    .line 670
    :pswitch_ba
    iput-boolean v8, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mloadIcon:Z

    #@bc
    .line 671
    iget-object v7, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mIconLoader:Lcom/android/internal/telephony/cat/IconLoader;

    #@be
    iget v9, v6, Lcom/android/internal/telephony/cat/IconId;->recordNumber:I

    #@c0
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->obtainMessage(I)Landroid/os/Message;

    #@c3
    move-result-object v10

    #@c4
    invoke-virtual {v7, v9, v10}, Lcom/android/internal/telephony/cat/IconLoader;->loadIcon(ILandroid/os/Message;)V

    #@c7
    goto :goto_ab

    #@c8
    .line 675
    :pswitch_c8
    iget-object v5, v1, Lcom/android/internal/telephony/cat/ItemsIconId;->recordNumbers:[I

    #@ca
    .line 676
    .local v5, recordNumbers:[I
    if-eqz v6, :cond_df

    #@cc
    .line 678
    iget-object v7, v1, Lcom/android/internal/telephony/cat/ItemsIconId;->recordNumbers:[I

    #@ce
    array-length v7, v7

    #@cf
    add-int/lit8 v7, v7, 0x1

    #@d1
    new-array v5, v7, [I

    #@d3
    .line 679
    iget v7, v6, Lcom/android/internal/telephony/cat/IconId;->recordNumber:I

    #@d5
    aput v7, v5, v9

    #@d7
    .line 680
    iget-object v7, v1, Lcom/android/internal/telephony/cat/ItemsIconId;->recordNumbers:[I

    #@d9
    iget-object v10, v1, Lcom/android/internal/telephony/cat/ItemsIconId;->recordNumbers:[I

    #@db
    array-length v10, v10

    #@dc
    invoke-static {v7, v9, v5, v8, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@df
    .line 683
    :cond_df
    iput-boolean v8, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mloadIcon:Z

    #@e1
    .line 684
    iget-object v7, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mIconLoader:Lcom/android/internal/telephony/cat/IconLoader;

    #@e3
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->obtainMessage(I)Landroid/os/Message;

    #@e6
    move-result-object v9

    #@e7
    invoke-virtual {v7, v5, v9}, Lcom/android/internal/telephony/cat/IconLoader;->loadIcons([ILandroid/os/Message;)V

    #@ea
    goto :goto_ab

    #@eb
    .line 666
    nop

    #@ec
    :pswitch_data_ec
    .packed-switch 0x0
        :pswitch_ac
        :pswitch_ba
        :pswitch_c8
    .end packed-switch
.end method

.method private processSetUpEventList(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z
    .registers 12
    .parameter "cmdDet"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/telephony/cat/CommandDetails;",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/cat/ComprehensionTlv;",
            ">;)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 744
    .local p2, ctlvs:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/cat/ComprehensionTlv;>;"
    const-string v8, "process SetUpEventList"

    #@2
    invoke-static {p0, v8}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@5
    .line 745
    sget-object v8, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->EVENT_LIST:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@7
    invoke-direct {p0, v8, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@a
    move-result-object v0

    #@b
    .line 746
    .local v0, ctlv:Lcom/android/internal/telephony/cat/ComprehensionTlv;
    if-eqz v0, :cond_37

    #@d
    .line 748
    :try_start_d
    invoke-virtual {v0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getRawValue()[B

    #@10
    move-result-object v5

    #@11
    .line 749
    .local v5, rawValue:[B
    invoke-virtual {v0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getValueIndex()I

    #@14
    move-result v6

    #@15
    .line 750
    .local v6, valueIndex:I
    invoke-virtual {v0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getLength()I

    #@18
    move-result v7

    #@19
    .line 751
    .local v7, valueLen:I
    new-array v2, v7, [I

    #@1b
    .line 752
    .local v2, eventList:[I
    const/4 v3, -0x1

    #@1c
    .line 753
    .local v3, eventValue:I
    const/4 v4, 0x0

    #@1d
    .line 754
    .local v4, i:I
    :goto_1d
    if-lez v7, :cond_30

    #@1f
    .line 755
    aget-byte v8, v5, v6

    #@21
    and-int/lit16 v3, v8, 0xff

    #@23
    .line 756
    add-int/lit8 v6, v6, 0x1

    #@25
    .line 757
    add-int/lit8 v7, v7, -0x1

    #@27
    .line 759
    sparse-switch v3, :sswitch_data_40

    #@2a
    goto :goto_1d

    #@2b
    .line 765
    :sswitch_2b
    aput v3, v2, v4

    #@2d
    .line 766
    add-int/lit8 v4, v4, 0x1

    #@2f
    .line 767
    goto :goto_1d

    #@30
    .line 773
    :cond_30
    new-instance v8, Lcom/android/internal/telephony/cat/SetEventListParams;

    #@32
    invoke-direct {v8, p1, v2}, Lcom/android/internal/telephony/cat/SetEventListParams;-><init>(Lcom/android/internal/telephony/cat/CommandDetails;[I)V

    #@35
    iput-object v8, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;
    :try_end_37
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_d .. :try_end_37} :catch_39

    #@37
    .line 778
    .end local v2           #eventList:[I
    .end local v3           #eventValue:I
    .end local v4           #i:I
    .end local v5           #rawValue:[B
    .end local v6           #valueIndex:I
    .end local v7           #valueLen:I
    :cond_37
    :goto_37
    const/4 v8, 0x0

    #@38
    return v8

    #@39
    .line 774
    :catch_39
    move-exception v1

    #@3a
    .line 775
    .local v1, e:Ljava/lang/IndexOutOfBoundsException;
    const-string v8, " IndexOutofBoundException in processSetUpEventList"

    #@3c
    invoke-static {p0, v8}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@3f
    goto :goto_37

    #@40
    .line 759
    :sswitch_data_40
    .sparse-switch
        0x4 -> :sswitch_2b
        0x5 -> :sswitch_2b
        0x7 -> :sswitch_2b
        0x8 -> :sswitch_2b
        0xf -> :sswitch_2b
    .end sparse-switch
.end method

.method private processSetUpIdleModeText(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z
    .registers 10
    .parameter "cmdDet"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/telephony/cat/CommandDetails;",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/cat/ComprehensionTlv;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cat/ResultException;
        }
    .end annotation

    #@0
    .prologue
    .local p2, ctlvs:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/cat/ComprehensionTlv;>;"
    const/4 v3, 0x1

    #@1
    .line 389
    const-string v4, "process SetUpIdleModeText"

    #@3
    invoke-static {p0, v4}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@6
    .line 391
    new-instance v2, Lcom/android/internal/telephony/cat/TextMessage;

    #@8
    invoke-direct {v2}, Lcom/android/internal/telephony/cat/TextMessage;-><init>()V

    #@b
    .line 392
    .local v2, textMsg:Lcom/android/internal/telephony/cat/TextMessage;
    const/4 v1, 0x0

    #@c
    .line 394
    .local v1, iconId:Lcom/android/internal/telephony/cat/IconId;
    sget-object v4, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->TEXT_STRING:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@e
    invoke-direct {p0, v4, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@11
    move-result-object v0

    #@12
    .line 396
    .local v0, ctlv:Lcom/android/internal/telephony/cat/ComprehensionTlv;
    if-eqz v0, :cond_1a

    #@14
    .line 397
    invoke-static {v0}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveTextString(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Ljava/lang/String;

    #@17
    move-result-object v4

    #@18
    iput-object v4, v2, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    #@1a
    .line 400
    :cond_1a
    sget-object v4, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->ICON_ID:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@1c
    invoke-direct {p0, v4, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@1f
    move-result-object v0

    #@20
    .line 401
    if-eqz v0, :cond_2a

    #@22
    .line 402
    invoke-static {v0}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveIconId(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Lcom/android/internal/telephony/cat/IconId;

    #@25
    move-result-object v1

    #@26
    .line 403
    iget-boolean v4, v1, Lcom/android/internal/telephony/cat/IconId;->selfExplanatory:Z

    #@28
    iput-boolean v4, v2, Lcom/android/internal/telephony/cat/TextMessage;->iconSelfExplanatory:Z

    #@2a
    .line 411
    :cond_2a
    iget-object v4, v2, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    #@2c
    if-nez v4, :cond_3c

    #@2e
    if-eqz v1, :cond_3c

    #@30
    iget-boolean v4, v2, Lcom/android/internal/telephony/cat/TextMessage;->iconSelfExplanatory:Z

    #@32
    if-nez v4, :cond_3c

    #@34
    .line 412
    new-instance v3, Lcom/android/internal/telephony/cat/ResultException;

    #@36
    sget-object v4, Lcom/android/internal/telephony/cat/ResultCode;->CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@38
    invoke-direct {v3, v4}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@3b
    throw v3

    #@3c
    .line 415
    :cond_3c
    new-instance v4, Lcom/android/internal/telephony/cat/DisplayTextParams;

    #@3e
    invoke-direct {v4, p1, v2}, Lcom/android/internal/telephony/cat/DisplayTextParams;-><init>(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/TextMessage;)V

    #@41
    iput-object v4, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

    #@43
    .line 417
    if-eqz v1, :cond_55

    #@45
    .line 418
    iput-boolean v3, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mloadIcon:Z

    #@47
    .line 419
    iput v3, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mIconLoadState:I

    #@49
    .line 420
    iget-object v4, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mIconLoader:Lcom/android/internal/telephony/cat/IconLoader;

    #@4b
    iget v5, v1, Lcom/android/internal/telephony/cat/IconId;->recordNumber:I

    #@4d
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->obtainMessage(I)Landroid/os/Message;

    #@50
    move-result-object v6

    #@51
    invoke-virtual {v4, v5, v6}, Lcom/android/internal/telephony/cat/IconLoader;->loadIcon(ILandroid/os/Message;)V

    #@54
    .line 424
    :goto_54
    return v3

    #@55
    :cond_55
    const/4 v3, 0x0

    #@56
    goto :goto_54
.end method

.method private processSetupCall(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z
    .registers 15
    .parameter "cmdDet"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/telephony/cat/CommandDetails;",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/cat/ComprehensionTlv;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cat/ResultException;
        }
    .end annotation

    #@0
    .prologue
    .local p2, ctlvs:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/cat/ComprehensionTlv;>;"
    const/4 v11, 0x2

    #@1
    const/4 v9, 0x0

    #@2
    const/4 v8, -0x1

    #@3
    const/4 v10, 0x1

    #@4
    .line 942
    const-string v7, "process SetupCall"

    #@6
    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@9
    .line 944
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@c
    move-result-object v5

    #@d
    .line 945
    .local v5, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/internal/telephony/cat/ComprehensionTlv;>;"
    const/4 v4, 0x0

    #@e
    .line 947
    .local v4, ctlv:Lcom/android/internal/telephony/cat/ComprehensionTlv;
    new-instance v3, Lcom/android/internal/telephony/cat/TextMessage;

    #@10
    invoke-direct {v3}, Lcom/android/internal/telephony/cat/TextMessage;-><init>()V

    #@13
    .line 949
    .local v3, confirmMsg:Lcom/android/internal/telephony/cat/TextMessage;
    new-instance v1, Lcom/android/internal/telephony/cat/TextMessage;

    #@15
    invoke-direct {v1}, Lcom/android/internal/telephony/cat/TextMessage;-><init>()V

    #@18
    .line 950
    .local v1, callMsg:Lcom/android/internal/telephony/cat/TextMessage;
    const/4 v2, 0x0

    #@19
    .line 951
    .local v2, confirmIconId:Lcom/android/internal/telephony/cat/IconId;
    const/4 v0, 0x0

    #@1a
    .line 954
    .local v0, callIconId:Lcom/android/internal/telephony/cat/IconId;
    sget-object v7, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->ALPHA_ID:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@1c
    invoke-direct {p0, v7, v5}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForNextTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/Iterator;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@1f
    move-result-object v4

    #@20
    .line 955
    invoke-static {v4}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveAlphaId(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Ljava/lang/String;

    #@23
    move-result-object v7

    #@24
    iput-object v7, v3, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    #@26
    .line 957
    sget-object v7, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->ICON_ID:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@28
    invoke-direct {p0, v7, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@2b
    move-result-object v4

    #@2c
    .line 958
    if-eqz v4, :cond_36

    #@2e
    .line 959
    invoke-static {v4}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveIconId(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Lcom/android/internal/telephony/cat/IconId;

    #@31
    move-result-object v2

    #@32
    .line 960
    iget-boolean v7, v2, Lcom/android/internal/telephony/cat/IconId;->selfExplanatory:Z

    #@34
    iput-boolean v7, v3, Lcom/android/internal/telephony/cat/TextMessage;->iconSelfExplanatory:Z

    #@36
    .line 964
    :cond_36
    sget-object v7, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->ALPHA_ID:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@38
    invoke-direct {p0, v7, v5}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForNextTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/Iterator;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@3b
    move-result-object v4

    #@3c
    .line 965
    if-eqz v4, :cond_44

    #@3e
    .line 966
    invoke-static {v4}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveAlphaId(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Ljava/lang/String;

    #@41
    move-result-object v7

    #@42
    iput-object v7, v1, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    #@44
    .line 969
    :cond_44
    sget-object v7, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->ICON_ID:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@46
    invoke-direct {p0, v7, p2}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@49
    move-result-object v4

    #@4a
    .line 970
    if-eqz v4, :cond_54

    #@4c
    .line 971
    invoke-static {v4}, Lcom/android/internal/telephony/cat/ValueParser;->retrieveIconId(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Lcom/android/internal/telephony/cat/IconId;

    #@4f
    move-result-object v0

    #@50
    .line 972
    iget-boolean v7, v0, Lcom/android/internal/telephony/cat/IconId;->selfExplanatory:Z

    #@52
    iput-boolean v7, v1, Lcom/android/internal/telephony/cat/TextMessage;->iconSelfExplanatory:Z

    #@54
    .line 975
    :cond_54
    new-instance v7, Lcom/android/internal/telephony/cat/CallSetupParams;

    #@56
    invoke-direct {v7, p1, v3, v1}, Lcom/android/internal/telephony/cat/CallSetupParams;-><init>(Lcom/android/internal/telephony/cat/CommandDetails;Lcom/android/internal/telephony/cat/TextMessage;Lcom/android/internal/telephony/cat/TextMessage;)V

    #@59
    iput-object v7, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

    #@5b
    .line 977
    if-nez v2, :cond_5f

    #@5d
    if-eqz v0, :cond_7c

    #@5f
    .line 978
    :cond_5f
    iput v11, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mIconLoadState:I

    #@61
    .line 979
    new-array v6, v11, [I

    #@63
    .line 980
    .local v6, recordNumbers:[I
    if-eqz v2, :cond_7a

    #@65
    iget v7, v2, Lcom/android/internal/telephony/cat/IconId;->recordNumber:I

    #@67
    :goto_67
    aput v7, v6, v9

    #@69
    .line 982
    if-eqz v0, :cond_6d

    #@6b
    iget v8, v0, Lcom/android/internal/telephony/cat/IconId;->recordNumber:I

    #@6d
    :cond_6d
    aput v8, v6, v10

    #@6f
    .line 985
    iget-object v7, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mIconLoader:Lcom/android/internal/telephony/cat/IconLoader;

    #@71
    invoke-virtual {p0, v10}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->obtainMessage(I)Landroid/os/Message;

    #@74
    move-result-object v8

    #@75
    invoke-virtual {v7, v6, v8}, Lcom/android/internal/telephony/cat/IconLoader;->loadIcons([ILandroid/os/Message;)V

    #@78
    move v7, v10

    #@79
    .line 989
    .end local v6           #recordNumbers:[I
    :goto_79
    return v7

    #@7a
    .restart local v6       #recordNumbers:[I
    :cond_7a
    move v7, v8

    #@7b
    .line 980
    goto :goto_67

    #@7c
    .end local v6           #recordNumbers:[I
    :cond_7c
    move v7, v9

    #@7d
    .line 989
    goto :goto_79
.end method

.method private searchForNextTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/Iterator;)Lcom/android/internal/telephony/cat/ComprehensionTlv;
    .registers 6
    .parameter "tag"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/telephony/cat/ComprehensionTlvTag;",
            "Ljava/util/Iterator",
            "<",
            "Lcom/android/internal/telephony/cat/ComprehensionTlv;",
            ">;)",
            "Lcom/android/internal/telephony/cat/ComprehensionTlv;"
        }
    .end annotation

    #@0
    .prologue
    .line 304
    .local p2, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/internal/telephony/cat/ComprehensionTlv;>;"
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->value()I

    #@3
    move-result v1

    #@4
    .line 305
    .local v1, tagValue:I
    :cond_4
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_17

    #@a
    .line 306
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@10
    .line 307
    .local v0, ctlv:Lcom/android/internal/telephony/cat/ComprehensionTlv;
    invoke-virtual {v0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getTag()I

    #@13
    move-result v2

    #@14
    if-ne v2, v1, :cond_4

    #@16
    .line 311
    .end local v0           #ctlv:Lcom/android/internal/telephony/cat/ComprehensionTlv;
    :goto_16
    return-object v0

    #@17
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_16
.end method

.method private searchForTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/List;)Lcom/android/internal/telephony/cat/ComprehensionTlv;
    .registers 5
    .parameter "tag"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/telephony/cat/ComprehensionTlvTag;",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/cat/ComprehensionTlv;",
            ">;)",
            "Lcom/android/internal/telephony/cat/ComprehensionTlv;"
        }
    .end annotation

    #@0
    .prologue
    .line 286
    .local p2, ctlvs:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/cat/ComprehensionTlv;>;"
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@3
    move-result-object v0

    #@4
    .line 287
    .local v0, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/internal/telephony/cat/ComprehensionTlv;>;"
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->searchForNextTag(Lcom/android/internal/telephony/cat/ComprehensionTlvTag;Ljava/util/Iterator;)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@7
    move-result-object v1

    #@8
    return-object v1
.end method

.method private sendCmdParams(Lcom/android/internal/telephony/cat/ResultCode;)V
    .registers 4
    .parameter "resCode"

    #@0
    .prologue
    .line 272
    iget-object v0, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCaller:Lcom/android/internal/telephony/cat/RilMessageDecoder;

    #@2
    iget-object v1, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

    #@4
    invoke-virtual {v0, p1, v1}, Lcom/android/internal/telephony/cat/RilMessageDecoder;->sendMsgParamsDecoded(Lcom/android/internal/telephony/cat/ResultCode;Lcom/android/internal/telephony/cat/CommandParams;)V

    #@7
    .line 273
    return-void
.end method

.method private setIcons(Ljava/lang/Object;)Lcom/android/internal/telephony/cat/ResultCode;
    .registers 10
    .parameter "data"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 238
    const/4 v4, 0x0

    #@2
    .line 239
    .local v4, icons:[Landroid/graphics/Bitmap;
    const/4 v3, 0x0

    #@3
    .line 241
    .local v3, iconIndex:I
    if-nez p1, :cond_1b

    #@5
    .line 242
    iget-boolean v6, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mloadIcon:Z

    #@7
    if-eqz v6, :cond_18

    #@9
    .line 243
    const-string v6, "Optional Icon data is NULL"

    #@b
    invoke-static {p0, v6}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@e
    .line 244
    iget-object v6, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

    #@10
    iput-boolean v7, v6, Lcom/android/internal/telephony/cat/CommandParams;->loadIconFailed:Z

    #@12
    .line 245
    const/4 v6, 0x0

    #@13
    iput-boolean v6, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mloadIcon:Z

    #@15
    .line 248
    sget-object v6, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    #@17
    .line 268
    .end local p1
    :goto_17
    return-object v6

    #@18
    .line 250
    .restart local p1
    :cond_18
    sget-object v6, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    #@1a
    goto :goto_17

    #@1b
    .line 252
    :cond_1b
    iget v6, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mIconLoadState:I

    #@1d
    packed-switch v6, :pswitch_data_4e

    #@20
    .line 268
    .end local p1
    :cond_20
    :goto_20
    sget-object v6, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    #@22
    goto :goto_17

    #@23
    .line 254
    .restart local p1
    :pswitch_23
    iget-object v6, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

    #@25
    check-cast p1, Landroid/graphics/Bitmap;

    #@27
    .end local p1
    invoke-virtual {v6, p1}, Lcom/android/internal/telephony/cat/CommandParams;->setIcon(Landroid/graphics/Bitmap;)Z

    #@2a
    goto :goto_20

    #@2b
    .line 257
    .restart local p1
    :pswitch_2b
    check-cast p1, [Landroid/graphics/Bitmap;

    #@2d
    .end local p1
    move-object v4, p1

    #@2e
    check-cast v4, [Landroid/graphics/Bitmap;

    #@30
    .line 259
    move-object v0, v4

    #@31
    .local v0, arr$:[Landroid/graphics/Bitmap;
    array-length v5, v0

    #@32
    .local v5, len$:I
    const/4 v1, 0x0

    #@33
    .local v1, i$:I
    :goto_33
    if-ge v1, v5, :cond_20

    #@35
    aget-object v2, v0, v1

    #@37
    .line 260
    .local v2, icon:Landroid/graphics/Bitmap;
    iget-object v6, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

    #@39
    invoke-virtual {v6, v2}, Lcom/android/internal/telephony/cat/CommandParams;->setIcon(Landroid/graphics/Bitmap;)Z

    #@3c
    .line 261
    if-nez v2, :cond_4b

    #@3e
    iget-boolean v6, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mloadIcon:Z

    #@40
    if-eqz v6, :cond_4b

    #@42
    .line 262
    const-string v6, "Optional Icon data is NULL while loading multi icons"

    #@44
    invoke-static {p0, v6}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@47
    .line 263
    iget-object v6, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

    #@49
    iput-boolean v7, v6, Lcom/android/internal/telephony/cat/CommandParams;->loadIconFailed:Z

    #@4b
    .line 259
    :cond_4b
    add-int/lit8 v1, v1, 0x1

    #@4d
    goto :goto_33

    #@4e
    .line 252
    :pswitch_data_4e
    .packed-switch 0x1
        :pswitch_23
        :pswitch_2b
    .end packed-switch
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 230
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v0, :pswitch_data_10

    #@5
    .line 235
    :goto_5
    return-void

    #@6
    .line 232
    :pswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->setIcons(Ljava/lang/Object;)Lcom/android/internal/telephony/cat/ResultCode;

    #@b
    move-result-object v0

    #@c
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->sendCmdParams(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@f
    goto :goto_5

    #@10
    .line 230
    :pswitch_data_10
    .packed-switch 0x1
        :pswitch_6
    .end packed-switch
.end method

.method make(Lcom/android/internal/telephony/cat/BerTlv;)V
    .registers 9
    .parameter "berTlv"

    #@0
    .prologue
    .line 120
    if-nez p1, :cond_3

    #@2
    .line 226
    :cond_2
    :goto_2
    return-void

    #@3
    .line 124
    :cond_3
    const/4 v5, 0x0

    #@4
    iput-object v5, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

    #@6
    .line 125
    const/4 v5, 0x0

    #@7
    iput v5, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mIconLoadState:I

    #@9
    .line 127
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/BerTlv;->getTag()I

    #@c
    move-result v5

    #@d
    const/16 v6, 0xd0

    #@f
    if-eq v5, v6, :cond_17

    #@11
    .line 128
    sget-object v5, Lcom/android/internal/telephony/cat/ResultCode;->CMD_TYPE_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@13
    invoke-direct {p0, v5}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->sendCmdParams(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@16
    goto :goto_2

    #@17
    .line 131
    :cond_17
    const/4 v1, 0x0

    #@18
    .line 132
    .local v1, cmdPending:Z
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/BerTlv;->getComprehensionTlvs()Ljava/util/List;

    #@1b
    move-result-object v3

    #@1c
    .line 134
    .local v3, ctlvs:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/cat/ComprehensionTlv;>;"
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->processCommandDetails(Ljava/util/List;)Lcom/android/internal/telephony/cat/CommandDetails;

    #@1f
    move-result-object v0

    #@20
    .line 135
    .local v0, cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;
    if-nez v0, :cond_28

    #@22
    .line 136
    sget-object v5, Lcom/android/internal/telephony/cat/ResultCode;->CMD_TYPE_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@24
    invoke-direct {p0, v5}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->sendCmdParams(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@27
    goto :goto_2

    #@28
    .line 142
    :cond_28
    iget v5, v0, Lcom/android/internal/telephony/cat/CommandDetails;->typeOfCommand:I

    #@2a
    invoke-static {v5}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->fromInt(I)Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    #@2d
    move-result-object v2

    #@2e
    .line 144
    .local v2, cmdType:Lcom/android/internal/telephony/cat/AppInterface$CommandType;
    if-nez v2, :cond_3d

    #@30
    .line 147
    new-instance v5, Lcom/android/internal/telephony/cat/CommandParams;

    #@32
    invoke-direct {v5, v0}, Lcom/android/internal/telephony/cat/CommandParams;-><init>(Lcom/android/internal/telephony/cat/CommandDetails;)V

    #@35
    iput-object v5, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

    #@37
    .line 148
    sget-object v5, Lcom/android/internal/telephony/cat/ResultCode;->BEYOND_TERMINAL_CAPABILITY:Lcom/android/internal/telephony/cat/ResultCode;

    #@39
    invoke-direct {p0, v5}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->sendCmdParams(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@3c
    goto :goto_2

    #@3d
    .line 153
    :cond_3d
    :try_start_3d
    sget-object v5, Lcom/android/internal/telephony/cat/CommandParamsFactory$1;->$SwitchMap$com$android$internal$telephony$cat$AppInterface$CommandType:[I

    #@3f
    invoke-virtual {v2}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->ordinal()I

    #@42
    move-result v6

    #@43
    aget v5, v5, v6

    #@45
    packed-switch v5, :pswitch_data_ce

    #@48
    .line 213
    new-instance v5, Lcom/android/internal/telephony/cat/CommandParams;

    #@4a
    invoke-direct {v5, v0}, Lcom/android/internal/telephony/cat/CommandParams;-><init>(Lcom/android/internal/telephony/cat/CommandDetails;)V

    #@4d
    iput-object v5, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

    #@4f
    .line 214
    sget-object v5, Lcom/android/internal/telephony/cat/ResultCode;->BEYOND_TERMINAL_CAPABILITY:Lcom/android/internal/telephony/cat/ResultCode;

    #@51
    invoke-direct {p0, v5}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->sendCmdParams(Lcom/android/internal/telephony/cat/ResultCode;)V
    :try_end_54
    .catch Lcom/android/internal/telephony/cat/ResultException; {:try_start_3d .. :try_end_54} :catch_55

    #@54
    goto :goto_2

    #@55
    .line 217
    :catch_55
    move-exception v4

    #@56
    .line 218
    .local v4, e:Lcom/android/internal/telephony/cat/ResultException;
    new-instance v5, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v6, "make: caught ResultException e="

    #@5d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v5

    #@61
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v5

    #@65
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v5

    #@69
    invoke-static {p0, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    #@6c
    .line 219
    new-instance v5, Lcom/android/internal/telephony/cat/CommandParams;

    #@6e
    invoke-direct {v5, v0}, Lcom/android/internal/telephony/cat/CommandParams;-><init>(Lcom/android/internal/telephony/cat/CommandDetails;)V

    #@71
    iput-object v5, p0, Lcom/android/internal/telephony/cat/CommandParamsFactory;->mCmdParams:Lcom/android/internal/telephony/cat/CommandParams;

    #@73
    .line 220
    invoke-virtual {v4}, Lcom/android/internal/telephony/cat/ResultException;->result()Lcom/android/internal/telephony/cat/ResultCode;

    #@76
    move-result-object v5

    #@77
    invoke-direct {p0, v5}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->sendCmdParams(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@7a
    goto :goto_2

    #@7b
    .line 155
    .end local v4           #e:Lcom/android/internal/telephony/cat/ResultException;
    :pswitch_7b
    :try_start_7b
    invoke-direct {p0, v0, v3}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->processSelectItem(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z
    :try_end_7e
    .catch Lcom/android/internal/telephony/cat/ResultException; {:try_start_7b .. :try_end_7e} :catch_55

    #@7e
    move-result v1

    #@7f
    .line 223
    :goto_7f
    if-nez v1, :cond_2

    #@81
    .line 224
    sget-object v5, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    #@83
    invoke-direct {p0, v5}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->sendCmdParams(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@86
    goto/16 :goto_2

    #@88
    .line 158
    :pswitch_88
    :try_start_88
    invoke-direct {p0, v0, v3}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->processSelectItem(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z

    #@8b
    move-result v1

    #@8c
    .line 159
    goto :goto_7f

    #@8d
    .line 161
    :pswitch_8d
    invoke-direct {p0, v0, v3}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->processDisplayText(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z

    #@90
    move-result v1

    #@91
    .line 162
    goto :goto_7f

    #@92
    .line 164
    :pswitch_92
    invoke-direct {p0, v0, v3}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->processSetUpIdleModeText(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z

    #@95
    move-result v1

    #@96
    .line 165
    goto :goto_7f

    #@97
    .line 167
    :pswitch_97
    invoke-direct {p0, v0, v3}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->processGetInkey(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z

    #@9a
    move-result v1

    #@9b
    .line 168
    goto :goto_7f

    #@9c
    .line 170
    :pswitch_9c
    invoke-direct {p0, v0, v3}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->processGetInput(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z

    #@9f
    move-result v1

    #@a0
    .line 171
    goto :goto_7f

    #@a1
    .line 176
    :pswitch_a1
    invoke-direct {p0, v0, v3}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->processEventNotify(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z

    #@a4
    move-result v1

    #@a5
    .line 177
    goto :goto_7f

    #@a6
    .line 180
    :pswitch_a6
    invoke-direct {p0, v0, v3}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->processSetupCall(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z

    #@a9
    move-result v1

    #@aa
    .line 181
    goto :goto_7f

    #@ab
    .line 183
    :pswitch_ab
    invoke-direct {p0, v0, v3}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->processRefresh(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z

    #@ae
    .line 184
    const/4 v1, 0x0

    #@af
    .line 185
    goto :goto_7f

    #@b0
    .line 187
    :pswitch_b0
    invoke-direct {p0, v0, v3}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->processLaunchBrowser(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z

    #@b3
    move-result v1

    #@b4
    .line 188
    goto :goto_7f

    #@b5
    .line 190
    :pswitch_b5
    invoke-direct {p0, v0, v3}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->processPlayTone(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z

    #@b8
    move-result v1

    #@b9
    .line 191
    goto :goto_7f

    #@ba
    .line 193
    :pswitch_ba
    invoke-direct {p0, v0, v3}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->processSetUpEventList(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z

    #@bd
    move-result v1

    #@be
    .line 194
    goto :goto_7f

    #@bf
    .line 196
    :pswitch_bf
    invoke-direct {p0, v0, v3}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->processProvideLocalInfo(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z

    #@c2
    move-result v1

    #@c3
    .line 197
    goto :goto_7f

    #@c4
    .line 202
    :pswitch_c4
    invoke-direct {p0, v0, v3}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->processBIPClient(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z

    #@c7
    move-result v1

    #@c8
    .line 203
    goto :goto_7f

    #@c9
    .line 207
    :pswitch_c9
    invoke-direct {p0, v0, v3}, Lcom/android/internal/telephony/cat/CommandParamsFactory;->processActivate(Lcom/android/internal/telephony/cat/CommandDetails;Ljava/util/List;)Z
    :try_end_cc
    .catch Lcom/android/internal/telephony/cat/ResultException; {:try_start_88 .. :try_end_cc} :catch_55

    #@cc
    move-result v1

    #@cd
    .line 208
    goto :goto_7f

    #@ce
    .line 153
    :pswitch_data_ce
    .packed-switch 0x1
        :pswitch_7b
        :pswitch_88
        :pswitch_8d
        :pswitch_92
        :pswitch_97
        :pswitch_9c
        :pswitch_a1
        :pswitch_a1
        :pswitch_a1
        :pswitch_a1
        :pswitch_a6
        :pswitch_a6
        :pswitch_ab
        :pswitch_b0
        :pswitch_b5
        :pswitch_ba
        :pswitch_bf
        :pswitch_c4
        :pswitch_c4
        :pswitch_c4
        :pswitch_c4
        :pswitch_c9
    .end packed-switch
.end method
