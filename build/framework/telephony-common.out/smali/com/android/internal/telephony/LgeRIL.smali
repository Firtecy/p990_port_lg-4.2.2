.class public final Lcom/android/internal/telephony/LgeRIL;
.super Lcom/android/internal/telephony/RIL;
.source "LgeRIL.java"


# static fields
#the value of this static final field might be set in the static constructor
.field static final DBG:Z = false

#the value of this static final field might be set in the static constructor
.field public static final FEATURE_LGRIL_API:Z = false

.field static final LOG_TAG:Ljava/lang/String; = "RILJLge"


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 40
    const-string v0, "ro.lge.capp_smartcard_lgril"

    #@2
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@9
    move-result v0

    #@a
    sput-boolean v0, Lcom/android/internal/telephony/LgeRIL;->FEATURE_LGRIL_API:Z

    #@c
    .line 41
    const-string v0, "ro.debuggable"

    #@e
    const/4 v1, 0x0

    #@f
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    const-string v1, "1"

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v0

    #@19
    sput-boolean v0, Lcom/android/internal/telephony/LgeRIL;->DBG:Z

    #@1b
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IILjava/lang/String;)V
    .registers 11
    .parameter "context"
    .parameter "networkMode"
    .parameter "cdmaSubscription"
    .parameter "featureset"

    #@0
    .prologue
    .line 45
    const/4 v4, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move v2, p2

    #@4
    move v3, p3

    #@5
    move-object v5, p4

    #@6
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/telephony/RIL;-><init>(Landroid/content/Context;IILjava/lang/Integer;Ljava/lang/String;)V

    #@9
    .line 47
    return-void
.end method

.method private dLogD(Ljava/lang/String;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 244
    sget-boolean v0, Lcom/android/internal/telephony/LgeRIL;->DBG:Z

    #@2
    const/4 v1, 0x1

    #@3
    if-ne v0, v1, :cond_1d

    #@5
    .line 245
    const-string v0, "LGSmartcard"

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "[RILJLge] "

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 246
    :cond_1d
    return-void
.end method

.method public static getLgeRIL(Lcom/android/internal/telephony/CommandsInterface;)Lcom/android/internal/telephony/LgeRIL;
    .registers 2
    .parameter "aCM"

    #@0
    .prologue
    .line 57
    instance-of v0, p0, Lcom/android/internal/telephony/LgeRIL;

    #@2
    if-eqz v0, :cond_7

    #@4
    check-cast p0, Lcom/android/internal/telephony/LgeRIL;

    #@6
    .end local p0
    :goto_6
    return-object p0

    #@7
    .restart local p0
    :cond_7
    const/4 p0, 0x0

    #@8
    goto :goto_6
.end method

.method static requestToStringEx(I)Ljava/lang/String;
    .registers 2
    .parameter "request"

    #@0
    .prologue
    .line 99
    sparse-switch p0, :sswitch_data_1e

    #@3
    .line 114
    const/4 v0, 0x0

    #@4
    :goto_4
    return-object v0

    #@5
    .line 100
    :sswitch_5
    const-string v0, "LGE_CLOSE_LOGICAL_CHANNEL"

    #@7
    goto :goto_4

    #@8
    .line 102
    :sswitch_8
    const-string v0, "SC_SIM_OPEN_LOGICAL_CHANNEL"

    #@a
    goto :goto_4

    #@b
    .line 103
    :sswitch_b
    const-string v0, "SC_SIM_TRANSMIT_BASIC"

    #@d
    goto :goto_4

    #@e
    .line 104
    :sswitch_e
    const-string v0, "SC_SIM_TRANSMIT_CHANNEL"

    #@10
    goto :goto_4

    #@11
    .line 105
    :sswitch_11
    const-string v0, "SC_SIM_GET_ATR"

    #@13
    goto :goto_4

    #@14
    .line 106
    :sswitch_14
    const-string v0, "SC_IS_AVAILABLE_SC_ON_SIM"

    #@16
    goto :goto_4

    #@17
    .line 109
    :sswitch_17
    const-string v0, "RIL_REQUEST_LGE_SMS_ENABLE_AUTO_DC_DISCONNECT"

    #@19
    goto :goto_4

    #@1a
    .line 112
    :sswitch_1a
    const-string v0, "RIL_REQUEST_LGE_DATA_GET_MODEM_PATCKET_COUNT"

    #@1c
    goto :goto_4

    #@1d
    .line 99
    nop

    #@1e
    :sswitch_data_1e
    .sparse-switch
        0xbf -> :sswitch_17
        0x157 -> :sswitch_1a
        0x15f -> :sswitch_b
        0x160 -> :sswitch_8
        0x161 -> :sswitch_5
        0x162 -> :sswitch_e
        0x163 -> :sswitch_11
        0x164 -> :sswitch_14
    .end sparse-switch
.end method

.method private responseLGSmartcardIccIo(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 8
    .parameter "p"

    #@0
    .prologue
    .line 204
    const/4 v4, 0x3

    #@1
    new-array v1, v4, [Ljava/lang/String;

    #@3
    .line 208
    .local v1, response:[Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v2

    #@7
    .line 209
    .local v2, sw1:I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@a
    move-result v3

    #@b
    .line 210
    .local v3, sw2:I
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .line 212
    .local v0, payload:Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v5, "< responseLGSmartcardIccIo:  0x"

    #@16
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v4

    #@1a
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@1d
    move-result-object v5

    #@1e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v4

    #@22
    const-string v5, " 0x"

    #@24
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v4

    #@28
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@2b
    move-result-object v5

    #@2c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v4

    #@30
    const-string v5, " "

    #@32
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v4

    #@36
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v4

    #@3a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v4

    #@3e
    invoke-direct {p0, v4}, Lcom/android/internal/telephony/LgeRIL;->dLogD(Ljava/lang/String;)V

    #@41
    .line 217
    const/4 v4, 0x0

    #@42
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@45
    move-result-object v5

    #@46
    aput-object v5, v1, v4

    #@48
    .line 218
    const/4 v4, 0x1

    #@49
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@4c
    move-result-object v5

    #@4d
    aput-object v5, v1, v4

    #@4f
    .line 219
    const/4 v4, 0x2

    #@50
    aput-object v0, v1, v4

    #@52
    .line 221
    return-object v1
.end method

.method static responseToStringEx(I)Ljava/lang/String;
    .registers 2
    .parameter "request"

    #@0
    .prologue
    .line 119
    .line 124
    const/4 v0, 0x0

    #@1
    return-object v0
.end method


# virtual methods
.method public enableAutoDCDisconnect(I)V
    .registers 5
    .parameter "timeOut"

    #@0
    .prologue
    .line 415
    const/16 v1, 0xbf

    #@2
    const/4 v2, 0x0

    #@3
    invoke-static {v1, v2}, Lcom/android/internal/telephony/RILRequest;->obtain(ILandroid/os/Message;)Lcom/android/internal/telephony/RILRequest;

    #@6
    move-result-object v0

    #@7
    .line 417
    .local v0, rr:Lcom/android/internal/telephony/RILRequest;
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@9
    const/4 v2, 0x1

    #@a
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@d
    .line 418
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@f
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 421
    new-instance v1, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    invoke-virtual {v0}, Lcom/android/internal/telephony/RILRequest;->serialString()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const-string v2, "> "

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    iget v2, v0, Lcom/android/internal/telephony/RILRequest;->mRequest:I

    #@27
    invoke-static {v2}, Lcom/android/internal/telephony/LgeRIL;->requestToString(I)Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/LgeRIL;->riljLog(Ljava/lang/String;)V

    #@36
    .line 423
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/LgeRIL;->sendEx(Lcom/android/internal/telephony/RILRequest;)V

    #@39
    .line 424
    return-void
.end method

.method protected enableSmartcardLog(Lcom/android/internal/telephony/RILRequest;)Z
    .registers 5
    .parameter "rr"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 400
    iget v1, p1, Lcom/android/internal/telephony/RILRequest;->mRequest:I

    #@3
    const/16 v2, 0x15f

    #@5
    if-lt v1, v2, :cond_d

    #@7
    iget v1, p1, Lcom/android/internal/telephony/RILRequest;->mRequest:I

    #@9
    const/16 v2, 0x164

    #@b
    if-le v1, v2, :cond_e

    #@d
    .line 408
    :cond_d
    :goto_d
    return v0

    #@e
    .line 405
    :cond_e
    sget-boolean v1, Lcom/android/internal/telephony/LgeRIL;->DBG:Z

    #@10
    if-nez v1, :cond_d

    #@12
    .line 408
    const/4 v0, 0x0

    #@13
    goto :goto_d
.end method

.method public getModemPacketCount(Ljava/lang/String;Landroid/os/Message;)V
    .registers 6
    .parameter "apn"
    .parameter "result"

    #@0
    .prologue
    .line 428
    const/16 v1, 0x157

    #@2
    invoke-static {v1, p2}, Lcom/android/internal/telephony/RILRequest;->obtain(ILandroid/os/Message;)Lcom/android/internal/telephony/RILRequest;

    #@5
    move-result-object v0

    #@6
    .line 430
    .local v0, rr:Lcom/android/internal/telephony/RILRequest;
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@8
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@b
    .line 433
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    invoke-virtual {v0}, Lcom/android/internal/telephony/RILRequest;->serialString()Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    const-string v2, "> "

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    iget v2, v0, Lcom/android/internal/telephony/RILRequest;->mRequest:I

    #@20
    invoke-static {v2}, Lcom/android/internal/telephony/LgeRIL;->requestToString(I)Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    const-string v2, " apn: "

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v1

    #@36
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/LgeRIL;->riljLog(Ljava/lang/String;)V

    #@39
    .line 436
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/LgeRIL;->sendEx(Lcom/android/internal/telephony/RILRequest;)V

    #@3c
    .line 437
    return-void
.end method

.method public iccCloseChannel(ILandroid/os/Message;)V
    .registers 6
    .parameter "channel"
    .parameter "result"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 339
    sget-boolean v1, Lcom/android/internal/telephony/LgeRIL;->FEATURE_LGRIL_API:Z

    #@3
    if-ne v1, v2, :cond_9

    #@5
    .line 340
    invoke-virtual {p0, p1, p2}, Lcom/android/internal/telephony/LgeRIL;->lgeCloseLogicalChannel(ILandroid/os/Message;)V

    #@8
    .line 354
    :goto_8
    return-void

    #@9
    .line 344
    :cond_9
    const/16 v1, 0x161

    #@b
    invoke-static {v1, p2}, Lcom/android/internal/telephony/RILRequest;->obtain(ILandroid/os/Message;)Lcom/android/internal/telephony/RILRequest;

    #@e
    move-result-object v0

    #@f
    .line 347
    .local v0, rr:Lcom/android/internal/telephony/RILRequest;
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@11
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 348
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@16
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 350
    new-instance v1, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    invoke-virtual {v0}, Lcom/android/internal/telephony/RILRequest;->serialString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    const-string v2, "> iccCloseChannel: "

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    iget v2, v0, Lcom/android/internal/telephony/RILRequest;->mRequest:I

    #@2e
    invoke-static {v2}, Lcom/android/internal/telephony/RIL;->requestToString(I)Ljava/lang/String;

    #@31
    move-result-object v2

    #@32
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v1

    #@36
    const-string v2, " "

    #@38
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v1

    #@3c
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v1

    #@40
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v1

    #@44
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/LgeRIL;->dLogD(Ljava/lang/String;)V

    #@47
    .line 353
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/LgeRIL;->sendEx(Lcom/android/internal/telephony/RILRequest;)V

    #@4a
    goto :goto_8
.end method

.method public iccExchangeAPDU(IIIIIILjava/lang/String;Landroid/os/Message;)V
    .registers 19
    .parameter "cla"
    .parameter "command"
    .parameter "channel"
    .parameter "p1"
    .parameter "p2"
    .parameter "p3"
    .parameter "data"
    .parameter "result"

    #@0
    .prologue
    .line 270
    sget-boolean v1, Lcom/android/internal/telephony/LgeRIL;->FEATURE_LGRIL_API:Z

    #@2
    const/4 v2, 0x1

    #@3
    if-ne v1, v2, :cond_1a

    #@5
    .line 271
    if-nez p3, :cond_16

    #@7
    move-object v1, p0

    #@8
    move v2, p1

    #@9
    move v3, p2

    #@a
    move v4, p4

    #@b
    move v5, p5

    #@c
    move/from16 v6, p6

    #@e
    move-object/from16 v7, p7

    #@10
    move-object/from16 v8, p8

    #@12
    invoke-virtual/range {v1 .. v8}, Lcom/android/internal/telephony/LgeRIL;->lgeSCTransmitBasic(IIIIILjava/lang/String;Landroid/os/Message;)V

    #@15
    .line 299
    :goto_15
    return-void

    #@16
    .line 272
    :cond_16
    invoke-virtual/range {p0 .. p8}, Lcom/android/internal/telephony/LgeRIL;->lgeSCTransmitChannel(IIIIIILjava/lang/String;Landroid/os/Message;)V

    #@19
    goto :goto_15

    #@1a
    .line 276
    :cond_1a
    if-nez p3, :cond_ca

    #@1c
    .line 277
    const/16 v1, 0x15f

    #@1e
    move-object/from16 v0, p8

    #@20
    invoke-static {v1, v0}, Lcom/android/internal/telephony/RILRequest;->obtain(ILandroid/os/Message;)Lcom/android/internal/telephony/RILRequest;

    #@23
    move-result-object v9

    #@24
    .line 282
    .local v9, rr:Lcom/android/internal/telephony/RILRequest;
    :goto_24
    iget-object v1, v9, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@26
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    .line 283
    iget-object v1, v9, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@2b
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@2e
    .line 284
    iget-object v1, v9, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@30
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@33
    .line 285
    iget-object v1, v9, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@35
    const/4 v2, 0x0

    #@36
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@39
    .line 286
    iget-object v1, v9, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@3b
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@3e
    .line 287
    iget-object v1, v9, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@40
    invoke-virtual {v1, p5}, Landroid/os/Parcel;->writeInt(I)V

    #@43
    .line 288
    iget-object v1, v9, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@45
    move/from16 v0, p6

    #@47
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@4a
    .line 289
    iget-object v1, v9, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@4c
    move-object/from16 v0, p7

    #@4e
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@51
    .line 290
    iget-object v1, v9, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@53
    const/4 v2, 0x0

    #@54
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@57
    .line 292
    new-instance v1, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    invoke-virtual {v9}, Lcom/android/internal/telephony/RILRequest;->serialString()Ljava/lang/String;

    #@5f
    move-result-object v2

    #@60
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v1

    #@64
    const-string v2, "> iccExchangeAPDU: "

    #@66
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v1

    #@6a
    iget v2, v9, Lcom/android/internal/telephony/RILRequest;->mRequest:I

    #@6c
    invoke-static {v2}, Lcom/android/internal/telephony/RIL;->requestToString(I)Ljava/lang/String;

    #@6f
    move-result-object v2

    #@70
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v1

    #@74
    const-string v2, " 0x"

    #@76
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v1

    #@7a
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@7d
    move-result-object v2

    #@7e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v1

    #@82
    const-string v2, " 0x"

    #@84
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v1

    #@88
    invoke-static {p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@8b
    move-result-object v2

    #@8c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v1

    #@90
    const-string v2, " 0x"

    #@92
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v1

    #@96
    invoke-static {p3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@99
    move-result-object v2

    #@9a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v1

    #@9e
    const-string v2, " "

    #@a0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v1

    #@a4
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v1

    #@a8
    const-string v2, ","

    #@aa
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v1

    #@ae
    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v1

    #@b2
    const-string v2, ","

    #@b4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v1

    #@b8
    move/from16 v0, p6

    #@ba
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v1

    #@be
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c1
    move-result-object v1

    #@c2
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/LgeRIL;->dLogD(Ljava/lang/String;)V

    #@c5
    .line 298
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/LgeRIL;->sendEx(Lcom/android/internal/telephony/RILRequest;)V

    #@c8
    goto/16 :goto_15

    #@ca
    .line 279
    .end local v9           #rr:Lcom/android/internal/telephony/RILRequest;
    :cond_ca
    const/16 v1, 0x162

    #@cc
    move-object/from16 v0, p8

    #@ce
    invoke-static {v1, v0}, Lcom/android/internal/telephony/RILRequest;->obtain(ILandroid/os/Message;)Lcom/android/internal/telephony/RILRequest;

    #@d1
    move-result-object v9

    #@d2
    .restart local v9       #rr:Lcom/android/internal/telephony/RILRequest;
    goto/16 :goto_24
.end method

.method public iccGetATR(Landroid/os/Message;)V
    .registers 5
    .parameter "result"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 366
    sget-boolean v1, Lcom/android/internal/telephony/LgeRIL;->FEATURE_LGRIL_API:Z

    #@3
    if-ne v1, v2, :cond_9

    #@5
    .line 367
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/LgeRIL;->lgeSCGetATR(Landroid/os/Message;)V

    #@8
    .line 379
    :goto_8
    return-void

    #@9
    .line 371
    :cond_9
    const/16 v1, 0x163

    #@b
    invoke-static {v1, p1}, Lcom/android/internal/telephony/RILRequest;->obtain(ILandroid/os/Message;)Lcom/android/internal/telephony/RILRequest;

    #@e
    move-result-object v0

    #@f
    .line 373
    .local v0, rr:Lcom/android/internal/telephony/RILRequest;
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@11
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 374
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@16
    const/4 v2, 0x0

    #@17
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 376
    new-instance v1, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    invoke-virtual {v0}, Lcom/android/internal/telephony/RILRequest;->serialString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    const-string v2, "> iccGetATR: "

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    iget v2, v0, Lcom/android/internal/telephony/RILRequest;->mRequest:I

    #@2f
    invoke-static {v2}, Lcom/android/internal/telephony/RIL;->requestToString(I)Ljava/lang/String;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v1

    #@3b
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/LgeRIL;->dLogD(Ljava/lang/String;)V

    #@3e
    .line 378
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/LgeRIL;->sendEx(Lcom/android/internal/telephony/RILRequest;)V

    #@41
    goto :goto_8
.end method

.method public iccOpenChannel(Ljava/lang/String;Landroid/os/Message;)V
    .registers 6
    .parameter "AID"
    .parameter "result"

    #@0
    .prologue
    .line 312
    sget-boolean v1, Lcom/android/internal/telephony/LgeRIL;->FEATURE_LGRIL_API:Z

    #@2
    const/4 v2, 0x1

    #@3
    if-ne v1, v2, :cond_9

    #@5
    .line 313
    invoke-virtual {p0, p1, p2}, Lcom/android/internal/telephony/LgeRIL;->lgeSCOpenLogicalChannel(Ljava/lang/String;Landroid/os/Message;)V

    #@8
    .line 326
    :goto_8
    return-void

    #@9
    .line 317
    :cond_9
    const/16 v1, 0x160

    #@b
    invoke-static {v1, p2}, Lcom/android/internal/telephony/RILRequest;->obtain(ILandroid/os/Message;)Lcom/android/internal/telephony/RILRequest;

    #@e
    move-result-object v0

    #@f
    .line 320
    .local v0, rr:Lcom/android/internal/telephony/RILRequest;
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@11
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 322
    new-instance v1, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    invoke-virtual {v0}, Lcom/android/internal/telephony/RILRequest;->serialString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    const-string v2, "> iccOpenChannel: "

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    iget v2, v0, Lcom/android/internal/telephony/RILRequest;->mRequest:I

    #@29
    invoke-static {v2}, Lcom/android/internal/telephony/RIL;->requestToString(I)Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    const-string v2, " "

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v1

    #@3b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v1

    #@3f
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/LgeRIL;->dLogD(Ljava/lang/String;)V

    #@42
    .line 325
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/LgeRIL;->sendEx(Lcom/android/internal/telephony/RILRequest;)V

    #@45
    goto :goto_8
.end method

.method public iccisAvailableSCOnSIM(Landroid/os/Message;)V
    .registers 5
    .parameter "result"

    #@0
    .prologue
    .line 390
    const/16 v1, 0x164

    #@2
    invoke-static {v1, p1}, Lcom/android/internal/telephony/RILRequest;->obtain(ILandroid/os/Message;)Lcom/android/internal/telephony/RILRequest;

    #@5
    move-result-object v0

    #@6
    .line 393
    .local v0, rr:Lcom/android/internal/telephony/RILRequest;
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    invoke-virtual {v0}, Lcom/android/internal/telephony/RILRequest;->serialString()Ljava/lang/String;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, "> iccisAvailableSCOnSIM: "

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    iget v2, v0, Lcom/android/internal/telephony/RILRequest;->mRequest:I

    #@1b
    invoke-static {v2}, Lcom/android/internal/telephony/RIL;->requestToString(I)Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/LgeRIL;->dLogD(Ljava/lang/String;)V

    #@2a
    .line 395
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/LgeRIL;->sendEx(Lcom/android/internal/telephony/RILRequest;)V

    #@2d
    .line 396
    return-void
.end method

.method public lgeCloseLogicalChannel(ILandroid/os/Message;)V
    .registers 6
    .parameter "sessionid"
    .parameter "result"

    #@0
    .prologue
    .line 192
    const/16 v1, 0x161

    #@2
    invoke-static {v1, p2}, Lcom/android/internal/telephony/RILRequest;->obtain(ILandroid/os/Message;)Lcom/android/internal/telephony/RILRequest;

    #@5
    move-result-object v0

    #@6
    .line 194
    .local v0, rr:Lcom/android/internal/telephony/RILRequest;
    sget-boolean v1, Lcom/android/internal/telephony/LgeRIL;->DBG:Z

    #@8
    if-eqz v1, :cond_32

    #@a
    .line 195
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    invoke-virtual {v0}, Lcom/android/internal/telephony/RILRequest;->serialString()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, "> lgeCloseLogicalChannel: "

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    iget v2, v0, Lcom/android/internal/telephony/RILRequest;->mRequest:I

    #@1f
    invoke-static {v2}, Lcom/android/internal/telephony/RIL;->requestToString(I)Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/LgeRIL;->riljLog(Ljava/lang/String;)V

    #@32
    .line 197
    :cond_32
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@34
    const/4 v2, 0x1

    #@35
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@38
    .line 198
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@3a
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@3d
    .line 199
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/LgeRIL;->sendEx(Lcom/android/internal/telephony/RILRequest;)V

    #@40
    .line 200
    return-void
.end method

.method public lgeSCGetATR(Landroid/os/Message;)V
    .registers 5
    .parameter "response"

    #@0
    .prologue
    .line 166
    const/16 v1, 0x163

    #@2
    invoke-static {v1, p1}, Lcom/android/internal/telephony/RILRequest;->obtain(ILandroid/os/Message;)Lcom/android/internal/telephony/RILRequest;

    #@5
    move-result-object v0

    #@6
    .line 168
    .local v0, rr:Lcom/android/internal/telephony/RILRequest;
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@8
    const/4 v2, 0x1

    #@9
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 169
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@e
    const/4 v2, 0x0

    #@f
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 171
    sget-boolean v1, Lcom/android/internal/telephony/LgeRIL;->DBG:Z

    #@14
    if-eqz v1, :cond_3a

    #@16
    .line 172
    new-instance v1, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    invoke-virtual {v0}, Lcom/android/internal/telephony/RILRequest;->serialString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    const-string v2, "> lgeSCGetATR: "

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    iget v2, v0, Lcom/android/internal/telephony/RILRequest;->mRequest:I

    #@2b
    invoke-static {v2}, Lcom/android/internal/telephony/LgeRIL;->requestToString(I)Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/LgeRIL;->riljLog(Ljava/lang/String;)V

    #@3a
    .line 174
    :cond_3a
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/LgeRIL;->sendEx(Lcom/android/internal/telephony/RILRequest;)V

    #@3d
    .line 175
    return-void
.end method

.method public lgeSCOpenLogicalChannel(Ljava/lang/String;Landroid/os/Message;)V
    .registers 6
    .parameter "fdname"
    .parameter "result"

    #@0
    .prologue
    .line 179
    const/16 v1, 0x160

    #@2
    invoke-static {v1, p2}, Lcom/android/internal/telephony/RILRequest;->obtain(ILandroid/os/Message;)Lcom/android/internal/telephony/RILRequest;

    #@5
    move-result-object v0

    #@6
    .line 180
    .local v0, rr:Lcom/android/internal/telephony/RILRequest;
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@8
    const/4 v2, 0x1

    #@9
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 181
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@e
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@11
    .line 183
    sget-boolean v1, Lcom/android/internal/telephony/LgeRIL;->DBG:Z

    #@13
    if-eqz v1, :cond_3d

    #@15
    .line 184
    new-instance v1, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    invoke-virtual {v0}, Lcom/android/internal/telephony/RILRequest;->serialString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    const-string v2, "> lgeSCOpenLogicalChannel: "

    #@24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    iget v2, v0, Lcom/android/internal/telephony/RILRequest;->mRequest:I

    #@2a
    invoke-static {v2}, Lcom/android/internal/telephony/RIL;->requestToString(I)Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v1

    #@36
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v1

    #@3a
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/LgeRIL;->riljLog(Ljava/lang/String;)V

    #@3d
    .line 186
    :cond_3d
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/LgeRIL;->sendEx(Lcom/android/internal/telephony/RILRequest;)V

    #@40
    .line 187
    return-void
.end method

.method public lgeSCTransmitBasic(IIIIILjava/lang/String;Landroid/os/Message;)V
    .registers 11
    .parameter "cla"
    .parameter "command"
    .parameter "p1"
    .parameter "p2"
    .parameter "p3"
    .parameter "data"
    .parameter "result"

    #@0
    .prologue
    .line 130
    const/16 v1, 0x15f

    #@2
    invoke-static {v1, p7}, Lcom/android/internal/telephony/RILRequest;->obtain(ILandroid/os/Message;)Lcom/android/internal/telephony/RILRequest;

    #@5
    move-result-object v0

    #@6
    .line 131
    .local v0, rr:Lcom/android/internal/telephony/RILRequest;
    sget-boolean v1, Lcom/android/internal/telephony/LgeRIL;->DBG:Z

    #@8
    if-eqz v1, :cond_68

    #@a
    .line 132
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    invoke-virtual {v0}, Lcom/android/internal/telephony/RILRequest;->serialString()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, "> lgeSCTransmitBasic: "

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    iget v2, v0, Lcom/android/internal/telephony/RILRequest;->mRequest:I

    #@1f
    invoke-static {v2}, Lcom/android/internal/telephony/LgeRIL;->requestToString(I)Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    const-string v2, " 0x"

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    const-string v2, " 0x"

    #@37
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v1

    #@3b
    invoke-static {p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    const-string v2, " "

    #@45
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v1

    #@49
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v1

    #@4d
    const-string v2, ","

    #@4f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v1

    #@53
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@56
    move-result-object v1

    #@57
    const-string v2, ","

    #@59
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v1

    #@5d
    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@60
    move-result-object v1

    #@61
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v1

    #@65
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/LgeRIL;->riljLog(Ljava/lang/String;)V

    #@68
    .line 136
    :cond_68
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@6a
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@6d
    .line 137
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@6f
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@72
    .line 138
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@74
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@77
    .line 139
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@79
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@7c
    .line 140
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@7e
    invoke-virtual {v1, p5}, Landroid/os/Parcel;->writeInt(I)V

    #@81
    .line 141
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@83
    invoke-virtual {v1, p6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@86
    .line 143
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/LgeRIL;->sendEx(Lcom/android/internal/telephony/RILRequest;)V

    #@89
    .line 144
    return-void
.end method

.method public lgeSCTransmitChannel(IIIIIILjava/lang/String;Landroid/os/Message;)V
    .registers 12
    .parameter "cla"
    .parameter "command"
    .parameter "sessionid"
    .parameter "p1"
    .parameter "p2"
    .parameter "p3"
    .parameter "data"
    .parameter "result"

    #@0
    .prologue
    .line 147
    const/16 v1, 0x162

    #@2
    invoke-static {v1, p8}, Lcom/android/internal/telephony/RILRequest;->obtain(ILandroid/os/Message;)Lcom/android/internal/telephony/RILRequest;

    #@5
    move-result-object v0

    #@6
    .line 148
    .local v0, rr:Lcom/android/internal/telephony/RILRequest;
    sget-boolean v1, Lcom/android/internal/telephony/LgeRIL;->DBG:Z

    #@8
    if-eqz v1, :cond_76

    #@a
    .line 149
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    invoke-virtual {v0}, Lcom/android/internal/telephony/RILRequest;->serialString()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, "> lgeSCTransmitChannel: "

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    iget v2, v0, Lcom/android/internal/telephony/RILRequest;->mRequest:I

    #@1f
    invoke-static {v2}, Lcom/android/internal/telephony/LgeRIL;->requestToString(I)Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    const-string v2, " 0x"

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    const-string v2, " 0x"

    #@37
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v1

    #@3b
    invoke-static {p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    const-string v2, " 0x"

    #@45
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v1

    #@49
    invoke-static {p3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@4c
    move-result-object v2

    #@4d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v1

    #@51
    const-string v2, " "

    #@53
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v1

    #@57
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v1

    #@5b
    const-string v2, ","

    #@5d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v1

    #@61
    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@64
    move-result-object v1

    #@65
    const-string v2, ","

    #@67
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v1

    #@6b
    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v1

    #@6f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v1

    #@73
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/LgeRIL;->riljLog(Ljava/lang/String;)V

    #@76
    .line 154
    :cond_76
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@78
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@7b
    .line 155
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@7d
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@80
    .line 156
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@82
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@85
    .line 157
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@87
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@8a
    .line 158
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@8c
    invoke-virtual {v1, p5}, Landroid/os/Parcel;->writeInt(I)V

    #@8f
    .line 159
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@91
    invoke-virtual {v1, p6}, Landroid/os/Parcel;->writeInt(I)V

    #@94
    .line 160
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@96
    invoke-virtual {v1, p7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@99
    .line 162
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/LgeRIL;->sendEx(Lcom/android/internal/telephony/RILRequest;)V

    #@9c
    .line 163
    return-void
.end method

.method protected processSolicitedEx(Lcom/android/internal/telephony/RILRequest;Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 4
    .parameter "rr"
    .parameter "p"

    #@0
    .prologue
    .line 61
    iget v0, p1, Lcom/android/internal/telephony/RILRequest;->mRequest:I

    #@2
    sparse-switch v0, :sswitch_data_38

    #@5
    .line 81
    const-string v0, "calling processsolicited of RIL from LgeRIL"

    #@7
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/LgeRIL;->riljLog(Ljava/lang/String;)V

    #@a
    .line 82
    invoke-super {p0, p1, p2}, Lcom/android/internal/telephony/RIL;->processSolicitedEx(Lcom/android/internal/telephony/RILRequest;Landroid/os/Parcel;)Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    :goto_e
    return-object v0

    #@f
    .line 66
    :sswitch_f
    invoke-virtual {p0, p2}, Lcom/android/internal/telephony/LgeRIL;->responseVoidEx(Landroid/os/Parcel;)Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    goto :goto_e

    #@14
    .line 68
    :sswitch_14
    invoke-virtual {p0, p2}, Lcom/android/internal/telephony/LgeRIL;->responseLGSmartcardResult(Landroid/os/Parcel;)Ljava/lang/Object;

    #@17
    move-result-object v0

    #@18
    goto :goto_e

    #@19
    .line 69
    :sswitch_19
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/LgeRIL;->responseLGSmartcardIccIo(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1c
    move-result-object v0

    #@1d
    goto :goto_e

    #@1e
    .line 70
    :sswitch_1e
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/LgeRIL;->responseLGSmartcardIccIo(Landroid/os/Parcel;)Ljava/lang/Object;

    #@21
    move-result-object v0

    #@22
    goto :goto_e

    #@23
    .line 71
    :sswitch_23
    invoke-virtual {p0, p2}, Lcom/android/internal/telephony/LgeRIL;->responseStringEx(Landroid/os/Parcel;)Ljava/lang/Object;

    #@26
    move-result-object v0

    #@27
    goto :goto_e

    #@28
    .line 72
    :sswitch_28
    invoke-virtual {p0, p2}, Lcom/android/internal/telephony/LgeRIL;->responseIntsEx(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2b
    move-result-object v0

    #@2c
    goto :goto_e

    #@2d
    .line 75
    :sswitch_2d
    invoke-virtual {p0, p2}, Lcom/android/internal/telephony/LgeRIL;->responseVoidEx(Landroid/os/Parcel;)Ljava/lang/Object;

    #@30
    move-result-object v0

    #@31
    goto :goto_e

    #@32
    .line 78
    :sswitch_32
    invoke-virtual {p0, p2}, Lcom/android/internal/telephony/LgeRIL;->responseIntsEx(Landroid/os/Parcel;)Ljava/lang/Object;

    #@35
    move-result-object v0

    #@36
    goto :goto_e

    #@37
    .line 61
    nop

    #@38
    :sswitch_data_38
    .sparse-switch
        0xbf -> :sswitch_2d
        0x157 -> :sswitch_32
        0x15f -> :sswitch_19
        0x160 -> :sswitch_14
        0x161 -> :sswitch_f
        0x162 -> :sswitch_1e
        0x163 -> :sswitch_23
        0x164 -> :sswitch_28
    .end sparse-switch
.end method

.method protected processUnsolicitedEx(Landroid/os/Parcel;I)V
    .registers 3
    .parameter "p"
    .parameter "response"

    #@0
    .prologue
    .line 90
    .line 92
    invoke-super {p0, p1, p2}, Lcom/android/internal/telephony/RIL;->processUnsolicitedEx(Landroid/os/Parcel;I)V

    #@3
    .line 93
    return-void
.end method

.method public responseLGSmartcardResult(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 8
    .parameter "p"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 227
    const/4 v2, 0x2

    #@3
    new-array v0, v2, [Ljava/lang/String;

    #@5
    .line 229
    .local v0, response:[Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@8
    move-result v1

    #@9
    .line 230
    .local v1, temp:I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v2

    #@d
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@10
    move-result-object v2

    #@11
    aput-object v2, v0, v4

    #@13
    .line 231
    invoke-virtual {p1}, Landroid/os/Parcel;->dataAvail()I

    #@16
    move-result v2

    #@17
    if-lez v2, :cond_66

    #@19
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    :goto_1d
    aput-object v2, v0, v5

    #@1f
    .line 233
    new-instance v2, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v3, "responseLGSmartcardResult::Type(1-int[])(2-int,String)::"

    #@26
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v2

    #@32
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/LgeRIL;->dLogD(Ljava/lang/String;)V

    #@35
    .line 234
    new-instance v2, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v3, "responseLGSmartcardResult::channel id::"

    #@3c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v2

    #@40
    aget-object v3, v0, v4

    #@42
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v2

    #@46
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v2

    #@4a
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/LgeRIL;->dLogD(Ljava/lang/String;)V

    #@4d
    .line 235
    new-instance v2, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v3, "responseLGSmartcardResult::response::"

    #@54
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v2

    #@58
    aget-object v3, v0, v5

    #@5a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v2

    #@5e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v2

    #@62
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/LgeRIL;->dLogD(Ljava/lang/String;)V

    #@65
    .line 237
    return-object v0

    #@66
    .line 231
    :cond_66
    const/4 v2, 0x0

    #@67
    goto :goto_1d
.end method
