.class public final Landroid/provider/Telephony$Mms;
.super Ljava/lang/Object;
.source "Telephony.java"

# interfaces
.implements Landroid/provider/Telephony$BaseMmsColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/Telephony;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Mms"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/provider/Telephony$Mms$Intents;,
        Landroid/provider/Telephony$Mms$ScrapSpace;,
        Landroid/provider/Telephony$Mms$Rate;,
        Landroid/provider/Telephony$Mms$DebugMms;,
        Landroid/provider/Telephony$Mms$Part;,
        Landroid/provider/Telephony$Mms$Addr;,
        Landroid/provider/Telephony$Mms$Outbox;,
        Landroid/provider/Telephony$Mms$Fail;,
        Landroid/provider/Telephony$Mms$Draft;,
        Landroid/provider/Telephony$Mms$Sent;,
        Landroid/provider/Telephony$Mms$Inbox;,
        Landroid/provider/Telephony$Mms$Temp;,
        Landroid/provider/Telephony$Mms$Save;,
        Landroid/provider/Telephony$Mms$Reserved;,
        Landroid/provider/Telephony$Mms$Spam;
    }
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final DEFAULT_SORT_ORDER:Ljava/lang/String; = "date DESC"

.field public static final NAME_ADDR_EMAIL_PATTERN:Ljava/util/regex/Pattern;

.field public static final QUOTED_STRING_PATTERN:Ljava/util/regex/Pattern;

.field public static final REPORT_REQUEST_URI:Landroid/net/Uri;

.field public static final REPORT_STATUS_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 3589
    const-string v0, "content://mms"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    #@8
    .line 3591
    sget-object v0, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    #@a
    const-string v1, "report-request"

    #@c
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@f
    move-result-object v0

    #@10
    sput-object v0, Landroid/provider/Telephony$Mms;->REPORT_REQUEST_URI:Landroid/net/Uri;

    #@12
    .line 3594
    sget-object v0, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    #@14
    const-string v1, "report-status"

    #@16
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@19
    move-result-object v0

    #@1a
    sput-object v0, Landroid/provider/Telephony$Mms;->REPORT_STATUS_URI:Landroid/net/Uri;

    #@1c
    .line 3607
    const-string v0, "\\s*(\"[^\"]*\"|[^<>\"]+)\\s*<([^<>]+)>\\s*"

    #@1e
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@21
    move-result-object v0

    #@22
    sput-object v0, Landroid/provider/Telephony$Mms;->NAME_ADDR_EMAIL_PATTERN:Ljava/util/regex/Pattern;

    #@24
    .line 3615
    const-string v0, "\\s*\"([^\"]*)\"\\s*"

    #@26
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@29
    move-result-object v0

    #@2a
    sput-object v0, Landroid/provider/Telephony$Mms;->QUOTED_STRING_PATTERN:Ljava/util/regex/Pattern;

    #@2c
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 3585
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 4017
    return-void
.end method

.method public static extractAddrSpec(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "address"

    #@0
    .prologue
    .line 3658
    sget-object v1, Landroid/provider/Telephony$Mms;->NAME_ADDR_EMAIL_PATTERN:Ljava/util/regex/Pattern;

    #@2
    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@5
    move-result-object v0

    #@6
    .line 3660
    .local v0, match:Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_11

    #@c
    .line 3661
    const/4 v1, 0x2

    #@d
    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@10
    move-result-object p0

    #@11
    .line 3663
    .end local p0
    :cond_11
    return-object p0
.end method

.method public static final getMessageBoxName(I)Ljava/lang/String;
    .registers 4
    .parameter "msgBox"

    #@0
    .prologue
    .line 3631
    packed-switch p0, :pswitch_data_38

    #@3
    .line 3653
    :pswitch_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "Invalid message box: "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 3633
    :pswitch_1c
    const-string v0, "all"

    #@1e
    .line 3650
    :goto_1e
    return-object v0

    #@1f
    .line 3635
    :pswitch_1f
    const-string v0, "inbox"

    #@21
    goto :goto_1e

    #@22
    .line 3637
    :pswitch_22
    const-string v0, "sent"

    #@24
    goto :goto_1e

    #@25
    .line 3639
    :pswitch_25
    const-string v0, "drafts"

    #@27
    goto :goto_1e

    #@28
    .line 3641
    :pswitch_28
    const-string v0, "outbox"

    #@2a
    goto :goto_1e

    #@2b
    .line 3644
    :pswitch_2b
    const-string v0, "spam"

    #@2d
    goto :goto_1e

    #@2e
    .line 3646
    :pswitch_2e
    const-string v0, "reserved"

    #@30
    goto :goto_1e

    #@31
    .line 3648
    :pswitch_31
    const-string v0, "save"

    #@33
    goto :goto_1e

    #@34
    .line 3650
    :pswitch_34
    const-string v0, "advertisement"

    #@36
    goto :goto_1e

    #@37
    .line 3631
    nop

    #@38
    :pswitch_data_38
    .packed-switch 0x0
        :pswitch_1c
        :pswitch_1f
        :pswitch_22
        :pswitch_25
        :pswitch_28
        :pswitch_3
        :pswitch_3
        :pswitch_2b
        :pswitch_2e
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_31
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_34
    .end packed-switch
.end method

.method public static isEmailAddress(Ljava/lang/String;)Z
    .registers 4
    .parameter "address"

    #@0
    .prologue
    .line 3673
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v2

    #@4
    if-eqz v2, :cond_8

    #@6
    .line 3674
    const/4 v2, 0x0

    #@7
    .line 3679
    :goto_7
    return v2

    #@8
    .line 3677
    :cond_8
    invoke-static {p0}, Landroid/provider/Telephony$Mms;->extractAddrSpec(Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    .line 3678
    .local v1, s:Ljava/lang/String;
    sget-object v2, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    #@e
    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@11
    move-result-object v0

    #@12
    .line 3679
    .local v0, match:Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    #@15
    move-result v2

    #@16
    goto :goto_7
.end method

.method public static isPhoneNumber(Ljava/lang/String;)Z
    .registers 3
    .parameter "number"

    #@0
    .prologue
    .line 3753
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_8

    #@6
    .line 3754
    const/4 v1, 0x0

    #@7
    .line 3758
    :goto_7
    return v1

    #@8
    .line 3757
    :cond_8
    sget-object v1, Landroid/util/Patterns;->PHONE:Ljava/util/regex/Pattern;

    #@a
    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@d
    move-result-object v0

    #@e
    .line 3758
    .local v0, match:Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    #@11
    move-result v1

    #@12
    goto :goto_7
.end method

.method public static final query(Landroid/content/ContentResolver;[Ljava/lang/String;)Landroid/database/Cursor;
    .registers 8
    .parameter "cr"
    .parameter "projection"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 3620
    sget-object v1, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    #@3
    const-string v5, "date DESC"

    #@5
    move-object v0, p0

    #@6
    move-object v2, p1

    #@7
    move-object v4, v3

    #@8
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method public static final query(Landroid/content/ContentResolver;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 10
    .parameter "cr"
    .parameter "projection"
    .parameter "where"
    .parameter "orderBy"

    #@0
    .prologue
    .line 3626
    sget-object v1, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    #@2
    const/4 v4, 0x0

    #@3
    if-nez p3, :cond_f

    #@5
    const-string v5, "date DESC"

    #@7
    :goto_7
    move-object v0, p0

    #@8
    move-object v2, p1

    #@9
    move-object v3, p2

    #@a
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@d
    move-result-object v0

    #@e
    return-object v0

    #@f
    :cond_f
    move-object v5, p3

    #@10
    goto :goto_7
.end method
