.class public final Landroid/provider/Telephony$Sms$Kpas;
.super Ljava/lang/Object;
.source "Telephony.java"

# interfaces
.implements Landroid/provider/BaseColumns;
.implements Landroid/provider/Telephony$TextBasedSmsColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/Telephony$Sms;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Kpas"
.end annotation


# static fields
.field public static final DEFAULT_SORT_ORDER:Ljava/lang/String; = "date DESC"

.field public static final KPAS_BODY:Ljava/lang/String; = "body"

.field public static final KPAS_CONTENT_URI:Landroid/net/Uri; = null

.field public static final KPAS_MESSAGEID:Ljava/lang/String; = "kpas_messageid"

.field public static final KPAS_SERIALNUMBER:Ljava/lang/String; = "kpas_serialnumber"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 2576
    const-string v0, "content://sms/kpas"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/provider/Telephony$Sms$Kpas;->KPAS_CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 2572
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static addMessage(Landroid/content/Context;Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;IIJLjava/lang/String;II)Landroid/net/Uri;
    .registers 14
    .parameter "context"
    .parameter "resolver"
    .parameter "address"
    .parameter "name"
    .parameter "tag"
    .parameter "tag_eng"
    .parameter "threadId"
    .parameter "body"
    .parameter "message_id"
    .parameter "serialnumber"

    #@0
    .prologue
    .line 2604
    new-instance v0, Landroid/content/ContentValues;

    #@2
    const/16 v1, 0x8

    #@4
    invoke-direct {v0, v1}, Landroid/content/ContentValues;-><init>(I)V

    #@7
    .line 2605
    .local v0, values:Landroid/content/ContentValues;
    const-string v1, "address"

    #@9
    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@c
    .line 2606
    const-string v1, "name"

    #@e
    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 2607
    const-string v1, "tag"

    #@13
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@1a
    .line 2608
    const-string v1, "tag_eng"

    #@1c
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@23
    .line 2609
    const-wide/16 v1, -0x1

    #@25
    cmp-long v1, p6, v1

    #@27
    if-eqz v1, :cond_32

    #@29
    .line 2610
    const-string v1, "thread_id"

    #@2b
    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@32
    .line 2612
    :cond_32
    const-string v1, "body"

    #@34
    invoke-virtual {v0, v1, p8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@37
    .line 2613
    const-string v1, "kpas_messageid"

    #@39
    invoke-static {p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@40
    .line 2614
    const-string v1, "kpas_serialnumber"

    #@42
    invoke-static {p10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@45
    move-result-object v2

    #@46
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@49
    .line 2615
    sget-object v1, Landroid/provider/Telephony$Sms$Kpas;->KPAS_CONTENT_URI:Landroid/net/Uri;

    #@4b
    invoke-virtual {p1, v1, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@4e
    move-result-object v1

    #@4f
    return-object v1
.end method
