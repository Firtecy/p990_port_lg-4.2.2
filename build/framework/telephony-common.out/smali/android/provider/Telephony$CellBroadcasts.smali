.class public final Landroid/provider/Telephony$CellBroadcasts;
.super Ljava/lang/Object;
.source "Telephony.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/Telephony;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CellBroadcasts"
.end annotation


# static fields
.field public static final CID:Ljava/lang/String; = "cid"

.field public static final CMAS_CATEGORY:Ljava/lang/String; = "cmas_category"

.field public static final CMAS_CERTAINTY:Ljava/lang/String; = "cmas_certainty"

.field public static final CMAS_MESSAGE_CLASS:Ljava/lang/String; = "cmas_message_class"

.field public static final CMAS_RESPONSE_TYPE:Ljava/lang/String; = "cmas_response_type"

.field public static final CMAS_SEVERITY:Ljava/lang/String; = "cmas_severity"

.field public static final CMAS_URGENCY:Ljava/lang/String; = "cmas_urgency"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final DEFAULT_SORT_ORDER:Ljava/lang/String; = "date DESC"

.field public static final DELIVERY_TIME:Ljava/lang/String; = "date"

.field public static final ETWS_WARNING_TYPE:Ljava/lang/String; = "etws_warning_type"

.field public static final GEOGRAPHICAL_SCOPE:Ljava/lang/String; = "geo_scope"

.field public static final LAC:Ljava/lang/String; = "lac"

.field public static final LANGUAGE_CODE:Ljava/lang/String; = "language"

.field public static final MESSAGE_BODY:Ljava/lang/String; = "body"

.field public static final MESSAGE_FORMAT:Ljava/lang/String; = "format"

.field public static final MESSAGE_PRIORITY:Ljava/lang/String; = "priority"

.field public static final MESSAGE_READ:Ljava/lang/String; = "read"

.field public static final PLMN:Ljava/lang/String; = "plmn"

.field public static final QUERY_COLUMNS:[Ljava/lang/String; = null

.field public static final SERIAL_NUMBER:Ljava/lang/String; = "serial_number"

.field public static final SERVICE_CATEGORY:Ljava/lang/String; = "service_category"

.field public static final V1_MESSAGE_CODE:Ljava/lang/String; = "message_code"

.field public static final V1_MESSAGE_IDENTIFIER:Ljava/lang/String; = "message_id"


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 6934
    const-string v0, "content://cellbroadcasts"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/provider/Telephony$CellBroadcasts;->CONTENT_URI:Landroid/net/Uri;

    #@8
    .line 7074
    const/16 v0, 0x14

    #@a
    new-array v0, v0, [Ljava/lang/String;

    #@c
    const/4 v1, 0x0

    #@d
    const-string v2, "_id"

    #@f
    aput-object v2, v0, v1

    #@11
    const/4 v1, 0x1

    #@12
    const-string v2, "geo_scope"

    #@14
    aput-object v2, v0, v1

    #@16
    const/4 v1, 0x2

    #@17
    const-string v2, "plmn"

    #@19
    aput-object v2, v0, v1

    #@1b
    const/4 v1, 0x3

    #@1c
    const-string v2, "lac"

    #@1e
    aput-object v2, v0, v1

    #@20
    const/4 v1, 0x4

    #@21
    const-string v2, "cid"

    #@23
    aput-object v2, v0, v1

    #@25
    const/4 v1, 0x5

    #@26
    const-string v2, "serial_number"

    #@28
    aput-object v2, v0, v1

    #@2a
    const/4 v1, 0x6

    #@2b
    const-string v2, "service_category"

    #@2d
    aput-object v2, v0, v1

    #@2f
    const/4 v1, 0x7

    #@30
    const-string v2, "language"

    #@32
    aput-object v2, v0, v1

    #@34
    const/16 v1, 0x8

    #@36
    const-string v2, "body"

    #@38
    aput-object v2, v0, v1

    #@3a
    const/16 v1, 0x9

    #@3c
    const-string v2, "date"

    #@3e
    aput-object v2, v0, v1

    #@40
    const/16 v1, 0xa

    #@42
    const-string v2, "read"

    #@44
    aput-object v2, v0, v1

    #@46
    const/16 v1, 0xb

    #@48
    const-string v2, "format"

    #@4a
    aput-object v2, v0, v1

    #@4c
    const/16 v1, 0xc

    #@4e
    const-string v2, "priority"

    #@50
    aput-object v2, v0, v1

    #@52
    const/16 v1, 0xd

    #@54
    const-string v2, "etws_warning_type"

    #@56
    aput-object v2, v0, v1

    #@58
    const/16 v1, 0xe

    #@5a
    const-string v2, "cmas_message_class"

    #@5c
    aput-object v2, v0, v1

    #@5e
    const/16 v1, 0xf

    #@60
    const-string v2, "cmas_category"

    #@62
    aput-object v2, v0, v1

    #@64
    const/16 v1, 0x10

    #@66
    const-string v2, "cmas_response_type"

    #@68
    aput-object v2, v0, v1

    #@6a
    const/16 v1, 0x11

    #@6c
    const-string v2, "cmas_severity"

    #@6e
    aput-object v2, v0, v1

    #@70
    const/16 v1, 0x12

    #@72
    const-string v2, "cmas_urgency"

    #@74
    aput-object v2, v0, v1

    #@76
    const/16 v1, 0x13

    #@78
    const-string v2, "cmas_certainty"

    #@7a
    aput-object v2, v0, v1

    #@7c
    sput-object v0, Landroid/provider/Telephony$CellBroadcasts;->QUERY_COLUMNS:[Ljava/lang/String;

    #@7e
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 6929
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
