.class public final Landroid/provider/Telephony$Fota;
.super Ljava/lang/Object;
.source "Telephony.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/Telephony;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Fota"
.end annotation


# static fields
.field public static final BODY:Ljava/lang/String; = "body"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final DATE:Ljava/lang/String; = "date"

.field public static final DEFAULT_SORT_ORDER:Ljava/lang/String; = "date DESC"

.field public static final EXEINFO:Ljava/lang/String; = "exeinfo"

.field public static final PREF_KEY_NOTIFICATION_METHOD:Ljava/lang/String; = "lge_pref_key_notification_method"

.field public static final PREF_KEY_NOTIFICATION_OF_THE_CALL:Ljava/lang/String; = "lge_pref_key_notification_of_the_call"

.field public static final PREF_KEY_NOTIFICATION_SOUND:Ljava/lang/String; = "lge_pref_key_notification_sound"

.field public static final PREF_KEY_NOTIFICATION_VIBRATION:Ljava/lang/String; = "lge_pref_key_notification_vibration"

.field public static final PREF_VALUE_ERROR:I = -0x1

.field public static final PREF_VALUE_NOTI_CALL_OFF:I = 0x2

.field public static final PREF_VALUE_NOTI_CALL_SILENT:I = 0x2

.field public static final PREF_VALUE_NOTI_CALL_SOUND:I = 0x0

.field public static final PREF_VALUE_NOTI_CALL_VIBRATION:I = 0x1

.field public static final PREF_VALUE_NOTI_VIBRATION_ALWAYS:I = 0x0

.field public static final PREF_VALUE_NOTI_VIBRATION_NEVER:I = 0x2

.field public static final PREF_VALUE_NOTI_VIBRATION_WHENSILENT:I = 0x1

.field public static final TABLE_FOTA:Ljava/lang/String; = "fota"


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 7203
    sget-object v0, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    #@2
    const-string v1, "fota"

    #@4
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@7
    move-result-object v0

    #@8
    sput-object v0, Landroid/provider/Telephony$Fota;->CONTENT_URI:Landroid/net/Uri;

    #@a
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 7201
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method static synthetic access$000(Landroid/media/MediaPlayer;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 7201
    invoke-static {p0}, Landroid/provider/Telephony$Fota;->cleanupPlayer(Landroid/media/MediaPlayer;)V

    #@3
    return-void
.end method

.method public static addFotaSmsToUri(Landroid/content/ContentResolver;Landroid/net/Uri;JILjava/lang/String;)Landroid/net/Uri;
    .registers 9
    .parameter "resolver"
    .parameter "uri"
    .parameter "date"
    .parameter "exeinfo"
    .parameter "body"

    #@0
    .prologue
    .line 7231
    new-instance v0, Landroid/content/ContentValues;

    #@2
    const/4 v1, 0x3

    #@3
    invoke-direct {v0, v1}, Landroid/content/ContentValues;-><init>(I)V

    #@6
    .line 7232
    .local v0, values:Landroid/content/ContentValues;
    const-string v1, "date"

    #@8
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@f
    .line 7233
    const-string v1, "exeinfo"

    #@11
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@18
    .line 7234
    const-string v1, "body"

    #@1a
    invoke-virtual {v0, v1, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    .line 7235
    invoke-virtual {p0, p1, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@20
    move-result-object v1

    #@21
    return-object v1
.end method

.method private static cleanupPlayer(Landroid/media/MediaPlayer;)V
    .registers 2
    .parameter "mp"

    #@0
    .prologue
    .line 7371
    if-eqz p0, :cond_8

    #@2
    .line 7373
    :try_start_2
    invoke-virtual {p0}, Landroid/media/MediaPlayer;->stop()V

    #@5
    .line 7374
    invoke-virtual {p0}, Landroid/media/MediaPlayer;->release()V
    :try_end_8
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_8} :catch_9

    #@8
    .line 7379
    :cond_8
    :goto_8
    return-void

    #@9
    .line 7375
    :catch_9
    move-exception v0

    #@a
    goto :goto_8
.end method

.method public static deleteFotaSmsToUri(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 5
    .parameter "resolver"
    .parameter "uri"
    .parameter "where"
    .parameter "selectionArgs"

    #@0
    .prologue
    .line 7243
    invoke-virtual {p0, p1, p2, p3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private static getNotiCall(Landroid/content/Context;)I
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 7312
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@3
    move-result-object v1

    #@4
    .line 7315
    .local v1, sharedPreferences:Landroid/content/SharedPreferences;
    const-string v2, "lge_pref_key_notification_of_the_call"

    #@6
    const-string v3, "TurnOff"

    #@8
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    .line 7316
    .local v0, notiCall:Ljava/lang/String;
    const-string v2, "Vibration"

    #@e
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v2

    #@12
    if-eqz v2, :cond_16

    #@14
    .line 7317
    const/4 v2, 0x1

    #@15
    .line 7323
    :goto_15
    return v2

    #@16
    .line 7318
    :cond_16
    const-string v2, "Sound"

    #@18
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v2

    #@1c
    if-eqz v2, :cond_20

    #@1e
    .line 7319
    const/4 v2, 0x0

    #@1f
    goto :goto_15

    #@20
    .line 7320
    :cond_20
    const-string v2, "TurnOff"

    #@22
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v2

    #@26
    if-eqz v2, :cond_2a

    #@28
    .line 7321
    const/4 v2, 0x2

    #@29
    goto :goto_15

    #@2a
    .line 7323
    :cond_2a
    const/4 v2, -0x1

    #@2b
    goto :goto_15
.end method

.method private static getNotiMethodSet(Landroid/content/Context;)I
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    .line 7295
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@4
    move-result-object v1

    #@5
    .line 7296
    .local v1, sharedPreferences:Landroid/content/SharedPreferences;
    const-string v3, "lge_pref_key_notification_method"

    #@7
    const-string v4, "Sound"

    #@9
    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    .line 7298
    .local v0, notiMethodValue:Ljava/lang/String;
    const-string v3, "Vibration"

    #@f
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v3

    #@13
    if-eqz v3, :cond_17

    #@15
    .line 7299
    const/4 v2, 0x1

    #@16
    .line 7307
    :cond_16
    :goto_16
    return v2

    #@17
    .line 7300
    :cond_17
    const-string v3, "Sound"

    #@19
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_21

    #@1f
    .line 7301
    const/4 v2, 0x0

    #@20
    goto :goto_16

    #@21
    .line 7302
    :cond_21
    const-string v3, "TurnOff"

    #@23
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26
    move-result v3

    #@27
    if-nez v3, :cond_16

    #@29
    .line 7304
    const-string v3, "Silent"

    #@2b
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v3

    #@2f
    if-nez v3, :cond_16

    #@31
    .line 7307
    const/4 v2, -0x1

    #@32
    goto :goto_16
.end method

.method public static getNotification(Landroid/content/Context;Landroid/app/Notification;)Landroid/app/Notification;
    .registers 10
    .parameter "context"
    .parameter "notification"

    #@0
    .prologue
    const/4 v7, 0x2

    #@1
    const/4 v6, 0x1

    #@2
    .line 7247
    const-string v5, "audio"

    #@4
    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Landroid/media/AudioManager;

    #@a
    .line 7248
    .local v0, audioManager:Landroid/media/AudioManager;
    invoke-static {p0}, Landroid/provider/Telephony$Fota;->getPreferenceDefaultNotiSoundValue(Landroid/content/Context;)Ljava/lang/String;

    #@d
    move-result-object v4

    #@e
    .line 7249
    .local v4, ringtoneStr:Ljava/lang/String;
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    #@11
    move-result v2

    #@12
    .line 7250
    .local v2, audioManagerRingerMode:I
    invoke-virtual {v0}, Landroid/media/AudioManager;->getMode()I

    #@15
    move-result v1

    #@16
    .line 7251
    .local v1, audioManagerMode:I
    invoke-static {p0}, Landroid/provider/Telephony$Fota;->getNotiMethodSet(Landroid/content/Context;)I

    #@19
    move-result v3

    #@1a
    .line 7253
    .local v3, preferenceNotiMethodSet:I
    if-ne v1, v7, :cond_3a

    #@1c
    .line 7254
    invoke-static {p0}, Landroid/provider/Telephony$Fota;->getNotiCall(Landroid/content/Context;)I

    #@1f
    move-result v5

    #@20
    if-ne v5, v6, :cond_29

    #@22
    .line 7255
    iget v5, p1, Landroid/app/Notification;->defaults:I

    #@24
    or-int/lit8 v5, v5, 0x2

    #@26
    iput v5, p1, Landroid/app/Notification;->defaults:I

    #@28
    .line 7283
    :cond_28
    :goto_28
    return-object p1

    #@29
    .line 7256
    :cond_29
    invoke-static {p0}, Landroid/provider/Telephony$Fota;->getNotiCall(Landroid/content/Context;)I

    #@2c
    move-result v5

    #@2d
    if-nez v5, :cond_33

    #@2f
    .line 7257
    invoke-static {p0, p1, v4}, Landroid/provider/Telephony$Fota;->setSoundRingtone(Landroid/content/Context;Landroid/app/Notification;Ljava/lang/String;)V

    #@32
    goto :goto_28

    #@33
    .line 7259
    :cond_33
    iget v5, p1, Landroid/app/Notification;->defaults:I

    #@35
    or-int/lit8 v5, v5, -0x4

    #@37
    iput v5, p1, Landroid/app/Notification;->defaults:I

    #@39
    goto :goto_28

    #@3a
    .line 7261
    :cond_3a
    packed-switch v2, :pswitch_data_72

    #@3d
    goto :goto_28

    #@3e
    .line 7277
    :pswitch_3e
    iget v5, p1, Landroid/app/Notification;->defaults:I

    #@40
    or-int/lit8 v5, v5, -0x4

    #@42
    iput v5, p1, Landroid/app/Notification;->defaults:I

    #@44
    goto :goto_28

    #@45
    .line 7263
    :pswitch_45
    if-nez v3, :cond_4b

    #@47
    .line 7264
    invoke-static {p0, p1, v4}, Landroid/provider/Telephony$Fota;->setSoundRingtone(Landroid/content/Context;Landroid/app/Notification;Ljava/lang/String;)V

    #@4a
    goto :goto_28

    #@4b
    .line 7265
    :cond_4b
    if-ne v3, v6, :cond_54

    #@4d
    .line 7266
    iget v5, p1, Landroid/app/Notification;->defaults:I

    #@4f
    or-int/lit8 v5, v5, 0x2

    #@51
    iput v5, p1, Landroid/app/Notification;->defaults:I

    #@53
    goto :goto_28

    #@54
    .line 7267
    :cond_54
    if-ne v3, v7, :cond_28

    #@56
    .line 7268
    iget v5, p1, Landroid/app/Notification;->defaults:I

    #@58
    or-int/lit8 v5, v5, -0x4

    #@5a
    iput v5, p1, Landroid/app/Notification;->defaults:I

    #@5c
    goto :goto_28

    #@5d
    .line 7271
    :pswitch_5d
    if-eq v3, v6, :cond_61

    #@5f
    if-nez v3, :cond_68

    #@61
    .line 7272
    :cond_61
    iget v5, p1, Landroid/app/Notification;->defaults:I

    #@63
    or-int/lit8 v5, v5, 0x2

    #@65
    iput v5, p1, Landroid/app/Notification;->defaults:I

    #@67
    goto :goto_28

    #@68
    .line 7273
    :cond_68
    if-ne v3, v7, :cond_28

    #@6a
    .line 7274
    iget v5, p1, Landroid/app/Notification;->defaults:I

    #@6c
    or-int/lit8 v5, v5, -0x4

    #@6e
    iput v5, p1, Landroid/app/Notification;->defaults:I

    #@70
    goto :goto_28

    #@71
    .line 7261
    nop

    #@72
    :pswitch_data_72
    .packed-switch 0x0
        :pswitch_3e
        :pswitch_5d
        :pswitch_45
    .end packed-switch
.end method

.method private static getPreferenceDefaultNotiSoundValue(Landroid/content/Context;)Ljava/lang/String;
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 7288
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@3
    move-result-object v1

    #@4
    .line 7289
    .local v1, sharedPreferences:Landroid/content/SharedPreferences;
    const-string v2, "lge_pref_key_notification_sound"

    #@6
    const-string v3, "content://settings/system/notification_sound"

    #@8
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    .line 7290
    .local v0, returnValue:Ljava/lang/String;
    return-object v0
.end method

.method public static final query(Landroid/content/ContentResolver;[Ljava/lang/String;)Landroid/database/Cursor;
    .registers 8
    .parameter "cr"
    .parameter "projection"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 7227
    sget-object v1, Landroid/provider/Telephony$Fota;->CONTENT_URI:Landroid/net/Uri;

    #@3
    const-string v5, "date DESC"

    #@5
    move-object v0, p0

    #@6
    move-object v2, p1

    #@7
    move-object v4, v3

    #@8
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method private static setSoundRingtone(Landroid/content/Context;Landroid/app/Notification;Ljava/lang/String;)V
    .registers 10
    .parameter "context"
    .parameter "notification"
    .parameter "ringtoneStr"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 7327
    const-string v1, "/system/media/audio/ui/SMSinCall.ogg"

    #@3
    .line 7328
    .local v1, filepath:Ljava/lang/String;
    const-string v5, "phone"

    #@5
    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@8
    move-result-object v3

    #@9
    check-cast v3, Landroid/telephony/TelephonyManager;

    #@b
    .line 7331
    .local v3, tm:Landroid/telephony/TelephonyManager;
    const-string v5, "audio"

    #@d
    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@10
    move-result-object v0

    #@11
    check-cast v0, Landroid/media/AudioManager;

    #@13
    .line 7334
    .local v0, au:Landroid/media/AudioManager;
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getCallState()I

    #@16
    move-result v5

    #@17
    const/4 v6, 0x2

    #@18
    if-ne v5, v6, :cond_41

    #@1a
    .line 7338
    iput-object v4, p1, Landroid/app/Notification;->sound:Landroid/net/Uri;

    #@1c
    .line 7339
    new-instance v2, Landroid/media/MediaPlayer;

    #@1e
    invoke-direct {v2}, Landroid/media/MediaPlayer;-><init>()V

    #@21
    .line 7340
    .local v2, mMediaPlayer:Landroid/media/MediaPlayer;
    if-eqz v2, :cond_40

    #@23
    .line 7342
    :try_start_23
    invoke-virtual {v2, v1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    #@26
    .line 7343
    const/4 v4, 0x0

    #@27
    invoke-virtual {v2, v4}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    #@2a
    .line 7344
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->prepare()V

    #@2d
    .line 7345
    new-instance v4, Landroid/provider/Telephony$Fota$1;

    #@2f
    invoke-direct {v4}, Landroid/provider/Telephony$Fota$1;-><init>()V

    #@32
    invoke-virtual {v2, v4}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    #@35
    .line 7350
    new-instance v4, Landroid/provider/Telephony$Fota$2;

    #@37
    invoke-direct {v4}, Landroid/provider/Telephony$Fota$2;-><init>()V

    #@3a
    invoke-virtual {v2, v4}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    #@3d
    .line 7356
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->start()V
    :try_end_40
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_40} :catch_53
    .catch Ljava/lang/IllegalArgumentException; {:try_start_23 .. :try_end_40} :catch_51
    .catch Ljava/lang/IllegalStateException; {:try_start_23 .. :try_end_40} :catch_4f

    #@40
    .line 7368
    .end local v2           #mMediaPlayer:Landroid/media/MediaPlayer;
    :cond_40
    :goto_40
    return-void

    #@41
    .line 7366
    :cond_41
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@44
    move-result v5

    #@45
    if-eqz v5, :cond_4a

    #@47
    :goto_47
    iput-object v4, p1, Landroid/app/Notification;->sound:Landroid/net/Uri;

    #@49
    goto :goto_40

    #@4a
    :cond_4a
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@4d
    move-result-object v4

    #@4e
    goto :goto_47

    #@4f
    .line 7361
    .restart local v2       #mMediaPlayer:Landroid/media/MediaPlayer;
    :catch_4f
    move-exception v4

    #@50
    goto :goto_40

    #@51
    .line 7359
    :catch_51
    move-exception v4

    #@52
    goto :goto_40

    #@53
    .line 7357
    :catch_53
    move-exception v4

    #@54
    goto :goto_40
.end method

.method public static updateFotaSmsToUri(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;)I
    .registers 4
    .parameter "resolver"
    .parameter "uri"
    .parameter "values"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 7239
    invoke-virtual {p0, p1, p2, v0, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@4
    move-result v0

    #@5
    return v0
.end method
