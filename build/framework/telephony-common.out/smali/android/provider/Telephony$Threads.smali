.class public final Landroid/provider/Telephony$Threads;
.super Ljava/lang/Object;
.source "Telephony.java"

# interfaces
.implements Landroid/provider/Telephony$ThreadsColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/Telephony;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Threads"
.end annotation


# static fields
.field public static final BROADCAST_THREAD:I = 0x1

.field public static final COMMON_THREAD:I = 0x0

.field public static final CONTENT_SIMPLE_URI:Landroid/net/Uri; = null

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final CONTENT_URI_CMAS:Landroid/net/Uri; = null

.field private static final ID_PROJECTION:[Ljava/lang/String; = null

.field private static final NOW_THREAD_ID_CONTENT_URI:Landroid/net/Uri; = null

.field public static final OBSOLETE_THREADS_URI:Landroid/net/Uri; = null

.field public static final SPAM_THREAD:I = 0x2

.field private static final SPAM_THREAD_ID_CONTENT_URI:Landroid/net/Uri; = null

.field private static final STANDARD_ENCODING:Ljava/lang/String; = "UTF-8"

.field private static final THREAD_ID_CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 3360
    const/4 v0, 0x1

    #@1
    new-array v0, v0, [Ljava/lang/String;

    #@3
    const/4 v1, 0x0

    #@4
    const-string v2, "_id"

    #@6
    aput-object v2, v0, v1

    #@8
    sput-object v0, Landroid/provider/Telephony$Threads;->ID_PROJECTION:[Ljava/lang/String;

    #@a
    .line 3362
    const-string v0, "content://mms-sms/threadID"

    #@c
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@f
    move-result-object v0

    #@10
    sput-object v0, Landroid/provider/Telephony$Threads;->THREAD_ID_CONTENT_URI:Landroid/net/Uri;

    #@12
    .line 3365
    const-string v0, "content://mms-sms/nowThreadID"

    #@14
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@17
    move-result-object v0

    #@18
    sput-object v0, Landroid/provider/Telephony$Threads;->NOW_THREAD_ID_CONTENT_URI:Landroid/net/Uri;

    #@1a
    .line 3368
    const-string v0, "content://mms-sms/spamThreadID"

    #@1c
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@1f
    move-result-object v0

    #@20
    sput-object v0, Landroid/provider/Telephony$Threads;->SPAM_THREAD_ID_CONTENT_URI:Landroid/net/Uri;

    #@22
    .line 3371
    sget-object v0, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    #@24
    const-string v1, "conversations"

    #@26
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@29
    move-result-object v0

    #@2a
    sput-object v0, Landroid/provider/Telephony$Threads;->CONTENT_URI:Landroid/net/Uri;

    #@2c
    .line 3376
    sget-object v0, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    #@2e
    const-string v1, "conversations_cmas"

    #@30
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@33
    move-result-object v0

    #@34
    sput-object v0, Landroid/provider/Telephony$Threads;->CONTENT_URI_CMAS:Landroid/net/Uri;

    #@36
    .line 3379
    sget-object v0, Landroid/provider/Telephony$Threads;->CONTENT_URI:Landroid/net/Uri;

    #@38
    const-string v1, "obsolete"

    #@3a
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@3d
    move-result-object v0

    #@3e
    sput-object v0, Landroid/provider/Telephony$Threads;->OBSOLETE_THREADS_URI:Landroid/net/Uri;

    #@40
    .line 3383
    sget-object v0, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    #@42
    const-string v1, "simple-conversations"

    #@44
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@47
    move-result-object v0

    #@48
    sput-object v0, Landroid/provider/Telephony$Threads;->CONTENT_SIMPLE_URI:Landroid/net/Uri;

    #@4a
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 3393
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 3394
    return-void
.end method

.method public static getOrCreateSpamThreadId(Landroid/content/Context;)J
    .registers 10
    .parameter "context"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 3551
    sget-object v0, Landroid/provider/Telephony$Threads;->SPAM_THREAD_ID_CONTENT_URI:Landroid/net/Uri;

    #@3
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    #@6
    move-result-object v8

    #@7
    .line 3553
    .local v8, uriBuilder:Landroid/net/Uri$Builder;
    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    #@a
    move-result-object v2

    #@b
    .line 3555
    .local v2, uri:Landroid/net/Uri;
    const-string v0, "tag"

    #@d
    new-instance v1, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "getOrCreateSpamThreadId uri: "

    #@14
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 3557
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@26
    move-result-object v1

    #@27
    sget-object v3, Landroid/provider/Telephony$Threads;->ID_PROJECTION:[Ljava/lang/String;

    #@29
    move-object v0, p0

    #@2a
    move-object v5, v4

    #@2b
    move-object v6, v4

    #@2c
    invoke-static/range {v0 .. v6}, Landroid/database/sqlite/SqliteWrapper;->query(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@2f
    move-result-object v7

    #@30
    .line 3559
    .local v7, cursor:Landroid/database/Cursor;
    if-eqz v7, :cond_73

    #@32
    .line 3561
    const-string v0, "tag"

    #@34
    new-instance v1, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v3, "getOrCreateSpamThreadId cursor cnt: "

    #@3b
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v1

    #@3f
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    #@42
    move-result v3

    #@43
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v1

    #@4b
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 3564
    :try_start_4e
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    #@51
    move-result v0

    #@52
    if-eqz v0, :cond_5d

    #@54
    .line 3565
    const/4 v0, 0x0

    #@55
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_58
    .catchall {:try_start_4e .. :try_end_58} :catchall_97

    #@58
    move-result-wide v0

    #@59
    .line 3572
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@5c
    :goto_5c
    return-wide v0

    #@5d
    .line 3566
    :cond_5d
    :try_start_5d
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I
    :try_end_60
    .catchall {:try_start_5d .. :try_end_60} :catchall_97

    #@60
    move-result v0

    #@61
    if-nez v0, :cond_69

    #@63
    .line 3567
    const-wide/16 v0, 0x0

    #@65
    .line 3572
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@68
    goto :goto_5c

    #@69
    .line 3569
    :cond_69
    :try_start_69
    const-string v0, "tag"

    #@6b
    const-string v1, "getOrCreateSpamThreadId returned no rows!"

    #@6d
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_70
    .catchall {:try_start_69 .. :try_end_70} :catchall_97

    #@70
    .line 3572
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@73
    .line 3576
    :cond_73
    const-string v0, "tag"

    #@75
    new-instance v1, Ljava/lang/StringBuilder;

    #@77
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7a
    const-string v3, "getOrCreateSpamThreadId failed with uri "

    #@7c
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v1

    #@80
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@83
    move-result-object v3

    #@84
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v1

    #@88
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8b
    move-result-object v1

    #@8c
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8f
    .line 3577
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@91
    const-string v1, "Unable to find or allocate a thread ID."

    #@93
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@96
    throw v0

    #@97
    .line 3572
    :catchall_97
    move-exception v0

    #@98
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@9b
    throw v0
.end method

.method public static getOrCreateThreadId(Landroid/content/Context;Ljava/lang/String;)J
    .registers 5
    .parameter "context"
    .parameter "recipient"

    #@0
    .prologue
    .line 3402
    new-instance v0, Ljava/util/HashSet;

    #@2
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@5
    .line 3404
    .local v0, recipients:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    #@8
    .line 3405
    invoke-static {p0, v0}, Landroid/provider/Telephony$Threads;->getOrCreateThreadId(Landroid/content/Context;Ljava/util/Set;)J

    #@b
    move-result-wide v1

    #@c
    return-wide v1
.end method

.method public static getOrCreateThreadId(Landroid/content/Context;Ljava/util/Set;)J
    .registers 13
    .parameter "context"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    #@0
    .prologue
    .local p1, recipients:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v4, 0x0

    #@1
    .line 3502
    sget-object v0, Landroid/provider/Telephony$Threads;->THREAD_ID_CONTENT_URI:Landroid/net/Uri;

    #@3
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    #@6
    move-result-object v10

    #@7
    .line 3504
    .local v10, uriBuilder:Landroid/net/Uri$Builder;
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v8

    #@b
    .local v8, i$:Ljava/util/Iterator;
    :goto_b
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_27

    #@11
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v9

    #@15
    check-cast v9, Ljava/lang/String;

    #@17
    .line 3505
    .local v9, recipient:Ljava/lang/String;
    invoke-static {v9}, Landroid/provider/Telephony$Mms;->isEmailAddress(Ljava/lang/String;)Z

    #@1a
    move-result v0

    #@1b
    if-eqz v0, :cond_21

    #@1d
    .line 3506
    invoke-static {v9}, Landroid/provider/Telephony$Mms;->extractAddrSpec(Ljava/lang/String;)Ljava/lang/String;

    #@20
    move-result-object v9

    #@21
    .line 3509
    :cond_21
    const-string v0, "recipient"

    #@23
    invoke-virtual {v10, v0, v9}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@26
    goto :goto_b

    #@27
    .line 3514
    .end local v9           #recipient:Ljava/lang/String;
    :cond_27
    const-string v0, "cdma_privacy_indicator"

    #@29
    invoke-static {v4, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@2c
    move-result v0

    #@2d
    if-eqz v0, :cond_3b

    #@2f
    .line 3515
    const-string v0, "mpi"

    #@31
    new-instance v1, Ljava/lang/String;

    #@33
    const-string v3, "0"

    #@35
    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@38
    invoke-virtual {v10, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@3b
    .line 3519
    :cond_3b
    invoke-virtual {v10}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    #@3e
    move-result-object v2

    #@3f
    .line 3522
    .local v2, uri:Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@42
    move-result-object v1

    #@43
    sget-object v3, Landroid/provider/Telephony$Threads;->ID_PROJECTION:[Ljava/lang/String;

    #@45
    move-object v0, p0

    #@46
    move-object v5, v4

    #@47
    move-object v6, v4

    #@48
    invoke-static/range {v0 .. v6}, Landroid/database/sqlite/SqliteWrapper;->query(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@4b
    move-result-object v7

    #@4c
    .line 3524
    .local v7, cursor:Landroid/database/Cursor;
    if-eqz v7, :cond_67

    #@4e
    .line 3526
    :try_start_4e
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    #@51
    move-result v0

    #@52
    if-eqz v0, :cond_5d

    #@54
    .line 3527
    const/4 v0, 0x0

    #@55
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_58
    .catchall {:try_start_4e .. :try_end_58} :catchall_8b

    #@58
    move-result-wide v0

    #@59
    .line 3532
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@5c
    return-wide v0

    #@5d
    .line 3529
    :cond_5d
    :try_start_5d
    const-string v0, "tag"

    #@5f
    const-string v1, "getOrCreateThreadId returned no rows!"

    #@61
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_64
    .catchall {:try_start_5d .. :try_end_64} :catchall_8b

    #@64
    .line 3532
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@67
    .line 3536
    :cond_67
    const-string v0, "tag"

    #@69
    new-instance v1, Ljava/lang/StringBuilder;

    #@6b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6e
    const-string v3, "getOrCreateThreadId failed with uri "

    #@70
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v1

    #@74
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@77
    move-result-object v3

    #@78
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v1

    #@7c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f
    move-result-object v1

    #@80
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@83
    .line 3537
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@85
    const-string v1, "Unable to find or allocate a thread ID."

    #@87
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@8a
    throw v0

    #@8b
    .line 3532
    :catchall_8b
    move-exception v0

    #@8c
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@8f
    throw v0
.end method

.method public static getOrCreateThreadIdForMPI(Landroid/content/Context;Ljava/lang/String;)J
    .registers 11
    .parameter "context"
    .parameter "recipient"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 3451
    sget-object v0, Landroid/provider/Telephony$Threads;->THREAD_ID_CONTENT_URI:Landroid/net/Uri;

    #@3
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    #@6
    move-result-object v8

    #@7
    .line 3453
    .local v8, uriBuilder:Landroid/net/Uri$Builder;
    invoke-static {p1}, Landroid/provider/Telephony$Mms;->isEmailAddress(Ljava/lang/String;)Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_11

    #@d
    .line 3454
    invoke-static {p1}, Landroid/provider/Telephony$Mms;->extractAddrSpec(Ljava/lang/String;)Ljava/lang/String;

    #@10
    move-result-object p1

    #@11
    .line 3457
    :cond_11
    const-string v0, "recipient"

    #@13
    invoke-virtual {v8, v0, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@16
    .line 3458
    const-string v0, "mpi"

    #@18
    new-instance v1, Ljava/lang/String;

    #@1a
    const-string v3, "1"

    #@1c
    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@1f
    invoke-virtual {v8, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@22
    .line 3460
    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    #@25
    move-result-object v2

    #@26
    .line 3461
    .local v2, uri:Landroid/net/Uri;
    const-string v0, "tag"

    #@28
    new-instance v1, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v3, "[SMS.MT.MPI] getOrCreateThreadIdForMPI uri: "

    #@2f
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v1

    #@3b
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 3463
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@41
    move-result-object v1

    #@42
    sget-object v3, Landroid/provider/Telephony$Threads;->ID_PROJECTION:[Ljava/lang/String;

    #@44
    move-object v0, p0

    #@45
    move-object v5, v4

    #@46
    move-object v6, v4

    #@47
    invoke-static/range {v0 .. v6}, Landroid/database/sqlite/SqliteWrapper;->query(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@4a
    move-result-object v7

    #@4b
    .line 3468
    .local v7, cursor:Landroid/database/Cursor;
    if-eqz v7, :cond_69

    #@4d
    .line 3469
    const-string v0, "tag"

    #@4f
    new-instance v1, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string v3, "[SMS.MT.MPI] getOrCreateThreadIdForMPI cursor cnt: "

    #@56
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v1

    #@5a
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    #@5d
    move-result v3

    #@5e
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@61
    move-result-object v1

    #@62
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v1

    #@66
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    .line 3473
    :cond_69
    if-eqz v7, :cond_84

    #@6b
    .line 3475
    :try_start_6b
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    #@6e
    move-result v0

    #@6f
    if-eqz v0, :cond_7a

    #@71
    .line 3476
    const/4 v0, 0x0

    #@72
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_75
    .catchall {:try_start_6b .. :try_end_75} :catchall_a8

    #@75
    move-result-wide v0

    #@76
    .line 3481
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@79
    return-wide v0

    #@7a
    .line 3478
    :cond_7a
    :try_start_7a
    const-string v0, "tag"

    #@7c
    const-string v1, "[SMS.MT.MPI] getOrCreateThreadIdForMPI returned no rows!"

    #@7e
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_81
    .catchall {:try_start_7a .. :try_end_81} :catchall_a8

    #@81
    .line 3481
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@84
    .line 3485
    :cond_84
    const-string v0, "tag"

    #@86
    new-instance v1, Ljava/lang/StringBuilder;

    #@88
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8b
    const-string v3, "[SMS.MT.MPI] getOrCreateThreadIdForMPI failed with uri "

    #@8d
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v1

    #@91
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@94
    move-result-object v3

    #@95
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v1

    #@99
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9c
    move-result-object v1

    #@9d
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a0
    .line 3486
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@a2
    const-string v1, "[SMS.MT.MPI] Unable to find or allocate a thread ID in getOrCreateThreadIdForMPI()."

    #@a4
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a7
    throw v0

    #@a8
    .line 3481
    :catchall_a8
    move-exception v0

    #@a9
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@ac
    throw v0
.end method

.method public static getThreadId(Landroid/content/Context;Ljava/lang/String;)J
    .registers 14
    .parameter "context"
    .parameter "recipient"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 3410
    new-instance v10, Ljava/util/HashSet;

    #@3
    invoke-direct {v10}, Ljava/util/HashSet;-><init>()V

    #@6
    .line 3411
    .local v10, recipients:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v10, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    #@9
    .line 3413
    sget-object v0, Landroid/provider/Telephony$Threads;->NOW_THREAD_ID_CONTENT_URI:Landroid/net/Uri;

    #@b
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    #@e
    move-result-object v11

    #@f
    .line 3415
    .local v11, uriBuilder:Landroid/net/Uri$Builder;
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@12
    move-result-object v8

    #@13
    .local v8, i$:Ljava/util/Iterator;
    :goto_13
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@16
    move-result v0

    #@17
    if-eqz v0, :cond_2f

    #@19
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1c
    move-result-object v9

    #@1d
    check-cast v9, Ljava/lang/String;

    #@1f
    .line 3416
    .local v9, recipient1:Ljava/lang/String;
    invoke-static {v9}, Landroid/provider/Telephony$Mms;->isEmailAddress(Ljava/lang/String;)Z

    #@22
    move-result v0

    #@23
    if-eqz v0, :cond_29

    #@25
    .line 3417
    invoke-static {v9}, Landroid/provider/Telephony$Mms;->extractAddrSpec(Ljava/lang/String;)Ljava/lang/String;

    #@28
    move-result-object v9

    #@29
    .line 3420
    :cond_29
    const-string v0, "recipient"

    #@2b
    invoke-virtual {v11, v0, v9}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@2e
    goto :goto_13

    #@2f
    .line 3423
    .end local v9           #recipient1:Ljava/lang/String;
    :cond_2f
    invoke-virtual {v11}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    #@32
    move-result-object v2

    #@33
    .line 3426
    .local v2, uri:Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@36
    move-result-object v1

    #@37
    sget-object v3, Landroid/provider/Telephony$Threads;->ID_PROJECTION:[Ljava/lang/String;

    #@39
    move-object v0, p0

    #@3a
    move-object v5, v4

    #@3b
    move-object v6, v4

    #@3c
    invoke-static/range {v0 .. v6}, Landroid/database/sqlite/SqliteWrapper;->query(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@3f
    move-result-object v7

    #@40
    .line 3428
    .local v7, cursor:Landroid/database/Cursor;
    if-eqz v7, :cond_67

    #@42
    .line 3430
    :try_start_42
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I
    :try_end_45
    .catchall {:try_start_42 .. :try_end_45} :catchall_8b

    #@45
    move-result v0

    #@46
    if-nez v0, :cond_4e

    #@48
    .line 3431
    const-wide/16 v0, 0x0

    #@4a
    .line 3438
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@4d
    :goto_4d
    return-wide v0

    #@4e
    .line 3432
    :cond_4e
    :try_start_4e
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    #@51
    move-result v0

    #@52
    if-eqz v0, :cond_5d

    #@54
    .line 3433
    const/4 v0, 0x0

    #@55
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_58
    .catchall {:try_start_4e .. :try_end_58} :catchall_8b

    #@58
    move-result-wide v0

    #@59
    .line 3438
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@5c
    goto :goto_4d

    #@5d
    .line 3435
    :cond_5d
    :try_start_5d
    const-string v0, "tag"

    #@5f
    const-string v1, "getOrCreateThreadId returned no rows!"

    #@61
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_64
    .catchall {:try_start_5d .. :try_end_64} :catchall_8b

    #@64
    .line 3438
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@67
    .line 3442
    :cond_67
    const-string v0, "tag"

    #@69
    new-instance v1, Ljava/lang/StringBuilder;

    #@6b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6e
    const-string v3, "getOrCreateThreadId failed with uri "

    #@70
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v1

    #@74
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@77
    move-result-object v3

    #@78
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v1

    #@7c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f
    move-result-object v1

    #@80
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@83
    .line 3443
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@85
    const-string v1, "Unable to find or allocate a thread ID."

    #@87
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@8a
    throw v0

    #@8b
    .line 3438
    :catchall_8b
    move-exception v0

    #@8c
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@8f
    throw v0
.end method
