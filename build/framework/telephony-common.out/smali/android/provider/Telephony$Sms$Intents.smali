.class public final Landroid/provider/Telephony$Sms$Intents;
.super Ljava/lang/Object;
.source "Telephony.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/Telephony$Sms;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Intents"
.end annotation


# static fields
.field public static final CALLBACKURLSPAM_KT_RECEIVED_ACTION:Ljava/lang/String; = "android.provider.Telephony.CALLBACKURLSPAM_KT_RECEIVED"

.field public static final CALLBACKURLSPAM_SKT_RECEIVED_ACTION:Ljava/lang/String; = "android.provider.Telephony.CALLBACKURLSPAM_SKT_RECEIVED"

.field public static final CALLBACKURL_KT_RECEIVED_ACTION:Ljava/lang/String; = "android.provider.Telephony.CALLBACKURL_KT_RECEIVED"

.field public static final CALLBACKURL_SKT_RECEIVED_ACTION:Ljava/lang/String; = "android.provider.Telephony.CALLBACKURL_SKT_RECEIVED"

.field public static final CB_SMS_RECEIVED_ACTION:Ljava/lang/String; = "android.provider.Telephony.CB_SMS_RECEIVED"

.field public static final COMMONPUSH_SKT_RECEIVED_ACTION:Ljava/lang/String; = "com.skt.push.SMS_PUSH"

.field public static final DATA_SMS_RECEIVED_ACTION:Ljava/lang/String; = "android.intent.action.DATA_SMS_RECEIVED"

.field public static final DELETE_SIM_MESSAGE_ACTION:Ljava/lang/String; = "android.provider.Telephony.DELETE_SIM_MESSAGE"

.field public static final DIRECTED_SMS_RECEIVED_ACTION:Ljava/lang/String; = "verizon.intent.action.DIRECTED_SMS_RECEIVED"

.field public static final EXTRA_NET_MODE:Ljava/lang/String; = "netmode"

.field public static final FOTA_RECEIVED_ACTION:Ljava/lang/String; = "android.provider.Telephony.FOTA_RECEIVED"

.field public static final LGE_CMAS_RECEIVED_ACTION:Ljava/lang/String; = "com.lge.provider.Telephony.LGE_CMAS_RECEIVED"

.field public static final LGE_MMS_RECEIVED_ACTION:Ljava/lang/String; = "android.provider.Telephony.MMS_RECEIVED"

.field public static final LMS_PULL_RECEIVED_ACTION:Ljava/lang/String; = "android.provider.Telephony.LMSPULL_RECEIVED"

.field public static final LMS_PULL_SPAM_RECEIVED_ACTION:Ljava/lang/String; = "android.provider.Telephony.LMSPULL_SPAM_RECEIVED"

.field public static final LMS_PUSH_RECEIVED_ACTION:Ljava/lang/String; = "android.provider.Telephony.LMSPUSH_RECEIVED"

.field public static final LMS_PUSH_SPAM_RECEIVED_ACTION:Ljava/lang/String; = "android.provider.Telephony.LMSPUSH_SPAM_RECEIVED"

.field public static final NETMODE_UPDATED_ACTION:Ljava/lang/String; = "android.provider.Telephony.NETMODE_UPDATED"

.field public static final PORTADDRESS_KT_RECEIVED_ACTION:Ljava/lang/String; = "android.provider.Telephony.PORTADDRESS_KT_RECEIVED"

.field public static final PORTADDRESS_SKT_RECEIVED_ACTION:Ljava/lang/String; = "android.provider.Telephony.PORTADDRESS_SKT_RECEIVED"

.field public static final RESULT_SMS_GENERIC_ERROR:I = 0x2

.field public static final RESULT_SMS_HANDLED:I = 0x1

.field public static final RESULT_SMS_NOT_HANDLED:I = 0x5

.field public static final RESULT_SMS_OUT_OF_MEMORY:I = 0x3

.field public static final RESULT_SMS_UNSUPPORTED:I = 0x4

.field public static final SEND_SMS_REQUST_BACKGROUND_ACTION:Ljava/lang/String; = "android.provider.Telephony.SEND_SMS_REQUST_BACKGROUND"

.field public static final SIM_FULL_ACTION:Ljava/lang/String; = "android.provider.Telephony.SIM_FULL"

.field public static final SKAF_MMS_RECEIVED_ACTION:Ljava/lang/String; = "com.skt.skaf.ACTION.MMS_RECEIVED"

.field public static final SMS_CB_RECEIVED_ACTION:Ljava/lang/String; = "android.provider.Telephony.SMS_CB_RECEIVED"

.field public static final SMS_CDMA_DAN_DELIVERED:Ljava/lang/String; = "com.lge.kddi.intent.action.DAN_DELIVERED"

.field public static final SMS_CDMA_DAN_FAIL_RETRY:Ljava/lang/String; = "com.lge.kddi.intent.action.DAN_FAIL_RETRY"

.field public static final SMS_CDMA_DAN_OK:Ljava/lang/String; = "com.lge.kddi.intent.action.DAN_SENT_OK"

.field public static final SMS_CDMA_DAN_SENT:Ljava/lang/String; = "com.lge.kddi.intent.action.DAN_SENT"

.field public static final SMS_CDMA_DOMAIN_NOTIFICATION:Ljava/lang/String; = "com.lge.ims.action.DOMAIN_NOTIFICATION"

.field public static final SMS_EMERGENCY_CB_RECEIVED_ACTION:Ljava/lang/String; = "android.provider.Telephony.SMS_EMERGENCY_CB_RECEIVED"

.field public static final SMS_OVER_IMS_REGI_INFO:Ljava/lang/String; = "com.lge.kddi.intent.action.IMS_REGI_INFO"

.field public static final SMS_RECEIVED_ACTION:Ljava/lang/String; = "android.provider.Telephony.SMS_RECEIVED"

.field public static final SMS_REJECTED_ACTION:Ljava/lang/String; = "android.provider.Telephony.SMS_REJECTED"

.field public static final SMS_SERVICE_CATEGORY_PROGRAM_DATA_RECEIVED_ACTION:Ljava/lang/String; = "android.provider.Telephony.SMS_SERVICE_CATEGORY_PROGRAM_DATA_RECEIVED"

.field public static final SMS_WIFI_OFF:Ljava/lang/String; = "android.intent.action.SMS_WIFI_OFF"

.field public static final SPAM_RECEIVED_ACTION:Ljava/lang/String; = "android.provider.Telephony.SPAM_RECEIVED"

.field public static final VIDEO_RECEIVED_ACTION:Ljava/lang/String; = "android.provider.Telephony.VIDEO_RECEIVED"

.field public static final VOICE_RECEIVED_ACTION:Ljava/lang/String; = "android.provider.Telephony.VOICE_RECEIVED"

.field public static final WAP_PUSH_RECEIVED_ACTION:Ljava/lang/String; = "android.provider.Telephony.WAP_PUSH_RECEIVED"

.field public static final WAP_PUSH_SPAM_RECEIVED_ACTION:Ljava/lang/String; = "android.provider.Telephony.WAP_PUSH_SPAM_RECEIVED"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 1389
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static getMessagesFromIntent(Landroid/content/Intent;)[Landroid/telephony/SmsMessage;
    .registers 12
    .parameter "intent"

    #@0
    .prologue
    .line 1850
    const-string v8, "pdus"

    #@2
    invoke-virtual {p0, v8}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    #@5
    move-result-object v8

    #@6
    check-cast v8, [Ljava/lang/Object;

    #@8
    move-object v2, v8

    #@9
    check-cast v2, [Ljava/lang/Object;

    #@b
    .line 1851
    .local v2, messages:[Ljava/lang/Object;
    const-string v8, "format"

    #@d
    invoke-virtual {p0, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@10
    move-result-object v0

    #@11
    .line 1852
    .local v0, format:Ljava/lang/String;
    array-length v8, v2

    #@12
    new-array v5, v8, [[B

    #@14
    .line 1853
    .local v5, pduObjs:[[B
    const-string v8, "subscription"

    #@16
    const/4 v9, 0x0

    #@17
    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@1a
    move-result v7

    #@1b
    .line 1855
    .local v7, subId:I
    const-string v8, "tag"

    #@1d
    new-instance v9, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v10, " getMessagesFromIntent sub_id : "

    #@24
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v9

    #@28
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v9

    #@2c
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v9

    #@30
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 1857
    const/4 v1, 0x0

    #@34
    .local v1, i:I
    :goto_34
    array-length v8, v2

    #@35
    if-ge v1, v8, :cond_42

    #@37
    .line 1858
    aget-object v8, v2, v1

    #@39
    check-cast v8, [B

    #@3b
    check-cast v8, [B

    #@3d
    aput-object v8, v5, v1

    #@3f
    .line 1857
    add-int/lit8 v1, v1, 0x1

    #@41
    goto :goto_34

    #@42
    .line 1860
    :cond_42
    array-length v8, v5

    #@43
    new-array v6, v8, [[B

    #@45
    .line 1861
    .local v6, pdus:[[B
    array-length v4, v6

    #@46
    .line 1862
    .local v4, pduCount:I
    new-array v3, v4, [Landroid/telephony/SmsMessage;

    #@48
    .line 1863
    .local v3, msgs:[Landroid/telephony/SmsMessage;
    const/4 v1, 0x0

    #@49
    :goto_49
    if-ge v1, v4, :cond_72

    #@4b
    .line 1864
    aget-object v8, v5, v1

    #@4d
    aput-object v8, v6, v1

    #@4f
    .line 1866
    const/4 v8, 0x0

    #@50
    const-string v9, "parse_pdu_twice"

    #@52
    invoke-static {v8, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@55
    move-result v8

    #@56
    const/4 v9, 0x1

    #@57
    if-ne v8, v9, :cond_69

    #@59
    .line 1868
    aget-object v8, v6, v1

    #@5b
    invoke-static {v8}, Landroid/telephony/SmsMessage;->createFromPdu([B)Landroid/telephony/SmsMessage;

    #@5e
    move-result-object v8

    #@5f
    aput-object v8, v3, v1

    #@61
    .line 1875
    :goto_61
    aget-object v8, v3, v1

    #@63
    invoke-virtual {v8, v7}, Landroid/telephony/SmsMessage;->setSubId(I)V

    #@66
    .line 1863
    add-int/lit8 v1, v1, 0x1

    #@68
    goto :goto_49

    #@69
    .line 1872
    :cond_69
    aget-object v8, v6, v1

    #@6b
    invoke-static {v8, v0}, Landroid/telephony/SmsMessage;->createFromPdu([BLjava/lang/String;)Landroid/telephony/SmsMessage;

    #@6e
    move-result-object v8

    #@6f
    aput-object v8, v3, v1

    #@71
    goto :goto_61

    #@72
    .line 1877
    :cond_72
    return-object v3
.end method
