.class public final Landroid/provider/Telephony$Sms$PortAddress;
.super Ljava/lang/Object;
.source "Telephony.java"

# interfaces
.implements Landroid/provider/BaseColumns;
.implements Landroid/provider/Telephony$TextBasedSmsColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/Telephony$Sms;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PortAddress"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final DEFAULT_SORT_ORDER:Ljava/lang/String; = "date DESC"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 2427
    const-string v0, "content://sms/portaddress"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/provider/Telephony$Sms$PortAddress;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 2423
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static addMessage(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Z)Landroid/net/Uri;
    .registers 20
    .parameter "resolver"
    .parameter "address"
    .parameter "body"
    .parameter "subject"
    .parameter "date"
    .parameter "read"

    #@0
    .prologue
    .line 2449
    const-wide/16 v6, -0x1

    #@2
    const/4 v8, 0x0

    #@3
    const/4 v9, -0x1

    #@4
    const-wide/16 v10, -0x1

    #@6
    const/4 v12, 0x0

    #@7
    const/4 v13, 0x0

    #@8
    move-object v0, p0

    #@9
    move-object v1, p1

    #@a
    move-object/from16 v2, p2

    #@c
    move-object/from16 v3, p3

    #@e
    move-object/from16 v4, p4

    #@10
    move/from16 v5, p5

    #@12
    invoke-static/range {v0 .. v13}, Landroid/provider/Telephony$Sms$PortAddress;->addMessageEx(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZJIIJLjava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    #@15
    move-result-object v0

    #@16
    return-object v0
.end method

.method public static addMessageEx(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZJIIJLjava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .registers 29
    .parameter "resolver"
    .parameter "recipient"
    .parameter "body"
    .parameter "subject"
    .parameter "date"
    .parameter "read"
    .parameter "threadId"
    .parameter "protocol"
    .parameter "dcs"
    .parameter "groupid"
    .parameter "replyAddress"
    .parameter "extraData"

    #@0
    .prologue
    .line 2465
    const/4 v1, -0x1

    #@1
    move-object/from16 v0, p12

    #@3
    invoke-static {v1, v0}, Landroid/telephony/SmsMessage;->makeSmsHeader(ILjava/lang/String;)[B

    #@6
    move-result-object v13

    #@7
    move-object v1, p0

    #@8
    move-object/from16 v2, p1

    #@a
    move-object/from16 v3, p2

    #@c
    move-object/from16 v4, p3

    #@e
    move-object/from16 v5, p4

    #@10
    move/from16 v6, p5

    #@12
    move-wide/from16 v7, p6

    #@14
    move/from16 v9, p8

    #@16
    move/from16 v10, p9

    #@18
    move-wide/from16 v11, p10

    #@1a
    move-object/from16 v14, p13

    #@1c
    invoke-static/range {v1 .. v14}, Landroid/provider/Telephony$Sms$PortAddress;->addMessageExEx(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZJIIJ[BLjava/lang/String;)Landroid/net/Uri;

    #@1f
    move-result-object v1

    #@20
    return-object v1
.end method

.method public static addMessageExEx(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZJIIJ[BLjava/lang/String;)Landroid/net/Uri;
    .registers 35
    .parameter "resolver"
    .parameter "recipient"
    .parameter "body"
    .parameter "subject"
    .parameter "date"
    .parameter "read"
    .parameter "threadId"
    .parameter "protocol"
    .parameter "dcs"
    .parameter "groupid"
    .parameter "header"
    .parameter "extraData"

    #@0
    .prologue
    .line 2473
    sget-object v1, Landroid/provider/Telephony$Sms$PortAddress;->CONTENT_URI:Landroid/net/Uri;

    #@2
    const/4 v7, 0x0

    #@3
    const/4 v14, 0x0

    #@4
    const/4 v15, 0x0

    #@5
    const/16 v16, 0x1

    #@7
    const/16 v18, 0x1

    #@9
    const/16 v20, 0x0

    #@b
    move-object/from16 v0, p0

    #@d
    move-object/from16 v2, p1

    #@f
    move-object/from16 v3, p2

    #@11
    move-object/from16 v4, p3

    #@13
    move-object/from16 v5, p4

    #@15
    move/from16 v6, p5

    #@17
    move-wide/from16 v8, p6

    #@19
    move/from16 v10, p8

    #@1b
    move/from16 v11, p9

    #@1d
    move-wide/from16 v12, p10

    #@1f
    move-object/from16 v17, p12

    #@21
    move-object/from16 v19, p13

    #@23
    invoke-static/range {v0 .. v20}, Landroid/provider/Telephony$Sms;->addMessageToUriEx(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZJIIJIII[BZLjava/lang/String;I)Landroid/net/Uri;

    #@26
    move-result-object v0

    #@27
    return-object v0
.end method
