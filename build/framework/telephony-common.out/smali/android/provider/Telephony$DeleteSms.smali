.class public final Landroid/provider/Telephony$DeleteSms;
.super Ljava/lang/Object;
.source "Telephony.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/Telephony;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DeleteSms"
.end annotation


# static fields
.field public static final CONTENTID:Ljava/lang/String; = "contentId"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final DELETESMSTABLE:Ljava/lang/String; = "delete_sms"

.field public static final DELETESMSTYPE:I = 0x8

.field public static final TYPE:Ljava/lang/String; = "type"


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 7180
    sget-object v0, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    #@2
    const-string v1, "delete_sms"

    #@4
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@7
    move-result-object v0

    #@8
    sput-object v0, Landroid/provider/Telephony$DeleteSms;->CONTENT_URI:Landroid/net/Uri;

    #@a
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 7175
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static deleteDeleteSmsToUri(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 5
    .parameter "resolver"
    .parameter "uri"
    .parameter "where"
    .parameter "selectionArgs"

    #@0
    .prologue
    .line 7188
    invoke-virtual {p0, p1, p2, p3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    return v0
.end method
