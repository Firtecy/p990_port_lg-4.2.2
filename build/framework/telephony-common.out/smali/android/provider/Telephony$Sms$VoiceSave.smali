.class public final Landroid/provider/Telephony$Sms$VoiceSave;
.super Ljava/lang/Object;
.source "Telephony.java"

# interfaces
.implements Landroid/provider/BaseColumns;
.implements Landroid/provider/Telephony$TextBasedSmsColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/Telephony$Sms;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VoiceSave"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final DEFAULT_SORT_ORDER:Ljava/lang/String; = "date DESC"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 2234
    const-string v0, "content://sms/voicesave"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/provider/Telephony$Sms$VoiceSave;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 2230
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static addMessage(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZJIIJI[B)Landroid/net/Uri;
    .registers 35
    .parameter "resolver"
    .parameter "recipient"
    .parameter "body"
    .parameter "subject"
    .parameter "date"
    .parameter "read"
    .parameter "threadId"
    .parameter "protocol"
    .parameter "dcs"
    .parameter "groupid"
    .parameter "savecalltype"
    .parameter "header"

    #@0
    .prologue
    .line 2257
    sget-object v1, Landroid/provider/Telephony$Sms$VoiceSave;->CONTENT_URI:Landroid/net/Uri;

    #@2
    const/4 v7, 0x0

    #@3
    const/4 v14, 0x0

    #@4
    const/16 v16, 0x4

    #@6
    const/16 v18, 0x1

    #@8
    const/16 v19, 0x0

    #@a
    const/16 v20, 0x0

    #@c
    move-object/from16 v0, p0

    #@e
    move-object/from16 v2, p1

    #@10
    move-object/from16 v3, p2

    #@12
    move-object/from16 v4, p3

    #@14
    move-object/from16 v5, p4

    #@16
    move/from16 v6, p5

    #@18
    move-wide/from16 v8, p6

    #@1a
    move/from16 v10, p8

    #@1c
    move/from16 v11, p9

    #@1e
    move-wide/from16 v12, p10

    #@20
    move/from16 v15, p12

    #@22
    move-object/from16 v17, p13

    #@24
    invoke-static/range {v0 .. v20}, Landroid/provider/Telephony$Sms;->addMessageToUriEx(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZJIIJIII[BZLjava/lang/String;I)Landroid/net/Uri;

    #@27
    move-result-object v0

    #@28
    return-object v0
.end method
