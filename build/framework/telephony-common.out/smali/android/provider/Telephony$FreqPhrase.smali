.class public final Landroid/provider/Telephony$FreqPhrase;
.super Ljava/lang/Object;
.source "Telephony.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/Telephony;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FreqPhrase"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final FREQPHRASE_IDS:Landroid/net/Uri; = null

.field public static final comUseStringColumn:Ljava/lang/String; = "comuse_string"


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 7153
    sget-object v0, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    #@2
    const-string v1, "freqphrase"

    #@4
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@7
    move-result-object v0

    #@8
    sput-object v0, Landroid/provider/Telephony$FreqPhrase;->CONTENT_URI:Landroid/net/Uri;

    #@a
    .line 7155
    sget-object v0, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    #@c
    const-string v1, "freqphraseids"

    #@e
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@11
    move-result-object v0

    #@12
    sput-object v0, Landroid/provider/Telephony$FreqPhrase;->FREQPHRASE_IDS:Landroid/net/Uri;

    #@14
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 7152
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static addComuseStringToUri(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .registers 5
    .parameter "resolver"
    .parameter "uri"
    .parameter "comUseString"

    #@0
    .prologue
    .line 7160
    new-instance v0, Landroid/content/ContentValues;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-direct {v0, v1}, Landroid/content/ContentValues;-><init>(I)V

    #@6
    .line 7161
    .local v0, values:Landroid/content/ContentValues;
    const-string v1, "comuse_string"

    #@8
    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 7162
    invoke-virtual {p0, p1, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@e
    move-result-object v1

    #@f
    return-object v1
.end method

.method public static deleteComuseStringToUri(Landroid/content/ContentResolver;Landroid/net/Uri;)I
    .registers 3
    .parameter "resolver"
    .parameter "uri"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 7172
    invoke-virtual {p0, p1, v0, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public static updateComuseStringToUri(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;)I
    .registers 6
    .parameter "resolver"
    .parameter "uri"
    .parameter "comUseString"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 7166
    new-instance v0, Landroid/content/ContentValues;

    #@3
    const/4 v1, 0x1

    #@4
    invoke-direct {v0, v1}, Landroid/content/ContentValues;-><init>(I)V

    #@7
    .line 7167
    .local v0, values:Landroid/content/ContentValues;
    const-string v1, "comuse_string"

    #@9
    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@c
    .line 7168
    invoke-virtual {p0, p1, v0, v2, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@f
    move-result v1

    #@10
    return v1
.end method
