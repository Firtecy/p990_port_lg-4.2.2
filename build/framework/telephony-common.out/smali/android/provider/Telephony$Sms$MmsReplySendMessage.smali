.class public final Landroid/provider/Telephony$Sms$MmsReplySendMessage;
.super Ljava/lang/Object;
.source "Telephony.java"

# interfaces
.implements Landroid/provider/BaseColumns;
.implements Landroid/provider/Telephony$TextBasedSmsColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/Telephony$Sms;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MmsReplySendMessage"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final DEFAULT_SORT_ORDER:Ljava/lang/String; = "date DESC"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 1945
    const-string v0, "content://sms/mmsreplysend"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/provider/Telephony$Sms$MmsReplySendMessage;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 1940
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static addMessage(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)Landroid/net/Uri;
    .registers 17
    .parameter "resolver"
    .parameter "address"
    .parameter "body"
    .parameter "subject"
    .parameter "date"

    #@0
    .prologue
    .line 1965
    const-wide/16 v5, -0x1

    #@2
    const/4 v7, 0x0

    #@3
    const/4 v8, -0x1

    #@4
    const-wide/16 v9, -0x1

    #@6
    const/4 v11, 0x0

    #@7
    move-object v0, p0

    #@8
    move-object v1, p1

    #@9
    move-object v2, p2

    #@a
    move-object v3, p3

    #@b
    move-object/from16 v4, p4

    #@d
    invoke-static/range {v0 .. v11}, Landroid/provider/Telephony$Sms$MmsReplySendMessage;->addMessageEx(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;JIIJLjava/lang/String;)Landroid/net/Uri;

    #@10
    move-result-object v0

    #@11
    return-object v0
.end method

.method public static addMessageEx(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;JIIJLjava/lang/String;)Landroid/net/Uri;
    .registers 25
    .parameter "resolver"
    .parameter "recipient"
    .parameter "body"
    .parameter "subject"
    .parameter "date"
    .parameter "threadId"
    .parameter "protocol"
    .parameter "dcs"
    .parameter "groupid"
    .parameter "replyAddress"

    #@0
    .prologue
    .line 1979
    const/4 v1, -0x1

    #@1
    move-object/from16 v0, p11

    #@3
    invoke-static {v1, v0}, Landroid/telephony/SmsMessage;->makeSmsHeader(ILjava/lang/String;)[B

    #@6
    move-result-object v12

    #@7
    move-object v1, p0

    #@8
    move-object v2, p1

    #@9
    move-object v3, p2

    #@a
    move-object/from16 v4, p3

    #@c
    move-object/from16 v5, p4

    #@e
    move-wide/from16 v6, p5

    #@10
    move/from16 v8, p7

    #@12
    move/from16 v9, p8

    #@14
    move-wide/from16 v10, p9

    #@16
    invoke-static/range {v1 .. v12}, Landroid/provider/Telephony$Sms$MmsReplySendMessage;->addMessageExEx(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;JIIJ[B)Landroid/net/Uri;

    #@19
    move-result-object v1

    #@1a
    return-object v1
.end method

.method public static addMessageExEx(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;JIIJ[B)Landroid/net/Uri;
    .registers 33
    .parameter "resolver"
    .parameter "recipient"
    .parameter "body"
    .parameter "subject"
    .parameter "date"
    .parameter "threadId"
    .parameter "protocol"
    .parameter "dcs"
    .parameter "groupid"
    .parameter "header"

    #@0
    .prologue
    .line 1987
    sget-object v1, Landroid/provider/Telephony$Sms$MmsReplySendMessage;->CONTENT_URI:Landroid/net/Uri;

    #@2
    const/4 v6, 0x1

    #@3
    const/4 v7, 0x0

    #@4
    const/4 v14, 0x0

    #@5
    const/4 v15, 0x0

    #@6
    const/16 v16, 0x2

    #@8
    const/16 v18, 0x0

    #@a
    const/16 v19, 0x0

    #@c
    const/16 v20, 0x0

    #@e
    move-object/from16 v0, p0

    #@10
    move-object/from16 v2, p1

    #@12
    move-object/from16 v3, p2

    #@14
    move-object/from16 v4, p3

    #@16
    move-object/from16 v5, p4

    #@18
    move-wide/from16 v8, p5

    #@1a
    move/from16 v10, p7

    #@1c
    move/from16 v11, p8

    #@1e
    move-wide/from16 v12, p9

    #@20
    move-object/from16 v17, p11

    #@22
    invoke-static/range {v0 .. v20}, Landroid/provider/Telephony$Sms;->addMessageToUriEx(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZJIIJIII[BZLjava/lang/String;I)Landroid/net/Uri;

    #@25
    move-result-object v0

    #@26
    return-object v0
.end method
