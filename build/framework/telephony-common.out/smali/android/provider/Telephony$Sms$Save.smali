.class public final Landroid/provider/Telephony$Sms$Save;
.super Ljava/lang/Object;
.source "Telephony.java"

# interfaces
.implements Landroid/provider/BaseColumns;
.implements Landroid/provider/Telephony$TextBasedSmsColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/Telephony$Sms;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Save"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final DEFAULT_SORT_ORDER:Ljava/lang/String; = "date DESC"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 2116
    const-string v0, "content://sms/save"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/provider/Telephony$Sms$Save;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 2112
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static addMessage(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZI)Landroid/net/Uri;
    .registers 21
    .parameter "resolver"
    .parameter "address"
    .parameter "body"
    .parameter "subject"
    .parameter "date"
    .parameter "read"
    .parameter "savecalltype"

    #@0
    .prologue
    .line 2139
    const-wide/16 v6, -0x1

    #@2
    const/4 v8, 0x0

    #@3
    const/4 v9, -0x1

    #@4
    const-wide/16 v10, -0x1

    #@6
    const/4 v13, 0x0

    #@7
    move-object v0, p0

    #@8
    move-object v1, p1

    #@9
    move-object/from16 v2, p2

    #@b
    move-object/from16 v3, p3

    #@d
    move-object/from16 v4, p4

    #@f
    move/from16 v5, p5

    #@11
    move/from16 v12, p6

    #@13
    invoke-static/range {v0 .. v13}, Landroid/provider/Telephony$Sms$Save;->addMessageEx(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZJIIJILjava/lang/String;)Landroid/net/Uri;

    #@16
    move-result-object v0

    #@17
    return-object v0
.end method

.method public static addMessageEx(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZJIIJILjava/lang/String;)Landroid/net/Uri;
    .registers 29
    .parameter "resolver"
    .parameter "recipient"
    .parameter "body"
    .parameter "subject"
    .parameter "date"
    .parameter "read"
    .parameter "threadId"
    .parameter "protocol"
    .parameter "dcs"
    .parameter "groupid"
    .parameter "savecalltype"
    .parameter "replyAddress"

    #@0
    .prologue
    .line 2154
    const/4 v1, -0x1

    #@1
    move-object/from16 v0, p13

    #@3
    invoke-static {v1, v0}, Landroid/telephony/SmsMessage;->makeSmsHeader(ILjava/lang/String;)[B

    #@6
    move-result-object v14

    #@7
    move-object v1, p0

    #@8
    move-object/from16 v2, p1

    #@a
    move-object/from16 v3, p2

    #@c
    move-object/from16 v4, p3

    #@e
    move-object/from16 v5, p4

    #@10
    move/from16 v6, p5

    #@12
    move-wide/from16 v7, p6

    #@14
    move/from16 v9, p8

    #@16
    move/from16 v10, p9

    #@18
    move-wide/from16 v11, p10

    #@1a
    move/from16 v13, p12

    #@1c
    invoke-static/range {v1 .. v14}, Landroid/provider/Telephony$Sms$Save;->addMessageExEx(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZJIIJI[B)Landroid/net/Uri;

    #@1f
    move-result-object v1

    #@20
    return-object v1
.end method

.method public static addMessageExEx(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZJIIJI[B)Landroid/net/Uri;
    .registers 35
    .parameter "resolver"
    .parameter "recipient"
    .parameter "body"
    .parameter "subject"
    .parameter "date"
    .parameter "read"
    .parameter "threadId"
    .parameter "protocol"
    .parameter "dcs"
    .parameter "groupid"
    .parameter "savecalltype"
    .parameter "header"

    #@0
    .prologue
    .line 2162
    sget-object v1, Landroid/provider/Telephony$Sms$Save;->CONTENT_URI:Landroid/net/Uri;

    #@2
    const/4 v7, 0x0

    #@3
    const/4 v14, 0x0

    #@4
    const/16 v16, 0x4

    #@6
    const/16 v18, 0x1

    #@8
    const/16 v19, 0x0

    #@a
    const/16 v20, 0x0

    #@c
    move-object/from16 v0, p0

    #@e
    move-object/from16 v2, p1

    #@10
    move-object/from16 v3, p2

    #@12
    move-object/from16 v4, p3

    #@14
    move-object/from16 v5, p4

    #@16
    move/from16 v6, p5

    #@18
    move-wide/from16 v8, p6

    #@1a
    move/from16 v10, p8

    #@1c
    move/from16 v11, p9

    #@1e
    move-wide/from16 v12, p10

    #@20
    move/from16 v15, p12

    #@22
    move-object/from16 v17, p13

    #@24
    invoke-static/range {v0 .. v20}, Landroid/provider/Telephony$Sms;->addMessageToUriEx(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZJIIJIII[BZLjava/lang/String;I)Landroid/net/Uri;

    #@27
    move-result-object v0

    #@28
    return-object v0
.end method
