.class public final Landroid/provider/Telephony$Sms$Voice;
.super Ljava/lang/Object;
.source "Telephony.java"

# interfaces
.implements Landroid/provider/BaseColumns;
.implements Landroid/provider/Telephony$TextBasedSmsColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/Telephony$Sms;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Voice"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final DEFAULT_SORT_ORDER:Ljava/lang/String; = "date DESC"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 2269
    const-string v0, "content://sms/voice"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/provider/Telephony$Sms$Voice;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 2265
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static addMessage(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZJIIJ[B)Landroid/net/Uri;
    .registers 34
    .parameter "resolver"
    .parameter "recipient"
    .parameter "body"
    .parameter "subject"
    .parameter "date"
    .parameter "read"
    .parameter "threadId"
    .parameter "protocol"
    .parameter "dcs"
    .parameter "groupid"
    .parameter "header"

    #@0
    .prologue
    .line 2292
    sget-object v1, Landroid/provider/Telephony$Sms$Voice;->CONTENT_URI:Landroid/net/Uri;

    #@2
    const/4 v7, 0x0

    #@3
    const/4 v14, 0x0

    #@4
    const/4 v15, 0x0

    #@5
    const/16 v16, 0x1

    #@7
    const/16 v18, 0x1

    #@9
    const/16 v19, 0x0

    #@b
    const/16 v20, 0x0

    #@d
    move-object/from16 v0, p0

    #@f
    move-object/from16 v2, p1

    #@11
    move-object/from16 v3, p2

    #@13
    move-object/from16 v4, p3

    #@15
    move-object/from16 v5, p4

    #@17
    move/from16 v6, p5

    #@19
    move-wide/from16 v8, p6

    #@1b
    move/from16 v10, p8

    #@1d
    move/from16 v11, p9

    #@1f
    move-wide/from16 v12, p10

    #@21
    move-object/from16 v17, p12

    #@23
    invoke-static/range {v0 .. v20}, Landroid/provider/Telephony$Sms;->addMessageToUriEx(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZJIIJIII[BZLjava/lang/String;I)Landroid/net/Uri;

    #@26
    move-result-object v0

    #@27
    return-object v0
.end method
