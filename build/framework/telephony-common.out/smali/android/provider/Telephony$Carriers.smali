.class public final Landroid/provider/Telephony$Carriers;
.super Ljava/lang/Object;
.source "Telephony.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/Telephony;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Carriers"
.end annotation


# static fields
.field public static final APN:Ljava/lang/String; = "apn"

.field public static final AUTH_TYPE:Ljava/lang/String; = "authtype"

.field private static AUTOPROFILE_FEATURE:Ljava/lang/String; = null

.field public static final BEARER:Ljava/lang/String; = "bearer"

.field public static final CARRIER_ENABLED:Ljava/lang/String; = "carrier_enabled"

.field public static final CLASS:Ljava/lang/String; = "class"

.field public static final CLONEDAPN_FILE:Ljava/lang/String; = "cloned-apn"

.field public static final CLONEDAPN_FILE_2:Ljava/lang/String; = "cloned-apn-2"

.field public static final COLUMN_APN_KEY:Ljava/lang/String; = "apn_key"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field private static COUNTRY_CODE:Ljava/lang/String; = null

.field public static final CURRENT:Ljava/lang/String; = "current"

.field public static final DEFAULTSETTING:Ljava/lang/String; = "defaultsetting"

.field public static final DEFAULTSETTING_EDITABLE:I = 0x0

.field public static final DEFAULTSETTING_HIDDEN:I = 0x2

.field public static final DEFAULTSETTING_NOT_EDITABLE:I = 0x1

.field public static final DEFAULT_SORT_ORDER:Ljava/lang/String; = "name ASC"

.field public static final ENABLED:Ljava/lang/String; = "enabled"

.field public static final EXTRA_ID:Ljava/lang/String; = "extraid"

.field private static final GID_Based_AutoProfile_OpList:Ljava/lang/String; = "23410;0A;23415;A0;23415;C1;23415;70;23430;28;20801;33;20801;52;20823;52;20601;52;20801;4E;20810;4E;20826;4E;20810;12;64710;12;20810;4C;20810;44;27211;0A;20601;0A;21408;00;302610;3D;302610;3E;302610;3F;302610;40;302780;5A;20404;5A;310410;DE;302220;54;302221;54;302220;4B;302221;4B;302220;50;302221;50;302220;43;302660;2C;302370;2C;52000;02;52000;01;45400;01010000;45400;01020000;45400;01030000;45400;01040000;45400;02020001;45400;02030001;45400;02020002;45402;01010000;45402;01020000;45402;01030000;45402;01040000;45402;02020001;45402;02030001;45402;02020002;45410;01010000;45410;01020000;45410;01030000;45410;01040000;45410;02020001;45410;02030001;45410;02020002;45418;01010000;45418;01020000;45418;01030000;45418;01040000;45418;02020001;45418;02030001;45418;02020002;"

.field private static final GLOBAL_OPERATORS:[[Ljava/lang/String; = null

.field private static final H3G_OpList:Ljava/lang/String; = "23210;3;23806;3;27205;;22299;;24002;3SE;24004;3SE;23420;3;23594;3;45403;3;"

.field private static final IMSI_Based_AutoProfile_OpList:Ljava/lang/String; = "22802;29402;21670;21406;21406;21406;24001;23820;24202;24007;24007;24201;20601;26207;26207;26207;50502;50503;22803;302720;302370;65507;21403;"

.field public static final INACTIVE_TIMER:Ljava/lang/String; = "inactivetimer"

.field public static final MCC:Ljava/lang/String; = "mcc"

.field public static final MMSC:Ljava/lang/String; = "mmsc"

.field public static final MMSPORT:Ljava/lang/String; = "mmsport"

.field public static final MMSPROXY:Ljava/lang/String; = "mmsproxy"

.field public static final MNC:Ljava/lang/String; = "mnc"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final NUMERIC:Ljava/lang/String; = "numeric"

.field private static final O2_OpList:Ljava/lang/String; = "23002;O2 - CZ;26207;;26207;262075_2620749;27202;O2.IE;23106;O2 - SK;21407;Base;23410;O2 - UK;"

.field private static OPERATOR_CODE:Ljava/lang/String; = null

.field private static OPERATOR_VERISON_EXTRAID:Ljava/lang/String; = null

.field private static OPERATOR_VERISON_NUMERIC:Ljava/lang/String; = null

.field private static final ORG_OpList:Ljava/lang/String; = "28310;;23205;ORANGE A;65202;;20610;MOBISTAR;62402;;62303;;60201;;62701;;20801;ORANGE F;64700;ORANGE RE;34001;ORANGE;61101;;63203;;42501;;61203;;41677;;416770;;63907;;27099;ORANGE;64602;;61002;;61701;;25901;ORANGE;60400;;61404;;26003;ORANGE;26803;P OPTIMUS;22610;ORANGE RO;60801;;23101;ORANGE SK;21403;ORANGE;22803;ORANGE;22803;22803[1-9][1-8];60501;;64114;;23433;ORANGE;23430;T-MOBILE;23431;T-MOBILE;23432;T-MOBILE;"

.field public static final PASSWORD:Ljava/lang/String; = "password"

.field public static final PORT:Ljava/lang/String; = "port"

.field public static final PROFILE_TYPE:Ljava/lang/String; = "profile_type"

.field public static final PROTOCOL:Ljava/lang/String; = "protocol"

.field public static final PROXY:Ljava/lang/String; = "proxy"

.field public static final ROAMING_PROTOCOL:Ljava/lang/String; = "roaming_protocol"

.field public static final SERVER:Ljava/lang/String; = "server"

.field private static final SFR_OpList:Ljava/lang/String; = "20810;F SFR;64710;SFR;20810;4C;"

.field public static final SIM_SLOT:Ljava/lang/String; = "sim_slot"

.field public static final SIM_SLOT_1:I = 0x1

.field public static final SIM_SLOT_2:I = 0x2

.field public static final SIM_SLOT_NONE:I = 0x0

.field public static final SUBSCRIPTION_SIM1:I = 0x0

.field public static final SUBSCRIPTION_SIM2:I = 0x1

.field private static final Spn_Based_AutoProfile_OpList:Ljava/lang/String; = "21407;JAZZTEL;21421;JAZZTEL;23801;TELMORE;23802;BIBOB;23802;CBB MOBIL;23820;CALL ME;23820;DLG TELE;20810;A MOBILE;20810;LECLERCMOBILE;26201;DEBITEL;26201;MOBILCOM-DEBITEL;26202;DEBITEL;26202;MOBILCOM-DEBITEL;26202;1&1;26203;DEBITEL;26203;MOBILCOM-DEBITEL;26207;DEBITEL;26207;MOBILCOM-DEBITEL;21601;DJUICE;22299;FASTWEB;22210;POSTEMOBILE;20408;RABO MOBIEL;20416;BEN NL;26801;ZON;24004;TELENOR SE;24005;HALEBOP;24005;TELE2;24005;TELIA;24024;TELE2;24024;TELENOR SE;25503;ACE&BASE;25503;DJUICE;26006;RED BULL MOBILE;24803;TELE2;22201;IUSACELL;20404;PELEPHONE;20404;MIRS;"

.field private static final TMO_OpList:Ljava/lang/String; = "23203;T MOBILE A;23207;TELE.RING;28405;284 05;21901;T-MOBILE HR;23001;T-MOBILE CZ;29401;T-MOBILE MK;26201;;20201;COSMOTE;21630;T-MOBILE HU;22004;T-MOBILE CG;29702;T-MOBILE CG;20416;T-MOBILE  NL;20420;T-MOBILE  NL;26002;T-MOBILE.PL;22603;COSMOTE;22606;COSMOTE;23102;T-MOBILE SK;"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final USER:Ljava/lang/String; = "user"

.field public static final USERCREATESETTING:Ljava/lang/String; = "usercreatesetting"

.field public static final USERCREATESETTING_FOTA:I = 0xa

.field public static final USERCREATESETTING_MANUAL:I = 0x1

.field public static final USERCREATESETTING_OTA:I = 0x2

.field public static final USERCREATESETTING_PRELOADED:I = 0x0

.field private static final VDF_OpList:Ljava/lang/String; = "27602;VODAFONE AL;23201;A1;20601;PROXIMUS;28401;;21910;HR VIP;28001;CYTAMOBILE-VODAFONE;23003;VODAFONE CZ;29403;VIP MK;20810;F SFR;64710;SFR;20810;4C;26202;VODAFONE.DE;20205;VODAFONE GR;21670;VODAFONE HU;21670;21670XX2;27402;;27403;;27201;VODAFONE IE;22210;VODAFONE IT;27801;VODAFONE MT;20404;;26801;;22601;VODAFONE RO;22005;VIP;29340;SOMOBIL;21401;VODAFONE ES;22801;SWISSCOM;23415;VODAFONE UK;53001;;53024;;50503;;60202;;65501;;26001;PLUS;26002;T-MOBILE.PL;26003;ORANGE;26006;PLAY;45005;SKT;45008;KT;"

.field private static isBuildOperatorVersionValues:Z

.field private static mNumericExtraIDSet:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static multiSimMode:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 9

    #@0
    .prologue
    const/4 v8, 0x4

    #@1
    const/4 v7, 0x3

    #@2
    const/4 v6, 0x2

    #@3
    const/4 v5, 0x1

    #@4
    const/4 v4, 0x0

    #@5
    .line 4170
    const-string v0, "content://telephony/carriers"

    #@7
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@a
    move-result-object v0

    #@b
    sput-object v0, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    #@d
    .line 4278
    const/16 v0, 0x209

    #@f
    new-array v0, v0, [[Ljava/lang/String;

    #@11
    new-array v1, v8, [Ljava/lang/String;

    #@13
    const-string v2, "ALB"

    #@15
    aput-object v2, v1, v4

    #@17
    const-string v2, "AL"

    #@19
    aput-object v2, v1, v5

    #@1b
    const-string v2, "27601"

    #@1d
    aput-object v2, v1, v6

    #@1f
    const-string v2, ""

    #@21
    aput-object v2, v1, v7

    #@23
    aput-object v1, v0, v4

    #@25
    new-array v1, v8, [Ljava/lang/String;

    #@27
    const-string v2, "VDF"

    #@29
    aput-object v2, v1, v4

    #@2b
    const-string v2, "AL"

    #@2d
    aput-object v2, v1, v5

    #@2f
    const-string v2, "27602"

    #@31
    aput-object v2, v1, v6

    #@33
    const-string v2, ""

    #@35
    aput-object v2, v1, v7

    #@37
    aput-object v1, v0, v5

    #@39
    new-array v1, v8, [Ljava/lang/String;

    #@3b
    const-string v2, "A1"

    #@3d
    aput-object v2, v1, v4

    #@3f
    const-string v2, "AT"

    #@41
    aput-object v2, v1, v5

    #@43
    const-string v2, "23201"

    #@45
    aput-object v2, v1, v6

    #@47
    const-string v2, ""

    #@49
    aput-object v2, v1, v7

    #@4b
    aput-object v1, v0, v6

    #@4d
    new-array v1, v8, [Ljava/lang/String;

    #@4f
    const-string v2, "BOB"

    #@51
    aput-object v2, v1, v4

    #@53
    const-string v2, "AT"

    #@55
    aput-object v2, v1, v5

    #@57
    const-string v2, "23211"

    #@59
    aput-object v2, v1, v6

    #@5b
    const-string v2, ""

    #@5d
    aput-object v2, v1, v7

    #@5f
    aput-object v1, v0, v7

    #@61
    new-array v1, v8, [Ljava/lang/String;

    #@63
    const-string v2, "H3G"

    #@65
    aput-object v2, v1, v4

    #@67
    const-string v2, "AT"

    #@69
    aput-object v2, v1, v5

    #@6b
    const-string v2, "23210"

    #@6d
    aput-object v2, v1, v6

    #@6f
    const-string v2, ""

    #@71
    aput-object v2, v1, v7

    #@73
    aput-object v1, v0, v8

    #@75
    const/4 v1, 0x5

    #@76
    new-array v2, v8, [Ljava/lang/String;

    #@78
    const-string v3, "ORG"

    #@7a
    aput-object v3, v2, v4

    #@7c
    const-string v3, "AT"

    #@7e
    aput-object v3, v2, v5

    #@80
    const-string v3, "23205"

    #@82
    aput-object v3, v2, v6

    #@84
    const-string v3, ""

    #@86
    aput-object v3, v2, v7

    #@88
    aput-object v2, v0, v1

    #@8a
    const/4 v1, 0x6

    #@8b
    new-array v2, v8, [Ljava/lang/String;

    #@8d
    const-string v3, "TMO"

    #@8f
    aput-object v3, v2, v4

    #@91
    const-string v3, "AT"

    #@93
    aput-object v3, v2, v5

    #@95
    const-string v3, "23203"

    #@97
    aput-object v3, v2, v6

    #@99
    const-string v3, ""

    #@9b
    aput-object v3, v2, v7

    #@9d
    aput-object v2, v0, v1

    #@9f
    const/4 v1, 0x7

    #@a0
    new-array v2, v8, [Ljava/lang/String;

    #@a2
    const-string v3, "TMO"

    #@a4
    aput-object v3, v2, v4

    #@a6
    const-string v3, "AT"

    #@a8
    aput-object v3, v2, v5

    #@aa
    const-string v3, "23207"

    #@ac
    aput-object v3, v2, v6

    #@ae
    const-string v3, ""

    #@b0
    aput-object v3, v2, v7

    #@b2
    aput-object v2, v0, v1

    #@b4
    const/16 v1, 0x8

    #@b6
    new-array v2, v8, [Ljava/lang/String;

    #@b8
    const-string v3, "TEL"

    #@ba
    aput-object v3, v2, v4

    #@bc
    const-string v3, "AT"

    #@be
    aput-object v3, v2, v5

    #@c0
    const-string v3, "23207"

    #@c2
    aput-object v3, v2, v6

    #@c4
    const-string v3, ""

    #@c6
    aput-object v3, v2, v7

    #@c8
    aput-object v2, v0, v1

    #@ca
    const/16 v1, 0x9

    #@cc
    new-array v2, v8, [Ljava/lang/String;

    #@ce
    const-string v3, "BASE"

    #@d0
    aput-object v3, v2, v4

    #@d2
    const-string v3, "BE"

    #@d4
    aput-object v3, v2, v5

    #@d6
    const-string v3, "20620"

    #@d8
    aput-object v3, v2, v6

    #@da
    const-string v3, ""

    #@dc
    aput-object v3, v2, v7

    #@de
    aput-object v2, v0, v1

    #@e0
    const/16 v1, 0xa

    #@e2
    new-array v2, v8, [Ljava/lang/String;

    #@e4
    const-string v3, "ORG"

    #@e6
    aput-object v3, v2, v4

    #@e8
    const-string v3, "BE"

    #@ea
    aput-object v3, v2, v5

    #@ec
    const-string v3, "20610"

    #@ee
    aput-object v3, v2, v6

    #@f0
    const-string v3, ""

    #@f2
    aput-object v3, v2, v7

    #@f4
    aput-object v2, v0, v1

    #@f6
    const/16 v1, 0xb

    #@f8
    new-array v2, v8, [Ljava/lang/String;

    #@fa
    const-string v3, "PRX"

    #@fc
    aput-object v3, v2, v4

    #@fe
    const-string v3, "BE"

    #@100
    aput-object v3, v2, v5

    #@102
    const-string v3, "20601"

    #@104
    aput-object v3, v2, v6

    #@106
    const-string v3, ""

    #@108
    aput-object v3, v2, v7

    #@10a
    aput-object v2, v0, v1

    #@10c
    const/16 v1, 0xc

    #@10e
    new-array v2, v8, [Ljava/lang/String;

    #@110
    const-string v3, "TEL"

    #@112
    aput-object v3, v2, v4

    #@114
    const-string v3, "BE"

    #@116
    aput-object v3, v2, v5

    #@118
    const-string v3, "20605"

    #@11a
    aput-object v3, v2, v6

    #@11c
    const-string v3, ""

    #@11e
    aput-object v3, v2, v7

    #@120
    aput-object v2, v0, v1

    #@122
    const/16 v1, 0xd

    #@124
    new-array v2, v8, [Ljava/lang/String;

    #@126
    const-string v3, "TEL"

    #@128
    aput-object v3, v2, v4

    #@12a
    const-string v3, "BE"

    #@12c
    aput-object v3, v2, v5

    #@12e
    const-string v3, "20601"

    #@130
    aput-object v3, v2, v6

    #@132
    const-string v3, "2060188"

    #@134
    aput-object v3, v2, v7

    #@136
    aput-object v2, v0, v1

    #@138
    const/16 v1, 0xe

    #@13a
    new-array v2, v8, [Ljava/lang/String;

    #@13c
    const-string v3, "BHT"

    #@13e
    aput-object v3, v2, v4

    #@140
    const-string v3, "BA"

    #@142
    aput-object v3, v2, v5

    #@144
    const-string v3, "21890"

    #@146
    aput-object v3, v2, v6

    #@148
    const-string v3, ""

    #@14a
    aput-object v3, v2, v7

    #@14c
    aput-object v2, v0, v1

    #@14e
    const/16 v1, 0xf

    #@150
    new-array v2, v8, [Ljava/lang/String;

    #@152
    const-string v3, "ERO"

    #@154
    aput-object v3, v2, v4

    #@156
    const-string v3, "BA"

    #@158
    aput-object v3, v2, v5

    #@15a
    const-string v3, "21803"

    #@15c
    aput-object v3, v2, v6

    #@15e
    const-string v3, ""

    #@160
    aput-object v3, v2, v7

    #@162
    aput-object v2, v0, v1

    #@164
    const/16 v1, 0x10

    #@166
    new-array v2, v8, [Ljava/lang/String;

    #@168
    const-string v3, "MTEL"

    #@16a
    aput-object v3, v2, v4

    #@16c
    const-string v3, "BA"

    #@16e
    aput-object v3, v2, v5

    #@170
    const-string v3, "21805"

    #@172
    aput-object v3, v2, v6

    #@174
    const-string v3, ""

    #@176
    aput-object v3, v2, v7

    #@178
    aput-object v2, v0, v1

    #@17a
    const/16 v1, 0x11

    #@17c
    new-array v2, v8, [Ljava/lang/String;

    #@17e
    const-string v3, "TMO"

    #@180
    aput-object v3, v2, v4

    #@182
    const-string v3, "BG"

    #@184
    aput-object v3, v2, v5

    #@186
    const-string v3, "28405"

    #@188
    aput-object v3, v2, v6

    #@18a
    const-string v3, ""

    #@18c
    aput-object v3, v2, v7

    #@18e
    aput-object v2, v0, v1

    #@190
    const/16 v1, 0x12

    #@192
    new-array v2, v8, [Ljava/lang/String;

    #@194
    const-string v3, "MTEL"

    #@196
    aput-object v3, v2, v4

    #@198
    const-string v3, "BG"

    #@19a
    aput-object v3, v2, v5

    #@19c
    const-string v3, "28401"

    #@19e
    aput-object v3, v2, v6

    #@1a0
    const-string v3, ""

    #@1a2
    aput-object v3, v2, v7

    #@1a4
    aput-object v2, v0, v1

    #@1a6
    const/16 v1, 0x13

    #@1a8
    new-array v2, v8, [Ljava/lang/String;

    #@1aa
    const-string v3, "VIV"

    #@1ac
    aput-object v3, v2, v4

    #@1ae
    const-string v3, "BG"

    #@1b0
    aput-object v3, v2, v5

    #@1b2
    const-string v3, "28403"

    #@1b4
    aput-object v3, v2, v6

    #@1b6
    const-string v3, ""

    #@1b8
    aput-object v3, v2, v7

    #@1ba
    aput-object v2, v0, v1

    #@1bc
    const/16 v1, 0x14

    #@1be
    new-array v2, v8, [Ljava/lang/String;

    #@1c0
    const-string v3, "TMO"

    #@1c2
    aput-object v3, v2, v4

    #@1c4
    const-string v3, "HR"

    #@1c6
    aput-object v3, v2, v5

    #@1c8
    const-string v3, "21901"

    #@1ca
    aput-object v3, v2, v6

    #@1cc
    const-string v3, ""

    #@1ce
    aput-object v3, v2, v7

    #@1d0
    aput-object v2, v0, v1

    #@1d2
    const/16 v1, 0x15

    #@1d4
    new-array v2, v8, [Ljava/lang/String;

    #@1d6
    const-string v3, "TEL2"

    #@1d8
    aput-object v3, v2, v4

    #@1da
    const-string v3, "HR"

    #@1dc
    aput-object v3, v2, v5

    #@1de
    const-string v3, "21902"

    #@1e0
    aput-object v3, v2, v6

    #@1e2
    const-string v3, ""

    #@1e4
    aput-object v3, v2, v7

    #@1e6
    aput-object v2, v0, v1

    #@1e8
    const/16 v1, 0x16

    #@1ea
    new-array v2, v8, [Ljava/lang/String;

    #@1ec
    const-string v3, "TEL2"

    #@1ee
    aput-object v3, v2, v4

    #@1f0
    const-string v3, "HR"

    #@1f2
    aput-object v3, v2, v5

    #@1f4
    const-string v3, "24007"

    #@1f6
    aput-object v3, v2, v6

    #@1f8
    const-string v3, "24007695_24007696"

    #@1fa
    aput-object v3, v2, v7

    #@1fc
    aput-object v2, v0, v1

    #@1fe
    const/16 v1, 0x17

    #@200
    new-array v2, v8, [Ljava/lang/String;

    #@202
    const-string v3, "VIP"

    #@204
    aput-object v3, v2, v4

    #@206
    const-string v3, "HR"

    #@208
    aput-object v3, v2, v5

    #@20a
    const-string v3, "21910"

    #@20c
    aput-object v3, v2, v6

    #@20e
    const-string v3, ""

    #@210
    aput-object v3, v2, v7

    #@212
    aput-object v2, v0, v1

    #@214
    const/16 v1, 0x18

    #@216
    new-array v2, v8, [Ljava/lang/String;

    #@218
    const-string v3, "CYTA"

    #@21a
    aput-object v3, v2, v4

    #@21c
    const-string v3, "CY"

    #@21e
    aput-object v3, v2, v5

    #@220
    const-string v3, "28001"

    #@222
    aput-object v3, v2, v6

    #@224
    const-string v3, ""

    #@226
    aput-object v3, v2, v7

    #@228
    aput-object v2, v0, v1

    #@22a
    const/16 v1, 0x19

    #@22c
    new-array v2, v8, [Ljava/lang/String;

    #@22e
    const-string v3, "O2"

    #@230
    aput-object v3, v2, v4

    #@232
    const-string v3, "CZ"

    #@234
    aput-object v3, v2, v5

    #@236
    const-string v3, "23002"

    #@238
    aput-object v3, v2, v6

    #@23a
    const-string v3, ""

    #@23c
    aput-object v3, v2, v7

    #@23e
    aput-object v2, v0, v1

    #@240
    const/16 v1, 0x1a

    #@242
    new-array v2, v8, [Ljava/lang/String;

    #@244
    const-string v3, "TMO"

    #@246
    aput-object v3, v2, v4

    #@248
    const-string v3, "CZ"

    #@24a
    aput-object v3, v2, v5

    #@24c
    const-string v3, "23001"

    #@24e
    aput-object v3, v2, v6

    #@250
    const-string v3, ""

    #@252
    aput-object v3, v2, v7

    #@254
    aput-object v2, v0, v1

    #@256
    const/16 v1, 0x1b

    #@258
    new-array v2, v8, [Ljava/lang/String;

    #@25a
    const-string v3, "VDF"

    #@25c
    aput-object v3, v2, v4

    #@25e
    const-string v3, "CZ"

    #@260
    aput-object v3, v2, v5

    #@262
    const-string v3, "23003"

    #@264
    aput-object v3, v2, v6

    #@266
    const-string v3, ""

    #@268
    aput-object v3, v2, v7

    #@26a
    aput-object v2, v0, v1

    #@26c
    const/16 v1, 0x1c

    #@26e
    new-array v2, v8, [Ljava/lang/String;

    #@270
    const-string v3, "H3G"

    #@272
    aput-object v3, v2, v4

    #@274
    const-string v3, "DK"

    #@276
    aput-object v3, v2, v5

    #@278
    const-string v3, "23806"

    #@27a
    aput-object v3, v2, v6

    #@27c
    const-string v3, ""

    #@27e
    aput-object v3, v2, v7

    #@280
    aput-object v2, v0, v1

    #@282
    const/16 v1, 0x1d

    #@284
    new-array v2, v8, [Ljava/lang/String;

    #@286
    const-string v3, "BIB"

    #@288
    aput-object v3, v2, v4

    #@28a
    const-string v3, "DK"

    #@28c
    aput-object v3, v2, v5

    #@28e
    const-string v3, "23802"

    #@290
    aput-object v3, v2, v6

    #@292
    const-string v3, "BiBoB"

    #@294
    aput-object v3, v2, v7

    #@296
    aput-object v2, v0, v1

    #@298
    const/16 v1, 0x1e

    #@29a
    new-array v2, v8, [Ljava/lang/String;

    #@29c
    const-string v3, "CALL"

    #@29e
    aput-object v3, v2, v4

    #@2a0
    const-string v3, "DK"

    #@2a2
    aput-object v3, v2, v5

    #@2a4
    const-string v3, "23820"

    #@2a6
    aput-object v3, v2, v6

    #@2a8
    const-string v3, "Call me"

    #@2aa
    aput-object v3, v2, v7

    #@2ac
    aput-object v2, v0, v1

    #@2ae
    const/16 v1, 0x1f

    #@2b0
    new-array v2, v8, [Ljava/lang/String;

    #@2b2
    const-string v3, "CBB"

    #@2b4
    aput-object v3, v2, v4

    #@2b6
    const-string v3, "DK"

    #@2b8
    aput-object v3, v2, v5

    #@2ba
    const-string v3, "23802"

    #@2bc
    aput-object v3, v2, v6

    #@2be
    const-string v3, "CBB Mobil"

    #@2c0
    aput-object v3, v2, v7

    #@2c2
    aput-object v2, v0, v1

    #@2c4
    const/16 v1, 0x20

    #@2c6
    new-array v2, v8, [Ljava/lang/String;

    #@2c8
    const-string v3, "DLG"

    #@2ca
    aput-object v3, v2, v4

    #@2cc
    const-string v3, "DK"

    #@2ce
    aput-object v3, v2, v5

    #@2d0
    const-string v3, "23820"

    #@2d2
    aput-object v3, v2, v6

    #@2d4
    const-string v3, "DLG Tele"

    #@2d6
    aput-object v3, v2, v7

    #@2d8
    aput-object v2, v0, v1

    #@2da
    const/16 v1, 0x21

    #@2dc
    new-array v2, v8, [Ljava/lang/String;

    #@2de
    const-string v3, "TDC"

    #@2e0
    aput-object v3, v2, v4

    #@2e2
    const-string v3, "DK"

    #@2e4
    aput-object v3, v2, v5

    #@2e6
    const-string v3, "23801"

    #@2e8
    aput-object v3, v2, v6

    #@2ea
    const-string v3, ""

    #@2ec
    aput-object v3, v2, v7

    #@2ee
    aput-object v2, v0, v1

    #@2f0
    const/16 v1, 0x22

    #@2f2
    new-array v2, v8, [Ljava/lang/String;

    #@2f4
    const-string v3, "TEL"

    #@2f6
    aput-object v3, v2, v4

    #@2f8
    const-string v3, "DK"

    #@2fa
    aput-object v3, v2, v5

    #@2fc
    const-string v3, "23802"

    #@2fe
    aput-object v3, v2, v6

    #@300
    const-string v3, ""

    #@302
    aput-object v3, v2, v7

    #@304
    aput-object v2, v0, v1

    #@306
    const/16 v1, 0x23

    #@308
    new-array v2, v8, [Ljava/lang/String;

    #@30a
    const-string v3, "TELI"

    #@30c
    aput-object v3, v2, v4

    #@30e
    const-string v3, "DK"

    #@310
    aput-object v3, v2, v5

    #@312
    const-string v3, "23820"

    #@314
    aput-object v3, v2, v6

    #@316
    const-string v3, "2382010_2382030"

    #@318
    aput-object v3, v2, v7

    #@31a
    aput-object v2, v0, v1

    #@31c
    const/16 v1, 0x24

    #@31e
    new-array v2, v8, [Ljava/lang/String;

    #@320
    const-string v3, "TELI"

    #@322
    aput-object v3, v2, v4

    #@324
    const-string v3, "DK"

    #@326
    aput-object v3, v2, v5

    #@328
    const-string v3, "23830"

    #@32a
    aput-object v3, v2, v6

    #@32c
    const-string v3, ""

    #@32e
    aput-object v3, v2, v7

    #@330
    aput-object v2, v0, v1

    #@332
    const/16 v1, 0x25

    #@334
    new-array v2, v8, [Ljava/lang/String;

    #@336
    const-string v3, "TELM"

    #@338
    aput-object v3, v2, v4

    #@33a
    const-string v3, "DK"

    #@33c
    aput-object v3, v2, v5

    #@33e
    const-string v3, "23801"

    #@340
    aput-object v3, v2, v6

    #@342
    const-string v3, "TELMORE"

    #@344
    aput-object v3, v2, v7

    #@346
    aput-object v2, v0, v1

    #@348
    const/16 v1, 0x26

    #@34a
    new-array v2, v8, [Ljava/lang/String;

    #@34c
    const-string v3, "ELI"

    #@34e
    aput-object v3, v2, v4

    #@350
    const-string v3, "EE"

    #@352
    aput-object v3, v2, v5

    #@354
    const-string v3, "24802"

    #@356
    aput-object v3, v2, v6

    #@358
    const-string v3, ""

    #@35a
    aput-object v3, v2, v7

    #@35c
    aput-object v2, v0, v1

    #@35e
    const/16 v1, 0x27

    #@360
    new-array v2, v8, [Ljava/lang/String;

    #@362
    const-string v3, "EMT"

    #@364
    aput-object v3, v2, v4

    #@366
    const-string v3, "EE"

    #@368
    aput-object v3, v2, v5

    #@36a
    const-string v3, "24801"

    #@36c
    aput-object v3, v2, v6

    #@36e
    const-string v3, ""

    #@370
    aput-object v3, v2, v7

    #@372
    aput-object v2, v0, v1

    #@374
    const/16 v1, 0x28

    #@376
    new-array v2, v8, [Ljava/lang/String;

    #@378
    const-string v3, "EMT"

    #@37a
    aput-object v3, v2, v4

    #@37c
    const-string v3, "EE"

    #@37e
    aput-object v3, v2, v5

    #@380
    const-string v3, "24801"

    #@382
    aput-object v3, v2, v6

    #@384
    const-string v3, "24801012_24801013"

    #@386
    aput-object v3, v2, v7

    #@388
    aput-object v2, v0, v1

    #@38a
    const/16 v1, 0x29

    #@38c
    new-array v2, v8, [Ljava/lang/String;

    #@38e
    const-string v3, "EMT"

    #@390
    aput-object v3, v2, v4

    #@392
    const-string v3, "EE"

    #@394
    aput-object v3, v2, v5

    #@396
    const-string v3, "24801"

    #@398
    aput-object v3, v2, v6

    #@39a
    const-string v3, "24801022_24801023"

    #@39c
    aput-object v3, v2, v7

    #@39e
    aput-object v2, v0, v1

    #@3a0
    const/16 v1, 0x2a

    #@3a2
    new-array v2, v8, [Ljava/lang/String;

    #@3a4
    const-string v3, "TEL2"

    #@3a6
    aput-object v3, v2, v4

    #@3a8
    const-string v3, "EE"

    #@3aa
    aput-object v3, v2, v5

    #@3ac
    const-string v3, "24803"

    #@3ae
    aput-object v3, v2, v6

    #@3b0
    const-string v3, ""

    #@3b2
    aput-object v3, v2, v7

    #@3b4
    aput-object v2, v0, v1

    #@3b6
    const/16 v1, 0x2b

    #@3b8
    new-array v2, v8, [Ljava/lang/String;

    #@3ba
    const-string v3, "TEL2"

    #@3bc
    aput-object v3, v2, v4

    #@3be
    const-string v3, "EE"

    #@3c0
    aput-object v3, v2, v5

    #@3c2
    const-string v3, "24803"

    #@3c4
    aput-object v3, v2, v6

    #@3c6
    const-string v3, "TELE2"

    #@3c8
    aput-object v3, v2, v7

    #@3ca
    aput-object v2, v0, v1

    #@3cc
    const/16 v1, 0x2c

    #@3ce
    new-array v2, v8, [Ljava/lang/String;

    #@3d0
    const-string v3, "DNS"

    #@3d2
    aput-object v3, v2, v4

    #@3d4
    const-string v3, "FI"

    #@3d6
    aput-object v3, v2, v5

    #@3d8
    const-string v3, "24412"

    #@3da
    aput-object v3, v2, v6

    #@3dc
    const-string v3, ""

    #@3de
    aput-object v3, v2, v7

    #@3e0
    aput-object v2, v0, v1

    #@3e2
    const/16 v1, 0x2d

    #@3e4
    new-array v2, v8, [Ljava/lang/String;

    #@3e6
    const-string v3, "DNS"

    #@3e8
    aput-object v3, v2, v4

    #@3ea
    const-string v3, "FI"

    #@3ec
    aput-object v3, v2, v5

    #@3ee
    const-string v3, "24403"

    #@3f0
    aput-object v3, v2, v6

    #@3f2
    const-string v3, ""

    #@3f4
    aput-object v3, v2, v7

    #@3f6
    aput-object v2, v0, v1

    #@3f8
    const/16 v1, 0x2e

    #@3fa
    new-array v2, v8, [Ljava/lang/String;

    #@3fc
    const-string v3, "ELI"

    #@3fe
    aput-object v3, v2, v4

    #@400
    const-string v3, "FI"

    #@402
    aput-object v3, v2, v5

    #@404
    const-string v3, "24405"

    #@406
    aput-object v3, v2, v6

    #@408
    const-string v3, ""

    #@40a
    aput-object v3, v2, v7

    #@40c
    aput-object v2, v0, v1

    #@40e
    const/16 v1, 0x2f

    #@410
    new-array v2, v8, [Ljava/lang/String;

    #@412
    const-string v3, "SAU"

    #@414
    aput-object v3, v2, v4

    #@416
    const-string v3, "FI"

    #@418
    aput-object v3, v2, v5

    #@41a
    const-string v3, "24421"

    #@41c
    aput-object v3, v2, v6

    #@41e
    const-string v3, ""

    #@420
    aput-object v3, v2, v7

    #@422
    aput-object v2, v0, v1

    #@424
    const/16 v1, 0x30

    #@426
    new-array v2, v8, [Ljava/lang/String;

    #@428
    const-string v3, "SON"

    #@42a
    aput-object v3, v2, v4

    #@42c
    const-string v3, "FI"

    #@42e
    aput-object v3, v2, v5

    #@430
    const-string v3, "24499"

    #@432
    aput-object v3, v2, v6

    #@434
    const-string v3, ""

    #@436
    aput-object v3, v2, v7

    #@438
    aput-object v2, v0, v1

    #@43a
    const/16 v1, 0x31

    #@43c
    new-array v2, v8, [Ljava/lang/String;

    #@43e
    const-string v3, "TMO"

    #@440
    aput-object v3, v2, v4

    #@442
    const-string v3, "MK"

    #@444
    aput-object v3, v2, v5

    #@446
    const-string v3, "29401"

    #@448
    aput-object v3, v2, v6

    #@44a
    const-string v3, ""

    #@44c
    aput-object v3, v2, v7

    #@44e
    aput-object v2, v0, v1

    #@450
    const/16 v1, 0x32

    #@452
    new-array v2, v8, [Ljava/lang/String;

    #@454
    const-string v3, "VIP"

    #@456
    aput-object v3, v2, v4

    #@458
    const-string v3, "MK"

    #@45a
    aput-object v3, v2, v5

    #@45c
    const-string v3, "29403"

    #@45e
    aput-object v3, v2, v6

    #@460
    const-string v3, ""

    #@462
    aput-object v3, v2, v7

    #@464
    aput-object v2, v0, v1

    #@466
    const/16 v1, 0x33

    #@468
    new-array v2, v8, [Ljava/lang/String;

    #@46a
    const-string v3, "ONE"

    #@46c
    aput-object v3, v2, v4

    #@46e
    const-string v3, "MK"

    #@470
    aput-object v3, v2, v5

    #@472
    const-string v3, "29402"

    #@474
    aput-object v3, v2, v6

    #@476
    const-string v3, ""

    #@478
    aput-object v3, v2, v7

    #@47a
    aput-object v2, v0, v1

    #@47c
    const/16 v1, 0x34

    #@47e
    new-array v2, v8, [Ljava/lang/String;

    #@480
    const-string v3, "ONE"

    #@482
    aput-object v3, v2, v4

    #@484
    const-string v3, "MK"

    #@486
    aput-object v3, v2, v5

    #@488
    const-string v3, "29402"

    #@48a
    aput-object v3, v2, v6

    #@48c
    const-string v3, "294020001"

    #@48e
    aput-object v3, v2, v7

    #@490
    aput-object v2, v0, v1

    #@492
    const/16 v1, 0x35

    #@494
    new-array v2, v8, [Ljava/lang/String;

    #@496
    const-string v3, "AUC"

    #@498
    aput-object v3, v2, v4

    #@49a
    const-string v3, "FR"

    #@49c
    aput-object v3, v2, v5

    #@49e
    const-string v3, "20810"

    #@4a0
    aput-object v3, v2, v6

    #@4a2
    const-string v3, "A MOBILE"

    #@4a4
    aput-object v3, v2, v7

    #@4a6
    aput-object v2, v0, v1

    #@4a8
    const/16 v1, 0x36

    #@4aa
    new-array v2, v8, [Ljava/lang/String;

    #@4ac
    const-string v3, "BYT"

    #@4ae
    aput-object v3, v2, v4

    #@4b0
    const-string v3, "FR"

    #@4b2
    aput-object v3, v2, v5

    #@4b4
    const-string v3, "20820"

    #@4b6
    aput-object v3, v2, v6

    #@4b8
    const-string v3, ""

    #@4ba
    aput-object v3, v2, v7

    #@4bc
    aput-object v2, v0, v1

    #@4be
    const/16 v1, 0x37

    #@4c0
    new-array v2, v8, [Ljava/lang/String;

    #@4c2
    const-string v3, "CAR"

    #@4c4
    aput-object v3, v2, v4

    #@4c6
    const-string v3, "FR"

    #@4c8
    aput-object v3, v2, v5

    #@4ca
    const-string v3, "20801"

    #@4cc
    aput-object v3, v2, v6

    #@4ce
    const-string v3, "33"

    #@4d0
    aput-object v3, v2, v7

    #@4d2
    aput-object v2, v0, v1

    #@4d4
    const/16 v1, 0x38

    #@4d6
    new-array v2, v8, [Ljava/lang/String;

    #@4d8
    const-string v3, "COR"

    #@4da
    aput-object v3, v2, v4

    #@4dc
    const-string v3, "FR"

    #@4de
    aput-object v3, v2, v5

    #@4e0
    const-string v3, "20810"

    #@4e2
    aput-object v3, v2, v6

    #@4e4
    const-string v3, "12"

    #@4e6
    aput-object v3, v2, v7

    #@4e8
    aput-object v2, v0, v1

    #@4ea
    const/16 v1, 0x39

    #@4ec
    new-array v2, v8, [Ljava/lang/String;

    #@4ee
    const-string v3, "DAR"

    #@4f0
    aput-object v3, v2, v4

    #@4f2
    const-string v3, "FR"

    #@4f4
    aput-object v3, v2, v5

    #@4f6
    const-string v3, "20810"

    #@4f8
    aput-object v3, v2, v6

    #@4fa
    const-string v3, "44"

    #@4fc
    aput-object v3, v2, v7

    #@4fe
    aput-object v2, v0, v1

    #@500
    const/16 v1, 0x3a

    #@502
    new-array v2, v8, [Ljava/lang/String;

    #@504
    const-string v3, "FREE"

    #@506
    aput-object v3, v2, v4

    #@508
    const-string v3, "FR"

    #@50a
    aput-object v3, v2, v5

    #@50c
    const-string v3, "20815"

    #@50e
    aput-object v3, v2, v6

    #@510
    const-string v3, ""

    #@512
    aput-object v3, v2, v7

    #@514
    aput-object v2, v0, v1

    #@516
    const/16 v1, 0x3b

    #@518
    new-array v2, v8, [Ljava/lang/String;

    #@51a
    const-string v3, "LPM"

    #@51c
    aput-object v3, v2, v4

    #@51e
    const-string v3, "FR"

    #@520
    aput-object v3, v2, v5

    #@522
    const-string v3, "20810"

    #@524
    aput-object v3, v2, v6

    #@526
    const-string v3, "4C"

    #@528
    aput-object v3, v2, v7

    #@52a
    aput-object v2, v0, v1

    #@52c
    const/16 v1, 0x3c

    #@52e
    new-array v2, v8, [Ljava/lang/String;

    #@530
    const-string v3, "LEC"

    #@532
    aput-object v3, v2, v4

    #@534
    const-string v3, "FR"

    #@536
    aput-object v3, v2, v5

    #@538
    const-string v3, "20810"

    #@53a
    aput-object v3, v2, v6

    #@53c
    const-string v3, "LeclercMobile"

    #@53e
    aput-object v3, v2, v7

    #@540
    aput-object v2, v0, v1

    #@542
    const/16 v1, 0x3d

    #@544
    new-array v2, v8, [Ljava/lang/String;

    #@546
    const-string v3, "NRJ"

    #@548
    aput-object v3, v2, v4

    #@54a
    const-string v3, "FR"

    #@54c
    aput-object v3, v2, v5

    #@54e
    const-string v3, "20801"

    #@550
    aput-object v3, v2, v6

    #@552
    const-string v3, "4E"

    #@554
    aput-object v3, v2, v7

    #@556
    aput-object v2, v0, v1

    #@558
    const/16 v1, 0x3e

    #@55a
    new-array v2, v8, [Ljava/lang/String;

    #@55c
    const-string v3, "NRJ"

    #@55e
    aput-object v3, v2, v4

    #@560
    const-string v3, "FR"

    #@562
    aput-object v3, v2, v5

    #@564
    const-string v3, "20810"

    #@566
    aput-object v3, v2, v6

    #@568
    const-string v3, "4E"

    #@56a
    aput-object v3, v2, v7

    #@56c
    aput-object v2, v0, v1

    #@56e
    const/16 v1, 0x3f

    #@570
    new-array v2, v8, [Ljava/lang/String;

    #@572
    const-string v3, "ORG"

    #@574
    aput-object v3, v2, v4

    #@576
    const-string v3, "FR"

    #@578
    aput-object v3, v2, v5

    #@57a
    const-string v3, "20801"

    #@57c
    aput-object v3, v2, v6

    #@57e
    const-string v3, ""

    #@580
    aput-object v3, v2, v7

    #@582
    aput-object v2, v0, v1

    #@584
    const/16 v1, 0x40

    #@586
    new-array v2, v8, [Ljava/lang/String;

    #@588
    const-string v3, "ORG"

    #@58a
    aput-object v3, v2, v4

    #@58c
    const-string v3, "FR"

    #@58e
    aput-object v3, v2, v5

    #@590
    const-string v3, "34001"

    #@592
    aput-object v3, v2, v6

    #@594
    const-string v3, ""

    #@596
    aput-object v3, v2, v7

    #@598
    aput-object v2, v0, v1

    #@59a
    const/16 v1, 0x41

    #@59c
    new-array v2, v8, [Ljava/lang/String;

    #@59e
    const-string v3, "ORG"

    #@5a0
    aput-object v3, v2, v4

    #@5a2
    const-string v3, "FR"

    #@5a4
    aput-object v3, v2, v5

    #@5a6
    const-string v3, "64700"

    #@5a8
    aput-object v3, v2, v6

    #@5aa
    const-string v3, ""

    #@5ac
    aput-object v3, v2, v7

    #@5ae
    aput-object v2, v0, v1

    #@5b0
    const/16 v1, 0x42

    #@5b2
    new-array v2, v8, [Ljava/lang/String;

    #@5b4
    const-string v3, "SFR"

    #@5b6
    aput-object v3, v2, v4

    #@5b8
    const-string v3, "FR"

    #@5ba
    aput-object v3, v2, v5

    #@5bc
    const-string v3, "20810"

    #@5be
    aput-object v3, v2, v6

    #@5c0
    const-string v3, ""

    #@5c2
    aput-object v3, v2, v7

    #@5c4
    aput-object v2, v0, v1

    #@5c6
    const/16 v1, 0x43

    #@5c8
    new-array v2, v8, [Ljava/lang/String;

    #@5ca
    const-string v3, "SFR"

    #@5cc
    aput-object v3, v2, v4

    #@5ce
    const-string v3, "FR"

    #@5d0
    aput-object v3, v2, v5

    #@5d2
    const-string v3, "64710"

    #@5d4
    aput-object v3, v2, v6

    #@5d6
    const-string v3, ""

    #@5d8
    aput-object v3, v2, v7

    #@5da
    aput-object v2, v0, v1

    #@5dc
    const/16 v1, 0x44

    #@5de
    new-array v2, v8, [Ljava/lang/String;

    #@5e0
    const-string v3, "VIR"

    #@5e2
    aput-object v3, v2, v4

    #@5e4
    const-string v3, "FR"

    #@5e6
    aput-object v3, v2, v5

    #@5e8
    const-string v3, "20801"

    #@5ea
    aput-object v3, v2, v6

    #@5ec
    const-string v3, "52"

    #@5ee
    aput-object v3, v2, v7

    #@5f0
    aput-object v2, v0, v1

    #@5f2
    const/16 v1, 0x45

    #@5f4
    new-array v2, v8, [Ljava/lang/String;

    #@5f6
    const-string v3, "VIR"

    #@5f8
    aput-object v3, v2, v4

    #@5fa
    const-string v3, "FR"

    #@5fc
    aput-object v3, v2, v5

    #@5fe
    const-string v3, "20823"

    #@600
    aput-object v3, v2, v6

    #@602
    const-string v3, "52"

    #@604
    aput-object v3, v2, v7

    #@606
    aput-object v2, v0, v1

    #@608
    const/16 v1, 0x46

    #@60a
    new-array v2, v8, [Ljava/lang/String;

    #@60c
    const-string v3, "VIR"

    #@60e
    aput-object v3, v2, v4

    #@610
    const-string v3, "FR"

    #@612
    aput-object v3, v2, v5

    #@614
    const-string v3, "20601"

    #@616
    aput-object v3, v2, v6

    #@618
    const-string v3, "52"

    #@61a
    aput-object v3, v2, v7

    #@61c
    aput-object v2, v0, v1

    #@61e
    const/16 v1, 0x47

    #@620
    new-array v2, v8, [Ljava/lang/String;

    #@622
    const-string v3, "DIG"

    #@624
    aput-object v3, v2, v4

    #@626
    const-string v3, "FR"

    #@628
    aput-object v3, v2, v5

    #@62a
    const-string v3, "34020"

    #@62c
    aput-object v3, v2, v6

    #@62e
    const-string v3, ""

    #@630
    aput-object v3, v2, v7

    #@632
    aput-object v2, v0, v1

    #@634
    const/16 v1, 0x48

    #@636
    new-array v2, v8, [Ljava/lang/String;

    #@638
    const-string v3, "ORC"

    #@63a
    aput-object v3, v2, v4

    #@63c
    const-string v3, "FR"

    #@63e
    aput-object v3, v2, v5

    #@640
    const-string v3, "34001"

    #@642
    aput-object v3, v2, v6

    #@644
    const-string v3, ""

    #@646
    aput-object v3, v2, v7

    #@648
    aput-object v2, v0, v1

    #@64a
    const/16 v1, 0x49

    #@64c
    new-array v2, v8, [Ljava/lang/String;

    #@64e
    const-string v3, "OUT"

    #@650
    aput-object v3, v2, v4

    #@652
    const-string v3, "FR"

    #@654
    aput-object v3, v2, v5

    #@656
    const-string v3, "34002"

    #@658
    aput-object v3, v2, v6

    #@65a
    const-string v3, ""

    #@65c
    aput-object v3, v2, v7

    #@65e
    aput-object v2, v0, v1

    #@660
    const/16 v1, 0x4a

    #@662
    new-array v2, v8, [Ljava/lang/String;

    #@664
    const-string v3, "ORR"

    #@666
    aput-object v3, v2, v4

    #@668
    const-string v3, "FR"

    #@66a
    aput-object v3, v2, v5

    #@66c
    const-string v3, "64700"

    #@66e
    aput-object v3, v2, v6

    #@670
    const-string v3, ""

    #@672
    aput-object v3, v2, v7

    #@674
    aput-object v2, v0, v1

    #@676
    const/16 v1, 0x4b

    #@678
    new-array v2, v8, [Ljava/lang/String;

    #@67a
    const-string v3, "OUT"

    #@67c
    aput-object v3, v2, v4

    #@67e
    const-string v3, "FR"

    #@680
    aput-object v3, v2, v5

    #@682
    const-string v3, "64702"

    #@684
    aput-object v3, v2, v6

    #@686
    const-string v3, ""

    #@688
    aput-object v3, v2, v7

    #@68a
    aput-object v2, v0, v1

    #@68c
    const/16 v1, 0x4c

    #@68e
    new-array v2, v8, [Ljava/lang/String;

    #@690
    const-string v3, "SRR"

    #@692
    aput-object v3, v2, v4

    #@694
    const-string v3, "FR"

    #@696
    aput-object v3, v2, v5

    #@698
    const-string v3, "64710"

    #@69a
    aput-object v3, v2, v6

    #@69c
    const-string v3, ""

    #@69e
    aput-object v3, v2, v7

    #@6a0
    aput-object v2, v0, v1

    #@6a2
    const/16 v1, 0x4d

    #@6a4
    new-array v2, v8, [Ljava/lang/String;

    #@6a6
    const-string v3, "COR"

    #@6a8
    aput-object v3, v2, v4

    #@6aa
    const-string v3, "FR"

    #@6ac
    aput-object v3, v2, v5

    #@6ae
    const-string v3, "64710"

    #@6b0
    aput-object v3, v2, v6

    #@6b2
    const-string v3, "12"

    #@6b4
    aput-object v3, v2, v7

    #@6b6
    aput-object v2, v0, v1

    #@6b8
    const/16 v1, 0x4e

    #@6ba
    new-array v2, v8, [Ljava/lang/String;

    #@6bc
    const-string v3, "1&1"

    #@6be
    aput-object v3, v2, v4

    #@6c0
    const-string v3, "DE"

    #@6c2
    aput-object v3, v2, v5

    #@6c4
    const-string v3, "26202"

    #@6c6
    aput-object v3, v2, v6

    #@6c8
    const-string v3, "1&1"

    #@6ca
    aput-object v3, v2, v7

    #@6cc
    aput-object v2, v0, v1

    #@6ce
    const/16 v1, 0x4f

    #@6d0
    new-array v2, v8, [Ljava/lang/String;

    #@6d2
    const-string v3, "DEB"

    #@6d4
    aput-object v3, v2, v4

    #@6d6
    const-string v3, "DE"

    #@6d8
    aput-object v3, v2, v5

    #@6da
    const-string v3, "26203"

    #@6dc
    aput-object v3, v2, v6

    #@6de
    const-string v3, "debitel"

    #@6e0
    aput-object v3, v2, v7

    #@6e2
    aput-object v2, v0, v1

    #@6e4
    const/16 v1, 0x50

    #@6e6
    new-array v2, v8, [Ljava/lang/String;

    #@6e8
    const-string v3, "DEB"

    #@6ea
    aput-object v3, v2, v4

    #@6ec
    const-string v3, "DE"

    #@6ee
    aput-object v3, v2, v5

    #@6f0
    const-string v3, "26203"

    #@6f2
    aput-object v3, v2, v6

    #@6f4
    const-string v3, "mobilcom-debitel"

    #@6f6
    aput-object v3, v2, v7

    #@6f8
    aput-object v2, v0, v1

    #@6fa
    const/16 v1, 0x51

    #@6fc
    new-array v2, v8, [Ljava/lang/String;

    #@6fe
    const-string v3, "DEB"

    #@700
    aput-object v3, v2, v4

    #@702
    const-string v3, "DE"

    #@704
    aput-object v3, v2, v5

    #@706
    const-string v3, "26207"

    #@708
    aput-object v3, v2, v6

    #@70a
    const-string v3, "debitel"

    #@70c
    aput-object v3, v2, v7

    #@70e
    aput-object v2, v0, v1

    #@710
    const/16 v1, 0x52

    #@712
    new-array v2, v8, [Ljava/lang/String;

    #@714
    const-string v3, "DEB"

    #@716
    aput-object v3, v2, v4

    #@718
    const-string v3, "DE"

    #@71a
    aput-object v3, v2, v5

    #@71c
    const-string v3, "26207"

    #@71e
    aput-object v3, v2, v6

    #@720
    const-string v3, "mobilcom-debitel"

    #@722
    aput-object v3, v2, v7

    #@724
    aput-object v2, v0, v1

    #@726
    const/16 v1, 0x53

    #@728
    new-array v2, v8, [Ljava/lang/String;

    #@72a
    const-string v3, "DEB"

    #@72c
    aput-object v3, v2, v4

    #@72e
    const-string v3, "DE"

    #@730
    aput-object v3, v2, v5

    #@732
    const-string v3, "26201"

    #@734
    aput-object v3, v2, v6

    #@736
    const-string v3, "debitel"

    #@738
    aput-object v3, v2, v7

    #@73a
    aput-object v2, v0, v1

    #@73c
    const/16 v1, 0x54

    #@73e
    new-array v2, v8, [Ljava/lang/String;

    #@740
    const-string v3, "DEB"

    #@742
    aput-object v3, v2, v4

    #@744
    const-string v3, "DE"

    #@746
    aput-object v3, v2, v5

    #@748
    const-string v3, "26201"

    #@74a
    aput-object v3, v2, v6

    #@74c
    const-string v3, "mobilcom-debitel"

    #@74e
    aput-object v3, v2, v7

    #@750
    aput-object v2, v0, v1

    #@752
    const/16 v1, 0x55

    #@754
    new-array v2, v8, [Ljava/lang/String;

    #@756
    const-string v3, "DEB"

    #@758
    aput-object v3, v2, v4

    #@75a
    const-string v3, "DE"

    #@75c
    aput-object v3, v2, v5

    #@75e
    const-string v3, "26202"

    #@760
    aput-object v3, v2, v6

    #@762
    const-string v3, "debitel"

    #@764
    aput-object v3, v2, v7

    #@766
    aput-object v2, v0, v1

    #@768
    const/16 v1, 0x56

    #@76a
    new-array v2, v8, [Ljava/lang/String;

    #@76c
    const-string v3, "DEB"

    #@76e
    aput-object v3, v2, v4

    #@770
    const-string v3, "DE"

    #@772
    aput-object v3, v2, v5

    #@774
    const-string v3, "26202"

    #@776
    aput-object v3, v2, v6

    #@778
    const-string v3, "mobilcom-debitel"

    #@77a
    aput-object v3, v2, v7

    #@77c
    aput-object v2, v0, v1

    #@77e
    const/16 v1, 0x57

    #@780
    new-array v2, v8, [Ljava/lang/String;

    #@782
    const-string v3, "EPLU"

    #@784
    aput-object v3, v2, v4

    #@786
    const-string v3, "DE"

    #@788
    aput-object v3, v2, v5

    #@78a
    const-string v3, "26203"

    #@78c
    aput-object v3, v2, v6

    #@78e
    const-string v3, ""

    #@790
    aput-object v3, v2, v7

    #@792
    aput-object v2, v0, v1

    #@794
    const/16 v1, 0x58

    #@796
    new-array v2, v8, [Ljava/lang/String;

    #@798
    const-string v3, "O2"

    #@79a
    aput-object v3, v2, v4

    #@79c
    const-string v3, "DE"

    #@79e
    aput-object v3, v2, v5

    #@7a0
    const-string v3, "26207"

    #@7a2
    aput-object v3, v2, v6

    #@7a4
    const-string v3, ""

    #@7a6
    aput-object v3, v2, v7

    #@7a8
    aput-object v2, v0, v1

    #@7aa
    const/16 v1, 0x59

    #@7ac
    new-array v2, v8, [Ljava/lang/String;

    #@7ae
    const-string v3, "O2"

    #@7b0
    aput-object v3, v2, v4

    #@7b2
    const-string v3, "DE"

    #@7b4
    aput-object v3, v2, v5

    #@7b6
    const-string v3, "26207"

    #@7b8
    aput-object v3, v2, v6

    #@7ba
    const-string v3, "262075_2620749"

    #@7bc
    aput-object v3, v2, v7

    #@7be
    aput-object v2, v0, v1

    #@7c0
    const/16 v1, 0x5a

    #@7c2
    new-array v2, v8, [Ljava/lang/String;

    #@7c4
    const-string v3, "TCH"

    #@7c6
    aput-object v3, v2, v4

    #@7c8
    const-string v3, "DE"

    #@7ca
    aput-object v3, v2, v5

    #@7cc
    const-string v3, "26207"

    #@7ce
    aput-object v3, v2, v6

    #@7d0
    const-string v3, "Tchibo"

    #@7d2
    aput-object v3, v2, v7

    #@7d4
    aput-object v2, v0, v1

    #@7d6
    const/16 v1, 0x5b

    #@7d8
    new-array v2, v8, [Ljava/lang/String;

    #@7da
    const-string v3, "TMO"

    #@7dc
    aput-object v3, v2, v4

    #@7de
    const-string v3, "DE"

    #@7e0
    aput-object v3, v2, v5

    #@7e2
    const-string v3, "26201"

    #@7e4
    aput-object v3, v2, v6

    #@7e6
    const-string v3, ""

    #@7e8
    aput-object v3, v2, v7

    #@7ea
    aput-object v2, v0, v1

    #@7ec
    const/16 v1, 0x5c

    #@7ee
    new-array v2, v8, [Ljava/lang/String;

    #@7f0
    const-string v3, "VDF"

    #@7f2
    aput-object v3, v2, v4

    #@7f4
    const-string v3, "DE"

    #@7f6
    aput-object v3, v2, v5

    #@7f8
    const-string v3, "26202"

    #@7fa
    aput-object v3, v2, v6

    #@7fc
    const-string v3, ""

    #@7fe
    aput-object v3, v2, v7

    #@800
    aput-object v2, v0, v1

    #@802
    const/16 v1, 0x5d

    #@804
    new-array v2, v8, [Ljava/lang/String;

    #@806
    const-string v3, "COS"

    #@808
    aput-object v3, v2, v4

    #@80a
    const-string v3, "GR"

    #@80c
    aput-object v3, v2, v5

    #@80e
    const-string v3, "20201"

    #@810
    aput-object v3, v2, v6

    #@812
    const-string v3, ""

    #@814
    aput-object v3, v2, v7

    #@816
    aput-object v2, v0, v1

    #@818
    const/16 v1, 0x5e

    #@81a
    new-array v2, v8, [Ljava/lang/String;

    #@81c
    const-string v3, "VDF"

    #@81e
    aput-object v3, v2, v4

    #@820
    const-string v3, "GR"

    #@822
    aput-object v3, v2, v5

    #@824
    const-string v3, "20205"

    #@826
    aput-object v3, v2, v6

    #@828
    const-string v3, ""

    #@82a
    aput-object v3, v2, v7

    #@82c
    aput-object v2, v0, v1

    #@82e
    const/16 v1, 0x5f

    #@830
    new-array v2, v8, [Ljava/lang/String;

    #@832
    const-string v3, "Q"

    #@834
    aput-object v3, v2, v4

    #@836
    const-string v3, "GR"

    #@838
    aput-object v3, v2, v5

    #@83a
    const-string v3, "20209"

    #@83c
    aput-object v3, v2, v6

    #@83e
    const-string v3, ""

    #@840
    aput-object v3, v2, v7

    #@842
    aput-object v2, v0, v1

    #@844
    const/16 v1, 0x60

    #@846
    new-array v2, v8, [Ljava/lang/String;

    #@848
    const-string v3, "WIND"

    #@84a
    aput-object v3, v2, v4

    #@84c
    const-string v3, "GR"

    #@84e
    aput-object v3, v2, v5

    #@850
    const-string v3, "20210"

    #@852
    aput-object v3, v2, v6

    #@854
    const-string v3, ""

    #@856
    aput-object v3, v2, v7

    #@858
    aput-object v2, v0, v1

    #@85a
    const/16 v1, 0x61

    #@85c
    new-array v2, v8, [Ljava/lang/String;

    #@85e
    const-string v3, "TELE"

    #@860
    aput-object v3, v2, v4

    #@862
    const-string v3, "GL"

    #@864
    aput-object v3, v2, v5

    #@866
    const-string v3, "29001"

    #@868
    aput-object v3, v2, v6

    #@86a
    const-string v3, ""

    #@86c
    aput-object v3, v2, v7

    #@86e
    aput-object v2, v0, v1

    #@870
    const/16 v1, 0x62

    #@872
    new-array v2, v8, [Ljava/lang/String;

    #@874
    const-string v3, "TEL"

    #@876
    aput-object v3, v2, v4

    #@878
    const-string v3, "HU"

    #@87a
    aput-object v3, v2, v5

    #@87c
    const-string v3, "21601"

    #@87e
    aput-object v3, v2, v6

    #@880
    const-string v3, ""

    #@882
    aput-object v3, v2, v7

    #@884
    aput-object v2, v0, v1

    #@886
    const/16 v1, 0x63

    #@888
    new-array v2, v8, [Ljava/lang/String;

    #@88a
    const-string v3, "TMO"

    #@88c
    aput-object v3, v2, v4

    #@88e
    const-string v3, "HU"

    #@890
    aput-object v3, v2, v5

    #@892
    const-string v3, "21630"

    #@894
    aput-object v3, v2, v6

    #@896
    const-string v3, ""

    #@898
    aput-object v3, v2, v7

    #@89a
    aput-object v2, v0, v1

    #@89c
    const/16 v1, 0x64

    #@89e
    new-array v2, v8, [Ljava/lang/String;

    #@8a0
    const-string v3, "DJU"

    #@8a2
    aput-object v3, v2, v4

    #@8a4
    const-string v3, "HU"

    #@8a6
    aput-object v3, v2, v5

    #@8a8
    const-string v3, "21601"

    #@8aa
    aput-object v3, v2, v6

    #@8ac
    const-string v3, "Djuice"

    #@8ae
    aput-object v3, v2, v7

    #@8b0
    aput-object v2, v0, v1

    #@8b2
    const/16 v1, 0x65

    #@8b4
    new-array v2, v8, [Ljava/lang/String;

    #@8b6
    const-string v3, "VDF"

    #@8b8
    aput-object v3, v2, v4

    #@8ba
    const-string v3, "HU"

    #@8bc
    aput-object v3, v2, v5

    #@8be
    const-string v3, "21670"

    #@8c0
    aput-object v3, v2, v6

    #@8c2
    const-string v3, ""

    #@8c4
    aput-object v3, v2, v7

    #@8c6
    aput-object v2, v0, v1

    #@8c8
    const/16 v1, 0x66

    #@8ca
    new-array v2, v8, [Ljava/lang/String;

    #@8cc
    const-string v3, "VDF"

    #@8ce
    aput-object v3, v2, v4

    #@8d0
    const-string v3, "HU"

    #@8d2
    aput-object v3, v2, v5

    #@8d4
    const-string v3, "21670"

    #@8d6
    aput-object v3, v2, v6

    #@8d8
    const-string v3, "21670XX2"

    #@8da
    aput-object v3, v2, v7

    #@8dc
    aput-object v2, v0, v1

    #@8de
    const/16 v1, 0x67

    #@8e0
    new-array v2, v8, [Ljava/lang/String;

    #@8e2
    const-string v3, "RED"

    #@8e4
    aput-object v3, v2, v4

    #@8e6
    const-string v3, "HU"

    #@8e8
    aput-object v3, v2, v5

    #@8ea
    const-string v3, "21601"

    #@8ec
    aput-object v3, v2, v6

    #@8ee
    const-string v3, "RedBull"

    #@8f0
    aput-object v3, v2, v7

    #@8f2
    aput-object v2, v0, v1

    #@8f4
    const/16 v1, 0x68

    #@8f6
    new-array v2, v8, [Ljava/lang/String;

    #@8f8
    const-string v3, "NOVA"

    #@8fa
    aput-object v3, v2, v4

    #@8fc
    const-string v3, "IS"

    #@8fe
    aput-object v3, v2, v5

    #@900
    const-string v3, "27411"

    #@902
    aput-object v3, v2, v6

    #@904
    const-string v3, ""

    #@906
    aput-object v3, v2, v7

    #@908
    aput-object v2, v0, v1

    #@90a
    const/16 v1, 0x69

    #@90c
    new-array v2, v8, [Ljava/lang/String;

    #@90e
    const-string v3, "SIM"

    #@910
    aput-object v3, v2, v4

    #@912
    const-string v3, "IS"

    #@914
    aput-object v3, v2, v5

    #@916
    const-string v3, "27401"

    #@918
    aput-object v3, v2, v6

    #@91a
    const-string v3, ""

    #@91c
    aput-object v3, v2, v7

    #@91e
    aput-object v2, v0, v1

    #@920
    const/16 v1, 0x6a

    #@922
    new-array v2, v8, [Ljava/lang/String;

    #@924
    const-string v3, "VDF"

    #@926
    aput-object v3, v2, v4

    #@928
    const-string v3, "IS"

    #@92a
    aput-object v3, v2, v5

    #@92c
    const-string v3, "27402"

    #@92e
    aput-object v3, v2, v6

    #@930
    const-string v3, ""

    #@932
    aput-object v3, v2, v7

    #@934
    aput-object v2, v0, v1

    #@936
    const/16 v1, 0x6b

    #@938
    new-array v2, v8, [Ljava/lang/String;

    #@93a
    const-string v3, "VDF"

    #@93c
    aput-object v3, v2, v4

    #@93e
    const-string v3, "IS"

    #@940
    aput-object v3, v2, v5

    #@942
    const-string v3, "27403"

    #@944
    aput-object v3, v2, v6

    #@946
    const-string v3, ""

    #@948
    aput-object v3, v2, v7

    #@94a
    aput-object v2, v0, v1

    #@94c
    const/16 v1, 0x6c

    #@94e
    new-array v2, v8, [Ljava/lang/String;

    #@950
    const-string v3, "H3G"

    #@952
    aput-object v3, v2, v4

    #@954
    const-string v3, "IE"

    #@956
    aput-object v3, v2, v5

    #@958
    const-string v3, "27205"

    #@95a
    aput-object v3, v2, v6

    #@95c
    const-string v3, ""

    #@95e
    aput-object v3, v2, v7

    #@960
    aput-object v2, v0, v1

    #@962
    const/16 v1, 0x6d

    #@964
    new-array v2, v8, [Ljava/lang/String;

    #@966
    const-string v3, "ECM"

    #@968
    aput-object v3, v2, v4

    #@96a
    const-string v3, "IE"

    #@96c
    aput-object v3, v2, v5

    #@96e
    const-string v3, "27203"

    #@970
    aput-object v3, v2, v6

    #@972
    const-string v3, "Eircom"

    #@974
    aput-object v3, v2, v7

    #@976
    aput-object v2, v0, v1

    #@978
    const/16 v1, 0x6e

    #@97a
    new-array v2, v8, [Ljava/lang/String;

    #@97c
    const-string v3, "MET"

    #@97e
    aput-object v3, v2, v4

    #@980
    const-string v3, "IE"

    #@982
    aput-object v3, v2, v5

    #@984
    const-string v3, "27203"

    #@986
    aput-object v3, v2, v6

    #@988
    const-string v3, ""

    #@98a
    aput-object v3, v2, v7

    #@98c
    aput-object v2, v0, v1

    #@98e
    const/16 v1, 0x6f

    #@990
    new-array v2, v8, [Ljava/lang/String;

    #@992
    const-string v3, "O2"

    #@994
    aput-object v3, v2, v4

    #@996
    const-string v3, "IE"

    #@998
    aput-object v3, v2, v5

    #@99a
    const-string v3, "27202"

    #@99c
    aput-object v3, v2, v6

    #@99e
    const-string v3, ""

    #@9a0
    aput-object v3, v2, v7

    #@9a2
    aput-object v2, v0, v1

    #@9a4
    const/16 v1, 0x70

    #@9a6
    new-array v2, v8, [Ljava/lang/String;

    #@9a8
    const-string v3, "TSC"

    #@9aa
    aput-object v3, v2, v4

    #@9ac
    const-string v3, "IE"

    #@9ae
    aput-object v3, v2, v5

    #@9b0
    const-string v3, "27211"

    #@9b2
    aput-object v3, v2, v6

    #@9b4
    const-string v3, "0A"

    #@9b6
    aput-object v3, v2, v7

    #@9b8
    aput-object v2, v0, v1

    #@9ba
    const/16 v1, 0x71

    #@9bc
    new-array v2, v8, [Ljava/lang/String;

    #@9be
    const-string v3, "TSC"

    #@9c0
    aput-object v3, v2, v4

    #@9c2
    const-string v3, "IE"

    #@9c4
    aput-object v3, v2, v5

    #@9c6
    const-string v3, "20601"

    #@9c8
    aput-object v3, v2, v6

    #@9ca
    const-string v3, "0A"

    #@9cc
    aput-object v3, v2, v7

    #@9ce
    aput-object v2, v0, v1

    #@9d0
    const/16 v1, 0x72

    #@9d2
    new-array v2, v8, [Ljava/lang/String;

    #@9d4
    const-string v3, "VDF"

    #@9d6
    aput-object v3, v2, v4

    #@9d8
    const-string v3, "IE"

    #@9da
    aput-object v3, v2, v5

    #@9dc
    const-string v3, "27201"

    #@9de
    aput-object v3, v2, v6

    #@9e0
    const-string v3, ""

    #@9e2
    aput-object v3, v2, v7

    #@9e4
    aput-object v2, v0, v1

    #@9e6
    const/16 v1, 0x73

    #@9e8
    new-array v2, v8, [Ljava/lang/String;

    #@9ea
    const-string v3, "VDF"

    #@9ec
    aput-object v3, v2, v4

    #@9ee
    const-string v3, "IT"

    #@9f0
    aput-object v3, v2, v5

    #@9f2
    const-string v3, "22210"

    #@9f4
    aput-object v3, v2, v6

    #@9f6
    const-string v3, ""

    #@9f8
    aput-object v3, v2, v7

    #@9fa
    aput-object v2, v0, v1

    #@9fc
    const/16 v1, 0x74

    #@9fe
    new-array v2, v8, [Ljava/lang/String;

    #@a00
    const-string v3, "TIM"

    #@a02
    aput-object v3, v2, v4

    #@a04
    const-string v3, "IT"

    #@a06
    aput-object v3, v2, v5

    #@a08
    const-string v3, "22201"

    #@a0a
    aput-object v3, v2, v6

    #@a0c
    const-string v3, ""

    #@a0e
    aput-object v3, v2, v7

    #@a10
    aput-object v2, v0, v1

    #@a12
    const/16 v1, 0x75

    #@a14
    new-array v2, v8, [Ljava/lang/String;

    #@a16
    const-string v3, "WIND"

    #@a18
    aput-object v3, v2, v4

    #@a1a
    const-string v3, "IT"

    #@a1c
    aput-object v3, v2, v5

    #@a1e
    const-string v3, "22288"

    #@a20
    aput-object v3, v2, v6

    #@a22
    const-string v3, ""

    #@a24
    aput-object v3, v2, v7

    #@a26
    aput-object v2, v0, v1

    #@a28
    const/16 v1, 0x76

    #@a2a
    new-array v2, v8, [Ljava/lang/String;

    #@a2c
    const-string v3, "H3G"

    #@a2e
    aput-object v3, v2, v4

    #@a30
    const-string v3, "IT"

    #@a32
    aput-object v3, v2, v5

    #@a34
    const-string v3, "22299"

    #@a36
    aput-object v3, v2, v6

    #@a38
    const-string v3, ""

    #@a3a
    aput-object v3, v2, v7

    #@a3c
    aput-object v2, v0, v1

    #@a3e
    const/16 v1, 0x77

    #@a40
    new-array v2, v8, [Ljava/lang/String;

    #@a42
    const-string v3, "FAST"

    #@a44
    aput-object v3, v2, v4

    #@a46
    const-string v3, "IT"

    #@a48
    aput-object v3, v2, v5

    #@a4a
    const-string v3, "22299"

    #@a4c
    aput-object v3, v2, v6

    #@a4e
    const-string v3, "FastWeb"

    #@a50
    aput-object v3, v2, v7

    #@a52
    aput-object v2, v0, v1

    #@a54
    const/16 v1, 0x78

    #@a56
    new-array v2, v8, [Ljava/lang/String;

    #@a58
    const-string v3, "POS"

    #@a5a
    aput-object v3, v2, v4

    #@a5c
    const-string v3, "IT"

    #@a5e
    aput-object v3, v2, v5

    #@a60
    const-string v3, "22210"

    #@a62
    aput-object v3, v2, v6

    #@a64
    const-string v3, "PosteMobile"

    #@a66
    aput-object v3, v2, v7

    #@a68
    aput-object v2, v0, v1

    #@a6a
    const/16 v1, 0x79

    #@a6c
    new-array v2, v8, [Ljava/lang/String;

    #@a6e
    const-string v3, "LMT"

    #@a70
    aput-object v3, v2, v4

    #@a72
    const-string v3, "LV"

    #@a74
    aput-object v3, v2, v5

    #@a76
    const-string v3, "24701"

    #@a78
    aput-object v3, v2, v6

    #@a7a
    const-string v3, ""

    #@a7c
    aput-object v3, v2, v7

    #@a7e
    aput-object v2, v0, v1

    #@a80
    const/16 v1, 0x7a

    #@a82
    new-array v2, v8, [Ljava/lang/String;

    #@a84
    const-string v3, "TEL2"

    #@a86
    aput-object v3, v2, v4

    #@a88
    const-string v3, "LV"

    #@a8a
    aput-object v3, v2, v5

    #@a8c
    const-string v3, "24702"

    #@a8e
    aput-object v3, v2, v6

    #@a90
    const-string v3, ""

    #@a92
    aput-object v3, v2, v7

    #@a94
    aput-object v2, v0, v1

    #@a96
    const/16 v1, 0x7b

    #@a98
    new-array v2, v8, [Ljava/lang/String;

    #@a9a
    const-string v3, "BITE"

    #@a9c
    aput-object v3, v2, v4

    #@a9e
    const-string v3, "LV"

    #@aa0
    aput-object v3, v2, v5

    #@aa2
    const-string v3, "24705"

    #@aa4
    aput-object v3, v2, v6

    #@aa6
    const-string v3, ""

    #@aa8
    aput-object v3, v2, v7

    #@aaa
    aput-object v2, v0, v1

    #@aac
    const/16 v1, 0x7c

    #@aae
    new-array v2, v8, [Ljava/lang/String;

    #@ab0
    const-string v3, "BITE"

    #@ab2
    aput-object v3, v2, v4

    #@ab4
    const-string v3, "LT"

    #@ab6
    aput-object v3, v2, v5

    #@ab8
    const-string v3, "24602"

    #@aba
    aput-object v3, v2, v6

    #@abc
    const-string v3, ""

    #@abe
    aput-object v3, v2, v7

    #@ac0
    aput-object v2, v0, v1

    #@ac2
    const/16 v1, 0x7d

    #@ac4
    new-array v2, v8, [Ljava/lang/String;

    #@ac6
    const-string v3, "OMNI"

    #@ac8
    aput-object v3, v2, v4

    #@aca
    const-string v3, "LT"

    #@acc
    aput-object v3, v2, v5

    #@ace
    const-string v3, "24601"

    #@ad0
    aput-object v3, v2, v6

    #@ad2
    const-string v3, ""

    #@ad4
    aput-object v3, v2, v7

    #@ad6
    aput-object v2, v0, v1

    #@ad8
    const/16 v1, 0x7e

    #@ada
    new-array v2, v8, [Ljava/lang/String;

    #@adc
    const-string v3, "TEL2"

    #@ade
    aput-object v3, v2, v4

    #@ae0
    const-string v3, "LT"

    #@ae2
    aput-object v3, v2, v5

    #@ae4
    const-string v3, "24603"

    #@ae6
    aput-object v3, v2, v6

    #@ae8
    const-string v3, ""

    #@aea
    aput-object v3, v2, v7

    #@aec
    aput-object v2, v0, v1

    #@aee
    const/16 v1, 0x7f

    #@af0
    new-array v2, v8, [Ljava/lang/String;

    #@af2
    const-string v3, "TNG"

    #@af4
    aput-object v3, v2, v4

    #@af6
    const-string v3, "LU"

    #@af8
    aput-object v3, v2, v5

    #@afa
    const-string v3, "27077"

    #@afc
    aput-object v3, v2, v6

    #@afe
    const-string v3, ""

    #@b00
    aput-object v3, v2, v7

    #@b02
    aput-object v2, v0, v1

    #@b04
    const/16 v1, 0x80

    #@b06
    new-array v2, v8, [Ljava/lang/String;

    #@b08
    const-string v3, "LUX"

    #@b0a
    aput-object v3, v2, v4

    #@b0c
    const-string v3, "LU"

    #@b0e
    aput-object v3, v2, v5

    #@b10
    const-string v3, "27001"

    #@b12
    aput-object v3, v2, v6

    #@b14
    const-string v3, ""

    #@b16
    aput-object v3, v2, v7

    #@b18
    aput-object v2, v0, v1

    #@b1a
    const/16 v1, 0x81

    #@b1c
    new-array v2, v8, [Ljava/lang/String;

    #@b1e
    const-string v3, "ORG"

    #@b20
    aput-object v3, v2, v4

    #@b22
    const-string v3, "LU"

    #@b24
    aput-object v3, v2, v5

    #@b26
    const-string v3, "27099"

    #@b28
    aput-object v3, v2, v6

    #@b2a
    const-string v3, ""

    #@b2c
    aput-object v3, v2, v7

    #@b2e
    aput-object v2, v0, v1

    #@b30
    const/16 v1, 0x82

    #@b32
    new-array v2, v8, [Ljava/lang/String;

    #@b34
    const-string v3, "VDF"

    #@b36
    aput-object v3, v2, v4

    #@b38
    const-string v3, "MT"

    #@b3a
    aput-object v3, v2, v5

    #@b3c
    const-string v3, "27801"

    #@b3e
    aput-object v3, v2, v6

    #@b40
    const-string v3, ""

    #@b42
    aput-object v3, v2, v7

    #@b44
    aput-object v2, v0, v1

    #@b46
    const/16 v1, 0x83

    #@b48
    new-array v2, v8, [Ljava/lang/String;

    #@b4a
    const-string v3, "TMO"

    #@b4c
    aput-object v3, v2, v4

    #@b4e
    const-string v3, "ME"

    #@b50
    aput-object v3, v2, v5

    #@b52
    const-string v3, "29702"

    #@b54
    aput-object v3, v2, v6

    #@b56
    const-string v3, ""

    #@b58
    aput-object v3, v2, v7

    #@b5a
    aput-object v2, v0, v1

    #@b5c
    const/16 v1, 0x84

    #@b5e
    new-array v2, v8, [Ljava/lang/String;

    #@b60
    const-string v3, "TMO"

    #@b62
    aput-object v3, v2, v4

    #@b64
    const-string v3, "ME"

    #@b66
    aput-object v3, v2, v5

    #@b68
    const-string v3, "22004"

    #@b6a
    aput-object v3, v2, v6

    #@b6c
    const-string v3, ""

    #@b6e
    aput-object v3, v2, v7

    #@b70
    aput-object v2, v0, v1

    #@b72
    const/16 v1, 0x85

    #@b74
    new-array v2, v8, [Ljava/lang/String;

    #@b76
    const-string v3, "TEL"

    #@b78
    aput-object v3, v2, v4

    #@b7a
    const-string v3, "ME"

    #@b7c
    aput-object v3, v2, v5

    #@b7e
    const-string v3, "29701"

    #@b80
    aput-object v3, v2, v6

    #@b82
    const-string v3, ""

    #@b84
    aput-object v3, v2, v7

    #@b86
    aput-object v2, v0, v1

    #@b88
    const/16 v1, 0x86

    #@b8a
    new-array v2, v8, [Ljava/lang/String;

    #@b8c
    const-string v3, "TEL"

    #@b8e
    aput-object v3, v2, v4

    #@b90
    const-string v3, "ME"

    #@b92
    aput-object v3, v2, v5

    #@b94
    const-string v3, "22002"

    #@b96
    aput-object v3, v2, v6

    #@b98
    const-string v3, ""

    #@b9a
    aput-object v3, v2, v7

    #@b9c
    aput-object v2, v0, v1

    #@b9e
    const/16 v1, 0x87

    #@ba0
    new-array v2, v8, [Ljava/lang/String;

    #@ba2
    const-string v3, "MTEL"

    #@ba4
    aput-object v3, v2, v4

    #@ba6
    const-string v3, "ME"

    #@ba8
    aput-object v3, v2, v5

    #@baa
    const-string v3, "29703"

    #@bac
    aput-object v3, v2, v6

    #@bae
    const-string v3, ""

    #@bb0
    aput-object v3, v2, v7

    #@bb2
    aput-object v2, v0, v1

    #@bb4
    const/16 v1, 0x88

    #@bb6
    new-array v2, v8, [Ljava/lang/String;

    #@bb8
    const-string v3, "BEN"

    #@bba
    aput-object v3, v2, v4

    #@bbc
    const-string v3, "NL"

    #@bbe
    aput-object v3, v2, v5

    #@bc0
    const-string v3, "20416"

    #@bc2
    aput-object v3, v2, v6

    #@bc4
    const-string v3, "Ben NL"

    #@bc6
    aput-object v3, v2, v7

    #@bc8
    aput-object v2, v0, v1

    #@bca
    const/16 v1, 0x89

    #@bcc
    new-array v2, v8, [Ljava/lang/String;

    #@bce
    const-string v3, "VDF"

    #@bd0
    aput-object v3, v2, v4

    #@bd2
    const-string v3, "NL"

    #@bd4
    aput-object v3, v2, v5

    #@bd6
    const-string v3, "20404"

    #@bd8
    aput-object v3, v2, v6

    #@bda
    const-string v3, ""

    #@bdc
    aput-object v3, v2, v7

    #@bde
    aput-object v2, v0, v1

    #@be0
    const/16 v1, 0x8a

    #@be2
    new-array v2, v8, [Ljava/lang/String;

    #@be4
    const-string v3, "TMO"

    #@be6
    aput-object v3, v2, v4

    #@be8
    const-string v3, "NL"

    #@bea
    aput-object v3, v2, v5

    #@bec
    const-string v3, "20416"

    #@bee
    aput-object v3, v2, v6

    #@bf0
    const-string v3, ""

    #@bf2
    aput-object v3, v2, v7

    #@bf4
    aput-object v2, v0, v1

    #@bf6
    const/16 v1, 0x8b

    #@bf8
    new-array v2, v8, [Ljava/lang/String;

    #@bfa
    const-string v3, "TMO"

    #@bfc
    aput-object v3, v2, v4

    #@bfe
    const-string v3, "NL"

    #@c00
    aput-object v3, v2, v5

    #@c02
    const-string v3, "20420"

    #@c04
    aput-object v3, v2, v6

    #@c06
    const-string v3, ""

    #@c08
    aput-object v3, v2, v7

    #@c0a
    aput-object v2, v0, v1

    #@c0c
    const/16 v1, 0x8c

    #@c0e
    new-array v2, v8, [Ljava/lang/String;

    #@c10
    const-string v3, "TELF"

    #@c12
    aput-object v3, v2, v4

    #@c14
    const-string v3, "NL"

    #@c16
    aput-object v3, v2, v5

    #@c18
    const-string v3, "20412"

    #@c1a
    aput-object v3, v2, v6

    #@c1c
    const-string v3, ""

    #@c1e
    aput-object v3, v2, v7

    #@c20
    aput-object v2, v0, v1

    #@c22
    const/16 v1, 0x8d

    #@c24
    new-array v2, v8, [Ljava/lang/String;

    #@c26
    const-string v3, "TEL2"

    #@c28
    aput-object v3, v2, v4

    #@c2a
    const-string v3, "NL"

    #@c2c
    aput-object v3, v2, v5

    #@c2e
    const-string v3, "20402"

    #@c30
    aput-object v3, v2, v6

    #@c32
    const-string v3, ""

    #@c34
    aput-object v3, v2, v7

    #@c36
    aput-object v2, v0, v1

    #@c38
    const/16 v1, 0x8e

    #@c3a
    new-array v2, v8, [Ljava/lang/String;

    #@c3c
    const-string v3, "RABO"

    #@c3e
    aput-object v3, v2, v4

    #@c40
    const-string v3, "NL"

    #@c42
    aput-object v3, v2, v5

    #@c44
    const-string v3, "20408"

    #@c46
    aput-object v3, v2, v6

    #@c48
    const-string v3, "Rabo Mobiel"

    #@c4a
    aput-object v3, v2, v7

    #@c4c
    aput-object v2, v0, v1

    #@c4e
    const/16 v1, 0x8f

    #@c50
    new-array v2, v8, [Ljava/lang/String;

    #@c52
    const-string v3, "KPN"

    #@c54
    aput-object v3, v2, v4

    #@c56
    const-string v3, "NL"

    #@c58
    aput-object v3, v2, v5

    #@c5a
    const-string v3, "20408"

    #@c5c
    aput-object v3, v2, v6

    #@c5e
    const-string v3, ""

    #@c60
    aput-object v3, v2, v7

    #@c62
    aput-object v2, v0, v1

    #@c64
    const/16 v1, 0x90

    #@c66
    new-array v2, v8, [Ljava/lang/String;

    #@c68
    const-string v3, "CES"

    #@c6a
    aput-object v3, v2, v4

    #@c6c
    const-string v3, "NO"

    #@c6e
    aput-object v3, v2, v5

    #@c70
    const-string v3, "24202"

    #@c72
    aput-object v3, v2, v6

    #@c74
    const-string v3, "2420256"

    #@c76
    aput-object v3, v2, v7

    #@c78
    aput-object v2, v0, v1

    #@c7a
    const/16 v1, 0x91

    #@c7c
    new-array v2, v8, [Ljava/lang/String;

    #@c7e
    const-string v3, "NETC"

    #@c80
    aput-object v3, v2, v4

    #@c82
    const-string v3, "NO"

    #@c84
    aput-object v3, v2, v5

    #@c86
    const-string v3, "24202"

    #@c88
    aput-object v3, v2, v6

    #@c8a
    const-string v3, ""

    #@c8c
    aput-object v3, v2, v7

    #@c8e
    aput-object v2, v0, v1

    #@c90
    const/16 v1, 0x92

    #@c92
    new-array v2, v8, [Ljava/lang/String;

    #@c94
    const-string v3, "NET"

    #@c96
    aput-object v3, v2, v4

    #@c98
    const-string v3, "NO"

    #@c9a
    aput-object v3, v2, v5

    #@c9c
    const-string v3, "24205"

    #@c9e
    aput-object v3, v2, v6

    #@ca0
    const-string v3, ""

    #@ca2
    aput-object v3, v2, v7

    #@ca4
    aput-object v2, v0, v1

    #@ca6
    const/16 v1, 0x93

    #@ca8
    new-array v2, v8, [Ljava/lang/String;

    #@caa
    const-string v3, "TEL2"

    #@cac
    aput-object v3, v2, v4

    #@cae
    const-string v3, "NO"

    #@cb0
    aput-object v3, v2, v5

    #@cb2
    const-string v3, "24007"

    #@cb4
    aput-object v3, v2, v6

    #@cb6
    const-string v3, "2400768"

    #@cb8
    aput-object v3, v2, v7

    #@cba
    aput-object v2, v0, v1

    #@cbc
    const/16 v1, 0x94

    #@cbe
    new-array v2, v8, [Ljava/lang/String;

    #@cc0
    const-string v3, "TEL"

    #@cc2
    aput-object v3, v2, v4

    #@cc4
    const-string v3, "NO"

    #@cc6
    aput-object v3, v2, v5

    #@cc8
    const-string v3, "24201"

    #@cca
    aput-object v3, v2, v6

    #@ccc
    const-string v3, ""

    #@cce
    aput-object v3, v2, v7

    #@cd0
    aput-object v2, v0, v1

    #@cd2
    const/16 v1, 0x95

    #@cd4
    new-array v2, v8, [Ljava/lang/String;

    #@cd6
    const-string v3, "VEN"

    #@cd8
    aput-object v3, v2, v4

    #@cda
    const-string v3, "NO"

    #@cdc
    aput-object v3, v2, v5

    #@cde
    const-string v3, "24201"

    #@ce0
    aput-object v3, v2, v6

    #@ce2
    const-string v3, "24201700"

    #@ce4
    aput-object v3, v2, v7

    #@ce6
    aput-object v2, v0, v1

    #@ce8
    const/16 v1, 0x96

    #@cea
    new-array v2, v8, [Ljava/lang/String;

    #@cec
    const-string v3, "TMO"

    #@cee
    aput-object v3, v2, v4

    #@cf0
    const-string v3, "PL"

    #@cf2
    aput-object v3, v2, v5

    #@cf4
    const-string v3, "26002"

    #@cf6
    aput-object v3, v2, v6

    #@cf8
    const-string v3, ""

    #@cfa
    aput-object v3, v2, v7

    #@cfc
    aput-object v2, v0, v1

    #@cfe
    const/16 v1, 0x97

    #@d00
    new-array v2, v8, [Ljava/lang/String;

    #@d02
    const-string v3, "ORG"

    #@d04
    aput-object v3, v2, v4

    #@d06
    const-string v3, "PL"

    #@d08
    aput-object v3, v2, v5

    #@d0a
    const-string v3, "26003"

    #@d0c
    aput-object v3, v2, v6

    #@d0e
    const-string v3, ""

    #@d10
    aput-object v3, v2, v7

    #@d12
    aput-object v2, v0, v1

    #@d14
    const/16 v1, 0x98

    #@d16
    new-array v2, v8, [Ljava/lang/String;

    #@d18
    const-string v3, "P4P"

    #@d1a
    aput-object v3, v2, v4

    #@d1c
    const-string v3, "PL"

    #@d1e
    aput-object v3, v2, v5

    #@d20
    const-string v3, "26006"

    #@d22
    aput-object v3, v2, v6

    #@d24
    const-string v3, ""

    #@d26
    aput-object v3, v2, v7

    #@d28
    aput-object v2, v0, v1

    #@d2a
    const/16 v1, 0x99

    #@d2c
    new-array v2, v8, [Ljava/lang/String;

    #@d2e
    const-string v3, "RED"

    #@d30
    aput-object v3, v2, v4

    #@d32
    const-string v3, "PL"

    #@d34
    aput-object v3, v2, v5

    #@d36
    const-string v3, "26006"

    #@d38
    aput-object v3, v2, v6

    #@d3a
    const-string v3, "Red Bull MOBILE"

    #@d3c
    aput-object v3, v2, v7

    #@d3e
    aput-object v2, v0, v1

    #@d40
    const/16 v1, 0x9a

    #@d42
    new-array v2, v8, [Ljava/lang/String;

    #@d44
    const-string v3, "PLS"

    #@d46
    aput-object v3, v2, v4

    #@d48
    const-string v3, "PL"

    #@d4a
    aput-object v3, v2, v5

    #@d4c
    const-string v3, "26001"

    #@d4e
    aput-object v3, v2, v6

    #@d50
    const-string v3, ""

    #@d52
    aput-object v3, v2, v7

    #@d54
    aput-object v2, v0, v1

    #@d56
    const/16 v1, 0x9b

    #@d58
    new-array v2, v8, [Ljava/lang/String;

    #@d5a
    const-string v3, "ORG"

    #@d5c
    aput-object v3, v2, v4

    #@d5e
    const-string v3, "PT"

    #@d60
    aput-object v3, v2, v5

    #@d62
    const-string v3, "26803"

    #@d64
    aput-object v3, v2, v6

    #@d66
    const-string v3, ""

    #@d68
    aput-object v3, v2, v7

    #@d6a
    aput-object v2, v0, v1

    #@d6c
    const/16 v1, 0x9c

    #@d6e
    new-array v2, v8, [Ljava/lang/String;

    #@d70
    const-string v3, "TMN"

    #@d72
    aput-object v3, v2, v4

    #@d74
    const-string v3, "PT"

    #@d76
    aput-object v3, v2, v5

    #@d78
    const-string v3, "26806"

    #@d7a
    aput-object v3, v2, v6

    #@d7c
    const-string v3, ""

    #@d7e
    aput-object v3, v2, v7

    #@d80
    aput-object v2, v0, v1

    #@d82
    const/16 v1, 0x9d

    #@d84
    new-array v2, v8, [Ljava/lang/String;

    #@d86
    const-string v3, "VDF"

    #@d88
    aput-object v3, v2, v4

    #@d8a
    const-string v3, "PT"

    #@d8c
    aput-object v3, v2, v5

    #@d8e
    const-string v3, "26801"

    #@d90
    aput-object v3, v2, v6

    #@d92
    const-string v3, ""

    #@d94
    aput-object v3, v2, v7

    #@d96
    aput-object v2, v0, v1

    #@d98
    const/16 v1, 0x9e

    #@d9a
    new-array v2, v8, [Ljava/lang/String;

    #@d9c
    const-string v3, "ZON"

    #@d9e
    aput-object v3, v2, v4

    #@da0
    const-string v3, "PT"

    #@da2
    aput-object v3, v2, v5

    #@da4
    const-string v3, "26801"

    #@da6
    aput-object v3, v2, v6

    #@da8
    const-string v3, "ZON"

    #@daa
    aput-object v3, v2, v7

    #@dac
    aput-object v2, v0, v1

    #@dae
    const/16 v1, 0x9f

    #@db0
    new-array v2, v8, [Ljava/lang/String;

    #@db2
    const-string v3, "COS"

    #@db4
    aput-object v3, v2, v4

    #@db6
    const-string v3, "RO"

    #@db8
    aput-object v3, v2, v5

    #@dba
    const-string v3, "22603"

    #@dbc
    aput-object v3, v2, v6

    #@dbe
    const-string v3, ""

    #@dc0
    aput-object v3, v2, v7

    #@dc2
    aput-object v2, v0, v1

    #@dc4
    const/16 v1, 0xa0

    #@dc6
    new-array v2, v8, [Ljava/lang/String;

    #@dc8
    const-string v3, "COS"

    #@dca
    aput-object v3, v2, v4

    #@dcc
    const-string v3, "RO"

    #@dce
    aput-object v3, v2, v5

    #@dd0
    const-string v3, "22606"

    #@dd2
    aput-object v3, v2, v6

    #@dd4
    const-string v3, ""

    #@dd6
    aput-object v3, v2, v7

    #@dd8
    aput-object v2, v0, v1

    #@dda
    const/16 v1, 0xa1

    #@ddc
    new-array v2, v8, [Ljava/lang/String;

    #@dde
    const-string v3, "DIGI"

    #@de0
    aput-object v3, v2, v4

    #@de2
    const-string v3, "RO"

    #@de4
    aput-object v3, v2, v5

    #@de6
    const-string v3, "22605"

    #@de8
    aput-object v3, v2, v6

    #@dea
    const-string v3, ""

    #@dec
    aput-object v3, v2, v7

    #@dee
    aput-object v2, v0, v1

    #@df0
    const/16 v1, 0xa2

    #@df2
    new-array v2, v8, [Ljava/lang/String;

    #@df4
    const-string v3, "ORG"

    #@df6
    aput-object v3, v2, v4

    #@df8
    const-string v3, "RO"

    #@dfa
    aput-object v3, v2, v5

    #@dfc
    const-string v3, "22610"

    #@dfe
    aput-object v3, v2, v6

    #@e00
    const-string v3, ""

    #@e02
    aput-object v3, v2, v7

    #@e04
    aput-object v2, v0, v1

    #@e06
    const/16 v1, 0xa3

    #@e08
    new-array v2, v8, [Ljava/lang/String;

    #@e0a
    const-string v3, "VDF"

    #@e0c
    aput-object v3, v2, v4

    #@e0e
    const-string v3, "RO"

    #@e10
    aput-object v3, v2, v5

    #@e12
    const-string v3, "22601"

    #@e14
    aput-object v3, v2, v6

    #@e16
    const-string v3, ""

    #@e18
    aput-object v3, v2, v7

    #@e1a
    aput-object v2, v0, v1

    #@e1c
    const/16 v1, 0xa4

    #@e1e
    new-array v2, v8, [Ljava/lang/String;

    #@e20
    const-string v3, "MTS"

    #@e22
    aput-object v3, v2, v4

    #@e24
    const-string v3, "RS"

    #@e26
    aput-object v3, v2, v5

    #@e28
    const-string v3, "22003"

    #@e2a
    aput-object v3, v2, v6

    #@e2c
    const-string v3, ""

    #@e2e
    aput-object v3, v2, v7

    #@e30
    aput-object v2, v0, v1

    #@e32
    const/16 v1, 0xa5

    #@e34
    new-array v2, v8, [Ljava/lang/String;

    #@e36
    const-string v3, "TEL"

    #@e38
    aput-object v3, v2, v4

    #@e3a
    const-string v3, "RS"

    #@e3c
    aput-object v3, v2, v5

    #@e3e
    const-string v3, "22001"

    #@e40
    aput-object v3, v2, v6

    #@e42
    const-string v3, ""

    #@e44
    aput-object v3, v2, v7

    #@e46
    aput-object v2, v0, v1

    #@e48
    const/16 v1, 0xa6

    #@e4a
    new-array v2, v8, [Ljava/lang/String;

    #@e4c
    const-string v3, "VIP"

    #@e4e
    aput-object v3, v2, v4

    #@e50
    const-string v3, "RS"

    #@e52
    aput-object v3, v2, v5

    #@e54
    const-string v3, "22005"

    #@e56
    aput-object v3, v2, v6

    #@e58
    const-string v3, ""

    #@e5a
    aput-object v3, v2, v7

    #@e5c
    aput-object v2, v0, v1

    #@e5e
    const/16 v1, 0xa7

    #@e60
    new-array v2, v8, [Ljava/lang/String;

    #@e62
    const-string v3, "O2"

    #@e64
    aput-object v3, v2, v4

    #@e66
    const-string v3, "SK"

    #@e68
    aput-object v3, v2, v5

    #@e6a
    const-string v3, "23106"

    #@e6c
    aput-object v3, v2, v6

    #@e6e
    const-string v3, ""

    #@e70
    aput-object v3, v2, v7

    #@e72
    aput-object v2, v0, v1

    #@e74
    const/16 v1, 0xa8

    #@e76
    new-array v2, v8, [Ljava/lang/String;

    #@e78
    const-string v3, "ORG"

    #@e7a
    aput-object v3, v2, v4

    #@e7c
    const-string v3, "SK"

    #@e7e
    aput-object v3, v2, v5

    #@e80
    const-string v3, "23101"

    #@e82
    aput-object v3, v2, v6

    #@e84
    const-string v3, ""

    #@e86
    aput-object v3, v2, v7

    #@e88
    aput-object v2, v0, v1

    #@e8a
    const/16 v1, 0xa9

    #@e8c
    new-array v2, v8, [Ljava/lang/String;

    #@e8e
    const-string v3, "TMO"

    #@e90
    aput-object v3, v2, v4

    #@e92
    const-string v3, "SK"

    #@e94
    aput-object v3, v2, v5

    #@e96
    const-string v3, "23102"

    #@e98
    aput-object v3, v2, v6

    #@e9a
    const-string v3, ""

    #@e9c
    aput-object v3, v2, v7

    #@e9e
    aput-object v2, v0, v1

    #@ea0
    const/16 v1, 0xaa

    #@ea2
    new-array v2, v8, [Ljava/lang/String;

    #@ea4
    const-string v3, "MOBI"

    #@ea6
    aput-object v3, v2, v4

    #@ea8
    const-string v3, "SI"

    #@eaa
    aput-object v3, v2, v5

    #@eac
    const-string v3, "29341"

    #@eae
    aput-object v3, v2, v6

    #@eb0
    const-string v3, ""

    #@eb2
    aput-object v3, v2, v7

    #@eb4
    aput-object v2, v0, v1

    #@eb6
    const/16 v1, 0xab

    #@eb8
    new-array v2, v8, [Ljava/lang/String;

    #@eba
    const-string v3, "SI"

    #@ebc
    aput-object v3, v2, v4

    #@ebe
    const-string v3, "SI"

    #@ec0
    aput-object v3, v2, v5

    #@ec2
    const-string v3, "29340"

    #@ec4
    aput-object v3, v2, v6

    #@ec6
    const-string v3, ""

    #@ec8
    aput-object v3, v2, v7

    #@eca
    aput-object v2, v0, v1

    #@ecc
    const/16 v1, 0xac

    #@ece
    new-array v2, v8, [Ljava/lang/String;

    #@ed0
    const-string v3, "TUS"

    #@ed2
    aput-object v3, v2, v4

    #@ed4
    const-string v3, "SI"

    #@ed6
    aput-object v3, v2, v5

    #@ed8
    const-string v3, "29370"

    #@eda
    aput-object v3, v2, v6

    #@edc
    const-string v3, ""

    #@ede
    aput-object v3, v2, v7

    #@ee0
    aput-object v2, v0, v1

    #@ee2
    const/16 v1, 0xad

    #@ee4
    new-array v2, v8, [Ljava/lang/String;

    #@ee6
    const-string v3, "YOI"

    #@ee8
    aput-object v3, v2, v4

    #@eea
    const-string v3, "ES"

    #@eec
    aput-object v3, v2, v5

    #@eee
    const-string v3, "21404"

    #@ef0
    aput-object v3, v2, v6

    #@ef2
    const-string v3, ""

    #@ef4
    aput-object v3, v2, v7

    #@ef6
    aput-object v2, v0, v1

    #@ef8
    const/16 v1, 0xae

    #@efa
    new-array v2, v8, [Ljava/lang/String;

    #@efc
    const-string v3, "EUS"

    #@efe
    aput-object v3, v2, v4

    #@f00
    const-string v3, "ES"

    #@f02
    aput-object v3, v2, v5

    #@f04
    const-string v3, "21408"

    #@f06
    aput-object v3, v2, v6

    #@f08
    const-string v3, ""

    #@f0a
    aput-object v3, v2, v7

    #@f0c
    aput-object v2, v0, v1

    #@f0e
    const/16 v1, 0xaf

    #@f10
    new-array v2, v8, [Ljava/lang/String;

    #@f12
    const-string v3, "EUS"

    #@f14
    aput-object v3, v2, v4

    #@f16
    const-string v3, "ES"

    #@f18
    aput-object v3, v2, v5

    #@f1a
    const-string v3, "21406"

    #@f1c
    aput-object v3, v2, v6

    #@f1e
    const-string v3, "00"

    #@f20
    aput-object v3, v2, v7

    #@f22
    aput-object v2, v0, v1

    #@f24
    const/16 v1, 0xb0

    #@f26
    new-array v2, v8, [Ljava/lang/String;

    #@f28
    const-string v3, "ORG"

    #@f2a
    aput-object v3, v2, v4

    #@f2c
    const-string v3, "ES"

    #@f2e
    aput-object v3, v2, v5

    #@f30
    const-string v3, "21403"

    #@f32
    aput-object v3, v2, v6

    #@f34
    const-string v3, ""

    #@f36
    aput-object v3, v2, v7

    #@f38
    aput-object v2, v0, v1

    #@f3a
    const/16 v1, 0xb1

    #@f3c
    new-array v2, v8, [Ljava/lang/String;

    #@f3e
    const-string v3, "TLF"

    #@f40
    aput-object v3, v2, v4

    #@f42
    const-string v3, "ES"

    #@f44
    aput-object v3, v2, v5

    #@f46
    const-string v3, "21407"

    #@f48
    aput-object v3, v2, v6

    #@f4a
    const-string v3, ""

    #@f4c
    aput-object v3, v2, v7

    #@f4e
    aput-object v2, v0, v1

    #@f50
    const/16 v1, 0xb2

    #@f52
    new-array v2, v8, [Ljava/lang/String;

    #@f54
    const-string v3, "TLF"

    #@f56
    aput-object v3, v2, v4

    #@f58
    const-string v3, "ES"

    #@f5a
    aput-object v3, v2, v5

    #@f5c
    const-string v3, "23410"

    #@f5e
    aput-object v3, v2, v6

    #@f60
    const-string v3, "TELEFONICA"

    #@f62
    aput-object v3, v2, v7

    #@f64
    aput-object v2, v0, v1

    #@f66
    const/16 v1, 0xb3

    #@f68
    new-array v2, v8, [Ljava/lang/String;

    #@f6a
    const-string v3, "VDF"

    #@f6c
    aput-object v3, v2, v4

    #@f6e
    const-string v3, "ES"

    #@f70
    aput-object v3, v2, v5

    #@f72
    const-string v3, "21401"

    #@f74
    aput-object v3, v2, v6

    #@f76
    const-string v3, ""

    #@f78
    aput-object v3, v2, v7

    #@f7a
    aput-object v2, v0, v1

    #@f7c
    const/16 v1, 0xb4

    #@f7e
    new-array v2, v8, [Ljava/lang/String;

    #@f80
    const-string v3, "KPN"

    #@f82
    aput-object v3, v2, v4

    #@f84
    const-string v3, "ES"

    #@f86
    aput-object v3, v2, v5

    #@f88
    const-string v3, "21419"

    #@f8a
    aput-object v3, v2, v6

    #@f8c
    const-string v3, ""

    #@f8e
    aput-object v3, v2, v7

    #@f90
    aput-object v2, v0, v1

    #@f92
    const/16 v1, 0xb5

    #@f94
    new-array v2, v8, [Ljava/lang/String;

    #@f96
    const-string v3, "TELC"

    #@f98
    aput-object v3, v2, v4

    #@f9a
    const-string v3, "ES"

    #@f9c
    aput-object v3, v2, v5

    #@f9e
    const-string v3, "21406"

    #@fa0
    aput-object v3, v2, v6

    #@fa2
    const-string v3, "2140613"

    #@fa4
    aput-object v3, v2, v7

    #@fa6
    aput-object v2, v0, v1

    #@fa8
    const/16 v1, 0xb6

    #@faa
    new-array v2, v8, [Ljava/lang/String;

    #@fac
    const-string v3, "ERO"

    #@fae
    aput-object v3, v2, v4

    #@fb0
    const-string v3, "ES"

    #@fb2
    aput-object v3, v2, v5

    #@fb4
    const-string v3, "21406"

    #@fb6
    aput-object v3, v2, v6

    #@fb8
    const-string v3, "2140606"

    #@fba
    aput-object v3, v2, v7

    #@fbc
    aput-object v2, v0, v1

    #@fbe
    const/16 v1, 0xb7

    #@fc0
    new-array v2, v8, [Ljava/lang/String;

    #@fc2
    const-string v3, "TELC"

    #@fc4
    aput-object v3, v2, v4

    #@fc6
    const-string v3, "ES"

    #@fc8
    aput-object v3, v2, v5

    #@fca
    const-string v3, "21416"

    #@fcc
    aput-object v3, v2, v6

    #@fce
    const-string v3, ""

    #@fd0
    aput-object v3, v2, v7

    #@fd2
    aput-object v2, v0, v1

    #@fd4
    const/16 v1, 0xb8

    #@fd6
    new-array v2, v8, [Ljava/lang/String;

    #@fd8
    const-string v3, "ONO"

    #@fda
    aput-object v3, v2, v4

    #@fdc
    const-string v3, "ES"

    #@fde
    aput-object v3, v2, v5

    #@fe0
    const-string v3, "21418"

    #@fe2
    aput-object v3, v2, v6

    #@fe4
    const-string v3, ""

    #@fe6
    aput-object v3, v2, v7

    #@fe8
    aput-object v2, v0, v1

    #@fea
    const/16 v1, 0xb9

    #@fec
    new-array v2, v8, [Ljava/lang/String;

    #@fee
    const-string v3, "YOI"

    #@ff0
    aput-object v3, v2, v4

    #@ff2
    const-string v3, "SP"

    #@ff4
    aput-object v3, v2, v5

    #@ff6
    const-string v3, "21404"

    #@ff8
    aput-object v3, v2, v6

    #@ffa
    const-string v3, ""

    #@ffc
    aput-object v3, v2, v7

    #@ffe
    aput-object v2, v0, v1

    #@1000
    const/16 v1, 0xba

    #@1002
    new-array v2, v8, [Ljava/lang/String;

    #@1004
    const-string v3, "EUS"

    #@1006
    aput-object v3, v2, v4

    #@1008
    const-string v3, "SP"

    #@100a
    aput-object v3, v2, v5

    #@100c
    const-string v3, "21408"

    #@100e
    aput-object v3, v2, v6

    #@1010
    const-string v3, ""

    #@1012
    aput-object v3, v2, v7

    #@1014
    aput-object v2, v0, v1

    #@1016
    const/16 v1, 0xbb

    #@1018
    new-array v2, v8, [Ljava/lang/String;

    #@101a
    const-string v3, "EUS"

    #@101c
    aput-object v3, v2, v4

    #@101e
    const-string v3, "SP"

    #@1020
    aput-object v3, v2, v5

    #@1022
    const-string v3, "21406"

    #@1024
    aput-object v3, v2, v6

    #@1026
    const-string v3, "00"

    #@1028
    aput-object v3, v2, v7

    #@102a
    aput-object v2, v0, v1

    #@102c
    const/16 v1, 0xbc

    #@102e
    new-array v2, v8, [Ljava/lang/String;

    #@1030
    const-string v3, "ORG"

    #@1032
    aput-object v3, v2, v4

    #@1034
    const-string v3, "SP"

    #@1036
    aput-object v3, v2, v5

    #@1038
    const-string v3, "21403"

    #@103a
    aput-object v3, v2, v6

    #@103c
    const-string v3, ""

    #@103e
    aput-object v3, v2, v7

    #@1040
    aput-object v2, v0, v1

    #@1042
    const/16 v1, 0xbd

    #@1044
    new-array v2, v8, [Ljava/lang/String;

    #@1046
    const-string v3, "TLF"

    #@1048
    aput-object v3, v2, v4

    #@104a
    const-string v3, "SP"

    #@104c
    aput-object v3, v2, v5

    #@104e
    const-string v3, "21407"

    #@1050
    aput-object v3, v2, v6

    #@1052
    const-string v3, ""

    #@1054
    aput-object v3, v2, v7

    #@1056
    aput-object v2, v0, v1

    #@1058
    const/16 v1, 0xbe

    #@105a
    new-array v2, v8, [Ljava/lang/String;

    #@105c
    const-string v3, "VDF"

    #@105e
    aput-object v3, v2, v4

    #@1060
    const-string v3, "SP"

    #@1062
    aput-object v3, v2, v5

    #@1064
    const-string v3, "21401"

    #@1066
    aput-object v3, v2, v6

    #@1068
    const-string v3, ""

    #@106a
    aput-object v3, v2, v7

    #@106c
    aput-object v2, v0, v1

    #@106e
    const/16 v1, 0xbf

    #@1070
    new-array v2, v8, [Ljava/lang/String;

    #@1072
    const-string v3, "KPN"

    #@1074
    aput-object v3, v2, v4

    #@1076
    const-string v3, "SP"

    #@1078
    aput-object v3, v2, v5

    #@107a
    const-string v3, "21419"

    #@107c
    aput-object v3, v2, v6

    #@107e
    const-string v3, ""

    #@1080
    aput-object v3, v2, v7

    #@1082
    aput-object v2, v0, v1

    #@1084
    const/16 v1, 0xc0

    #@1086
    new-array v2, v8, [Ljava/lang/String;

    #@1088
    const-string v3, "TELC"

    #@108a
    aput-object v3, v2, v4

    #@108c
    const-string v3, "SP"

    #@108e
    aput-object v3, v2, v5

    #@1090
    const-string v3, "21406"

    #@1092
    aput-object v3, v2, v6

    #@1094
    const-string v3, "2140613"

    #@1096
    aput-object v3, v2, v7

    #@1098
    aput-object v2, v0, v1

    #@109a
    const/16 v1, 0xc1

    #@109c
    new-array v2, v8, [Ljava/lang/String;

    #@109e
    const-string v3, "ERO"

    #@10a0
    aput-object v3, v2, v4

    #@10a2
    const-string v3, "SP"

    #@10a4
    aput-object v3, v2, v5

    #@10a6
    const-string v3, "21406"

    #@10a8
    aput-object v3, v2, v6

    #@10aa
    const-string v3, "2140606"

    #@10ac
    aput-object v3, v2, v7

    #@10ae
    aput-object v2, v0, v1

    #@10b0
    const/16 v1, 0xc2

    #@10b2
    new-array v2, v8, [Ljava/lang/String;

    #@10b4
    const-string v3, "TELC"

    #@10b6
    aput-object v3, v2, v4

    #@10b8
    const-string v3, "SP"

    #@10ba
    aput-object v3, v2, v5

    #@10bc
    const-string v3, "21416"

    #@10be
    aput-object v3, v2, v6

    #@10c0
    const-string v3, ""

    #@10c2
    aput-object v3, v2, v7

    #@10c4
    aput-object v2, v0, v1

    #@10c6
    const/16 v1, 0xc3

    #@10c8
    new-array v2, v8, [Ljava/lang/String;

    #@10ca
    const-string v3, "ONO"

    #@10cc
    aput-object v3, v2, v4

    #@10ce
    const-string v3, "SP"

    #@10d0
    aput-object v3, v2, v5

    #@10d2
    const-string v3, "21418"

    #@10d4
    aput-object v3, v2, v6

    #@10d6
    const-string v3, ""

    #@10d8
    aput-object v3, v2, v7

    #@10da
    aput-object v2, v0, v1

    #@10dc
    const/16 v1, 0xc4

    #@10de
    new-array v2, v8, [Ljava/lang/String;

    #@10e0
    const-string v3, "H3G"

    #@10e2
    aput-object v3, v2, v4

    #@10e4
    const-string v3, "SE"

    #@10e6
    aput-object v3, v2, v5

    #@10e8
    const-string v3, "24002"

    #@10ea
    aput-object v3, v2, v6

    #@10ec
    const-string v3, ""

    #@10ee
    aput-object v3, v2, v7

    #@10f0
    aput-object v2, v0, v1

    #@10f2
    const/16 v1, 0xc5

    #@10f4
    new-array v2, v8, [Ljava/lang/String;

    #@10f6
    const-string v3, "H3G"

    #@10f8
    aput-object v3, v2, v4

    #@10fa
    const-string v3, "SE"

    #@10fc
    aput-object v3, v2, v5

    #@10fe
    const-string v3, "24004"

    #@1100
    aput-object v3, v2, v6

    #@1102
    const-string v3, ""

    #@1104
    aput-object v3, v2, v7

    #@1106
    aput-object v2, v0, v1

    #@1108
    const/16 v1, 0xc6

    #@110a
    new-array v2, v8, [Ljava/lang/String;

    #@110c
    const-string v3, "TEL"

    #@110e
    aput-object v3, v2, v4

    #@1110
    const-string v3, "SE"

    #@1112
    aput-object v3, v2, v5

    #@1114
    const-string v3, "24008"

    #@1116
    aput-object v3, v2, v6

    #@1118
    const-string v3, ""

    #@111a
    aput-object v3, v2, v7

    #@111c
    aput-object v2, v0, v1

    #@111e
    const/16 v1, 0xc7

    #@1120
    new-array v2, v8, [Ljava/lang/String;

    #@1122
    const-string v3, "TEL"

    #@1124
    aput-object v3, v2, v4

    #@1126
    const-string v3, "SE"

    #@1128
    aput-object v3, v2, v5

    #@112a
    const-string v3, "24004"

    #@112c
    aput-object v3, v2, v6

    #@112e
    const-string v3, "Telenor SE"

    #@1130
    aput-object v3, v2, v7

    #@1132
    aput-object v2, v0, v1

    #@1134
    const/16 v1, 0xc8

    #@1136
    new-array v2, v8, [Ljava/lang/String;

    #@1138
    const-string v3, "TEL"

    #@113a
    aput-object v3, v2, v4

    #@113c
    const-string v3, "SE"

    #@113e
    aput-object v3, v2, v5

    #@1140
    const-string v3, "24024"

    #@1142
    aput-object v3, v2, v6

    #@1144
    const-string v3, "Telenor SE"

    #@1146
    aput-object v3, v2, v7

    #@1148
    aput-object v2, v0, v1

    #@114a
    const/16 v1, 0xc9

    #@114c
    new-array v2, v8, [Ljava/lang/String;

    #@114e
    const-string v3, "TELI"

    #@1150
    aput-object v3, v2, v4

    #@1152
    const-string v3, "SE"

    #@1154
    aput-object v3, v2, v5

    #@1156
    const-string v3, "24001"

    #@1158
    aput-object v3, v2, v6

    #@115a
    const-string v3, ""

    #@115c
    aput-object v3, v2, v7

    #@115e
    aput-object v2, v0, v1

    #@1160
    const/16 v1, 0xca

    #@1162
    new-array v2, v8, [Ljava/lang/String;

    #@1164
    const-string v3, "TELI"

    #@1166
    aput-object v3, v2, v4

    #@1168
    const-string v3, "SE"

    #@116a
    aput-object v3, v2, v5

    #@116c
    const-string v3, "24005"

    #@116e
    aput-object v3, v2, v6

    #@1170
    const-string v3, "Telia"

    #@1172
    aput-object v3, v2, v7

    #@1174
    aput-object v2, v0, v1

    #@1176
    const/16 v1, 0xcb

    #@1178
    new-array v2, v8, [Ljava/lang/String;

    #@117a
    const-string v3, "TEL2"

    #@117c
    aput-object v3, v2, v4

    #@117e
    const-string v3, "SE"

    #@1180
    aput-object v3, v2, v5

    #@1182
    const-string v3, "24007"

    #@1184
    aput-object v3, v2, v6

    #@1186
    const-string v3, ""

    #@1188
    aput-object v3, v2, v7

    #@118a
    aput-object v2, v0, v1

    #@118c
    const/16 v1, 0xcc

    #@118e
    new-array v2, v8, [Ljava/lang/String;

    #@1190
    const-string v3, "TEL2"

    #@1192
    aput-object v3, v2, v4

    #@1194
    const-string v3, "SE"

    #@1196
    aput-object v3, v2, v5

    #@1198
    const-string v3, "24005"

    #@119a
    aput-object v3, v2, v6

    #@119c
    const-string v3, "Tele2"

    #@119e
    aput-object v3, v2, v7

    #@11a0
    aput-object v2, v0, v1

    #@11a2
    const/16 v1, 0xcd

    #@11a4
    new-array v2, v8, [Ljava/lang/String;

    #@11a6
    const-string v3, "TEL2"

    #@11a8
    aput-object v3, v2, v4

    #@11aa
    const-string v3, "SE"

    #@11ac
    aput-object v3, v2, v5

    #@11ae
    const-string v3, "24024"

    #@11b0
    aput-object v3, v2, v6

    #@11b2
    const-string v3, "Tele2"

    #@11b4
    aput-object v3, v2, v7

    #@11b6
    aput-object v2, v0, v1

    #@11b8
    const/16 v1, 0xce

    #@11ba
    new-array v2, v8, [Ljava/lang/String;

    #@11bc
    const-string v3, "HALE"

    #@11be
    aput-object v3, v2, v4

    #@11c0
    const-string v3, "SE"

    #@11c2
    aput-object v3, v2, v5

    #@11c4
    const-string v3, "24001"

    #@11c6
    aput-object v3, v2, v6

    #@11c8
    const-string v3, "240017"

    #@11ca
    aput-object v3, v2, v7

    #@11cc
    aput-object v2, v0, v1

    #@11ce
    const/16 v1, 0xcf

    #@11d0
    new-array v2, v8, [Ljava/lang/String;

    #@11d2
    const-string v3, "HALE"

    #@11d4
    aput-object v3, v2, v4

    #@11d6
    const-string v3, "SE"

    #@11d8
    aput-object v3, v2, v5

    #@11da
    const-string v3, "24005"

    #@11dc
    aput-object v3, v2, v6

    #@11de
    const-string v3, "Halebop"

    #@11e0
    aput-object v3, v2, v7

    #@11e2
    aput-object v2, v0, v1

    #@11e4
    const/16 v1, 0xd0

    #@11e6
    new-array v2, v8, [Ljava/lang/String;

    #@11e8
    const-string v3, "SUN"

    #@11ea
    aput-object v3, v2, v4

    #@11ec
    const-string v3, "CH"

    #@11ee
    aput-object v3, v2, v5

    #@11f0
    const-string v3, "22802"

    #@11f2
    aput-object v3, v2, v6

    #@11f4
    const-string v3, ""

    #@11f6
    aput-object v3, v2, v7

    #@11f8
    aput-object v2, v0, v1

    #@11fa
    const/16 v1, 0xd1

    #@11fc
    new-array v2, v8, [Ljava/lang/String;

    #@11fe
    const-string v3, "ORG"

    #@1200
    aput-object v3, v2, v4

    #@1202
    const-string v3, "CH"

    #@1204
    aput-object v3, v2, v5

    #@1206
    const-string v3, "22803"

    #@1208
    aput-object v3, v2, v6

    #@120a
    const-string v3, ""

    #@120c
    aput-object v3, v2, v7

    #@120e
    aput-object v2, v0, v1

    #@1210
    const/16 v1, 0xd2

    #@1212
    new-array v2, v8, [Ljava/lang/String;

    #@1214
    const-string v3, "ORG"

    #@1216
    aput-object v3, v2, v4

    #@1218
    const-string v3, "CH"

    #@121a
    aput-object v3, v2, v5

    #@121c
    const-string v3, "22803"

    #@121e
    aput-object v3, v2, v6

    #@1220
    const-string v3, "22803[1-9][1-8]"

    #@1222
    aput-object v3, v2, v7

    #@1224
    aput-object v2, v0, v1

    #@1226
    const/16 v1, 0xd3

    #@1228
    new-array v2, v8, [Ljava/lang/String;

    #@122a
    const-string v3, "SWI"

    #@122c
    aput-object v3, v2, v4

    #@122e
    const-string v3, "CH"

    #@1230
    aput-object v3, v2, v5

    #@1232
    const-string v3, "22801"

    #@1234
    aput-object v3, v2, v6

    #@1236
    const-string v3, ""

    #@1238
    aput-object v3, v2, v7

    #@123a
    aput-object v2, v0, v1

    #@123c
    const/16 v1, 0xd4

    #@123e
    new-array v2, v8, [Ljava/lang/String;

    #@1240
    const-string v3, "LEB"

    #@1242
    aput-object v3, v2, v4

    #@1244
    const-string v3, "CH"

    #@1246
    aput-object v3, v2, v5

    #@1248
    const-string v3, "22802"

    #@124a
    aput-object v3, v2, v6

    #@124c
    const-string v3, "228024X45"

    #@124e
    aput-object v3, v2, v7

    #@1250
    aput-object v2, v0, v1

    #@1252
    const/16 v1, 0xd5

    #@1254
    new-array v2, v8, [Ljava/lang/String;

    #@1256
    const-string v3, "VDF"

    #@1258
    aput-object v3, v2, v4

    #@125a
    const-string v3, "GB"

    #@125c
    aput-object v3, v2, v5

    #@125e
    const-string v3, "23415"

    #@1260
    aput-object v3, v2, v6

    #@1262
    const-string v3, ""

    #@1264
    aput-object v3, v2, v7

    #@1266
    aput-object v2, v0, v1

    #@1268
    const/16 v1, 0xd6

    #@126a
    new-array v2, v8, [Ljava/lang/String;

    #@126c
    const-string v3, "H3G"

    #@126e
    aput-object v3, v2, v4

    #@1270
    const-string v3, "GB"

    #@1272
    aput-object v3, v2, v5

    #@1274
    const-string v3, "23420"

    #@1276
    aput-object v3, v2, v6

    #@1278
    const-string v3, ""

    #@127a
    aput-object v3, v2, v7

    #@127c
    aput-object v2, v0, v1

    #@127e
    const/16 v1, 0xd7

    #@1280
    new-array v2, v8, [Ljava/lang/String;

    #@1282
    const-string v3, "H3G"

    #@1284
    aput-object v3, v2, v4

    #@1286
    const-string v3, "GB"

    #@1288
    aput-object v3, v2, v5

    #@128a
    const-string v3, "23594"

    #@128c
    aput-object v3, v2, v6

    #@128e
    const-string v3, ""

    #@1290
    aput-object v3, v2, v7

    #@1292
    aput-object v2, v0, v1

    #@1294
    const/16 v1, 0xd8

    #@1296
    new-array v2, v8, [Ljava/lang/String;

    #@1298
    const-string v3, "O2"

    #@129a
    aput-object v3, v2, v4

    #@129c
    const-string v3, "GB"

    #@129e
    aput-object v3, v2, v5

    #@12a0
    const-string v3, "23410"

    #@12a2
    aput-object v3, v2, v6

    #@12a4
    const-string v3, ""

    #@12a6
    aput-object v3, v2, v7

    #@12a8
    aput-object v2, v0, v1

    #@12aa
    const/16 v1, 0xd9

    #@12ac
    new-array v2, v8, [Ljava/lang/String;

    #@12ae
    const-string v3, "ORG"

    #@12b0
    aput-object v3, v2, v4

    #@12b2
    const-string v3, "GB"

    #@12b4
    aput-object v3, v2, v5

    #@12b6
    const-string v3, "23433"

    #@12b8
    aput-object v3, v2, v6

    #@12ba
    const-string v3, ""

    #@12bc
    aput-object v3, v2, v7

    #@12be
    aput-object v2, v0, v1

    #@12c0
    const/16 v1, 0xda

    #@12c2
    new-array v2, v8, [Ljava/lang/String;

    #@12c4
    const-string v3, "ORG"

    #@12c6
    aput-object v3, v2, v4

    #@12c8
    const-string v3, "GB"

    #@12ca
    aput-object v3, v2, v5

    #@12cc
    const-string v3, "23430"

    #@12ce
    aput-object v3, v2, v6

    #@12d0
    const-string v3, ""

    #@12d2
    aput-object v3, v2, v7

    #@12d4
    aput-object v2, v0, v1

    #@12d6
    const/16 v1, 0xdb

    #@12d8
    new-array v2, v8, [Ljava/lang/String;

    #@12da
    const-string v3, "ORG"

    #@12dc
    aput-object v3, v2, v4

    #@12de
    const-string v3, "GB"

    #@12e0
    aput-object v3, v2, v5

    #@12e2
    const-string v3, "23431"

    #@12e4
    aput-object v3, v2, v6

    #@12e6
    const-string v3, ""

    #@12e8
    aput-object v3, v2, v7

    #@12ea
    aput-object v2, v0, v1

    #@12ec
    const/16 v1, 0xdc

    #@12ee
    new-array v2, v8, [Ljava/lang/String;

    #@12f0
    const-string v3, "ORG"

    #@12f2
    aput-object v3, v2, v4

    #@12f4
    const-string v3, "GB"

    #@12f6
    aput-object v3, v2, v5

    #@12f8
    const-string v3, "23432"

    #@12fa
    aput-object v3, v2, v6

    #@12fc
    const-string v3, ""

    #@12fe
    aput-object v3, v2, v7

    #@1300
    aput-object v2, v0, v1

    #@1302
    const/16 v1, 0xdd

    #@1304
    new-array v2, v8, [Ljava/lang/String;

    #@1306
    const-string v3, "TALK"

    #@1308
    aput-object v3, v2, v4

    #@130a
    const-string v3, "GB"

    #@130c
    aput-object v3, v2, v5

    #@130e
    const-string v3, "23415"

    #@1310
    aput-object v3, v2, v6

    #@1312
    const-string v3, "C1"

    #@1314
    aput-object v3, v2, v7

    #@1316
    aput-object v2, v0, v1

    #@1318
    const/16 v1, 0xde

    #@131a
    new-array v2, v8, [Ljava/lang/String;

    #@131c
    const-string v3, "TSC"

    #@131e
    aput-object v3, v2, v4

    #@1320
    const-string v3, "GB"

    #@1322
    aput-object v3, v2, v5

    #@1324
    const-string v3, "23410"

    #@1326
    aput-object v3, v2, v6

    #@1328
    const-string v3, "0A"

    #@132a
    aput-object v3, v2, v7

    #@132c
    aput-object v2, v0, v1

    #@132e
    const/16 v1, 0xdf

    #@1330
    new-array v2, v8, [Ljava/lang/String;

    #@1332
    const-string v3, "TSC"

    #@1334
    aput-object v3, v2, v4

    #@1336
    const-string v3, "GB"

    #@1338
    aput-object v3, v2, v5

    #@133a
    const-string v3, "27211"

    #@133c
    aput-object v3, v2, v6

    #@133e
    const-string v3, "0A"

    #@1340
    aput-object v3, v2, v7

    #@1342
    aput-object v2, v0, v1

    #@1344
    const/16 v1, 0xe0

    #@1346
    new-array v2, v8, [Ljava/lang/String;

    #@1348
    const-string v3, "TSC"

    #@134a
    aput-object v3, v2, v4

    #@134c
    const-string v3, "GB"

    #@134e
    aput-object v3, v2, v5

    #@1350
    const-string v3, "20601"

    #@1352
    aput-object v3, v2, v6

    #@1354
    const-string v3, "0A"

    #@1356
    aput-object v3, v2, v7

    #@1358
    aput-object v2, v0, v1

    #@135a
    const/16 v1, 0xe1

    #@135c
    new-array v2, v8, [Ljava/lang/String;

    #@135e
    const-string v3, "ASDA"

    #@1360
    aput-object v3, v2, v4

    #@1362
    const-string v3, "GB"

    #@1364
    aput-object v3, v2, v5

    #@1366
    const-string v3, "23415"

    #@1368
    aput-object v3, v2, v6

    #@136a
    const-string v3, "A0"

    #@136c
    aput-object v3, v2, v7

    #@136e
    aput-object v2, v0, v1

    #@1370
    const/16 v1, 0xe2

    #@1372
    new-array v2, v8, [Ljava/lang/String;

    #@1374
    const-string v3, "VIR"

    #@1376
    aput-object v3, v2, v4

    #@1378
    const-string v3, "GB"

    #@137a
    aput-object v3, v2, v5

    #@137c
    const-string v3, "23430"

    #@137e
    aput-object v3, v2, v6

    #@1380
    const-string v3, "28"

    #@1382
    aput-object v3, v2, v7

    #@1384
    aput-object v2, v0, v1

    #@1386
    const/16 v1, 0xe3

    #@1388
    new-array v2, v8, [Ljava/lang/String;

    #@138a
    const-string v3, "VDF"

    #@138c
    aput-object v3, v2, v4

    #@138e
    const-string v3, "UK"

    #@1390
    aput-object v3, v2, v5

    #@1392
    const-string v3, "23415"

    #@1394
    aput-object v3, v2, v6

    #@1396
    const-string v3, ""

    #@1398
    aput-object v3, v2, v7

    #@139a
    aput-object v2, v0, v1

    #@139c
    const/16 v1, 0xe4

    #@139e
    new-array v2, v8, [Ljava/lang/String;

    #@13a0
    const-string v3, "H3G"

    #@13a2
    aput-object v3, v2, v4

    #@13a4
    const-string v3, "UK"

    #@13a6
    aput-object v3, v2, v5

    #@13a8
    const-string v3, "23420"

    #@13aa
    aput-object v3, v2, v6

    #@13ac
    const-string v3, ""

    #@13ae
    aput-object v3, v2, v7

    #@13b0
    aput-object v2, v0, v1

    #@13b2
    const/16 v1, 0xe5

    #@13b4
    new-array v2, v8, [Ljava/lang/String;

    #@13b6
    const-string v3, "H3G"

    #@13b8
    aput-object v3, v2, v4

    #@13ba
    const-string v3, "UK"

    #@13bc
    aput-object v3, v2, v5

    #@13be
    const-string v3, "23594"

    #@13c0
    aput-object v3, v2, v6

    #@13c2
    const-string v3, ""

    #@13c4
    aput-object v3, v2, v7

    #@13c6
    aput-object v2, v0, v1

    #@13c8
    const/16 v1, 0xe6

    #@13ca
    new-array v2, v8, [Ljava/lang/String;

    #@13cc
    const-string v3, "O2"

    #@13ce
    aput-object v3, v2, v4

    #@13d0
    const-string v3, "UK"

    #@13d2
    aput-object v3, v2, v5

    #@13d4
    const-string v3, "23410"

    #@13d6
    aput-object v3, v2, v6

    #@13d8
    const-string v3, ""

    #@13da
    aput-object v3, v2, v7

    #@13dc
    aput-object v2, v0, v1

    #@13de
    const/16 v1, 0xe7

    #@13e0
    new-array v2, v8, [Ljava/lang/String;

    #@13e2
    const-string v3, "ORG"

    #@13e4
    aput-object v3, v2, v4

    #@13e6
    const-string v3, "UK"

    #@13e8
    aput-object v3, v2, v5

    #@13ea
    const-string v3, "23433"

    #@13ec
    aput-object v3, v2, v6

    #@13ee
    const-string v3, ""

    #@13f0
    aput-object v3, v2, v7

    #@13f2
    aput-object v2, v0, v1

    #@13f4
    const/16 v1, 0xe8

    #@13f6
    new-array v2, v8, [Ljava/lang/String;

    #@13f8
    const-string v3, "ORG"

    #@13fa
    aput-object v3, v2, v4

    #@13fc
    const-string v3, "UK"

    #@13fe
    aput-object v3, v2, v5

    #@1400
    const-string v3, "23430"

    #@1402
    aput-object v3, v2, v6

    #@1404
    const-string v3, ""

    #@1406
    aput-object v3, v2, v7

    #@1408
    aput-object v2, v0, v1

    #@140a
    const/16 v1, 0xe9

    #@140c
    new-array v2, v8, [Ljava/lang/String;

    #@140e
    const-string v3, "ORG"

    #@1410
    aput-object v3, v2, v4

    #@1412
    const-string v3, "UK"

    #@1414
    aput-object v3, v2, v5

    #@1416
    const-string v3, "23431"

    #@1418
    aput-object v3, v2, v6

    #@141a
    const-string v3, ""

    #@141c
    aput-object v3, v2, v7

    #@141e
    aput-object v2, v0, v1

    #@1420
    const/16 v1, 0xea

    #@1422
    new-array v2, v8, [Ljava/lang/String;

    #@1424
    const-string v3, "ORG"

    #@1426
    aput-object v3, v2, v4

    #@1428
    const-string v3, "UK"

    #@142a
    aput-object v3, v2, v5

    #@142c
    const-string v3, "23432"

    #@142e
    aput-object v3, v2, v6

    #@1430
    const-string v3, ""

    #@1432
    aput-object v3, v2, v7

    #@1434
    aput-object v2, v0, v1

    #@1436
    const/16 v1, 0xeb

    #@1438
    new-array v2, v8, [Ljava/lang/String;

    #@143a
    const-string v3, "EVE"

    #@143c
    aput-object v3, v2, v4

    #@143e
    const-string v3, "UK"

    #@1440
    aput-object v3, v2, v5

    #@1442
    const-string v3, "23430"

    #@1444
    aput-object v3, v2, v6

    #@1446
    const-string v3, ""

    #@1448
    aput-object v3, v2, v7

    #@144a
    aput-object v2, v0, v1

    #@144c
    const/16 v1, 0xec

    #@144e
    new-array v2, v8, [Ljava/lang/String;

    #@1450
    const-string v3, "EVE"

    #@1452
    aput-object v3, v2, v4

    #@1454
    const-string v3, "UK"

    #@1456
    aput-object v3, v2, v5

    #@1458
    const-string v3, "23431"

    #@145a
    aput-object v3, v2, v6

    #@145c
    const-string v3, ""

    #@145e
    aput-object v3, v2, v7

    #@1460
    aput-object v2, v0, v1

    #@1462
    const/16 v1, 0xed

    #@1464
    new-array v2, v8, [Ljava/lang/String;

    #@1466
    const-string v3, "EVE"

    #@1468
    aput-object v3, v2, v4

    #@146a
    const-string v3, "UK"

    #@146c
    aput-object v3, v2, v5

    #@146e
    const-string v3, "23432"

    #@1470
    aput-object v3, v2, v6

    #@1472
    const-string v3, ""

    #@1474
    aput-object v3, v2, v7

    #@1476
    aput-object v2, v0, v1

    #@1478
    const/16 v1, 0xee

    #@147a
    new-array v2, v8, [Ljava/lang/String;

    #@147c
    const-string v3, "EVE"

    #@147e
    aput-object v3, v2, v4

    #@1480
    const-string v3, "UK"

    #@1482
    aput-object v3, v2, v5

    #@1484
    const-string v3, "23433"

    #@1486
    aput-object v3, v2, v6

    #@1488
    const-string v3, ""

    #@148a
    aput-object v3, v2, v7

    #@148c
    aput-object v2, v0, v1

    #@148e
    const/16 v1, 0xef

    #@1490
    new-array v2, v8, [Ljava/lang/String;

    #@1492
    const-string v3, "TALK"

    #@1494
    aput-object v3, v2, v4

    #@1496
    const-string v3, "UK"

    #@1498
    aput-object v3, v2, v5

    #@149a
    const-string v3, "23415"

    #@149c
    aput-object v3, v2, v6

    #@149e
    const-string v3, "C1"

    #@14a0
    aput-object v3, v2, v7

    #@14a2
    aput-object v2, v0, v1

    #@14a4
    const/16 v1, 0xf0

    #@14a6
    new-array v2, v8, [Ljava/lang/String;

    #@14a8
    const-string v3, "TSC"

    #@14aa
    aput-object v3, v2, v4

    #@14ac
    const-string v3, "UK"

    #@14ae
    aput-object v3, v2, v5

    #@14b0
    const-string v3, "23410"

    #@14b2
    aput-object v3, v2, v6

    #@14b4
    const-string v3, "0A"

    #@14b6
    aput-object v3, v2, v7

    #@14b8
    aput-object v2, v0, v1

    #@14ba
    const/16 v1, 0xf1

    #@14bc
    new-array v2, v8, [Ljava/lang/String;

    #@14be
    const-string v3, "TSC"

    #@14c0
    aput-object v3, v2, v4

    #@14c2
    const-string v3, "UK"

    #@14c4
    aput-object v3, v2, v5

    #@14c6
    const-string v3, "27211"

    #@14c8
    aput-object v3, v2, v6

    #@14ca
    const-string v3, "0A"

    #@14cc
    aput-object v3, v2, v7

    #@14ce
    aput-object v2, v0, v1

    #@14d0
    const/16 v1, 0xf2

    #@14d2
    new-array v2, v8, [Ljava/lang/String;

    #@14d4
    const-string v3, "TSC"

    #@14d6
    aput-object v3, v2, v4

    #@14d8
    const-string v3, "UK"

    #@14da
    aput-object v3, v2, v5

    #@14dc
    const-string v3, "20601"

    #@14de
    aput-object v3, v2, v6

    #@14e0
    const-string v3, "0A"

    #@14e2
    aput-object v3, v2, v7

    #@14e4
    aput-object v2, v0, v1

    #@14e6
    const/16 v1, 0xf3

    #@14e8
    new-array v2, v8, [Ljava/lang/String;

    #@14ea
    const-string v3, "ASDA"

    #@14ec
    aput-object v3, v2, v4

    #@14ee
    const-string v3, "UK"

    #@14f0
    aput-object v3, v2, v5

    #@14f2
    const-string v3, "23415"

    #@14f4
    aput-object v3, v2, v6

    #@14f6
    const-string v3, "A0"

    #@14f8
    aput-object v3, v2, v7

    #@14fa
    aput-object v2, v0, v1

    #@14fc
    const/16 v1, 0xf4

    #@14fe
    new-array v2, v8, [Ljava/lang/String;

    #@1500
    const-string v3, "VIR"

    #@1502
    aput-object v3, v2, v4

    #@1504
    const-string v3, "UK"

    #@1506
    aput-object v3, v2, v5

    #@1508
    const-string v3, "23430"

    #@150a
    aput-object v3, v2, v6

    #@150c
    const-string v3, "28"

    #@150e
    aput-object v3, v2, v7

    #@1510
    aput-object v2, v0, v1

    #@1512
    const/16 v1, 0xf5

    #@1514
    new-array v2, v8, [Ljava/lang/String;

    #@1516
    const-string v3, "ORG"

    #@1518
    aput-object v3, v2, v4

    #@151a
    const-string v3, "AM"

    #@151c
    aput-object v3, v2, v5

    #@151e
    const-string v3, "28310"

    #@1520
    aput-object v3, v2, v6

    #@1522
    const-string v3, ""

    #@1524
    aput-object v3, v2, v7

    #@1526
    aput-object v2, v0, v1

    #@1528
    const/16 v1, 0xf6

    #@152a
    new-array v2, v8, [Ljava/lang/String;

    #@152c
    const-string v3, "VEL"

    #@152e
    aput-object v3, v2, v4

    #@1530
    const-string v3, "BY"

    #@1532
    aput-object v3, v2, v5

    #@1534
    const-string v3, "25701"

    #@1536
    aput-object v3, v2, v6

    #@1538
    const-string v3, ""

    #@153a
    aput-object v3, v2, v7

    #@153c
    aput-object v2, v0, v1

    #@153e
    const/16 v1, 0xf7

    #@1540
    new-array v2, v8, [Ljava/lang/String;

    #@1542
    const-string v3, "MTC"

    #@1544
    aput-object v3, v2, v4

    #@1546
    const-string v3, "BY"

    #@1548
    aput-object v3, v2, v5

    #@154a
    const-string v3, "25702"

    #@154c
    aput-object v3, v2, v6

    #@154e
    const-string v3, ""

    #@1550
    aput-object v3, v2, v7

    #@1552
    aput-object v2, v0, v1

    #@1554
    const/16 v1, 0xf8

    #@1556
    new-array v2, v8, [Ljava/lang/String;

    #@1558
    const-string v3, "LIFE"

    #@155a
    aput-object v3, v2, v4

    #@155c
    const-string v3, "BY"

    #@155e
    aput-object v3, v2, v5

    #@1560
    const-string v3, "25704"

    #@1562
    aput-object v3, v2, v6

    #@1564
    const-string v3, ""

    #@1566
    aput-object v3, v2, v7

    #@1568
    aput-object v2, v0, v1

    #@156a
    const/16 v1, 0xf9

    #@156c
    new-array v2, v8, [Ljava/lang/String;

    #@156e
    const-string v3, "BEE"

    #@1570
    aput-object v3, v2, v4

    #@1572
    const-string v3, "KZ"

    #@1574
    aput-object v3, v2, v5

    #@1576
    const-string v3, "40101"

    #@1578
    aput-object v3, v2, v6

    #@157a
    const-string v3, ""

    #@157c
    aput-object v3, v2, v7

    #@157e
    aput-object v2, v0, v1

    #@1580
    const/16 v1, 0xfa

    #@1582
    new-array v2, v8, [Ljava/lang/String;

    #@1584
    const-string v3, "KCEL"

    #@1586
    aput-object v3, v2, v4

    #@1588
    const-string v3, "KZ"

    #@158a
    aput-object v3, v2, v5

    #@158c
    const-string v3, "40102"

    #@158e
    aput-object v3, v2, v6

    #@1590
    const-string v3, ""

    #@1592
    aput-object v3, v2, v7

    #@1594
    aput-object v2, v0, v1

    #@1596
    const/16 v1, 0xfb

    #@1598
    new-array v2, v8, [Ljava/lang/String;

    #@159a
    const-string v3, "NEO"

    #@159c
    aput-object v3, v2, v4

    #@159e
    const-string v3, "KZ"

    #@15a0
    aput-object v3, v2, v5

    #@15a2
    const-string v3, "40177"

    #@15a4
    aput-object v3, v2, v6

    #@15a6
    const-string v3, ""

    #@15a8
    aput-object v3, v2, v7

    #@15aa
    aput-object v2, v0, v1

    #@15ac
    const/16 v1, 0xfc

    #@15ae
    new-array v2, v8, [Ljava/lang/String;

    #@15b0
    const-string v3, "ORG"

    #@15b2
    aput-object v3, v2, v4

    #@15b4
    const-string v3, "MD"

    #@15b6
    aput-object v3, v2, v5

    #@15b8
    const-string v3, "25901"

    #@15ba
    aput-object v3, v2, v6

    #@15bc
    const-string v3, ""

    #@15be
    aput-object v3, v2, v7

    #@15c0
    aput-object v2, v0, v1

    #@15c2
    const/16 v1, 0xfd

    #@15c4
    new-array v2, v8, [Ljava/lang/String;

    #@15c6
    const-string v3, "MOLD"

    #@15c8
    aput-object v3, v2, v4

    #@15ca
    const-string v3, "MD"

    #@15cc
    aput-object v3, v2, v5

    #@15ce
    const-string v3, "25902"

    #@15d0
    aput-object v3, v2, v6

    #@15d2
    const-string v3, ""

    #@15d4
    aput-object v3, v2, v7

    #@15d6
    aput-object v2, v0, v1

    #@15d8
    const/16 v1, 0xfe

    #@15da
    new-array v2, v8, [Ljava/lang/String;

    #@15dc
    const-string v3, "EVE"

    #@15de
    aput-object v3, v2, v4

    #@15e0
    const-string v3, "MD"

    #@15e2
    aput-object v3, v2, v5

    #@15e4
    const-string v3, "25904"

    #@15e6
    aput-object v3, v2, v6

    #@15e8
    const-string v3, ""

    #@15ea
    aput-object v3, v2, v7

    #@15ec
    aput-object v2, v0, v1

    #@15ee
    const/16 v1, 0xff

    #@15f0
    new-array v2, v8, [Ljava/lang/String;

    #@15f2
    const-string v3, "UNT"

    #@15f4
    aput-object v3, v2, v4

    #@15f6
    const-string v3, "MD"

    #@15f8
    aput-object v3, v2, v5

    #@15fa
    const-string v3, "25905"

    #@15fc
    aput-object v3, v2, v6

    #@15fe
    const-string v3, ""

    #@1600
    aput-object v3, v2, v7

    #@1602
    aput-object v2, v0, v1

    #@1604
    const/16 v1, 0x100

    #@1606
    new-array v2, v8, [Ljava/lang/String;

    #@1608
    const-string v3, "MTS"

    #@160a
    aput-object v3, v2, v4

    #@160c
    const-string v3, "RU"

    #@160e
    aput-object v3, v2, v5

    #@1610
    const-string v3, "25001"

    #@1612
    aput-object v3, v2, v6

    #@1614
    const-string v3, ""

    #@1616
    aput-object v3, v2, v7

    #@1618
    aput-object v2, v0, v1

    #@161a
    const/16 v1, 0x101

    #@161c
    new-array v2, v8, [Ljava/lang/String;

    #@161e
    const-string v3, "MGF"

    #@1620
    aput-object v3, v2, v4

    #@1622
    const-string v3, "RU"

    #@1624
    aput-object v3, v2, v5

    #@1626
    const-string v3, "25002"

    #@1628
    aput-object v3, v2, v6

    #@162a
    const-string v3, ""

    #@162c
    aput-object v3, v2, v7

    #@162e
    aput-object v2, v0, v1

    #@1630
    const/16 v1, 0x102

    #@1632
    new-array v2, v8, [Ljava/lang/String;

    #@1634
    const-string v3, "TEL2"

    #@1636
    aput-object v3, v2, v4

    #@1638
    const-string v3, "RU"

    #@163a
    aput-object v3, v2, v5

    #@163c
    const-string v3, "25020"

    #@163e
    aput-object v3, v2, v6

    #@1640
    const-string v3, ""

    #@1642
    aput-object v3, v2, v7

    #@1644
    aput-object v2, v0, v1

    #@1646
    const/16 v1, 0x103

    #@1648
    new-array v2, v8, [Ljava/lang/String;

    #@164a
    const-string v3, "BEE"

    #@164c
    aput-object v3, v2, v4

    #@164e
    const-string v3, "RU"

    #@1650
    aput-object v3, v2, v5

    #@1652
    const-string v3, "25099"

    #@1654
    aput-object v3, v2, v6

    #@1656
    const-string v3, ""

    #@1658
    aput-object v3, v2, v7

    #@165a
    aput-object v2, v0, v1

    #@165c
    const/16 v1, 0x104

    #@165e
    new-array v2, v8, [Ljava/lang/String;

    #@1660
    const-string v3, "YOTA"

    #@1662
    aput-object v3, v2, v4

    #@1664
    const-string v3, "RU"

    #@1666
    aput-object v3, v2, v5

    #@1668
    const-string v3, "25011"

    #@166a
    aput-object v3, v2, v6

    #@166c
    const-string v3, ""

    #@166e
    aput-object v3, v2, v7

    #@1670
    aput-object v2, v0, v1

    #@1672
    const/16 v1, 0x105

    #@1674
    new-array v2, v8, [Ljava/lang/String;

    #@1676
    const-string v3, "YOTA"

    #@1678
    aput-object v3, v2, v4

    #@167a
    const-string v3, "RU"

    #@167c
    aput-object v3, v2, v5

    #@167e
    const-string v3, "25001"

    #@1680
    aput-object v3, v2, v6

    #@1682
    const-string v3, ""

    #@1684
    aput-object v3, v2, v7

    #@1686
    aput-object v2, v0, v1

    #@1688
    const/16 v1, 0x106

    #@168a
    new-array v2, v8, [Ljava/lang/String;

    #@168c
    const-string v3, "YOTA"

    #@168e
    aput-object v3, v2, v4

    #@1690
    const-string v3, "RU"

    #@1692
    aput-object v3, v2, v5

    #@1694
    const-string v3, "25002"

    #@1696
    aput-object v3, v2, v6

    #@1698
    const-string v3, ""

    #@169a
    aput-object v3, v2, v7

    #@169c
    aput-object v2, v0, v1

    #@169e
    const/16 v1, 0x107

    #@16a0
    new-array v2, v8, [Ljava/lang/String;

    #@16a2
    const-string v3, "YOTA"

    #@16a4
    aput-object v3, v2, v4

    #@16a6
    const-string v3, "RU"

    #@16a8
    aput-object v3, v2, v5

    #@16aa
    const-string v3, "25020"

    #@16ac
    aput-object v3, v2, v6

    #@16ae
    const-string v3, ""

    #@16b0
    aput-object v3, v2, v7

    #@16b2
    aput-object v2, v0, v1

    #@16b4
    const/16 v1, 0x108

    #@16b6
    new-array v2, v8, [Ljava/lang/String;

    #@16b8
    const-string v3, "YOTA"

    #@16ba
    aput-object v3, v2, v4

    #@16bc
    const-string v3, "RU"

    #@16be
    aput-object v3, v2, v5

    #@16c0
    const-string v3, "25099"

    #@16c2
    aput-object v3, v2, v6

    #@16c4
    const-string v3, ""

    #@16c6
    aput-object v3, v2, v7

    #@16c8
    aput-object v2, v0, v1

    #@16ca
    const/16 v1, 0x109

    #@16cc
    new-array v2, v8, [Ljava/lang/String;

    #@16ce
    const-string v3, "MTS"

    #@16d0
    aput-object v3, v2, v4

    #@16d2
    const-string v3, "UA"

    #@16d4
    aput-object v3, v2, v5

    #@16d6
    const-string v3, "25501"

    #@16d8
    aput-object v3, v2, v6

    #@16da
    const-string v3, ""

    #@16dc
    aput-object v3, v2, v7

    #@16de
    aput-object v2, v0, v1

    #@16e0
    const/16 v1, 0x10a

    #@16e2
    new-array v2, v8, [Ljava/lang/String;

    #@16e4
    const-string v3, "BEE"

    #@16e6
    aput-object v3, v2, v4

    #@16e8
    const-string v3, "UA"

    #@16ea
    aput-object v3, v2, v5

    #@16ec
    const-string v3, "25502"

    #@16ee
    aput-object v3, v2, v6

    #@16f0
    const-string v3, ""

    #@16f2
    aput-object v3, v2, v7

    #@16f4
    aput-object v2, v0, v1

    #@16f6
    const/16 v1, 0x10b

    #@16f8
    new-array v2, v8, [Ljava/lang/String;

    #@16fa
    const-string v3, "KYIV"

    #@16fc
    aput-object v3, v2, v4

    #@16fe
    const-string v3, "UA"

    #@1700
    aput-object v3, v2, v5

    #@1702
    const-string v3, "25503"

    #@1704
    aput-object v3, v2, v6

    #@1706
    const-string v3, ""

    #@1708
    aput-object v3, v2, v7

    #@170a
    aput-object v2, v0, v1

    #@170c
    const/16 v1, 0x10c

    #@170e
    new-array v2, v8, [Ljava/lang/String;

    #@1710
    const-string v3, "ACE"

    #@1712
    aput-object v3, v2, v4

    #@1714
    const-string v3, "UA"

    #@1716
    aput-object v3, v2, v5

    #@1718
    const-string v3, "25503"

    #@171a
    aput-object v3, v2, v6

    #@171c
    const-string v3, "Ace&Base"

    #@171e
    aput-object v3, v2, v7

    #@1720
    aput-object v2, v0, v1

    #@1722
    const/16 v1, 0x10d

    #@1724
    new-array v2, v8, [Ljava/lang/String;

    #@1726
    const-string v3, "DJU"

    #@1728
    aput-object v3, v2, v4

    #@172a
    const-string v3, "UA"

    #@172c
    aput-object v3, v2, v5

    #@172e
    const-string v3, "25503"

    #@1730
    aput-object v3, v2, v6

    #@1732
    const-string v3, "Djuice"

    #@1734
    aput-object v3, v2, v7

    #@1736
    aput-object v2, v0, v1

    #@1738
    const/16 v1, 0x10e

    #@173a
    new-array v2, v8, [Ljava/lang/String;

    #@173c
    const-string v3, "LIFE"

    #@173e
    aput-object v3, v2, v4

    #@1740
    const-string v3, "UA"

    #@1742
    aput-object v3, v2, v5

    #@1744
    const-string v3, "25506"

    #@1746
    aput-object v3, v2, v6

    #@1748
    const-string v3, ""

    #@174a
    aput-object v3, v2, v7

    #@174c
    aput-object v2, v0, v1

    #@174e
    const/16 v1, 0x10f

    #@1750
    new-array v2, v8, [Ljava/lang/String;

    #@1752
    const-string v3, "UTEL"

    #@1754
    aput-object v3, v2, v4

    #@1756
    const-string v3, "UA"

    #@1758
    aput-object v3, v2, v5

    #@175a
    const-string v3, "25507"

    #@175c
    aput-object v3, v2, v6

    #@175e
    const-string v3, ""

    #@1760
    aput-object v3, v2, v7

    #@1762
    aput-object v2, v0, v1

    #@1764
    const/16 v1, 0x110

    #@1766
    new-array v2, v8, [Ljava/lang/String;

    #@1768
    const-string v3, "BEE"

    #@176a
    aput-object v3, v2, v4

    #@176c
    const-string v3, "UZ"

    #@176e
    aput-object v3, v2, v5

    #@1770
    const-string v3, "43404"

    #@1772
    aput-object v3, v2, v6

    #@1774
    const-string v3, ""

    #@1776
    aput-object v3, v2, v7

    #@1778
    aput-object v2, v0, v1

    #@177a
    const/16 v1, 0x111

    #@177c
    new-array v2, v8, [Ljava/lang/String;

    #@177e
    const-string v3, "UCEL"

    #@1780
    aput-object v3, v2, v4

    #@1782
    const-string v3, "UZ"

    #@1784
    aput-object v3, v2, v5

    #@1786
    const-string v3, "43405"

    #@1788
    aput-object v3, v2, v6

    #@178a
    const-string v3, ""

    #@178c
    aput-object v3, v2, v7

    #@178e
    aput-object v2, v0, v1

    #@1790
    const/16 v1, 0x112

    #@1792
    new-array v2, v8, [Ljava/lang/String;

    #@1794
    const-string v3, "MTS"

    #@1796
    aput-object v3, v2, v4

    #@1798
    const-string v3, "UZ"

    #@179a
    aput-object v3, v2, v5

    #@179c
    const-string v3, "43407"

    #@179e
    aput-object v3, v2, v6

    #@17a0
    const-string v3, ""

    #@17a2
    aput-object v3, v2, v7

    #@17a4
    aput-object v2, v0, v1

    #@17a6
    const/16 v1, 0x113

    #@17a8
    new-array v2, v8, [Ljava/lang/String;

    #@17aa
    const-string v3, "MOBI"

    #@17ac
    aput-object v3, v2, v4

    #@17ae
    const-string v3, "DZ"

    #@17b0
    aput-object v3, v2, v5

    #@17b2
    const-string v3, "60301"

    #@17b4
    aput-object v3, v2, v6

    #@17b6
    const-string v3, ""

    #@17b8
    aput-object v3, v2, v7

    #@17ba
    aput-object v2, v0, v1

    #@17bc
    const/16 v1, 0x114

    #@17be
    new-array v2, v8, [Ljava/lang/String;

    #@17c0
    const-string v3, "DJE"

    #@17c2
    aput-object v3, v2, v4

    #@17c4
    const-string v3, "DZ"

    #@17c6
    aput-object v3, v2, v5

    #@17c8
    const-string v3, "60302"

    #@17ca
    aput-object v3, v2, v6

    #@17cc
    const-string v3, ""

    #@17ce
    aput-object v3, v2, v7

    #@17d0
    aput-object v2, v0, v1

    #@17d2
    const/16 v1, 0x115

    #@17d4
    new-array v2, v8, [Ljava/lang/String;

    #@17d6
    const-string v3, "NED"

    #@17d8
    aput-object v3, v2, v4

    #@17da
    const-string v3, "DZ"

    #@17dc
    aput-object v3, v2, v5

    #@17de
    const-string v3, "60303"

    #@17e0
    aput-object v3, v2, v6

    #@17e2
    const-string v3, ""

    #@17e4
    aput-object v3, v2, v7

    #@17e6
    aput-object v2, v0, v1

    #@17e8
    const/16 v1, 0x116

    #@17ea
    new-array v2, v8, [Ljava/lang/String;

    #@17ec
    const-string v3, "UNI"

    #@17ee
    aput-object v3, v2, v4

    #@17f0
    const-string v3, "AO"

    #@17f2
    aput-object v3, v2, v5

    #@17f4
    const-string v3, "63102"

    #@17f6
    aput-object v3, v2, v6

    #@17f8
    const-string v3, ""

    #@17fa
    aput-object v3, v2, v7

    #@17fc
    aput-object v2, v0, v1

    #@17fe
    const/16 v1, 0x117

    #@1800
    new-array v2, v8, [Ljava/lang/String;

    #@1802
    const-string v3, "MOVI"

    #@1804
    aput-object v3, v2, v4

    #@1806
    const-string v3, "AO"

    #@1808
    aput-object v3, v2, v5

    #@180a
    const-string v3, "63104"

    #@180c
    aput-object v3, v2, v6

    #@180e
    const-string v3, ""

    #@1810
    aput-object v3, v2, v7

    #@1812
    aput-object v2, v0, v1

    #@1814
    const/16 v1, 0x118

    #@1816
    new-array v2, v8, [Ljava/lang/String;

    #@1818
    const-string v3, "ORG"

    #@181a
    aput-object v3, v2, v4

    #@181c
    const-string v3, "BW"

    #@181e
    aput-object v3, v2, v5

    #@1820
    const-string v3, "65202"

    #@1822
    aput-object v3, v2, v6

    #@1824
    const-string v3, ""

    #@1826
    aput-object v3, v2, v7

    #@1828
    aput-object v2, v0, v1

    #@182a
    const/16 v1, 0x119

    #@182c
    new-array v2, v8, [Ljava/lang/String;

    #@182e
    const-string v3, "ORG"

    #@1830
    aput-object v3, v2, v4

    #@1832
    const-string v3, "CM"

    #@1834
    aput-object v3, v2, v5

    #@1836
    const-string v3, "62402"

    #@1838
    aput-object v3, v2, v6

    #@183a
    const-string v3, ""

    #@183c
    aput-object v3, v2, v7

    #@183e
    aput-object v2, v0, v1

    #@1840
    const/16 v1, 0x11a

    #@1842
    new-array v2, v8, [Ljava/lang/String;

    #@1844
    const-string v3, "ORG"

    #@1846
    aput-object v3, v2, v4

    #@1848
    const-string v3, "CF"

    #@184a
    aput-object v3, v2, v5

    #@184c
    const-string v3, "62303"

    #@184e
    aput-object v3, v2, v6

    #@1850
    const-string v3, ""

    #@1852
    aput-object v3, v2, v7

    #@1854
    aput-object v2, v0, v1

    #@1856
    const/16 v1, 0x11b

    #@1858
    new-array v2, v8, [Ljava/lang/String;

    #@185a
    const-string v3, "VDF"

    #@185c
    aput-object v3, v2, v4

    #@185e
    const-string v3, "CD"

    #@1860
    aput-object v3, v2, v5

    #@1862
    const-string v3, "63001"

    #@1864
    aput-object v3, v2, v6

    #@1866
    const-string v3, ""

    #@1868
    aput-object v3, v2, v7

    #@186a
    aput-object v2, v0, v1

    #@186c
    const/16 v1, 0x11c

    #@186e
    new-array v2, v8, [Ljava/lang/String;

    #@1870
    const-string v3, "AIR"

    #@1872
    aput-object v3, v2, v4

    #@1874
    const-string v3, "CD"

    #@1876
    aput-object v3, v2, v5

    #@1878
    const-string v3, "63002"

    #@187a
    aput-object v3, v2, v6

    #@187c
    const-string v3, ""

    #@187e
    aput-object v3, v2, v7

    #@1880
    aput-object v2, v0, v1

    #@1882
    const/16 v1, 0x11d

    #@1884
    new-array v2, v8, [Ljava/lang/String;

    #@1886
    const-string v3, "CCT"

    #@1888
    aput-object v3, v2, v4

    #@188a
    const-string v3, "CD"

    #@188c
    aput-object v3, v2, v5

    #@188e
    const-string v3, "63086"

    #@1890
    aput-object v3, v2, v6

    #@1892
    const-string v3, ""

    #@1894
    aput-object v3, v2, v7

    #@1896
    aput-object v2, v0, v1

    #@1898
    const/16 v1, 0x11e

    #@189a
    new-array v2, v8, [Ljava/lang/String;

    #@189c
    const-string v3, "SAIT"

    #@189e
    aput-object v3, v2, v4

    #@18a0
    const-string v3, "CD"

    #@18a2
    aput-object v3, v2, v5

    #@18a4
    const-string v3, "63089"

    #@18a6
    aput-object v3, v2, v6

    #@18a8
    const-string v3, ""

    #@18aa
    aput-object v3, v2, v7

    #@18ac
    aput-object v2, v0, v1

    #@18ae
    const/16 v1, 0x11f

    #@18b0
    new-array v2, v8, [Ljava/lang/String;

    #@18b2
    const-string v3, "ORG"

    #@18b4
    aput-object v3, v2, v4

    #@18b6
    const-string v3, "EG"

    #@18b8
    aput-object v3, v2, v5

    #@18ba
    const-string v3, "60201"

    #@18bc
    aput-object v3, v2, v6

    #@18be
    const-string v3, ""

    #@18c0
    aput-object v3, v2, v7

    #@18c2
    aput-object v2, v0, v1

    #@18c4
    const/16 v1, 0x120

    #@18c6
    new-array v2, v8, [Ljava/lang/String;

    #@18c8
    const-string v3, "VDF"

    #@18ca
    aput-object v3, v2, v4

    #@18cc
    const-string v3, "EG"

    #@18ce
    aput-object v3, v2, v5

    #@18d0
    const-string v3, "60202"

    #@18d2
    aput-object v3, v2, v6

    #@18d4
    const-string v3, ""

    #@18d6
    aput-object v3, v2, v7

    #@18d8
    aput-object v2, v0, v1

    #@18da
    const/16 v1, 0x121

    #@18dc
    new-array v2, v8, [Ljava/lang/String;

    #@18de
    const-string v3, "ETSL"

    #@18e0
    aput-object v3, v2, v4

    #@18e2
    const-string v3, "EG"

    #@18e4
    aput-object v3, v2, v5

    #@18e6
    const-string v3, "60203"

    #@18e8
    aput-object v3, v2, v6

    #@18ea
    const-string v3, ""

    #@18ec
    aput-object v3, v2, v7

    #@18ee
    aput-object v2, v0, v1

    #@18f0
    const/16 v1, 0x122

    #@18f2
    new-array v2, v8, [Ljava/lang/String;

    #@18f4
    const-string v3, "ORG"

    #@18f6
    aput-object v3, v2, v4

    #@18f8
    const-string v3, "GQ"

    #@18fa
    aput-object v3, v2, v5

    #@18fc
    const-string v3, "62701"

    #@18fe
    aput-object v3, v2, v6

    #@1900
    const-string v3, ""

    #@1902
    aput-object v3, v2, v7

    #@1904
    aput-object v2, v0, v1

    #@1906
    const/16 v1, 0x123

    #@1908
    new-array v2, v8, [Ljava/lang/String;

    #@190a
    const-string v3, "MTN"

    #@190c
    aput-object v3, v2, v4

    #@190e
    const-string v3, "GH"

    #@1910
    aput-object v3, v2, v5

    #@1912
    const-string v3, "620001"

    #@1914
    aput-object v3, v2, v6

    #@1916
    const-string v3, ""

    #@1918
    aput-object v3, v2, v7

    #@191a
    aput-object v2, v0, v1

    #@191c
    const/16 v1, 0x124

    #@191e
    new-array v2, v8, [Ljava/lang/String;

    #@1920
    const-string v3, "VDF"

    #@1922
    aput-object v3, v2, v4

    #@1924
    const-string v3, "GH"

    #@1926
    aput-object v3, v2, v5

    #@1928
    const-string v3, "620002"

    #@192a
    aput-object v3, v2, v6

    #@192c
    const-string v3, ""

    #@192e
    aput-object v3, v2, v7

    #@1930
    aput-object v2, v0, v1

    #@1932
    const/16 v1, 0x125

    #@1934
    new-array v2, v8, [Ljava/lang/String;

    #@1936
    const-string v3, "TIGO"

    #@1938
    aput-object v3, v2, v4

    #@193a
    const-string v3, "GH"

    #@193c
    aput-object v3, v2, v5

    #@193e
    const-string v3, "620003"

    #@1940
    aput-object v3, v2, v6

    #@1942
    const-string v3, ""

    #@1944
    aput-object v3, v2, v7

    #@1946
    aput-object v2, v0, v1

    #@1948
    const/16 v1, 0x126

    #@194a
    new-array v2, v8, [Ljava/lang/String;

    #@194c
    const-string v3, "AIR"

    #@194e
    aput-object v3, v2, v4

    #@1950
    const-string v3, "GH"

    #@1952
    aput-object v3, v2, v5

    #@1954
    const-string v3, "620006"

    #@1956
    aput-object v3, v2, v6

    #@1958
    const-string v3, ""

    #@195a
    aput-object v3, v2, v7

    #@195c
    aput-object v2, v0, v1

    #@195e
    const/16 v1, 0x127

    #@1960
    new-array v2, v8, [Ljava/lang/String;

    #@1962
    const-string v3, "ORG"

    #@1964
    aput-object v3, v2, v4

    #@1966
    const-string v3, "GN"

    #@1968
    aput-object v3, v2, v5

    #@196a
    const-string v3, "61101"

    #@196c
    aput-object v3, v2, v6

    #@196e
    const-string v3, ""

    #@1970
    aput-object v3, v2, v7

    #@1972
    aput-object v2, v0, v1

    #@1974
    const/16 v1, 0x128

    #@1976
    new-array v2, v8, [Ljava/lang/String;

    #@1978
    const-string v3, "ORG"

    #@197a
    aput-object v3, v2, v4

    #@197c
    const-string v3, "GW"

    #@197e
    aput-object v3, v2, v5

    #@1980
    const-string v3, "63203"

    #@1982
    aput-object v3, v2, v6

    #@1984
    const-string v3, ""

    #@1986
    aput-object v3, v2, v7

    #@1988
    aput-object v2, v0, v1

    #@198a
    const/16 v1, 0x129

    #@198c
    new-array v2, v8, [Ljava/lang/String;

    #@198e
    const-string v3, "MOOV"

    #@1990
    aput-object v3, v2, v4

    #@1992
    const-string v3, "CI"

    #@1994
    aput-object v3, v2, v5

    #@1996
    const-string v3, "61202"

    #@1998
    aput-object v3, v2, v6

    #@199a
    const-string v3, ""

    #@199c
    aput-object v3, v2, v7

    #@199e
    aput-object v2, v0, v1

    #@19a0
    const/16 v1, 0x12a

    #@19a2
    new-array v2, v8, [Ljava/lang/String;

    #@19a4
    const-string v3, "ORG"

    #@19a6
    aput-object v3, v2, v4

    #@19a8
    const-string v3, "CI"

    #@19aa
    aput-object v3, v2, v5

    #@19ac
    const-string v3, "61203"

    #@19ae
    aput-object v3, v2, v6

    #@19b0
    const-string v3, ""

    #@19b2
    aput-object v3, v2, v7

    #@19b4
    aput-object v2, v0, v1

    #@19b6
    const/16 v1, 0x12b

    #@19b8
    new-array v2, v8, [Ljava/lang/String;

    #@19ba
    const-string v3, "KOZ"

    #@19bc
    aput-object v3, v2, v4

    #@19be
    const-string v3, "CI"

    #@19c0
    aput-object v3, v2, v5

    #@19c2
    const-string v3, "61204"

    #@19c4
    aput-object v3, v2, v6

    #@19c6
    const-string v3, ""

    #@19c8
    aput-object v3, v2, v7

    #@19ca
    aput-object v2, v0, v1

    #@19cc
    const/16 v1, 0x12c

    #@19ce
    new-array v2, v8, [Ljava/lang/String;

    #@19d0
    const-string v3, "MTN"

    #@19d2
    aput-object v3, v2, v4

    #@19d4
    const-string v3, "CI"

    #@19d6
    aput-object v3, v2, v5

    #@19d8
    const-string v3, "61205"

    #@19da
    aput-object v3, v2, v6

    #@19dc
    const-string v3, ""

    #@19de
    aput-object v3, v2, v7

    #@19e0
    aput-object v2, v0, v1

    #@19e2
    const/16 v1, 0x12d

    #@19e4
    new-array v2, v8, [Ljava/lang/String;

    #@19e6
    const-string v3, "SAFA"

    #@19e8
    aput-object v3, v2, v4

    #@19ea
    const-string v3, "KE"

    #@19ec
    aput-object v3, v2, v5

    #@19ee
    const-string v3, "63902"

    #@19f0
    aput-object v3, v2, v6

    #@19f2
    const-string v3, ""

    #@19f4
    aput-object v3, v2, v7

    #@19f6
    aput-object v2, v0, v1

    #@19f8
    const/16 v1, 0x12e

    #@19fa
    new-array v2, v8, [Ljava/lang/String;

    #@19fc
    const-string v3, "SAFA"

    #@19fe
    aput-object v3, v2, v4

    #@1a00
    const-string v3, "KE"

    #@1a02
    aput-object v3, v2, v5

    #@1a04
    const-string v3, "6392"

    #@1a06
    aput-object v3, v2, v6

    #@1a08
    const-string v3, ""

    #@1a0a
    aput-object v3, v2, v7

    #@1a0c
    aput-object v2, v0, v1

    #@1a0e
    const/16 v1, 0x12f

    #@1a10
    new-array v2, v8, [Ljava/lang/String;

    #@1a12
    const-string v3, "AIR"

    #@1a14
    aput-object v3, v2, v4

    #@1a16
    const-string v3, "KE"

    #@1a18
    aput-object v3, v2, v5

    #@1a1a
    const-string v3, "63903"

    #@1a1c
    aput-object v3, v2, v6

    #@1a1e
    const-string v3, ""

    #@1a20
    aput-object v3, v2, v7

    #@1a22
    aput-object v2, v0, v1

    #@1a24
    const/16 v1, 0x130

    #@1a26
    new-array v2, v8, [Ljava/lang/String;

    #@1a28
    const-string v3, "AIR"

    #@1a2a
    aput-object v3, v2, v4

    #@1a2c
    const-string v3, "KE"

    #@1a2e
    aput-object v3, v2, v5

    #@1a30
    const-string v3, "6393"

    #@1a32
    aput-object v3, v2, v6

    #@1a34
    const-string v3, ""

    #@1a36
    aput-object v3, v2, v7

    #@1a38
    aput-object v2, v0, v1

    #@1a3a
    const/16 v1, 0x131

    #@1a3c
    new-array v2, v8, [Ljava/lang/String;

    #@1a3e
    const-string v3, "YU"

    #@1a40
    aput-object v3, v2, v4

    #@1a42
    const-string v3, "KE"

    #@1a44
    aput-object v3, v2, v5

    #@1a46
    const-string v3, "63905"

    #@1a48
    aput-object v3, v2, v6

    #@1a4a
    const-string v3, ""

    #@1a4c
    aput-object v3, v2, v7

    #@1a4e
    aput-object v2, v0, v1

    #@1a50
    const/16 v1, 0x132

    #@1a52
    new-array v2, v8, [Ljava/lang/String;

    #@1a54
    const-string v3, "YU"

    #@1a56
    aput-object v3, v2, v4

    #@1a58
    const-string v3, "KE"

    #@1a5a
    aput-object v3, v2, v5

    #@1a5c
    const-string v3, "6395"

    #@1a5e
    aput-object v3, v2, v6

    #@1a60
    const-string v3, ""

    #@1a62
    aput-object v3, v2, v7

    #@1a64
    aput-object v2, v0, v1

    #@1a66
    const/16 v1, 0x133

    #@1a68
    new-array v2, v8, [Ljava/lang/String;

    #@1a6a
    const-string v3, "ORG"

    #@1a6c
    aput-object v3, v2, v4

    #@1a6e
    const-string v3, "KE"

    #@1a70
    aput-object v3, v2, v5

    #@1a72
    const-string v3, "63907"

    #@1a74
    aput-object v3, v2, v6

    #@1a76
    const-string v3, ""

    #@1a78
    aput-object v3, v2, v7

    #@1a7a
    aput-object v2, v0, v1

    #@1a7c
    const/16 v1, 0x134

    #@1a7e
    new-array v2, v8, [Ljava/lang/String;

    #@1a80
    const-string v3, "ORG"

    #@1a82
    aput-object v3, v2, v4

    #@1a84
    const-string v3, "KE"

    #@1a86
    aput-object v3, v2, v5

    #@1a88
    const-string v3, "6397"

    #@1a8a
    aput-object v3, v2, v6

    #@1a8c
    const-string v3, ""

    #@1a8e
    aput-object v3, v2, v7

    #@1a90
    aput-object v2, v0, v1

    #@1a92
    const/16 v1, 0x135

    #@1a94
    new-array v2, v8, [Ljava/lang/String;

    #@1a96
    const-string v3, "VDF"

    #@1a98
    aput-object v3, v2, v4

    #@1a9a
    const-string v3, "LS"

    #@1a9c
    aput-object v3, v2, v5

    #@1a9e
    const-string v3, "65101"

    #@1aa0
    aput-object v3, v2, v6

    #@1aa2
    const-string v3, ""

    #@1aa4
    aput-object v3, v2, v7

    #@1aa6
    aput-object v2, v0, v1

    #@1aa8
    const/16 v1, 0x136

    #@1aaa
    new-array v2, v8, [Ljava/lang/String;

    #@1aac
    const-string v3, "ECO"

    #@1aae
    aput-object v3, v2, v4

    #@1ab0
    const-string v3, "LS"

    #@1ab2
    aput-object v3, v2, v5

    #@1ab4
    const-string v3, "65101"

    #@1ab6
    aput-object v3, v2, v6

    #@1ab8
    const-string v3, ""

    #@1aba
    aput-object v3, v2, v7

    #@1abc
    aput-object v2, v0, v1

    #@1abe
    const/16 v1, 0x137

    #@1ac0
    new-array v2, v8, [Ljava/lang/String;

    #@1ac2
    const-string v3, "LIB"

    #@1ac4
    aput-object v3, v2, v4

    #@1ac6
    const-string v3, "LY"

    #@1ac8
    aput-object v3, v2, v5

    #@1aca
    const-string v3, "60600"

    #@1acc
    aput-object v3, v2, v6

    #@1ace
    const-string v3, ""

    #@1ad0
    aput-object v3, v2, v7

    #@1ad2
    aput-object v2, v0, v1

    #@1ad4
    const/16 v1, 0x138

    #@1ad6
    new-array v2, v8, [Ljava/lang/String;

    #@1ad8
    const-string v3, "MAD"

    #@1ada
    aput-object v3, v2, v4

    #@1adc
    const-string v3, "LY"

    #@1ade
    aput-object v3, v2, v5

    #@1ae0
    const-string v3, "60601"

    #@1ae2
    aput-object v3, v2, v6

    #@1ae4
    const-string v3, ""

    #@1ae6
    aput-object v3, v2, v7

    #@1ae8
    aput-object v2, v0, v1

    #@1aea
    const/16 v1, 0x139

    #@1aec
    new-array v2, v8, [Ljava/lang/String;

    #@1aee
    const-string v3, "ORG"

    #@1af0
    aput-object v3, v2, v4

    #@1af2
    const-string v3, "MG"

    #@1af4
    aput-object v3, v2, v5

    #@1af6
    const-string v3, "64602"

    #@1af8
    aput-object v3, v2, v6

    #@1afa
    const-string v3, ""

    #@1afc
    aput-object v3, v2, v7

    #@1afe
    aput-object v2, v0, v1

    #@1b00
    const/16 v1, 0x13a

    #@1b02
    new-array v2, v8, [Ljava/lang/String;

    #@1b04
    const-string v3, "ORG"

    #@1b06
    aput-object v3, v2, v4

    #@1b08
    const-string v3, "ML"

    #@1b0a
    aput-object v3, v2, v5

    #@1b0c
    const-string v3, "61002"

    #@1b0e
    aput-object v3, v2, v6

    #@1b10
    const-string v3, ""

    #@1b12
    aput-object v3, v2, v7

    #@1b14
    aput-object v2, v0, v1

    #@1b16
    const/16 v1, 0x13b

    #@1b18
    new-array v2, v8, [Ljava/lang/String;

    #@1b1a
    const-string v3, "ORG"

    #@1b1c
    aput-object v3, v2, v4

    #@1b1e
    const-string v3, "MU"

    #@1b20
    aput-object v3, v2, v5

    #@1b22
    const-string v3, "61701"

    #@1b24
    aput-object v3, v2, v6

    #@1b26
    const-string v3, ""

    #@1b28
    aput-object v3, v2, v7

    #@1b2a
    aput-object v2, v0, v1

    #@1b2c
    const/16 v1, 0x13c

    #@1b2e
    new-array v2, v8, [Ljava/lang/String;

    #@1b30
    const-string v3, "IAM"

    #@1b32
    aput-object v3, v2, v4

    #@1b34
    const-string v3, "MA"

    #@1b36
    aput-object v3, v2, v5

    #@1b38
    const-string v3, "60401"

    #@1b3a
    aput-object v3, v2, v6

    #@1b3c
    const-string v3, ""

    #@1b3e
    aput-object v3, v2, v7

    #@1b40
    aput-object v2, v0, v1

    #@1b42
    const/16 v1, 0x13d

    #@1b44
    new-array v2, v8, [Ljava/lang/String;

    #@1b46
    const-string v3, "ORG"

    #@1b48
    aput-object v3, v2, v4

    #@1b4a
    const-string v3, "MA"

    #@1b4c
    aput-object v3, v2, v5

    #@1b4e
    const-string v3, "60400"

    #@1b50
    aput-object v3, v2, v6

    #@1b52
    const-string v3, ""

    #@1b54
    aput-object v3, v2, v7

    #@1b56
    aput-object v2, v0, v1

    #@1b58
    const/16 v1, 0x13e

    #@1b5a
    new-array v2, v8, [Ljava/lang/String;

    #@1b5c
    const-string v3, "WANA"

    #@1b5e
    aput-object v3, v2, v4

    #@1b60
    const-string v3, "MA"

    #@1b62
    aput-object v3, v2, v5

    #@1b64
    const-string v3, "60402"

    #@1b66
    aput-object v3, v2, v6

    #@1b68
    const-string v3, ""

    #@1b6a
    aput-object v3, v2, v7

    #@1b6c
    aput-object v2, v0, v1

    #@1b6e
    const/16 v1, 0x13f

    #@1b70
    new-array v2, v8, [Ljava/lang/String;

    #@1b72
    const-string v3, "ORG"

    #@1b74
    aput-object v3, v2, v4

    #@1b76
    const-string v3, "NI"

    #@1b78
    aput-object v3, v2, v5

    #@1b7a
    const-string v3, "61404"

    #@1b7c
    aput-object v3, v2, v6

    #@1b7e
    const-string v3, ""

    #@1b80
    aput-object v3, v2, v7

    #@1b82
    aput-object v2, v0, v1

    #@1b84
    const/16 v1, 0x140

    #@1b86
    new-array v2, v8, [Ljava/lang/String;

    #@1b88
    const-string v3, "AIR"

    #@1b8a
    aput-object v3, v2, v4

    #@1b8c
    const-string v3, "NG"

    #@1b8e
    aput-object v3, v2, v5

    #@1b90
    const-string v3, "62120"

    #@1b92
    aput-object v3, v2, v6

    #@1b94
    const-string v3, ""

    #@1b96
    aput-object v3, v2, v7

    #@1b98
    aput-object v2, v0, v1

    #@1b9a
    const/16 v1, 0x141

    #@1b9c
    new-array v2, v8, [Ljava/lang/String;

    #@1b9e
    const-string v3, "MTN"

    #@1ba0
    aput-object v3, v2, v4

    #@1ba2
    const-string v3, "NG"

    #@1ba4
    aput-object v3, v2, v5

    #@1ba6
    const-string v3, "62130"

    #@1ba8
    aput-object v3, v2, v6

    #@1baa
    const-string v3, ""

    #@1bac
    aput-object v3, v2, v7

    #@1bae
    aput-object v2, v0, v1

    #@1bb0
    const/16 v1, 0x142

    #@1bb2
    new-array v2, v8, [Ljava/lang/String;

    #@1bb4
    const-string v3, "GLO"

    #@1bb6
    aput-object v3, v2, v4

    #@1bb8
    const-string v3, "NG"

    #@1bba
    aput-object v3, v2, v5

    #@1bbc
    const-string v3, "62150"

    #@1bbe
    aput-object v3, v2, v6

    #@1bc0
    const-string v3, ""

    #@1bc2
    aput-object v3, v2, v7

    #@1bc4
    aput-object v2, v0, v1

    #@1bc6
    const/16 v1, 0x143

    #@1bc8
    new-array v2, v8, [Ljava/lang/String;

    #@1bca
    const-string v3, "ETI"

    #@1bcc
    aput-object v3, v2, v4

    #@1bce
    const-string v3, "NG"

    #@1bd0
    aput-object v3, v2, v5

    #@1bd2
    const-string v3, "62160"

    #@1bd4
    aput-object v3, v2, v6

    #@1bd6
    const-string v3, ""

    #@1bd8
    aput-object v3, v2, v7

    #@1bda
    aput-object v2, v0, v1

    #@1bdc
    const/16 v1, 0x144

    #@1bde
    new-array v2, v8, [Ljava/lang/String;

    #@1be0
    const-string v3, "ORG"

    #@1be2
    aput-object v3, v2, v4

    #@1be4
    const-string v3, "SN"

    #@1be6
    aput-object v3, v2, v5

    #@1be8
    const-string v3, "60801"

    #@1bea
    aput-object v3, v2, v6

    #@1bec
    const-string v3, ""

    #@1bee
    aput-object v3, v2, v7

    #@1bf0
    aput-object v2, v0, v1

    #@1bf2
    const/16 v1, 0x145

    #@1bf4
    new-array v2, v8, [Ljava/lang/String;

    #@1bf6
    const-string v3, "TIGO"

    #@1bf8
    aput-object v3, v2, v4

    #@1bfa
    const-string v3, "SN"

    #@1bfc
    aput-object v3, v2, v5

    #@1bfe
    const-string v3, "60802"

    #@1c00
    aput-object v3, v2, v6

    #@1c02
    const-string v3, ""

    #@1c04
    aput-object v3, v2, v7

    #@1c06
    aput-object v2, v0, v1

    #@1c08
    const/16 v1, 0x146

    #@1c0a
    new-array v2, v8, [Ljava/lang/String;

    #@1c0c
    const-string v3, "EXP"

    #@1c0e
    aput-object v3, v2, v4

    #@1c10
    const-string v3, "SN"

    #@1c12
    aput-object v3, v2, v5

    #@1c14
    const-string v3, "60803"

    #@1c16
    aput-object v3, v2, v6

    #@1c18
    const-string v3, ""

    #@1c1a
    aput-object v3, v2, v7

    #@1c1c
    aput-object v2, v0, v1

    #@1c1e
    const/16 v1, 0x147

    #@1c20
    new-array v2, v8, [Ljava/lang/String;

    #@1c22
    const-string v3, "VDF"

    #@1c24
    aput-object v3, v2, v4

    #@1c26
    const-string v3, "ZA"

    #@1c28
    aput-object v3, v2, v5

    #@1c2a
    const-string v3, "65501"

    #@1c2c
    aput-object v3, v2, v6

    #@1c2e
    const-string v3, ""

    #@1c30
    aput-object v3, v2, v7

    #@1c32
    aput-object v2, v0, v1

    #@1c34
    const/16 v1, 0x148

    #@1c36
    new-array v2, v8, [Ljava/lang/String;

    #@1c38
    const-string v3, "CELL"

    #@1c3a
    aput-object v3, v2, v4

    #@1c3c
    const-string v3, "ZA"

    #@1c3e
    aput-object v3, v2, v5

    #@1c40
    const-string v3, "65507"

    #@1c42
    aput-object v3, v2, v6

    #@1c44
    const-string v3, ""

    #@1c46
    aput-object v3, v2, v7

    #@1c48
    aput-object v2, v0, v1

    #@1c4a
    const/16 v1, 0x149

    #@1c4c
    new-array v2, v8, [Ljava/lang/String;

    #@1c4e
    const-string v3, "VIR"

    #@1c50
    aput-object v3, v2, v4

    #@1c52
    const-string v3, "ZA"

    #@1c54
    aput-object v3, v2, v5

    #@1c56
    const-string v3, "65507"

    #@1c58
    aput-object v3, v2, v6

    #@1c5a
    const-string v3, "6550710"

    #@1c5c
    aput-object v3, v2, v7

    #@1c5e
    aput-object v2, v0, v1

    #@1c60
    const/16 v1, 0x14a

    #@1c62
    new-array v2, v8, [Ljava/lang/String;

    #@1c64
    const-string v3, "RED"

    #@1c66
    aput-object v3, v2, v4

    #@1c68
    const-string v3, "ZA"

    #@1c6a
    aput-object v3, v2, v5

    #@1c6c
    const-string v3, "65507"

    #@1c6e
    aput-object v3, v2, v6

    #@1c70
    const-string v3, "6550713"

    #@1c72
    aput-object v3, v2, v7

    #@1c74
    aput-object v2, v0, v1

    #@1c76
    const/16 v1, 0x14b

    #@1c78
    new-array v2, v8, [Ljava/lang/String;

    #@1c7a
    const-string v3, "MTN"

    #@1c7c
    aput-object v3, v2, v4

    #@1c7e
    const-string v3, "ZA"

    #@1c80
    aput-object v3, v2, v5

    #@1c82
    const-string v3, "65510"

    #@1c84
    aput-object v3, v2, v6

    #@1c86
    const-string v3, ""

    #@1c88
    aput-object v3, v2, v7

    #@1c8a
    aput-object v2, v0, v1

    #@1c8c
    const/16 v1, 0x14c

    #@1c8e
    new-array v2, v8, [Ljava/lang/String;

    #@1c90
    const-string v3, "ORG"

    #@1c92
    aput-object v3, v2, v4

    #@1c94
    const-string v3, "UG"

    #@1c96
    aput-object v3, v2, v5

    #@1c98
    const-string v3, "64114"

    #@1c9a
    aput-object v3, v2, v6

    #@1c9c
    const-string v3, ""

    #@1c9e
    aput-object v3, v2, v7

    #@1ca0
    aput-object v2, v0, v1

    #@1ca2
    const/16 v1, 0x14d

    #@1ca4
    new-array v2, v8, [Ljava/lang/String;

    #@1ca6
    const-string v3, "ORG"

    #@1ca8
    aput-object v3, v2, v4

    #@1caa
    const-string v3, "TN"

    #@1cac
    aput-object v3, v2, v5

    #@1cae
    const-string v3, "60501"

    #@1cb0
    aput-object v3, v2, v6

    #@1cb2
    const-string v3, ""

    #@1cb4
    aput-object v3, v2, v7

    #@1cb6
    aput-object v2, v0, v1

    #@1cb8
    const/16 v1, 0x14e

    #@1cba
    new-array v2, v8, [Ljava/lang/String;

    #@1cbc
    const-string v3, "TUNI"

    #@1cbe
    aput-object v3, v2, v4

    #@1cc0
    const-string v3, "TN"

    #@1cc2
    aput-object v3, v2, v5

    #@1cc4
    const-string v3, "60502"

    #@1cc6
    aput-object v3, v2, v6

    #@1cc8
    const-string v3, ""

    #@1cca
    aput-object v3, v2, v7

    #@1ccc
    aput-object v2, v0, v1

    #@1cce
    const/16 v1, 0x14f

    #@1cd0
    new-array v2, v8, [Ljava/lang/String;

    #@1cd2
    const-string v3, "TUNS"

    #@1cd4
    aput-object v3, v2, v4

    #@1cd6
    const-string v3, "TN"

    #@1cd8
    aput-object v3, v2, v5

    #@1cda
    const-string v3, "60503"

    #@1cdc
    aput-object v3, v2, v6

    #@1cde
    const-string v3, ""

    #@1ce0
    aput-object v3, v2, v7

    #@1ce2
    aput-object v2, v0, v1

    #@1ce4
    const/16 v1, 0x150

    #@1ce6
    new-array v2, v8, [Ljava/lang/String;

    #@1ce8
    const-string v3, "SKT"

    #@1cea
    aput-object v3, v2, v4

    #@1cec
    const-string v3, "KR"

    #@1cee
    aput-object v3, v2, v5

    #@1cf0
    const-string v3, "45005"

    #@1cf2
    aput-object v3, v2, v6

    #@1cf4
    const-string v3, ""

    #@1cf6
    aput-object v3, v2, v7

    #@1cf8
    aput-object v2, v0, v1

    #@1cfa
    const/16 v1, 0x151

    #@1cfc
    new-array v2, v8, [Ljava/lang/String;

    #@1cfe
    const-string v3, "KT"

    #@1d00
    aput-object v3, v2, v4

    #@1d02
    const-string v3, "KR"

    #@1d04
    aput-object v3, v2, v5

    #@1d06
    const-string v3, "45008"

    #@1d08
    aput-object v3, v2, v6

    #@1d0a
    const-string v3, ""

    #@1d0c
    aput-object v3, v2, v7

    #@1d0e
    aput-object v2, v0, v1

    #@1d10
    const/16 v1, 0x152

    #@1d12
    new-array v2, v8, [Ljava/lang/String;

    #@1d14
    const-string v3, "TEL"

    #@1d16
    aput-object v3, v2, v4

    #@1d18
    const-string v3, "AU"

    #@1d1a
    aput-object v3, v2, v5

    #@1d1c
    const-string v3, "50501"

    #@1d1e
    aput-object v3, v2, v6

    #@1d20
    const-string v3, ""

    #@1d22
    aput-object v3, v2, v7

    #@1d24
    aput-object v2, v0, v1

    #@1d26
    const/16 v1, 0x153

    #@1d28
    new-array v2, v8, [Ljava/lang/String;

    #@1d2a
    const-string v3, "OPT"

    #@1d2c
    aput-object v3, v2, v4

    #@1d2e
    const-string v3, "AU"

    #@1d30
    aput-object v3, v2, v5

    #@1d32
    const-string v3, "50502"

    #@1d34
    aput-object v3, v2, v6

    #@1d36
    const-string v3, ""

    #@1d38
    aput-object v3, v2, v7

    #@1d3a
    aput-object v2, v0, v1

    #@1d3c
    const/16 v1, 0x154

    #@1d3e
    new-array v2, v8, [Ljava/lang/String;

    #@1d40
    const-string v3, "OPT"

    #@1d42
    aput-object v3, v2, v4

    #@1d44
    const-string v3, "AU"

    #@1d46
    aput-object v3, v2, v5

    #@1d48
    const-string v3, "50502"

    #@1d4a
    aput-object v3, v2, v6

    #@1d4c
    const-string v3, "505029"

    #@1d4e
    aput-object v3, v2, v7

    #@1d50
    aput-object v2, v0, v1

    #@1d52
    const/16 v1, 0x155

    #@1d54
    new-array v2, v8, [Ljava/lang/String;

    #@1d56
    const-string v3, "VIR"

    #@1d58
    aput-object v3, v2, v4

    #@1d5a
    const-string v3, "AU"

    #@1d5c
    aput-object v3, v2, v5

    #@1d5e
    const-string v3, "50502"

    #@1d60
    aput-object v3, v2, v6

    #@1d62
    const-string v3, "505029"

    #@1d64
    aput-object v3, v2, v7

    #@1d66
    aput-object v2, v0, v1

    #@1d68
    const/16 v1, 0x156

    #@1d6a
    new-array v2, v8, [Ljava/lang/String;

    #@1d6c
    const-string v3, "VDF"

    #@1d6e
    aput-object v3, v2, v4

    #@1d70
    const-string v3, "AU"

    #@1d72
    aput-object v3, v2, v5

    #@1d74
    const-string v3, "50503"

    #@1d76
    aput-object v3, v2, v6

    #@1d78
    const-string v3, ""

    #@1d7a
    aput-object v3, v2, v7

    #@1d7c
    aput-object v2, v0, v1

    #@1d7e
    const/16 v1, 0x157

    #@1d80
    new-array v2, v8, [Ljava/lang/String;

    #@1d82
    const-string v3, "CRZ"

    #@1d84
    aput-object v3, v2, v4

    #@1d86
    const-string v3, "AU"

    #@1d88
    aput-object v3, v2, v5

    #@1d8a
    const-string v3, "50503"

    #@1d8c
    aput-object v3, v2, v6

    #@1d8e
    const-string v3, "50503823"

    #@1d90
    aput-object v3, v2, v7

    #@1d92
    aput-object v2, v0, v1

    #@1d94
    const/16 v1, 0x158

    #@1d96
    new-array v2, v8, [Ljava/lang/String;

    #@1d98
    const-string v3, "BAT"

    #@1d9a
    aput-object v3, v2, v4

    #@1d9c
    const-string v3, "BH"

    #@1d9e
    aput-object v3, v2, v5

    #@1da0
    const-string v3, "42601"

    #@1da2
    aput-object v3, v2, v6

    #@1da4
    const-string v3, ""

    #@1da6
    aput-object v3, v2, v7

    #@1da8
    aput-object v2, v0, v1

    #@1daa
    const/16 v1, 0x159

    #@1dac
    new-array v2, v8, [Ljava/lang/String;

    #@1dae
    const-string v3, "ZAIN"

    #@1db0
    aput-object v3, v2, v4

    #@1db2
    const-string v3, "BH"

    #@1db4
    aput-object v3, v2, v5

    #@1db6
    const-string v3, "42602"

    #@1db8
    aput-object v3, v2, v6

    #@1dba
    const-string v3, ""

    #@1dbc
    aput-object v3, v2, v7

    #@1dbe
    aput-object v2, v0, v1

    #@1dc0
    const/16 v1, 0x15a

    #@1dc2
    new-array v2, v8, [Ljava/lang/String;

    #@1dc4
    const-string v3, "VIVA"

    #@1dc6
    aput-object v3, v2, v4

    #@1dc8
    const-string v3, "BH"

    #@1dca
    aput-object v3, v2, v5

    #@1dcc
    const-string v3, "42604"

    #@1dce
    aput-object v3, v2, v6

    #@1dd0
    const-string v3, ""

    #@1dd2
    aput-object v3, v2, v7

    #@1dd4
    aput-object v2, v0, v1

    #@1dd6
    const/16 v1, 0x15b

    #@1dd8
    new-array v2, v8, [Ljava/lang/String;

    #@1dda
    const-string v3, "CMN"

    #@1ddc
    aput-object v3, v2, v4

    #@1dde
    const-string v3, "CN"

    #@1de0
    aput-object v3, v2, v5

    #@1de2
    const-string v3, "46000"

    #@1de4
    aput-object v3, v2, v6

    #@1de6
    const-string v3, ""

    #@1de8
    aput-object v3, v2, v7

    #@1dea
    aput-object v2, v0, v1

    #@1dec
    const/16 v1, 0x15c

    #@1dee
    new-array v2, v8, [Ljava/lang/String;

    #@1df0
    const-string v3, "CMN"

    #@1df2
    aput-object v3, v2, v4

    #@1df4
    const-string v3, "CN"

    #@1df6
    aput-object v3, v2, v5

    #@1df8
    const-string v3, "46002"

    #@1dfa
    aput-object v3, v2, v6

    #@1dfc
    const-string v3, ""

    #@1dfe
    aput-object v3, v2, v7

    #@1e00
    aput-object v2, v0, v1

    #@1e02
    const/16 v1, 0x15d

    #@1e04
    new-array v2, v8, [Ljava/lang/String;

    #@1e06
    const-string v3, "CMN"

    #@1e08
    aput-object v3, v2, v4

    #@1e0a
    const-string v3, "CN"

    #@1e0c
    aput-object v3, v2, v5

    #@1e0e
    const-string v3, "46007"

    #@1e10
    aput-object v3, v2, v6

    #@1e12
    const-string v3, ""

    #@1e14
    aput-object v3, v2, v7

    #@1e16
    aput-object v2, v0, v1

    #@1e18
    const/16 v1, 0x15e

    #@1e1a
    new-array v2, v8, [Ljava/lang/String;

    #@1e1c
    const-string v3, "CNU"

    #@1e1e
    aput-object v3, v2, v4

    #@1e20
    const-string v3, "CN"

    #@1e22
    aput-object v3, v2, v5

    #@1e24
    const-string v3, "46001"

    #@1e26
    aput-object v3, v2, v6

    #@1e28
    const-string v3, ""

    #@1e2a
    aput-object v3, v2, v7

    #@1e2c
    aput-object v2, v0, v1

    #@1e2e
    const/16 v1, 0x15f

    #@1e30
    new-array v2, v8, [Ljava/lang/String;

    #@1e32
    const-string v3, "H3G"

    #@1e34
    aput-object v3, v2, v4

    #@1e36
    const-string v3, "HK"

    #@1e38
    aput-object v3, v2, v5

    #@1e3a
    const-string v3, "45403"

    #@1e3c
    aput-object v3, v2, v6

    #@1e3e
    const-string v3, ""

    #@1e40
    aput-object v3, v2, v7

    #@1e42
    aput-object v2, v0, v1

    #@1e44
    const/16 v1, 0x160

    #@1e46
    new-array v2, v8, [Ljava/lang/String;

    #@1e48
    const-string v3, "MCCI"

    #@1e4a
    aput-object v3, v2, v4

    #@1e4c
    const-string v3, "IR"

    #@1e4e
    aput-object v3, v2, v5

    #@1e50
    const-string v3, "43211"

    #@1e52
    aput-object v3, v2, v6

    #@1e54
    const-string v3, ""

    #@1e56
    aput-object v3, v2, v7

    #@1e58
    aput-object v2, v0, v1

    #@1e5a
    const/16 v1, 0x161

    #@1e5c
    new-array v2, v8, [Ljava/lang/String;

    #@1e5e
    const-string v3, "MCCI"

    #@1e60
    aput-object v3, v2, v4

    #@1e62
    const-string v3, "IR"

    #@1e64
    aput-object v3, v2, v5

    #@1e66
    const-string v3, "43214"

    #@1e68
    aput-object v3, v2, v6

    #@1e6a
    const-string v3, ""

    #@1e6c
    aput-object v3, v2, v7

    #@1e6e
    aput-object v2, v0, v1

    #@1e70
    const/16 v1, 0x162

    #@1e72
    new-array v2, v8, [Ljava/lang/String;

    #@1e74
    const-string v3, "MCCI"

    #@1e76
    aput-object v3, v2, v4

    #@1e78
    const-string v3, "IR"

    #@1e7a
    aput-object v3, v2, v5

    #@1e7c
    const-string v3, "43219"

    #@1e7e
    aput-object v3, v2, v6

    #@1e80
    const-string v3, ""

    #@1e82
    aput-object v3, v2, v7

    #@1e84
    aput-object v2, v0, v1

    #@1e86
    const/16 v1, 0x163

    #@1e88
    new-array v2, v8, [Ljava/lang/String;

    #@1e8a
    const-string v3, "MCCI"

    #@1e8c
    aput-object v3, v2, v4

    #@1e8e
    const-string v3, "IR"

    #@1e90
    aput-object v3, v2, v5

    #@1e92
    const-string v3, "43270"

    #@1e94
    aput-object v3, v2, v6

    #@1e96
    const-string v3, ""

    #@1e98
    aput-object v3, v2, v7

    #@1e9a
    aput-object v2, v0, v1

    #@1e9c
    const/16 v1, 0x164

    #@1e9e
    new-array v2, v8, [Ljava/lang/String;

    #@1ea0
    const-string v3, "MCCI"

    #@1ea2
    aput-object v3, v2, v4

    #@1ea4
    const-string v3, "IR"

    #@1ea6
    aput-object v3, v2, v5

    #@1ea8
    const-string v3, "43293"

    #@1eaa
    aput-object v3, v2, v6

    #@1eac
    const-string v3, ""

    #@1eae
    aput-object v3, v2, v7

    #@1eb0
    aput-object v2, v0, v1

    #@1eb2
    const/16 v1, 0x165

    #@1eb4
    new-array v2, v8, [Ljava/lang/String;

    #@1eb6
    const-string v3, "ICEL"

    #@1eb8
    aput-object v3, v2, v4

    #@1eba
    const-string v3, "IR"

    #@1ebc
    aput-object v3, v2, v5

    #@1ebe
    const-string v3, "43235"

    #@1ec0
    aput-object v3, v2, v6

    #@1ec2
    const-string v3, ""

    #@1ec4
    aput-object v3, v2, v7

    #@1ec6
    aput-object v2, v0, v1

    #@1ec8
    const/16 v1, 0x166

    #@1eca
    new-array v2, v8, [Ljava/lang/String;

    #@1ecc
    const-string v3, "ASIA"

    #@1ece
    aput-object v3, v2, v4

    #@1ed0
    const-string v3, "IQ"

    #@1ed2
    aput-object v3, v2, v5

    #@1ed4
    const-string v3, "41805"

    #@1ed6
    aput-object v3, v2, v6

    #@1ed8
    const-string v3, ""

    #@1eda
    aput-object v3, v2, v7

    #@1edc
    aput-object v2, v0, v1

    #@1ede
    const/16 v1, 0x167

    #@1ee0
    new-array v2, v8, [Ljava/lang/String;

    #@1ee2
    const-string v3, "ZAIN"

    #@1ee4
    aput-object v3, v2, v4

    #@1ee6
    const-string v3, "IQ"

    #@1ee8
    aput-object v3, v2, v5

    #@1eea
    const-string v3, "41820"

    #@1eec
    aput-object v3, v2, v6

    #@1eee
    const-string v3, ""

    #@1ef0
    aput-object v3, v2, v7

    #@1ef2
    aput-object v2, v0, v1

    #@1ef4
    const/16 v1, 0x168

    #@1ef6
    new-array v2, v8, [Ljava/lang/String;

    #@1ef8
    const-string v3, "ZAIN"

    #@1efa
    aput-object v3, v2, v4

    #@1efc
    const-string v3, "IQ"

    #@1efe
    aput-object v3, v2, v5

    #@1f00
    const-string v3, "41830"

    #@1f02
    aput-object v3, v2, v6

    #@1f04
    const-string v3, ""

    #@1f06
    aput-object v3, v2, v7

    #@1f08
    aput-object v2, v0, v1

    #@1f0a
    const/16 v1, 0x169

    #@1f0c
    new-array v2, v8, [Ljava/lang/String;

    #@1f0e
    const-string v3, "KOR"

    #@1f10
    aput-object v3, v2, v4

    #@1f12
    const-string v3, "IQ"

    #@1f14
    aput-object v3, v2, v5

    #@1f16
    const-string v3, "41840"

    #@1f18
    aput-object v3, v2, v6

    #@1f1a
    const-string v3, ""

    #@1f1c
    aput-object v3, v2, v7

    #@1f1e
    aput-object v2, v0, v1

    #@1f20
    const/16 v1, 0x16a

    #@1f22
    new-array v2, v8, [Ljava/lang/String;

    #@1f24
    const-string v3, "ORI"

    #@1f26
    aput-object v3, v2, v4

    #@1f28
    const-string v3, "IL"

    #@1f2a
    aput-object v3, v2, v5

    #@1f2c
    const-string v3, "42501"

    #@1f2e
    aput-object v3, v2, v6

    #@1f30
    const-string v3, ""

    #@1f32
    aput-object v3, v2, v7

    #@1f34
    aput-object v2, v0, v1

    #@1f36
    const/16 v1, 0x16b

    #@1f38
    new-array v2, v8, [Ljava/lang/String;

    #@1f3a
    const-string v3, "CCM"

    #@1f3c
    aput-object v3, v2, v4

    #@1f3e
    const-string v3, "IL"

    #@1f40
    aput-object v3, v2, v5

    #@1f42
    const-string v3, "42502"

    #@1f44
    aput-object v3, v2, v6

    #@1f46
    const-string v3, ""

    #@1f48
    aput-object v3, v2, v7

    #@1f4a
    aput-object v2, v0, v1

    #@1f4c
    const/16 v1, 0x16c

    #@1f4e
    new-array v2, v8, [Ljava/lang/String;

    #@1f50
    const-string v3, "PCL"

    #@1f52
    aput-object v3, v2, v4

    #@1f54
    const-string v3, "IL"

    #@1f56
    aput-object v3, v2, v5

    #@1f58
    const-string v3, "42503"

    #@1f5a
    aput-object v3, v2, v6

    #@1f5c
    const-string v3, ""

    #@1f5e
    aput-object v3, v2, v7

    #@1f60
    aput-object v2, v0, v1

    #@1f62
    const/16 v1, 0x16d

    #@1f64
    new-array v2, v8, [Ljava/lang/String;

    #@1f66
    const-string v3, "PCL"

    #@1f68
    aput-object v3, v2, v4

    #@1f6a
    const-string v3, "IL"

    #@1f6c
    aput-object v3, v2, v5

    #@1f6e
    const-string v3, "20404"

    #@1f70
    aput-object v3, v2, v6

    #@1f72
    const-string v3, "PELEPHONE"

    #@1f74
    aput-object v3, v2, v7

    #@1f76
    aput-object v2, v0, v1

    #@1f78
    const/16 v1, 0x16e

    #@1f7a
    new-array v2, v8, [Ljava/lang/String;

    #@1f7c
    const-string v3, "JAW"

    #@1f7e
    aput-object v3, v2, v4

    #@1f80
    const-string v3, "IL"

    #@1f82
    aput-object v3, v2, v5

    #@1f84
    const-string v3, "42505"

    #@1f86
    aput-object v3, v2, v6

    #@1f88
    const-string v3, ""

    #@1f8a
    aput-object v3, v2, v7

    #@1f8c
    aput-object v2, v0, v1

    #@1f8e
    const/16 v1, 0x16f

    #@1f90
    new-array v2, v8, [Ljava/lang/String;

    #@1f92
    const-string v3, "WAT"

    #@1f94
    aput-object v3, v2, v4

    #@1f96
    const-string v3, "IL"

    #@1f98
    aput-object v3, v2, v5

    #@1f9a
    const-string v3, "42506"

    #@1f9c
    aput-object v3, v2, v6

    #@1f9e
    const-string v3, ""

    #@1fa0
    aput-object v3, v2, v7

    #@1fa2
    aput-object v2, v0, v1

    #@1fa4
    const/16 v1, 0x170

    #@1fa6
    new-array v2, v8, [Ljava/lang/String;

    #@1fa8
    const-string v3, "MIRS"

    #@1faa
    aput-object v3, v2, v4

    #@1fac
    const-string v3, "IL"

    #@1fae
    aput-object v3, v2, v5

    #@1fb0
    const-string v3, "42507"

    #@1fb2
    aput-object v3, v2, v6

    #@1fb4
    const-string v3, ""

    #@1fb6
    aput-object v3, v2, v7

    #@1fb8
    aput-object v2, v0, v1

    #@1fba
    const/16 v1, 0x171

    #@1fbc
    new-array v2, v8, [Ljava/lang/String;

    #@1fbe
    const-string v3, "MIRS"

    #@1fc0
    aput-object v3, v2, v4

    #@1fc2
    const-string v3, "IL"

    #@1fc4
    aput-object v3, v2, v5

    #@1fc6
    const-string v3, "20404"

    #@1fc8
    aput-object v3, v2, v6

    #@1fca
    const-string v3, "MIRS"

    #@1fcc
    aput-object v3, v2, v7

    #@1fce
    aput-object v2, v0, v1

    #@1fd0
    const/16 v1, 0x172

    #@1fd2
    new-array v2, v8, [Ljava/lang/String;

    #@1fd4
    const-string v3, "ZAIN"

    #@1fd6
    aput-object v3, v2, v4

    #@1fd8
    const-string v3, "JO"

    #@1fda
    aput-object v3, v2, v5

    #@1fdc
    const-string v3, "41601"

    #@1fde
    aput-object v3, v2, v6

    #@1fe0
    const-string v3, ""

    #@1fe2
    aput-object v3, v2, v7

    #@1fe4
    aput-object v2, v0, v1

    #@1fe6
    const/16 v1, 0x173

    #@1fe8
    new-array v2, v8, [Ljava/lang/String;

    #@1fea
    const-string v3, "UMNI"

    #@1fec
    aput-object v3, v2, v4

    #@1fee
    const-string v3, "JO"

    #@1ff0
    aput-object v3, v2, v5

    #@1ff2
    const-string v3, "41603"

    #@1ff4
    aput-object v3, v2, v6

    #@1ff6
    const-string v3, ""

    #@1ff8
    aput-object v3, v2, v7

    #@1ffa
    aput-object v2, v0, v1

    #@1ffc
    const/16 v1, 0x174

    #@1ffe
    new-array v2, v8, [Ljava/lang/String;

    #@2000
    const-string v3, "ORG"

    #@2002
    aput-object v3, v2, v4

    #@2004
    const-string v3, "JO"

    #@2006
    aput-object v3, v2, v5

    #@2008
    const-string v3, "41677"

    #@200a
    aput-object v3, v2, v6

    #@200c
    const-string v3, ""

    #@200e
    aput-object v3, v2, v7

    #@2010
    aput-object v2, v0, v1

    #@2012
    const/16 v1, 0x175

    #@2014
    new-array v2, v8, [Ljava/lang/String;

    #@2016
    const-string v3, "ORG"

    #@2018
    aput-object v3, v2, v4

    #@201a
    const-string v3, "JO"

    #@201c
    aput-object v3, v2, v5

    #@201e
    const-string v3, "416770"

    #@2020
    aput-object v3, v2, v6

    #@2022
    const-string v3, ""

    #@2024
    aput-object v3, v2, v7

    #@2026
    aput-object v2, v0, v1

    #@2028
    const/16 v1, 0x176

    #@202a
    new-array v2, v8, [Ljava/lang/String;

    #@202c
    const-string v3, "ZAIN"

    #@202e
    aput-object v3, v2, v4

    #@2030
    const-string v3, "KW"

    #@2032
    aput-object v3, v2, v5

    #@2034
    const-string v3, "41902"

    #@2036
    aput-object v3, v2, v6

    #@2038
    const-string v3, ""

    #@203a
    aput-object v3, v2, v7

    #@203c
    aput-object v2, v0, v1

    #@203e
    const/16 v1, 0x177

    #@2040
    new-array v2, v8, [Ljava/lang/String;

    #@2042
    const-string v3, "WAT"

    #@2044
    aput-object v3, v2, v4

    #@2046
    const-string v3, "KW"

    #@2048
    aput-object v3, v2, v5

    #@204a
    const-string v3, "41903"

    #@204c
    aput-object v3, v2, v6

    #@204e
    const-string v3, ""

    #@2050
    aput-object v3, v2, v7

    #@2052
    aput-object v2, v0, v1

    #@2054
    const/16 v1, 0x178

    #@2056
    new-array v2, v8, [Ljava/lang/String;

    #@2058
    const-string v3, "VIVA"

    #@205a
    aput-object v3, v2, v4

    #@205c
    const-string v3, "KW"

    #@205e
    aput-object v3, v2, v5

    #@2060
    const-string v3, "41904"

    #@2062
    aput-object v3, v2, v6

    #@2064
    const-string v3, ""

    #@2066
    aput-object v3, v2, v7

    #@2068
    aput-object v2, v0, v1

    #@206a
    const/16 v1, 0x179

    #@206c
    new-array v2, v8, [Ljava/lang/String;

    #@206e
    const-string v3, "ALFA"

    #@2070
    aput-object v3, v2, v4

    #@2072
    const-string v3, "LB"

    #@2074
    aput-object v3, v2, v5

    #@2076
    const-string v3, "41501"

    #@2078
    aput-object v3, v2, v6

    #@207a
    const-string v3, ""

    #@207c
    aput-object v3, v2, v7

    #@207e
    aput-object v2, v0, v1

    #@2080
    const/16 v1, 0x17a

    #@2082
    new-array v2, v8, [Ljava/lang/String;

    #@2084
    const-string v3, "MTC"

    #@2086
    aput-object v3, v2, v4

    #@2088
    const-string v3, "LB"

    #@208a
    aput-object v3, v2, v5

    #@208c
    const-string v3, "41503"

    #@208e
    aput-object v3, v2, v6

    #@2090
    const-string v3, ""

    #@2092
    aput-object v3, v2, v7

    #@2094
    aput-object v2, v0, v1

    #@2096
    const/16 v1, 0x17b

    #@2098
    new-array v2, v8, [Ljava/lang/String;

    #@209a
    const-string v3, "STM"

    #@209c
    aput-object v3, v2, v4

    #@209e
    const-string v3, "MO"

    #@20a0
    aput-object v3, v2, v5

    #@20a2
    const-string v3, "45500"

    #@20a4
    aput-object v3, v2, v6

    #@20a6
    const-string v3, ""

    #@20a8
    aput-object v3, v2, v7

    #@20aa
    aput-object v2, v0, v1

    #@20ac
    const/16 v1, 0x17c

    #@20ae
    new-array v2, v8, [Ljava/lang/String;

    #@20b0
    const-string v3, "DIGI"

    #@20b2
    aput-object v3, v2, v4

    #@20b4
    const-string v3, "MY"

    #@20b6
    aput-object v3, v2, v5

    #@20b8
    const-string v3, "50210"

    #@20ba
    aput-object v3, v2, v6

    #@20bc
    const-string v3, ""

    #@20be
    aput-object v3, v2, v7

    #@20c0
    aput-object v2, v0, v1

    #@20c2
    const/16 v1, 0x17d

    #@20c4
    new-array v2, v8, [Ljava/lang/String;

    #@20c6
    const-string v3, "NTC"

    #@20c8
    aput-object v3, v2, v4

    #@20ca
    const-string v3, "NP"

    #@20cc
    aput-object v3, v2, v5

    #@20ce
    const-string v3, "42901"

    #@20d0
    aput-object v3, v2, v6

    #@20d2
    const-string v3, ""

    #@20d4
    aput-object v3, v2, v7

    #@20d6
    aput-object v2, v0, v1

    #@20d8
    const/16 v1, 0x17e

    #@20da
    new-array v2, v8, [Ljava/lang/String;

    #@20dc
    const-string v3, "NCEL"

    #@20de
    aput-object v3, v2, v4

    #@20e0
    const-string v3, "NP"

    #@20e2
    aput-object v3, v2, v5

    #@20e4
    const-string v3, "42902"

    #@20e6
    aput-object v3, v2, v6

    #@20e8
    const-string v3, ""

    #@20ea
    aput-object v3, v2, v7

    #@20ec
    aput-object v2, v0, v1

    #@20ee
    const/16 v1, 0x17f

    #@20f0
    new-array v2, v8, [Ljava/lang/String;

    #@20f2
    const-string v3, "VDF"

    #@20f4
    aput-object v3, v2, v4

    #@20f6
    const-string v3, "NZ"

    #@20f8
    aput-object v3, v2, v5

    #@20fa
    const-string v3, "53001"

    #@20fc
    aput-object v3, v2, v6

    #@20fe
    const-string v3, ""

    #@2100
    aput-object v3, v2, v7

    #@2102
    aput-object v2, v0, v1

    #@2104
    const/16 v1, 0x180

    #@2106
    new-array v2, v8, [Ljava/lang/String;

    #@2108
    const-string v3, "VDF"

    #@210a
    aput-object v3, v2, v4

    #@210c
    const-string v3, "NZ"

    #@210e
    aput-object v3, v2, v5

    #@2110
    const-string v3, "53024"

    #@2112
    aput-object v3, v2, v6

    #@2114
    const-string v3, ""

    #@2116
    aput-object v3, v2, v7

    #@2118
    aput-object v2, v0, v1

    #@211a
    const/16 v1, 0x181

    #@211c
    new-array v2, v8, [Ljava/lang/String;

    #@211e
    const-string v3, "DGR"

    #@2120
    aput-object v3, v2, v4

    #@2122
    const-string v3, "NZ"

    #@2124
    aput-object v3, v2, v5

    #@2126
    const-string v3, "53024"

    #@2128
    aput-object v3, v2, v6

    #@212a
    const-string v3, ""

    #@212c
    aput-object v3, v2, v7

    #@212e
    aput-object v2, v0, v1

    #@2130
    const/16 v1, 0x182

    #@2132
    new-array v2, v8, [Ljava/lang/String;

    #@2134
    const-string v3, "DGR"

    #@2136
    aput-object v3, v2, v4

    #@2138
    const-string v3, "NZ"

    #@213a
    aput-object v3, v2, v5

    #@213c
    const-string v3, "53001"

    #@213e
    aput-object v3, v2, v6

    #@2140
    const-string v3, ""

    #@2142
    aput-object v3, v2, v7

    #@2144
    aput-object v2, v0, v1

    #@2146
    const/16 v1, 0x183

    #@2148
    new-array v2, v8, [Ljava/lang/String;

    #@214a
    const-string v3, "OMAN"

    #@214c
    aput-object v3, v2, v4

    #@214e
    const-string v3, "OM"

    #@2150
    aput-object v3, v2, v5

    #@2152
    const-string v3, "42202"

    #@2154
    aput-object v3, v2, v6

    #@2156
    const-string v3, ""

    #@2158
    aput-object v3, v2, v7

    #@215a
    aput-object v2, v0, v1

    #@215c
    const/16 v1, 0x184

    #@215e
    new-array v2, v8, [Ljava/lang/String;

    #@2160
    const-string v3, "NAW"

    #@2162
    aput-object v3, v2, v4

    #@2164
    const-string v3, "OM"

    #@2166
    aput-object v3, v2, v5

    #@2168
    const-string v3, "42203"

    #@216a
    aput-object v3, v2, v6

    #@216c
    const-string v3, ""

    #@216e
    aput-object v3, v2, v7

    #@2170
    aput-object v2, v0, v1

    #@2172
    const/16 v1, 0x185

    #@2174
    new-array v2, v8, [Ljava/lang/String;

    #@2176
    const-string v3, "QTEL"

    #@2178
    aput-object v3, v2, v4

    #@217a
    const-string v3, "QA"

    #@217c
    aput-object v3, v2, v5

    #@217e
    const-string v3, "42701"

    #@2180
    aput-object v3, v2, v6

    #@2182
    const-string v3, ""

    #@2184
    aput-object v3, v2, v7

    #@2186
    aput-object v2, v0, v1

    #@2188
    const/16 v1, 0x186

    #@218a
    new-array v2, v8, [Ljava/lang/String;

    #@218c
    const-string v3, "VDF"

    #@218e
    aput-object v3, v2, v4

    #@2190
    const-string v3, "QA"

    #@2192
    aput-object v3, v2, v5

    #@2194
    const-string v3, "42702"

    #@2196
    aput-object v3, v2, v6

    #@2198
    const-string v3, ""

    #@219a
    aput-object v3, v2, v7

    #@219c
    aput-object v2, v0, v1

    #@219e
    const/16 v1, 0x187

    #@21a0
    new-array v2, v8, [Ljava/lang/String;

    #@21a2
    const-string v3, "STC"

    #@21a4
    aput-object v3, v2, v4

    #@21a6
    const-string v3, "SA"

    #@21a8
    aput-object v3, v2, v5

    #@21aa
    const-string v3, "42001"

    #@21ac
    aput-object v3, v2, v6

    #@21ae
    const-string v3, ""

    #@21b0
    aput-object v3, v2, v7

    #@21b2
    aput-object v2, v0, v1

    #@21b4
    const/16 v1, 0x188

    #@21b6
    new-array v2, v8, [Ljava/lang/String;

    #@21b8
    const-string v3, "MOBI"

    #@21ba
    aput-object v3, v2, v4

    #@21bc
    const-string v3, "SA"

    #@21be
    aput-object v3, v2, v5

    #@21c0
    const-string v3, "42003"

    #@21c2
    aput-object v3, v2, v6

    #@21c4
    const-string v3, ""

    #@21c6
    aput-object v3, v2, v7

    #@21c8
    aput-object v2, v0, v1

    #@21ca
    const/16 v1, 0x189

    #@21cc
    new-array v2, v8, [Ljava/lang/String;

    #@21ce
    const-string v3, "ZAIN"

    #@21d0
    aput-object v3, v2, v4

    #@21d2
    const-string v3, "SA"

    #@21d4
    aput-object v3, v2, v5

    #@21d6
    const-string v3, "42004"

    #@21d8
    aput-object v3, v2, v6

    #@21da
    const-string v3, ""

    #@21dc
    aput-object v3, v2, v7

    #@21de
    aput-object v2, v0, v1

    #@21e0
    const/16 v1, 0x18a

    #@21e2
    new-array v2, v8, [Ljava/lang/String;

    #@21e4
    const-string v3, "SING"

    #@21e6
    aput-object v3, v2, v4

    #@21e8
    const-string v3, "SG"

    #@21ea
    aput-object v3, v2, v5

    #@21ec
    const-string v3, "52501"

    #@21ee
    aput-object v3, v2, v6

    #@21f0
    const-string v3, ""

    #@21f2
    aput-object v3, v2, v7

    #@21f4
    aput-object v2, v0, v1

    #@21f6
    const/16 v1, 0x18b

    #@21f8
    new-array v2, v8, [Ljava/lang/String;

    #@21fa
    const-string v3, "SING"

    #@21fc
    aput-object v3, v2, v4

    #@21fe
    const-string v3, "SG"

    #@2200
    aput-object v3, v2, v5

    #@2202
    const-string v3, "52502"

    #@2204
    aput-object v3, v2, v6

    #@2206
    const-string v3, ""

    #@2208
    aput-object v3, v2, v7

    #@220a
    aput-object v2, v0, v1

    #@220c
    const/16 v1, 0x18c

    #@220e
    new-array v2, v8, [Ljava/lang/String;

    #@2210
    const-string v3, "SING"

    #@2212
    aput-object v3, v2, v4

    #@2214
    const-string v3, "SG"

    #@2216
    aput-object v3, v2, v5

    #@2218
    const-string v3, "52503"

    #@221a
    aput-object v3, v2, v6

    #@221c
    const-string v3, ""

    #@221e
    aput-object v3, v2, v7

    #@2220
    aput-object v2, v0, v1

    #@2222
    const/16 v1, 0x18d

    #@2224
    new-array v2, v8, [Ljava/lang/String;

    #@2226
    const-string v3, "SING"

    #@2228
    aput-object v3, v2, v4

    #@222a
    const-string v3, "SG"

    #@222c
    aput-object v3, v2, v5

    #@222e
    const-string v3, "52504"

    #@2230
    aput-object v3, v2, v6

    #@2232
    const-string v3, ""

    #@2234
    aput-object v3, v2, v7

    #@2236
    aput-object v2, v0, v1

    #@2238
    const/16 v1, 0x18e

    #@223a
    new-array v2, v8, [Ljava/lang/String;

    #@223c
    const-string v3, "SING"

    #@223e
    aput-object v3, v2, v4

    #@2240
    const-string v3, "SG"

    #@2242
    aput-object v3, v2, v5

    #@2244
    const-string v3, "52505"

    #@2246
    aput-object v3, v2, v6

    #@2248
    const-string v3, ""

    #@224a
    aput-object v3, v2, v7

    #@224c
    aput-object v2, v0, v1

    #@224e
    const/16 v1, 0x18f

    #@2250
    new-array v2, v8, [Ljava/lang/String;

    #@2252
    const-string v3, "M1"

    #@2254
    aput-object v3, v2, v4

    #@2256
    const-string v3, "SG"

    #@2258
    aput-object v3, v2, v5

    #@225a
    const-string v3, "52503"

    #@225c
    aput-object v3, v2, v6

    #@225e
    const-string v3, ""

    #@2260
    aput-object v3, v2, v7

    #@2262
    aput-object v2, v0, v1

    #@2264
    const/16 v1, 0x190

    #@2266
    new-array v2, v8, [Ljava/lang/String;

    #@2268
    const-string v3, "M1"

    #@226a
    aput-object v3, v2, v4

    #@226c
    const-string v3, "SG"

    #@226e
    aput-object v3, v2, v5

    #@2270
    const-string v3, "52504"

    #@2272
    aput-object v3, v2, v6

    #@2274
    const-string v3, ""

    #@2276
    aput-object v3, v2, v7

    #@2278
    aput-object v2, v0, v1

    #@227a
    const/16 v1, 0x191

    #@227c
    new-array v2, v8, [Ljava/lang/String;

    #@227e
    const-string v3, "STAR"

    #@2280
    aput-object v3, v2, v4

    #@2282
    const-string v3, "SG"

    #@2284
    aput-object v3, v2, v5

    #@2286
    const-string v3, "52505"

    #@2288
    aput-object v3, v2, v6

    #@228a
    const-string v3, ""

    #@228c
    aput-object v3, v2, v7

    #@228e
    aput-object v2, v0, v1

    #@2290
    const/16 v1, 0x192

    #@2292
    new-array v2, v8, [Ljava/lang/String;

    #@2294
    const-string v3, "STAR"

    #@2296
    aput-object v3, v2, v4

    #@2298
    const-string v3, "SG"

    #@229a
    aput-object v3, v2, v5

    #@229c
    const-string v3, "52501"

    #@229e
    aput-object v3, v2, v6

    #@22a0
    const-string v3, ""

    #@22a2
    aput-object v3, v2, v7

    #@22a4
    aput-object v2, v0, v1

    #@22a6
    const/16 v1, 0x193

    #@22a8
    new-array v2, v8, [Ljava/lang/String;

    #@22aa
    const-string v3, "STAR"

    #@22ac
    aput-object v3, v2, v4

    #@22ae
    const-string v3, "SG"

    #@22b0
    aput-object v3, v2, v5

    #@22b2
    const-string v3, "52502"

    #@22b4
    aput-object v3, v2, v6

    #@22b6
    const-string v3, ""

    #@22b8
    aput-object v3, v2, v7

    #@22ba
    aput-object v2, v0, v1

    #@22bc
    const/16 v1, 0x194

    #@22be
    new-array v2, v8, [Ljava/lang/String;

    #@22c0
    const-string v3, "STAR"

    #@22c2
    aput-object v3, v2, v4

    #@22c4
    const-string v3, "SG"

    #@22c6
    aput-object v3, v2, v5

    #@22c8
    const-string v3, "52503"

    #@22ca
    aput-object v3, v2, v6

    #@22cc
    const-string v3, ""

    #@22ce
    aput-object v3, v2, v7

    #@22d0
    aput-object v2, v0, v1

    #@22d2
    const/16 v1, 0x195

    #@22d4
    new-array v2, v8, [Ljava/lang/String;

    #@22d6
    const-string v3, "STAR"

    #@22d8
    aput-object v3, v2, v4

    #@22da
    const-string v3, "SG"

    #@22dc
    aput-object v3, v2, v5

    #@22de
    const-string v3, "52504"

    #@22e0
    aput-object v3, v2, v6

    #@22e2
    const-string v3, ""

    #@22e4
    aput-object v3, v2, v7

    #@22e6
    aput-object v2, v0, v1

    #@22e8
    const/16 v1, 0x196

    #@22ea
    new-array v2, v8, [Ljava/lang/String;

    #@22ec
    const-string v3, "MOBI"

    #@22ee
    aput-object v3, v2, v4

    #@22f0
    const-string v3, "LK"

    #@22f2
    aput-object v3, v2, v5

    #@22f4
    const-string v3, "41301"

    #@22f6
    aput-object v3, v2, v6

    #@22f8
    const-string v3, ""

    #@22fa
    aput-object v3, v2, v7

    #@22fc
    aput-object v2, v0, v1

    #@22fe
    const/16 v1, 0x197

    #@2300
    new-array v2, v8, [Ljava/lang/String;

    #@2302
    const-string v3, "DIAL"

    #@2304
    aput-object v3, v2, v4

    #@2306
    const-string v3, "LK"

    #@2308
    aput-object v3, v2, v5

    #@230a
    const-string v3, "41302"

    #@230c
    aput-object v3, v2, v6

    #@230e
    const-string v3, ""

    #@2310
    aput-object v3, v2, v7

    #@2312
    aput-object v2, v0, v1

    #@2314
    const/16 v1, 0x198

    #@2316
    new-array v2, v8, [Ljava/lang/String;

    #@2318
    const-string v3, "ETI"

    #@231a
    aput-object v3, v2, v4

    #@231c
    const-string v3, "LK"

    #@231e
    aput-object v3, v2, v5

    #@2320
    const-string v3, "41303"

    #@2322
    aput-object v3, v2, v6

    #@2324
    const-string v3, ""

    #@2326
    aput-object v3, v2, v7

    #@2328
    aput-object v2, v0, v1

    #@232a
    const/16 v1, 0x199

    #@232c
    new-array v2, v8, [Ljava/lang/String;

    #@232e
    const-string v3, "AIR"

    #@2330
    aput-object v3, v2, v4

    #@2332
    const-string v3, "LK"

    #@2334
    aput-object v3, v2, v5

    #@2336
    const-string v3, "41305"

    #@2338
    aput-object v3, v2, v6

    #@233a
    const-string v3, ""

    #@233c
    aput-object v3, v2, v7

    #@233e
    aput-object v2, v0, v1

    #@2340
    const/16 v1, 0x19a

    #@2342
    new-array v2, v8, [Ljava/lang/String;

    #@2344
    const-string v3, "SRA"

    #@2346
    aput-object v3, v2, v4

    #@2348
    const-string v3, "SY"

    #@234a
    aput-object v3, v2, v5

    #@234c
    const-string v3, "41701"

    #@234e
    aput-object v3, v2, v6

    #@2350
    const-string v3, ""

    #@2352
    aput-object v3, v2, v7

    #@2354
    aput-object v2, v0, v1

    #@2356
    const/16 v1, 0x19b

    #@2358
    new-array v2, v8, [Ljava/lang/String;

    #@235a
    const-string v3, "MTN"

    #@235c
    aput-object v3, v2, v4

    #@235e
    const-string v3, "SY"

    #@2360
    aput-object v3, v2, v5

    #@2362
    const-string v3, "41702"

    #@2364
    aput-object v3, v2, v6

    #@2366
    const-string v3, ""

    #@2368
    aput-object v3, v2, v7

    #@236a
    aput-object v2, v0, v1

    #@236c
    const/16 v1, 0x19c

    #@236e
    new-array v2, v8, [Ljava/lang/String;

    #@2370
    const-string v3, "TWM"

    #@2372
    aput-object v3, v2, v4

    #@2374
    const-string v3, "TW"

    #@2376
    aput-object v3, v2, v5

    #@2378
    const-string v3, "46697"

    #@237a
    aput-object v3, v2, v6

    #@237c
    const-string v3, ""

    #@237e
    aput-object v3, v2, v7

    #@2380
    aput-object v2, v0, v1

    #@2382
    const/16 v1, 0x19d

    #@2384
    new-array v2, v8, [Ljava/lang/String;

    #@2386
    const-string v3, "TRUE"

    #@2388
    aput-object v3, v2, v4

    #@238a
    const-string v3, "TH"

    #@238c
    aput-object v3, v2, v5

    #@238e
    const-string v3, "52000"

    #@2390
    aput-object v3, v2, v6

    #@2392
    const-string v3, "01"

    #@2394
    aput-object v3, v2, v7

    #@2396
    aput-object v2, v0, v1

    #@2398
    const/16 v1, 0x19e

    #@239a
    new-array v2, v8, [Ljava/lang/String;

    #@239c
    const-string v3, "CAT"

    #@239e
    aput-object v3, v2, v4

    #@23a0
    const-string v3, "TH"

    #@23a2
    aput-object v3, v2, v5

    #@23a4
    const-string v3, "52000"

    #@23a6
    aput-object v3, v2, v6

    #@23a8
    const-string v3, "02"

    #@23aa
    aput-object v3, v2, v7

    #@23ac
    aput-object v2, v0, v1

    #@23ae
    const/16 v1, 0x19f

    #@23b0
    new-array v2, v8, [Ljava/lang/String;

    #@23b2
    const-string v3, "AIS"

    #@23b4
    aput-object v3, v2, v4

    #@23b6
    const-string v3, "TH"

    #@23b8
    aput-object v3, v2, v5

    #@23ba
    const-string v3, "52001"

    #@23bc
    aput-object v3, v2, v6

    #@23be
    const-string v3, ""

    #@23c0
    aput-object v3, v2, v7

    #@23c2
    aput-object v2, v0, v1

    #@23c4
    const/16 v1, 0x1a0

    #@23c6
    new-array v2, v8, [Ljava/lang/String;

    #@23c8
    const-string v3, "TOT"

    #@23ca
    aput-object v3, v2, v4

    #@23cc
    const-string v3, "TH"

    #@23ce
    aput-object v3, v2, v5

    #@23d0
    const-string v3, "52015"

    #@23d2
    aput-object v3, v2, v6

    #@23d4
    const-string v3, ""

    #@23d6
    aput-object v3, v2, v7

    #@23d8
    aput-object v2, v0, v1

    #@23da
    const/16 v1, 0x1a1

    #@23dc
    new-array v2, v8, [Ljava/lang/String;

    #@23de
    const-string v3, "DTAC"

    #@23e0
    aput-object v3, v2, v4

    #@23e2
    const-string v3, "TH"

    #@23e4
    aput-object v3, v2, v5

    #@23e6
    const-string v3, "52018"

    #@23e8
    aput-object v3, v2, v6

    #@23ea
    const-string v3, ""

    #@23ec
    aput-object v3, v2, v7

    #@23ee
    aput-object v2, v0, v1

    #@23f0
    const/16 v1, 0x1a2

    #@23f2
    new-array v2, v8, [Ljava/lang/String;

    #@23f4
    const-string v3, "TRUE"

    #@23f6
    aput-object v3, v2, v4

    #@23f8
    const-string v3, "TH"

    #@23fa
    aput-object v3, v2, v5

    #@23fc
    const-string v3, "52099"

    #@23fe
    aput-object v3, v2, v6

    #@2400
    const-string v3, ""

    #@2402
    aput-object v3, v2, v7

    #@2404
    aput-object v2, v0, v1

    #@2406
    const/16 v1, 0x1a3

    #@2408
    new-array v2, v8, [Ljava/lang/String;

    #@240a
    const-string v3, "TUR"

    #@240c
    aput-object v3, v2, v4

    #@240e
    const-string v3, "TR"

    #@2410
    aput-object v3, v2, v5

    #@2412
    const-string v3, "28601"

    #@2414
    aput-object v3, v2, v6

    #@2416
    const-string v3, ""

    #@2418
    aput-object v3, v2, v7

    #@241a
    aput-object v2, v0, v1

    #@241c
    const/16 v1, 0x1a4

    #@241e
    new-array v2, v8, [Ljava/lang/String;

    #@2420
    const-string v3, "VDF"

    #@2422
    aput-object v3, v2, v4

    #@2424
    const-string v3, "TR"

    #@2426
    aput-object v3, v2, v5

    #@2428
    const-string v3, "28602"

    #@242a
    aput-object v3, v2, v6

    #@242c
    const-string v3, ""

    #@242e
    aput-object v3, v2, v7

    #@2430
    aput-object v2, v0, v1

    #@2432
    const/16 v1, 0x1a5

    #@2434
    new-array v2, v8, [Ljava/lang/String;

    #@2436
    const-string v3, "AVEA"

    #@2438
    aput-object v3, v2, v4

    #@243a
    const-string v3, "TR"

    #@243c
    aput-object v3, v2, v5

    #@243e
    const-string v3, "28603"

    #@2440
    aput-object v3, v2, v6

    #@2442
    const-string v3, ""

    #@2444
    aput-object v3, v2, v7

    #@2446
    aput-object v2, v0, v1

    #@2448
    const/16 v1, 0x1a6

    #@244a
    new-array v2, v8, [Ljava/lang/String;

    #@244c
    const-string v3, "ETIS"

    #@244e
    aput-object v3, v2, v4

    #@2450
    const-string v3, "AE"

    #@2452
    aput-object v3, v2, v5

    #@2454
    const-string v3, "42402"

    #@2456
    aput-object v3, v2, v6

    #@2458
    const-string v3, ""

    #@245a
    aput-object v3, v2, v7

    #@245c
    aput-object v2, v0, v1

    #@245e
    const/16 v1, 0x1a7

    #@2460
    new-array v2, v8, [Ljava/lang/String;

    #@2462
    const-string v3, "DU"

    #@2464
    aput-object v3, v2, v4

    #@2466
    const-string v3, "AE"

    #@2468
    aput-object v3, v2, v5

    #@246a
    const-string v3, "42403"

    #@246c
    aput-object v3, v2, v6

    #@246e
    const-string v3, ""

    #@2470
    aput-object v3, v2, v7

    #@2472
    aput-object v2, v0, v1

    #@2474
    const/16 v1, 0x1a8

    #@2476
    new-array v2, v8, [Ljava/lang/String;

    #@2478
    const-string v3, "MOBI"

    #@247a
    aput-object v3, v2, v4

    #@247c
    const-string v3, "VN"

    #@247e
    aput-object v3, v2, v5

    #@2480
    const-string v3, "45201"

    #@2482
    aput-object v3, v2, v6

    #@2484
    const-string v3, ""

    #@2486
    aput-object v3, v2, v7

    #@2488
    aput-object v2, v0, v1

    #@248a
    const/16 v1, 0x1a9

    #@248c
    new-array v2, v8, [Ljava/lang/String;

    #@248e
    const-string v3, "VINA"

    #@2490
    aput-object v3, v2, v4

    #@2492
    const-string v3, "VN"

    #@2494
    aput-object v3, v2, v5

    #@2496
    const-string v3, "45202"

    #@2498
    aput-object v3, v2, v6

    #@249a
    const-string v3, ""

    #@249c
    aput-object v3, v2, v7

    #@249e
    aput-object v2, v0, v1

    #@24a0
    const/16 v1, 0x1aa

    #@24a2
    new-array v2, v8, [Ljava/lang/String;

    #@24a4
    const-string v3, "VNT"

    #@24a6
    aput-object v3, v2, v4

    #@24a8
    const-string v3, "VN"

    #@24aa
    aput-object v3, v2, v5

    #@24ac
    const-string v3, "45204"

    #@24ae
    aput-object v3, v2, v6

    #@24b0
    const-string v3, ""

    #@24b2
    aput-object v3, v2, v7

    #@24b4
    aput-object v2, v0, v1

    #@24b6
    const/16 v1, 0x1ab

    #@24b8
    new-array v2, v8, [Ljava/lang/String;

    #@24ba
    const-string v3, "VNM"

    #@24bc
    aput-object v3, v2, v4

    #@24be
    const-string v3, "VN"

    #@24c0
    aput-object v3, v2, v5

    #@24c2
    const-string v3, "45205"

    #@24c4
    aput-object v3, v2, v6

    #@24c6
    const-string v3, ""

    #@24c8
    aput-object v3, v2, v7

    #@24ca
    aput-object v2, v0, v1

    #@24cc
    const/16 v1, 0x1ac

    #@24ce
    new-array v2, v8, [Ljava/lang/String;

    #@24d0
    const-string v3, "BEE"

    #@24d2
    aput-object v3, v2, v4

    #@24d4
    const-string v3, "VN"

    #@24d6
    aput-object v3, v2, v5

    #@24d8
    const-string v3, "45207"

    #@24da
    aput-object v3, v2, v6

    #@24dc
    const-string v3, ""

    #@24de
    aput-object v3, v2, v7

    #@24e0
    aput-object v2, v0, v1

    #@24e2
    const/16 v1, 0x1ad

    #@24e4
    new-array v2, v8, [Ljava/lang/String;

    #@24e6
    const-string v3, "EVN"

    #@24e8
    aput-object v3, v2, v4

    #@24ea
    const-string v3, "VN"

    #@24ec
    aput-object v3, v2, v5

    #@24ee
    const-string v3, "45208"

    #@24f0
    aput-object v3, v2, v6

    #@24f2
    const-string v3, ""

    #@24f4
    aput-object v3, v2, v7

    #@24f6
    aput-object v2, v0, v1

    #@24f8
    const/16 v1, 0x1ae

    #@24fa
    new-array v2, v8, [Ljava/lang/String;

    #@24fc
    const-string v3, "TCL"

    #@24fe
    aput-object v3, v2, v4

    #@2500
    const-string v3, "MX"

    #@2502
    aput-object v3, v2, v5

    #@2504
    const-string v3, "334020"

    #@2506
    aput-object v3, v2, v6

    #@2508
    const-string v3, ""

    #@250a
    aput-object v3, v2, v7

    #@250c
    aput-object v2, v0, v1

    #@250e
    const/16 v1, 0x1af

    #@2510
    new-array v2, v8, [Ljava/lang/String;

    #@2512
    const-string v3, "TLF"

    #@2514
    aput-object v3, v2, v4

    #@2516
    const-string v3, "MX"

    #@2518
    aput-object v3, v2, v5

    #@251a
    const-string v3, "334030"

    #@251c
    aput-object v3, v2, v6

    #@251e
    const-string v3, ""

    #@2520
    aput-object v3, v2, v7

    #@2522
    aput-object v2, v0, v1

    #@2524
    const/16 v1, 0x1b0

    #@2526
    new-array v2, v8, [Ljava/lang/String;

    #@2528
    const-string v3, "TLF"

    #@252a
    aput-object v3, v2, v4

    #@252c
    const-string v3, "MX"

    #@252e
    aput-object v3, v2, v5

    #@2530
    const-string v3, "33403"

    #@2532
    aput-object v3, v2, v6

    #@2534
    const-string v3, ""

    #@2536
    aput-object v3, v2, v7

    #@2538
    aput-object v2, v0, v1

    #@253a
    const/16 v1, 0x1b1

    #@253c
    new-array v2, v8, [Ljava/lang/String;

    #@253e
    const-string v3, "UNE"

    #@2540
    aput-object v3, v2, v4

    #@2542
    const-string v3, "MX"

    #@2544
    aput-object v3, v2, v5

    #@2546
    const-string v3, "334050"

    #@2548
    aput-object v3, v2, v6

    #@254a
    const-string v3, ""

    #@254c
    aput-object v3, v2, v7

    #@254e
    aput-object v2, v0, v1

    #@2550
    const/16 v1, 0x1b2

    #@2552
    new-array v2, v8, [Ljava/lang/String;

    #@2554
    const-string v3, "UNE"

    #@2556
    aput-object v3, v2, v4

    #@2558
    const-string v3, "MX"

    #@255a
    aput-object v3, v2, v5

    #@255c
    const-string v3, "22201"

    #@255e
    aput-object v3, v2, v6

    #@2560
    const-string v3, "Iusacell"

    #@2562
    aput-object v3, v2, v7

    #@2564
    aput-object v2, v0, v1

    #@2566
    const/16 v1, 0x1b3

    #@2568
    new-array v2, v8, [Ljava/lang/String;

    #@256a
    const-string v3, "USC"

    #@256c
    aput-object v3, v2, v4

    #@256e
    const-string v3, "MX"

    #@2570
    aput-object v3, v2, v5

    #@2572
    const-string v3, "334050"

    #@2574
    aput-object v3, v2, v6

    #@2576
    const-string v3, ""

    #@2578
    aput-object v3, v2, v7

    #@257a
    aput-object v2, v0, v1

    #@257c
    const/16 v1, 0x1b4

    #@257e
    new-array v2, v8, [Ljava/lang/String;

    #@2580
    const-string v3, "USC"

    #@2582
    aput-object v3, v2, v4

    #@2584
    const-string v3, "MX"

    #@2586
    aput-object v3, v2, v5

    #@2588
    const-string v3, "22201"

    #@258a
    aput-object v3, v2, v6

    #@258c
    const-string v3, "Iusacell"

    #@258e
    aput-object v3, v2, v7

    #@2590
    aput-object v2, v0, v1

    #@2592
    const/16 v1, 0x1b5

    #@2594
    new-array v2, v8, [Ljava/lang/String;

    #@2596
    const-string v3, "TLF"

    #@2598
    aput-object v3, v2, v4

    #@259a
    const-string v3, "GT"

    #@259c
    aput-object v3, v2, v5

    #@259e
    const-string v3, "70403"

    #@25a0
    aput-object v3, v2, v6

    #@25a2
    const-string v3, ""

    #@25a4
    aput-object v3, v2, v7

    #@25a6
    aput-object v2, v0, v1

    #@25a8
    const/16 v1, 0x1b6

    #@25aa
    new-array v2, v8, [Ljava/lang/String;

    #@25ac
    const-string v3, "TLF"

    #@25ae
    aput-object v3, v2, v4

    #@25b0
    const-string v3, "GT"

    #@25b2
    aput-object v3, v2, v5

    #@25b4
    const-string v3, "704030"

    #@25b6
    aput-object v3, v2, v6

    #@25b8
    const-string v3, ""

    #@25ba
    aput-object v3, v2, v7

    #@25bc
    aput-object v2, v0, v1

    #@25be
    const/16 v1, 0x1b7

    #@25c0
    new-array v2, v8, [Ljava/lang/String;

    #@25c2
    const-string v3, "TLF"

    #@25c4
    aput-object v3, v2, v4

    #@25c6
    const-string v3, "SV"

    #@25c8
    aput-object v3, v2, v5

    #@25ca
    const-string v3, "70604"

    #@25cc
    aput-object v3, v2, v6

    #@25ce
    const-string v3, ""

    #@25d0
    aput-object v3, v2, v7

    #@25d2
    aput-object v2, v0, v1

    #@25d4
    const/16 v1, 0x1b8

    #@25d6
    new-array v2, v8, [Ljava/lang/String;

    #@25d8
    const-string v3, "TLF"

    #@25da
    aput-object v3, v2, v4

    #@25dc
    const-string v3, "SV"

    #@25de
    aput-object v3, v2, v5

    #@25e0
    const-string v3, "706040"

    #@25e2
    aput-object v3, v2, v6

    #@25e4
    const-string v3, ""

    #@25e6
    aput-object v3, v2, v7

    #@25e8
    aput-object v2, v0, v1

    #@25ea
    const/16 v1, 0x1b9

    #@25ec
    new-array v2, v8, [Ljava/lang/String;

    #@25ee
    const-string v3, "TLF"

    #@25f0
    aput-object v3, v2, v4

    #@25f2
    const-string v3, "NI"

    #@25f4
    aput-object v3, v2, v5

    #@25f6
    const-string v3, "71030"

    #@25f8
    aput-object v3, v2, v6

    #@25fa
    const-string v3, ""

    #@25fc
    aput-object v3, v2, v7

    #@25fe
    aput-object v2, v0, v1

    #@2600
    const/16 v1, 0x1ba

    #@2602
    new-array v2, v8, [Ljava/lang/String;

    #@2604
    const-string v3, "TLF"

    #@2606
    aput-object v3, v2, v4

    #@2608
    const-string v3, "NI"

    #@260a
    aput-object v3, v2, v5

    #@260c
    const-string v3, "710300"

    #@260e
    aput-object v3, v2, v6

    #@2610
    const-string v3, ""

    #@2612
    aput-object v3, v2, v7

    #@2614
    aput-object v2, v0, v1

    #@2616
    const/16 v1, 0x1bb

    #@2618
    new-array v2, v8, [Ljava/lang/String;

    #@261a
    const-string v3, "TLF"

    #@261c
    aput-object v3, v2, v4

    #@261e
    const-string v3, "PA"

    #@2620
    aput-object v3, v2, v5

    #@2622
    const-string v3, "71402"

    #@2624
    aput-object v3, v2, v6

    #@2626
    const-string v3, ""

    #@2628
    aput-object v3, v2, v7

    #@262a
    aput-object v2, v0, v1

    #@262c
    const/16 v1, 0x1bc

    #@262e
    new-array v2, v8, [Ljava/lang/String;

    #@2630
    const-string v3, "TLF"

    #@2632
    aput-object v3, v2, v4

    #@2634
    const-string v3, "PA"

    #@2636
    aput-object v3, v2, v5

    #@2638
    const-string v3, "714020"

    #@263a
    aput-object v3, v2, v6

    #@263c
    const-string v3, ""

    #@263e
    aput-object v3, v2, v7

    #@2640
    aput-object v2, v0, v1

    #@2642
    const/16 v1, 0x1bd

    #@2644
    new-array v2, v8, [Ljava/lang/String;

    #@2646
    const-string v3, "TLF"

    #@2648
    aput-object v3, v2, v4

    #@264a
    const-string v3, "PE"

    #@264c
    aput-object v3, v2, v5

    #@264e
    const-string v3, "71606"

    #@2650
    aput-object v3, v2, v6

    #@2652
    const-string v3, ""

    #@2654
    aput-object v3, v2, v7

    #@2656
    aput-object v2, v0, v1

    #@2658
    const/16 v1, 0x1be

    #@265a
    new-array v2, v8, [Ljava/lang/String;

    #@265c
    const-string v3, "CLR"

    #@265e
    aput-object v3, v2, v4

    #@2660
    const-string v3, "PR"

    #@2662
    aput-object v3, v2, v5

    #@2664
    const-string v3, "330110"

    #@2666
    aput-object v3, v2, v6

    #@2668
    const-string v3, ""

    #@266a
    aput-object v3, v2, v7

    #@266c
    aput-object v2, v0, v1

    #@266e
    const/16 v1, 0x1bf

    #@2670
    new-array v2, v8, [Ljava/lang/String;

    #@2672
    const-string v3, "TLF"

    #@2674
    aput-object v3, v2, v4

    #@2676
    const-string v3, "AR"

    #@2678
    aput-object v3, v2, v5

    #@267a
    const-string v3, "72207"

    #@267c
    aput-object v3, v2, v6

    #@267e
    const-string v3, ""

    #@2680
    aput-object v3, v2, v7

    #@2682
    aput-object v2, v0, v1

    #@2684
    const/16 v1, 0x1c0

    #@2686
    new-array v2, v8, [Ljava/lang/String;

    #@2688
    const-string v3, "TIM"

    #@268a
    aput-object v3, v2, v4

    #@268c
    const-string v3, "BR"

    #@268e
    aput-object v3, v2, v5

    #@2690
    const-string v3, "72402"

    #@2692
    aput-object v3, v2, v6

    #@2694
    const-string v3, ""

    #@2696
    aput-object v3, v2, v7

    #@2698
    aput-object v2, v0, v1

    #@269a
    const/16 v1, 0x1c1

    #@269c
    new-array v2, v8, [Ljava/lang/String;

    #@269e
    const-string v3, "TIM"

    #@26a0
    aput-object v3, v2, v4

    #@26a2
    const-string v3, "BR"

    #@26a4
    aput-object v3, v2, v5

    #@26a6
    const-string v3, "72403"

    #@26a8
    aput-object v3, v2, v6

    #@26aa
    const-string v3, ""

    #@26ac
    aput-object v3, v2, v7

    #@26ae
    aput-object v2, v0, v1

    #@26b0
    const/16 v1, 0x1c2

    #@26b2
    new-array v2, v8, [Ljava/lang/String;

    #@26b4
    const-string v3, "TIM"

    #@26b6
    aput-object v3, v2, v4

    #@26b8
    const-string v3, "BR"

    #@26ba
    aput-object v3, v2, v5

    #@26bc
    const-string v3, "72404"

    #@26be
    aput-object v3, v2, v6

    #@26c0
    const-string v3, ""

    #@26c2
    aput-object v3, v2, v7

    #@26c4
    aput-object v2, v0, v1

    #@26c6
    const/16 v1, 0x1c3

    #@26c8
    new-array v2, v8, [Ljava/lang/String;

    #@26ca
    const-string v3, "CLR"

    #@26cc
    aput-object v3, v2, v4

    #@26ce
    const-string v3, "BR"

    #@26d0
    aput-object v3, v2, v5

    #@26d2
    const-string v3, "72405"

    #@26d4
    aput-object v3, v2, v6

    #@26d6
    const-string v3, ""

    #@26d8
    aput-object v3, v2, v7

    #@26da
    aput-object v2, v0, v1

    #@26dc
    const/16 v1, 0x1c4

    #@26de
    new-array v2, v8, [Ljava/lang/String;

    #@26e0
    const-string v3, "VIV"

    #@26e2
    aput-object v3, v2, v4

    #@26e4
    const-string v3, "BR"

    #@26e6
    aput-object v3, v2, v5

    #@26e8
    const-string v3, "72406"

    #@26ea
    aput-object v3, v2, v6

    #@26ec
    const-string v3, ""

    #@26ee
    aput-object v3, v2, v7

    #@26f0
    aput-object v2, v0, v1

    #@26f2
    const/16 v1, 0x1c5

    #@26f4
    new-array v2, v8, [Ljava/lang/String;

    #@26f6
    const-string v3, "VIV"

    #@26f8
    aput-object v3, v2, v4

    #@26fa
    const-string v3, "BR"

    #@26fc
    aput-object v3, v2, v5

    #@26fe
    const-string v3, "72410"

    #@2700
    aput-object v3, v2, v6

    #@2702
    const-string v3, ""

    #@2704
    aput-object v3, v2, v7

    #@2706
    aput-object v2, v0, v1

    #@2708
    const/16 v1, 0x1c6

    #@270a
    new-array v2, v8, [Ljava/lang/String;

    #@270c
    const-string v3, "VIV"

    #@270e
    aput-object v3, v2, v4

    #@2710
    const-string v3, "BR"

    #@2712
    aput-object v3, v2, v5

    #@2714
    const-string v3, "72411"

    #@2716
    aput-object v3, v2, v6

    #@2718
    const-string v3, ""

    #@271a
    aput-object v3, v2, v7

    #@271c
    aput-object v2, v0, v1

    #@271e
    const/16 v1, 0x1c7

    #@2720
    new-array v2, v8, [Ljava/lang/String;

    #@2722
    const-string v3, "VIV"

    #@2724
    aput-object v3, v2, v4

    #@2726
    const-string v3, "BR"

    #@2728
    aput-object v3, v2, v5

    #@272a
    const-string v3, "72423"

    #@272c
    aput-object v3, v2, v6

    #@272e
    const-string v3, ""

    #@2730
    aput-object v3, v2, v7

    #@2732
    aput-object v2, v0, v1

    #@2734
    const/16 v1, 0x1c8

    #@2736
    new-array v2, v8, [Ljava/lang/String;

    #@2738
    const-string v3, "SCTL"

    #@273a
    aput-object v3, v2, v4

    #@273c
    const-string v3, "BR"

    #@273e
    aput-object v3, v2, v5

    #@2740
    const-string v3, "72415"

    #@2742
    aput-object v3, v2, v6

    #@2744
    const-string v3, ""

    #@2746
    aput-object v3, v2, v7

    #@2748
    aput-object v2, v0, v1

    #@274a
    const/16 v1, 0x1c9

    #@274c
    new-array v2, v8, [Ljava/lang/String;

    #@274e
    const-string v3, "BRT"

    #@2750
    aput-object v3, v2, v4

    #@2752
    const-string v3, "BR"

    #@2754
    aput-object v3, v2, v5

    #@2756
    const-string v3, "72416"

    #@2758
    aput-object v3, v2, v6

    #@275a
    const-string v3, ""

    #@275c
    aput-object v3, v2, v7

    #@275e
    aput-object v2, v0, v1

    #@2760
    const/16 v1, 0x1ca

    #@2762
    new-array v2, v8, [Ljava/lang/String;

    #@2764
    const-string v3, "AMZ"

    #@2766
    aput-object v3, v2, v4

    #@2768
    const-string v3, "BR"

    #@276a
    aput-object v3, v2, v5

    #@276c
    const-string v3, "72424"

    #@276e
    aput-object v3, v2, v6

    #@2770
    const-string v3, ""

    #@2772
    aput-object v3, v2, v7

    #@2774
    aput-object v2, v0, v1

    #@2776
    const/16 v1, 0x1cb

    #@2778
    new-array v2, v8, [Ljava/lang/String;

    #@277a
    const-string v3, "Oi"

    #@277c
    aput-object v3, v2, v4

    #@277e
    const-string v3, "BR"

    #@2780
    aput-object v3, v2, v5

    #@2782
    const-string v3, "72431"

    #@2784
    aput-object v3, v2, v6

    #@2786
    const-string v3, ""

    #@2788
    aput-object v3, v2, v7

    #@278a
    aput-object v2, v0, v1

    #@278c
    const/16 v1, 0x1cc

    #@278e
    new-array v2, v8, [Ljava/lang/String;

    #@2790
    const-string v3, "CTBC"

    #@2792
    aput-object v3, v2, v4

    #@2794
    const-string v3, "BR"

    #@2796
    aput-object v3, v2, v5

    #@2798
    const-string v3, "72432"

    #@279a
    aput-object v3, v2, v6

    #@279c
    const-string v3, ""

    #@279e
    aput-object v3, v2, v7

    #@27a0
    aput-object v2, v0, v1

    #@27a2
    const/16 v1, 0x1cd

    #@27a4
    new-array v2, v8, [Ljava/lang/String;

    #@27a6
    const-string v3, "CTBC"

    #@27a8
    aput-object v3, v2, v4

    #@27aa
    const-string v3, "BR"

    #@27ac
    aput-object v3, v2, v5

    #@27ae
    const-string v3, "72433"

    #@27b0
    aput-object v3, v2, v6

    #@27b2
    const-string v3, ""

    #@27b4
    aput-object v3, v2, v7

    #@27b6
    aput-object v2, v0, v1

    #@27b8
    const/16 v1, 0x1ce

    #@27ba
    new-array v2, v8, [Ljava/lang/String;

    #@27bc
    const-string v3, "CTBC"

    #@27be
    aput-object v3, v2, v4

    #@27c0
    const-string v3, "BR"

    #@27c2
    aput-object v3, v2, v5

    #@27c4
    const-string v3, "72434"

    #@27c6
    aput-object v3, v2, v6

    #@27c8
    const-string v3, ""

    #@27ca
    aput-object v3, v2, v7

    #@27cc
    aput-object v2, v0, v1

    #@27ce
    const/16 v1, 0x1cf

    #@27d0
    new-array v2, v8, [Ljava/lang/String;

    #@27d2
    const-string v3, "ENT"

    #@27d4
    aput-object v3, v2, v4

    #@27d6
    const-string v3, "CL"

    #@27d8
    aput-object v3, v2, v5

    #@27da
    const-string v3, "73001"

    #@27dc
    aput-object v3, v2, v6

    #@27de
    const-string v3, ""

    #@27e0
    aput-object v3, v2, v7

    #@27e2
    aput-object v2, v0, v1

    #@27e4
    const/16 v1, 0x1d0

    #@27e6
    new-array v2, v8, [Ljava/lang/String;

    #@27e8
    const-string v3, "ENT"

    #@27ea
    aput-object v3, v2, v4

    #@27ec
    const-string v3, "CL"

    #@27ee
    aput-object v3, v2, v5

    #@27f0
    const-string v3, "73010"

    #@27f2
    aput-object v3, v2, v6

    #@27f4
    const-string v3, ""

    #@27f6
    aput-object v3, v2, v7

    #@27f8
    aput-object v2, v0, v1

    #@27fa
    const/16 v1, 0x1d1

    #@27fc
    new-array v2, v8, [Ljava/lang/String;

    #@27fe
    const-string v3, "TLE"

    #@2800
    aput-object v3, v2, v4

    #@2802
    const-string v3, "CL"

    #@2804
    aput-object v3, v2, v5

    #@2806
    const-string v3, "73002"

    #@2808
    aput-object v3, v2, v6

    #@280a
    const-string v3, ""

    #@280c
    aput-object v3, v2, v7

    #@280e
    aput-object v2, v0, v1

    #@2810
    const/16 v1, 0x1d2

    #@2812
    new-array v2, v8, [Ljava/lang/String;

    #@2814
    const-string v3, "CLR"

    #@2816
    aput-object v3, v2, v4

    #@2818
    const-string v3, "CL"

    #@281a
    aput-object v3, v2, v5

    #@281c
    const-string v3, "73003"

    #@281e
    aput-object v3, v2, v6

    #@2820
    const-string v3, ""

    #@2822
    aput-object v3, v2, v7

    #@2824
    aput-object v2, v0, v1

    #@2826
    const/16 v1, 0x1d3

    #@2828
    new-array v2, v8, [Ljava/lang/String;

    #@282a
    const-string v3, "TLF"

    #@282c
    aput-object v3, v2, v4

    #@282e
    const-string v3, "CO"

    #@2830
    aput-object v3, v2, v5

    #@2832
    const-string v3, "732123"

    #@2834
    aput-object v3, v2, v6

    #@2836
    const-string v3, ""

    #@2838
    aput-object v3, v2, v7

    #@283a
    aput-object v2, v0, v1

    #@283c
    const/16 v1, 0x1d4

    #@283e
    new-array v2, v8, [Ljava/lang/String;

    #@2840
    const-string v3, "TLF"

    #@2842
    aput-object v3, v2, v4

    #@2844
    const-string v3, "UY"

    #@2846
    aput-object v3, v2, v5

    #@2848
    const-string v3, "74807"

    #@284a
    aput-object v3, v2, v6

    #@284c
    const-string v3, ""

    #@284e
    aput-object v3, v2, v7

    #@2850
    aput-object v2, v0, v1

    #@2852
    const/16 v1, 0x1d5

    #@2854
    new-array v2, v8, [Ljava/lang/String;

    #@2856
    const-string v3, "TMO"

    #@2858
    aput-object v3, v2, v4

    #@285a
    const-string v3, "US"

    #@285c
    aput-object v3, v2, v5

    #@285e
    const-string v3, "310260"

    #@2860
    aput-object v3, v2, v6

    #@2862
    const-string v3, ""

    #@2864
    aput-object v3, v2, v7

    #@2866
    aput-object v2, v0, v1

    #@2868
    const/16 v1, 0x1d6

    #@286a
    new-array v2, v8, [Ljava/lang/String;

    #@286c
    const-string v3, "TMO"

    #@286e
    aput-object v3, v2, v4

    #@2870
    const-string v3, "US"

    #@2872
    aput-object v3, v2, v5

    #@2874
    const-string v3, "310160"

    #@2876
    aput-object v3, v2, v6

    #@2878
    const-string v3, ""

    #@287a
    aput-object v3, v2, v7

    #@287c
    aput-object v2, v0, v1

    #@287e
    const/16 v1, 0x1d7

    #@2880
    new-array v2, v8, [Ljava/lang/String;

    #@2882
    const-string v3, "TMO"

    #@2884
    aput-object v3, v2, v4

    #@2886
    const-string v3, "US"

    #@2888
    aput-object v3, v2, v5

    #@288a
    const-string v3, "310200"

    #@288c
    aput-object v3, v2, v6

    #@288e
    const-string v3, ""

    #@2890
    aput-object v3, v2, v7

    #@2892
    aput-object v2, v0, v1

    #@2894
    const/16 v1, 0x1d8

    #@2896
    new-array v2, v8, [Ljava/lang/String;

    #@2898
    const-string v3, "TMO"

    #@289a
    aput-object v3, v2, v4

    #@289c
    const-string v3, "US"

    #@289e
    aput-object v3, v2, v5

    #@28a0
    const-string v3, "310210"

    #@28a2
    aput-object v3, v2, v6

    #@28a4
    const-string v3, ""

    #@28a6
    aput-object v3, v2, v7

    #@28a8
    aput-object v2, v0, v1

    #@28aa
    const/16 v1, 0x1d9

    #@28ac
    new-array v2, v8, [Ljava/lang/String;

    #@28ae
    const-string v3, "TMO"

    #@28b0
    aput-object v3, v2, v4

    #@28b2
    const-string v3, "US"

    #@28b4
    aput-object v3, v2, v5

    #@28b6
    const-string v3, "310220"

    #@28b8
    aput-object v3, v2, v6

    #@28ba
    const-string v3, ""

    #@28bc
    aput-object v3, v2, v7

    #@28be
    aput-object v2, v0, v1

    #@28c0
    const/16 v1, 0x1da

    #@28c2
    new-array v2, v8, [Ljava/lang/String;

    #@28c4
    const-string v3, "TMO"

    #@28c6
    aput-object v3, v2, v4

    #@28c8
    const-string v3, "US"

    #@28ca
    aput-object v3, v2, v5

    #@28cc
    const-string v3, "310230"

    #@28ce
    aput-object v3, v2, v6

    #@28d0
    const-string v3, ""

    #@28d2
    aput-object v3, v2, v7

    #@28d4
    aput-object v2, v0, v1

    #@28d6
    const/16 v1, 0x1db

    #@28d8
    new-array v2, v8, [Ljava/lang/String;

    #@28da
    const-string v3, "TMO"

    #@28dc
    aput-object v3, v2, v4

    #@28de
    const-string v3, "US"

    #@28e0
    aput-object v3, v2, v5

    #@28e2
    const-string v3, "310240"

    #@28e4
    aput-object v3, v2, v6

    #@28e6
    const-string v3, ""

    #@28e8
    aput-object v3, v2, v7

    #@28ea
    aput-object v2, v0, v1

    #@28ec
    const/16 v1, 0x1dc

    #@28ee
    new-array v2, v8, [Ljava/lang/String;

    #@28f0
    const-string v3, "TMO"

    #@28f2
    aput-object v3, v2, v4

    #@28f4
    const-string v3, "US"

    #@28f6
    aput-object v3, v2, v5

    #@28f8
    const-string v3, "310250"

    #@28fa
    aput-object v3, v2, v6

    #@28fc
    const-string v3, ""

    #@28fe
    aput-object v3, v2, v7

    #@2900
    aput-object v2, v0, v1

    #@2902
    const/16 v1, 0x1dd

    #@2904
    new-array v2, v8, [Ljava/lang/String;

    #@2906
    const-string v3, "TMO"

    #@2908
    aput-object v3, v2, v4

    #@290a
    const-string v3, "US"

    #@290c
    aput-object v3, v2, v5

    #@290e
    const-string v3, "310270"

    #@2910
    aput-object v3, v2, v6

    #@2912
    const-string v3, ""

    #@2914
    aput-object v3, v2, v7

    #@2916
    aput-object v2, v0, v1

    #@2918
    const/16 v1, 0x1de

    #@291a
    new-array v2, v8, [Ljava/lang/String;

    #@291c
    const-string v3, "TMO"

    #@291e
    aput-object v3, v2, v4

    #@2920
    const-string v3, "US"

    #@2922
    aput-object v3, v2, v5

    #@2924
    const-string v3, "310300"

    #@2926
    aput-object v3, v2, v6

    #@2928
    const-string v3, ""

    #@292a
    aput-object v3, v2, v7

    #@292c
    aput-object v2, v0, v1

    #@292e
    const/16 v1, 0x1df

    #@2930
    new-array v2, v8, [Ljava/lang/String;

    #@2932
    const-string v3, "TMO"

    #@2934
    aput-object v3, v2, v4

    #@2936
    const-string v3, "US"

    #@2938
    aput-object v3, v2, v5

    #@293a
    const-string v3, "310310"

    #@293c
    aput-object v3, v2, v6

    #@293e
    const-string v3, ""

    #@2940
    aput-object v3, v2, v7

    #@2942
    aput-object v2, v0, v1

    #@2944
    const/16 v1, 0x1e0

    #@2946
    new-array v2, v8, [Ljava/lang/String;

    #@2948
    const-string v3, "TMO"

    #@294a
    aput-object v3, v2, v4

    #@294c
    const-string v3, "US"

    #@294e
    aput-object v3, v2, v5

    #@2950
    const-string v3, "310490"

    #@2952
    aput-object v3, v2, v6

    #@2954
    const-string v3, ""

    #@2956
    aput-object v3, v2, v7

    #@2958
    aput-object v2, v0, v1

    #@295a
    const/16 v1, 0x1e1

    #@295c
    new-array v2, v8, [Ljava/lang/String;

    #@295e
    const-string v3, "TMO"

    #@2960
    aput-object v3, v2, v4

    #@2962
    const-string v3, "US"

    #@2964
    aput-object v3, v2, v5

    #@2966
    const-string v3, "310530"

    #@2968
    aput-object v3, v2, v6

    #@296a
    const-string v3, ""

    #@296c
    aput-object v3, v2, v7

    #@296e
    aput-object v2, v0, v1

    #@2970
    const/16 v1, 0x1e2

    #@2972
    new-array v2, v8, [Ljava/lang/String;

    #@2974
    const-string v3, "TMO"

    #@2976
    aput-object v3, v2, v4

    #@2978
    const-string v3, "US"

    #@297a
    aput-object v3, v2, v5

    #@297c
    const-string v3, "310580"

    #@297e
    aput-object v3, v2, v6

    #@2980
    const-string v3, ""

    #@2982
    aput-object v3, v2, v7

    #@2984
    aput-object v2, v0, v1

    #@2986
    const/16 v1, 0x1e3

    #@2988
    new-array v2, v8, [Ljava/lang/String;

    #@298a
    const-string v3, "TMO"

    #@298c
    aput-object v3, v2, v4

    #@298e
    const-string v3, "US"

    #@2990
    aput-object v3, v2, v5

    #@2992
    const-string v3, "310590"

    #@2994
    aput-object v3, v2, v6

    #@2996
    const-string v3, ""

    #@2998
    aput-object v3, v2, v7

    #@299a
    aput-object v2, v0, v1

    #@299c
    const/16 v1, 0x1e4

    #@299e
    new-array v2, v8, [Ljava/lang/String;

    #@29a0
    const-string v3, "TMO"

    #@29a2
    aput-object v3, v2, v4

    #@29a4
    const-string v3, "US"

    #@29a6
    aput-object v3, v2, v5

    #@29a8
    const-string v3, "310640"

    #@29aa
    aput-object v3, v2, v6

    #@29ac
    const-string v3, ""

    #@29ae
    aput-object v3, v2, v7

    #@29b0
    aput-object v2, v0, v1

    #@29b2
    const/16 v1, 0x1e5

    #@29b4
    new-array v2, v8, [Ljava/lang/String;

    #@29b6
    const-string v3, "TMO"

    #@29b8
    aput-object v3, v2, v4

    #@29ba
    const-string v3, "US"

    #@29bc
    aput-object v3, v2, v5

    #@29be
    const-string v3, "310660"

    #@29c0
    aput-object v3, v2, v6

    #@29c2
    const-string v3, ""

    #@29c4
    aput-object v3, v2, v7

    #@29c6
    aput-object v2, v0, v1

    #@29c8
    const/16 v1, 0x1e6

    #@29ca
    new-array v2, v8, [Ljava/lang/String;

    #@29cc
    const-string v3, "TMO"

    #@29ce
    aput-object v3, v2, v4

    #@29d0
    const-string v3, "US"

    #@29d2
    aput-object v3, v2, v5

    #@29d4
    const-string v3, "310800"

    #@29d6
    aput-object v3, v2, v6

    #@29d8
    const-string v3, ""

    #@29da
    aput-object v3, v2, v7

    #@29dc
    aput-object v2, v0, v1

    #@29de
    const/16 v1, 0x1e7

    #@29e0
    new-array v2, v8, [Ljava/lang/String;

    #@29e2
    const-string v3, "TMO"

    #@29e4
    aput-object v3, v2, v4

    #@29e6
    const-string v3, "US"

    #@29e8
    aput-object v3, v2, v5

    #@29ea
    const-string v3, "31026"

    #@29ec
    aput-object v3, v2, v6

    #@29ee
    const-string v3, ""

    #@29f0
    aput-object v3, v2, v7

    #@29f2
    aput-object v2, v0, v1

    #@29f4
    const/16 v1, 0x1e8

    #@29f6
    new-array v2, v8, [Ljava/lang/String;

    #@29f8
    const-string v3, "TMO"

    #@29fa
    aput-object v3, v2, v4

    #@29fc
    const-string v3, "US"

    #@29fe
    aput-object v3, v2, v5

    #@2a00
    const-string v3, "31021"

    #@2a02
    aput-object v3, v2, v6

    #@2a04
    const-string v3, ""

    #@2a06
    aput-object v3, v2, v7

    #@2a08
    aput-object v2, v0, v1

    #@2a0a
    const/16 v1, 0x1e9

    #@2a0c
    new-array v2, v8, [Ljava/lang/String;

    #@2a0e
    const-string v3, "TMO"

    #@2a10
    aput-object v3, v2, v4

    #@2a12
    const-string v3, "US"

    #@2a14
    aput-object v3, v2, v5

    #@2a16
    const-string v3, "45000"

    #@2a18
    aput-object v3, v2, v6

    #@2a1a
    const-string v3, ""

    #@2a1c
    aput-object v3, v2, v7

    #@2a1e
    aput-object v2, v0, v1

    #@2a20
    const/16 v1, 0x1ea

    #@2a22
    new-array v2, v8, [Ljava/lang/String;

    #@2a24
    const-string v3, "RGS"

    #@2a26
    aput-object v3, v2, v4

    #@2a28
    const-string v3, "CA"

    #@2a2a
    aput-object v3, v2, v5

    #@2a2c
    const-string v3, "302720"

    #@2a2e
    aput-object v3, v2, v6

    #@2a30
    const-string v3, "302720XXX"

    #@2a32
    aput-object v3, v2, v7

    #@2a34
    aput-object v2, v0, v1

    #@2a36
    const/16 v1, 0x1eb

    #@2a38
    new-array v2, v8, [Ljava/lang/String;

    #@2a3a
    const-string v3, "RGS"

    #@2a3c
    aput-object v3, v2, v4

    #@2a3e
    const-string v3, "CA"

    #@2a40
    aput-object v3, v2, v5

    #@2a42
    const-string v3, "302720"

    #@2a44
    aput-object v3, v2, v6

    #@2a46
    const-string v3, "302720X94"

    #@2a48
    aput-object v3, v2, v7

    #@2a4a
    aput-object v2, v0, v1

    #@2a4c
    const/16 v1, 0x1ec

    #@2a4e
    new-array v2, v8, [Ljava/lang/String;

    #@2a50
    const-string v3, "RGS"

    #@2a52
    aput-object v3, v2, v4

    #@2a54
    const-string v3, "CA"

    #@2a56
    aput-object v3, v2, v5

    #@2a58
    const-string v3, "302660"

    #@2a5a
    aput-object v3, v2, v6

    #@2a5c
    const-string v3, "2C"

    #@2a5e
    aput-object v3, v2, v7

    #@2a60
    aput-object v2, v0, v1

    #@2a62
    const/16 v1, 0x1ed

    #@2a64
    new-array v2, v8, [Ljava/lang/String;

    #@2a66
    const-string v3, "RGS"

    #@2a68
    aput-object v3, v2, v4

    #@2a6a
    const-string v3, "CA"

    #@2a6c
    aput-object v3, v2, v5

    #@2a6e
    const-string v3, "302370"

    #@2a70
    aput-object v3, v2, v6

    #@2a72
    const-string v3, "2C"

    #@2a74
    aput-object v3, v2, v7

    #@2a76
    aput-object v2, v0, v1

    #@2a78
    const/16 v1, 0x1ee

    #@2a7a
    new-array v2, v8, [Ljava/lang/String;

    #@2a7c
    const-string v3, "RGS"

    #@2a7e
    aput-object v3, v2, v4

    #@2a80
    const-string v3, "CA"

    #@2a82
    aput-object v3, v2, v5

    #@2a84
    const-string v3, "302370"

    #@2a86
    aput-object v3, v2, v6

    #@2a88
    const-string v3, "302370XXX"

    #@2a8a
    aput-object v3, v2, v7

    #@2a8c
    aput-object v2, v0, v1

    #@2a8e
    const/16 v1, 0x1ef

    #@2a90
    new-array v2, v8, [Ljava/lang/String;

    #@2a92
    const-string v3, "RGS"

    #@2a94
    aput-object v3, v2, v4

    #@2a96
    const-string v3, "CA"

    #@2a98
    aput-object v3, v2, v5

    #@2a9a
    const-string v3, "45000"

    #@2a9c
    aput-object v3, v2, v6

    #@2a9e
    const-string v3, ""

    #@2aa0
    aput-object v3, v2, v7

    #@2aa2
    aput-object v2, v0, v1

    #@2aa4
    const/16 v1, 0x1f0

    #@2aa6
    new-array v2, v8, [Ljava/lang/String;

    #@2aa8
    const-string v3, "BELL"

    #@2aaa
    aput-object v3, v2, v4

    #@2aac
    const-string v3, "CA"

    #@2aae
    aput-object v3, v2, v5

    #@2ab0
    const-string v3, "302610"

    #@2ab2
    aput-object v3, v2, v6

    #@2ab4
    const-string v3, "3D"

    #@2ab6
    aput-object v3, v2, v7

    #@2ab8
    aput-object v2, v0, v1

    #@2aba
    const/16 v1, 0x1f1

    #@2abc
    new-array v2, v8, [Ljava/lang/String;

    #@2abe
    const-string v3, "BELL"

    #@2ac0
    aput-object v3, v2, v4

    #@2ac2
    const-string v3, "CA"

    #@2ac4
    aput-object v3, v2, v5

    #@2ac6
    const-string v3, "302610"

    #@2ac8
    aput-object v3, v2, v6

    #@2aca
    const-string v3, "3E"

    #@2acc
    aput-object v3, v2, v7

    #@2ace
    aput-object v2, v0, v1

    #@2ad0
    const/16 v1, 0x1f2

    #@2ad2
    new-array v2, v8, [Ljava/lang/String;

    #@2ad4
    const-string v3, "BELL"

    #@2ad6
    aput-object v3, v2, v4

    #@2ad8
    const-string v3, "CA"

    #@2ada
    aput-object v3, v2, v5

    #@2adc
    const-string v3, "302610"

    #@2ade
    aput-object v3, v2, v6

    #@2ae0
    const-string v3, "3F"

    #@2ae2
    aput-object v3, v2, v7

    #@2ae4
    aput-object v2, v0, v1

    #@2ae6
    const/16 v1, 0x1f3

    #@2ae8
    new-array v2, v8, [Ljava/lang/String;

    #@2aea
    const-string v3, "BELL"

    #@2aec
    aput-object v3, v2, v4

    #@2aee
    const-string v3, "CA"

    #@2af0
    aput-object v3, v2, v5

    #@2af2
    const-string v3, "302610"

    #@2af4
    aput-object v3, v2, v6

    #@2af6
    const-string v3, "40"

    #@2af8
    aput-object v3, v2, v7

    #@2afa
    aput-object v2, v0, v1

    #@2afc
    const/16 v1, 0x1f4

    #@2afe
    new-array v2, v8, [Ljava/lang/String;

    #@2b00
    const-string v3, "BELL"

    #@2b02
    aput-object v3, v2, v4

    #@2b04
    const-string v3, "CA"

    #@2b06
    aput-object v3, v2, v5

    #@2b08
    const-string v3, "302780"

    #@2b0a
    aput-object v3, v2, v6

    #@2b0c
    const-string v3, "5A"

    #@2b0e
    aput-object v3, v2, v7

    #@2b10
    aput-object v2, v0, v1

    #@2b12
    const/16 v1, 0x1f5

    #@2b14
    new-array v2, v8, [Ljava/lang/String;

    #@2b16
    const-string v3, "BELL"

    #@2b18
    aput-object v3, v2, v4

    #@2b1a
    const-string v3, "CA"

    #@2b1c
    aput-object v3, v2, v5

    #@2b1e
    const-string v3, "20404"

    #@2b20
    aput-object v3, v2, v6

    #@2b22
    const-string v3, "5A"

    #@2b24
    aput-object v3, v2, v7

    #@2b26
    aput-object v2, v0, v1

    #@2b28
    const/16 v1, 0x1f6

    #@2b2a
    new-array v2, v8, [Ljava/lang/String;

    #@2b2c
    const-string v3, "BELL"

    #@2b2e
    aput-object v3, v2, v4

    #@2b30
    const-string v3, "CA"

    #@2b32
    aput-object v3, v2, v5

    #@2b34
    const-string v3, "45000"

    #@2b36
    aput-object v3, v2, v6

    #@2b38
    const-string v3, ""

    #@2b3a
    aput-object v3, v2, v7

    #@2b3c
    aput-object v2, v0, v1

    #@2b3e
    const/16 v1, 0x1f7

    #@2b40
    new-array v2, v8, [Ljava/lang/String;

    #@2b42
    const-string v3, "TLS"

    #@2b44
    aput-object v3, v2, v4

    #@2b46
    const-string v3, "CA"

    #@2b48
    aput-object v3, v2, v5

    #@2b4a
    const-string v3, "302220"

    #@2b4c
    aput-object v3, v2, v6

    #@2b4e
    const-string v3, "54"

    #@2b50
    aput-object v3, v2, v7

    #@2b52
    aput-object v2, v0, v1

    #@2b54
    const/16 v1, 0x1f8

    #@2b56
    new-array v2, v8, [Ljava/lang/String;

    #@2b58
    const-string v3, "TLS"

    #@2b5a
    aput-object v3, v2, v4

    #@2b5c
    const-string v3, "CA"

    #@2b5e
    aput-object v3, v2, v5

    #@2b60
    const-string v3, "302221"

    #@2b62
    aput-object v3, v2, v6

    #@2b64
    const-string v3, "54"

    #@2b66
    aput-object v3, v2, v7

    #@2b68
    aput-object v2, v0, v1

    #@2b6a
    const/16 v1, 0x1f9

    #@2b6c
    new-array v2, v8, [Ljava/lang/String;

    #@2b6e
    const-string v3, "TLS"

    #@2b70
    aput-object v3, v2, v4

    #@2b72
    const-string v3, "CA"

    #@2b74
    aput-object v3, v2, v5

    #@2b76
    const-string v3, "302220"

    #@2b78
    aput-object v3, v2, v6

    #@2b7a
    const-string v3, "4B"

    #@2b7c
    aput-object v3, v2, v7

    #@2b7e
    aput-object v2, v0, v1

    #@2b80
    const/16 v1, 0x1fa

    #@2b82
    new-array v2, v8, [Ljava/lang/String;

    #@2b84
    const-string v3, "TLS"

    #@2b86
    aput-object v3, v2, v4

    #@2b88
    const-string v3, "CA"

    #@2b8a
    aput-object v3, v2, v5

    #@2b8c
    const-string v3, "302221"

    #@2b8e
    aput-object v3, v2, v6

    #@2b90
    const-string v3, "4B"

    #@2b92
    aput-object v3, v2, v7

    #@2b94
    aput-object v2, v0, v1

    #@2b96
    const/16 v1, 0x1fb

    #@2b98
    new-array v2, v8, [Ljava/lang/String;

    #@2b9a
    const-string v3, "TLS"

    #@2b9c
    aput-object v3, v2, v4

    #@2b9e
    const-string v3, "CA"

    #@2ba0
    aput-object v3, v2, v5

    #@2ba2
    const-string v3, "302220"

    #@2ba4
    aput-object v3, v2, v6

    #@2ba6
    const-string v3, "50"

    #@2ba8
    aput-object v3, v2, v7

    #@2baa
    aput-object v2, v0, v1

    #@2bac
    const/16 v1, 0x1fc

    #@2bae
    new-array v2, v8, [Ljava/lang/String;

    #@2bb0
    const-string v3, "TLS"

    #@2bb2
    aput-object v3, v2, v4

    #@2bb4
    const-string v3, "CA"

    #@2bb6
    aput-object v3, v2, v5

    #@2bb8
    const-string v3, "302221"

    #@2bba
    aput-object v3, v2, v6

    #@2bbc
    const-string v3, "50"

    #@2bbe
    aput-object v3, v2, v7

    #@2bc0
    aput-object v2, v0, v1

    #@2bc2
    const/16 v1, 0x1fd

    #@2bc4
    new-array v2, v8, [Ljava/lang/String;

    #@2bc6
    const-string v3, "TLS"

    #@2bc8
    aput-object v3, v2, v4

    #@2bca
    const-string v3, "CA"

    #@2bcc
    aput-object v3, v2, v5

    #@2bce
    const-string v3, "302220"

    #@2bd0
    aput-object v3, v2, v6

    #@2bd2
    const-string v3, "43"

    #@2bd4
    aput-object v3, v2, v7

    #@2bd6
    aput-object v2, v0, v1

    #@2bd8
    const/16 v1, 0x1fe

    #@2bda
    new-array v2, v8, [Ljava/lang/String;

    #@2bdc
    const-string v3, "TLS"

    #@2bde
    aput-object v3, v2, v4

    #@2be0
    const-string v3, "CA"

    #@2be2
    aput-object v3, v2, v5

    #@2be4
    const-string v3, "45000"

    #@2be6
    aput-object v3, v2, v6

    #@2be8
    const-string v3, ""

    #@2bea
    aput-object v3, v2, v7

    #@2bec
    aput-object v2, v0, v1

    #@2bee
    const/16 v1, 0x1ff

    #@2bf0
    new-array v2, v8, [Ljava/lang/String;

    #@2bf2
    const-string v3, "VTR"

    #@2bf4
    aput-object v3, v2, v4

    #@2bf6
    const-string v3, "CA"

    #@2bf8
    aput-object v3, v2, v5

    #@2bfa
    const-string v3, "302500"

    #@2bfc
    aput-object v3, v2, v6

    #@2bfe
    const-string v3, ""

    #@2c00
    aput-object v3, v2, v7

    #@2c02
    aput-object v2, v0, v1

    #@2c04
    const/16 v1, 0x200

    #@2c06
    new-array v2, v8, [Ljava/lang/String;

    #@2c08
    const-string v3, "VTR"

    #@2c0a
    aput-object v3, v2, v4

    #@2c0c
    const-string v3, "CA"

    #@2c0e
    aput-object v3, v2, v5

    #@2c10
    const-string v3, "302510"

    #@2c12
    aput-object v3, v2, v6

    #@2c14
    const-string v3, ""

    #@2c16
    aput-object v3, v2, v7

    #@2c18
    aput-object v2, v0, v1

    #@2c1a
    const/16 v1, 0x201

    #@2c1c
    new-array v2, v8, [Ljava/lang/String;

    #@2c1e
    const-string v3, "VTR"

    #@2c20
    aput-object v3, v2, v4

    #@2c22
    const-string v3, "CA"

    #@2c24
    aput-object v3, v2, v5

    #@2c26
    const-string v3, "20404"

    #@2c28
    aput-object v3, v2, v6

    #@2c2a
    const-string v3, ""

    #@2c2c
    aput-object v3, v2, v7

    #@2c2e
    aput-object v2, v0, v1

    #@2c30
    const/16 v1, 0x202

    #@2c32
    new-array v2, v8, [Ljava/lang/String;

    #@2c34
    const-string v3, "VTR"

    #@2c36
    aput-object v3, v2, v4

    #@2c38
    const-string v3, "CA"

    #@2c3a
    aput-object v3, v2, v5

    #@2c3c
    const-string v3, "302490"

    #@2c3e
    aput-object v3, v2, v6

    #@2c40
    const-string v3, ""

    #@2c42
    aput-object v3, v2, v7

    #@2c44
    aput-object v2, v0, v1

    #@2c46
    const/16 v1, 0x203

    #@2c48
    new-array v2, v8, [Ljava/lang/String;

    #@2c4a
    const-string v3, "VTR"

    #@2c4c
    aput-object v3, v2, v4

    #@2c4e
    const-string v3, "CA"

    #@2c50
    aput-object v3, v2, v5

    #@2c52
    const-string v3, "22201"

    #@2c54
    aput-object v3, v2, v6

    #@2c56
    const-string v3, ""

    #@2c58
    aput-object v3, v2, v7

    #@2c5a
    aput-object v2, v0, v1

    #@2c5c
    const/16 v1, 0x204

    #@2c5e
    new-array v2, v8, [Ljava/lang/String;

    #@2c60
    const-string v3, "VTR"

    #@2c62
    aput-object v3, v2, v4

    #@2c64
    const-string v3, "CA"

    #@2c66
    aput-object v3, v2, v5

    #@2c68
    const-string v3, "22288"

    #@2c6a
    aput-object v3, v2, v6

    #@2c6c
    const-string v3, ""

    #@2c6e
    aput-object v3, v2, v7

    #@2c70
    aput-object v2, v0, v1

    #@2c72
    const/16 v1, 0x205

    #@2c74
    new-array v2, v8, [Ljava/lang/String;

    #@2c76
    const-string v3, "VTR"

    #@2c78
    aput-object v3, v2, v4

    #@2c7a
    const-string v3, "CA"

    #@2c7c
    aput-object v3, v2, v5

    #@2c7e
    const-string v3, "45000"

    #@2c80
    aput-object v3, v2, v6

    #@2c82
    const-string v3, ""

    #@2c84
    aput-object v3, v2, v7

    #@2c86
    aput-object v2, v0, v1

    #@2c88
    const/16 v1, 0x206

    #@2c8a
    new-array v2, v8, [Ljava/lang/String;

    #@2c8c
    const-string v3, "ATT"

    #@2c8e
    aput-object v3, v2, v4

    #@2c90
    const-string v3, "US"

    #@2c92
    aput-object v3, v2, v5

    #@2c94
    const-string v3, "310410"

    #@2c96
    aput-object v3, v2, v6

    #@2c98
    const-string v3, ""

    #@2c9a
    aput-object v3, v2, v7

    #@2c9c
    aput-object v2, v0, v1

    #@2c9e
    const/16 v1, 0x207

    #@2ca0
    new-array v2, v8, [Ljava/lang/String;

    #@2ca2
    const-string v3, "TRF"

    #@2ca4
    aput-object v3, v2, v4

    #@2ca6
    const-string v3, "US"

    #@2ca8
    aput-object v3, v2, v5

    #@2caa
    const-string v3, "310410"

    #@2cac
    aput-object v3, v2, v6

    #@2cae
    const-string v3, "DE"

    #@2cb0
    aput-object v3, v2, v7

    #@2cb2
    aput-object v2, v0, v1

    #@2cb4
    const/16 v1, 0x208

    #@2cb6
    new-array v2, v8, [Ljava/lang/String;

    #@2cb8
    const-string v3, "Default"

    #@2cba
    aput-object v3, v2, v4

    #@2cbc
    const-string v3, "Default"

    #@2cbe
    aput-object v3, v2, v5

    #@2cc0
    const-string v3, ""

    #@2cc2
    aput-object v3, v2, v6

    #@2cc4
    const-string v3, ""

    #@2cc6
    aput-object v3, v2, v7

    #@2cc8
    aput-object v2, v0, v1

    #@2cca
    sput-object v0, Landroid/provider/Telephony$Carriers;->GLOBAL_OPERATORS:[[Ljava/lang/String;

    #@2ccc
    .line 5055
    const/4 v0, 0x0

    #@2ccd
    sput-object v0, Landroid/provider/Telephony$Carriers;->AUTOPROFILE_FEATURE:Ljava/lang/String;

    #@2ccf
    .line 5056
    const/4 v0, 0x0

    #@2cd0
    sput-object v0, Landroid/provider/Telephony$Carriers;->OPERATOR_CODE:Ljava/lang/String;

    #@2cd2
    .line 5057
    const/4 v0, 0x0

    #@2cd3
    sput-object v0, Landroid/provider/Telephony$Carriers;->COUNTRY_CODE:Ljava/lang/String;

    #@2cd5
    .line 5059
    const/4 v0, 0x0

    #@2cd6
    sput-object v0, Landroid/provider/Telephony$Carriers;->OPERATOR_VERISON_NUMERIC:Ljava/lang/String;

    #@2cd8
    .line 5060
    const/4 v0, 0x0

    #@2cd9
    sput-object v0, Landroid/provider/Telephony$Carriers;->OPERATOR_VERISON_EXTRAID:Ljava/lang/String;

    #@2cdb
    .line 5063
    new-instance v0, Ljava/util/HashSet;

    #@2cdd
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@2ce0
    sput-object v0, Landroid/provider/Telephony$Carriers;->mNumericExtraIDSet:Ljava/util/HashSet;

    #@2ce2
    .line 5064
    sput-boolean v4, Landroid/provider/Telephony$Carriers;->isBuildOperatorVersionValues:Z

    #@2ce4
    .line 6776
    const/4 v0, 0x0

    #@2ce5
    sput-object v0, Landroid/provider/Telephony$Carriers;->multiSimMode:Ljava/lang/String;

    #@2ce7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 4166
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static addClonedApnKey(Landroid/content/Context;Ljava/lang/String;I)V
    .registers 10
    .parameter "context"
    .parameter "key"
    .parameter "sim_slot"

    #@0
    .prologue
    .line 6856
    const-string v4, "Telephony"

    #@2
    new-instance v5, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v6, "addClonedApnKey: sim_slot:"

    #@9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v5

    #@d
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v5

    #@11
    const-string v6, " key : "

    #@13
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v5

    #@17
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v5

    #@1b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v5

    #@1f
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 6858
    const-string v1, "cloned-apn"

    #@24
    .line 6859
    .local v1, file:Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    const-string v5, ";"

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    invoke-static {p0, p2}, Landroid/provider/Telephony$Carriers;->getClonedApnKey(Landroid/content/Context;I)Ljava/lang/String;

    #@36
    move-result-object v5

    #@37
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v2

    #@3f
    .line 6860
    .local v2, keylist:Ljava/lang/String;
    const/4 v4, 0x2

    #@40
    if-ne p2, v4, :cond_44

    #@42
    .line 6861
    const-string v1, "cloned-apn-2"

    #@44
    .line 6863
    :cond_44
    const/4 v4, 0x0

    #@45
    invoke-virtual {p0, v1, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@48
    move-result-object v3

    #@49
    .line 6865
    .local v3, sp:Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@4c
    move-result-object v0

    #@4d
    .line 6866
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v4, "apn_key"

    #@4f
    invoke-interface {v0, v4, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@52
    .line 6867
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    #@55
    .line 6868
    return-void
.end method

.method private static buildOperatorVersionValues()Z
    .registers 11

    #@0
    .prologue
    const/4 v10, 0x3

    #@1
    const/4 v9, 0x2

    #@2
    const/4 v5, 0x0

    #@3
    const/4 v4, 0x1

    #@4
    .line 5068
    sget-boolean v6, Landroid/provider/Telephony$Carriers;->isBuildOperatorVersionValues:Z

    #@6
    if-eqz v6, :cond_9

    #@8
    .line 5112
    .local v1, arr$:[[Ljava/lang/String;
    .local v2, i$:I
    .local v3, len$:I
    :goto_8
    return v4

    #@9
    .line 5072
    .end local v1           #arr$:[[Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :cond_9
    invoke-static {}, Landroid/provider/Telephony$Carriers;->getOperatorCode()Ljava/lang/String;

    #@c
    move-result-object v6

    #@d
    if-eqz v6, :cond_15

    #@f
    invoke-static {}, Landroid/provider/Telephony$Carriers;->getCountryCode()Ljava/lang/String;

    #@12
    move-result-object v6

    #@13
    if-nez v6, :cond_17

    #@15
    :cond_15
    move v4, v5

    #@16
    .line 5073
    goto :goto_8

    #@17
    .line 5076
    :cond_17
    sget-object v1, Landroid/provider/Telephony$Carriers;->GLOBAL_OPERATORS:[[Ljava/lang/String;

    #@19
    .restart local v1       #arr$:[[Ljava/lang/String;
    array-length v3, v1

    #@1a
    .restart local v3       #len$:I
    const/4 v2, 0x0

    #@1b
    .restart local v2       #i$:I
    :goto_1b
    if-ge v2, v3, :cond_b5

    #@1d
    aget-object v0, v1, v2

    #@1f
    .line 5078
    .local v0, Op:[Ljava/lang/String;
    array-length v6, v0

    #@20
    const/4 v7, 0x4

    #@21
    if-ne v6, v7, :cond_ad

    #@23
    aget-object v6, v0, v5

    #@25
    if-eqz v6, :cond_ad

    #@27
    aget-object v6, v0, v4

    #@29
    if-eqz v6, :cond_ad

    #@2b
    aget-object v6, v0, v9

    #@2d
    if-eqz v6, :cond_ad

    #@2f
    aget-object v6, v0, v10

    #@31
    if-eqz v6, :cond_ad

    #@33
    .line 5081
    aget-object v6, v0, v5

    #@35
    invoke-static {}, Landroid/provider/Telephony$Carriers;->getOperatorCode()Ljava/lang/String;

    #@38
    move-result-object v7

    #@39
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3c
    move-result v6

    #@3d
    if-eqz v6, :cond_a9

    #@3f
    aget-object v6, v0, v4

    #@41
    invoke-static {}, Landroid/provider/Telephony$Carriers;->getCountryCode()Ljava/lang/String;

    #@44
    move-result-object v7

    #@45
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@48
    move-result v6

    #@49
    if-eqz v6, :cond_a9

    #@4b
    .line 5084
    sget-object v6, Landroid/provider/Telephony$Carriers;->mNumericExtraIDSet:Ljava/util/HashSet;

    #@4d
    new-instance v7, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    aget-object v8, v0, v9

    #@54
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v7

    #@58
    const-string v8, ";"

    #@5a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v7

    #@5e
    aget-object v8, v0, v10

    #@60
    invoke-virtual {v8}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@63
    move-result-object v8

    #@64
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v7

    #@68
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v7

    #@6c
    invoke-virtual {v6, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@6f
    .line 5087
    sget-object v6, Landroid/provider/Telephony$Carriers;->OPERATOR_VERISON_NUMERIC:Ljava/lang/String;

    #@71
    if-nez v6, :cond_77

    #@73
    .line 5088
    aget-object v6, v0, v9

    #@75
    sput-object v6, Landroid/provider/Telephony$Carriers;->OPERATOR_VERISON_NUMERIC:Ljava/lang/String;

    #@77
    .line 5090
    :cond_77
    sget-object v6, Landroid/provider/Telephony$Carriers;->OPERATOR_VERISON_EXTRAID:Ljava/lang/String;

    #@79
    if-nez v6, :cond_83

    #@7b
    .line 5091
    aget-object v6, v0, v10

    #@7d
    invoke-virtual {v6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@80
    move-result-object v6

    #@81
    sput-object v6, Landroid/provider/Telephony$Carriers;->OPERATOR_VERISON_EXTRAID:Ljava/lang/String;

    #@83
    .line 5094
    :cond_83
    const-string v6, "Telephony"

    #@85
    new-instance v7, Ljava/lang/StringBuilder;

    #@87
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@8a
    const-string v8, "Found Operator_Version Numeric and Extra ID "

    #@8c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v7

    #@90
    aget-object v8, v0, v9

    #@92
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v7

    #@96
    const-string v8, "/"

    #@98
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v7

    #@9c
    aget-object v8, v0, v10

    #@9e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v7

    #@a2
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a5
    move-result-object v7

    #@a6
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a9
    .line 5076
    :cond_a9
    :goto_a9
    add-int/lit8 v2, v2, 0x1

    #@ab
    goto/16 :goto_1b

    #@ad
    .line 5101
    :cond_ad
    const-string v6, "Telephony"

    #@af
    const-string v7, "There are invaild Operator in GLOBAL_OPERATORS"

    #@b1
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b4
    goto :goto_a9

    #@b5
    .line 5105
    .end local v0           #Op:[Ljava/lang/String;
    :cond_b5
    sget-object v6, Landroid/provider/Telephony$Carriers;->OPERATOR_VERISON_NUMERIC:Ljava/lang/String;

    #@b7
    if-eqz v6, :cond_bd

    #@b9
    sget-object v6, Landroid/provider/Telephony$Carriers;->OPERATOR_VERISON_EXTRAID:Ljava/lang/String;

    #@bb
    if-nez v6, :cond_c0

    #@bd
    :cond_bd
    move v4, v5

    #@be
    .line 5107
    goto/16 :goto_8

    #@c0
    .line 5110
    :cond_c0
    sput-boolean v4, Landroid/provider/Telephony$Carriers;->isBuildOperatorVersionValues:Z

    #@c2
    goto/16 :goto_8
.end method

.method public static clearClonedApnKey(Landroid/content/Context;I)V
    .registers 8
    .parameter "context"
    .parameter "sim_slot"

    #@0
    .prologue
    .line 6871
    const-string v3, "Telephony"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "clearClonedApnKey sim_slot:"

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 6873
    const-string v1, "cloned-apn"

    #@1a
    .line 6874
    .local v1, file:Ljava/lang/String;
    const/4 v3, 0x2

    #@1b
    if-ne p1, v3, :cond_1f

    #@1d
    .line 6875
    const-string v1, "cloned-apn-2"

    #@1f
    .line 6877
    :cond_1f
    const/4 v3, 0x0

    #@20
    invoke-virtual {p0, v1, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@23
    move-result-object v2

    #@24
    .line 6879
    .local v2, sp:Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@27
    move-result-object v0

    #@28
    .line 6880
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    #@2b
    .line 6881
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    #@2e
    .line 6882
    return-void
.end method

.method public static cloneApn(Landroid/content/Context;I)V
    .registers 13
    .parameter "context"
    .parameter "sim_slot"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 6798
    add-int/lit8 v0, p1, -0x1

    #@3
    invoke-static {v0}, Landroid/provider/Telephony$Carriers;->getNumeric(I)Ljava/lang/String;

    #@6
    move-result-object v10

    #@7
    .line 6799
    .local v10, operator:Ljava/lang/String;
    const-string v0, "Telephony"

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v4, "cloneApn for operator = ["

    #@10
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    const-string v4, "]"

    #@1a
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 6801
    if-eqz v10, :cond_dc

    #@27
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    #@2a
    move-result v0

    #@2b
    if-lez v0, :cond_dc

    #@2d
    if-lez p1, :cond_dc

    #@2f
    .line 6802
    add-int/lit8 v0, p1, -0x1

    #@31
    invoke-static {v10, v0}, Landroid/provider/Telephony$Carriers;->getAutoProfileKey(Ljava/lang/String;I)Ljava/lang/String;

    #@34
    move-result-object v6

    #@35
    .line 6803
    .local v6, autoprofileKey:Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v1, "numeric = \'"

    #@3c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v0

    #@40
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v0

    #@44
    const-string v1, "\' and "

    #@46
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v0

    #@4a
    const-string v1, "extraid = \'"

    #@4c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v0

    #@50
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v0

    #@54
    const-string v1, "\'"

    #@56
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v0

    #@5a
    const-string v1, " and "

    #@5c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v0

    #@60
    const-string v1, "sim_slot = \'0\'"

    #@62
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v0

    #@66
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v3

    #@6a
    .line 6804
    .local v3, selection:Ljava/lang/String;
    invoke-static {p0, p1}, Landroid/provider/Telephony$Carriers;->getClonedApnKey(Landroid/content/Context;I)Ljava/lang/String;

    #@6d
    move-result-object v7

    #@6e
    .line 6805
    .local v7, clonedApn:Ljava/lang/String;
    const/4 v9, 0x0

    #@6f
    .line 6809
    .local v9, map:Landroid/content/ContentValues;
    new-instance v0, Ljava/lang/StringBuilder;

    #@71
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@74
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v0

    #@78
    const-string v1, " and carrier_enabled = 1"

    #@7a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v0

    #@7e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81
    move-result-object v3

    #@82
    .line 6810
    const-string v0, "Telephony"

    #@84
    new-instance v1, Ljava/lang/StringBuilder;

    #@86
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@89
    const-string v4, "cloneApn: selection="

    #@8b
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v1

    #@8f
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v1

    #@93
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@96
    move-result-object v1

    #@97
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@9a
    .line 6811
    const-string v0, "Telephony"

    #@9c
    new-instance v1, Ljava/lang/StringBuilder;

    #@9e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a1
    const-string v4, "cloneApn: existing clonedApn list ="

    #@a3
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v1

    #@a7
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v1

    #@ab
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ae
    move-result-object v1

    #@af
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@b2
    .line 6813
    new-instance v0, Ljava/lang/StringBuilder;

    #@b4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b7
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v0

    #@bb
    const-string v1, ","

    #@bd
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v0

    #@c1
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v0

    #@c5
    const-string v1, ";"

    #@c7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v0

    #@cb
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ce
    move-result-object v0

    #@cf
    invoke-virtual {v7, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@d2
    move-result v0

    #@d3
    if-eqz v0, :cond_dd

    #@d5
    .line 6814
    const-string v0, "Telephony"

    #@d7
    const-string v1, "cloneApn: aleady cloned"

    #@d9
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@dc
    .line 6842
    .end local v3           #selection:Ljava/lang/String;
    .end local v6           #autoprofileKey:Ljava/lang/String;
    .end local v7           #clonedApn:Ljava/lang/String;
    .end local v9           #map:Landroid/content/ContentValues;
    :cond_dc
    :goto_dc
    return-void

    #@dd
    .line 6818
    .restart local v3       #selection:Ljava/lang/String;
    .restart local v6       #autoprofileKey:Ljava/lang/String;
    .restart local v7       #clonedApn:Ljava/lang/String;
    .restart local v9       #map:Landroid/content/ContentValues;
    :cond_dd
    const/4 v8, 0x0

    #@de
    .line 6819
    .local v8, cursor:Landroid/database/Cursor;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@e1
    move-result-object v0

    #@e2
    sget-object v1, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    #@e4
    move-object v4, v2

    #@e5
    move-object v5, v2

    #@e6
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@e9
    move-result-object v8

    #@ea
    .line 6823
    if-eqz v8, :cond_108

    #@ec
    const-string v0, "Telephony"

    #@ee
    new-instance v1, Ljava/lang/StringBuilder;

    #@f0
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f3
    const-string v2, "cloneApn : cursor count is "

    #@f5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v1

    #@f9
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    #@fc
    move-result v2

    #@fd
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@100
    move-result-object v1

    #@101
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@104
    move-result-object v1

    #@105
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@108
    .line 6826
    :cond_108
    if-eqz v8, :cond_dc

    #@10a
    .line 6827
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    #@10d
    move-result v0

    #@10e
    if-lez v0, :cond_14e

    #@110
    .line 6828
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@113
    move-result v0

    #@114
    if-eqz v0, :cond_14e

    #@116
    .line 6829
    new-instance v0, Ljava/lang/StringBuilder;

    #@118
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@11b
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11e
    move-result-object v0

    #@11f
    const-string v1, ","

    #@121
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@124
    move-result-object v0

    #@125
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@128
    move-result-object v0

    #@129
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12c
    move-result-object v0

    #@12d
    invoke-static {p0, v0, p1}, Landroid/provider/Telephony$Carriers;->addClonedApnKey(Landroid/content/Context;Ljava/lang/String;I)V

    #@130
    .line 6831
    :cond_130
    invoke-static {v8}, Landroid/provider/Telephony$Carriers;->makeApnContentValues(Landroid/database/Cursor;)Landroid/content/ContentValues;

    #@133
    move-result-object v9

    #@134
    .line 6832
    if-eqz v9, :cond_148

    #@136
    .line 6833
    const-string v0, "sim_slot"

    #@138
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13b
    move-result-object v1

    #@13c
    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@13f
    .line 6834
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@142
    move-result-object v0

    #@143
    sget-object v1, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    #@145
    invoke-virtual {v0, v1, v9}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@148
    .line 6836
    :cond_148
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    #@14b
    move-result v0

    #@14c
    if-nez v0, :cond_130

    #@14e
    .line 6839
    :cond_14e
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@151
    goto :goto_dc
.end method

.method public static getAutoProfileKey(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "numeric"

    #@0
    .prologue
    .line 5645
    const-string v3, "Telephony"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "getAutoProfileKey: "

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 5647
    const-string v3, "gsm.sim.operator.gid"

    #@1a
    const-string v4, ""

    #@1c
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    .line 5648
    .local v0, simGid:Ljava/lang/String;
    const-string v3, "gsm.sim.operator.alpha"

    #@22
    const-string v4, ""

    #@24
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    .line 5649
    .local v2, simSPN:Ljava/lang/String;
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@2b
    move-result-object v3

    #@2c
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    .line 5651
    .local v1, simIMSI:Ljava/lang/String;
    invoke-static {p0, v0, v2, v1}, Landroid/provider/Telephony$Carriers;->getAutoProfileKey(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@33
    move-result-object v3

    #@34
    return-object v3
.end method

.method public static getAutoProfileKey(Ljava/lang/String;I)Ljava/lang/String;
    .registers 9
    .parameter "numeric"
    .parameter "subs"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 5656
    const-string v3, "Telephony"

    #@3
    new-instance v4, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v5, "getAutoProfileKey: "

    #@a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v4

    #@e
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v4

    #@12
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v4

    #@16
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 5657
    const-string v3, "Telephony"

    #@1b
    new-instance v4, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v5, "getAutoProfileKey_mSub: "

    #@22
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v4

    #@26
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v4

    #@2a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v4

    #@2e
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 5659
    const-string v0, ""

    #@33
    .line 5660
    .local v0, simGid:Ljava/lang/String;
    const-string v2, ""

    #@35
    .line 5661
    .local v2, simSPN:Ljava/lang/String;
    const-string v1, ""

    #@37
    .line 5681
    .local v1, simIMSI:Ljava/lang/String;
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@3a
    const-string v3, "gsm.sim.operator.gid"

    #@3c
    invoke-static {v3, p1, v6}, Landroid/telephony/MSimTelephonyManager;->getTelephonyProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    #@3f
    move-result-object v0

    #@40
    .line 5682
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@43
    const-string v3, "gsm.sim.operator.alpha"

    #@45
    invoke-static {v3, p1, v6}, Landroid/telephony/MSimTelephonyManager;->getTelephonyProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    #@48
    move-result-object v2

    #@49
    .line 5683
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@4c
    move-result-object v3

    #@4d
    invoke-virtual {v3, p1}, Landroid/telephony/MSimTelephonyManager;->getSubscriberId(I)Ljava/lang/String;

    #@50
    move-result-object v1

    #@51
    .line 5686
    invoke-static {p0, v0, v2, v1}, Landroid/provider/Telephony$Carriers;->getAutoProfileKey(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@54
    move-result-object v3

    #@55
    return-object v3
.end method

.method public static getAutoProfileKey(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 16
    .parameter "numeric"
    .parameter "simGid"
    .parameter "simSPN"
    .parameter "simIMSI"

    #@0
    .prologue
    const/16 v11, 0x9

    #@2
    const/4 v10, 0x6

    #@3
    const/16 v9, 0x8

    #@5
    const/4 v8, 0x0

    #@6
    const/4 v7, 0x7

    #@7
    .line 5691
    const-string v3, ""

    #@9
    .line 5692
    .local v3, retCode:Ljava/lang/String;
    const-string v0, ""

    #@b
    .local v0, operatorGid:Ljava/lang/String;
    const-string v2, ""

    #@d
    .local v2, operatorSPN:Ljava/lang/String;
    const-string v1, ""

    #@f
    .line 5694
    .local v1, operatorIMSI:Ljava/lang/String;
    if-nez p0, :cond_13

    #@11
    move-object v4, v3

    #@12
    .line 6035
    .end local v3           #retCode:Ljava/lang/String;
    .local v4, retCode:Ljava/lang/String;
    :goto_12
    return-object v4

    #@13
    .line 5697
    .end local v4           #retCode:Ljava/lang/String;
    .restart local v3       #retCode:Ljava/lang/String;
    :cond_13
    const-string v5, "45000"

    #@15
    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v5

    #@19
    if-eqz v5, :cond_1d

    #@1b
    move-object v4, v3

    #@1c
    .end local v3           #retCode:Ljava/lang/String;
    .restart local v4       #retCode:Ljava/lang/String;
    goto :goto_12

    #@1d
    .line 5701
    .end local v4           #retCode:Ljava/lang/String;
    .restart local v3       #retCode:Ljava/lang/String;
    :cond_1d
    move-object v0, p1

    #@1e
    .line 5702
    move-object v2, p2

    #@1f
    .line 5703
    move-object v1, p3

    #@20
    .line 5705
    invoke-static {p0, v0}, Landroid/provider/Telephony$Carriers;->isGidBasedOperator(Ljava/lang/String;Ljava/lang/String;)Z

    #@23
    move-result v5

    #@24
    if-eqz v5, :cond_6f

    #@26
    .line 5707
    const-string v5, "Telephony"

    #@28
    new-instance v6, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v6

    #@31
    const-string v7, "is GidBasedOperator, Gid Value : "

    #@33
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v6

    #@37
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v6

    #@3b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v6

    #@3f
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 5710
    if-eqz v0, :cond_54

    #@44
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@47
    move-result v5

    #@48
    const/4 v6, 0x2

    #@49
    if-lt v5, v6, :cond_54

    #@4b
    .line 5712
    const/4 v5, 0x2

    #@4c
    invoke-virtual {v0, v8, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@4f
    move-result-object v0

    #@50
    .line 5713
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@53
    move-result-object v0

    #@54
    .line 5716
    :cond_54
    const-string v5, "Telephony"

    #@56
    new-instance v6, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v7, "Selected GID Value : "

    #@5d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v6

    #@61
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v6

    #@65
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v6

    #@69
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@6c
    .line 5717
    move-object v3, v0

    #@6d
    :cond_6d
    :goto_6d
    move-object v4, v3

    #@6e
    .line 6035
    .end local v3           #retCode:Ljava/lang/String;
    .restart local v4       #retCode:Ljava/lang/String;
    goto :goto_12

    #@6f
    .line 5719
    .end local v4           #retCode:Ljava/lang/String;
    .restart local v3       #retCode:Ljava/lang/String;
    :cond_6f
    invoke-static {p0, v2}, Landroid/provider/Telephony$Carriers;->isSPNBasedOperator(Ljava/lang/String;Ljava/lang/String;)Z

    #@72
    move-result v5

    #@73
    if-eqz v5, :cond_b7

    #@75
    .line 5721
    const-string v5, "Telephony"

    #@77
    new-instance v6, Ljava/lang/StringBuilder;

    #@79
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@7c
    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v6

    #@80
    const-string v7, "is SPNBasedOperator, SPN Value : "

    #@82
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v6

    #@86
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v6

    #@8a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8d
    move-result-object v6

    #@8e
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@91
    .line 5724
    if-eqz v2, :cond_9d

    #@93
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@96
    move-result v5

    #@97
    if-lez v5, :cond_9d

    #@99
    .line 5725
    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@9c
    move-result-object v2

    #@9d
    .line 5728
    :cond_9d
    const-string v5, "Telephony"

    #@9f
    new-instance v6, Ljava/lang/StringBuilder;

    #@a1
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@a4
    const-string v7, "Selected SPN Value : "

    #@a6
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v6

    #@aa
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v6

    #@ae
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b1
    move-result-object v6

    #@b2
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@b5
    .line 5729
    move-object v3, v2

    #@b6
    goto :goto_6d

    #@b7
    .line 5731
    :cond_b7
    invoke-static {p0}, Landroid/provider/Telephony$Carriers;->isIMSIBasedOperator(Ljava/lang/String;)Z

    #@ba
    move-result v5

    #@bb
    if-eqz v5, :cond_478

    #@bd
    .line 5738
    if-eqz v1, :cond_105

    #@bf
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@c2
    move-result v5

    #@c3
    if-lt v5, v11, :cond_105

    #@c5
    const-string v5, "22802"

    #@c7
    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ca
    move-result v5

    #@cb
    if-eqz v5, :cond_105

    #@cd
    .line 5742
    invoke-virtual {v1, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@d0
    move-result-object v5

    #@d1
    const-string v6, "43"

    #@d3
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d6
    move-result v5

    #@d7
    if-nez v5, :cond_e5

    #@d9
    invoke-virtual {v1, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@dc
    move-result-object v5

    #@dd
    const-string v6, "45"

    #@df
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e2
    move-result v5

    #@e3
    if-eqz v5, :cond_102

    #@e5
    .line 5744
    :cond_e5
    const-string v1, "228024X45"

    #@e7
    .line 5996
    :cond_e7
    :goto_e7
    const-string v5, "Telephony"

    #@e9
    new-instance v6, Ljava/lang/StringBuilder;

    #@eb
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@ee
    const-string v7, "Selected IMSI Value : "

    #@f0
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v6

    #@f4
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v6

    #@f8
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fb
    move-result-object v6

    #@fc
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@ff
    .line 5997
    move-object v3, v1

    #@100
    goto/16 :goto_6d

    #@102
    .line 5746
    :cond_102
    const-string v1, ""

    #@104
    goto :goto_e7

    #@105
    .line 5750
    :cond_105
    if-eqz v1, :cond_127

    #@107
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@10a
    move-result v5

    #@10b
    if-lt v5, v11, :cond_127

    #@10d
    const-string v5, "29402"

    #@10f
    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@112
    move-result v5

    #@113
    if-eqz v5, :cond_127

    #@115
    .line 5754
    invoke-virtual {v1, v8, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@118
    move-result-object v5

    #@119
    const-string v6, "294020001"

    #@11b
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11e
    move-result v5

    #@11f
    if-eqz v5, :cond_124

    #@121
    .line 5755
    const-string v1, "294020001"

    #@123
    goto :goto_e7

    #@124
    .line 5758
    :cond_124
    const-string v1, ""

    #@126
    goto :goto_e7

    #@127
    .line 5762
    :cond_127
    if-eqz v1, :cond_149

    #@129
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@12c
    move-result v5

    #@12d
    if-lt v5, v9, :cond_149

    #@12f
    const-string v5, "21670"

    #@131
    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@134
    move-result v5

    #@135
    if-eqz v5, :cond_149

    #@137
    .line 5766
    invoke-virtual {v1, v7, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@13a
    move-result-object v5

    #@13b
    const-string v6, "2"

    #@13d
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@140
    move-result v5

    #@141
    if-eqz v5, :cond_146

    #@143
    .line 5767
    const-string v1, "21670XX2"

    #@145
    goto :goto_e7

    #@146
    .line 5770
    :cond_146
    const-string v1, ""

    #@148
    goto :goto_e7

    #@149
    .line 5774
    :cond_149
    if-eqz v1, :cond_16c

    #@14b
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@14e
    move-result v5

    #@14f
    if-lt v5, v7, :cond_16c

    #@151
    const-string v5, "21403"

    #@153
    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@156
    move-result v5

    #@157
    if-eqz v5, :cond_16c

    #@159
    .line 5777
    invoke-virtual {v1, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@15c
    move-result-object v5

    #@15d
    const-string v6, "2140359"

    #@15f
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@162
    move-result v5

    #@163
    if-eqz v5, :cond_168

    #@165
    .line 5778
    const-string v1, "2140359"

    #@167
    goto :goto_e7

    #@168
    .line 5780
    :cond_168
    const-string v1, ""

    #@16a
    goto/16 :goto_e7

    #@16c
    .line 5784
    :cond_16c
    if-eqz v1, :cond_1b0

    #@16e
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@171
    move-result v5

    #@172
    if-lt v5, v7, :cond_1b0

    #@174
    const-string v5, "21406"

    #@176
    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@179
    move-result v5

    #@17a
    if-eqz v5, :cond_1b0

    #@17c
    .line 5788
    invoke-virtual {v1, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@17f
    move-result-object v5

    #@180
    const-string v6, "2140613"

    #@182
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@185
    move-result v5

    #@186
    if-eqz v5, :cond_18c

    #@188
    .line 5789
    const-string v1, "2140613"

    #@18a
    goto/16 :goto_e7

    #@18c
    .line 5792
    :cond_18c
    invoke-virtual {v1, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@18f
    move-result-object v5

    #@190
    const-string v6, "2140606"

    #@192
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@195
    move-result v5

    #@196
    if-eqz v5, :cond_19c

    #@198
    .line 5793
    const-string v1, "2140606"

    #@19a
    goto/16 :goto_e7

    #@19c
    .line 5796
    :cond_19c
    invoke-virtual {v1, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@19f
    move-result-object v5

    #@1a0
    const-string v6, "2140612"

    #@1a2
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a5
    move-result v5

    #@1a6
    if-eqz v5, :cond_1ac

    #@1a8
    .line 5797
    const-string v1, "2140612"

    #@1aa
    goto/16 :goto_e7

    #@1ac
    .line 5801
    :cond_1ac
    const-string v1, "00"

    #@1ae
    goto/16 :goto_e7

    #@1b0
    .line 5805
    :cond_1b0
    if-eqz v1, :cond_1d4

    #@1b2
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@1b5
    move-result v5

    #@1b6
    if-lt v5, v10, :cond_1d4

    #@1b8
    const-string v5, "24001"

    #@1ba
    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1bd
    move-result v5

    #@1be
    if-eqz v5, :cond_1d4

    #@1c0
    .line 5809
    invoke-virtual {v1, v8, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1c3
    move-result-object v5

    #@1c4
    const-string v6, "240017"

    #@1c6
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c9
    move-result v5

    #@1ca
    if-eqz v5, :cond_1d0

    #@1cc
    .line 5810
    const-string v1, "240017"

    #@1ce
    goto/16 :goto_e7

    #@1d0
    .line 5813
    :cond_1d0
    const-string v1, ""

    #@1d2
    goto/16 :goto_e7

    #@1d4
    .line 5817
    :cond_1d4
    if-eqz v1, :cond_204

    #@1d6
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@1d9
    move-result v5

    #@1da
    if-lt v5, v7, :cond_204

    #@1dc
    const-string v5, "23820"

    #@1de
    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e1
    move-result v5

    #@1e2
    if-eqz v5, :cond_204

    #@1e4
    .line 5821
    invoke-virtual {v1, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1e7
    move-result-object v5

    #@1e8
    const-string v6, "2382010"

    #@1ea
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1ed
    move-result v5

    #@1ee
    if-nez v5, :cond_1fc

    #@1f0
    invoke-virtual {v1, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1f3
    move-result-object v5

    #@1f4
    const-string v6, "2382030"

    #@1f6
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f9
    move-result v5

    #@1fa
    if-eqz v5, :cond_200

    #@1fc
    .line 5823
    :cond_1fc
    const-string v1, "2382010_2382030"

    #@1fe
    goto/16 :goto_e7

    #@200
    .line 5826
    :cond_200
    const-string v1, ""

    #@202
    goto/16 :goto_e7

    #@204
    .line 5830
    :cond_204
    if-eqz v1, :cond_228

    #@206
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@209
    move-result v5

    #@20a
    if-lt v5, v7, :cond_228

    #@20c
    const-string v5, "24202"

    #@20e
    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@211
    move-result v5

    #@212
    if-eqz v5, :cond_228

    #@214
    .line 5834
    invoke-virtual {v1, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@217
    move-result-object v5

    #@218
    const-string v6, "2420256"

    #@21a
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21d
    move-result v5

    #@21e
    if-eqz v5, :cond_224

    #@220
    .line 5835
    const-string v1, "2420256XX"

    #@222
    goto/16 :goto_e7

    #@224
    .line 5838
    :cond_224
    const-string v1, ""

    #@226
    goto/16 :goto_e7

    #@228
    .line 5842
    :cond_228
    if-eqz v1, :cond_278

    #@22a
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@22d
    move-result v5

    #@22e
    if-lt v5, v7, :cond_278

    #@230
    const-string v5, "24007"

    #@232
    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@235
    move-result v5

    #@236
    if-eqz v5, :cond_278

    #@238
    .line 5846
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@23b
    move-result v5

    #@23c
    if-lt v5, v9, :cond_24e

    #@23e
    invoke-virtual {v1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@241
    move-result-object v5

    #@242
    const-string v6, "24007695"

    #@244
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@247
    move-result v5

    #@248
    if-eqz v5, :cond_24e

    #@24a
    .line 5848
    const-string v1, "24007695_24007696"

    #@24c
    goto/16 :goto_e7

    #@24e
    .line 5849
    :cond_24e
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@251
    move-result v5

    #@252
    if-lt v5, v9, :cond_264

    #@254
    invoke-virtual {v1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@257
    move-result-object v5

    #@258
    const-string v6, "24007696"

    #@25a
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25d
    move-result v5

    #@25e
    if-eqz v5, :cond_264

    #@260
    .line 5851
    const-string v1, "24007695_24007696"

    #@262
    goto/16 :goto_e7

    #@264
    .line 5852
    :cond_264
    invoke-virtual {v1, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@267
    move-result-object v5

    #@268
    const-string v6, "2400768"

    #@26a
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26d
    move-result v5

    #@26e
    if-eqz v5, :cond_274

    #@270
    .line 5853
    const-string v1, "2400768"

    #@272
    goto/16 :goto_e7

    #@274
    .line 5856
    :cond_274
    const-string v1, ""

    #@276
    goto/16 :goto_e7

    #@278
    .line 5860
    :cond_278
    if-eqz v1, :cond_29c

    #@27a
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@27d
    move-result v5

    #@27e
    if-lt v5, v9, :cond_29c

    #@280
    const-string v5, "24201"

    #@282
    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@285
    move-result v5

    #@286
    if-eqz v5, :cond_29c

    #@288
    .line 5864
    invoke-virtual {v1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@28b
    move-result-object v5

    #@28c
    const-string v6, "24201700"

    #@28e
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@291
    move-result v5

    #@292
    if-eqz v5, :cond_298

    #@294
    .line 5865
    const-string v1, "24201700"

    #@296
    goto/16 :goto_e7

    #@298
    .line 5868
    :cond_298
    const-string v1, ""

    #@29a
    goto/16 :goto_e7

    #@29c
    .line 5872
    :cond_29c
    if-eqz v1, :cond_2c0

    #@29e
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@2a1
    move-result v5

    #@2a2
    if-lt v5, v7, :cond_2c0

    #@2a4
    const-string v5, "20601"

    #@2a6
    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a9
    move-result v5

    #@2aa
    if-eqz v5, :cond_2c0

    #@2ac
    .line 5876
    invoke-virtual {v1, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2af
    move-result-object v5

    #@2b0
    const-string v6, "2060188"

    #@2b2
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b5
    move-result v5

    #@2b6
    if-eqz v5, :cond_2bc

    #@2b8
    .line 5877
    const-string v1, "2060188"

    #@2ba
    goto/16 :goto_e7

    #@2bc
    .line 5880
    :cond_2bc
    const-string v1, ""

    #@2be
    goto/16 :goto_e7

    #@2c0
    .line 5884
    :cond_2c0
    if-eqz v1, :cond_31c

    #@2c2
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@2c5
    move-result v5

    #@2c6
    if-lt v5, v10, :cond_31c

    #@2c8
    const-string v5, "26207"

    #@2ca
    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2cd
    move-result v5

    #@2ce
    if-eqz v5, :cond_31c

    #@2d0
    .line 5888
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@2d3
    move-result v5

    #@2d4
    const/16 v6, 0xf

    #@2d6
    if-lt v5, v6, :cond_2e8

    #@2d8
    invoke-virtual {v1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2db
    move-result-object v5

    #@2dc
    const-string v6, "26207500"

    #@2de
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e1
    move-result v5

    #@2e2
    if-eqz v5, :cond_2e8

    #@2e4
    .line 5889
    const-string v1, "26207500"

    #@2e6
    goto/16 :goto_e7

    #@2e8
    .line 5892
    :cond_2e8
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@2eb
    move-result v5

    #@2ec
    const/16 v6, 0xf

    #@2ee
    if-lt v5, v6, :cond_300

    #@2f0
    invoke-virtual {v1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2f3
    move-result-object v5

    #@2f4
    const-string v6, "26207404"

    #@2f6
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f9
    move-result v5

    #@2fa
    if-eqz v5, :cond_300

    #@2fc
    .line 5893
    const-string v1, "26207404"

    #@2fe
    goto/16 :goto_e7

    #@300
    .line 5896
    :cond_300
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@303
    move-result v5

    #@304
    const/16 v6, 0xf

    #@306
    if-lt v5, v6, :cond_318

    #@308
    invoke-virtual {v1, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@30b
    move-result-object v5

    #@30c
    const-string v6, "2620749"

    #@30e
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@311
    move-result v5

    #@312
    if-eqz v5, :cond_318

    #@314
    .line 5897
    const-string v1, "2620749"

    #@316
    goto/16 :goto_e7

    #@318
    .line 5901
    :cond_318
    const-string v1, ""

    #@31a
    goto/16 :goto_e7

    #@31c
    .line 5905
    :cond_31c
    if-eqz v1, :cond_340

    #@31e
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@321
    move-result v5

    #@322
    if-lt v5, v10, :cond_340

    #@324
    const-string v5, "50502"

    #@326
    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@329
    move-result v5

    #@32a
    if-eqz v5, :cond_340

    #@32c
    .line 5909
    invoke-virtual {v1, v8, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@32f
    move-result-object v5

    #@330
    const-string v6, "505029"

    #@332
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@335
    move-result v5

    #@336
    if-eqz v5, :cond_33c

    #@338
    .line 5910
    const-string v1, "505029"

    #@33a
    goto/16 :goto_e7

    #@33c
    .line 5913
    :cond_33c
    const-string v1, ""

    #@33e
    goto/16 :goto_e7

    #@340
    .line 5917
    :cond_340
    if-eqz v1, :cond_364

    #@342
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@345
    move-result v5

    #@346
    if-lt v5, v9, :cond_364

    #@348
    const-string v5, "50503"

    #@34a
    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@34d
    move-result v5

    #@34e
    if-eqz v5, :cond_364

    #@350
    .line 5921
    invoke-virtual {v1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@353
    move-result-object v5

    #@354
    const-string v6, "50503823"

    #@356
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@359
    move-result v5

    #@35a
    if-eqz v5, :cond_360

    #@35c
    .line 5922
    const-string v1, "50503823"

    #@35e
    goto/16 :goto_e7

    #@360
    .line 5925
    :cond_360
    const-string v1, ""

    #@362
    goto/16 :goto_e7

    #@364
    .line 5929
    :cond_364
    const-string v5, "ORG"

    #@366
    invoke-static {}, Landroid/provider/Telephony$Carriers;->getOperatorCode()Ljava/lang/String;

    #@369
    move-result-object v6

    #@36a
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36d
    move-result v5

    #@36e
    if-eqz v5, :cond_3ac

    #@370
    const-string v5, "CH"

    #@372
    invoke-static {}, Landroid/provider/Telephony$Carriers;->getCountryCode()Ljava/lang/String;

    #@375
    move-result-object v6

    #@376
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@379
    move-result v5

    #@37a
    if-eqz v5, :cond_3ac

    #@37c
    if-eqz v1, :cond_3ac

    #@37e
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@381
    move-result v5

    #@382
    if-lt v5, v7, :cond_3ac

    #@384
    const-string v5, "22803"

    #@386
    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@389
    move-result v5

    #@38a
    if-eqz v5, :cond_3ac

    #@38c
    .line 5935
    invoke-virtual {v1, v10, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@38f
    move-result-object v5

    #@390
    const-string v6, "0"

    #@392
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@395
    move-result v5

    #@396
    if-nez v5, :cond_3a4

    #@398
    invoke-virtual {v1, v10, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@39b
    move-result-object v5

    #@39c
    const-string v6, "9"

    #@39e
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3a1
    move-result v5

    #@3a2
    if-eqz v5, :cond_3a8

    #@3a4
    .line 5937
    :cond_3a4
    const-string v1, ""

    #@3a6
    goto/16 :goto_e7

    #@3a8
    .line 5940
    :cond_3a8
    const-string v1, "22803[1-9][1-8]"

    #@3aa
    goto/16 :goto_e7

    #@3ac
    .line 5945
    :cond_3ac
    if-eqz v1, :cond_3e0

    #@3ae
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@3b1
    move-result v5

    #@3b2
    if-lt v5, v7, :cond_3e0

    #@3b4
    const-string v5, "65507"

    #@3b6
    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b9
    move-result v5

    #@3ba
    if-eqz v5, :cond_3e0

    #@3bc
    .line 5949
    invoke-virtual {v1, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3bf
    move-result-object v5

    #@3c0
    const-string v6, "6550710"

    #@3c2
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3c5
    move-result v5

    #@3c6
    if-eqz v5, :cond_3cc

    #@3c8
    .line 5950
    const-string v1, "6550710"

    #@3ca
    goto/16 :goto_e7

    #@3cc
    .line 5951
    :cond_3cc
    invoke-virtual {v1, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3cf
    move-result-object v5

    #@3d0
    const-string v6, "6550713"

    #@3d2
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3d5
    move-result v5

    #@3d6
    if-eqz v5, :cond_3dc

    #@3d8
    .line 5952
    const-string v1, "6550713"

    #@3da
    goto/16 :goto_e7

    #@3dc
    .line 5955
    :cond_3dc
    const-string v1, ""

    #@3de
    goto/16 :goto_e7

    #@3e0
    .line 5959
    :cond_3e0
    if-eqz v1, :cond_412

    #@3e2
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@3e5
    move-result v5

    #@3e6
    const/16 v6, 0xf

    #@3e8
    if-lt v5, v6, :cond_412

    #@3ea
    const-string v5, "302720"

    #@3ec
    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3ef
    move-result v5

    #@3f0
    if-eqz v5, :cond_412

    #@3f2
    .line 5962
    invoke-virtual {v1, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3f5
    move-result-object v5

    #@3f6
    const-string v6, "94"

    #@3f8
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3fb
    move-result v5

    #@3fc
    if-nez v5, :cond_402

    #@3fe
    .line 5963
    const-string v1, "302720XXX"

    #@400
    goto/16 :goto_e7

    #@402
    .line 5966
    :cond_402
    invoke-virtual {v1, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@405
    move-result-object v5

    #@406
    const-string v6, "94"

    #@408
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@40b
    move-result v5

    #@40c
    if-eqz v5, :cond_e7

    #@40e
    .line 5967
    const-string v1, "302720X94"

    #@410
    goto/16 :goto_e7

    #@412
    .line 5970
    :cond_412
    if-eqz v1, :cond_428

    #@414
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@417
    move-result v5

    #@418
    const/16 v6, 0xf

    #@41a
    if-lt v5, v6, :cond_428

    #@41c
    const-string v5, "302370"

    #@41e
    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@421
    move-result v5

    #@422
    if-eqz v5, :cond_428

    #@424
    .line 5973
    const-string v1, "302370XXX"

    #@426
    goto/16 :goto_e7

    #@428
    .line 5977
    :cond_428
    if-eqz v1, :cond_474

    #@42a
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@42d
    move-result v5

    #@42e
    if-lt v5, v9, :cond_474

    #@430
    const-string v5, "24801"

    #@432
    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@435
    move-result v5

    #@436
    if-eqz v5, :cond_474

    #@438
    .line 5980
    invoke-virtual {v1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@43b
    move-result-object v5

    #@43c
    const-string v6, "24801012"

    #@43e
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@441
    move-result v5

    #@442
    if-nez v5, :cond_450

    #@444
    invoke-virtual {v1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@447
    move-result-object v5

    #@448
    const-string v6, "24801013"

    #@44a
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@44d
    move-result v5

    #@44e
    if-eqz v5, :cond_454

    #@450
    .line 5981
    :cond_450
    const-string v1, "24801012_24801013"

    #@452
    goto/16 :goto_e7

    #@454
    .line 5984
    :cond_454
    invoke-virtual {v1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@457
    move-result-object v5

    #@458
    const-string v6, "24801022"

    #@45a
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@45d
    move-result v5

    #@45e
    if-nez v5, :cond_46c

    #@460
    invoke-virtual {v1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@463
    move-result-object v5

    #@464
    const-string v6, "24801023"

    #@466
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@469
    move-result v5

    #@46a
    if-eqz v5, :cond_470

    #@46c
    .line 5985
    :cond_46c
    const-string v1, "24801022_24801023"

    #@46e
    goto/16 :goto_e7

    #@470
    .line 5988
    :cond_470
    const-string v1, ""

    #@472
    goto/16 :goto_e7

    #@474
    .line 5993
    :cond_474
    const-string v1, ""

    #@476
    goto/16 :goto_e7

    #@478
    .line 6003
    :cond_478
    const-string v5, "USC"

    #@47a
    invoke-static {}, Landroid/provider/Telephony$Carriers;->getOperatorCode()Ljava/lang/String;

    #@47d
    move-result-object v6

    #@47e
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@481
    move-result v5

    #@482
    if-eqz v5, :cond_4a0

    #@484
    const-string v5, "MX"

    #@486
    invoke-static {}, Landroid/provider/Telephony$Carriers;->getCountryCode()Ljava/lang/String;

    #@489
    move-result-object v6

    #@48a
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@48d
    move-result v5

    #@48e
    if-eqz v5, :cond_4a0

    #@490
    const-string v5, "22201"

    #@492
    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@495
    move-result v5

    #@496
    if-eqz v5, :cond_4a0

    #@498
    .line 6006
    const-string v5, "Iusacell"

    #@49a
    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@49d
    move-result-object v3

    #@49e
    goto/16 :goto_6d

    #@4a0
    .line 6009
    :cond_4a0
    const-string v5, "PCL"

    #@4a2
    invoke-static {}, Landroid/provider/Telephony$Carriers;->getOperatorCode()Ljava/lang/String;

    #@4a5
    move-result-object v6

    #@4a6
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4a9
    move-result v5

    #@4aa
    if-eqz v5, :cond_4c8

    #@4ac
    const-string v5, "IL"

    #@4ae
    invoke-static {}, Landroid/provider/Telephony$Carriers;->getCountryCode()Ljava/lang/String;

    #@4b1
    move-result-object v6

    #@4b2
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4b5
    move-result v5

    #@4b6
    if-eqz v5, :cond_4c8

    #@4b8
    const-string v5, "20404"

    #@4ba
    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4bd
    move-result v5

    #@4be
    if-eqz v5, :cond_4c8

    #@4c0
    .line 6012
    const-string v5, "Pelephone"

    #@4c2
    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@4c5
    move-result-object v3

    #@4c6
    goto/16 :goto_6d

    #@4c8
    .line 6015
    :cond_4c8
    const-string v5, "MIRS"

    #@4ca
    invoke-static {}, Landroid/provider/Telephony$Carriers;->getOperatorCode()Ljava/lang/String;

    #@4cd
    move-result-object v6

    #@4ce
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4d1
    move-result v5

    #@4d2
    if-eqz v5, :cond_4f0

    #@4d4
    const-string v5, "IL"

    #@4d6
    invoke-static {}, Landroid/provider/Telephony$Carriers;->getCountryCode()Ljava/lang/String;

    #@4d9
    move-result-object v6

    #@4da
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4dd
    move-result v5

    #@4de
    if-eqz v5, :cond_4f0

    #@4e0
    const-string v5, "20404"

    #@4e2
    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4e5
    move-result v5

    #@4e6
    if-eqz v5, :cond_4f0

    #@4e8
    .line 6018
    const-string v5, "MIRS"

    #@4ea
    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@4ed
    move-result-object v3

    #@4ee
    goto/16 :goto_6d

    #@4f0
    .line 6021
    :cond_4f0
    const-string v5, "TLF"

    #@4f2
    invoke-static {}, Landroid/provider/Telephony$Carriers;->getOperatorCode()Ljava/lang/String;

    #@4f5
    move-result-object v6

    #@4f6
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4f9
    move-result v5

    #@4fa
    if-eqz v5, :cond_50c

    #@4fc
    const-string v5, "23410"

    #@4fe
    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@501
    move-result v5

    #@502
    if-eqz v5, :cond_50c

    #@504
    .line 6023
    const-string v5, "Telefonica"

    #@506
    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@509
    move-result-object v3

    #@50a
    goto/16 :goto_6d

    #@50c
    .line 6029
    :cond_50c
    invoke-static {}, Landroid/provider/Telephony$Carriers;->isAutoProfileNeeded()Z

    #@50f
    move-result v5

    #@510
    if-nez v5, :cond_6d

    #@512
    .line 6032
    invoke-static {}, Landroid/provider/Telephony$Carriers;->getOpVersionExtraID()Ljava/lang/String;

    #@515
    move-result-object v5

    #@516
    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@519
    move-result-object v3

    #@51a
    goto/16 :goto_6d
.end method

.method public static getAutoprofileFeature()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 5118
    sget-object v0, Landroid/provider/Telephony$Carriers;->AUTOPROFILE_FEATURE:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_7

    #@4
    sget-object v0, Landroid/provider/Telephony$Carriers;->AUTOPROFILE_FEATURE:Ljava/lang/String;

    #@6
    .line 5120
    :goto_6
    return-object v0

    #@7
    .line 5119
    :cond_7
    const-string v0, "ro.lge.autoprofile"

    #@9
    const-string v1, "false"

    #@b
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    sput-object v0, Landroid/provider/Telephony$Carriers;->AUTOPROFILE_FEATURE:Ljava/lang/String;

    #@11
    .line 5120
    sget-object v0, Landroid/provider/Telephony$Carriers;->AUTOPROFILE_FEATURE:Ljava/lang/String;

    #@13
    goto :goto_6
.end method

.method public static getClonedApnKey(Landroid/content/Context;I)Ljava/lang/String;
    .registers 7
    .parameter "context"
    .parameter "sim_slot"

    #@0
    .prologue
    .line 6844
    const-string v2, "Telephony"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "getClonedApnKey:sim_slot:"

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 6846
    const-string v0, "cloned-apn"

    #@1a
    .line 6847
    .local v0, file:Ljava/lang/String;
    const/4 v2, 0x2

    #@1b
    if-ne p1, v2, :cond_1f

    #@1d
    .line 6848
    const-string v0, "cloned-apn-2"

    #@1f
    .line 6850
    :cond_1f
    const/4 v2, 0x0

    #@20
    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@23
    move-result-object v1

    #@24
    .line 6852
    .local v1, sp:Landroid/content/SharedPreferences;
    const-string v2, "apn_key"

    #@26
    const-string v3, ""

    #@28
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    return-object v2
.end method

.method private static getCountryCode()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 5150
    sget-object v0, Landroid/provider/Telephony$Carriers;->COUNTRY_CODE:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 5151
    sget-object v0, Landroid/provider/Telephony$Carriers;->COUNTRY_CODE:Ljava/lang/String;

    #@6
    .line 5170
    :goto_6
    return-object v0

    #@7
    .line 5154
    :cond_7
    const-string v0, "ro.build.target_operator"

    #@9
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    sput-object v0, Landroid/provider/Telephony$Carriers;->OPERATOR_CODE:Ljava/lang/String;

    #@f
    .line 5155
    const-string v0, "ro.build.target_country"

    #@11
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    sput-object v0, Landroid/provider/Telephony$Carriers;->COUNTRY_CODE:Ljava/lang/String;

    #@17
    .line 5161
    const-string v0, "1"

    #@19
    const-string v1, "ro.build.regional"

    #@1b
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v0

    #@23
    if-eqz v0, :cond_46

    #@25
    .line 5163
    const-string v0, "VDF"

    #@27
    sget-object v1, Landroid/provider/Telephony$Carriers;->OPERATOR_CODE:Ljava/lang/String;

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c
    move-result v0

    #@2d
    if-eqz v0, :cond_46

    #@2f
    const-string v0, "EU"

    #@31
    sget-object v1, Landroid/provider/Telephony$Carriers;->COUNTRY_CODE:Ljava/lang/String;

    #@33
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36
    move-result v0

    #@37
    if-eqz v0, :cond_46

    #@39
    .line 5164
    const-string v0, "27602;VODAFONE AL;23201;A1;20601;PROXIMUS;28401;;21910;HR VIP;28001;CYTAMOBILE-VODAFONE;23003;VODAFONE CZ;29403;VIP MK;20810;F SFR;64710;SFR;20810;4C;26202;VODAFONE.DE;20205;VODAFONE GR;21670;VODAFONE HU;21670;21670XX2;27402;;27403;;27201;VODAFONE IE;22210;VODAFONE IT;27801;VODAFONE MT;20404;;26801;;22601;VODAFONE RO;22005;VIP;29340;SOMOBIL;21401;VODAFONE ES;22801;SWISSCOM;23415;VODAFONE UK;53001;;53024;;50503;;60202;;65501;;26001;PLUS;26002;T-MOBILE.PL;26003;ORANGE;26006;PLAY;45005;SKT;45008;KT;"

    #@3b
    const-string v1, "persist.radio.mcc-list"

    #@3d
    const-string v2, "FFF"

    #@3f
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@42
    move-result-object v1

    #@43
    invoke-static {v0, v1}, Landroid/provider/Telephony$Carriers;->rebuildProperties(Ljava/lang/String;Ljava/lang/String;)V

    #@46
    .line 5170
    :cond_46
    sget-object v0, Landroid/provider/Telephony$Carriers;->COUNTRY_CODE:Ljava/lang/String;

    #@48
    goto :goto_6
.end method

.method public static getNumOfSimSlot()I
    .registers 3

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 6786
    const/4 v0, 0x1

    #@2
    .line 6787
    .local v0, nSim:I
    invoke-static {}, Landroid/provider/Telephony$Carriers;->isSupportAutoProfileAsMultiSimMode()Z

    #@5
    move-result v1

    #@6
    if-nez v1, :cond_9

    #@8
    .line 6794
    :cond_8
    :goto_8
    return v2

    #@9
    .line 6791
    :cond_9
    if-ge v0, v2, :cond_8

    #@b
    goto :goto_8
.end method

.method public static getNumeric()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 5572
    invoke-static {}, Landroid/provider/Telephony$Carriers;->isAutoProfileNeeded()Z

    #@3
    move-result v2

    #@4
    if-eqz v2, :cond_f

    #@6
    .line 5574
    const-string v2, "gsm.sim.operator.numeric"

    #@8
    const-string v3, ""

    #@a
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    .line 5589
    .local v0, extraid:Ljava/lang/String;
    :cond_e
    :goto_e
    return-object v1

    #@f
    .line 5577
    .end local v0           #extraid:Ljava/lang/String;
    :cond_f
    invoke-static {}, Landroid/provider/Telephony$Carriers;->isMultipleNumericOperator()Z

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_27

    #@15
    .line 5580
    const-string v2, "gsm.sim.operator.numeric"

    #@17
    const-string v3, ""

    #@19
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    .line 5582
    .local v1, numeric:Ljava/lang/String;
    invoke-static {v1}, Landroid/provider/Telephony$Carriers;->getAutoProfileKey(Ljava/lang/String;)Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    .line 5584
    .restart local v0       #extraid:Ljava/lang/String;
    invoke-static {v1, v0}, Landroid/provider/Telephony$Carriers;->isAcceptableSIM(Ljava/lang/String;Ljava/lang/String;)Z

    #@24
    move-result v2

    #@25
    if-nez v2, :cond_e

    #@27
    .line 5589
    :cond_27
    invoke-static {}, Landroid/provider/Telephony$Carriers;->getOpVersionNumeric()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    goto :goto_e
.end method

.method public static getNumeric(I)Ljava/lang/String;
    .registers 7
    .parameter "subs"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 5626
    invoke-static {}, Landroid/provider/Telephony$Carriers;->isAutoProfileNeeded()Z

    #@4
    move-result v2

    #@5
    if-eqz v2, :cond_29

    #@7
    .line 5628
    const-string v2, "Telephony"

    #@9
    new-instance v3, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v4, "getNumeric: "

    #@10
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 5629
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@22
    const-string v2, "gsm.sim.operator.numeric"

    #@24
    invoke-static {v2, p0, v5}, Landroid/telephony/MSimTelephonyManager;->getTelephonyProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    .line 5640
    :cond_28
    :goto_28
    return-object v1

    #@29
    .line 5631
    :cond_29
    invoke-static {}, Landroid/provider/Telephony$Carriers;->isMultipleNumericOperator()Z

    #@2c
    move-result v2

    #@2d
    if-eqz v2, :cond_42

    #@2f
    .line 5633
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@32
    const-string v2, "gsm.sim.operator.numeric"

    #@34
    invoke-static {v2, p0, v5}, Landroid/telephony/MSimTelephonyManager;->getTelephonyProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    #@37
    move-result-object v1

    #@38
    .line 5634
    .local v1, numeric:Ljava/lang/String;
    invoke-static {v1, p0}, Landroid/provider/Telephony$Carriers;->getAutoProfileKey(Ljava/lang/String;I)Ljava/lang/String;

    #@3b
    move-result-object v0

    #@3c
    .line 5636
    .local v0, extraid:Ljava/lang/String;
    invoke-static {v1, v0}, Landroid/provider/Telephony$Carriers;->isAcceptableSIM(Ljava/lang/String;Ljava/lang/String;)Z

    #@3f
    move-result v2

    #@40
    if-nez v2, :cond_28

    #@42
    .line 5640
    .end local v0           #extraid:Ljava/lang/String;
    .end local v1           #numeric:Ljava/lang/String;
    :cond_42
    invoke-static {}, Landroid/provider/Telephony$Carriers;->getOpVersionNumeric()Ljava/lang/String;

    #@45
    move-result-object v1

    #@46
    goto :goto_28
.end method

.method public static getOpVersionExtraID()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 5189
    invoke-static {}, Landroid/provider/Telephony$Carriers;->isAutoProfileNeeded()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_9

    #@6
    .line 5190
    const-string v0, ""

    #@8
    .line 5198
    :goto_8
    return-object v0

    #@9
    .line 5192
    :cond_9
    sget-object v0, Landroid/provider/Telephony$Carriers;->OPERATOR_VERISON_EXTRAID:Ljava/lang/String;

    #@b
    if-eqz v0, :cond_10

    #@d
    .line 5193
    sget-object v0, Landroid/provider/Telephony$Carriers;->OPERATOR_VERISON_EXTRAID:Ljava/lang/String;

    #@f
    goto :goto_8

    #@10
    .line 5195
    :cond_10
    invoke-static {}, Landroid/provider/Telephony$Carriers;->buildOperatorVersionValues()Z

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_19

    #@16
    .line 5196
    sget-object v0, Landroid/provider/Telephony$Carriers;->OPERATOR_VERISON_EXTRAID:Ljava/lang/String;

    #@18
    goto :goto_8

    #@19
    .line 5198
    :cond_19
    const-string v0, ""

    #@1b
    goto :goto_8
.end method

.method public static getOpVersionNumeric()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 5175
    invoke-static {}, Landroid/provider/Telephony$Carriers;->isAutoProfileNeeded()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_9

    #@6
    .line 5176
    const-string v0, ""

    #@8
    .line 5184
    :goto_8
    return-object v0

    #@9
    .line 5178
    :cond_9
    sget-object v0, Landroid/provider/Telephony$Carriers;->OPERATOR_VERISON_NUMERIC:Ljava/lang/String;

    #@b
    if-eqz v0, :cond_10

    #@d
    .line 5179
    sget-object v0, Landroid/provider/Telephony$Carriers;->OPERATOR_VERISON_NUMERIC:Ljava/lang/String;

    #@f
    goto :goto_8

    #@10
    .line 5181
    :cond_10
    invoke-static {}, Landroid/provider/Telephony$Carriers;->buildOperatorVersionValues()Z

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_19

    #@16
    .line 5182
    sget-object v0, Landroid/provider/Telephony$Carriers;->OPERATOR_VERISON_NUMERIC:Ljava/lang/String;

    #@18
    goto :goto_8

    #@19
    .line 5184
    :cond_19
    const-string v0, ""

    #@1b
    goto :goto_8
.end method

.method private static getOperatorCode()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 5125
    sget-object v0, Landroid/provider/Telephony$Carriers;->OPERATOR_CODE:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 5126
    sget-object v0, Landroid/provider/Telephony$Carriers;->OPERATOR_CODE:Ljava/lang/String;

    #@6
    .line 5145
    :goto_6
    return-object v0

    #@7
    .line 5129
    :cond_7
    const-string v0, "ro.build.target_operator"

    #@9
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    sput-object v0, Landroid/provider/Telephony$Carriers;->OPERATOR_CODE:Ljava/lang/String;

    #@f
    .line 5130
    const-string v0, "ro.build.target_country"

    #@11
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    sput-object v0, Landroid/provider/Telephony$Carriers;->COUNTRY_CODE:Ljava/lang/String;

    #@17
    .line 5136
    const-string v0, "1"

    #@19
    const-string v1, "ro.build.regional"

    #@1b
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v0

    #@23
    if-eqz v0, :cond_46

    #@25
    .line 5138
    const-string v0, "VDF"

    #@27
    sget-object v1, Landroid/provider/Telephony$Carriers;->OPERATOR_CODE:Ljava/lang/String;

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c
    move-result v0

    #@2d
    if-eqz v0, :cond_46

    #@2f
    const-string v0, "EU"

    #@31
    sget-object v1, Landroid/provider/Telephony$Carriers;->COUNTRY_CODE:Ljava/lang/String;

    #@33
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36
    move-result v0

    #@37
    if-eqz v0, :cond_46

    #@39
    .line 5139
    const-string v0, "27602;VODAFONE AL;23201;A1;20601;PROXIMUS;28401;;21910;HR VIP;28001;CYTAMOBILE-VODAFONE;23003;VODAFONE CZ;29403;VIP MK;20810;F SFR;64710;SFR;20810;4C;26202;VODAFONE.DE;20205;VODAFONE GR;21670;VODAFONE HU;21670;21670XX2;27402;;27403;;27201;VODAFONE IE;22210;VODAFONE IT;27801;VODAFONE MT;20404;;26801;;22601;VODAFONE RO;22005;VIP;29340;SOMOBIL;21401;VODAFONE ES;22801;SWISSCOM;23415;VODAFONE UK;53001;;53024;;50503;;60202;;65501;;26001;PLUS;26002;T-MOBILE.PL;26003;ORANGE;26006;PLAY;45005;SKT;45008;KT;"

    #@3b
    const-string v1, "persist.radio.mcc-list"

    #@3d
    const-string v2, "FFF"

    #@3f
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@42
    move-result-object v1

    #@43
    invoke-static {v0, v1}, Landroid/provider/Telephony$Carriers;->rebuildProperties(Ljava/lang/String;Ljava/lang/String;)V

    #@46
    .line 5145
    :cond_46
    sget-object v0, Landroid/provider/Telephony$Carriers;->OPERATOR_CODE:Ljava/lang/String;

    #@48
    goto :goto_6
.end method

.method public static isAcceptableSIM(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 6
    .parameter "numeric"
    .parameter "extraid"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 5214
    invoke-static {}, Landroid/provider/Telephony$Carriers;->isAutoProfileNeeded()Z

    #@4
    move-result v1

    #@5
    if-eqz v1, :cond_8

    #@7
    .line 5224
    :cond_7
    :goto_7
    return v0

    #@8
    .line 5218
    :cond_8
    invoke-static {}, Landroid/provider/Telephony$Carriers;->buildOperatorVersionValues()Z

    #@b
    .line 5220
    sget-object v1, Landroid/provider/Telephony$Carriers;->mNumericExtraIDSet:Ljava/util/HashSet;

    #@d
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    const-string v3, ";"

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@2b
    move-result v1

    #@2c
    if-nez v1, :cond_7

    #@2e
    .line 5222
    const/4 v0, 0x0

    #@2f
    goto :goto_7
.end method

.method public static isAutoProfileNeeded()Z
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 5524
    invoke-static {}, Landroid/provider/Telephony$Carriers;->getCountryCode()Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    .line 5525
    .local v1, countryCode:Ljava/lang/String;
    invoke-static {}, Landroid/provider/Telephony$Carriers;->getOperatorCode()Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    .line 5526
    .local v2, operatorCode:Ljava/lang/String;
    invoke-static {}, Landroid/provider/Telephony$Carriers;->getAutoprofileFeature()Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    .line 5529
    .local v0, autoprofileFeature:Ljava/lang/String;
    const-string v5, "true"

    #@10
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v5

    #@14
    if-eqz v5, :cond_17

    #@16
    .line 5567
    :cond_16
    :goto_16
    return v3

    #@17
    .line 5534
    :cond_17
    const-string v5, "ro.build.feature.smart_apn"

    #@19
    invoke-static {v5, v4}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@1c
    move-result v5

    #@1d
    if-nez v5, :cond_16

    #@1f
    .line 5540
    const-string v5, "OPEN"

    #@21
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v5

    #@25
    if-nez v5, :cond_16

    #@27
    const-string v5, "ATT"

    #@29
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c
    move-result v5

    #@2d
    if-nez v5, :cond_16

    #@2f
    const-string v5, "VZW"

    #@31
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@34
    move-result v5

    #@35
    if-nez v5, :cond_16

    #@37
    const-string v5, "DCM"

    #@39
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3c
    move-result v5

    #@3d
    if-nez v5, :cond_16

    #@3f
    const-string v5, "SKT"

    #@41
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@44
    move-result v5

    #@45
    if-nez v5, :cond_16

    #@47
    const-string v5, "KT"

    #@49
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4c
    move-result v5

    #@4d
    if-nez v5, :cond_16

    #@4f
    const-string v5, "LGU"

    #@51
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@54
    move-result v5

    #@55
    if-nez v5, :cond_16

    #@57
    const-string v5, "SPR"

    #@59
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5c
    move-result v5

    #@5d
    if-nez v5, :cond_16

    #@5f
    const-string v5, "VDF"

    #@61
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@64
    move-result v5

    #@65
    if-nez v5, :cond_16

    #@67
    const-string v5, "TEL"

    #@69
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6c
    move-result v5

    #@6d
    if-nez v5, :cond_16

    #@6f
    const-string v5, "SHB"

    #@71
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@74
    move-result v5

    #@75
    if-nez v5, :cond_16

    #@77
    const-string v5, "PLS"

    #@79
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7c
    move-result v5

    #@7d
    if-nez v5, :cond_16

    #@7f
    const-string v5, "ORG"

    #@81
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@84
    move-result v5

    #@85
    if-nez v5, :cond_16

    #@87
    const-string v5, "TCL"

    #@89
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8c
    move-result v5

    #@8d
    if-nez v5, :cond_16

    #@8f
    const-string v5, "USC"

    #@91
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@94
    move-result v5

    #@95
    if-nez v5, :cond_16

    #@97
    const-string v5, "VIV"

    #@99
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9c
    move-result v5

    #@9d
    if-nez v5, :cond_16

    #@9f
    const-string v5, "CLR"

    #@a1
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a4
    move-result v5

    #@a5
    if-nez v5, :cond_16

    #@a7
    const-string v5, "STL"

    #@a9
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ac
    move-result v5

    #@ad
    if-nez v5, :cond_16

    #@af
    const-string v5, "MON"

    #@b1
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b4
    move-result v5

    #@b5
    if-nez v5, :cond_16

    #@b7
    const-string v5, "KDDI"

    #@b9
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@bc
    move-result v5

    #@bd
    if-nez v5, :cond_16

    #@bf
    const-string v5, "CUCC"

    #@c1
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c4
    move-result v5

    #@c5
    if-nez v5, :cond_16

    #@c7
    const-string v5, "OPT"

    #@c9
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@cc
    move-result v5

    #@cd
    if-nez v5, :cond_16

    #@cf
    const-string v5, "GLO"

    #@d1
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d4
    move-result v5

    #@d5
    if-nez v5, :cond_16

    #@d7
    const-string v5, "TMN"

    #@d9
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@dc
    move-result v5

    #@dd
    if-nez v5, :cond_16

    #@df
    const-string v5, "TIM"

    #@e1
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e4
    move-result v5

    #@e5
    if-nez v5, :cond_16

    #@e7
    const-string v5, "PLY"

    #@e9
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ec
    move-result v5

    #@ed
    if-nez v5, :cond_16

    #@ef
    const-string v5, "USC"

    #@f1
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f4
    move-result v5

    #@f5
    if-nez v5, :cond_16

    #@f7
    .line 5563
    const-string v5, "COM"

    #@f9
    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@fc
    move-result v5

    #@fd
    if-nez v5, :cond_16

    #@ff
    move v3, v4

    #@100
    .line 5567
    goto/16 :goto_16
.end method

.method private static isGidBasedOperator(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 10
    .parameter "numeric"
    .parameter "gid"

    #@0
    .prologue
    const/16 v7, 0x8

    #@2
    const/4 v2, 0x1

    #@3
    const/4 v3, 0x0

    #@4
    .line 5318
    invoke-static {}, Landroid/provider/Telephony$Carriers;->getCountryCode()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 5319
    .local v0, countryCode:Ljava/lang/String;
    invoke-static {}, Landroid/provider/Telephony$Carriers;->getOperatorCode()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    .line 5322
    .local v1, operatorCode:Ljava/lang/String;
    if-eqz p0, :cond_14

    #@e
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@11
    move-result v4

    #@12
    if-nez v4, :cond_16

    #@14
    :cond_14
    move v2, v3

    #@15
    .line 5351
    :cond_15
    :goto_15
    return v2

    #@16
    .line 5325
    :cond_16
    if-eqz p1, :cond_1e

    #@18
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@1b
    move-result v4

    #@1c
    if-nez v4, :cond_20

    #@1e
    :cond_1e
    move v2, v3

    #@1f
    .line 5326
    goto :goto_15

    #@20
    .line 5335
    :cond_20
    const-string v4, "21406"

    #@22
    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v4

    #@26
    if-eqz v4, :cond_30

    #@28
    const-string v4, "0008"

    #@2a
    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@2d
    move-result v4

    #@2e
    if-nez v4, :cond_15

    #@30
    .line 5336
    :cond_30
    const-string v4, "21408"

    #@32
    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@35
    move-result v4

    #@36
    if-eqz v4, :cond_40

    #@38
    const-string v4, "0008"

    #@3a
    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@3d
    move-result v4

    #@3e
    if-nez v4, :cond_15

    #@40
    .line 5339
    :cond_40
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@43
    move-result v4

    #@44
    if-lt v4, v7, :cond_8b

    #@46
    const-string v4, "OPEN"

    #@48
    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4b
    move-result v4

    #@4c
    if-eqz v4, :cond_8b

    #@4e
    const-string v4, "HK"

    #@50
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@53
    move-result v4

    #@54
    if-eqz v4, :cond_8b

    #@56
    .line 5341
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@59
    move-result v4

    #@5a
    if-lt v4, v7, :cond_15

    #@5c
    const-string v4, "23410;0A;23415;A0;23415;C1;23415;70;23430;28;20801;33;20801;52;20823;52;20601;52;20801;4E;20810;4E;20826;4E;20810;12;64710;12;20810;4C;20810;44;27211;0A;20601;0A;21408;00;302610;3D;302610;3E;302610;3F;302610;40;302780;5A;20404;5A;310410;DE;302220;54;302221;54;302220;4B;302221;4B;302220;50;302221;50;302220;43;302660;2C;302370;2C;52000;02;52000;01;45400;01010000;45400;01020000;45400;01030000;45400;01040000;45400;02020001;45400;02030001;45400;02020002;45402;01010000;45402;01020000;45402;01030000;45402;01040000;45402;02020001;45402;02030001;45402;02020002;45410;01010000;45410;01020000;45410;01030000;45410;01040000;45410;02020001;45410;02030001;45410;02020002;45418;01010000;45418;01020000;45418;01030000;45418;01040000;45418;02020001;45418;02030001;45418;02020002;"

    #@5e
    new-instance v5, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v5

    #@67
    const-string v6, ";"

    #@69
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v5

    #@6d
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@70
    move-result-object v6

    #@71
    invoke-virtual {v6, v3, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@74
    move-result-object v6

    #@75
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v5

    #@79
    const-string v6, ";"

    #@7b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v5

    #@7f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v5

    #@83
    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@86
    move-result v4

    #@87
    if-gez v4, :cond_15

    #@89
    move v2, v3

    #@8a
    .line 5342
    goto :goto_15

    #@8b
    .line 5346
    :cond_8b
    const-string v4, "23410;0A;23415;A0;23415;C1;23415;70;23430;28;20801;33;20801;52;20823;52;20601;52;20801;4E;20810;4E;20826;4E;20810;12;64710;12;20810;4C;20810;44;27211;0A;20601;0A;21408;00;302610;3D;302610;3E;302610;3F;302610;40;302780;5A;20404;5A;310410;DE;302220;54;302221;54;302220;4B;302221;4B;302220;50;302221;50;302220;43;302660;2C;302370;2C;52000;02;52000;01;45400;01010000;45400;01020000;45400;01030000;45400;01040000;45400;02020001;45400;02030001;45400;02020002;45402;01010000;45402;01020000;45402;01030000;45402;01040000;45402;02020001;45402;02030001;45402;02020002;45410;01010000;45410;01020000;45410;01030000;45410;01040000;45410;02020001;45410;02030001;45410;02020002;45418;01010000;45418;01020000;45418;01030000;45418;01040000;45418;02020001;45418;02030001;45418;02020002;"

    #@8d
    new-instance v5, Ljava/lang/StringBuilder;

    #@8f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@92
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v5

    #@96
    const-string v6, ";"

    #@98
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v5

    #@9c
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@9f
    move-result-object v6

    #@a0
    const/4 v7, 0x2

    #@a1
    invoke-virtual {v6, v3, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@a4
    move-result-object v6

    #@a5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v5

    #@a9
    const-string v6, ";"

    #@ab
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v5

    #@af
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b2
    move-result-object v5

    #@b3
    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@b6
    move-result v4

    #@b7
    if-gez v4, :cond_15

    #@b9
    move v2, v3

    #@ba
    .line 5348
    goto/16 :goto_15
.end method

.method private static isIMSIBasedOperator(Ljava/lang/String;)Z
    .registers 3
    .parameter "numeric"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 5513
    if-eqz p0, :cond_9

    #@3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@6
    move-result v1

    #@7
    if-nez v1, :cond_a

    #@9
    .line 5519
    :cond_9
    :goto_9
    return v0

    #@a
    .line 5516
    :cond_a
    const-string v1, "22802;29402;21670;21406;21406;21406;24001;23820;24202;24007;24007;24201;20601;26207;26207;26207;50502;50503;22803;302720;302370;65507;21403;"

    #@c
    invoke-virtual {v1, p0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@f
    move-result v1

    #@10
    if-ltz v1, :cond_9

    #@12
    .line 5519
    const/4 v0, 0x1

    #@13
    goto :goto_9
.end method

.method public static isLoadableAPN(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 19
    .parameter "title"
    .parameter "numeric"
    .parameter "extraid"
    .parameter "dedicated"

    #@0
    .prologue
    .line 6267
    invoke-static {}, Landroid/provider/Telephony$Carriers;->getCountryCode()Ljava/lang/String;

    #@3
    move-result-object v2

    #@4
    .line 6268
    .local v2, countryCode:Ljava/lang/String;
    invoke-static {}, Landroid/provider/Telephony$Carriers;->getOperatorCode()Ljava/lang/String;

    #@7
    move-result-object v6

    #@8
    .line 6269
    .local v6, operatorCode:Ljava/lang/String;
    invoke-static {}, Landroid/provider/Telephony$Carriers;->getAutoprofileFeature()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    .line 6272
    .local v1, autoprofileFeature:Ljava/lang/String;
    const-string v12, "ro.build.feature.smart_apn"

    #@e
    const/4 v13, 0x0

    #@f
    invoke-static {v12, v13}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@12
    move-result v12

    #@13
    if-eqz v12, :cond_17

    #@15
    .line 6273
    const/4 v12, 0x1

    #@16
    .line 6770
    :goto_16
    return v12

    #@17
    .line 6277
    :cond_17
    if-nez p2, :cond_71

    #@19
    .line 6278
    const-string p2, ""

    #@1b
    .line 6283
    :goto_1b
    const-string v12, "Telephony"

    #@1d
    new-instance v13, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v14, "title: "

    #@24
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v13

    #@28
    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v13

    #@2c
    const-string v14, ", numeric: "

    #@2e
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v13

    #@32
    move-object/from16 v0, p1

    #@34
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v13

    #@38
    const-string v14, ", extraid: "

    #@3a
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v13

    #@3e
    move-object/from16 v0, p2

    #@40
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v13

    #@44
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v13

    #@48
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 6284
    const-string v12, "Telephony"

    #@4d
    new-instance v13, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v14, "countryCode: "

    #@54
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v13

    #@58
    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v13

    #@5c
    const-string v14, ", operatorCode: "

    #@5e
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v13

    #@62
    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v13

    #@66
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v13

    #@6a
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    .line 6286
    if-nez p1, :cond_76

    #@6f
    .line 6287
    const/4 v12, 0x0

    #@70
    goto :goto_16

    #@71
    .line 6280
    :cond_71
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@74
    move-result-object p2

    #@75
    goto :goto_1b

    #@76
    .line 6290
    :cond_76
    if-eqz p3, :cond_b8

    #@78
    .line 6292
    new-instance v12, Ljava/lang/StringBuilder;

    #@7a
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@7d
    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v12

    #@81
    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v12

    #@85
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@88
    move-result-object v11

    #@89
    .line 6293
    .local v11, version:Ljava/lang/String;
    move-object/from16 v0, p3

    #@8b
    invoke-virtual {v11, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@8e
    move-result v12

    #@8f
    if-gez v12, :cond_b8

    #@91
    .line 6295
    const-string v12, "Telephony"

    #@93
    new-instance v13, Ljava/lang/StringBuilder;

    #@95
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@98
    const-string v14, "This version is for "

    #@9a
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v13

    #@9e
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v13

    #@a2
    const-string v14, ", but profile is dedicated for "

    #@a4
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v13

    #@a8
    move-object/from16 v0, p3

    #@aa
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v13

    #@ae
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b1
    move-result-object v13

    #@b2
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@b5
    .line 6297
    const/4 v12, 0x0

    #@b6
    goto/16 :goto_16

    #@b8
    .line 6301
    .end local v11           #version:Ljava/lang/String;
    :cond_b8
    const-string v12, "true"

    #@ba
    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@bd
    move-result v12

    #@be
    if-eqz v12, :cond_ed

    #@c0
    .line 6302
    const-string v12, "Telephony"

    #@c2
    new-instance v13, Ljava/lang/StringBuilder;

    #@c4
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@c7
    const-string v14, "Force OPEN, title: "

    #@c9
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v13

    #@cd
    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v13

    #@d1
    const-string v14, ", numeric: "

    #@d3
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v13

    #@d7
    move-object/from16 v0, p1

    #@d9
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v13

    #@dd
    const-string v14, " is added"

    #@df
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v13

    #@e3
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e6
    move-result-object v13

    #@e7
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@ea
    .line 6303
    const/4 v12, 0x1

    #@eb
    goto/16 :goto_16

    #@ed
    .line 6307
    :cond_ed
    const-string v12, "RGS"

    #@ef
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f2
    move-result v12

    #@f3
    if-eqz v12, :cond_466

    #@f5
    .line 6308
    const-string v12, "302370"

    #@f7
    move-object/from16 v0, p1

    #@f9
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@fc
    move-result v12

    #@fd
    if-nez v12, :cond_11d

    #@ff
    const-string v12, "302720"

    #@101
    move-object/from16 v0, p1

    #@103
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@106
    move-result v12

    #@107
    if-nez v12, :cond_11d

    #@109
    const-string v12, "302660"

    #@10b
    move-object/from16 v0, p1

    #@10d
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@110
    move-result v12

    #@111
    if-nez v12, :cond_11d

    #@113
    const-string v12, "45000"

    #@115
    move-object/from16 v0, p1

    #@117
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11a
    move-result v12

    #@11b
    if-eqz v12, :cond_463

    #@11d
    .line 6309
    :cond_11d
    const-string v12, "gsm.sim.operator.gid"

    #@11f
    const-string v13, ""

    #@121
    invoke-static {v12, v13}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@124
    move-result-object v7

    #@125
    .line 6310
    .local v7, operatorGid:Ljava/lang/String;
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@128
    move-result-object v12

    #@129
    invoke-virtual {v12}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    #@12c
    move-result-object v8

    #@12d
    .line 6311
    .local v8, operatorIMSI:Ljava/lang/String;
    const-string v12, "persist.lg.data.firstopsim"

    #@12f
    const-string v13, ""

    #@131
    invoke-static {v12, v13}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@134
    move-result-object v3

    #@135
    .line 6312
    .local v3, firstopsim:Ljava/lang/String;
    const-string v12, "Telephony"

    #@137
    new-instance v13, Ljava/lang/StringBuilder;

    #@139
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@13c
    const-string v14, "[singleSW] firstopsim: "

    #@13e
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@141
    move-result-object v13

    #@142
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@145
    move-result-object v13

    #@146
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@149
    move-result-object v13

    #@14a
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@14d
    .line 6313
    if-eqz v3, :cond_18b

    #@14f
    const-string v12, "null"

    #@151
    invoke-virtual {v3, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@154
    move-result v12

    #@155
    if-nez v12, :cond_18b

    #@157
    .line 6314
    const-string v7, ""

    #@159
    .line 6315
    const-string v8, ""

    #@15b
    .line 6316
    const-string v12, ";"

    #@15d
    invoke-virtual {v3, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@160
    move-result-object v9

    #@161
    .line 6317
    .local v9, opinfo:[Ljava/lang/String;
    const-string v12, "Telephony"

    #@163
    new-instance v13, Ljava/lang/StringBuilder;

    #@165
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@168
    const-string v14, "[singleSW] length :"

    #@16a
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16d
    move-result-object v13

    #@16e
    array-length v14, v9

    #@16f
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@172
    move-result-object v13

    #@173
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@176
    move-result-object v13

    #@177
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@17a
    .line 6318
    if-eqz v9, :cond_182

    #@17c
    array-length v12, v9

    #@17d
    if-lez v12, :cond_182

    #@17f
    .line 6319
    const/4 v12, 0x0

    #@180
    aget-object v8, v9, v12

    #@182
    .line 6321
    :cond_182
    if-eqz v9, :cond_18b

    #@184
    array-length v12, v9

    #@185
    const/4 v13, 0x1

    #@186
    if-le v12, v13, :cond_18b

    #@188
    .line 6322
    const/4 v12, 0x1

    #@189
    aget-object v7, v9, v12

    #@18b
    .line 6325
    .end local v9           #opinfo:[Ljava/lang/String;
    :cond_18b
    const-string v12, "Telephony"

    #@18d
    new-instance v13, Ljava/lang/StringBuilder;

    #@18f
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@192
    const-string v14, "[singleSW] OperatorIMSI: "

    #@194
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@197
    move-result-object v13

    #@198
    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19b
    move-result-object v13

    #@19c
    const-string v14, "  operatorGid : "

    #@19e
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a1
    move-result-object v13

    #@1a2
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a5
    move-result-object v13

    #@1a6
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a9
    move-result-object v13

    #@1aa
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1ad
    .line 6326
    if-eqz v8, :cond_1b1

    #@1af
    if-nez v7, :cond_1b4

    #@1b1
    .line 6327
    :cond_1b1
    const/4 v12, 0x0

    #@1b2
    goto/16 :goto_16

    #@1b4
    .line 6329
    :cond_1b4
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@1b7
    move-result v12

    #@1b8
    const/4 v13, 0x6

    #@1b9
    if-lt v12, v13, :cond_1c1

    #@1bb
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@1be
    move-result v12

    #@1bf
    if-nez v12, :cond_1c4

    #@1c1
    .line 6330
    :cond_1c1
    const/4 v12, 0x0

    #@1c2
    goto/16 :goto_16

    #@1c4
    .line 6334
    :cond_1c4
    const-string v12, "45000"

    #@1c6
    move-object/from16 v0, p1

    #@1c8
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1cb
    move-result v12

    #@1cc
    if-eqz v12, :cond_21f

    #@1ce
    const/4 v12, 0x0

    #@1cf
    const/4 v13, 0x5

    #@1d0
    invoke-virtual {v8, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1d3
    move-result-object v12

    #@1d4
    const-string v13, "45000"

    #@1d6
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d9
    move-result v12

    #@1da
    if-eqz v12, :cond_21f

    #@1dc
    .line 6335
    const-string v12, "Telephony"

    #@1de
    new-instance v13, Ljava/lang/StringBuilder;

    #@1e0
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@1e3
    const-string v14, "title: "

    #@1e5
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e8
    move-result-object v13

    #@1e9
    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ec
    move-result-object v13

    #@1ed
    const-string v14, ", numeric: "

    #@1ef
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f2
    move-result-object v13

    #@1f3
    move-object/from16 v0, p1

    #@1f5
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f8
    move-result-object v13

    #@1f9
    const-string v14, ", extraid: "

    #@1fb
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fe
    move-result-object v13

    #@1ff
    move-object/from16 v0, p2

    #@201
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@204
    move-result-object v13

    #@205
    const-string v14, ", operatorGid:"

    #@207
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20a
    move-result-object v13

    #@20b
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20e
    move-result-object v13

    #@20f
    const-string v14, " is added"

    #@211
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@214
    move-result-object v13

    #@215
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@218
    move-result-object v13

    #@219
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@21c
    .line 6336
    const/4 v12, 0x1

    #@21d
    goto/16 :goto_16

    #@21f
    .line 6340
    :cond_21f
    const-string v12, "302370"

    #@221
    move-object/from16 v0, p1

    #@223
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@226
    move-result v12

    #@227
    if-eqz v12, :cond_292

    #@229
    const/4 v12, 0x0

    #@22a
    const/4 v13, 0x6

    #@22b
    invoke-virtual {v8, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@22e
    move-result-object v12

    #@22f
    const-string v13, "302370"

    #@231
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@234
    move-result v12

    #@235
    if-eqz v12, :cond_292

    #@237
    const-string v12, "302370XXX"

    #@239
    move-object/from16 v0, p2

    #@23b
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23e
    move-result v12

    #@23f
    if-eqz v12, :cond_292

    #@241
    const/4 v12, 0x0

    #@242
    const/4 v13, 0x2

    #@243
    invoke-virtual {v7, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@246
    move-result-object v12

    #@247
    const-string v13, "2C"

    #@249
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24c
    move-result v12

    #@24d
    if-nez v12, :cond_292

    #@24f
    .line 6341
    const-string v12, "Telephony"

    #@251
    new-instance v13, Ljava/lang/StringBuilder;

    #@253
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@256
    const-string v14, "title: "

    #@258
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25b
    move-result-object v13

    #@25c
    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25f
    move-result-object v13

    #@260
    const-string v14, ", numeric: "

    #@262
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@265
    move-result-object v13

    #@266
    move-object/from16 v0, p1

    #@268
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26b
    move-result-object v13

    #@26c
    const-string v14, ", extraid: "

    #@26e
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@271
    move-result-object v13

    #@272
    move-object/from16 v0, p2

    #@274
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@277
    move-result-object v13

    #@278
    const-string v14, ", operatorGid:"

    #@27a
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27d
    move-result-object v13

    #@27e
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@281
    move-result-object v13

    #@282
    const-string v14, " is added"

    #@284
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@287
    move-result-object v13

    #@288
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28b
    move-result-object v13

    #@28c
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@28f
    .line 6342
    const/4 v12, 0x1

    #@290
    goto/16 :goto_16

    #@292
    .line 6343
    :cond_292
    const-string v12, "302370"

    #@294
    move-object/from16 v0, p1

    #@296
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@299
    move-result v12

    #@29a
    if-eqz v12, :cond_305

    #@29c
    const/4 v12, 0x0

    #@29d
    const/4 v13, 0x6

    #@29e
    invoke-virtual {v8, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2a1
    move-result-object v12

    #@2a2
    const-string v13, "302370"

    #@2a4
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a7
    move-result v12

    #@2a8
    if-eqz v12, :cond_305

    #@2aa
    const-string v12, "2C"

    #@2ac
    move-object/from16 v0, p2

    #@2ae
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b1
    move-result v12

    #@2b2
    if-eqz v12, :cond_305

    #@2b4
    const/4 v12, 0x0

    #@2b5
    const/4 v13, 0x2

    #@2b6
    invoke-virtual {v7, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2b9
    move-result-object v12

    #@2ba
    const-string v13, "2C"

    #@2bc
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2bf
    move-result v12

    #@2c0
    if-eqz v12, :cond_305

    #@2c2
    .line 6344
    const-string v12, "Telephony"

    #@2c4
    new-instance v13, Ljava/lang/StringBuilder;

    #@2c6
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@2c9
    const-string v14, "title: "

    #@2cb
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ce
    move-result-object v13

    #@2cf
    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d2
    move-result-object v13

    #@2d3
    const-string v14, ", numeric: "

    #@2d5
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d8
    move-result-object v13

    #@2d9
    move-object/from16 v0, p1

    #@2db
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2de
    move-result-object v13

    #@2df
    const-string v14, ", extraid: "

    #@2e1
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e4
    move-result-object v13

    #@2e5
    move-object/from16 v0, p2

    #@2e7
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ea
    move-result-object v13

    #@2eb
    const-string v14, ", operatorGid:"

    #@2ed
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f0
    move-result-object v13

    #@2f1
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f4
    move-result-object v13

    #@2f5
    const-string v14, " is added"

    #@2f7
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2fa
    move-result-object v13

    #@2fb
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2fe
    move-result-object v13

    #@2ff
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@302
    .line 6345
    const/4 v12, 0x1

    #@303
    goto/16 :goto_16

    #@305
    .line 6346
    :cond_305
    const-string v12, "302660"

    #@307
    move-object/from16 v0, p1

    #@309
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30c
    move-result v12

    #@30d
    if-eqz v12, :cond_378

    #@30f
    const/4 v12, 0x0

    #@310
    const/4 v13, 0x6

    #@311
    invoke-virtual {v8, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@314
    move-result-object v12

    #@315
    const-string v13, "302660"

    #@317
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31a
    move-result v12

    #@31b
    if-eqz v12, :cond_378

    #@31d
    const-string v12, "2C"

    #@31f
    move-object/from16 v0, p2

    #@321
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@324
    move-result v12

    #@325
    if-eqz v12, :cond_378

    #@327
    const/4 v12, 0x0

    #@328
    const/4 v13, 0x2

    #@329
    invoke-virtual {v7, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@32c
    move-result-object v12

    #@32d
    const-string v13, "2C"

    #@32f
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@332
    move-result v12

    #@333
    if-eqz v12, :cond_378

    #@335
    .line 6347
    const-string v12, "Telephony"

    #@337
    new-instance v13, Ljava/lang/StringBuilder;

    #@339
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@33c
    const-string v14, "title: "

    #@33e
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@341
    move-result-object v13

    #@342
    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@345
    move-result-object v13

    #@346
    const-string v14, ", numeric: "

    #@348
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34b
    move-result-object v13

    #@34c
    move-object/from16 v0, p1

    #@34e
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@351
    move-result-object v13

    #@352
    const-string v14, ", extraid: "

    #@354
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@357
    move-result-object v13

    #@358
    move-object/from16 v0, p2

    #@35a
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35d
    move-result-object v13

    #@35e
    const-string v14, ", operatorGid:"

    #@360
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@363
    move-result-object v13

    #@364
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@367
    move-result-object v13

    #@368
    const-string v14, " is added"

    #@36a
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36d
    move-result-object v13

    #@36e
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@371
    move-result-object v13

    #@372
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@375
    .line 6348
    const/4 v12, 0x1

    #@376
    goto/16 :goto_16

    #@378
    .line 6349
    :cond_378
    const-string v12, "302720XXX"

    #@37a
    move-object/from16 v0, p2

    #@37c
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@37f
    move-result v12

    #@380
    if-eqz v12, :cond_3ec

    #@382
    const/4 v12, 0x0

    #@383
    const/4 v13, 0x6

    #@384
    invoke-virtual {v8, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@387
    move-result-object v12

    #@388
    const-string v13, "302720"

    #@38a
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38d
    move-result v12

    #@38e
    if-eqz v12, :cond_3ec

    #@390
    const/4 v12, 0x7

    #@391
    const/16 v13, 0x9

    #@393
    invoke-virtual {v8, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@396
    move-result-object v12

    #@397
    const-string v13, "94"

    #@399
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39c
    move-result v12

    #@39d
    if-nez v12, :cond_3ec

    #@39f
    .line 6350
    const-string v12, "Telephony"

    #@3a1
    new-instance v13, Ljava/lang/StringBuilder;

    #@3a3
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@3a6
    const-string v14, "title: "

    #@3a8
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3ab
    move-result-object v13

    #@3ac
    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3af
    move-result-object v13

    #@3b0
    const-string v14, ", numeric: "

    #@3b2
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b5
    move-result-object v13

    #@3b6
    move-object/from16 v0, p1

    #@3b8
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3bb
    move-result-object v13

    #@3bc
    const-string v14, ", extraid: "

    #@3be
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c1
    move-result-object v13

    #@3c2
    move-object/from16 v0, p2

    #@3c4
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c7
    move-result-object v13

    #@3c8
    const-string v14, ", operatorIMSI:"

    #@3ca
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3cd
    move-result-object v13

    #@3ce
    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d1
    move-result-object v13

    #@3d2
    const-string v14, ", operatorGid:"

    #@3d4
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d7
    move-result-object v13

    #@3d8
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3db
    move-result-object v13

    #@3dc
    const-string v14, " is added"

    #@3de
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e1
    move-result-object v13

    #@3e2
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e5
    move-result-object v13

    #@3e6
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3e9
    .line 6351
    const/4 v12, 0x1

    #@3ea
    goto/16 :goto_16

    #@3ec
    .line 6352
    :cond_3ec
    const-string v12, "302720X94"

    #@3ee
    move-object/from16 v0, p2

    #@3f0
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3f3
    move-result v12

    #@3f4
    if-eqz v12, :cond_460

    #@3f6
    const/4 v12, 0x0

    #@3f7
    const/4 v13, 0x6

    #@3f8
    invoke-virtual {v8, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3fb
    move-result-object v12

    #@3fc
    const-string v13, "302720"

    #@3fe
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@401
    move-result v12

    #@402
    if-eqz v12, :cond_460

    #@404
    const/4 v12, 0x7

    #@405
    const/16 v13, 0x9

    #@407
    invoke-virtual {v8, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@40a
    move-result-object v12

    #@40b
    const-string v13, "94"

    #@40d
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@410
    move-result v12

    #@411
    if-eqz v12, :cond_460

    #@413
    .line 6353
    const-string v12, "Telephony"

    #@415
    new-instance v13, Ljava/lang/StringBuilder;

    #@417
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@41a
    const-string v14, "title: "

    #@41c
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41f
    move-result-object v13

    #@420
    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@423
    move-result-object v13

    #@424
    const-string v14, ", numeric: "

    #@426
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@429
    move-result-object v13

    #@42a
    move-object/from16 v0, p1

    #@42c
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42f
    move-result-object v13

    #@430
    const-string v14, ", extraid: "

    #@432
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@435
    move-result-object v13

    #@436
    move-object/from16 v0, p2

    #@438
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43b
    move-result-object v13

    #@43c
    const-string v14, ", operatorIMSI:"

    #@43e
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@441
    move-result-object v13

    #@442
    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@445
    move-result-object v13

    #@446
    const-string v14, ", operatorGid:"

    #@448
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44b
    move-result-object v13

    #@44c
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44f
    move-result-object v13

    #@450
    const-string v14, " is added"

    #@452
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@455
    move-result-object v13

    #@456
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@459
    move-result-object v13

    #@45a
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@45d
    .line 6354
    const/4 v12, 0x1

    #@45e
    goto/16 :goto_16

    #@460
    .line 6356
    :cond_460
    const/4 v12, 0x0

    #@461
    goto/16 :goto_16

    #@463
    .line 6359
    .end local v3           #firstopsim:Ljava/lang/String;
    .end local v7           #operatorGid:Ljava/lang/String;
    .end local v8           #operatorIMSI:Ljava/lang/String;
    :cond_463
    const/4 v12, 0x0

    #@464
    goto/16 :goto_16

    #@466
    .line 6362
    :cond_466
    const-string v12, "BELL"

    #@468
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@46b
    move-result v12

    #@46c
    if-eqz v12, :cond_6ea

    #@46e
    .line 6363
    const-string v12, "302610"

    #@470
    move-object/from16 v0, p1

    #@472
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@475
    move-result v12

    #@476
    if-nez v12, :cond_496

    #@478
    const-string v12, "302780"

    #@47a
    move-object/from16 v0, p1

    #@47c
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@47f
    move-result v12

    #@480
    if-nez v12, :cond_496

    #@482
    const-string v12, "45000"

    #@484
    move-object/from16 v0, p1

    #@486
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@489
    move-result v12

    #@48a
    if-nez v12, :cond_496

    #@48c
    const-string v12, "20404"

    #@48e
    move-object/from16 v0, p1

    #@490
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@493
    move-result v12

    #@494
    if-eqz v12, :cond_6e7

    #@496
    .line 6365
    :cond_496
    const-string v12, "gsm.sim.operator.gid"

    #@498
    const-string v13, ""

    #@49a
    invoke-static {v12, v13}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@49d
    move-result-object v7

    #@49e
    .line 6366
    .restart local v7       #operatorGid:Ljava/lang/String;
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@4a1
    move-result-object v12

    #@4a2
    invoke-virtual {v12}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    #@4a5
    move-result-object v8

    #@4a6
    .line 6367
    .restart local v8       #operatorIMSI:Ljava/lang/String;
    const-string v12, "persist.lg.data.firstopsim"

    #@4a8
    const-string v13, ""

    #@4aa
    invoke-static {v12, v13}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@4ad
    move-result-object v3

    #@4ae
    .line 6368
    .restart local v3       #firstopsim:Ljava/lang/String;
    const-string v12, "Telephony"

    #@4b0
    new-instance v13, Ljava/lang/StringBuilder;

    #@4b2
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@4b5
    const-string v14, "[singleSW] firstopsim: "

    #@4b7
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4ba
    move-result-object v13

    #@4bb
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4be
    move-result-object v13

    #@4bf
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c2
    move-result-object v13

    #@4c3
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4c6
    .line 6369
    if-eqz v3, :cond_504

    #@4c8
    const-string v12, "null"

    #@4ca
    invoke-virtual {v3, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4cd
    move-result v12

    #@4ce
    if-nez v12, :cond_504

    #@4d0
    .line 6370
    const-string v7, ""

    #@4d2
    .line 6371
    const-string v8, ""

    #@4d4
    .line 6372
    const-string v12, ";"

    #@4d6
    invoke-virtual {v3, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@4d9
    move-result-object v9

    #@4da
    .line 6373
    .restart local v9       #opinfo:[Ljava/lang/String;
    const-string v12, "Telephony"

    #@4dc
    new-instance v13, Ljava/lang/StringBuilder;

    #@4de
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@4e1
    const-string v14, "[singleSW] length :"

    #@4e3
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e6
    move-result-object v13

    #@4e7
    array-length v14, v9

    #@4e8
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4eb
    move-result-object v13

    #@4ec
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4ef
    move-result-object v13

    #@4f0
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4f3
    .line 6374
    if-eqz v9, :cond_4fb

    #@4f5
    array-length v12, v9

    #@4f6
    if-lez v12, :cond_4fb

    #@4f8
    .line 6375
    const/4 v12, 0x0

    #@4f9
    aget-object v8, v9, v12

    #@4fb
    .line 6377
    :cond_4fb
    if-eqz v9, :cond_504

    #@4fd
    array-length v12, v9

    #@4fe
    const/4 v13, 0x1

    #@4ff
    if-le v12, v13, :cond_504

    #@501
    .line 6378
    const/4 v12, 0x1

    #@502
    aget-object v7, v9, v12

    #@504
    .line 6381
    .end local v9           #opinfo:[Ljava/lang/String;
    :cond_504
    const-string v12, "Telephony"

    #@506
    new-instance v13, Ljava/lang/StringBuilder;

    #@508
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@50b
    const-string v14, "[singleSW] OperatorIMSI: "

    #@50d
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@510
    move-result-object v13

    #@511
    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@514
    move-result-object v13

    #@515
    const-string v14, "  operatorGid : "

    #@517
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51a
    move-result-object v13

    #@51b
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51e
    move-result-object v13

    #@51f
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@522
    move-result-object v13

    #@523
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@526
    .line 6383
    if-eqz v8, :cond_52a

    #@528
    if-nez v7, :cond_52d

    #@52a
    .line 6384
    :cond_52a
    const/4 v12, 0x0

    #@52b
    goto/16 :goto_16

    #@52d
    .line 6386
    :cond_52d
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@530
    move-result v12

    #@531
    const/4 v13, 0x6

    #@532
    if-lt v12, v13, :cond_53a

    #@534
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@537
    move-result v12

    #@538
    if-nez v12, :cond_53d

    #@53a
    .line 6387
    :cond_53a
    const/4 v12, 0x0

    #@53b
    goto/16 :goto_16

    #@53d
    .line 6392
    :cond_53d
    const-string v12, "45000"

    #@53f
    move-object/from16 v0, p1

    #@541
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@544
    move-result v12

    #@545
    if-eqz v12, :cond_598

    #@547
    const/4 v12, 0x0

    #@548
    const/4 v13, 0x5

    #@549
    invoke-virtual {v8, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@54c
    move-result-object v12

    #@54d
    const-string v13, "45000"

    #@54f
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@552
    move-result v12

    #@553
    if-eqz v12, :cond_598

    #@555
    .line 6393
    const-string v12, "Telephony"

    #@557
    new-instance v13, Ljava/lang/StringBuilder;

    #@559
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@55c
    const-string v14, "title: "

    #@55e
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@561
    move-result-object v13

    #@562
    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@565
    move-result-object v13

    #@566
    const-string v14, ", numeric: "

    #@568
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56b
    move-result-object v13

    #@56c
    move-object/from16 v0, p1

    #@56e
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@571
    move-result-object v13

    #@572
    const-string v14, ", extraid: "

    #@574
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@577
    move-result-object v13

    #@578
    move-object/from16 v0, p2

    #@57a
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57d
    move-result-object v13

    #@57e
    const-string v14, ", operatorGid:"

    #@580
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@583
    move-result-object v13

    #@584
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@587
    move-result-object v13

    #@588
    const-string v14, " is added"

    #@58a
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58d
    move-result-object v13

    #@58e
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@591
    move-result-object v13

    #@592
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@595
    .line 6394
    const/4 v12, 0x1

    #@596
    goto/16 :goto_16

    #@598
    .line 6397
    :cond_598
    const-string v12, "302610"

    #@59a
    move-object/from16 v0, p1

    #@59c
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@59f
    move-result v12

    #@5a0
    if-eqz v12, :cond_641

    #@5a2
    const/4 v12, 0x0

    #@5a3
    const/4 v13, 0x6

    #@5a4
    invoke-virtual {v8, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@5a7
    move-result-object v12

    #@5a8
    const-string v13, "302610"

    #@5aa
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5ad
    move-result v12

    #@5ae
    if-eqz v12, :cond_641

    #@5b0
    .line 6398
    if-eqz p2, :cond_5b8

    #@5b2
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    #@5b5
    move-result v12

    #@5b6
    if-nez v12, :cond_5bb

    #@5b8
    .line 6399
    :cond_5b8
    const/4 v12, 0x0

    #@5b9
    goto/16 :goto_16

    #@5bb
    .line 6401
    :cond_5bb
    const/4 v12, 0x0

    #@5bc
    const/4 v13, 0x2

    #@5bd
    invoke-virtual {v7, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@5c0
    move-result-object v12

    #@5c1
    move-object/from16 v0, p2

    #@5c3
    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5c6
    move-result v12

    #@5c7
    if-eqz v12, :cond_63e

    #@5c9
    const-string v12, "3D"

    #@5cb
    move-object/from16 v0, p2

    #@5cd
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5d0
    move-result v12

    #@5d1
    if-nez v12, :cond_5f1

    #@5d3
    const-string v12, "3E"

    #@5d5
    move-object/from16 v0, p2

    #@5d7
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5da
    move-result v12

    #@5db
    if-nez v12, :cond_5f1

    #@5dd
    const-string v12, "3F"

    #@5df
    move-object/from16 v0, p2

    #@5e1
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5e4
    move-result v12

    #@5e5
    if-nez v12, :cond_5f1

    #@5e7
    const-string v12, "40"

    #@5e9
    move-object/from16 v0, p2

    #@5eb
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5ee
    move-result v12

    #@5ef
    if-eqz v12, :cond_63e

    #@5f1
    .line 6403
    :cond_5f1
    const-string v12, "Telephony"

    #@5f3
    new-instance v13, Ljava/lang/StringBuilder;

    #@5f5
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@5f8
    const-string v14, "title: "

    #@5fa
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5fd
    move-result-object v13

    #@5fe
    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@601
    move-result-object v13

    #@602
    const-string v14, ", "

    #@604
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@607
    move-result-object v13

    #@608
    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60b
    move-result-object v13

    #@60c
    const-string v14, " "

    #@60e
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@611
    move-result-object v13

    #@612
    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@615
    move-result-object v13

    #@616
    const-string v14, ", numeric: "

    #@618
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61b
    move-result-object v13

    #@61c
    move-object/from16 v0, p1

    #@61e
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@621
    move-result-object v13

    #@622
    const-string v14, ", extraid : "

    #@624
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@627
    move-result-object v13

    #@628
    move-object/from16 v0, p2

    #@62a
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62d
    move-result-object v13

    #@62e
    const-string v14, " is added"

    #@630
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@633
    move-result-object v13

    #@634
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@637
    move-result-object v13

    #@638
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@63b
    .line 6404
    const/4 v12, 0x1

    #@63c
    goto/16 :goto_16

    #@63e
    .line 6406
    :cond_63e
    const/4 v12, 0x0

    #@63f
    goto/16 :goto_16

    #@641
    .line 6408
    :cond_641
    const-string v12, "302780"

    #@643
    move-object/from16 v0, p1

    #@645
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@648
    move-result v12

    #@649
    if-nez v12, :cond_655

    #@64b
    const-string v12, "20404"

    #@64d
    move-object/from16 v0, p1

    #@64f
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@652
    move-result v12

    #@653
    if-eqz v12, :cond_6e4

    #@655
    :cond_655
    const/4 v12, 0x0

    #@656
    const/4 v13, 0x6

    #@657
    invoke-virtual {v8, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@65a
    move-result-object v12

    #@65b
    const-string v13, "302780"

    #@65d
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@660
    move-result v12

    #@661
    if-nez v12, :cond_671

    #@663
    const/4 v12, 0x0

    #@664
    const/4 v13, 0x5

    #@665
    invoke-virtual {v8, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@668
    move-result-object v12

    #@669
    const-string v13, "20404"

    #@66b
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@66e
    move-result v12

    #@66f
    if-eqz v12, :cond_6e4

    #@671
    .line 6409
    :cond_671
    if-eqz p2, :cond_679

    #@673
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    #@676
    move-result v12

    #@677
    if-nez v12, :cond_67c

    #@679
    .line 6410
    :cond_679
    const/4 v12, 0x0

    #@67a
    goto/16 :goto_16

    #@67c
    .line 6412
    :cond_67c
    const/4 v12, 0x0

    #@67d
    const/4 v13, 0x2

    #@67e
    invoke-virtual {v7, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@681
    move-result-object v12

    #@682
    move-object/from16 v0, p2

    #@684
    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@687
    move-result v12

    #@688
    if-eqz v12, :cond_6e1

    #@68a
    const-string v12, "5A"

    #@68c
    move-object/from16 v0, p2

    #@68e
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@691
    move-result v12

    #@692
    if-eqz v12, :cond_6e1

    #@694
    .line 6413
    const-string v12, "Telephony"

    #@696
    new-instance v13, Ljava/lang/StringBuilder;

    #@698
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@69b
    const-string v14, "title: "

    #@69d
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a0
    move-result-object v13

    #@6a1
    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a4
    move-result-object v13

    #@6a5
    const-string v14, ", "

    #@6a7
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6aa
    move-result-object v13

    #@6ab
    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6ae
    move-result-object v13

    #@6af
    const-string v14, " "

    #@6b1
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b4
    move-result-object v13

    #@6b5
    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b8
    move-result-object v13

    #@6b9
    const-string v14, ", numeric: "

    #@6bb
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6be
    move-result-object v13

    #@6bf
    move-object/from16 v0, p1

    #@6c1
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c4
    move-result-object v13

    #@6c5
    const-string v14, ", extraid : "

    #@6c7
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6ca
    move-result-object v13

    #@6cb
    move-object/from16 v0, p2

    #@6cd
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d0
    move-result-object v13

    #@6d1
    const-string v14, " is added"

    #@6d3
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d6
    move-result-object v13

    #@6d7
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6da
    move-result-object v13

    #@6db
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@6de
    .line 6414
    const/4 v12, 0x1

    #@6df
    goto/16 :goto_16

    #@6e1
    .line 6416
    :cond_6e1
    const/4 v12, 0x0

    #@6e2
    goto/16 :goto_16

    #@6e4
    .line 6419
    :cond_6e4
    const/4 v12, 0x0

    #@6e5
    goto/16 :goto_16

    #@6e7
    .line 6423
    .end local v3           #firstopsim:Ljava/lang/String;
    .end local v7           #operatorGid:Ljava/lang/String;
    .end local v8           #operatorIMSI:Ljava/lang/String;
    :cond_6e7
    const/4 v12, 0x0

    #@6e8
    goto/16 :goto_16

    #@6ea
    .line 6427
    :cond_6ea
    const-string v12, "TLS"

    #@6ec
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6ef
    move-result v12

    #@6f0
    if-eqz v12, :cond_8d9

    #@6f2
    .line 6428
    const-string v12, "302220"

    #@6f4
    move-object/from16 v0, p1

    #@6f6
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6f9
    move-result v12

    #@6fa
    if-nez v12, :cond_710

    #@6fc
    const-string v12, "302221"

    #@6fe
    move-object/from16 v0, p1

    #@700
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@703
    move-result v12

    #@704
    if-nez v12, :cond_710

    #@706
    const-string v12, "45000"

    #@708
    move-object/from16 v0, p1

    #@70a
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@70d
    move-result v12

    #@70e
    if-eqz v12, :cond_8d6

    #@710
    .line 6430
    :cond_710
    const-string v12, "gsm.sim.operator.gid"

    #@712
    const-string v13, ""

    #@714
    invoke-static {v12, v13}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@717
    move-result-object v7

    #@718
    .line 6431
    .restart local v7       #operatorGid:Ljava/lang/String;
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@71b
    move-result-object v12

    #@71c
    invoke-virtual {v12}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    #@71f
    move-result-object v8

    #@720
    .line 6433
    .restart local v8       #operatorIMSI:Ljava/lang/String;
    const-string v12, "persist.lg.data.firstopsim"

    #@722
    const-string v13, ""

    #@724
    invoke-static {v12, v13}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@727
    move-result-object v3

    #@728
    .line 6434
    .restart local v3       #firstopsim:Ljava/lang/String;
    const-string v12, "Telephony"

    #@72a
    new-instance v13, Ljava/lang/StringBuilder;

    #@72c
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@72f
    const-string v14, "[singleSW] firstopsim: "

    #@731
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@734
    move-result-object v13

    #@735
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@738
    move-result-object v13

    #@739
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73c
    move-result-object v13

    #@73d
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@740
    .line 6435
    if-eqz v3, :cond_77e

    #@742
    const-string v12, "null"

    #@744
    invoke-virtual {v3, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@747
    move-result v12

    #@748
    if-nez v12, :cond_77e

    #@74a
    .line 6436
    const-string v7, ""

    #@74c
    .line 6437
    const-string v8, ""

    #@74e
    .line 6438
    const-string v12, ";"

    #@750
    invoke-virtual {v3, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@753
    move-result-object v9

    #@754
    .line 6439
    .restart local v9       #opinfo:[Ljava/lang/String;
    const-string v12, "Telephony"

    #@756
    new-instance v13, Ljava/lang/StringBuilder;

    #@758
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@75b
    const-string v14, "[singleSW] length :"

    #@75d
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@760
    move-result-object v13

    #@761
    array-length v14, v9

    #@762
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@765
    move-result-object v13

    #@766
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@769
    move-result-object v13

    #@76a
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@76d
    .line 6440
    if-eqz v9, :cond_775

    #@76f
    array-length v12, v9

    #@770
    if-lez v12, :cond_775

    #@772
    .line 6441
    const/4 v12, 0x0

    #@773
    aget-object v8, v9, v12

    #@775
    .line 6443
    :cond_775
    if-eqz v9, :cond_77e

    #@777
    array-length v12, v9

    #@778
    const/4 v13, 0x1

    #@779
    if-le v12, v13, :cond_77e

    #@77b
    .line 6444
    const/4 v12, 0x1

    #@77c
    aget-object v7, v9, v12

    #@77e
    .line 6447
    .end local v9           #opinfo:[Ljava/lang/String;
    :cond_77e
    const-string v12, "Telephony"

    #@780
    new-instance v13, Ljava/lang/StringBuilder;

    #@782
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@785
    const-string v14, "[singleSW] OperatorIMSI: "

    #@787
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78a
    move-result-object v13

    #@78b
    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78e
    move-result-object v13

    #@78f
    const-string v14, "  operatorGid : "

    #@791
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@794
    move-result-object v13

    #@795
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@798
    move-result-object v13

    #@799
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79c
    move-result-object v13

    #@79d
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7a0
    .line 6449
    if-eqz v8, :cond_7a4

    #@7a2
    if-nez v7, :cond_7a7

    #@7a4
    .line 6450
    :cond_7a4
    const/4 v12, 0x0

    #@7a5
    goto/16 :goto_16

    #@7a7
    .line 6452
    :cond_7a7
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@7aa
    move-result v12

    #@7ab
    const/4 v13, 0x6

    #@7ac
    if-lt v12, v13, :cond_7b4

    #@7ae
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@7b1
    move-result v12

    #@7b2
    if-nez v12, :cond_7b7

    #@7b4
    .line 6453
    :cond_7b4
    const/4 v12, 0x0

    #@7b5
    goto/16 :goto_16

    #@7b7
    .line 6457
    :cond_7b7
    const-string v12, "45000"

    #@7b9
    move-object/from16 v0, p1

    #@7bb
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7be
    move-result v12

    #@7bf
    if-eqz v12, :cond_812

    #@7c1
    const/4 v12, 0x0

    #@7c2
    const/4 v13, 0x5

    #@7c3
    invoke-virtual {v8, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@7c6
    move-result-object v12

    #@7c7
    const-string v13, "45000"

    #@7c9
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7cc
    move-result v12

    #@7cd
    if-eqz v12, :cond_812

    #@7cf
    .line 6458
    const-string v12, "Telephony"

    #@7d1
    new-instance v13, Ljava/lang/StringBuilder;

    #@7d3
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@7d6
    const-string v14, "title: "

    #@7d8
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7db
    move-result-object v13

    #@7dc
    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7df
    move-result-object v13

    #@7e0
    const-string v14, ", numeric: "

    #@7e2
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e5
    move-result-object v13

    #@7e6
    move-object/from16 v0, p1

    #@7e8
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7eb
    move-result-object v13

    #@7ec
    const-string v14, ", extraid: "

    #@7ee
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f1
    move-result-object v13

    #@7f2
    move-object/from16 v0, p2

    #@7f4
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f7
    move-result-object v13

    #@7f8
    const-string v14, ", operatorGid:"

    #@7fa
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7fd
    move-result-object v13

    #@7fe
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@801
    move-result-object v13

    #@802
    const-string v14, " is added"

    #@804
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@807
    move-result-object v13

    #@808
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80b
    move-result-object v13

    #@80c
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@80f
    .line 6459
    const/4 v12, 0x1

    #@810
    goto/16 :goto_16

    #@812
    .line 6462
    :cond_812
    const-string v12, "302220"

    #@814
    move-object/from16 v0, p1

    #@816
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@819
    move-result v12

    #@81a
    if-eqz v12, :cond_82a

    #@81c
    const/4 v12, 0x0

    #@81d
    const/4 v13, 0x6

    #@81e
    invoke-virtual {v8, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@821
    move-result-object v12

    #@822
    const-string v13, "302220"

    #@824
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@827
    move-result v12

    #@828
    if-nez v12, :cond_842

    #@82a
    :cond_82a
    const-string v12, "302221"

    #@82c
    move-object/from16 v0, p1

    #@82e
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@831
    move-result v12

    #@832
    if-eqz v12, :cond_8d3

    #@834
    const/4 v12, 0x0

    #@835
    const/4 v13, 0x6

    #@836
    invoke-virtual {v8, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@839
    move-result-object v12

    #@83a
    const-string v13, "302221"

    #@83c
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@83f
    move-result v12

    #@840
    if-eqz v12, :cond_8d3

    #@842
    .line 6464
    :cond_842
    if-eqz p2, :cond_84a

    #@844
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    #@847
    move-result v12

    #@848
    if-nez v12, :cond_84d

    #@84a
    .line 6465
    :cond_84a
    const/4 v12, 0x0

    #@84b
    goto/16 :goto_16

    #@84d
    .line 6468
    :cond_84d
    const/4 v12, 0x0

    #@84e
    const/4 v13, 0x2

    #@84f
    invoke-virtual {v7, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@852
    move-result-object v12

    #@853
    move-object/from16 v0, p2

    #@855
    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@858
    move-result v12

    #@859
    if-eqz v12, :cond_8d0

    #@85b
    const-string v12, "54"

    #@85d
    move-object/from16 v0, p2

    #@85f
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@862
    move-result v12

    #@863
    if-nez v12, :cond_883

    #@865
    const-string v12, "4B"

    #@867
    move-object/from16 v0, p2

    #@869
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@86c
    move-result v12

    #@86d
    if-nez v12, :cond_883

    #@86f
    const-string v12, "50"

    #@871
    move-object/from16 v0, p2

    #@873
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@876
    move-result v12

    #@877
    if-nez v12, :cond_883

    #@879
    const-string v12, "43"

    #@87b
    move-object/from16 v0, p2

    #@87d
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@880
    move-result v12

    #@881
    if-eqz v12, :cond_8d0

    #@883
    .line 6470
    :cond_883
    const-string v12, "Telephony"

    #@885
    new-instance v13, Ljava/lang/StringBuilder;

    #@887
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@88a
    const-string v14, "title: "

    #@88c
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88f
    move-result-object v13

    #@890
    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@893
    move-result-object v13

    #@894
    const-string v14, ", "

    #@896
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@899
    move-result-object v13

    #@89a
    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89d
    move-result-object v13

    #@89e
    const-string v14, " "

    #@8a0
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a3
    move-result-object v13

    #@8a4
    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a7
    move-result-object v13

    #@8a8
    const-string v14, ", numeric: "

    #@8aa
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8ad
    move-result-object v13

    #@8ae
    move-object/from16 v0, p1

    #@8b0
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b3
    move-result-object v13

    #@8b4
    const-string v14, ", extraid : "

    #@8b6
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b9
    move-result-object v13

    #@8ba
    move-object/from16 v0, p2

    #@8bc
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8bf
    move-result-object v13

    #@8c0
    const-string v14, " is added"

    #@8c2
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c5
    move-result-object v13

    #@8c6
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8c9
    move-result-object v13

    #@8ca
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@8cd
    .line 6471
    const/4 v12, 0x1

    #@8ce
    goto/16 :goto_16

    #@8d0
    .line 6473
    :cond_8d0
    const/4 v12, 0x0

    #@8d1
    goto/16 :goto_16

    #@8d3
    .line 6476
    :cond_8d3
    const/4 v12, 0x0

    #@8d4
    goto/16 :goto_16

    #@8d6
    .line 6479
    .end local v3           #firstopsim:Ljava/lang/String;
    .end local v7           #operatorGid:Ljava/lang/String;
    .end local v8           #operatorIMSI:Ljava/lang/String;
    :cond_8d6
    const/4 v12, 0x0

    #@8d7
    goto/16 :goto_16

    #@8d9
    .line 6483
    :cond_8d9
    const-string v12, "VTR"

    #@8db
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8de
    move-result v12

    #@8df
    if-eqz v12, :cond_b79

    #@8e1
    .line 6484
    const-string v12, "302500"

    #@8e3
    move-object/from16 v0, p1

    #@8e5
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8e8
    move-result v12

    #@8e9
    if-nez v12, :cond_927

    #@8eb
    const-string v12, "20404"

    #@8ed
    move-object/from16 v0, p1

    #@8ef
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8f2
    move-result v12

    #@8f3
    if-nez v12, :cond_927

    #@8f5
    const-string v12, "302510"

    #@8f7
    move-object/from16 v0, p1

    #@8f9
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8fc
    move-result v12

    #@8fd
    if-nez v12, :cond_927

    #@8ff
    const-string v12, "302490"

    #@901
    move-object/from16 v0, p1

    #@903
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@906
    move-result v12

    #@907
    if-nez v12, :cond_927

    #@909
    const-string v12, "22201"

    #@90b
    move-object/from16 v0, p1

    #@90d
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@910
    move-result v12

    #@911
    if-nez v12, :cond_927

    #@913
    const-string v12, "22288"

    #@915
    move-object/from16 v0, p1

    #@917
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@91a
    move-result v12

    #@91b
    if-nez v12, :cond_927

    #@91d
    const-string v12, "45000"

    #@91f
    move-object/from16 v0, p1

    #@921
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@924
    move-result v12

    #@925
    if-eqz v12, :cond_b76

    #@927
    .line 6486
    :cond_927
    const-string v12, "gsm.sim.operator.gid"

    #@929
    const-string v13, ""

    #@92b
    invoke-static {v12, v13}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@92e
    move-result-object v7

    #@92f
    .line 6487
    .restart local v7       #operatorGid:Ljava/lang/String;
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@932
    move-result-object v12

    #@933
    invoke-virtual {v12}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    #@936
    move-result-object v8

    #@937
    .line 6488
    .restart local v8       #operatorIMSI:Ljava/lang/String;
    const-string v12, "persist.lg.data.firstopsim"

    #@939
    const-string v13, ""

    #@93b
    invoke-static {v12, v13}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@93e
    move-result-object v3

    #@93f
    .line 6489
    .restart local v3       #firstopsim:Ljava/lang/String;
    const-string v12, "Telephony"

    #@941
    new-instance v13, Ljava/lang/StringBuilder;

    #@943
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@946
    const-string v14, "[singleSW] firstopsim: "

    #@948
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94b
    move-result-object v13

    #@94c
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94f
    move-result-object v13

    #@950
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@953
    move-result-object v13

    #@954
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@957
    .line 6490
    if-eqz v3, :cond_995

    #@959
    const-string v12, "null"

    #@95b
    invoke-virtual {v3, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@95e
    move-result v12

    #@95f
    if-nez v12, :cond_995

    #@961
    .line 6491
    const-string v7, ""

    #@963
    .line 6492
    const-string v8, ""

    #@965
    .line 6493
    const-string v12, ";"

    #@967
    invoke-virtual {v3, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@96a
    move-result-object v9

    #@96b
    .line 6494
    .restart local v9       #opinfo:[Ljava/lang/String;
    const-string v12, "Telephony"

    #@96d
    new-instance v13, Ljava/lang/StringBuilder;

    #@96f
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@972
    const-string v14, "[singleSW] length :"

    #@974
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@977
    move-result-object v13

    #@978
    array-length v14, v9

    #@979
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@97c
    move-result-object v13

    #@97d
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@980
    move-result-object v13

    #@981
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@984
    .line 6495
    if-eqz v9, :cond_98c

    #@986
    array-length v12, v9

    #@987
    if-lez v12, :cond_98c

    #@989
    .line 6496
    const/4 v12, 0x0

    #@98a
    aget-object v8, v9, v12

    #@98c
    .line 6498
    :cond_98c
    if-eqz v9, :cond_995

    #@98e
    array-length v12, v9

    #@98f
    const/4 v13, 0x1

    #@990
    if-le v12, v13, :cond_995

    #@992
    .line 6499
    const/4 v12, 0x1

    #@993
    aget-object v7, v9, v12

    #@995
    .line 6502
    .end local v9           #opinfo:[Ljava/lang/String;
    :cond_995
    const-string v12, "Telephony"

    #@997
    new-instance v13, Ljava/lang/StringBuilder;

    #@999
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@99c
    const-string v14, "[singleSW] OperatorIMSI: "

    #@99e
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a1
    move-result-object v13

    #@9a2
    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a5
    move-result-object v13

    #@9a6
    const-string v14, "  operatorGid : "

    #@9a8
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9ab
    move-result-object v13

    #@9ac
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9af
    move-result-object v13

    #@9b0
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9b3
    move-result-object v13

    #@9b4
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@9b7
    .line 6504
    if-nez v8, :cond_9bc

    #@9b9
    .line 6505
    const/4 v12, 0x0

    #@9ba
    goto/16 :goto_16

    #@9bc
    .line 6507
    :cond_9bc
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@9bf
    move-result v12

    #@9c0
    const/4 v13, 0x6

    #@9c1
    if-ge v12, v13, :cond_9c6

    #@9c3
    .line 6508
    const/4 v12, 0x0

    #@9c4
    goto/16 :goto_16

    #@9c6
    .line 6511
    :cond_9c6
    const-string v12, "45000"

    #@9c8
    move-object/from16 v0, p1

    #@9ca
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9cd
    move-result v12

    #@9ce
    if-eqz v12, :cond_a17

    #@9d0
    const/4 v12, 0x0

    #@9d1
    const/4 v13, 0x5

    #@9d2
    invoke-virtual {v8, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@9d5
    move-result-object v12

    #@9d6
    const-string v13, "45000"

    #@9d8
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9db
    move-result v12

    #@9dc
    if-eqz v12, :cond_a17

    #@9de
    .line 6512
    const-string v12, "Telephony"

    #@9e0
    new-instance v13, Ljava/lang/StringBuilder;

    #@9e2
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@9e5
    const-string v14, "title: "

    #@9e7
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9ea
    move-result-object v13

    #@9eb
    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9ee
    move-result-object v13

    #@9ef
    const-string v14, ", numeric: "

    #@9f1
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f4
    move-result-object v13

    #@9f5
    move-object/from16 v0, p1

    #@9f7
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9fa
    move-result-object v13

    #@9fb
    const-string v14, ", extraid: "

    #@9fd
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a00
    move-result-object v13

    #@a01
    move-object/from16 v0, p2

    #@a03
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a06
    move-result-object v13

    #@a07
    const-string v14, " is added"

    #@a09
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0c
    move-result-object v13

    #@a0d
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a10
    move-result-object v13

    #@a11
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a14
    .line 6513
    const/4 v12, 0x1

    #@a15
    goto/16 :goto_16

    #@a17
    .line 6516
    :cond_a17
    const-string v12, "302490"

    #@a19
    move-object/from16 v0, p1

    #@a1b
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a1e
    move-result v12

    #@a1f
    if-nez v12, :cond_a2b

    #@a21
    const-string v12, "22201"

    #@a23
    move-object/from16 v0, p1

    #@a25
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a28
    move-result v12

    #@a29
    if-eqz v12, :cond_a74

    #@a2b
    :cond_a2b
    const/4 v12, 0x0

    #@a2c
    const/4 v13, 0x6

    #@a2d
    invoke-virtual {v8, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@a30
    move-result-object v12

    #@a31
    const-string v13, "302490"

    #@a33
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a36
    move-result v12

    #@a37
    if-nez v12, :cond_a47

    #@a39
    const/4 v12, 0x0

    #@a3a
    const/4 v13, 0x5

    #@a3b
    invoke-virtual {v8, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@a3e
    move-result-object v12

    #@a3f
    const-string v13, "22201"

    #@a41
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a44
    move-result v12

    #@a45
    if-eqz v12, :cond_a74

    #@a47
    .line 6517
    :cond_a47
    const-string v12, "Telephony"

    #@a49
    new-instance v13, Ljava/lang/StringBuilder;

    #@a4b
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@a4e
    const-string v14, "title: "

    #@a50
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a53
    move-result-object v13

    #@a54
    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a57
    move-result-object v13

    #@a58
    const-string v14, ", numeric: "

    #@a5a
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5d
    move-result-object v13

    #@a5e
    move-object/from16 v0, p1

    #@a60
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a63
    move-result-object v13

    #@a64
    const-string v14, " is added"

    #@a66
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a69
    move-result-object v13

    #@a6a
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a6d
    move-result-object v13

    #@a6e
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a71
    .line 6518
    const/4 v12, 0x1

    #@a72
    goto/16 :goto_16

    #@a74
    .line 6519
    :cond_a74
    const-string v12, "22288"

    #@a76
    move-object/from16 v0, p1

    #@a78
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a7b
    move-result v12

    #@a7c
    if-eqz v12, :cond_ab9

    #@a7e
    const/4 v12, 0x0

    #@a7f
    const/4 v13, 0x5

    #@a80
    invoke-virtual {v8, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@a83
    move-result-object v12

    #@a84
    const-string v13, "22288"

    #@a86
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a89
    move-result v12

    #@a8a
    if-eqz v12, :cond_ab9

    #@a8c
    .line 6520
    const-string v12, "Telephony"

    #@a8e
    new-instance v13, Ljava/lang/StringBuilder;

    #@a90
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@a93
    const-string v14, "title: "

    #@a95
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a98
    move-result-object v13

    #@a99
    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9c
    move-result-object v13

    #@a9d
    const-string v14, ", numeric: "

    #@a9f
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa2
    move-result-object v13

    #@aa3
    move-object/from16 v0, p1

    #@aa5
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa8
    move-result-object v13

    #@aa9
    const-string v14, " is added"

    #@aab
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aae
    move-result-object v13

    #@aaf
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ab2
    move-result-object v13

    #@ab3
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@ab6
    .line 6521
    const/4 v12, 0x1

    #@ab7
    goto/16 :goto_16

    #@ab9
    .line 6522
    :cond_ab9
    const-string v12, "302500"

    #@abb
    move-object/from16 v0, p1

    #@abd
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ac0
    move-result v12

    #@ac1
    if-nez v12, :cond_acd

    #@ac3
    const-string v12, "20404"

    #@ac5
    move-object/from16 v0, p1

    #@ac7
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@aca
    move-result v12

    #@acb
    if-eqz v12, :cond_b16

    #@acd
    :cond_acd
    const/4 v12, 0x0

    #@ace
    const/4 v13, 0x6

    #@acf
    invoke-virtual {v8, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@ad2
    move-result-object v12

    #@ad3
    const-string v13, "302500"

    #@ad5
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ad8
    move-result v12

    #@ad9
    if-nez v12, :cond_ae9

    #@adb
    const/4 v12, 0x0

    #@adc
    const/4 v13, 0x5

    #@add
    invoke-virtual {v8, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@ae0
    move-result-object v12

    #@ae1
    const-string v13, "20404"

    #@ae3
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ae6
    move-result v12

    #@ae7
    if-eqz v12, :cond_b16

    #@ae9
    .line 6523
    :cond_ae9
    const-string v12, "Telephony"

    #@aeb
    new-instance v13, Ljava/lang/StringBuilder;

    #@aed
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@af0
    const-string v14, "title: "

    #@af2
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af5
    move-result-object v13

    #@af6
    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af9
    move-result-object v13

    #@afa
    const-string v14, ", numeric: "

    #@afc
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aff
    move-result-object v13

    #@b00
    move-object/from16 v0, p1

    #@b02
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b05
    move-result-object v13

    #@b06
    const-string v14, " is added"

    #@b08
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0b
    move-result-object v13

    #@b0c
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b0f
    move-result-object v13

    #@b10
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@b13
    .line 6524
    const/4 v12, 0x1

    #@b14
    goto/16 :goto_16

    #@b16
    .line 6525
    :cond_b16
    const-string v12, "302510"

    #@b18
    move-object/from16 v0, p1

    #@b1a
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b1d
    move-result v12

    #@b1e
    if-nez v12, :cond_b2a

    #@b20
    const-string v12, "20404"

    #@b22
    move-object/from16 v0, p1

    #@b24
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b27
    move-result v12

    #@b28
    if-eqz v12, :cond_b73

    #@b2a
    :cond_b2a
    const/4 v12, 0x0

    #@b2b
    const/4 v13, 0x6

    #@b2c
    invoke-virtual {v8, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@b2f
    move-result-object v12

    #@b30
    const-string v13, "302510"

    #@b32
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b35
    move-result v12

    #@b36
    if-nez v12, :cond_b46

    #@b38
    const/4 v12, 0x0

    #@b39
    const/4 v13, 0x5

    #@b3a
    invoke-virtual {v8, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@b3d
    move-result-object v12

    #@b3e
    const-string v13, "20404"

    #@b40
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b43
    move-result v12

    #@b44
    if-eqz v12, :cond_b73

    #@b46
    .line 6526
    :cond_b46
    const-string v12, "Telephony"

    #@b48
    new-instance v13, Ljava/lang/StringBuilder;

    #@b4a
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@b4d
    const-string v14, "title: "

    #@b4f
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b52
    move-result-object v13

    #@b53
    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b56
    move-result-object v13

    #@b57
    const-string v14, ", numeric: "

    #@b59
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5c
    move-result-object v13

    #@b5d
    move-object/from16 v0, p1

    #@b5f
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b62
    move-result-object v13

    #@b63
    const-string v14, " is added"

    #@b65
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b68
    move-result-object v13

    #@b69
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b6c
    move-result-object v13

    #@b6d
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@b70
    .line 6527
    const/4 v12, 0x1

    #@b71
    goto/16 :goto_16

    #@b73
    .line 6529
    :cond_b73
    const/4 v12, 0x0

    #@b74
    goto/16 :goto_16

    #@b76
    .line 6532
    .end local v3           #firstopsim:Ljava/lang/String;
    .end local v7           #operatorGid:Ljava/lang/String;
    .end local v8           #operatorIMSI:Ljava/lang/String;
    :cond_b76
    const/4 v12, 0x0

    #@b77
    goto/16 :goto_16

    #@b79
    .line 6541
    :cond_b79
    const-string v12, "OPEN"

    #@b7b
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b7e
    move-result v12

    #@b7f
    if-nez v12, :cond_c49

    #@b81
    const-string v12, "ATT"

    #@b83
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b86
    move-result v12

    #@b87
    if-nez v12, :cond_c49

    #@b89
    const-string v12, "VZW"

    #@b8b
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b8e
    move-result v12

    #@b8f
    if-nez v12, :cond_c49

    #@b91
    const-string v12, "DCM"

    #@b93
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b96
    move-result v12

    #@b97
    if-nez v12, :cond_c49

    #@b99
    const-string v12, "SKT"

    #@b9b
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b9e
    move-result v12

    #@b9f
    if-nez v12, :cond_c49

    #@ba1
    const-string v12, "KT"

    #@ba3
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ba6
    move-result v12

    #@ba7
    if-nez v12, :cond_c49

    #@ba9
    const-string v12, "LGU"

    #@bab
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@bae
    move-result v12

    #@baf
    if-nez v12, :cond_c49

    #@bb1
    const-string v12, "SPR"

    #@bb3
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@bb6
    move-result v12

    #@bb7
    if-nez v12, :cond_c49

    #@bb9
    const-string v12, "SHB"

    #@bbb
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@bbe
    move-result v12

    #@bbf
    if-nez v12, :cond_c49

    #@bc1
    const-string v12, "TCL"

    #@bc3
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@bc6
    move-result v12

    #@bc7
    if-nez v12, :cond_c49

    #@bc9
    const-string v12, "USC"

    #@bcb
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@bce
    move-result v12

    #@bcf
    if-nez v12, :cond_c49

    #@bd1
    const-string v12, "VIV"

    #@bd3
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@bd6
    move-result v12

    #@bd7
    if-nez v12, :cond_c49

    #@bd9
    const-string v12, "CLR"

    #@bdb
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@bde
    move-result v12

    #@bdf
    if-nez v12, :cond_c49

    #@be1
    const-string v12, "PLS"

    #@be3
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@be6
    move-result v12

    #@be7
    if-nez v12, :cond_c49

    #@be9
    const-string v12, "ORG"

    #@beb
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@bee
    move-result v12

    #@bef
    if-nez v12, :cond_c49

    #@bf1
    const-string v12, "STL"

    #@bf3
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@bf6
    move-result v12

    #@bf7
    if-nez v12, :cond_c49

    #@bf9
    const-string v12, "MON"

    #@bfb
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@bfe
    move-result v12

    #@bff
    if-nez v12, :cond_c49

    #@c01
    const-string v12, "KDDI"

    #@c03
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c06
    move-result v12

    #@c07
    if-nez v12, :cond_c49

    #@c09
    const-string v12, "TEL"

    #@c0b
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c0e
    move-result v12

    #@c0f
    if-nez v12, :cond_c49

    #@c11
    const-string v12, "OPT"

    #@c13
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c16
    move-result v12

    #@c17
    if-nez v12, :cond_c49

    #@c19
    const-string v12, "GLO"

    #@c1b
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c1e
    move-result v12

    #@c1f
    if-nez v12, :cond_c49

    #@c21
    const-string v12, "CUCC"

    #@c23
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c26
    move-result v12

    #@c27
    if-nez v12, :cond_c49

    #@c29
    const-string v12, "TMN"

    #@c2b
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c2e
    move-result v12

    #@c2f
    if-nez v12, :cond_c49

    #@c31
    const-string v12, "TIM"

    #@c33
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c36
    move-result v12

    #@c37
    if-nez v12, :cond_c49

    #@c39
    const-string v12, "PLY"

    #@c3b
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c3e
    move-result v12

    #@c3f
    if-nez v12, :cond_c49

    #@c41
    const-string v12, "USC"

    #@c43
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c46
    move-result v12

    #@c47
    if-eqz v12, :cond_d87

    #@c49
    .line 6568
    :cond_c49
    const-string v12, "45000"

    #@c4b
    move-object/from16 v0, p1

    #@c4d
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c50
    move-result v12

    #@c51
    if-eqz v12, :cond_c80

    #@c53
    .line 6570
    const-string v12, "Telephony"

    #@c55
    new-instance v13, Ljava/lang/StringBuilder;

    #@c57
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@c5a
    const-string v14, "OPEN, title: "

    #@c5c
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5f
    move-result-object v13

    #@c60
    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c63
    move-result-object v13

    #@c64
    const-string v14, ", numeric: "

    #@c66
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c69
    move-result-object v13

    #@c6a
    move-object/from16 v0, p1

    #@c6c
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6f
    move-result-object v13

    #@c70
    const-string v14, " is added"

    #@c72
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c75
    move-result-object v13

    #@c76
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c79
    move-result-object v13

    #@c7a
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@c7d
    .line 6572
    const/4 v12, 0x1

    #@c7e
    goto/16 :goto_16

    #@c80
    .line 6576
    :cond_c80
    const-string v12, "BR"

    #@c82
    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c85
    move-result v12

    #@c86
    if-eqz v12, :cond_cc2

    #@c88
    .line 6578
    const-string v12, "724"

    #@c8a
    move-object/from16 v0, p1

    #@c8c
    invoke-virtual {v0, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@c8f
    move-result v12

    #@c90
    if-eqz v12, :cond_cbf

    #@c92
    .line 6579
    const-string v12, "Telephony"

    #@c94
    new-instance v13, Ljava/lang/StringBuilder;

    #@c96
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@c99
    const-string v14, "OPEN BR, title: "

    #@c9b
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9e
    move-result-object v13

    #@c9f
    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca2
    move-result-object v13

    #@ca3
    const-string v14, ", numeric: "

    #@ca5
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca8
    move-result-object v13

    #@ca9
    move-object/from16 v0, p1

    #@cab
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cae
    move-result-object v13

    #@caf
    const-string v14, " is added"

    #@cb1
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb4
    move-result-object v13

    #@cb5
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cb8
    move-result-object v13

    #@cb9
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@cbc
    .line 6581
    const/4 v12, 0x1

    #@cbd
    goto/16 :goto_16

    #@cbf
    .line 6583
    :cond_cbf
    const/4 v12, 0x0

    #@cc0
    goto/16 :goto_16

    #@cc2
    .line 6586
    :cond_cc2
    const-string v12, "DE"

    #@cc4
    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@cc7
    move-result v12

    #@cc8
    if-eqz v12, :cond_d04

    #@cca
    .line 6588
    const-string v12, "262"

    #@ccc
    move-object/from16 v0, p1

    #@cce
    invoke-virtual {v0, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@cd1
    move-result v12

    #@cd2
    if-eqz v12, :cond_d01

    #@cd4
    .line 6589
    const-string v12, "Telephony"

    #@cd6
    new-instance v13, Ljava/lang/StringBuilder;

    #@cd8
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@cdb
    const-string v14, "OPEN DE, title: "

    #@cdd
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce0
    move-result-object v13

    #@ce1
    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce4
    move-result-object v13

    #@ce5
    const-string v14, ", numeric: "

    #@ce7
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cea
    move-result-object v13

    #@ceb
    move-object/from16 v0, p1

    #@ced
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf0
    move-result-object v13

    #@cf1
    const-string v14, " is added"

    #@cf3
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf6
    move-result-object v13

    #@cf7
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cfa
    move-result-object v13

    #@cfb
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@cfe
    .line 6591
    const/4 v12, 0x1

    #@cff
    goto/16 :goto_16

    #@d01
    .line 6593
    :cond_d01
    const/4 v12, 0x0

    #@d02
    goto/16 :goto_16

    #@d04
    .line 6596
    :cond_d04
    const-string v12, "FR"

    #@d06
    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d09
    move-result v12

    #@d0a
    if-eqz v12, :cond_d5a

    #@d0c
    .line 6598
    const-string v12, "208"

    #@d0e
    move-object/from16 v0, p1

    #@d10
    invoke-virtual {v0, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@d13
    move-result v12

    #@d14
    if-nez v12, :cond_d2a

    #@d16
    const-string v12, "340"

    #@d18
    move-object/from16 v0, p1

    #@d1a
    invoke-virtual {v0, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@d1d
    move-result v12

    #@d1e
    if-nez v12, :cond_d2a

    #@d20
    const-string v12, "647"

    #@d22
    move-object/from16 v0, p1

    #@d24
    invoke-virtual {v0, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@d27
    move-result v12

    #@d28
    if-eqz v12, :cond_d57

    #@d2a
    .line 6602
    :cond_d2a
    const-string v12, "Telephony"

    #@d2c
    new-instance v13, Ljava/lang/StringBuilder;

    #@d2e
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@d31
    const-string v14, "OPEN FR, title: "

    #@d33
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d36
    move-result-object v13

    #@d37
    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3a
    move-result-object v13

    #@d3b
    const-string v14, ", numeric: "

    #@d3d
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d40
    move-result-object v13

    #@d41
    move-object/from16 v0, p1

    #@d43
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d46
    move-result-object v13

    #@d47
    const-string v14, " is added"

    #@d49
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4c
    move-result-object v13

    #@d4d
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d50
    move-result-object v13

    #@d51
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@d54
    .line 6604
    const/4 v12, 0x1

    #@d55
    goto/16 :goto_16

    #@d57
    .line 6606
    :cond_d57
    const/4 v12, 0x0

    #@d58
    goto/16 :goto_16

    #@d5a
    .line 6609
    :cond_d5a
    const-string v12, "Telephony"

    #@d5c
    new-instance v13, Ljava/lang/StringBuilder;

    #@d5e
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@d61
    const-string v14, "EU OPEN, title: "

    #@d63
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d66
    move-result-object v13

    #@d67
    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d6a
    move-result-object v13

    #@d6b
    const-string v14, ", numeric: "

    #@d6d
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d70
    move-result-object v13

    #@d71
    move-object/from16 v0, p1

    #@d73
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d76
    move-result-object v13

    #@d77
    const-string v14, " is added"

    #@d79
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7c
    move-result-object v13

    #@d7d
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d80
    move-result-object v13

    #@d81
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@d84
    .line 6613
    const/4 v12, 0x1

    #@d85
    goto/16 :goto_16

    #@d87
    .line 6616
    :cond_d87
    const-string v12, "COM"

    #@d89
    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d8c
    move-result v12

    #@d8d
    if-nez v12, :cond_daf

    #@d8f
    const-string v12, "EU"

    #@d91
    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d94
    move-result v12

    #@d95
    if-nez v12, :cond_daf

    #@d97
    const-string v12, "LGU"

    #@d99
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d9c
    move-result v12

    #@d9d
    if-nez v12, :cond_daf

    #@d9f
    const-string v12, "KT"

    #@da1
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@da4
    move-result v12

    #@da5
    if-nez v12, :cond_daf

    #@da7
    const-string v12, "SKT"

    #@da9
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@dac
    move-result v12

    #@dad
    if-eqz v12, :cond_10ef

    #@daf
    .line 6619
    :cond_daf
    const-string v12, "45000"

    #@db1
    move-object/from16 v0, p1

    #@db3
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@db6
    move-result v12

    #@db7
    if-eqz v12, :cond_de6

    #@db9
    .line 6621
    const-string v12, "Telephony"

    #@dbb
    new-instance v13, Ljava/lang/StringBuilder;

    #@dbd
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@dc0
    const-string v14, "Regional COM, title: "

    #@dc2
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc5
    move-result-object v13

    #@dc6
    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc9
    move-result-object v13

    #@dca
    const-string v14, ", numeric: "

    #@dcc
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dcf
    move-result-object v13

    #@dd0
    move-object/from16 v0, p1

    #@dd2
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd5
    move-result-object v13

    #@dd6
    const-string v14, " is added"

    #@dd8
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ddb
    move-result-object v13

    #@ddc
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ddf
    move-result-object v13

    #@de0
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@de3
    .line 6623
    const/4 v12, 0x1

    #@de4
    goto/16 :goto_16

    #@de6
    .line 6626
    :cond_de6
    const-string v12, "ORG"

    #@de8
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@deb
    move-result v12

    #@dec
    if-eqz v12, :cond_e2d

    #@dee
    .line 6628
    const-string v12, "28310;;23205;ORANGE A;65202;;20610;MOBISTAR;62402;;62303;;60201;;62701;;20801;ORANGE F;64700;ORANGE RE;34001;ORANGE;61101;;63203;;42501;;61203;;41677;;416770;;63907;;27099;ORANGE;64602;;61002;;61701;;25901;ORANGE;60400;;61404;;26003;ORANGE;26803;P OPTIMUS;22610;ORANGE RO;60801;;23101;ORANGE SK;21403;ORANGE;22803;ORANGE;22803;22803[1-9][1-8];60501;;64114;;23433;ORANGE;23430;T-MOBILE;23431;T-MOBILE;23432;T-MOBILE;"

    #@df0
    move-object/from16 v0, p1

    #@df2
    invoke-virtual {v12, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@df5
    move-result v12

    #@df6
    if-gez v12, :cond_dfb

    #@df8
    .line 6629
    const/4 v12, 0x0

    #@df9
    goto/16 :goto_16

    #@dfb
    .line 6630
    :cond_dfb
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    #@dfe
    move-result v12

    #@dff
    if-lez v12, :cond_f90

    #@e01
    const-string v12, "28310;;23205;ORANGE A;65202;;20610;MOBISTAR;62402;;62303;;60201;;62701;;20801;ORANGE F;64700;ORANGE RE;34001;ORANGE;61101;;63203;;42501;;61203;;41677;;416770;;63907;;27099;ORANGE;64602;;61002;;61701;;25901;ORANGE;60400;;61404;;26003;ORANGE;26803;P OPTIMUS;22610;ORANGE RO;60801;;23101;ORANGE SK;21403;ORANGE;22803;ORANGE;22803;22803[1-9][1-8];60501;;64114;;23433;ORANGE;23430;T-MOBILE;23431;T-MOBILE;23432;T-MOBILE;"

    #@e03
    new-instance v13, Ljava/lang/StringBuilder;

    #@e05
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@e08
    move-object/from16 v0, p1

    #@e0a
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e0d
    move-result-object v13

    #@e0e
    const-string v14, ";"

    #@e10
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e13
    move-result-object v13

    #@e14
    move-object/from16 v0, p2

    #@e16
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e19
    move-result-object v13

    #@e1a
    const-string v14, ";"

    #@e1c
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e1f
    move-result-object v13

    #@e20
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e23
    move-result-object v13

    #@e24
    invoke-virtual {v12, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@e27
    move-result v12

    #@e28
    if-gez v12, :cond_f90

    #@e2a
    .line 6633
    const/4 v12, 0x0

    #@e2b
    goto/16 :goto_16

    #@e2d
    .line 6636
    :cond_e2d
    const-string v12, "VDF"

    #@e2f
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e32
    move-result v12

    #@e33
    if-eqz v12, :cond_e74

    #@e35
    .line 6638
    const-string v12, "27602;VODAFONE AL;23201;A1;20601;PROXIMUS;28401;;21910;HR VIP;28001;CYTAMOBILE-VODAFONE;23003;VODAFONE CZ;29403;VIP MK;20810;F SFR;64710;SFR;20810;4C;26202;VODAFONE.DE;20205;VODAFONE GR;21670;VODAFONE HU;21670;21670XX2;27402;;27403;;27201;VODAFONE IE;22210;VODAFONE IT;27801;VODAFONE MT;20404;;26801;;22601;VODAFONE RO;22005;VIP;29340;SOMOBIL;21401;VODAFONE ES;22801;SWISSCOM;23415;VODAFONE UK;53001;;53024;;50503;;60202;;65501;;26001;PLUS;26002;T-MOBILE.PL;26003;ORANGE;26006;PLAY;45005;SKT;45008;KT;"

    #@e37
    move-object/from16 v0, p1

    #@e39
    invoke-virtual {v12, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@e3c
    move-result v12

    #@e3d
    if-gez v12, :cond_e42

    #@e3f
    .line 6639
    const/4 v12, 0x0

    #@e40
    goto/16 :goto_16

    #@e42
    .line 6640
    :cond_e42
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    #@e45
    move-result v12

    #@e46
    if-lez v12, :cond_f90

    #@e48
    const-string v12, "27602;VODAFONE AL;23201;A1;20601;PROXIMUS;28401;;21910;HR VIP;28001;CYTAMOBILE-VODAFONE;23003;VODAFONE CZ;29403;VIP MK;20810;F SFR;64710;SFR;20810;4C;26202;VODAFONE.DE;20205;VODAFONE GR;21670;VODAFONE HU;21670;21670XX2;27402;;27403;;27201;VODAFONE IE;22210;VODAFONE IT;27801;VODAFONE MT;20404;;26801;;22601;VODAFONE RO;22005;VIP;29340;SOMOBIL;21401;VODAFONE ES;22801;SWISSCOM;23415;VODAFONE UK;53001;;53024;;50503;;60202;;65501;;26001;PLUS;26002;T-MOBILE.PL;26003;ORANGE;26006;PLAY;45005;SKT;45008;KT;"

    #@e4a
    new-instance v13, Ljava/lang/StringBuilder;

    #@e4c
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@e4f
    move-object/from16 v0, p1

    #@e51
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e54
    move-result-object v13

    #@e55
    const-string v14, ";"

    #@e57
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e5a
    move-result-object v13

    #@e5b
    move-object/from16 v0, p2

    #@e5d
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e60
    move-result-object v13

    #@e61
    const-string v14, ";"

    #@e63
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e66
    move-result-object v13

    #@e67
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e6a
    move-result-object v13

    #@e6b
    invoke-virtual {v12, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@e6e
    move-result v12

    #@e6f
    if-gez v12, :cond_f90

    #@e71
    .line 6643
    const/4 v12, 0x0

    #@e72
    goto/16 :goto_16

    #@e74
    .line 6646
    :cond_e74
    const-string v12, "TMO"

    #@e76
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e79
    move-result v12

    #@e7a
    if-eqz v12, :cond_ebb

    #@e7c
    .line 6648
    const-string v12, "23203;T MOBILE A;23207;TELE.RING;28405;284 05;21901;T-MOBILE HR;23001;T-MOBILE CZ;29401;T-MOBILE MK;26201;;20201;COSMOTE;21630;T-MOBILE HU;22004;T-MOBILE CG;29702;T-MOBILE CG;20416;T-MOBILE  NL;20420;T-MOBILE  NL;26002;T-MOBILE.PL;22603;COSMOTE;22606;COSMOTE;23102;T-MOBILE SK;"

    #@e7e
    move-object/from16 v0, p1

    #@e80
    invoke-virtual {v12, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@e83
    move-result v12

    #@e84
    if-gez v12, :cond_e89

    #@e86
    .line 6649
    const/4 v12, 0x0

    #@e87
    goto/16 :goto_16

    #@e89
    .line 6650
    :cond_e89
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    #@e8c
    move-result v12

    #@e8d
    if-lez v12, :cond_f90

    #@e8f
    const-string v12, "23203;T MOBILE A;23207;TELE.RING;28405;284 05;21901;T-MOBILE HR;23001;T-MOBILE CZ;29401;T-MOBILE MK;26201;;20201;COSMOTE;21630;T-MOBILE HU;22004;T-MOBILE CG;29702;T-MOBILE CG;20416;T-MOBILE  NL;20420;T-MOBILE  NL;26002;T-MOBILE.PL;22603;COSMOTE;22606;COSMOTE;23102;T-MOBILE SK;"

    #@e91
    new-instance v13, Ljava/lang/StringBuilder;

    #@e93
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@e96
    move-object/from16 v0, p1

    #@e98
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e9b
    move-result-object v13

    #@e9c
    const-string v14, ";"

    #@e9e
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea1
    move-result-object v13

    #@ea2
    move-object/from16 v0, p2

    #@ea4
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea7
    move-result-object v13

    #@ea8
    const-string v14, ";"

    #@eaa
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ead
    move-result-object v13

    #@eae
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@eb1
    move-result-object v13

    #@eb2
    invoke-virtual {v12, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@eb5
    move-result v12

    #@eb6
    if-gez v12, :cond_f90

    #@eb8
    .line 6653
    const/4 v12, 0x0

    #@eb9
    goto/16 :goto_16

    #@ebb
    .line 6656
    :cond_ebb
    const-string v12, "H3G"

    #@ebd
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ec0
    move-result v12

    #@ec1
    if-eqz v12, :cond_f02

    #@ec3
    .line 6658
    const-string v12, "23210;3;23806;3;27205;;22299;;24002;3SE;24004;3SE;23420;3;23594;3;45403;3;"

    #@ec5
    move-object/from16 v0, p1

    #@ec7
    invoke-virtual {v12, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@eca
    move-result v12

    #@ecb
    if-gez v12, :cond_ed0

    #@ecd
    .line 6659
    const/4 v12, 0x0

    #@ece
    goto/16 :goto_16

    #@ed0
    .line 6660
    :cond_ed0
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    #@ed3
    move-result v12

    #@ed4
    if-lez v12, :cond_f90

    #@ed6
    const-string v12, "23210;3;23806;3;27205;;22299;;24002;3SE;24004;3SE;23420;3;23594;3;45403;3;"

    #@ed8
    new-instance v13, Ljava/lang/StringBuilder;

    #@eda
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@edd
    move-object/from16 v0, p1

    #@edf
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee2
    move-result-object v13

    #@ee3
    const-string v14, ";"

    #@ee5
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee8
    move-result-object v13

    #@ee9
    move-object/from16 v0, p2

    #@eeb
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eee
    move-result-object v13

    #@eef
    const-string v14, ";"

    #@ef1
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ef4
    move-result-object v13

    #@ef5
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ef8
    move-result-object v13

    #@ef9
    invoke-virtual {v12, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@efc
    move-result v12

    #@efd
    if-gez v12, :cond_f90

    #@eff
    .line 6663
    const/4 v12, 0x0

    #@f00
    goto/16 :goto_16

    #@f02
    .line 6666
    :cond_f02
    const-string v12, "O2"

    #@f04
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f07
    move-result v12

    #@f08
    if-eqz v12, :cond_f49

    #@f0a
    .line 6668
    const-string v12, "23002;O2 - CZ;26207;;26207;262075_2620749;27202;O2.IE;23106;O2 - SK;21407;Base;23410;O2 - UK;"

    #@f0c
    move-object/from16 v0, p1

    #@f0e
    invoke-virtual {v12, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@f11
    move-result v12

    #@f12
    if-gez v12, :cond_f17

    #@f14
    .line 6669
    const/4 v12, 0x0

    #@f15
    goto/16 :goto_16

    #@f17
    .line 6670
    :cond_f17
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    #@f1a
    move-result v12

    #@f1b
    if-lez v12, :cond_f90

    #@f1d
    const-string v12, "23002;O2 - CZ;26207;;26207;262075_2620749;27202;O2.IE;23106;O2 - SK;21407;Base;23410;O2 - UK;"

    #@f1f
    new-instance v13, Ljava/lang/StringBuilder;

    #@f21
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@f24
    move-object/from16 v0, p1

    #@f26
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f29
    move-result-object v13

    #@f2a
    const-string v14, ";"

    #@f2c
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f2f
    move-result-object v13

    #@f30
    move-object/from16 v0, p2

    #@f32
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f35
    move-result-object v13

    #@f36
    const-string v14, ";"

    #@f38
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3b
    move-result-object v13

    #@f3c
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f3f
    move-result-object v13

    #@f40
    invoke-virtual {v12, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@f43
    move-result v12

    #@f44
    if-gez v12, :cond_f90

    #@f46
    .line 6672
    const/4 v12, 0x0

    #@f47
    goto/16 :goto_16

    #@f49
    .line 6675
    :cond_f49
    const-string v12, "SFR"

    #@f4b
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f4e
    move-result v12

    #@f4f
    if-eqz v12, :cond_f90

    #@f51
    .line 6677
    const-string v12, "20810;F SFR;64710;SFR;20810;4C;"

    #@f53
    move-object/from16 v0, p1

    #@f55
    invoke-virtual {v12, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@f58
    move-result v12

    #@f59
    if-gez v12, :cond_f5e

    #@f5b
    .line 6678
    const/4 v12, 0x0

    #@f5c
    goto/16 :goto_16

    #@f5e
    .line 6679
    :cond_f5e
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    #@f61
    move-result v12

    #@f62
    if-lez v12, :cond_f90

    #@f64
    const-string v12, "20810;F SFR;64710;SFR;20810;4C;"

    #@f66
    new-instance v13, Ljava/lang/StringBuilder;

    #@f68
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@f6b
    move-object/from16 v0, p1

    #@f6d
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f70
    move-result-object v13

    #@f71
    const-string v14, ";"

    #@f73
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f76
    move-result-object v13

    #@f77
    move-object/from16 v0, p2

    #@f79
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f7c
    move-result-object v13

    #@f7d
    const-string v14, ";"

    #@f7f
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f82
    move-result-object v13

    #@f83
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f86
    move-result-object v13

    #@f87
    invoke-virtual {v12, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@f8a
    move-result v12

    #@f8b
    if-gez v12, :cond_f90

    #@f8d
    .line 6682
    const/4 v12, 0x0

    #@f8e
    goto/16 :goto_16

    #@f90
    .line 6690
    :cond_f90
    const-string v12, "1"

    #@f92
    const-string v13, "ro.build.regional"

    #@f94
    invoke-static {v13}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@f97
    move-result-object v13

    #@f98
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f9b
    move-result v12

    #@f9c
    if-eqz v12, :cond_107d

    #@f9e
    const-string v12, "VDF"

    #@fa0
    const-string v13, "ro.build.target_operator"

    #@fa2
    invoke-static {v13}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@fa5
    move-result-object v13

    #@fa6
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@fa9
    move-result v12

    #@faa
    if-eqz v12, :cond_107d

    #@fac
    const-string v12, "EU"

    #@fae
    const-string v13, "ro.build.target_country"

    #@fb0
    invoke-static {v13}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@fb3
    move-result-object v13

    #@fb4
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@fb7
    move-result v12

    #@fb8
    if-eqz v12, :cond_107d

    #@fba
    .line 6695
    const-string v12, "persist.radio.mcc-list"

    #@fbc
    const-string v13, "FFF"

    #@fbe
    invoke-static {v12, v13}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@fc1
    move-result-object v4

    #@fc2
    .line 6698
    .local v4, mccList:Ljava/lang/String;
    const-string v12, "FFF"

    #@fc4
    invoke-virtual {v4, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@fc7
    move-result v12

    #@fc8
    if-nez v12, :cond_1040

    #@fca
    const-string v12, ","

    #@fcc
    invoke-virtual {v4, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@fcf
    move-result v12

    #@fd0
    if-eqz v12, :cond_1040

    #@fd2
    const/4 v10, 0x1

    #@fd3
    .line 6700
    .local v10, regional:Z
    :goto_fd3
    const-string v12, "FFF"

    #@fd5
    invoke-virtual {v4, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@fd8
    move-result v12

    #@fd9
    if-nez v12, :cond_1042

    #@fdb
    if-nez v10, :cond_1042

    #@fdd
    const/4 v5, 0x1

    #@fde
    .line 6702
    .local v5, nonRegional:Z
    :goto_fde
    if-eqz v10, :cond_1044

    #@fe0
    .line 6704
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    #@fe3
    move-result v12

    #@fe4
    const/4 v13, 0x3

    #@fe5
    if-lt v12, v13, :cond_ff5

    #@fe7
    const/4 v12, 0x0

    #@fe8
    const/4 v13, 0x3

    #@fe9
    move-object/from16 v0, p1

    #@feb
    invoke-virtual {v0, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@fee
    move-result-object v12

    #@fef
    invoke-virtual {v4, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@ff2
    move-result v12

    #@ff3
    if-nez v12, :cond_107d

    #@ff5
    .line 6707
    :cond_ff5
    const-string v12, "Telephony"

    #@ff7
    new-instance v13, Ljava/lang/StringBuilder;

    #@ff9
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@ffc
    const-string v14, "This version is COM for specific operators, MccList("

    #@ffe
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1001
    move-result-object v13

    #@1002
    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1005
    move-result-object v13

    #@1006
    const-string v14, "), the OpCode/CounCode is "

    #@1008
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100b
    move-result-object v13

    #@100c
    sget-object v14, Landroid/provider/Telephony$Carriers;->OPERATOR_CODE:Ljava/lang/String;

    #@100e
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1011
    move-result-object v13

    #@1012
    const-string v14, "/"

    #@1014
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1017
    move-result-object v13

    #@1018
    sget-object v14, Landroid/provider/Telephony$Carriers;->COUNTRY_CODE:Ljava/lang/String;

    #@101a
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@101d
    move-result-object v13

    #@101e
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1021
    move-result-object v13

    #@1022
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1025
    .line 6713
    const-string v12, "Telephony"

    #@1027
    new-instance v13, Ljava/lang/StringBuilder;

    #@1029
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@102c
    const-string v14, "Hence this profile is not loaded : "

    #@102e
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1031
    move-result-object v13

    #@1032
    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1035
    move-result-object v13

    #@1036
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1039
    move-result-object v13

    #@103a
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@103d
    .line 6715
    const/4 v12, 0x0

    #@103e
    goto/16 :goto_16

    #@1040
    .line 6698
    .end local v5           #nonRegional:Z
    .end local v10           #regional:Z
    :cond_1040
    const/4 v10, 0x0

    #@1041
    goto :goto_fd3

    #@1042
    .line 6700
    .restart local v10       #regional:Z
    :cond_1042
    const/4 v5, 0x0

    #@1043
    goto :goto_fde

    #@1044
    .line 6718
    .restart local v5       #nonRegional:Z
    :cond_1044
    if-eqz v5, :cond_10be

    #@1046
    .line 6720
    const-string v12, "Telephony"

    #@1048
    const-string v13, "ERROR! THIS COMMNET SHOULD NOT BE PRINTED : COM Non-Reginoal"

    #@104a
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@104d
    .line 6722
    const-string v12, "Telephony"

    #@104f
    new-instance v13, Ljava/lang/StringBuilder;

    #@1051
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@1054
    const-string v14, "This version is COM Non-Reginoal, MccList("

    #@1056
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1059
    move-result-object v13

    #@105a
    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105d
    move-result-object v13

    #@105e
    const-string v14, "), the OpCode/CounCode is "

    #@1060
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1063
    move-result-object v13

    #@1064
    sget-object v14, Landroid/provider/Telephony$Carriers;->OPERATOR_CODE:Ljava/lang/String;

    #@1066
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1069
    move-result-object v13

    #@106a
    const-string v14, "/"

    #@106c
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106f
    move-result-object v13

    #@1070
    sget-object v14, Landroid/provider/Telephony$Carriers;->COUNTRY_CODE:Ljava/lang/String;

    #@1072
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1075
    move-result-object v13

    #@1076
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1079
    move-result-object v13

    #@107a
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@107d
    .line 6739
    .end local v4           #mccList:Ljava/lang/String;
    .end local v5           #nonRegional:Z
    .end local v10           #regional:Z
    :cond_107d
    :goto_107d
    const-string v12, "Telephony"

    #@107f
    new-instance v13, Ljava/lang/StringBuilder;

    #@1081
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@1084
    const-string v14, "title: "

    #@1086
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1089
    move-result-object v13

    #@108a
    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108d
    move-result-object v13

    #@108e
    const-string v14, ", "

    #@1090
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1093
    move-result-object v13

    #@1094
    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1097
    move-result-object v13

    #@1098
    const-string v14, " "

    #@109a
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@109d
    move-result-object v13

    #@109e
    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10a1
    move-result-object v13

    #@10a2
    const-string v14, ", numeric: "

    #@10a4
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10a7
    move-result-object v13

    #@10a8
    move-object/from16 v0, p1

    #@10aa
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10ad
    move-result-object v13

    #@10ae
    const-string v14, " is added"

    #@10b0
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10b3
    move-result-object v13

    #@10b4
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10b7
    move-result-object v13

    #@10b8
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@10bb
    .line 6742
    const/4 v12, 0x1

    #@10bc
    goto/16 :goto_16

    #@10be
    .line 6728
    .restart local v4       #mccList:Ljava/lang/String;
    .restart local v5       #nonRegional:Z
    .restart local v10       #regional:Z
    :cond_10be
    const-string v12, "Telephony"

    #@10c0
    new-instance v13, Ljava/lang/StringBuilder;

    #@10c2
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@10c5
    const-string v14, "This version is COM for all COM operators, MccList("

    #@10c7
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10ca
    move-result-object v13

    #@10cb
    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10ce
    move-result-object v13

    #@10cf
    const-string v14, "), the OpCode/CounCode is "

    #@10d1
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10d4
    move-result-object v13

    #@10d5
    sget-object v14, Landroid/provider/Telephony$Carriers;->OPERATOR_CODE:Ljava/lang/String;

    #@10d7
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10da
    move-result-object v13

    #@10db
    const-string v14, "/"

    #@10dd
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e0
    move-result-object v13

    #@10e1
    sget-object v14, Landroid/provider/Telephony$Carriers;->COUNTRY_CODE:Ljava/lang/String;

    #@10e3
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e6
    move-result-object v13

    #@10e7
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10ea
    move-result-object v13

    #@10eb
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@10ee
    goto :goto_107d

    #@10ef
    .line 6748
    .end local v4           #mccList:Ljava/lang/String;
    .end local v5           #nonRegional:Z
    .end local v10           #regional:Z
    :cond_10ef
    invoke-static {}, Landroid/provider/Telephony$Carriers;->isAutoProfileNeeded()Z

    #@10f2
    move-result v12

    #@10f3
    if-eqz v12, :cond_1133

    #@10f5
    .line 6750
    const-string v12, "45000"

    #@10f7
    move-object/from16 v0, p1

    #@10f9
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10fc
    move-result v12

    #@10fd
    if-eqz v12, :cond_112c

    #@10ff
    .line 6752
    const-string v12, "Telephony"

    #@1101
    new-instance v13, Ljava/lang/StringBuilder;

    #@1103
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@1106
    const-string v14, "isAutoProfileNeeded() is true, title: "

    #@1108
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@110b
    move-result-object v13

    #@110c
    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@110f
    move-result-object v13

    #@1110
    const-string v14, ", numeric: "

    #@1112
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1115
    move-result-object v13

    #@1116
    move-object/from16 v0, p1

    #@1118
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@111b
    move-result-object v13

    #@111c
    const-string v14, " is added"

    #@111e
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1121
    move-result-object v13

    #@1122
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1125
    move-result-object v13

    #@1126
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1129
    .line 6754
    const/4 v12, 0x1

    #@112a
    goto/16 :goto_16

    #@112c
    .line 6757
    :cond_112c
    const-string v12, "Telephony"

    #@112e
    const-string v13, "ERROR! THIS COMMNET SHOULD NOT BE PRINTED."

    #@1130
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1133
    .line 6760
    :cond_1133
    invoke-static {}, Landroid/provider/Telephony$Carriers;->buildOperatorVersionValues()Z

    #@1136
    .line 6762
    sget-object v12, Landroid/provider/Telephony$Carriers;->mNumericExtraIDSet:Ljava/util/HashSet;

    #@1138
    new-instance v13, Ljava/lang/StringBuilder;

    #@113a
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@113d
    move-object/from16 v0, p1

    #@113f
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1142
    move-result-object v13

    #@1143
    const-string v14, ";"

    #@1145
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1148
    move-result-object v13

    #@1149
    move-object/from16 v0, p2

    #@114b
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@114e
    move-result-object v13

    #@114f
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1152
    move-result-object v13

    #@1153
    invoke-virtual {v12, v13}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@1156
    move-result v12

    #@1157
    if-nez v12, :cond_115c

    #@1159
    .line 6763
    const/4 v12, 0x0

    #@115a
    goto/16 :goto_16

    #@115c
    .line 6766
    :cond_115c
    const-string v12, "Telephony"

    #@115e
    new-instance v13, Ljava/lang/StringBuilder;

    #@1160
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@1163
    const-string v14, "title: "

    #@1165
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1168
    move-result-object v13

    #@1169
    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@116c
    move-result-object v13

    #@116d
    const-string v14, ", "

    #@116f
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1172
    move-result-object v13

    #@1173
    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1176
    move-result-object v13

    #@1177
    const-string v14, " "

    #@1179
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@117c
    move-result-object v13

    #@117d
    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1180
    move-result-object v13

    #@1181
    const-string v14, ", numeric: "

    #@1183
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1186
    move-result-object v13

    #@1187
    move-object/from16 v0, p1

    #@1189
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@118c
    move-result-object v13

    #@118d
    const-string v14, ", extraid: "

    #@118f
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1192
    move-result-object v13

    #@1193
    move-object/from16 v0, p2

    #@1195
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1198
    move-result-object v13

    #@1199
    const-string v14, " is added"

    #@119b
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@119e
    move-result-object v13

    #@119f
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11a2
    move-result-object v13

    #@11a3
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@11a6
    .line 6770
    const/4 v12, 0x1

    #@11a7
    goto/16 :goto_16
.end method

.method public static isMultipleNumericOperator()Z
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 5203
    invoke-static {}, Landroid/provider/Telephony$Carriers;->isAutoProfileNeeded()Z

    #@4
    move-result v1

    #@5
    if-eqz v1, :cond_8

    #@7
    .line 5209
    :cond_7
    :goto_7
    return v0

    #@8
    .line 5207
    :cond_8
    invoke-static {}, Landroid/provider/Telephony$Carriers;->buildOperatorVersionValues()Z

    #@b
    .line 5209
    sget-object v1, Landroid/provider/Telephony$Carriers;->mNumericExtraIDSet:Ljava/util/HashSet;

    #@d
    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    #@10
    move-result v1

    #@11
    if-gt v1, v0, :cond_7

    #@13
    const/4 v0, 0x0

    #@14
    goto :goto_7
.end method

.method private static isSPNBasedOperator(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 6
    .parameter "numeric"
    .parameter "spn"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 5420
    if-eqz p0, :cond_9

    #@3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@6
    move-result v1

    #@7
    if-nez v1, :cond_a

    #@9
    .line 5430
    :cond_9
    :goto_9
    return v0

    #@a
    .line 5423
    :cond_a
    if-eqz p1, :cond_9

    #@c
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@f
    move-result v1

    #@10
    if-eqz v1, :cond_9

    #@12
    .line 5426
    const-string v1, "21407;JAZZTEL;21421;JAZZTEL;23801;TELMORE;23802;BIBOB;23802;CBB MOBIL;23820;CALL ME;23820;DLG TELE;20810;A MOBILE;20810;LECLERCMOBILE;26201;DEBITEL;26201;MOBILCOM-DEBITEL;26202;DEBITEL;26202;MOBILCOM-DEBITEL;26202;1&1;26203;DEBITEL;26203;MOBILCOM-DEBITEL;26207;DEBITEL;26207;MOBILCOM-DEBITEL;21601;DJUICE;22299;FASTWEB;22210;POSTEMOBILE;20408;RABO MOBIEL;20416;BEN NL;26801;ZON;24004;TELENOR SE;24005;HALEBOP;24005;TELE2;24005;TELIA;24024;TELE2;24024;TELENOR SE;25503;ACE&BASE;25503;DJUICE;26006;RED BULL MOBILE;24803;TELE2;22201;IUSACELL;20404;PELEPHONE;20404;MIRS;"

    #@14
    new-instance v2, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, ";"

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    const-string v3, ";"

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@38
    move-result v1

    #@39
    if-ltz v1, :cond_9

    #@3b
    .line 5430
    const/4 v0, 0x1

    #@3c
    goto :goto_9
.end method

.method public static isSupportAutoProfileAsMultiSimMode()Z
    .registers 1

    #@0
    .prologue
    .line 6781
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public static makeApnContentValues(Landroid/database/Cursor;)Landroid/content/ContentValues;
    .registers 5
    .parameter "cursor"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 6886
    if-nez p0, :cond_5

    #@3
    const/4 v0, 0x0

    #@4
    .line 6917
    :goto_4
    return-object v0

    #@5
    .line 6888
    :cond_5
    new-instance v0, Landroid/content/ContentValues;

    #@7
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@a
    .line 6889
    .local v0, values:Landroid/content/ContentValues;
    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    #@d
    .line 6890
    const-string v1, "name"

    #@f
    const-string v2, "name"

    #@11
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@14
    move-result v2

    #@15
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1c
    .line 6891
    const-string v1, "apn"

    #@1e
    const-string v2, "apn"

    #@20
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@23
    move-result v2

    #@24
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2b
    .line 6892
    const-string v1, "proxy"

    #@2d
    const-string v2, "proxy"

    #@2f
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@32
    move-result v2

    #@33
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@3a
    .line 6893
    const-string v1, "port"

    #@3c
    const-string v2, "port"

    #@3e
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@41
    move-result v2

    #@42
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@45
    move-result-object v2

    #@46
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@49
    .line 6894
    const-string v1, "mmsproxy"

    #@4b
    const-string v2, "mmsproxy"

    #@4d
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@50
    move-result v2

    #@51
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@54
    move-result-object v2

    #@55
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@58
    .line 6895
    const-string v1, "mmsport"

    #@5a
    const-string v2, "mmsport"

    #@5c
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@5f
    move-result v2

    #@60
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@63
    move-result-object v2

    #@64
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@67
    .line 6896
    const-string v1, "server"

    #@69
    const-string v2, "server"

    #@6b
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@6e
    move-result v2

    #@6f
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@72
    move-result-object v2

    #@73
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@76
    .line 6897
    const-string v1, "user"

    #@78
    const-string v2, "user"

    #@7a
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@7d
    move-result v2

    #@7e
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@81
    move-result-object v2

    #@82
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@85
    .line 6898
    const-string v1, "password"

    #@87
    const-string v2, "password"

    #@89
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@8c
    move-result v2

    #@8d
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@90
    move-result-object v2

    #@91
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@94
    .line 6899
    const-string v1, "mmsc"

    #@96
    const-string v2, "mmsc"

    #@98
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@9b
    move-result v2

    #@9c
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@9f
    move-result-object v2

    #@a0
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@a3
    .line 6900
    const-string v1, "mcc"

    #@a5
    const-string v2, "mcc"

    #@a7
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@aa
    move-result v2

    #@ab
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@ae
    move-result-object v2

    #@af
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@b2
    .line 6901
    const-string v1, "mnc"

    #@b4
    const-string v2, "mnc"

    #@b6
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@b9
    move-result v2

    #@ba
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@bd
    move-result-object v2

    #@be
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@c1
    .line 6902
    const-string v1, "numeric"

    #@c3
    const-string v2, "numeric"

    #@c5
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@c8
    move-result v2

    #@c9
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@cc
    move-result-object v2

    #@cd
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@d0
    .line 6903
    const-string v1, "authtype"

    #@d2
    const-string v2, "authtype"

    #@d4
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@d7
    move-result v2

    #@d8
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    #@db
    move-result v2

    #@dc
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@df
    move-result-object v2

    #@e0
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@e3
    .line 6904
    const-string v1, "type"

    #@e5
    const-string v2, "type"

    #@e7
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@ea
    move-result v2

    #@eb
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@ee
    move-result-object v2

    #@ef
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@f2
    .line 6905
    const-string v1, "protocol"

    #@f4
    const-string v2, "protocol"

    #@f6
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@f9
    move-result v2

    #@fa
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@fd
    move-result-object v2

    #@fe
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@101
    .line 6906
    const-string v1, "roaming_protocol"

    #@103
    const-string v2, "roaming_protocol"

    #@105
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@108
    move-result v2

    #@109
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@10c
    move-result-object v2

    #@10d
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@110
    .line 6907
    const-string v1, "carrier_enabled"

    #@112
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@115
    move-result v1

    #@116
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    #@119
    move-result v1

    #@11a
    if-ne v1, v3, :cond_17e

    #@11c
    .line 6908
    const-string v1, "carrier_enabled"

    #@11e
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@121
    move-result-object v2

    #@122
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@125
    .line 6911
    :goto_125
    const-string v1, "bearer"

    #@127
    const-string v2, "bearer"

    #@129
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@12c
    move-result v2

    #@12d
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@130
    move-result-object v2

    #@131
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@134
    .line 6912
    const-string v1, "extraid"

    #@136
    const-string v2, "extraid"

    #@138
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@13b
    move-result v2

    #@13c
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@13f
    move-result-object v2

    #@140
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@143
    .line 6913
    const-string v1, "defaultsetting"

    #@145
    const-string v2, "defaultsetting"

    #@147
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@14a
    move-result v2

    #@14b
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    #@14e
    move-result v2

    #@14f
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@152
    move-result-object v2

    #@153
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@156
    .line 6914
    const-string v1, "usercreatesetting"

    #@158
    const-string v2, "usercreatesetting"

    #@15a
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@15d
    move-result v2

    #@15e
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    #@161
    move-result v2

    #@162
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@165
    move-result-object v2

    #@166
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@169
    .line 6915
    const-string v1, "sim_slot"

    #@16b
    const-string v2, "sim_slot"

    #@16d
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@170
    move-result v2

    #@171
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    #@174
    move-result v2

    #@175
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@178
    move-result-object v2

    #@179
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@17c
    goto/16 :goto_4

    #@17e
    .line 6910
    :cond_17e
    const-string v1, "carrier_enabled"

    #@180
    const/4 v2, 0x0

    #@181
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@184
    move-result-object v2

    #@185
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@188
    goto :goto_125
.end method

.method private static rebuildProperties(Ljava/lang/String;Ljava/lang/String;)V
    .registers 14
    .parameter "baseOpList"
    .parameter "mccList"

    #@0
    .prologue
    .line 6145
    const-string v9, "FFF"

    #@2
    invoke-virtual {p1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v9

    #@6
    if-nez v9, :cond_eb

    #@8
    const-string v9, ","

    #@a
    invoke-virtual {p1, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@d
    move-result v9

    #@e
    if-eqz v9, :cond_eb

    #@10
    const/4 v8, 0x1

    #@11
    .line 6146
    .local v8, regional:Z
    :goto_11
    const-string v9, "FFF"

    #@13
    invoke-virtual {p1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v9

    #@17
    if-nez v9, :cond_ee

    #@19
    if-nez v8, :cond_ee

    #@1b
    const/4 v4, 0x1

    #@1c
    .line 6148
    .local v4, nonRegional:Z
    :goto_1c
    if-eqz v4, :cond_11e

    #@1e
    .line 6150
    const-string v9, "Telephony"

    #@20
    new-instance v10, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v11, "findBuildProperties(): This is Non-Regional version, unique MCC is "

    #@27
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v10

    #@2b
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v10

    #@2f
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v10

    #@33
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 6154
    const-string v9, ";"

    #@38
    invoke-virtual {p0, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@3b
    move-result-object v7

    #@3c
    .line 6156
    .local v7, operators:[Ljava/lang/String;
    const/4 v5, 0x0

    #@3d
    .line 6158
    .local v5, numric:Ljava/lang/String;
    move-object v1, v7

    #@3e
    .local v1, arr$:[Ljava/lang/String;
    array-length v3, v1

    #@3f
    .local v3, len$:I
    const/4 v2, 0x0

    #@40
    .local v2, i$:I
    :goto_40
    if-ge v2, v3, :cond_7a

    #@42
    aget-object v6, v1, v2

    #@44
    .line 6160
    .local v6, op:Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@47
    move-result v9

    #@48
    const/4 v10, 0x5

    #@49
    if-ne v9, v10, :cond_f1

    #@4b
    const/4 v9, 0x0

    #@4c
    const/4 v10, 0x3

    #@4d
    invoke-virtual {v6, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@50
    move-result-object v9

    #@51
    invoke-virtual {v9, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@54
    move-result v9

    #@55
    if-eqz v9, :cond_f1

    #@57
    .line 6162
    move-object v5, v6

    #@58
    .line 6164
    const-string v9, "Telephony"

    #@5a
    new-instance v10, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v11, "findBuildProperties(): Found numeric for MCC("

    #@61
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v10

    #@65
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v10

    #@69
    const-string v11, ") is "

    #@6b
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v10

    #@6f
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v10

    #@73
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v10

    #@77
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    .line 6171
    .end local v6           #op:Ljava/lang/String;
    :cond_7a
    if-eqz v5, :cond_ff

    #@7c
    .line 6173
    sget-object v1, Landroid/provider/Telephony$Carriers;->GLOBAL_OPERATORS:[[Ljava/lang/String;

    #@7e
    .local v1, arr$:[[Ljava/lang/String;
    array-length v3, v1

    #@7f
    const/4 v2, 0x0

    #@80
    :goto_80
    if-ge v2, v3, :cond_ea

    #@82
    aget-object v0, v1, v2

    #@84
    .line 6175
    .local v0, Op:[Ljava/lang/String;
    array-length v9, v0

    #@85
    const/4 v10, 0x4

    #@86
    if-ne v9, v10, :cond_f5

    #@88
    const/4 v9, 0x0

    #@89
    aget-object v9, v0, v9

    #@8b
    if-eqz v9, :cond_f5

    #@8d
    const/4 v9, 0x1

    #@8e
    aget-object v9, v0, v9

    #@90
    if-eqz v9, :cond_f5

    #@92
    const/4 v9, 0x2

    #@93
    aget-object v9, v0, v9

    #@95
    if-eqz v9, :cond_f5

    #@97
    const/4 v9, 0x3

    #@98
    aget-object v9, v0, v9

    #@9a
    if-eqz v9, :cond_f5

    #@9c
    .line 6178
    const/4 v9, 0x2

    #@9d
    aget-object v9, v0, v9

    #@9f
    invoke-virtual {v9, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a2
    move-result v9

    #@a3
    if-eqz v9, :cond_fc

    #@a5
    const/4 v9, 0x3

    #@a6
    aget-object v9, v0, v9

    #@a8
    const-string v10, ""

    #@aa
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ad
    move-result v9

    #@ae
    if-eqz v9, :cond_fc

    #@b0
    .line 6180
    const/4 v9, 0x0

    #@b1
    aget-object v9, v0, v9

    #@b3
    sput-object v9, Landroid/provider/Telephony$Carriers;->OPERATOR_CODE:Ljava/lang/String;

    #@b5
    .line 6181
    const/4 v9, 0x1

    #@b6
    aget-object v9, v0, v9

    #@b8
    sput-object v9, Landroid/provider/Telephony$Carriers;->COUNTRY_CODE:Ljava/lang/String;

    #@ba
    .line 6183
    const-string v9, "Telephony"

    #@bc
    new-instance v10, Ljava/lang/StringBuilder;

    #@be
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@c1
    const-string v11, "findBuildProperties(): Found build operator for "

    #@c3
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v10

    #@c7
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v10

    #@cb
    const-string v11, " is "

    #@cd
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v10

    #@d1
    sget-object v11, Landroid/provider/Telephony$Carriers;->OPERATOR_CODE:Ljava/lang/String;

    #@d3
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v10

    #@d7
    const-string v11, " "

    #@d9
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v10

    #@dd
    sget-object v11, Landroid/provider/Telephony$Carriers;->COUNTRY_CODE:Ljava/lang/String;

    #@df
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v10

    #@e3
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e6
    move-result-object v10

    #@e7
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ea
    .line 6210
    .end local v0           #Op:[Ljava/lang/String;
    .end local v1           #arr$:[[Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    .end local v5           #numric:Ljava/lang/String;
    .end local v7           #operators:[Ljava/lang/String;
    :cond_ea
    :goto_ea
    return-void

    #@eb
    .line 6145
    .end local v4           #nonRegional:Z
    .end local v8           #regional:Z
    :cond_eb
    const/4 v8, 0x0

    #@ec
    goto/16 :goto_11

    #@ee
    .line 6146
    .restart local v8       #regional:Z
    :cond_ee
    const/4 v4, 0x0

    #@ef
    goto/16 :goto_1c

    #@f1
    .line 6158
    .local v1, arr$:[Ljava/lang/String;
    .restart local v2       #i$:I
    .restart local v3       #len$:I
    .restart local v4       #nonRegional:Z
    .restart local v5       #numric:Ljava/lang/String;
    .restart local v6       #op:Ljava/lang/String;
    .restart local v7       #operators:[Ljava/lang/String;
    :cond_f1
    add-int/lit8 v2, v2, 0x1

    #@f3
    goto/16 :goto_40

    #@f5
    .line 6193
    .end local v6           #op:Ljava/lang/String;
    .restart local v0       #Op:[Ljava/lang/String;
    .local v1, arr$:[[Ljava/lang/String;
    :cond_f5
    const-string v9, "Telephony"

    #@f7
    const-string v10, "findBuildProperties(): There are invaild Operator in GLOBAL_OPERATORS"

    #@f9
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@fc
    .line 6173
    :cond_fc
    add-int/lit8 v2, v2, 0x1

    #@fe
    goto :goto_80

    #@ff
    .line 6200
    .end local v0           #Op:[Ljava/lang/String;
    .local v1, arr$:[Ljava/lang/String;
    :cond_ff
    const-string v9, "Telephony"

    #@101
    new-instance v10, Ljava/lang/StringBuilder;

    #@103
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@106
    const-string v11, "findBuildProperties() : Fail to find numeric for MCC("

    #@108
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10b
    move-result-object v10

    #@10c
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10f
    move-result-object v10

    #@110
    const-string v11, ")"

    #@112
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@115
    move-result-object v10

    #@116
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@119
    move-result-object v10

    #@11a
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11d
    goto :goto_ea

    #@11e
    .line 6207
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    .end local v5           #numric:Ljava/lang/String;
    .end local v7           #operators:[Ljava/lang/String;
    :cond_11e
    const-string v9, "Telephony"

    #@120
    const-string v10, "findBuildProperties(): This is OPEN or Regional version"

    #@122
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@125
    goto :goto_ea
.end method
