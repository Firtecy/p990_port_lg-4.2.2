.class public final Landroid/provider/Telephony$Sms$Draft;
.super Ljava/lang/Object;
.source "Telephony.java"

# interfaces
.implements Landroid/provider/BaseColumns;
.implements Landroid/provider/Telephony$TextBasedSmsColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/Telephony$Sms;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Draft"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final DEFAULT_SORT_ORDER:Ljava/lang/String; = "date DESC"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 1178
    const-string v0, "content://sms/draft"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/provider/Telephony$Sms$Draft;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 1174
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static addMessage(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)Landroid/net/Uri;
    .registers 17
    .parameter "resolver"
    .parameter "address"
    .parameter "body"
    .parameter "subject"
    .parameter "date"

    #@0
    .prologue
    .line 1199
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    const-string v2, "KROperator"

    #@4
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@7
    move-result v1

    #@8
    if-ne v0, v1, :cond_1c

    #@a
    .line 1200
    const-wide/16 v5, -0x1

    #@c
    const/4 v7, 0x0

    #@d
    const/4 v8, -0x1

    #@e
    const-wide/16 v9, -0x1

    #@10
    const/4 v11, 0x0

    #@11
    move-object v0, p0

    #@12
    move-object v1, p1

    #@13
    move-object v2, p2

    #@14
    move-object v3, p3

    #@15
    move-object/from16 v4, p4

    #@17
    invoke-static/range {v0 .. v11}, Landroid/provider/Telephony$Sms$Draft;->addMessageEx(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;JIIJLjava/lang/String;)Landroid/net/Uri;

    #@1a
    move-result-object v0

    #@1b
    .line 1210
    :goto_1b
    return-object v0

    #@1c
    :cond_1c
    sget-object v1, Landroid/provider/Telephony$Sms$Draft;->CONTENT_URI:Landroid/net/Uri;

    #@1e
    const/4 v6, 0x1

    #@1f
    const/4 v7, 0x0

    #@20
    invoke-static {}, Landroid/telephony/MSimSmsManager;->getDefault()Landroid/telephony/MSimSmsManager;

    #@23
    move-result-object v0

    #@24
    invoke-virtual {v0}, Landroid/telephony/MSimSmsManager;->getPreferredSmsSubscription()I

    #@27
    move-result v8

    #@28
    move-object v0, p0

    #@29
    move-object v2, p1

    #@2a
    move-object v3, p2

    #@2b
    move-object v4, p3

    #@2c
    move-object/from16 v5, p4

    #@2e
    invoke-static/range {v0 .. v8}, Landroid/provider/Telephony$Sms;->addMessageToUri(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZI)Landroid/net/Uri;

    #@31
    move-result-object v0

    #@32
    goto :goto_1b
.end method

.method public static addMessage(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;I)Landroid/net/Uri;
    .registers 15
    .parameter "resolver"
    .parameter "address"
    .parameter "body"
    .parameter "subject"
    .parameter "date"
    .parameter "subId"

    #@0
    .prologue
    .line 1229
    sget-object v1, Landroid/provider/Telephony$Sms$Draft;->CONTENT_URI:Landroid/net/Uri;

    #@2
    const/4 v6, 0x1

    #@3
    const/4 v7, 0x0

    #@4
    move-object v0, p0

    #@5
    move-object v2, p1

    #@6
    move-object v3, p2

    #@7
    move-object v4, p3

    #@8
    move-object v5, p4

    #@9
    move v8, p5

    #@a
    invoke-static/range {v0 .. v8}, Landroid/provider/Telephony$Sms;->addMessageToUri(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZI)Landroid/net/Uri;

    #@d
    move-result-object v0

    #@e
    return-object v0
.end method

.method public static addMessageEx(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;JIIJLjava/lang/String;)Landroid/net/Uri;
    .registers 25
    .parameter "resolver"
    .parameter "recipient"
    .parameter "body"
    .parameter "subject"
    .parameter "date"
    .parameter "threadId"
    .parameter "protocol"
    .parameter "dcs"
    .parameter "groupid"
    .parameter "replyAddress"

    #@0
    .prologue
    .line 1237
    const/4 v1, -0x1

    #@1
    move-object/from16 v0, p11

    #@3
    invoke-static {v1, v0}, Landroid/telephony/SmsMessage;->makeSmsHeader(ILjava/lang/String;)[B

    #@6
    move-result-object v12

    #@7
    move-object v1, p0

    #@8
    move-object v2, p1

    #@9
    move-object v3, p2

    #@a
    move-object/from16 v4, p3

    #@c
    move-object/from16 v5, p4

    #@e
    move-wide/from16 v6, p5

    #@10
    move/from16 v8, p7

    #@12
    move/from16 v9, p8

    #@14
    move-wide/from16 v10, p9

    #@16
    invoke-static/range {v1 .. v12}, Landroid/provider/Telephony$Sms$Draft;->addMessageExEx(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;JIIJ[B)Landroid/net/Uri;

    #@19
    move-result-object v1

    #@1a
    return-object v1
.end method

.method public static addMessageExEx(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;JIIJ[B)Landroid/net/Uri;
    .registers 33
    .parameter "resolver"
    .parameter "recipient"
    .parameter "body"
    .parameter "subject"
    .parameter "date"
    .parameter "threadId"
    .parameter "protocol"
    .parameter "dcs"
    .parameter "groupid"
    .parameter "header"

    #@0
    .prologue
    .line 1245
    sget-object v1, Landroid/provider/Telephony$Sms$Draft;->CONTENT_URI:Landroid/net/Uri;

    #@2
    const/4 v6, 0x1

    #@3
    const/4 v7, 0x0

    #@4
    const/4 v14, 0x0

    #@5
    const/4 v15, 0x0

    #@6
    const/16 v16, 0x2

    #@8
    const/16 v18, 0x0

    #@a
    const/16 v19, 0x0

    #@c
    const/16 v20, 0x0

    #@e
    move-object/from16 v0, p0

    #@10
    move-object/from16 v2, p1

    #@12
    move-object/from16 v3, p2

    #@14
    move-object/from16 v4, p3

    #@16
    move-object/from16 v5, p4

    #@18
    move-wide/from16 v8, p5

    #@1a
    move/from16 v10, p7

    #@1c
    move/from16 v11, p8

    #@1e
    move-wide/from16 v12, p9

    #@20
    move-object/from16 v17, p11

    #@22
    invoke-static/range {v0 .. v20}, Landroid/provider/Telephony$Sms;->addMessageToUriEx(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZJIIJIII[BZLjava/lang/String;I)Landroid/net/Uri;

    #@25
    move-result-object v0

    #@26
    return-object v0
.end method

.method public static saveMessage(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;)Z
    .registers 9
    .parameter "resolver"
    .parameter "uri"
    .parameter "body"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 1261
    new-instance v0, Landroid/content/ContentValues;

    #@4
    const/4 v2, 0x2

    #@5
    invoke-direct {v0, v2}, Landroid/content/ContentValues;-><init>(I)V

    #@8
    .line 1262
    .local v0, values:Landroid/content/ContentValues;
    const-string v2, "body"

    #@a
    invoke-virtual {v0, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 1263
    const-string v2, "date"

    #@f
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@12
    move-result-wide v3

    #@13
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@1a
    .line 1264
    invoke-virtual {p0, p1, v0, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@1d
    move-result v2

    #@1e
    if-ne v2, v1, :cond_21

    #@20
    :goto_20
    return v1

    #@21
    :cond_21
    const/4 v1, 0x0

    #@22
    goto :goto_20
.end method
