.class public final Landroid/provider/Telephony$SpamStr;
.super Ljava/lang/Object;
.source "Telephony.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/Telephony;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SpamStr"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final SPAMSTR_BYMSG_IDS:Landroid/net/Uri; = null

.field public static final SpamString:Ljava/lang/String; = "spam_string"

.field public static final SpamWildcardString:Ljava/lang/String; = "spam_wildcardstring"


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 7100
    sget-object v0, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    #@2
    const-string v1, "spamstring"

    #@4
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@7
    move-result-object v0

    #@8
    sput-object v0, Landroid/provider/Telephony$SpamStr;->CONTENT_URI:Landroid/net/Uri;

    #@a
    .line 7102
    sget-object v0, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    #@c
    const-string v1, "sapmstrids"

    #@e
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@11
    move-result-object v0

    #@12
    sput-object v0, Landroid/provider/Telephony$SpamStr;->SPAMSTR_BYMSG_IDS:Landroid/net/Uri;

    #@14
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 7099
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static addSpamstrToUri(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .registers 7
    .parameter "resolver"
    .parameter "uri"
    .parameter "spam_string"

    #@0
    .prologue
    .line 7109
    new-instance v0, Landroid/content/ContentValues;

    #@2
    const/4 v1, 0x2

    #@3
    invoke-direct {v0, v1}, Landroid/content/ContentValues;-><init>(I)V

    #@6
    .line 7110
    .local v0, values:Landroid/content/ContentValues;
    const-string v1, "spam_string"

    #@8
    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 7111
    const-string v1, "spam_wildcardstring"

    #@d
    const/16 v2, 0x3f

    #@f
    const/16 v3, 0x5f

    #@11
    invoke-virtual {p2, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 7112
    invoke-virtual {p0, p1, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@1b
    move-result-object v1

    #@1c
    return-object v1
.end method

.method public static updateSpamstrToUri(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;)I
    .registers 8
    .parameter "resolver"
    .parameter "uri"
    .parameter "spam_string"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 7117
    new-instance v0, Landroid/content/ContentValues;

    #@3
    const/4 v1, 0x2

    #@4
    invoke-direct {v0, v1}, Landroid/content/ContentValues;-><init>(I)V

    #@7
    .line 7118
    .local v0, values:Landroid/content/ContentValues;
    const-string v1, "spam_string"

    #@9
    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@c
    .line 7119
    const-string v1, "spam_wildcardstring"

    #@e
    const/16 v2, 0x3f

    #@10
    const/16 v3, 0x5f

    #@12
    invoke-virtual {p2, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@19
    .line 7120
    invoke-virtual {p0, p1, v0, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@1c
    move-result v1

    #@1d
    return v1
.end method
