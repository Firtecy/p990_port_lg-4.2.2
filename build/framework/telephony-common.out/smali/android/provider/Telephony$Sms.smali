.class public final Landroid/provider/Telephony$Sms;
.super Ljava/lang/Object;
.source "Telephony.java"

# interfaces
.implements Landroid/provider/BaseColumns;
.implements Landroid/provider/Telephony$TextBasedSmsColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/Telephony;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Sms"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/provider/Telephony$Sms$Kpas;,
        Landroid/provider/Telephony$Sms$UrlCallback;,
        Landroid/provider/Telephony$Sms$PortAddress;,
        Landroid/provider/Telephony$Sms$Temp;,
        Landroid/provider/Telephony$Sms$Video;,
        Landroid/provider/Telephony$Sms$Voice;,
        Landroid/provider/Telephony$Sms$VoiceSave;,
        Landroid/provider/Telephony$Sms$CallBackUrlSave;,
        Landroid/provider/Telephony$Sms$Save;,
        Landroid/provider/Telephony$Sms$CallBackUrlSpam;,
        Landroid/provider/Telephony$Sms$Spam;,
        Landroid/provider/Telephony$Sms$MmsReplySendMessage;,
        Landroid/provider/Telephony$Sms$Reserved;,
        Landroid/provider/Telephony$Sms$Intents;,
        Landroid/provider/Telephony$Sms$Conversations;,
        Landroid/provider/Telephony$Sms$Outbox;,
        Landroid/provider/Telephony$Sms$Draft;,
        Landroid/provider/Telephony$Sms$Sent;,
        Landroid/provider/Telephony$Sms$Inbox;
    }
.end annotation


# static fields
.field public static final APPDIRECT_URI:Landroid/net/Uri; = null

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final DEFAULT_SORT_ORDER:Ljava/lang/String; = "date DESC"

.field public static final IS_41_EMAIL_NETWORK_ADDRESS:Ljava/lang/String; = "6245"


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 605
    const-string v0, "content://sms"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    #@8
    .line 609
    sget-object v0, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    #@a
    const-string v1, "directed_app"

    #@c
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@f
    move-result-object v0

    #@10
    sput-object v0, Landroid/provider/Telephony$Sms;->APPDIRECT_URI:Landroid/net/Uri;

    #@12
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 591
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 2572
    return-void
.end method

.method public static addMessageToUri(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZ)Landroid/net/Uri;
    .registers 19
    .parameter "resolver"
    .parameter "uri"
    .parameter "address"
    .parameter "body"
    .parameter "subject"
    .parameter "date"
    .parameter "read"
    .parameter "deliveryReport"

    #@0
    .prologue
    .line 637
    const-wide/16 v8, -0x1

    #@2
    invoke-static {}, Landroid/telephony/MSimSmsManager;->getDefault()Landroid/telephony/MSimSmsManager;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/telephony/MSimSmsManager;->getPreferredSmsSubscription()I

    #@9
    move-result v10

    #@a
    move-object v0, p0

    #@b
    move-object v1, p1

    #@c
    move-object v2, p2

    #@d
    move-object v3, p3

    #@e
    move-object v4, p4

    #@f
    move-object/from16 v5, p5

    #@11
    move/from16 v6, p6

    #@13
    move/from16 v7, p7

    #@15
    invoke-static/range {v0 .. v10}, Landroid/provider/Telephony$Sms;->addMessageToUri(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZJI)Landroid/net/Uri;

    #@18
    move-result-object v0

    #@19
    return-object v0
.end method

.method public static addMessageToUri(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZI)Landroid/net/Uri;
    .registers 20
    .parameter "resolver"
    .parameter "uri"
    .parameter "address"
    .parameter "body"
    .parameter "subject"
    .parameter "date"
    .parameter "read"
    .parameter "deliveryReport"
    .parameter "subId"

    #@0
    .prologue
    .line 659
    const-wide/16 v8, -0x1

    #@2
    move-object v0, p0

    #@3
    move-object v1, p1

    #@4
    move-object v2, p2

    #@5
    move-object v3, p3

    #@6
    move-object v4, p4

    #@7
    move-object/from16 v5, p5

    #@9
    move/from16 v6, p6

    #@b
    move/from16 v7, p7

    #@d
    move/from16 v10, p8

    #@f
    invoke-static/range {v0 .. v10}, Landroid/provider/Telephony$Sms;->addMessageToUri(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZJI)Landroid/net/Uri;

    #@12
    move-result-object v0

    #@13
    return-object v0
.end method

.method public static addMessageToUri(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZJ)Landroid/net/Uri;
    .registers 21
    .parameter "resolver"
    .parameter "uri"
    .parameter "address"
    .parameter "body"
    .parameter "subject"
    .parameter "date"
    .parameter "read"
    .parameter "deliveryReport"
    .parameter "threadId"

    #@0
    .prologue
    .line 680
    invoke-static {}, Landroid/telephony/MSimSmsManager;->getDefault()Landroid/telephony/MSimSmsManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/telephony/MSimSmsManager;->getPreferredSmsSubscription()I

    #@7
    move-result v10

    #@8
    move-object v0, p0

    #@9
    move-object v1, p1

    #@a
    move-object v2, p2

    #@b
    move-object v3, p3

    #@c
    move-object v4, p4

    #@d
    move-object/from16 v5, p5

    #@f
    move/from16 v6, p6

    #@11
    move/from16 v7, p7

    #@13
    move-wide/from16 v8, p8

    #@15
    invoke-static/range {v0 .. v10}, Landroid/provider/Telephony$Sms;->addMessageToUri(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZJI)Landroid/net/Uri;

    #@18
    move-result-object v0

    #@19
    return-object v0
.end method

.method public static addMessageToUri(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZJI)Landroid/net/Uri;
    .registers 15
    .parameter "resolver"
    .parameter "uri"
    .parameter "address"
    .parameter "body"
    .parameter "subject"
    .parameter "date"
    .parameter "read"
    .parameter "deliveryReport"
    .parameter "threadId"
    .parameter "subId"

    #@0
    .prologue
    .line 703
    new-instance v0, Landroid/content/ContentValues;

    #@2
    const/16 v1, 0x8

    #@4
    invoke-direct {v0, v1}, Landroid/content/ContentValues;-><init>(I)V

    #@7
    .line 704
    .local v0, values:Landroid/content/ContentValues;
    const-string v1, "tag"

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "Telephony addMessageToUri sub id: "

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2, p10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 706
    const-string v1, "sub_id"

    #@21
    invoke-static {p10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@28
    .line 707
    const-string v1, "address"

    #@2a
    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2d
    .line 708
    if-eqz p5, :cond_34

    #@2f
    .line 709
    const-string v1, "date"

    #@31
    invoke-virtual {v0, v1, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@34
    .line 711
    :cond_34
    const-string v2, "read"

    #@36
    if-eqz p6, :cond_6b

    #@38
    const/4 v1, 0x1

    #@39
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3c
    move-result-object v1

    #@3d
    :goto_3d
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@40
    .line 712
    const-string v1, "subject"

    #@42
    invoke-virtual {v0, v1, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@45
    .line 713
    const-string v1, "body"

    #@47
    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@4a
    .line 714
    if-eqz p7, :cond_57

    #@4c
    .line 715
    const-string v1, "status"

    #@4e
    const/16 v2, 0x20

    #@50
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@53
    move-result-object v2

    #@54
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@57
    .line 717
    :cond_57
    const-wide/16 v1, -0x1

    #@59
    cmp-long v1, p8, v1

    #@5b
    if-eqz v1, :cond_66

    #@5d
    .line 718
    const-string v1, "thread_id"

    #@5f
    invoke-static {p8, p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@62
    move-result-object v2

    #@63
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@66
    .line 720
    :cond_66
    invoke-virtual {p0, p1, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@69
    move-result-object v1

    #@6a
    return-object v1

    #@6b
    .line 711
    :cond_6b
    const/4 v1, 0x0

    #@6c
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6f
    move-result-object v1

    #@70
    goto :goto_3d
.end method

.method public static addMessageToUri(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZJJ)Landroid/net/Uri;
    .registers 25
    .parameter "resolver"
    .parameter "uri"
    .parameter "address"
    .parameter "body"
    .parameter "subject"
    .parameter "date"
    .parameter "read"
    .parameter "deliveryReport"
    .parameter "threadId"
    .parameter "groupId"

    #@0
    .prologue
    .line 788
    invoke-static {}, Landroid/telephony/MSimSmsManager;->getDefault()Landroid/telephony/MSimSmsManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/telephony/MSimSmsManager;->getPreferredSmsSubscription()I

    #@7
    move-result v12

    #@8
    move-object v0, p0

    #@9
    move-object v1, p1

    #@a
    move-object v2, p2

    #@b
    move-object/from16 v3, p3

    #@d
    move-object/from16 v4, p4

    #@f
    move-object/from16 v5, p5

    #@11
    move/from16 v6, p6

    #@13
    move/from16 v7, p7

    #@15
    move-wide/from16 v8, p8

    #@17
    move-wide/from16 v10, p10

    #@19
    invoke-static/range {v0 .. v12}, Landroid/provider/Telephony$Sms;->addMessageToUri(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZJJI)Landroid/net/Uri;

    #@1c
    move-result-object v0

    #@1d
    return-object v0
.end method

.method public static addMessageToUri(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZJJI)Landroid/net/Uri;
    .registers 22
    .parameter "resolver"
    .parameter "uri"
    .parameter "address"
    .parameter "body"
    .parameter "subject"
    .parameter "date"
    .parameter "read"
    .parameter "deliveryReport"
    .parameter "threadId"
    .parameter "groupId"
    .parameter "subId"

    #@0
    .prologue
    .line 730
    const-string v1, "sms_imsi_data"

    #@2
    .line 731
    .local v1, SMS_IMSI_DATA:Ljava/lang/String;
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@5
    move-result-object v6

    #@6
    invoke-virtual {v6}, Landroid/telephony/MSimTelephonyManager;->isMultiSimEnabled()Z

    #@9
    move-result v6

    #@a
    if-eqz v6, :cond_d3

    #@c
    .line 732
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@f
    move-result-object v6

    #@10
    move/from16 v0, p12

    #@12
    invoke-virtual {v6, v0}, Landroid/telephony/MSimTelephonyManager;->getSimSerialNumber(I)Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    .line 733
    .local v2, iccd:Ljava/lang/String;
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@19
    move-result-object v6

    #@1a
    move/from16 v0, p12

    #@1c
    invoke-virtual {v6, v0}, Landroid/telephony/MSimTelephonyManager;->getSubscriberId(I)Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    .line 741
    .local v3, imsi:Ljava/lang/String;
    :goto_20
    const/4 v4, 0x0

    #@21
    .line 744
    .local v4, mIccImsi:Ljava/lang/String;
    new-instance v5, Landroid/content/ContentValues;

    #@23
    const/16 v6, 0xb

    #@25
    invoke-direct {v5, v6}, Landroid/content/ContentValues;-><init>(I)V

    #@28
    .line 745
    .local v5, values:Landroid/content/ContentValues;
    const-string v6, "tag"

    #@2a
    new-instance v7, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v8, "Telephony addMessageToUri sub id: "

    #@31
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v7

    #@35
    move/from16 v0, p12

    #@37
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v7

    #@3b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v7

    #@3f
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 746
    const-string v6, "sub_id"

    #@44
    invoke-static/range {p12 .. p12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@47
    move-result-object v7

    #@48
    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@4b
    .line 747
    if-nez v2, :cond_e9

    #@4d
    if-nez v3, :cond_e9

    #@4f
    .line 748
    const/4 v6, 0x1

    #@50
    move/from16 v0, p12

    #@52
    if-ne v0, v6, :cond_e5

    #@54
    .line 749
    const-string v4, "sim2"

    #@56
    .line 759
    :goto_56
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@59
    move-result-object v6

    #@5a
    invoke-virtual {v6}, Landroid/telephony/MSimTelephonyManager;->isMultiSimEnabled()Z

    #@5d
    move-result v6

    #@5e
    if-eqz v6, :cond_81

    #@60
    .line 761
    const-string v6, "tag"

    #@62
    new-instance v7, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string v8, "addMessageToUri mIccImsi2:["

    #@69
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v7

    #@6d
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v7

    #@71
    const-string v8, "]"

    #@73
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v7

    #@77
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v7

    #@7b
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7e
    .line 762
    invoke-virtual {v5, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@81
    .line 764
    :cond_81
    const-string v6, "address"

    #@83
    invoke-virtual {v5, v6, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@86
    .line 765
    if-eqz p5, :cond_8d

    #@88
    .line 766
    const-string v6, "date"

    #@8a
    invoke-virtual {v5, v6, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@8d
    .line 768
    :cond_8d
    const-string v7, "read"

    #@8f
    if-eqz p6, :cond_fc

    #@91
    const/4 v6, 0x1

    #@92
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@95
    move-result-object v6

    #@96
    :goto_96
    invoke-virtual {v5, v7, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@99
    .line 769
    const-string v6, "subject"

    #@9b
    invoke-virtual {v5, v6, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@9e
    .line 770
    const-string v6, "body"

    #@a0
    invoke-virtual {v5, v6, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@a3
    .line 771
    if-eqz p7, :cond_b0

    #@a5
    .line 772
    const-string v6, "status"

    #@a7
    const/16 v7, 0x20

    #@a9
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ac
    move-result-object v7

    #@ad
    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@b0
    .line 774
    :cond_b0
    const-wide/16 v6, -0x1

    #@b2
    cmp-long v6, p8, v6

    #@b4
    if-eqz v6, :cond_bf

    #@b6
    .line 775
    const-string v6, "thread_id"

    #@b8
    invoke-static/range {p8 .. p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@bb
    move-result-object v7

    #@bc
    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@bf
    .line 778
    :cond_bf
    const-wide/16 v6, -0x1

    #@c1
    cmp-long v6, p10, v6

    #@c3
    if-eqz v6, :cond_ce

    #@c5
    .line 779
    const-string v6, "group_id"

    #@c7
    invoke-static/range {p10 .. p11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@ca
    move-result-object v7

    #@cb
    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@ce
    .line 781
    :cond_ce
    invoke-virtual {p0, p1, v5}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@d1
    move-result-object v6

    #@d2
    return-object v6

    #@d3
    .line 735
    .end local v2           #iccd:Ljava/lang/String;
    .end local v3           #imsi:Ljava/lang/String;
    .end local v4           #mIccImsi:Ljava/lang/String;
    .end local v5           #values:Landroid/content/ContentValues;
    :cond_d3
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@d6
    move-result-object v6

    #@d7
    invoke-virtual {v6}, Landroid/telephony/TelephonyManager;->getSimSerialNumber()Ljava/lang/String;

    #@da
    move-result-object v2

    #@db
    .line 736
    .restart local v2       #iccd:Ljava/lang/String;
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@de
    move-result-object v6

    #@df
    invoke-virtual {v6}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    #@e2
    move-result-object v3

    #@e3
    .restart local v3       #imsi:Ljava/lang/String;
    goto/16 :goto_20

    #@e5
    .line 751
    .restart local v4       #mIccImsi:Ljava/lang/String;
    .restart local v5       #values:Landroid/content/ContentValues;
    :cond_e5
    const-string v4, "sim1"

    #@e7
    goto/16 :goto_56

    #@e9
    .line 754
    :cond_e9
    new-instance v6, Ljava/lang/StringBuilder;

    #@eb
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@ee
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f1
    move-result-object v6

    #@f2
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v6

    #@f6
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f9
    move-result-object v4

    #@fa
    goto/16 :goto_56

    #@fc
    .line 768
    :cond_fc
    const/4 v6, 0x0

    #@fd
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@100
    move-result-object v6

    #@101
    goto :goto_96
.end method

.method public static addMessageToUriEx(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZJIIJIII[BZLjava/lang/String;I)Landroid/net/Uri;
    .registers 32
    .parameter "resolver"
    .parameter "uri"
    .parameter "address"
    .parameter "body"
    .parameter "subject"
    .parameter "date"
    .parameter "read"
    .parameter "deliveryReport"
    .parameter "threadId"
    .parameter "protocol"
    .parameter "dcs"
    .parameter "groupid"
    .parameter "simcopy"
    .parameter "msg_boxtype"
    .parameter "save_call_type"
    .parameter "header"
    .parameter "inMessage"
    .parameter "extraData"
    .parameter "replyOption"

    #@0
    .prologue
    .line 798
    const/4 v3, 0x0

    #@1
    .line 799
    .local v3, replyAddress:Ljava/lang/String;
    const/4 v1, -0x1

    #@2
    .line 801
    .local v1, confirm_read:I
    if-eqz p17, :cond_16

    #@4
    .line 802
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@7
    move-result-object v8

    #@8
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    #@b
    move-result v8

    #@c
    const/4 v9, 0x2

    #@d
    if-ne v8, v9, :cond_f8

    #@f
    .line 803
    new-instance v3, Ljava/lang/String;

    #@11
    .end local v3           #replyAddress:Ljava/lang/String;
    move-object/from16 v0, p17

    #@13
    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([B)V

    #@16
    .line 827
    .restart local v3       #replyAddress:Ljava/lang/String;
    :cond_16
    :goto_16
    new-instance v7, Landroid/content/ContentValues;

    #@18
    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    #@1b
    .line 829
    .local v7, values:Landroid/content/ContentValues;
    const/4 v8, 0x1

    #@1c
    move/from16 v0, p18

    #@1e
    if-ne v0, v8, :cond_138

    #@20
    .line 834
    if-nez v3, :cond_135

    #@22
    if-nez p2, :cond_132

    #@24
    const-string v6, ""

    #@26
    .line 836
    .local v6, uiAddress:Ljava/lang/String;
    :goto_26
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@29
    move-result v8

    #@2a
    if-eqz v8, :cond_33

    #@2c
    .line 837
    new-instance v6, Ljava/lang/String;

    #@2e
    .end local v6           #uiAddress:Ljava/lang/String;
    const-string v8, "Unknown"

    #@30
    invoke-direct {v6, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@33
    .line 839
    .restart local v6       #uiAddress:Ljava/lang/String;
    :cond_33
    const-string v8, "address"

    #@35
    invoke-virtual {v7, v8, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@38
    .line 850
    :goto_38
    const-string v8, "original_address"

    #@3a
    if-nez p2, :cond_3e

    #@3c
    const-string p2, ""

    #@3e
    .end local p2
    :cond_3e
    invoke-virtual {v7, v8, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@41
    .line 851
    if-eqz v3, :cond_48

    #@43
    .line 852
    const-string v8, "reply_address"

    #@45
    invoke-virtual {v7, v8, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@48
    .line 855
    :cond_48
    const/4 v8, -0x1

    #@49
    if-le v1, v8, :cond_54

    #@4b
    .line 856
    const-string v8, "confirm_read"

    #@4d
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@50
    move-result-object v9

    #@51
    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@54
    .line 868
    :cond_54
    const-string v8, "simcopy"

    #@56
    invoke-static/range {p14 .. p14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@59
    move-result-object v9

    #@5a
    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@5d
    .line 873
    const-string v8, "save_call_type"

    #@5f
    invoke-static/range {p16 .. p16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@62
    move-result-object v9

    #@63
    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@66
    .line 875
    const-string v8, "msg_boxtype"

    #@68
    invoke-static/range {p15 .. p15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6b
    move-result-object v9

    #@6c
    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@6f
    .line 877
    if-eqz p5, :cond_78

    #@71
    .line 878
    const-string v8, "date"

    #@73
    move-object/from16 v0, p5

    #@75
    invoke-virtual {v7, v8, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@78
    .line 880
    :cond_78
    const-string v9, "read"

    #@7a
    if-eqz p6, :cond_14d

    #@7c
    const/4 v8, 0x1

    #@7d
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@80
    move-result-object v8

    #@81
    :goto_81
    invoke-virtual {v7, v9, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@84
    .line 881
    const-string v8, "subject"

    #@86
    invoke-virtual {v7, v8, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@89
    .line 882
    const-string v8, "body"

    #@8b
    invoke-virtual {v7, v8, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@8e
    .line 883
    if-eqz p7, :cond_9b

    #@90
    .line 884
    const-string v8, "status"

    #@92
    const/16 v9, 0x20

    #@94
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@97
    move-result-object v9

    #@98
    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@9b
    .line 886
    :cond_9b
    const-wide/16 v8, -0x1

    #@9d
    cmp-long v8, p8, v8

    #@9f
    if-eqz v8, :cond_aa

    #@a1
    .line 887
    const-string v8, "thread_id"

    #@a3
    invoke-static/range {p8 .. p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@a6
    move-result-object v9

    #@a7
    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@aa
    .line 889
    :cond_aa
    const-string v8, "protocol"

    #@ac
    invoke-static/range {p10 .. p10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@af
    move-result-object v9

    #@b0
    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@b3
    .line 890
    const/4 v8, -0x1

    #@b4
    move/from16 v0, p11

    #@b6
    if-eq v0, v8, :cond_c1

    #@b8
    .line 891
    const-string v8, "dcs"

    #@ba
    invoke-static/range {p11 .. p11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@bd
    move-result-object v9

    #@be
    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@c1
    .line 893
    :cond_c1
    const-wide/16 v8, -0x1

    #@c3
    cmp-long v8, p12, v8

    #@c5
    if-eqz v8, :cond_d0

    #@c7
    .line 894
    const-string v8, "group_id"

    #@c9
    invoke-static/range {p12 .. p13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@cc
    move-result-object v9

    #@cd
    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@d0
    .line 896
    :cond_d0
    invoke-static/range {p19 .. p19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@d3
    move-result v8

    #@d4
    if-nez v8, :cond_dd

    #@d6
    .line 897
    const-string v8, "extra_data"

    #@d8
    move-object/from16 v0, p19

    #@da
    invoke-virtual {v7, v8, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@dd
    .line 901
    :cond_dd
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@e0
    move-result-object v8

    #@e1
    const-string v9, "content://sms/mmsreplysend"

    #@e3
    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@e6
    move-result v8

    #@e7
    if-nez v8, :cond_154

    #@e9
    .line 902
    const-string v8, "typeex"

    #@eb
    const/4 v9, 0x2

    #@ec
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ef
    move-result-object v9

    #@f0
    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@f3
    .line 911
    :cond_f3
    :goto_f3
    invoke-virtual {p0, p1, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@f6
    move-result-object v8

    #@f7
    return-object v8

    #@f8
    .line 805
    .end local v6           #uiAddress:Ljava/lang/String;
    .end local v7           #values:Landroid/content/ContentValues;
    .restart local p2
    :cond_f8
    invoke-static/range {p17 .. p17}, Lcom/android/internal/telephony/SmsHeader;->fromByteArray([B)Lcom/android/internal/telephony/SmsHeader;

    #@fb
    move-result-object v5

    #@fc
    .line 806
    .local v5, smsHeader:Lcom/android/internal/telephony/SmsHeader;
    if-eqz v5, :cond_11c

    #@fe
    iget-object v8, v5, Lcom/android/internal/telephony/SmsHeader;->replyAddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;

    #@100
    if-eqz v8, :cond_11c

    #@102
    .line 809
    :try_start_102
    new-instance v4, Lcom/android/internal/telephony/gsm/GsmSmsAddress;

    #@104
    iget-object v8, v5, Lcom/android/internal/telephony/SmsHeader;->replyAddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;

    #@106
    iget-object v8, v8, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;->replyData:[B

    #@108
    const/4 v9, 0x0

    #@109
    iget-object v10, v5, Lcom/android/internal/telephony/SmsHeader;->replyAddress:Lcom/android/internal/telephony/SmsHeader$ReplyAddress;

    #@10b
    iget v10, v10, Lcom/android/internal/telephony/SmsHeader$ReplyAddress;->addressLength:I

    #@10d
    add-int/lit8 v10, v10, 0x1

    #@10f
    div-int/lit8 v10, v10, 0x2

    #@111
    add-int/lit8 v10, v10, 0x2

    #@113
    invoke-direct {v4, v8, v9, v10}, Lcom/android/internal/telephony/gsm/GsmSmsAddress;-><init>([BII)V

    #@116
    .line 811
    .local v4, replyAddressObj:Lcom/android/internal/telephony/gsm/GsmSmsAddress;
    if-eqz v4, :cond_11c

    #@118
    .line 812
    invoke-virtual {v4}, Lcom/android/internal/telephony/gsm/GsmSmsAddress;->getAddressString()Ljava/lang/String;
    :try_end_11b
    .catch Ljava/text/ParseException; {:try_start_102 .. :try_end_11b} :catch_12d

    #@11b
    move-result-object v3

    #@11c
    .line 819
    .end local v4           #replyAddressObj:Lcom/android/internal/telephony/gsm/GsmSmsAddress;
    :cond_11c
    :goto_11c
    if-eqz v5, :cond_16

    #@11e
    iget-object v8, v5, Lcom/android/internal/telephony/SmsHeader;->smsConfirmRead:Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;

    #@120
    if-eqz v8, :cond_16

    #@122
    .line 820
    iget-object v8, v5, Lcom/android/internal/telephony/SmsHeader;->smsConfirmRead:Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;

    #@124
    iget-object v8, v8, Lcom/android/internal/telephony/SmsHeader$SmsConfirmRead;->confirmRead:[B

    #@126
    const/4 v9, 0x0

    #@127
    aget-byte v8, v8, v9

    #@129
    and-int/lit16 v1, v8, 0xff

    #@12b
    goto/16 :goto_16

    #@12d
    .line 814
    :catch_12d
    move-exception v2

    #@12e
    .line 816
    .local v2, e:Ljava/text/ParseException;
    invoke-virtual {v2}, Ljava/text/ParseException;->printStackTrace()V

    #@131
    goto :goto_11c

    #@132
    .end local v2           #e:Ljava/text/ParseException;
    .end local v5           #smsHeader:Lcom/android/internal/telephony/SmsHeader;
    .restart local v7       #values:Landroid/content/ContentValues;
    :cond_132
    move-object v6, p2

    #@133
    .line 834
    goto/16 :goto_26

    #@135
    :cond_135
    move-object v6, v3

    #@136
    goto/16 :goto_26

    #@138
    .line 842
    :cond_138
    move-object v6, p2

    #@139
    .line 844
    .restart local v6       #uiAddress:Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@13c
    move-result v8

    #@13d
    if-eqz v8, :cond_146

    #@13f
    .line 845
    new-instance v6, Ljava/lang/String;

    #@141
    .end local v6           #uiAddress:Ljava/lang/String;
    const-string v8, "Unknown"

    #@143
    invoke-direct {v6, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@146
    .line 847
    .restart local v6       #uiAddress:Ljava/lang/String;
    :cond_146
    const-string v8, "address"

    #@148
    invoke-virtual {v7, v8, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@14b
    goto/16 :goto_38

    #@14d
    .line 880
    .end local p2
    :cond_14d
    const/4 v8, 0x0

    #@14e
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@151
    move-result-object v8

    #@152
    goto/16 :goto_81

    #@154
    .line 907
    :cond_154
    if-lez p20, :cond_f3

    #@156
    .line 908
    const-string v8, "reply_option"

    #@158
    invoke-static/range {p20 .. p20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@15b
    move-result-object v9

    #@15c
    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@15f
    goto :goto_f3
.end method

.method public static getDraftSmsNumber(Landroid/content/Context;J)Ljava/lang/String;
    .registers 23
    .parameter "context"
    .parameter "thread_Id"

    #@0
    .prologue
    .line 2629
    new-instance v13, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 2630
    .local v13, builder:Ljava/lang/StringBuilder;
    const/16 v18, 0x0

    #@7
    .line 2631
    .local v18, recipientids:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v3, "content://mms-sms/conversations/"

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    move-wide/from16 v0, p1

    #@14
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    const-string v3, "/recipients"

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@25
    move-result-object v4

    #@26
    .line 2632
    .local v4, muri:Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@29
    move-result-object v3

    #@2a
    const/4 v2, 0x1

    #@2b
    new-array v5, v2, [Ljava/lang/String;

    #@2d
    const/4 v2, 0x0

    #@2e
    const-string v6, "recipient_ids"

    #@30
    aput-object v6, v5, v2

    #@32
    const/4 v6, 0x0

    #@33
    const/4 v7, 0x0

    #@34
    const/4 v8, 0x0

    #@35
    move-object/from16 v2, p0

    #@37
    invoke-static/range {v2 .. v8}, Landroid/database/sqlite/SqliteWrapper;->query(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@3a
    move-result-object v19

    #@3b
    .line 2634
    .local v19, threadCursor:Landroid/database/Cursor;
    if-eqz v19, :cond_cc

    #@3d
    .line 2636
    :try_start_3d
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    #@40
    move-result v2

    #@41
    if-eqz v2, :cond_c9

    #@43
    .line 2637
    const-string v2, "recipient_ids"

    #@45
    move-object/from16 v0, v19

    #@47
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@4a
    move-result v2

    #@4b
    move-object/from16 v0, v19

    #@4d
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@50
    move-result-object v18

    #@51
    .line 2638
    if-eqz v18, :cond_c9

    #@53
    .line 2639
    const-string v2, " "

    #@55
    move-object/from16 v0, v18

    #@57
    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@5a
    move-result-object v12

    #@5b
    .local v12, arr$:[Ljava/lang/String;
    array-length v0, v12

    #@5c
    move/from16 v16, v0

    #@5e
    .local v16, len$:I
    const/4 v15, 0x0

    #@5f
    .local v15, i$:I
    :goto_5f
    move/from16 v0, v16

    #@61
    if-ge v15, v0, :cond_c9

    #@63
    aget-object v17, v12, v15

    #@65
    .line 2640
    .local v17, number:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@68
    move-result v2

    #@69
    if-nez v2, :cond_bc

    #@6b
    .line 2641
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->length()I

    #@6e
    move-result v2

    #@6f
    if-eqz v2, :cond_76

    #@71
    .line 2642
    const-string v2, ", "

    #@73
    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    .line 2644
    :cond_76
    new-instance v2, Ljava/lang/StringBuilder;

    #@78
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7b
    const-string v3, "content://mms-sms/canonical-address/"

    #@7d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v2

    #@81
    move-object/from16 v0, v17

    #@83
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v2

    #@87
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8a
    move-result-object v2

    #@8b
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@8e
    move-result-object v7

    #@8f
    .line 2645
    .local v7, uri:Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@92
    move-result-object v6

    #@93
    const/4 v2, 0x1

    #@94
    new-array v8, v2, [Ljava/lang/String;

    #@96
    const/4 v2, 0x0

    #@97
    const-string v3, "address"

    #@99
    aput-object v3, v8, v2

    #@9b
    const/4 v9, 0x0

    #@9c
    const/4 v10, 0x0

    #@9d
    const/4 v11, 0x0

    #@9e
    move-object/from16 v5, p0

    #@a0
    invoke-static/range {v5 .. v11}, Landroid/database/sqlite/SqliteWrapper;->query(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_a3
    .catchall {:try_start_3d .. :try_end_a3} :catchall_c4

    #@a3
    move-result-object v14

    #@a4
    .line 2647
    .local v14, canonicalCursor:Landroid/database/Cursor;
    if-eqz v14, :cond_bc

    #@a6
    .line 2649
    :try_start_a6
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    #@a9
    move-result v2

    #@aa
    if-eqz v2, :cond_b9

    #@ac
    .line 2650
    const-string v2, "address"

    #@ae
    invoke-interface {v14, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@b1
    move-result v2

    #@b2
    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@b5
    move-result-object v2

    #@b6
    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_b9
    .catchall {:try_start_a6 .. :try_end_b9} :catchall_bf

    #@b9
    .line 2654
    :cond_b9
    :try_start_b9
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    #@bc
    .line 2639
    .end local v7           #uri:Landroid/net/Uri;
    .end local v14           #canonicalCursor:Landroid/database/Cursor;
    :cond_bc
    add-int/lit8 v15, v15, 0x1

    #@be
    goto :goto_5f

    #@bf
    .line 2654
    .restart local v7       #uri:Landroid/net/Uri;
    .restart local v14       #canonicalCursor:Landroid/database/Cursor;
    :catchall_bf
    move-exception v2

    #@c0
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    #@c3
    throw v2
    :try_end_c4
    .catchall {:try_start_b9 .. :try_end_c4} :catchall_c4

    #@c4
    .line 2662
    .end local v7           #uri:Landroid/net/Uri;
    .end local v12           #arr$:[Ljava/lang/String;
    .end local v14           #canonicalCursor:Landroid/database/Cursor;
    .end local v15           #i$:I
    .end local v16           #len$:I
    .end local v17           #number:Ljava/lang/String;
    :catchall_c4
    move-exception v2

    #@c5
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    #@c8
    throw v2

    #@c9
    :cond_c9
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    #@cc
    .line 2665
    :cond_cc
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cf
    move-result-object v2

    #@d0
    return-object v2
.end method

.method public static isOutgoingFolder(I)Z
    .registers 2
    .parameter "messageType"

    #@0
    .prologue
    .line 982
    const/4 v0, 0x5

    #@1
    if-eq p0, v0, :cond_c

    #@3
    const/4 v0, 0x4

    #@4
    if-eq p0, v0, :cond_c

    #@6
    const/4 v0, 0x2

    #@7
    if-eq p0, v0, :cond_c

    #@9
    const/4 v0, 0x6

    #@a
    if-ne p0, v0, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public static moveMessageToFolder(Landroid/content/Context;Landroid/net/Uri;II)Z
    .registers 14
    .parameter "context"
    .parameter "uri"
    .parameter "folder"
    .parameter "error"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v8, 0x1

    #@2
    const/4 v9, 0x0

    #@3
    .line 924
    if-nez p1, :cond_6

    #@5
    .line 973
    :goto_5
    return v9

    #@6
    .line 928
    :cond_6
    const/4 v7, 0x0

    #@7
    .line 929
    .local v7, markAsUnread:Z
    const/4 v6, 0x0

    #@8
    .line 930
    .local v6, markAsRead:Z
    sparse-switch p2, :sswitch_data_54

    #@b
    goto :goto_5

    #@c
    .line 963
    :goto_c
    :sswitch_c
    new-instance v3, Landroid/content/ContentValues;

    #@e
    const/4 v0, 0x3

    #@f
    invoke-direct {v3, v0}, Landroid/content/ContentValues;-><init>(I)V

    #@12
    .line 965
    .local v3, values:Landroid/content/ContentValues;
    const-string v0, "type"

    #@14
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@1b
    .line 966
    if-eqz v7, :cond_45

    #@1d
    .line 967
    const-string v0, "read"

    #@1f
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@26
    .line 971
    :cond_26
    :goto_26
    const-string v0, "error_code"

    #@28
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@2f
    .line 973
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@32
    move-result-object v1

    #@33
    move-object v0, p0

    #@34
    move-object v2, p1

    #@35
    move-object v5, v4

    #@36
    invoke-static/range {v0 .. v5}, Landroid/database/sqlite/SqliteWrapper;->update(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@39
    move-result v0

    #@3a
    if-ne v8, v0, :cond_51

    #@3c
    move v0, v8

    #@3d
    :goto_3d
    move v9, v0

    #@3e
    goto :goto_5

    #@3f
    .line 936
    .end local v3           #values:Landroid/content/ContentValues;
    :sswitch_3f
    const/4 v6, 0x1

    #@40
    .line 937
    goto :goto_c

    #@41
    .line 940
    :sswitch_41
    const/4 v7, 0x1

    #@42
    .line 941
    goto :goto_c

    #@43
    .line 944
    :sswitch_43
    const/4 v6, 0x1

    #@44
    .line 945
    goto :goto_c

    #@45
    .line 968
    .restart local v3       #values:Landroid/content/ContentValues;
    :cond_45
    if-eqz v6, :cond_26

    #@47
    .line 969
    const-string v0, "read"

    #@49
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4c
    move-result-object v1

    #@4d
    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@50
    goto :goto_26

    #@51
    :cond_51
    move v0, v9

    #@52
    .line 973
    goto :goto_3d

    #@53
    .line 930
    nop

    #@54
    :sswitch_data_54
    .sparse-switch
        0x1 -> :sswitch_c
        0x2 -> :sswitch_3f
        0x3 -> :sswitch_c
        0x4 -> :sswitch_3f
        0x5 -> :sswitch_41
        0x6 -> :sswitch_41
        0x7 -> :sswitch_c
        0x8 -> :sswitch_43
        0x9 -> :sswitch_c
        0xa -> :sswitch_c
        0xb -> :sswitch_c
        0xc -> :sswitch_c
        0x34 -> :sswitch_c
        0x37 -> :sswitch_c
    .end sparse-switch
.end method

.method public static final query(Landroid/content/ContentResolver;[Ljava/lang/String;)Landroid/database/Cursor;
    .registers 8
    .parameter "cr"
    .parameter "projection"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 593
    sget-object v1, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    #@3
    const-string v5, "date DESC"

    #@5
    move-object v0, p0

    #@6
    move-object v2, p1

    #@7
    move-object v4, v3

    #@8
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method public static final query(Landroid/content/ContentResolver;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 10
    .parameter "cr"
    .parameter "projection"
    .parameter "where"
    .parameter "orderBy"

    #@0
    .prologue
    .line 598
    sget-object v1, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    #@2
    const/4 v4, 0x0

    #@3
    if-nez p3, :cond_f

    #@5
    const-string v5, "date DESC"

    #@7
    :goto_7
    move-object v0, p0

    #@8
    move-object v2, p1

    #@9
    move-object v3, p2

    #@a
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@d
    move-result-object v0

    #@e
    return-object v0

    #@f
    :cond_f
    move-object v5, p3

    #@10
    goto :goto_7
.end method

.method public static updateDeleteSmsToUri(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 6
    .parameter "resolver"
    .parameter "uri"
    .parameter "values"
    .parameter "where"
    .parameter "selectionArgs"

    #@0
    .prologue
    .line 2621
    const-string v0, "modified"

    #@2
    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_10

    #@8
    const-string v0, "modified_time"

    #@a
    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_15

    #@10
    .line 2622
    :cond_10
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@13
    move-result v0

    #@14
    .line 2624
    :goto_14
    return v0

    #@15
    :cond_15
    const/4 v0, 0x0

    #@16
    goto :goto_14
.end method
