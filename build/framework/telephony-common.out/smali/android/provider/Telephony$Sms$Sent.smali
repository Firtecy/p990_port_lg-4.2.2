.class public final Landroid/provider/Telephony$Sms$Sent;
.super Ljava/lang/Object;
.source "Telephony.java"

# interfaces
.implements Landroid/provider/BaseColumns;
.implements Landroid/provider/Telephony$TextBasedSmsColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/Telephony$Sms;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Sent"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final DEFAULT_SORT_ORDER:Ljava/lang/String; = "date DESC"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 1091
    const-string v0, "content://sms/sent"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/provider/Telephony$Sms$Sent;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 1087
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static addMessage(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)Landroid/net/Uri;
    .registers 14
    .parameter "resolver"
    .parameter "address"
    .parameter "body"
    .parameter "subject"
    .parameter "date"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 1112
    const/4 v0, 0x0

    #@3
    const-string v1, "KROperator"

    #@5
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@8
    move-result v0

    #@9
    if-ne v6, v0, :cond_18

    #@b
    .line 1113
    sget-object v1, Landroid/provider/Telephony$Sms$Sent;->CONTENT_URI:Landroid/net/Uri;

    #@d
    move-object v0, p0

    #@e
    move-object v2, p1

    #@f
    move-object v3, p2

    #@10
    move-object v4, p3

    #@11
    move-object v5, p4

    #@12
    move v8, v7

    #@13
    invoke-static/range {v0 .. v8}, Landroid/provider/Telephony$Sms;->addMessageToUri(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZI)Landroid/net/Uri;

    #@16
    move-result-object v0

    #@17
    .line 1119
    :goto_17
    return-object v0

    #@18
    :cond_18
    sget-object v1, Landroid/provider/Telephony$Sms$Sent;->CONTENT_URI:Landroid/net/Uri;

    #@1a
    invoke-static {}, Landroid/telephony/MSimSmsManager;->getDefault()Landroid/telephony/MSimSmsManager;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0}, Landroid/telephony/MSimSmsManager;->getPreferredSmsSubscription()I

    #@21
    move-result v8

    #@22
    move-object v0, p0

    #@23
    move-object v2, p1

    #@24
    move-object v3, p2

    #@25
    move-object v4, p3

    #@26
    move-object v5, p4

    #@27
    invoke-static/range {v0 .. v8}, Landroid/provider/Telephony$Sms;->addMessageToUri(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZI)Landroid/net/Uri;

    #@2a
    move-result-object v0

    #@2b
    goto :goto_17
.end method

.method public static addMessage(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;I)Landroid/net/Uri;
    .registers 19
    .parameter "resolver"
    .parameter "address"
    .parameter "body"
    .parameter "subject"
    .parameter "date"
    .parameter "subId"

    #@0
    .prologue
    .line 1137
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    const-string v2, "KROperator"

    #@4
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@7
    move-result v1

    #@8
    if-ne v0, v1, :cond_1e

    #@a
    .line 1138
    const-wide/16 v5, -0x1

    #@c
    const/4 v7, 0x0

    #@d
    const/4 v8, -0x1

    #@e
    const-wide/16 v9, -0x1

    #@10
    const/4 v11, 0x1

    #@11
    const/4 v12, 0x0

    #@12
    move-object v0, p0

    #@13
    move-object v1, p1

    #@14
    move-object v2, p2

    #@15
    move-object/from16 v3, p3

    #@17
    move-object/from16 v4, p4

    #@19
    invoke-static/range {v0 .. v12}, Landroid/provider/Telephony$Sms$Sent;->addMessageEx(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;JIIJILjava/lang/String;)Landroid/net/Uri;

    #@1c
    move-result-object v0

    #@1d
    .line 1149
    :goto_1d
    return-object v0

    #@1e
    :cond_1e
    sget-object v1, Landroid/provider/Telephony$Sms$Sent;->CONTENT_URI:Landroid/net/Uri;

    #@20
    const/4 v6, 0x1

    #@21
    const/4 v7, 0x0

    #@22
    move-object v0, p0

    #@23
    move-object v2, p1

    #@24
    move-object v3, p2

    #@25
    move-object/from16 v4, p3

    #@27
    move-object/from16 v5, p4

    #@29
    move/from16 v8, p5

    #@2b
    invoke-static/range {v0 .. v8}, Landroid/provider/Telephony$Sms;->addMessageToUri(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZI)Landroid/net/Uri;

    #@2e
    move-result-object v0

    #@2f
    goto :goto_1d
.end method

.method public static addMessageEx(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;JIIJILjava/lang/String;)Landroid/net/Uri;
    .registers 27
    .parameter "resolver"
    .parameter "recipient"
    .parameter "body"
    .parameter "subject"
    .parameter "date"
    .parameter "threadId"
    .parameter "protocol"
    .parameter "dcs"
    .parameter "groupid"
    .parameter "simcopy"
    .parameter "replyAddress"

    #@0
    .prologue
    .line 1156
    const/4 v1, -0x1

    #@1
    move-object/from16 v0, p12

    #@3
    invoke-static {v1, v0}, Landroid/telephony/SmsMessage;->makeSmsHeader(ILjava/lang/String;)[B

    #@6
    move-result-object v13

    #@7
    move-object v1, p0

    #@8
    move-object v2, p1

    #@9
    move-object/from16 v3, p2

    #@b
    move-object/from16 v4, p3

    #@d
    move-object/from16 v5, p4

    #@f
    move-wide/from16 v6, p5

    #@11
    move/from16 v8, p7

    #@13
    move/from16 v9, p8

    #@15
    move-wide/from16 v10, p9

    #@17
    move/from16 v12, p11

    #@19
    invoke-static/range {v1 .. v13}, Landroid/provider/Telephony$Sms$Sent;->addMessageExEx(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;JIIJI[B)Landroid/net/Uri;

    #@1c
    move-result-object v1

    #@1d
    return-object v1
.end method

.method public static addMessageExEx(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;JIIJI[B)Landroid/net/Uri;
    .registers 34
    .parameter "resolver"
    .parameter "recipient"
    .parameter "body"
    .parameter "subject"
    .parameter "date"
    .parameter "threadId"
    .parameter "protocol"
    .parameter "dcs"
    .parameter "groupid"
    .parameter "simcopy"
    .parameter "header"

    #@0
    .prologue
    .line 1164
    sget-object v1, Landroid/provider/Telephony$Sms$Sent;->CONTENT_URI:Landroid/net/Uri;

    #@2
    const/4 v6, 0x1

    #@3
    const/4 v7, 0x0

    #@4
    const/4 v15, 0x0

    #@5
    const/16 v16, 0x2

    #@7
    const/16 v18, 0x0

    #@9
    const/16 v19, 0x0

    #@b
    const/16 v20, 0x0

    #@d
    move-object/from16 v0, p0

    #@f
    move-object/from16 v2, p1

    #@11
    move-object/from16 v3, p2

    #@13
    move-object/from16 v4, p3

    #@15
    move-object/from16 v5, p4

    #@17
    move-wide/from16 v8, p5

    #@19
    move/from16 v10, p7

    #@1b
    move/from16 v11, p8

    #@1d
    move-wide/from16 v12, p9

    #@1f
    move/from16 v14, p11

    #@21
    move-object/from16 v17, p12

    #@23
    invoke-static/range {v0 .. v20}, Landroid/provider/Telephony$Sms;->addMessageToUriEx(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZJIIJIII[BZLjava/lang/String;I)Landroid/net/Uri;

    #@26
    move-result-object v0

    #@27
    return-object v0
.end method
