.class public final Landroid/provider/Telephony$Sms$Outbox;
.super Ljava/lang/Object;
.source "Telephony.java"

# interfaces
.implements Landroid/provider/BaseColumns;
.implements Landroid/provider/Telephony$TextBasedSmsColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/Telephony$Sms;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Outbox"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final DEFAULT_SORT_ORDER:Ljava/lang/String; = "date DESC"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 1275
    const-string v0, "content://sms/outbox"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/provider/Telephony$Sms$Outbox;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 1271
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static addMessage(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZJ)Landroid/net/Uri;
    .registers 21
    .parameter "resolver"
    .parameter "address"
    .parameter "body"
    .parameter "subject"
    .parameter "date"
    .parameter "deliveryReport"
    .parameter "threadId"

    #@0
    .prologue
    .line 1298
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    const-string v2, "KROperator"

    #@4
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@7
    move-result v1

    #@8
    if-ne v0, v1, :cond_1f

    #@a
    .line 1299
    const/4 v8, 0x0

    #@b
    const/4 v9, -0x1

    #@c
    const-wide/16 v10, -0x1

    #@e
    const/4 v12, 0x0

    #@f
    move-object v0, p0

    #@10
    move-object v1, p1

    #@11
    move-object v2, p2

    #@12
    move-object/from16 v3, p3

    #@14
    move-object/from16 v4, p4

    #@16
    move/from16 v5, p5

    #@18
    move-wide/from16 v6, p6

    #@1a
    invoke-static/range {v0 .. v12}, Landroid/provider/Telephony$Sms$Outbox;->addMessageEx(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZJIIJLjava/lang/String;)Landroid/net/Uri;

    #@1d
    move-result-object v0

    #@1e
    .line 1308
    :goto_1e
    return-object v0

    #@1f
    :cond_1f
    sget-object v1, Landroid/provider/Telephony$Sms$Outbox;->CONTENT_URI:Landroid/net/Uri;

    #@21
    const/4 v6, 0x1

    #@22
    invoke-static {}, Landroid/telephony/MSimSmsManager;->getDefault()Landroid/telephony/MSimSmsManager;

    #@25
    move-result-object v0

    #@26
    invoke-virtual {v0}, Landroid/telephony/MSimSmsManager;->getPreferredSmsSubscription()I

    #@29
    move-result v10

    #@2a
    move-object v0, p0

    #@2b
    move-object v2, p1

    #@2c
    move-object v3, p2

    #@2d
    move-object/from16 v4, p3

    #@2f
    move-object/from16 v5, p4

    #@31
    move/from16 v7, p5

    #@33
    move-wide/from16 v8, p6

    #@35
    invoke-static/range {v0 .. v10}, Landroid/provider/Telephony$Sms;->addMessageToUri(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZJI)Landroid/net/Uri;

    #@38
    move-result-object v0

    #@39
    goto :goto_1e
.end method

.method public static addMessage(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZJI)Landroid/net/Uri;
    .registers 20
    .parameter "resolver"
    .parameter "address"
    .parameter "body"
    .parameter "subject"
    .parameter "date"
    .parameter "deliveryReport"
    .parameter "threadId"
    .parameter "subId"

    #@0
    .prologue
    .line 1328
    sget-object v1, Landroid/provider/Telephony$Sms$Outbox;->CONTENT_URI:Landroid/net/Uri;

    #@2
    const/4 v6, 0x1

    #@3
    move-object v0, p0

    #@4
    move-object v2, p1

    #@5
    move-object v3, p2

    #@6
    move-object v4, p3

    #@7
    move-object v5, p4

    #@8
    move/from16 v7, p5

    #@a
    move-wide/from16 v8, p6

    #@c
    move/from16 v10, p8

    #@e
    invoke-static/range {v0 .. v10}, Landroid/provider/Telephony$Sms;->addMessageToUri(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZJI)Landroid/net/Uri;

    #@11
    move-result-object v0

    #@12
    return-object v0
.end method

.method public static addMessageEx(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZJIIJLjava/lang/String;)Landroid/net/Uri;
    .registers 27
    .parameter "resolver"
    .parameter "recipient"
    .parameter "body"
    .parameter "subject"
    .parameter "date"
    .parameter "deliveryReport"
    .parameter "threadId"
    .parameter "protocol"
    .parameter "dcs"
    .parameter "groupid"
    .parameter "replyAddress"

    #@0
    .prologue
    .line 1336
    const/4 v1, -0x1

    #@1
    move-object/from16 v0, p12

    #@3
    invoke-static {v1, v0}, Landroid/telephony/SmsMessage;->makeSmsHeader(ILjava/lang/String;)[B

    #@6
    move-result-object v13

    #@7
    move-object v1, p0

    #@8
    move-object v2, p1

    #@9
    move-object/from16 v3, p2

    #@b
    move-object/from16 v4, p3

    #@d
    move-object/from16 v5, p4

    #@f
    move/from16 v6, p5

    #@11
    move-wide/from16 v7, p6

    #@13
    move/from16 v9, p8

    #@15
    move/from16 v10, p9

    #@17
    move-wide/from16 v11, p10

    #@19
    invoke-static/range {v1 .. v13}, Landroid/provider/Telephony$Sms$Outbox;->addMessageExEx(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZJIIJ[B)Landroid/net/Uri;

    #@1c
    move-result-object v1

    #@1d
    return-object v1
.end method

.method public static addMessageExEx(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZJIIJ[B)Landroid/net/Uri;
    .registers 34
    .parameter "resolver"
    .parameter "recipient"
    .parameter "body"
    .parameter "subject"
    .parameter "date"
    .parameter "deliveryReport"
    .parameter "threadId"
    .parameter "protocol"
    .parameter "dcs"
    .parameter "groupid"
    .parameter "header"

    #@0
    .prologue
    .line 1344
    sget-object v1, Landroid/provider/Telephony$Sms$Outbox;->CONTENT_URI:Landroid/net/Uri;

    #@2
    const/4 v6, 0x1

    #@3
    const/4 v14, 0x0

    #@4
    const/4 v15, 0x0

    #@5
    const/16 v16, 0x5

    #@7
    const/16 v18, 0x0

    #@9
    const/16 v19, 0x0

    #@b
    const/16 v20, 0x0

    #@d
    move-object/from16 v0, p0

    #@f
    move-object/from16 v2, p1

    #@11
    move-object/from16 v3, p2

    #@13
    move-object/from16 v4, p3

    #@15
    move-object/from16 v5, p4

    #@17
    move/from16 v7, p5

    #@19
    move-wide/from16 v8, p6

    #@1b
    move/from16 v10, p8

    #@1d
    move/from16 v11, p9

    #@1f
    move-wide/from16 v12, p10

    #@21
    move-object/from16 v17, p12

    #@23
    invoke-static/range {v0 .. v20}, Landroid/provider/Telephony$Sms;->addMessageToUriEx(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZJIIJIII[BZLjava/lang/String;I)Landroid/net/Uri;

    #@26
    move-result-object v0

    #@27
    return-object v0
.end method
