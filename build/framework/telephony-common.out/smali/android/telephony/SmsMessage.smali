.class public Landroid/telephony/SmsMessage;
.super Ljava/lang/Object;
.source "SmsMessage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/telephony/SmsMessage$1;,
        Landroid/telephony/SmsMessage$DeliverPdu;,
        Landroid/telephony/SmsMessage$SubmitPdu;,
        Landroid/telephony/SmsMessage$MessageClass;
    }
.end annotation


# static fields
.field public static final ENCODING_16BIT:I = 0x3

.field public static final ENCODING_7BIT:I = 0x1

.field public static final ENCODING_8BIT:I = 0x2

.field public static final ENCODING_KSC5601:I = 0x4

.field public static final ENCODING_UNKNOWN:I = 0x0

.field public static final FORMAT_3GPP:Ljava/lang/String; = "3gpp"

.field public static final FORMAT_3GPP2:Ljava/lang/String; = "3gpp2"

#the value of this static final field might be set in the static constructor
.field public static final LIMIT_USER_DATA_SEPTETS:I = 0x0

.field private static final LOG_TAG:Ljava/lang/String; = "SMS"

.field public static final MAX_USER_DATA_BYTES:I = 0x8c

#the value of this static final field might be set in the static constructor
.field public static final MAX_USER_DATA_BYTES_EX:I = 0x0

.field public static final MAX_USER_DATA_BYTES_WITH_HEADER:I = 0x86

.field public static final MAX_USER_DATA_BYTES_WITH_HEADER_REPLYADDRESS:I = 0x7d

.field public static final MAX_USER_DATA_BYTES_WITH_HEADER_REPLYADDRESS_CONCAT:I = 0x78

.field public static final MAX_USER_DATA_SEPTETS:I = 0xa0

#the value of this static final field might be set in the static constructor
.field public static final MAX_USER_DATA_SEPTETS_EX:I = 0x0

.field public static final MAX_USER_DATA_SEPTETS_WITH_HEADER:I = 0x99

.field public static final MAX_USER_DATA_SEPTETS_WITH_HEADER_REPLYADDRESS:I = 0x8e

.field public static final MAX_USER_DATA_SEPTETS_WITH_HEADER_REPLYADDRESS_CONCAT:I = 0x89

.field public static final MIMENAME_EUC_KR:Ljava/lang/String; = "euc-kr"

.field public static final MIMENAME_KSC5601:Ljava/lang/String; = "ksc5601"

.field public static final MSG_WAITING_DISCARD:I = 0x1

.field public static final MSG_WAITING_EMAIL:I = 0x2

.field public static final MSG_WAITING_FAX:I = 0x1

.field public static final MSG_WAITING_NONE:I = 0x0

.field public static final MSG_WAITING_NONE_1000:I = 0x4

.field public static final MSG_WAITING_NONE_1111:I = 0x3

.field public static final MSG_WAITING_OTHER:I = 0x3

.field public static final MSG_WAITING_STORE:I = 0x2

.field public static final MSG_WAITING_VOICEMAIL:I = 0x0

.field public static final PID_INTERNET_EMAIL:I = 0x32

.field public static mEncodingtype:I


# instance fields
.field private mSubId:I

.field public mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/16 v1, 0xa0

    #@2
    const/4 v3, 0x0

    #@3
    .line 170
    const-string v0, "max_user_data_bytes_ex"

    #@5
    invoke-static {v3, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getFeatureInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    const-string v2, ""

    #@b
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_3a

    #@11
    const/16 v0, 0x8c

    #@13
    :goto_13
    sput v0, Landroid/telephony/SmsMessage;->MAX_USER_DATA_BYTES_EX:I

    #@15
    .line 179
    const-string v0, "max_user_data_septets_ex"

    #@17
    invoke-static {v3, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getFeatureInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    const-string v2, ""

    #@1d
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v0

    #@21
    if-eqz v0, :cond_49

    #@23
    move v0, v1

    #@24
    :goto_24
    sput v0, Landroid/telephony/SmsMessage;->MAX_USER_DATA_SEPTETS_EX:I

    #@26
    .line 188
    const-string v0, "limit_user_data_septets"

    #@28
    invoke-static {v3, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getFeatureInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    #@2b
    move-result-object v0

    #@2c
    const-string v2, ""

    #@2e
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31
    move-result v0

    #@32
    if-eqz v0, :cond_58

    #@34
    :goto_34
    sput v1, Landroid/telephony/SmsMessage;->LIMIT_USER_DATA_SEPTETS:I

    #@36
    .line 203
    const/4 v0, -0x1

    #@37
    sput v0, Landroid/telephony/SmsMessage;->mEncodingtype:I

    #@39
    return-void

    #@3a
    .line 170
    :cond_3a
    const-string v0, "max_user_data_bytes_ex"

    #@3c
    invoke-static {v3, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getFeatureInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    #@3f
    move-result-object v0

    #@40
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    #@43
    move-result-object v0

    #@44
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@47
    move-result v0

    #@48
    goto :goto_13

    #@49
    .line 179
    :cond_49
    const-string v0, "max_user_data_septets_ex"

    #@4b
    invoke-static {v3, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getFeatureInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    #@4e
    move-result-object v0

    #@4f
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    #@52
    move-result-object v0

    #@53
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@56
    move-result v0

    #@57
    goto :goto_24

    #@58
    .line 188
    :cond_58
    const-string v0, "limit_user_data_septets"

    #@5a
    invoke-static {v3, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getFeatureInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    #@5d
    move-result-object v0

    #@5e
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    #@61
    move-result-object v0

    #@62
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@65
    move-result v1

    #@66
    goto :goto_34
.end method

.method private constructor <init>(Lcom/android/internal/telephony/SmsMessageBase;)V
    .registers 3
    .parameter "smb"

    #@0
    .prologue
    .line 301
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 223
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/telephony/SmsMessage;->mSubId:I

    #@6
    .line 302
    iput-object p1, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@8
    .line 303
    return-void
.end method

.method public static calculateEmojiLength(Ljava/lang/String;Z)[I
    .registers 15
    .parameter "text"
    .parameter "use7bitOnly"

    #@0
    .prologue
    const/4 v12, 0x3

    #@1
    const/4 v11, 0x2

    #@2
    const/4 v10, 0x0

    #@3
    const/4 v9, 0x1

    #@4
    .line 824
    invoke-static {}, Landroid/telephony/SmsMessage;->useCdmaFormatForMoSms()Z

    #@7
    move-result v7

    #@8
    if-eqz v7, :cond_26

    #@a
    invoke-static {p0, v10}, Lcom/android/internal/telephony/cdma/SmsMessage;->calculateLGLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@d
    move-result-object v5

    #@e
    .line 829
    .local v5, ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :goto_e
    const/4 v7, 0x4

    #@f
    new-array v4, v7, [I

    #@11
    .line 831
    .local v4, ret:[I
    iget v7, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@13
    if-ne v7, v9, :cond_2b

    #@15
    .line 832
    iget v7, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@17
    aput v7, v4, v10

    #@19
    .line 833
    iget v7, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@1b
    aput v7, v4, v9

    #@1d
    .line 834
    iget v7, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@1f
    aput v7, v4, v11

    #@21
    .line 835
    iget v7, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@23
    aput v7, v4, v12

    #@25
    .line 876
    :goto_25
    return-object v4

    #@26
    .line 824
    .end local v4           #ret:[I
    .end local v5           #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_26
    invoke-static {p0, p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->calculateLGLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@29
    move-result-object v5

    #@2a
    goto :goto_e

    #@2b
    .line 839
    .restart local v4       #ret:[I
    .restart local v5       #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_2b
    iget v7, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@2d
    if-le v7, v9, :cond_a6

    #@2f
    .line 840
    const/16 v0, 0x86

    #@31
    .line 845
    .local v0, limit:I
    :goto_31
    const/4 v2, 0x0

    #@32
    .line 846
    .local v2, pos:I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@35
    move-result v6

    #@36
    .line 847
    .local v6, textLen:I
    new-instance v3, Ljava/util/ArrayList;

    #@38
    iget v7, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@3a
    invoke-direct {v3, v7}, Ljava/util/ArrayList;-><init>(I)V

    #@3d
    .line 848
    .local v3, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_3d
    if-ge v2, v6, :cond_60

    #@3f
    .line 849
    const/4 v1, 0x0

    #@40
    .line 851
    .local v1, nextPos:I
    div-int/lit8 v7, v0, 0x2

    #@42
    sub-int v8, v6, v2

    #@44
    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    #@47
    move-result v7

    #@48
    add-int v1, v2, v7

    #@4a
    .line 853
    iget v7, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@4c
    if-le v7, v9, :cond_5c

    #@4e
    add-int/lit8 v7, v1, -0x1

    #@50
    invoke-virtual {p0, v7}, Ljava/lang/String;->charAt(I)C

    #@53
    move-result v7

    #@54
    invoke-static {v7}, Ljava/lang/Character;->isHighSurrogate(C)Z

    #@57
    move-result v7

    #@58
    if-eqz v7, :cond_5c

    #@5a
    .line 854
    add-int/lit8 v1, v1, -0x1

    #@5c
    .line 856
    :cond_5c
    if-le v1, v2, :cond_60

    #@5e
    if-le v1, v6, :cond_a9

    #@60
    .line 865
    .end local v1           #nextPos:I
    :cond_60
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@63
    move-result v7

    #@64
    iput v7, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@66
    .line 866
    iget v7, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@68
    if-le v7, v9, :cond_95

    #@6a
    .line 867
    iget v7, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@6c
    add-int/lit8 v7, v7, -0x1

    #@6e
    mul-int/lit16 v7, v7, 0x86

    #@70
    div-int/lit8 v8, v7, 0x2

    #@72
    iget v7, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@74
    add-int/lit8 v7, v7, -0x1

    #@76
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@79
    move-result-object v7

    #@7a
    check-cast v7, Ljava/lang/String;

    #@7c
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@7f
    move-result v7

    #@80
    add-int/2addr v7, v8

    #@81
    iput v7, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@83
    .line 868
    iget v7, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@85
    add-int/lit8 v7, v7, -0x1

    #@87
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@8a
    move-result-object v7

    #@8b
    check-cast v7, Ljava/lang/String;

    #@8d
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@90
    move-result v7

    #@91
    rsub-int/lit8 v7, v7, 0x43

    #@93
    iput v7, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@95
    .line 871
    :cond_95
    iget v7, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@97
    aput v7, v4, v10

    #@99
    .line 872
    iget v7, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@9b
    aput v7, v4, v9

    #@9d
    .line 873
    iget v7, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@9f
    aput v7, v4, v11

    #@a1
    .line 874
    iget v7, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@a3
    aput v7, v4, v12

    #@a5
    goto :goto_25

    #@a6
    .line 842
    .end local v0           #limit:I
    .end local v2           #pos:I
    .end local v3           #result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v6           #textLen:I
    :cond_a6
    const/16 v0, 0x8c

    #@a8
    .restart local v0       #limit:I
    goto :goto_31

    #@a9
    .line 861
    .restart local v1       #nextPos:I
    .restart local v2       #pos:I
    .restart local v3       #result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v6       #textLen:I
    :cond_a9
    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@ac
    move-result-object v7

    #@ad
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b0
    .line 862
    move v2, v1

    #@b1
    .line 863
    goto :goto_3d
.end method

.method public static calculateLGLength(Ljava/lang/CharSequence;Z)[I
    .registers 6
    .parameter "msgBody"
    .parameter "use7bitOnly"

    #@0
    .prologue
    .line 497
    const/4 v2, 0x0

    #@1
    const-string v3, "support_emoji_in_concat_message"

    #@3
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@6
    move-result v2

    #@7
    if-eqz v2, :cond_12

    #@9
    .line 498
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@c
    move-result-object v2

    #@d
    invoke-static {v2, p1}, Landroid/telephony/SmsMessage;->calculateEmojiLength(Ljava/lang/String;Z)[I

    #@10
    move-result-object v0

    #@11
    .line 510
    :goto_11
    return-object v0

    #@12
    .line 502
    :cond_12
    invoke-static {}, Landroid/telephony/SmsMessage;->useCdmaFormatForMoSms()Z

    #@15
    move-result v2

    #@16
    if-eqz v2, :cond_34

    #@18
    invoke-static {p0, p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->calculateLGLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@1b
    move-result-object v1

    #@1c
    .line 505
    .local v1, ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :goto_1c
    const/4 v2, 0x4

    #@1d
    new-array v0, v2, [I

    #@1f
    .line 506
    .local v0, ret:[I
    const/4 v2, 0x0

    #@20
    iget v3, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@22
    aput v3, v0, v2

    #@24
    .line 507
    const/4 v2, 0x1

    #@25
    iget v3, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@27
    aput v3, v0, v2

    #@29
    .line 508
    const/4 v2, 0x2

    #@2a
    iget v3, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@2c
    aput v3, v0, v2

    #@2e
    .line 509
    const/4 v2, 0x3

    #@2f
    iget v3, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@31
    aput v3, v0, v2

    #@33
    goto :goto_11

    #@34
    .line 502
    .end local v0           #ret:[I
    .end local v1           #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_34
    invoke-static {p0, p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->calculateLGLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@37
    move-result-object v1

    #@38
    goto :goto_1c
.end method

.method public static calculateLGLength(Ljava/lang/String;Z)[I
    .registers 3
    .parameter "messageBody"
    .parameter "use7bitOnly"

    #@0
    .prologue
    .line 1085
    invoke-static {p0, p1}, Landroid/telephony/SmsMessage;->calculateLGLength(Ljava/lang/CharSequence;Z)[I

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static calculateLength(Ljava/lang/CharSequence;Z)[I
    .registers 6
    .parameter "msgBody"
    .parameter "use7bitOnly"

    #@0
    .prologue
    .line 478
    invoke-static {}, Landroid/telephony/SmsMessage;->useCdmaFormatForMoSms()Z

    #@3
    move-result v2

    #@4
    if-eqz v2, :cond_22

    #@6
    invoke-static {p0, p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->calculateLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@9
    move-result-object v1

    #@a
    .line 481
    .local v1, ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :goto_a
    const/4 v2, 0x4

    #@b
    new-array v0, v2, [I

    #@d
    .line 482
    .local v0, ret:[I
    const/4 v2, 0x0

    #@e
    iget v3, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@10
    aput v3, v0, v2

    #@12
    .line 483
    const/4 v2, 0x1

    #@13
    iget v3, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@15
    aput v3, v0, v2

    #@17
    .line 484
    const/4 v2, 0x2

    #@18
    iget v3, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@1a
    aput v3, v0, v2

    #@1c
    .line 485
    const/4 v2, 0x3

    #@1d
    iget v3, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@1f
    aput v3, v0, v2

    #@21
    .line 486
    return-object v0

    #@22
    .line 478
    .end local v0           #ret:[I
    .end local v1           #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_22
    invoke-static {p0, p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->calculateLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@25
    move-result-object v1

    #@26
    goto :goto_a
.end method

.method public static calculateLength(Ljava/lang/CharSequence;ZZ)[I
    .registers 9
    .parameter "msgBody"
    .parameter "use7bitOnly"
    .parameter "userInterface"

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    .line 535
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@4
    move-result-object v3

    #@5
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    #@8
    move-result v0

    #@9
    .line 536
    .local v0, activePhone:I
    const/4 v2, 0x0

    #@a
    .line 537
    .local v2, ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    if-ne v5, v0, :cond_27

    #@c
    .line 538
    invoke-static {p0, p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->calculateLengthEx(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@f
    move-result-object v2

    #@10
    .line 542
    :goto_10
    const/4 v3, 0x4

    #@11
    new-array v1, v3, [I

    #@13
    .line 543
    .local v1, ret:[I
    const/4 v3, 0x0

    #@14
    iget v4, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@16
    aput v4, v1, v3

    #@18
    .line 544
    const/4 v3, 0x1

    #@19
    iget v4, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@1b
    aput v4, v1, v3

    #@1d
    .line 545
    iget v3, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@1f
    aput v3, v1, v5

    #@21
    .line 546
    const/4 v3, 0x3

    #@22
    iget v4, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@24
    aput v4, v1, v3

    #@26
    .line 547
    return-object v1

    #@27
    .line 540
    .end local v1           #ret:[I
    :cond_27
    invoke-static {p0, p1, p2}, Lcom/android/internal/telephony/gsm/SmsMessage;->calculateLength(Ljava/lang/CharSequence;ZZ)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@2a
    move-result-object v2

    #@2b
    goto :goto_10
.end method

.method public static calculateLength(Ljava/lang/String;Z)[I
    .registers 3
    .parameter "messageBody"
    .parameter "use7bitOnly"

    #@0
    .prologue
    .line 1077
    invoke-static {p0, p1}, Landroid/telephony/SmsMessage;->calculateLength(Ljava/lang/CharSequence;Z)[I

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static calculateLengthEx(Ljava/lang/String;ZZ)[I
    .registers 4
    .parameter "messageBody"
    .parameter "use7bitOnly"
    .parameter "userInterface"

    #@0
    .prologue
    .line 1108
    invoke-static {p0, p1, p2}, Landroid/telephony/SmsMessage;->calculateLength(Ljava/lang/CharSequence;ZZ)[I

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static calculateLengthVZWGlobalMode(Ljava/lang/CharSequence;Z)[I
    .registers 6
    .parameter "msgBody"
    .parameter "use7bitOnly"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 571
    invoke-static {}, Landroid/telephony/SmsMessage;->useCdmaFormatForMoSms()Z

    #@4
    move-result v2

    #@5
    if-eqz v2, :cond_22

    #@7
    invoke-static {p0, v3}, Lcom/android/internal/telephony/cdma/SmsMessage;->calculateLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@a
    move-result-object v1

    #@b
    .line 574
    .local v1, ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :goto_b
    const/4 v2, 0x4

    #@c
    new-array v0, v2, [I

    #@e
    .line 575
    .local v0, ret:[I
    iget v2, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@10
    aput v2, v0, v3

    #@12
    .line 576
    const/4 v2, 0x1

    #@13
    iget v3, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@15
    aput v3, v0, v2

    #@17
    .line 577
    const/4 v2, 0x2

    #@18
    iget v3, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@1a
    aput v3, v0, v2

    #@1c
    .line 578
    const/4 v2, 0x3

    #@1d
    iget v3, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@1f
    aput v3, v0, v2

    #@21
    .line 579
    return-object v0

    #@22
    .line 571
    .end local v0           #ret:[I
    .end local v1           #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_22
    invoke-static {p0, p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->calculateLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@25
    move-result-object v1

    #@26
    goto :goto_b
.end method

.method public static calculateLengthVZWGlobalMode(Ljava/lang/CharSequence;ZZ)[I
    .registers 7
    .parameter "msgBody"
    .parameter "use7bitOnly"
    .parameter "is3GPP2Composing"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 602
    invoke-static {p2}, Landroid/telephony/SmsMessage;->useCdmaFormatForMoSms(Z)Z

    #@4
    move-result v2

    #@5
    if-eqz v2, :cond_22

    #@7
    invoke-static {p0, v3}, Lcom/android/internal/telephony/cdma/SmsMessage;->calculateLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@a
    move-result-object v1

    #@b
    .line 605
    .local v1, ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :goto_b
    const/4 v2, 0x4

    #@c
    new-array v0, v2, [I

    #@e
    .line 606
    .local v0, ret:[I
    iget v2, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@10
    aput v2, v0, v3

    #@12
    .line 607
    const/4 v2, 0x1

    #@13
    iget v3, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@15
    aput v3, v0, v2

    #@17
    .line 608
    const/4 v2, 0x2

    #@18
    iget v3, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@1a
    aput v3, v0, v2

    #@1c
    .line 609
    const/4 v2, 0x3

    #@1d
    iget v3, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@1f
    aput v3, v0, v2

    #@21
    .line 610
    return-object v0

    #@22
    .line 602
    .end local v0           #ret:[I
    .end local v1           #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_22
    invoke-static {p0, p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->calculateLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@25
    move-result-object v1

    #@26
    goto :goto_b
.end method

.method public static createFromEfRecord(I[B)Landroid/telephony/SmsMessage;
    .registers 5
    .parameter "index"
    .parameter "data"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 411
    const-string v2, "control_uicc_storage"

    #@3
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@6
    move-result v2

    #@7
    if-eqz v2, :cond_20

    #@9
    .line 412
    invoke-static {}, Landroid/telephony/SmsMessage;->isCdmaVoice()Z

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_1b

    #@f
    .line 413
    invoke-static {p0, p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->createFromEfRecord(I[B)Lcom/android/internal/telephony/cdma/SmsMessage;

    #@12
    move-result-object v0

    #@13
    .line 433
    .local v0, wrappedMessage:Lcom/android/internal/telephony/SmsMessageBase;
    :goto_13
    if-eqz v0, :cond_1a

    #@15
    new-instance v1, Landroid/telephony/SmsMessage;

    #@17
    invoke-direct {v1, v0}, Landroid/telephony/SmsMessage;-><init>(Lcom/android/internal/telephony/SmsMessageBase;)V

    #@1a
    :cond_1a
    return-object v1

    #@1b
    .line 416
    .end local v0           #wrappedMessage:Lcom/android/internal/telephony/SmsMessageBase;
    :cond_1b
    invoke-static {p0, p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->createFromEfRecord(I[B)Lcom/android/internal/telephony/gsm/SmsMessage;

    #@1e
    move-result-object v0

    #@1f
    .restart local v0       #wrappedMessage:Lcom/android/internal/telephony/SmsMessageBase;
    goto :goto_13

    #@20
    .line 423
    .end local v0           #wrappedMessage:Lcom/android/internal/telephony/SmsMessageBase;
    :cond_20
    invoke-static {}, Landroid/telephony/SmsMessage;->isCdmaVoice()Z

    #@23
    move-result v2

    #@24
    if-eqz v2, :cond_33

    #@26
    const-string v2, "save_usim_3gpp_in_cdma"

    #@28
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@2b
    move-result v2

    #@2c
    if-nez v2, :cond_33

    #@2e
    .line 425
    invoke-static {p0, p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->createFromEfRecord(I[B)Lcom/android/internal/telephony/cdma/SmsMessage;

    #@31
    move-result-object v0

    #@32
    .restart local v0       #wrappedMessage:Lcom/android/internal/telephony/SmsMessageBase;
    goto :goto_13

    #@33
    .line 428
    .end local v0           #wrappedMessage:Lcom/android/internal/telephony/SmsMessageBase;
    :cond_33
    invoke-static {p0, p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->createFromEfRecord(I[B)Lcom/android/internal/telephony/gsm/SmsMessage;

    #@36
    move-result-object v0

    #@37
    .restart local v0       #wrappedMessage:Lcom/android/internal/telephony/SmsMessageBase;
    goto :goto_13
.end method

.method public static createFromPdu([B)Landroid/telephony/SmsMessage;
    .registers 8
    .parameter "pdu"

    #@0
    .prologue
    const/4 v6, 0x2

    #@1
    .line 318
    const/4 v2, 0x0

    #@2
    .line 322
    .local v2, message:Landroid/telephony/SmsMessage;
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@5
    move-result-object v3

    #@6
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getCurrentPhoneType()I

    #@9
    move-result v0

    #@a
    .line 323
    .local v0, activePhone:I
    if-ne v6, v0, :cond_5d

    #@c
    const-string v1, "3gpp2"

    #@e
    .line 326
    .local v1, format:Ljava/lang/String;
    :goto_e
    const/4 v3, 0x1

    #@f
    const/4 v4, 0x0

    #@10
    const-string v5, "create_pdu_by_sms_format"

    #@12
    invoke-static {v4, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@15
    move-result v4

    #@16
    if-ne v3, v4, :cond_4a

    #@18
    .line 327
    new-instance v3, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v4, "createFromPdu(), format = "

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    #@26
    move-result-object v4

    #@27
    invoke-virtual {v4}, Landroid/telephony/SmsManager;->getImsSmsFormat()Ljava/lang/String;

    #@2a
    move-result-object v4

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v3

    #@33
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@36
    .line 328
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3}, Landroid/telephony/SmsManager;->getImsSmsFormat()Ljava/lang/String;

    #@3d
    move-result-object v1

    #@3e
    .line 329
    const-string v3, "unknown"

    #@40
    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@43
    move-result v3

    #@44
    if-eqz v3, :cond_4a

    #@46
    .line 330
    if-ne v6, v0, :cond_60

    #@48
    const-string v1, "3gpp2"

    #@4a
    .line 336
    :cond_4a
    :goto_4a
    invoke-static {p0, v1}, Landroid/telephony/SmsMessage;->createFromPdu([BLjava/lang/String;)Landroid/telephony/SmsMessage;

    #@4d
    move-result-object v2

    #@4e
    .line 338
    if-eqz v2, :cond_54

    #@50
    iget-object v3, v2, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@52
    if-nez v3, :cond_5c

    #@54
    .line 340
    :cond_54
    if-ne v6, v0, :cond_63

    #@56
    const-string v1, "3gpp"

    #@58
    .line 342
    :goto_58
    invoke-static {p0, v1}, Landroid/telephony/SmsMessage;->createFromPdu([BLjava/lang/String;)Landroid/telephony/SmsMessage;

    #@5b
    move-result-object v2

    #@5c
    .line 344
    :cond_5c
    return-object v2

    #@5d
    .line 323
    .end local v1           #format:Ljava/lang/String;
    :cond_5d
    const-string v1, "3gpp"

    #@5f
    goto :goto_e

    #@60
    .line 330
    .restart local v1       #format:Ljava/lang/String;
    :cond_60
    const-string v1, "3gpp"

    #@62
    goto :goto_4a

    #@63
    .line 340
    :cond_63
    const-string v1, "3gpp2"

    #@65
    goto :goto_58
.end method

.method public static createFromPdu([BLjava/lang/String;)Landroid/telephony/SmsMessage;
    .registers 6
    .parameter "pdu"
    .parameter "format"

    #@0
    .prologue
    .line 360
    const-string v1, "3gpp2"

    #@2
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_12

    #@8
    .line 361
    invoke-static {p0}, Lcom/android/internal/telephony/cdma/SmsMessage;->createFromPdu([B)Lcom/android/internal/telephony/cdma/SmsMessage;

    #@b
    move-result-object v0

    #@c
    .line 369
    .local v0, wrappedMessage:Lcom/android/internal/telephony/SmsMessageBase;
    :goto_c
    new-instance v1, Landroid/telephony/SmsMessage;

    #@e
    invoke-direct {v1, v0}, Landroid/telephony/SmsMessage;-><init>(Lcom/android/internal/telephony/SmsMessageBase;)V

    #@11
    .end local v0           #wrappedMessage:Lcom/android/internal/telephony/SmsMessageBase;
    :goto_11
    return-object v1

    #@12
    .line 362
    :cond_12
    const-string v1, "3gpp"

    #@14
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v1

    #@18
    if-eqz v1, :cond_1f

    #@1a
    .line 363
    invoke-static {p0}, Lcom/android/internal/telephony/gsm/SmsMessage;->createFromPdu([B)Lcom/android/internal/telephony/gsm/SmsMessage;

    #@1d
    move-result-object v0

    #@1e
    .restart local v0       #wrappedMessage:Lcom/android/internal/telephony/SmsMessageBase;
    goto :goto_c

    #@1f
    .line 365
    .end local v0           #wrappedMessage:Lcom/android/internal/telephony/SmsMessageBase;
    :cond_1f
    const-string v1, "SMS"

    #@21
    new-instance v2, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v3, "createFromPdu(): unsupported message format "

    #@28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v2

    #@34
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 366
    const/4 v1, 0x0

    #@38
    goto :goto_11
.end method

.method public static findKSC5601LimitIndex(Ljava/lang/String;II)I
    .registers 11
    .parameter "s"
    .parameter "start"
    .parameter "limit"

    #@0
    .prologue
    .line 2197
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@3
    move-result v4

    #@4
    .line 2201
    .local v4, size:I
    :try_start_4
    invoke-virtual {p0, p1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@7
    move-result-object v6

    #@8
    const-string v7, "euc-kr"

    #@a
    invoke-virtual {v6, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_d
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4 .. :try_end_d} :catch_22

    #@d
    move-result-object v5

    #@e
    .line 2207
    .local v5, text:[B
    array-length v1, v5

    #@f
    .line 2208
    .local v1, byte_size:I
    const/4 v3, 0x0

    #@10
    .local v3, i:I
    const/4 v0, 0x0

    #@11
    .local v0, accumulator:I
    :goto_11
    if-ge v0, v1, :cond_21

    #@13
    .line 2209
    aget-byte v6, v5, v0

    #@15
    and-int/lit16 v6, v6, 0xff

    #@17
    const/16 v7, 0xa0

    #@19
    if-lt v6, v7, :cond_24

    #@1b
    .line 2210
    add-int/lit8 v0, v0, 0x2

    #@1d
    .line 2226
    :goto_1d
    if-le v0, p2, :cond_58

    #@1f
    .line 2227
    add-int v4, p1, v3

    #@21
    .line 2230
    .end local v0           #accumulator:I
    .end local v1           #byte_size:I
    .end local v3           #i:I
    .end local v4           #size:I
    .end local v5           #text:[B
    :cond_21
    :goto_21
    return v4

    #@22
    .line 2202
    .restart local v4       #size:I
    :catch_22
    move-exception v2

    #@23
    .line 2204
    .local v2, e:Ljava/io/UnsupportedEncodingException;
    goto :goto_21

    #@24
    .line 2212
    .end local v2           #e:Ljava/io/UnsupportedEncodingException;
    .restart local v0       #accumulator:I
    .restart local v1       #byte_size:I
    .restart local v3       #i:I
    .restart local v5       #text:[B
    :cond_24
    aget-byte v6, v5, v0

    #@26
    and-int/lit16 v6, v6, 0xff

    #@28
    const/16 v7, 0x81

    #@2a
    if-lt v6, v7, :cond_55

    #@2c
    aget-byte v6, v5, v0

    #@2e
    and-int/lit16 v6, v6, 0xff

    #@30
    const/16 v7, 0xc6

    #@32
    if-gt v6, v7, :cond_55

    #@34
    .line 2213
    add-int/lit8 v6, v0, 0x1

    #@36
    if-ge v6, v1, :cond_52

    #@38
    .line 2214
    add-int/lit8 v6, v0, 0x1

    #@3a
    aget-byte v6, v5, v6

    #@3c
    and-int/lit16 v6, v6, 0xff

    #@3e
    const/16 v7, 0x41

    #@40
    if-lt v6, v7, :cond_4f

    #@42
    add-int/lit8 v6, v0, 0x1

    #@44
    aget-byte v6, v5, v6

    #@46
    and-int/lit16 v6, v6, 0xff

    #@48
    const/16 v7, 0xfe

    #@4a
    if-gt v6, v7, :cond_4f

    #@4c
    .line 2215
    add-int/lit8 v0, v0, 0x2

    #@4e
    goto :goto_1d

    #@4f
    .line 2217
    :cond_4f
    add-int/lit8 v0, v0, 0x1

    #@51
    goto :goto_1d

    #@52
    .line 2220
    :cond_52
    add-int/lit8 v0, v0, 0x1

    #@54
    goto :goto_1d

    #@55
    .line 2224
    :cond_55
    add-int/lit8 v0, v0, 0x1

    #@57
    goto :goto_1d

    #@58
    .line 2208
    :cond_58
    add-int/lit8 v3, v3, 0x1

    #@5a
    goto :goto_11
.end method

.method public static fragmentText(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 14
    .parameter "text"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    const/4 v11, 0x1

    #@2
    .line 626
    const-string v9, "persist.gsm.sms.forcegsm7"

    #@4
    const-string v10, "1"

    #@6
    invoke-static {v9, v10}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .line 627
    .local v0, encodingType:Ljava/lang/String;
    const-string v9, "0"

    #@c
    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v1

    #@10
    .line 631
    .local v1, forceGsm7bit:Z
    invoke-static {}, Landroid/telephony/SmsMessage;->useCdmaFormatForMoSms()Z

    #@13
    move-result v9

    #@14
    if-eqz v9, :cond_b1

    #@16
    const/4 v9, 0x0

    #@17
    invoke-static {p0, v9}, Lcom/android/internal/telephony/cdma/SmsMessage;->calculateLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@1a
    move-result-object v6

    #@1b
    .line 641
    .local v6, ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :goto_1b
    iget v9, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@1d
    sput v9, Landroid/telephony/SmsMessage;->mEncodingtype:I

    #@1f
    .line 649
    iget v9, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@21
    if-ne v9, v11, :cond_c5

    #@23
    .line 651
    iget v9, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageTable:I

    #@25
    if-eqz v9, :cond_b7

    #@27
    iget v9, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageShiftTable:I

    #@29
    if-eqz v9, :cond_b7

    #@2b
    .line 652
    const/4 v8, 0x7

    #@2c
    .line 659
    .local v8, udhLength:I
    :goto_2c
    iget v9, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@2e
    if-le v9, v11, :cond_32

    #@30
    .line 660
    add-int/lit8 v8, v8, 0x6

    #@32
    .line 663
    :cond_32
    if-eqz v8, :cond_36

    #@34
    .line 664
    add-int/lit8 v8, v8, 0x1

    #@36
    .line 667
    :cond_36
    rsub-int v2, v8, 0xa0

    #@38
    .line 676
    .end local v8           #udhLength:I
    .local v2, limit:I
    :goto_38
    const/4 v4, 0x0

    #@39
    .line 677
    .local v4, pos:I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@3c
    move-result v7

    #@3d
    .line 678
    .local v7, textLen:I
    new-instance v5, Ljava/util/ArrayList;

    #@3f
    iget v9, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@41
    invoke-direct {v5, v9}, Ljava/util/ArrayList;-><init>(I)V

    #@44
    .line 680
    .local v5, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v9, "allow_sending_empty_sms"

    #@46
    invoke-static {v12, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@49
    move-result v9

    #@4a
    if-eqz v9, :cond_57

    #@4c
    .line 681
    iget v9, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@4e
    if-ne v9, v11, :cond_57

    #@50
    if-nez v7, :cond_57

    #@52
    .line 682
    const-string v9, ""

    #@54
    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@57
    .line 687
    :cond_57
    :goto_57
    if-ge v4, v7, :cond_b0

    #@59
    .line 688
    const/4 v3, 0x0

    #@5a
    .line 689
    .local v3, nextPos:I
    iget v9, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@5c
    if-ne v9, v11, :cond_da

    #@5e
    .line 690
    invoke-static {}, Landroid/telephony/SmsMessage;->useCdmaFormatForMoSms()Z

    #@61
    move-result v9

    #@62
    if-eqz v9, :cond_d1

    #@64
    iget v9, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@66
    if-ne v9, v11, :cond_d1

    #@68
    .line 692
    sub-int v9, v7, v4

    #@6a
    invoke-static {v2, v9}, Ljava/lang/Math;->min(II)I

    #@6d
    move-result v9

    #@6e
    add-int v3, v4, v9

    #@70
    .line 711
    :cond_70
    :goto_70
    if-le v3, v4, :cond_74

    #@72
    if-le v3, v7, :cond_118

    #@74
    .line 712
    :cond_74
    const-string v9, "SMS"

    #@76
    new-instance v10, Ljava/lang/StringBuilder;

    #@78
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@7b
    const-string v11, "fragmentText failed ("

    #@7d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v10

    #@81
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@84
    move-result-object v10

    #@85
    const-string v11, " >= "

    #@87
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v10

    #@8b
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v10

    #@8f
    const-string v11, " or "

    #@91
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v10

    #@95
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@98
    move-result-object v10

    #@99
    const-string v11, " >= "

    #@9b
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v10

    #@9f
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v10

    #@a3
    const-string v11, ")"

    #@a5
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v10

    #@a9
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ac
    move-result-object v10

    #@ad
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b0
    .line 719
    .end local v3           #nextPos:I
    :cond_b0
    return-object v5

    #@b1
    .line 631
    .end local v2           #limit:I
    .end local v4           #pos:I
    .end local v5           #result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v6           #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    .end local v7           #textLen:I
    :cond_b1
    invoke-static {p0, v1}, Lcom/android/internal/telephony/gsm/SmsMessage;->calculateLGLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@b4
    move-result-object v6

    #@b5
    goto/16 :goto_1b

    #@b7
    .line 653
    .restart local v6       #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_b7
    iget v9, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageTable:I

    #@b9
    if-nez v9, :cond_bf

    #@bb
    iget v9, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageShiftTable:I

    #@bd
    if-eqz v9, :cond_c2

    #@bf
    .line 654
    :cond_bf
    const/4 v8, 0x4

    #@c0
    .restart local v8       #udhLength:I
    goto/16 :goto_2c

    #@c2
    .line 656
    .end local v8           #udhLength:I
    :cond_c2
    const/4 v8, 0x0

    #@c3
    .restart local v8       #udhLength:I
    goto/16 :goto_2c

    #@c5
    .line 669
    .end local v8           #udhLength:I
    :cond_c5
    iget v9, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@c7
    if-le v9, v11, :cond_cd

    #@c9
    .line 670
    const/16 v2, 0x86

    #@cb
    .restart local v2       #limit:I
    goto/16 :goto_38

    #@cd
    .line 672
    .end local v2           #limit:I
    :cond_cd
    const/16 v2, 0x8c

    #@cf
    .restart local v2       #limit:I
    goto/16 :goto_38

    #@d1
    .line 695
    .restart local v3       #nextPos:I
    .restart local v4       #pos:I
    .restart local v5       #result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v7       #textLen:I
    :cond_d1
    iget v9, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageTable:I

    #@d3
    iget v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageShiftTable:I

    #@d5
    invoke-static {p0, v4, v2, v9, v10}, Lcom/android/internal/telephony/GsmAlphabet;->findGsmSeptetLimitIndex(Ljava/lang/String;IIII)I

    #@d8
    move-result v3

    #@d9
    goto :goto_70

    #@da
    .line 699
    :cond_da
    div-int/lit8 v9, v2, 0x2

    #@dc
    sub-int v10, v7, v4

    #@de
    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    #@e1
    move-result v9

    #@e2
    add-int v3, v4, v9

    #@e4
    .line 702
    const-string v9, "support_emoji_in_concat_message"

    #@e6
    invoke-static {v12, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@e9
    move-result v9

    #@ea
    if-eqz v9, :cond_70

    #@ec
    .line 703
    iget v9, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@ee
    if-le v9, v11, :cond_70

    #@f0
    add-int/lit8 v9, v3, -0x1

    #@f2
    invoke-virtual {p0, v9}, Ljava/lang/String;->charAt(I)C

    #@f5
    move-result v9

    #@f6
    invoke-static {v9}, Ljava/lang/Character;->isHighSurrogate(C)Z

    #@f9
    move-result v9

    #@fa
    if-eqz v9, :cond_70

    #@fc
    .line 704
    new-instance v9, Ljava/lang/StringBuilder;

    #@fe
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@101
    const-string v10, "fragmentText(), isHighSurrogate, ted.msgCount = "

    #@103
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    move-result-object v9

    #@107
    iget v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@109
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v9

    #@10d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@110
    move-result-object v9

    #@111
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@114
    .line 705
    add-int/lit8 v3, v3, -0x1

    #@116
    goto/16 :goto_70

    #@118
    .line 716
    :cond_118
    invoke-virtual {p0, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@11b
    move-result-object v9

    #@11c
    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@11f
    .line 717
    move v4, v3

    #@120
    .line 718
    goto/16 :goto_57
.end method

.method public static fragmentText(Ljava/lang/String;I)Ljava/util/ArrayList;
    .registers 16
    .parameter "text"
    .parameter "emailLen"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v13, 0x1

    #@1
    const/4 v12, 0x0

    #@2
    .line 2265
    const/4 v1, 0x1

    #@3
    .line 2266
    .local v1, codeUnitSize:I
    const/4 v4, 0x0

    #@4
    .line 2267
    .local v4, pos:I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@7
    move-result v9

    #@8
    .line 2268
    .local v9, textLen:I
    const/4 v3, 0x0

    #@9
    .line 2269
    .local v3, mTotalSegment:I
    new-instance v0, Landroid/telephony/SprintSegmentProcess;

    #@b
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@e
    move-result v10

    #@f
    invoke-direct {v0, v10, p1}, Landroid/telephony/SprintSegmentProcess;-><init>(II)V

    #@12
    .line 2270
    .local v0, SegInfo:Landroid/telephony/SprintSegmentProcess;
    new-instance v5, Ljava/util/ArrayList;

    #@14
    invoke-virtual {v0}, Landroid/telephony/SprintSegmentProcess;->getMaxSegment()I

    #@17
    move-result v10

    #@18
    invoke-direct {v5, v10}, Ljava/util/ArrayList;-><init>(I)V

    #@1b
    .line 2272
    .local v5, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p0}, Ljava/lang/String;->toString()Ljava/lang/String;

    #@1e
    move-result-object v10

    #@1f
    invoke-static {v10}, Landroid/telephony/SprintSegmentProcess;->countAsciiSeptets(Ljava/lang/CharSequence;)I

    #@22
    move-result v10

    #@23
    const/4 v11, -0x1

    #@24
    if-eq v10, v11, :cond_37

    #@26
    .line 2273
    const/4 v1, 0x1

    #@27
    .line 2276
    :goto_27
    iput v1, v0, Landroid/telephony/SprintSegmentProcess;->codeUnitSize:I

    #@29
    .line 2278
    invoke-virtual {v0}, Landroid/telephony/SprintSegmentProcess;->needSprintSegmentProcess()Z

    #@2c
    move-result v10

    #@2d
    if-nez v10, :cond_39

    #@2f
    .line 2280
    invoke-virtual {p0, v12, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@32
    move-result-object v10

    #@33
    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@36
    .line 2322
    :cond_36
    return-object v5

    #@37
    .line 2275
    :cond_37
    const/4 v1, 0x3

    #@38
    goto :goto_27

    #@39
    .line 2284
    :cond_39
    invoke-virtual {v0, p0}, Landroid/telephony/SprintSegmentProcess;->calcNeedTotalSegment(Ljava/lang/String;)I

    #@3c
    move-result v3

    #@3d
    .line 2285
    if-le v3, v13, :cond_36

    #@3f
    invoke-virtual {v0}, Landroid/telephony/SprintSegmentProcess;->msegmentPosInfoSize()I

    #@42
    move-result v10

    #@43
    if-eqz v10, :cond_36

    #@45
    .line 2287
    new-instance v7, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    .line 2288
    .local v7, segmentedStr:Ljava/lang/StringBuilder;
    const/4 v8, 0x0

    #@4b
    .line 2289
    .local v8, startPos:I
    const/4 v2, 0x0

    #@4c
    .line 2290
    .local v2, endPos:I
    const/4 v6, 0x0

    #@4d
    .local v6, segIdx:I
    :goto_4d
    if-ge v6, v3, :cond_36

    #@4f
    invoke-virtual {v0}, Landroid/telephony/SprintSegmentProcess;->getMaxSegment()I

    #@52
    move-result v10

    #@53
    if-ge v6, v10, :cond_36

    #@55
    .line 2292
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    #@58
    move-result v10

    #@59
    invoke-virtual {v7, v12, v10}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    #@5c
    .line 2293
    const/4 v10, 0x0

    #@5d
    const-string v11, "remove_pagination_indicator"

    #@5f
    invoke-static {v10, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@62
    move-result v10

    #@63
    if-nez v10, :cond_72

    #@65
    .line 2295
    add-int/lit8 v10, v6, 0x1

    #@67
    invoke-virtual {v0, v10, v3}, Landroid/telephony/SprintSegmentProcess;->makeSegmentString(II)Ljava/lang/String;

    #@6a
    move-result-object v10

    #@6b
    invoke-virtual {v10}, Ljava/lang/String;->toString()Ljava/lang/String;

    #@6e
    move-result-object v10

    #@6f
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    .line 2298
    :cond_72
    invoke-virtual {v0, v6}, Landroid/telephony/SprintSegmentProcess;->msegmentPosInfoGet(I)I

    #@75
    move-result v8

    #@76
    .line 2300
    add-int/lit8 v10, v3, -0x1

    #@78
    if-ne v6, v10, :cond_a8

    #@7a
    .line 2301
    move v2, v9

    #@7b
    .line 2305
    :goto_7b
    iget v10, v0, Landroid/telephony/SprintSegmentProcess;->codeUnitSize:I

    #@7d
    if-ne v10, v13, :cond_af

    #@7f
    .line 2307
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    #@82
    move-result v10

    #@83
    sub-int v11, v2, v8

    #@85
    add-int/2addr v10, v11

    #@86
    invoke-virtual {v0}, Landroid/telephony/SprintSegmentProcess;->getMaxCharacterSegment()I

    #@89
    move-result v11

    #@8a
    if-le v10, v11, :cond_97

    #@8c
    .line 2308
    invoke-virtual {v0}, Landroid/telephony/SprintSegmentProcess;->getMaxCharacterSegment()I

    #@8f
    move-result v10

    #@90
    add-int/2addr v10, v8

    #@91
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    #@94
    move-result v11

    #@95
    sub-int v2, v10, v11

    #@97
    .line 2316
    :cond_97
    :goto_97
    invoke-virtual {p0, v8, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@9a
    move-result-object v10

    #@9b
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    .line 2318
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a1
    move-result-object v10

    #@a2
    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a5
    .line 2290
    add-int/lit8 v6, v6, 0x1

    #@a7
    goto :goto_4d

    #@a8
    .line 2303
    :cond_a8
    add-int/lit8 v10, v6, 0x1

    #@aa
    invoke-virtual {v0, v10}, Landroid/telephony/SprintSegmentProcess;->msegmentPosInfoGet(I)I

    #@ad
    move-result v2

    #@ae
    goto :goto_7b

    #@af
    .line 2312
    :cond_af
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    #@b2
    move-result v10

    #@b3
    sub-int v11, v2, v8

    #@b5
    add-int/2addr v10, v11

    #@b6
    invoke-virtual {v0}, Landroid/telephony/SprintSegmentProcess;->getMaxCharacterSegment()I

    #@b9
    move-result v11

    #@ba
    if-le v10, v11, :cond_97

    #@bc
    .line 2313
    invoke-virtual {v0}, Landroid/telephony/SprintSegmentProcess;->getMaxCharacterSegment()I

    #@bf
    move-result v10

    #@c0
    add-int/2addr v10, v8

    #@c1
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    #@c4
    move-result v11

    #@c5
    sub-int v2, v10, v11

    #@c7
    goto :goto_97
.end method

.method public static fragmentText(Ljava/lang/String;Z)Ljava/util/ArrayList;
    .registers 15
    .parameter "text"
    .parameter "isSimCopy"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v9, 0x1

    #@2
    .line 727
    const-string v11, "persist.gsm.sms.forcegsm7"

    #@4
    const-string v12, "1"

    #@6
    invoke-static {v11, v12}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .line 728
    .local v0, encodingType:Ljava/lang/String;
    const-string v11, "0"

    #@c
    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v11

    #@10
    if-eqz v11, :cond_b6

    #@12
    if-nez p1, :cond_b6

    #@14
    move v1, v9

    #@15
    .line 732
    .local v1, forceGsm7bit:Z
    :goto_15
    invoke-static {}, Landroid/telephony/SmsMessage;->useCdmaFormatForMoSms()Z

    #@18
    move-result v11

    #@19
    if-eqz v11, :cond_b9

    #@1b
    invoke-static {p0, v10}, Lcom/android/internal/telephony/cdma/SmsMessage;->calculateLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@1e
    move-result-object v6

    #@1f
    .line 742
    .local v6, ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :goto_1f
    iget v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@21
    sput v10, Landroid/telephony/SmsMessage;->mEncodingtype:I

    #@23
    .line 750
    iget v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@25
    if-ne v10, v9, :cond_cd

    #@27
    .line 752
    iget v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageTable:I

    #@29
    if-eqz v10, :cond_bf

    #@2b
    iget v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageShiftTable:I

    #@2d
    if-eqz v10, :cond_bf

    #@2f
    .line 753
    const/4 v8, 0x7

    #@30
    .line 760
    .local v8, udhLength:I
    :goto_30
    iget v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@32
    if-le v10, v9, :cond_36

    #@34
    .line 761
    add-int/lit8 v8, v8, 0x6

    #@36
    .line 764
    :cond_36
    if-eqz v8, :cond_3a

    #@38
    .line 765
    add-int/lit8 v8, v8, 0x1

    #@3a
    .line 768
    :cond_3a
    rsub-int v2, v8, 0xa0

    #@3c
    .line 777
    .end local v8           #udhLength:I
    .local v2, limit:I
    :goto_3c
    const/4 v4, 0x0

    #@3d
    .line 778
    .local v4, pos:I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@40
    move-result v7

    #@41
    .line 779
    .local v7, textLen:I
    new-instance v5, Ljava/util/ArrayList;

    #@43
    iget v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@45
    invoke-direct {v5, v10}, Ljava/util/ArrayList;-><init>(I)V

    #@48
    .line 782
    .local v5, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v10, 0x0

    #@49
    const-string v11, "allow_sending_empty_sms"

    #@4b
    invoke-static {v10, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@4e
    move-result v10

    #@4f
    if-eqz v10, :cond_5c

    #@51
    .line 783
    iget v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@53
    if-ne v10, v9, :cond_5c

    #@55
    if-nez v7, :cond_5c

    #@57
    .line 784
    const-string v10, ""

    #@59
    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5c
    .line 789
    :cond_5c
    :goto_5c
    if-ge v4, v7, :cond_b5

    #@5e
    .line 790
    const/4 v3, 0x0

    #@5f
    .line 791
    .local v3, nextPos:I
    iget v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@61
    if-ne v10, v9, :cond_e2

    #@63
    .line 792
    invoke-static {}, Landroid/telephony/SmsMessage;->useCdmaFormatForMoSms()Z

    #@66
    move-result v10

    #@67
    if-eqz v10, :cond_d9

    #@69
    iget v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@6b
    if-ne v10, v9, :cond_d9

    #@6d
    .line 794
    sub-int v10, v7, v4

    #@6f
    invoke-static {v2, v10}, Ljava/lang/Math;->min(II)I

    #@72
    move-result v10

    #@73
    add-int v3, v4, v10

    #@75
    .line 803
    :goto_75
    if-le v3, v4, :cond_79

    #@77
    if-le v3, v7, :cond_ed

    #@79
    .line 804
    :cond_79
    const-string v9, "SMS"

    #@7b
    new-instance v10, Ljava/lang/StringBuilder;

    #@7d
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@80
    const-string v11, "fragmentText failed ("

    #@82
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v10

    #@86
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@89
    move-result-object v10

    #@8a
    const-string v11, " >= "

    #@8c
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v10

    #@90
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@93
    move-result-object v10

    #@94
    const-string v11, " or "

    #@96
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v10

    #@9a
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v10

    #@9e
    const-string v11, " >= "

    #@a0
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v10

    #@a4
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v10

    #@a8
    const-string v11, ")"

    #@aa
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v10

    #@ae
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b1
    move-result-object v10

    #@b2
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b5
    .line 811
    .end local v3           #nextPos:I
    :cond_b5
    return-object v5

    #@b6
    .end local v1           #forceGsm7bit:Z
    .end local v2           #limit:I
    .end local v4           #pos:I
    .end local v5           #result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v6           #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    .end local v7           #textLen:I
    :cond_b6
    move v1, v10

    #@b7
    .line 728
    goto/16 :goto_15

    #@b9
    .line 732
    .restart local v1       #forceGsm7bit:Z
    :cond_b9
    invoke-static {p0, v1}, Lcom/android/internal/telephony/gsm/SmsMessage;->calculateLGLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@bc
    move-result-object v6

    #@bd
    goto/16 :goto_1f

    #@bf
    .line 754
    .restart local v6       #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_bf
    iget v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageTable:I

    #@c1
    if-nez v10, :cond_c7

    #@c3
    iget v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageShiftTable:I

    #@c5
    if-eqz v10, :cond_ca

    #@c7
    .line 755
    :cond_c7
    const/4 v8, 0x4

    #@c8
    .restart local v8       #udhLength:I
    goto/16 :goto_30

    #@ca
    .line 757
    .end local v8           #udhLength:I
    :cond_ca
    const/4 v8, 0x0

    #@cb
    .restart local v8       #udhLength:I
    goto/16 :goto_30

    #@cd
    .line 770
    .end local v8           #udhLength:I
    :cond_cd
    iget v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@cf
    if-le v10, v9, :cond_d5

    #@d1
    .line 771
    const/16 v2, 0x86

    #@d3
    .restart local v2       #limit:I
    goto/16 :goto_3c

    #@d5
    .line 773
    .end local v2           #limit:I
    :cond_d5
    const/16 v2, 0x8c

    #@d7
    .restart local v2       #limit:I
    goto/16 :goto_3c

    #@d9
    .line 797
    .restart local v3       #nextPos:I
    .restart local v4       #pos:I
    .restart local v5       #result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v7       #textLen:I
    :cond_d9
    iget v10, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageTable:I

    #@db
    iget v11, v6, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageShiftTable:I

    #@dd
    invoke-static {p0, v4, v2, v10, v11}, Lcom/android/internal/telephony/GsmAlphabet;->findGsmSeptetLimitIndex(Ljava/lang/String;IIII)I

    #@e0
    move-result v3

    #@e1
    goto :goto_75

    #@e2
    .line 801
    :cond_e2
    div-int/lit8 v10, v2, 0x2

    #@e4
    sub-int v11, v7, v4

    #@e6
    invoke-static {v10, v11}, Ljava/lang/Math;->min(II)I

    #@e9
    move-result v10

    #@ea
    add-int v3, v4, v10

    #@ec
    goto :goto_75

    #@ed
    .line 808
    :cond_ed
    invoke-virtual {p0, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@f0
    move-result-object v10

    #@f1
    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@f4
    .line 809
    move v4, v3

    #@f5
    .line 810
    goto/16 :goto_5c
.end method

.method public static fragmentTextEx(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 13
    .parameter "text"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v11, 0x2

    #@2
    const/4 v9, 0x0

    #@3
    const/4 v10, 0x1

    #@4
    .line 892
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@7
    move-result-object v8

    #@8
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    #@b
    move-result v0

    #@c
    .line 893
    .local v0, activePhone:I
    if-ne v11, v0, :cond_1b

    #@e
    invoke-static {p0, v9}, Lcom/android/internal/telephony/cdma/SmsMessage;->calculateLengthEx(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@11
    move-result-object v5

    #@12
    .line 897
    .local v5, ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :goto_12
    if-nez v5, :cond_20

    #@14
    .line 898
    const-string v8, "fragmentTextEx(), fragmentTextEx failed : ted is null"

    #@16
    invoke-static {v8}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@19
    move-object v4, v7

    #@1a
    .line 964
    :cond_1a
    :goto_1a
    return-object v4

    #@1b
    .line 893
    .end local v5           #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_1b
    invoke-static {p0, v9, v9}, Lcom/android/internal/telephony/gsm/SmsMessage;->calculateLength(Ljava/lang/CharSequence;ZZ)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@1e
    move-result-object v5

    #@1f
    goto :goto_12

    #@20
    .line 906
    .restart local v5       #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_20
    const-string v8, "KREncodingScheme"

    #@22
    invoke-static {v7, v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@25
    move-result v8

    #@26
    if-ne v8, v10, :cond_b3

    #@28
    .line 907
    invoke-static {}, Lcom/android/internal/telephony/gsm/SmsMessage;->isCountCharIndexInsteadOfSeptets()Z

    #@2b
    move-result v8

    #@2c
    if-eqz v8, :cond_97

    #@2e
    .line 908
    iget v8, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@30
    if-ne v8, v10, :cond_94

    #@32
    sget v1, Landroid/telephony/SmsMessage;->LIMIT_USER_DATA_SEPTETS:I

    #@34
    .line 929
    .local v1, limit:I
    :goto_34
    const/4 v3, 0x0

    #@35
    .line 930
    .local v3, pos:I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@38
    move-result v6

    #@39
    .line 931
    .local v6, textLen:I
    new-instance v4, Ljava/util/ArrayList;

    #@3b
    iget v8, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@3d
    invoke-direct {v4, v8}, Ljava/util/ArrayList;-><init>(I)V

    #@40
    .line 932
    .local v4, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_40
    if-ge v3, v6, :cond_1a

    #@42
    .line 933
    const/4 v2, 0x0

    #@43
    .line 934
    .local v2, nextPos:I
    iget v8, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@45
    if-ne v8, v10, :cond_d6

    #@47
    .line 935
    if-ne v0, v11, :cond_cd

    #@49
    iget v8, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@4b
    if-ne v8, v10, :cond_cd

    #@4d
    .line 938
    sub-int v8, v6, v3

    #@4f
    invoke-static {v1, v8}, Ljava/lang/Math;->min(II)I

    #@52
    move-result v8

    #@53
    add-int v2, v3, v8

    #@55
    .line 956
    :goto_55
    if-le v2, v3, :cond_59

    #@57
    if-le v2, v6, :cond_100

    #@59
    .line 957
    :cond_59
    new-instance v7, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v8, "fragmentTextEx(), fragmentText failed ("

    #@60
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v7

    #@64
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@67
    move-result-object v7

    #@68
    const-string v8, " >= "

    #@6a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v7

    #@6e
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@71
    move-result-object v7

    #@72
    const-string v8, " or "

    #@74
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v7

    #@78
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v7

    #@7c
    const-string v8, " >= "

    #@7e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v7

    #@82
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@85
    move-result-object v7

    #@86
    const-string v8, ")"

    #@88
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v7

    #@8c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f
    move-result-object v7

    #@90
    invoke-static {v7}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@93
    goto :goto_1a

    #@94
    .line 908
    .end local v1           #limit:I
    .end local v2           #nextPos:I
    .end local v3           #pos:I
    .end local v4           #result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v6           #textLen:I
    :cond_94
    sget v1, Landroid/telephony/SmsMessage;->MAX_USER_DATA_BYTES_EX:I

    #@96
    goto :goto_34

    #@97
    .line 911
    :cond_97
    iget v8, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@99
    if-le v8, v10, :cond_a9

    #@9b
    .line 912
    iget v8, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@9d
    if-ne v8, v10, :cond_a4

    #@9f
    sget v8, Landroid/telephony/SmsMessage;->MAX_USER_DATA_SEPTETS_EX:I

    #@a1
    add-int/lit8 v1, v8, -0x7

    #@a3
    .restart local v1       #limit:I
    :goto_a3
    goto :goto_34

    #@a4
    .end local v1           #limit:I
    :cond_a4
    sget v8, Landroid/telephony/SmsMessage;->MAX_USER_DATA_BYTES_EX:I

    #@a6
    add-int/lit8 v1, v8, -0x6

    #@a8
    goto :goto_a3

    #@a9
    .line 915
    :cond_a9
    iget v8, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@ab
    if-ne v8, v10, :cond_b0

    #@ad
    sget v1, Landroid/telephony/SmsMessage;->MAX_USER_DATA_SEPTETS_EX:I

    #@af
    .restart local v1       #limit:I
    :goto_af
    goto :goto_34

    #@b0
    .end local v1           #limit:I
    :cond_b0
    sget v1, Landroid/telephony/SmsMessage;->MAX_USER_DATA_BYTES_EX:I

    #@b2
    goto :goto_af

    #@b3
    .line 920
    :cond_b3
    iget v8, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@b5
    if-le v8, v10, :cond_c2

    #@b7
    .line 921
    iget v8, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@b9
    if-ne v8, v10, :cond_bf

    #@bb
    const/16 v1, 0x99

    #@bd
    .restart local v1       #limit:I
    :goto_bd
    goto/16 :goto_34

    #@bf
    .end local v1           #limit:I
    :cond_bf
    const/16 v1, 0x86

    #@c1
    goto :goto_bd

    #@c2
    .line 924
    :cond_c2
    iget v8, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@c4
    if-ne v8, v10, :cond_ca

    #@c6
    const/16 v1, 0xa0

    #@c8
    .restart local v1       #limit:I
    :goto_c8
    goto/16 :goto_34

    #@ca
    .end local v1           #limit:I
    :cond_ca
    const/16 v1, 0x8c

    #@cc
    goto :goto_c8

    #@cd
    .line 942
    .restart local v1       #limit:I
    .restart local v2       #nextPos:I
    .restart local v3       #pos:I
    .restart local v4       #result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v6       #textLen:I
    :cond_cd
    iget v8, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageTable:I

    #@cf
    iget v9, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageShiftTable:I

    #@d1
    invoke-static {p0, v3, v1, v8, v9}, Lcom/android/internal/telephony/GsmAlphabet;->findGsmSeptetLimitIndex(Ljava/lang/String;IIII)I

    #@d4
    move-result v2

    #@d5
    goto :goto_55

    #@d6
    .line 946
    :cond_d6
    const-string v8, "KSC5601Encoding"

    #@d8
    invoke-static {v7, v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@db
    move-result v8

    #@dc
    if-ne v8, v10, :cond_f4

    #@de
    .line 947
    iget v8, v5, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@e0
    if-ne v8, v11, :cond_e8

    #@e2
    .line 948
    invoke-static {p0, v3, v1}, Landroid/telephony/SmsMessage;->findKSC5601LimitIndex(Ljava/lang/String;II)I

    #@e5
    move-result v2

    #@e6
    goto/16 :goto_55

    #@e8
    .line 950
    :cond_e8
    div-int/lit8 v8, v1, 0x2

    #@ea
    sub-int v9, v6, v3

    #@ec
    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    #@ef
    move-result v8

    #@f0
    add-int v2, v3, v8

    #@f2
    goto/16 :goto_55

    #@f4
    .line 953
    :cond_f4
    div-int/lit8 v8, v1, 0x2

    #@f6
    sub-int v9, v6, v3

    #@f8
    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    #@fb
    move-result v8

    #@fc
    add-int v2, v3, v8

    #@fe
    goto/16 :goto_55

    #@100
    .line 961
    :cond_100
    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@103
    move-result-object v8

    #@104
    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@107
    .line 962
    move v3, v2

    #@108
    .line 963
    goto/16 :goto_40
.end method

.method public static fragmentTextEx(Ljava/lang/String;I)Ljava/util/ArrayList;
    .registers 4
    .parameter "text"
    .parameter "dataCodingScheme"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 978
    const/4 v0, 0x0

    #@1
    const-string v1, "replyAddress"

    #@3
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@6
    move-result v0

    #@7
    invoke-static {p0, p1, v0}, Landroid/telephony/SmsMessage;->fragmentTextEx(Ljava/lang/String;IZ)Ljava/util/ArrayList;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public static fragmentTextEx(Ljava/lang/String;IZ)Ljava/util/ArrayList;
    .registers 16
    .parameter "text"
    .parameter "dataCodingScheme"
    .parameter "bReplyAddress"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IZ)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v12, 0x2

    #@2
    const/4 v11, 0x1

    #@3
    .line 985
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@6
    move-result-object v9

    #@7
    invoke-virtual {v9}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    #@a
    move-result v0

    #@b
    .line 986
    .local v0, activePhone:I
    const/4 v7, 0x0

    #@c
    .line 989
    .local v7, ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    if-ne v12, v0, :cond_1e

    #@e
    const-string v9, "save_usim_3gpp_in_cdma"

    #@10
    invoke-static {v6, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@13
    move-result v9

    #@14
    if-nez v9, :cond_1e

    #@16
    .line 992
    new-instance v9, Ljava/lang/RuntimeException;

    #@18
    const-string v10, "Not supported!"

    #@1a
    invoke-direct {v9, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v9

    #@1e
    .line 994
    :cond_1e
    if-eqz p2, :cond_27

    #@20
    .line 995
    invoke-static {p0, p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->calculateLengthHeaderReplyaddressEx(Ljava/lang/CharSequence;I)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@23
    move-result-object v7

    #@24
    .line 1001
    :goto_24
    if-nez v7, :cond_2c

    #@26
    .line 1056
    :cond_26
    return-object v6

    #@27
    .line 997
    :cond_27
    invoke-static {p0, p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->calculateLength(Ljava/lang/CharSequence;I)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@2a
    move-result-object v7

    #@2b
    goto :goto_24

    #@2c
    .line 1005
    :cond_2c
    new-instance v9, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v10, "fragmentTextEx(), "

    #@33
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v9

    #@37
    invoke-virtual {v7}, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->toString()Ljava/lang/String;

    #@3a
    move-result-object v10

    #@3b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v9

    #@3f
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v9

    #@43
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@46
    .line 1009
    iget v9, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@48
    packed-switch v9, :pswitch_data_15a

    #@4b
    .line 1020
    if-eqz p2, :cond_12f

    #@4d
    .line 1021
    iget v9, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@4f
    if-le v9, v11, :cond_12b

    #@51
    const/16 v2, 0x78

    #@53
    .line 1028
    .local v2, limit:I
    :goto_53
    new-instance v9, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v10, "fragmentTextEx(), ["

    #@5a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v9

    #@5e
    iget v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@60
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@63
    move-result-object v9

    #@64
    const-string v10, "]"

    #@66
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v9

    #@6a
    const-string v10, " limit: "

    #@6c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v9

    #@70
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@73
    move-result-object v9

    #@74
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v9

    #@78
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@7b
    .line 1030
    const/4 v5, 0x0

    #@7c
    .line 1031
    .local v5, pos:I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@7f
    move-result v8

    #@80
    .line 1032
    .local v8, textLen:I
    new-instance v6, Ljava/util/ArrayList;

    #@82
    iget v9, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@84
    invoke-direct {v6, v9}, Ljava/util/ArrayList;-><init>(I)V

    #@87
    .line 1033
    .local v6, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_87
    if-ge v5, v8, :cond_d4

    #@89
    .line 1034
    const/4 v3, 0x0

    #@8a
    .line 1035
    .local v3, nextPos:I
    iget v9, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@8c
    if-ne v9, v11, :cond_13a

    #@8e
    .line 1037
    iget v9, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageTable:I

    #@90
    iget v10, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageShiftTable:I

    #@92
    invoke-static {p0, v5, v2, v9, v10}, Lcom/android/internal/telephony/GsmAlphabet;->findGsmSeptetLimitIndex(Ljava/lang/String;IIII)I

    #@95
    move-result v3

    #@96
    .line 1043
    :goto_96
    if-le v3, v5, :cond_9a

    #@98
    if-le v3, v8, :cond_150

    #@9a
    .line 1044
    :cond_9a
    new-instance v9, Ljava/lang/StringBuilder;

    #@9c
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@9f
    const-string v10, "fragmentTextEx(), fragmentText failed ("

    #@a1
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v9

    #@a5
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v9

    #@a9
    const-string v10, " >= "

    #@ab
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v9

    #@af
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v9

    #@b3
    const-string v10, " or "

    #@b5
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v9

    #@b9
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v9

    #@bd
    const-string v10, " >= "

    #@bf
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v9

    #@c3
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v9

    #@c7
    const-string v10, ")"

    #@c9
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v9

    #@cd
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d0
    move-result-object v9

    #@d1
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@d4
    .line 1052
    .end local v3           #nextPos:I
    :cond_d4
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@d7
    move-result-object v1

    #@d8
    .local v1, i$:Ljava/util/Iterator;
    :goto_d8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@db
    move-result v9

    #@dc
    if-eqz v9, :cond_26

    #@de
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@e1
    move-result-object v4

    #@e2
    check-cast v4, Ljava/lang/String;

    #@e4
    .line 1053
    .local v4, part:Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    #@e6
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@e9
    const-string v10, "fragmentTextEx(), PART["

    #@eb
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v9

    #@ef
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v9

    #@f3
    const-string v10, ","

    #@f5
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v9

    #@f9
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@fc
    move-result v10

    #@fd
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@100
    move-result-object v9

    #@101
    const-string v10, "]: "

    #@103
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    move-result-object v9

    #@107
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10a
    move-result-object v9

    #@10b
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10e
    move-result-object v9

    #@10f
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@112
    goto :goto_d8

    #@113
    .line 1011
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #limit:I
    .end local v4           #part:Ljava/lang/String;
    .end local v5           #pos:I
    .end local v6           #result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v8           #textLen:I
    :pswitch_113
    if-eqz p2, :cond_120

    #@115
    .line 1012
    iget v9, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@117
    if-le v9, v11, :cond_11d

    #@119
    const/16 v2, 0x89

    #@11b
    .restart local v2       #limit:I
    :goto_11b
    goto/16 :goto_53

    #@11d
    .end local v2           #limit:I
    :cond_11d
    const/16 v2, 0x8e

    #@11f
    goto :goto_11b

    #@120
    .line 1014
    :cond_120
    iget v9, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@122
    if-le v9, v11, :cond_128

    #@124
    const/16 v2, 0x99

    #@126
    .line 1016
    .restart local v2       #limit:I
    :goto_126
    goto/16 :goto_53

    #@128
    .line 1014
    .end local v2           #limit:I
    :cond_128
    const/16 v2, 0xa0

    #@12a
    goto :goto_126

    #@12b
    .line 1021
    :cond_12b
    const/16 v2, 0x7d

    #@12d
    goto/16 :goto_53

    #@12f
    .line 1023
    :cond_12f
    iget v9, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@131
    if-le v9, v11, :cond_137

    #@133
    const/16 v2, 0x86

    #@135
    .restart local v2       #limit:I
    :goto_135
    goto/16 :goto_53

    #@137
    .end local v2           #limit:I
    :cond_137
    const/16 v2, 0x8c

    #@139
    goto :goto_135

    #@13a
    .line 1038
    .restart local v2       #limit:I
    .restart local v3       #nextPos:I
    .restart local v5       #pos:I
    .restart local v6       #result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v8       #textLen:I
    :cond_13a
    iget v9, v7, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@13c
    if-ne v9, v12, :cond_144

    #@13e
    .line 1039
    invoke-static {p0, v5, v2}, Landroid/telephony/SmsMessage;->findKSC5601LimitIndex(Ljava/lang/String;II)I

    #@141
    move-result v3

    #@142
    goto/16 :goto_96

    #@144
    .line 1041
    :cond_144
    div-int/lit8 v9, v2, 0x2

    #@146
    sub-int v10, v8, v5

    #@148
    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    #@14b
    move-result v9

    #@14c
    add-int v3, v5, v9

    #@14e
    goto/16 :goto_96

    #@150
    .line 1048
    :cond_150
    invoke-virtual {p0, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@153
    move-result-object v9

    #@154
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@157
    .line 1049
    move v5, v3

    #@158
    .line 1050
    goto/16 :goto_87

    #@15a
    .line 1009
    :pswitch_data_15a
    .packed-switch 0x1
        :pswitch_113
    .end packed-switch
.end method

.method public static getDeliverPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ[BI)Landroid/telephony/SmsMessage$DeliverPdu;
    .registers 17
    .parameter "scAddress"
    .parameter "originatorAddress"
    .parameter "message"
    .parameter "statusReportRequested"
    .parameter "time"
    .parameter "header"
    .parameter "encodingtype"

    #@0
    .prologue
    .line 1179
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    #@7
    move-result v7

    #@8
    .line 1183
    .local v7, activePhone:I
    const/4 v0, 0x2

    #@9
    if-ne v0, v7, :cond_26

    #@b
    const/4 v0, 0x0

    #@c
    const-string v1, "save_usim_3gpp_in_cdma"

    #@e
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@11
    move-result v0

    #@12
    if-nez v0, :cond_26

    #@14
    .line 1186
    const/4 v4, 0x0

    #@15
    move-object v0, p0

    #@16
    move-object v1, p1

    #@17
    move-object v2, p2

    #@18
    move v3, p3

    #@19
    move-wide v5, p4

    #@1a
    invoke-static/range {v0 .. v6}, Lcom/android/internal/telephony/cdma/SmsMessage;->ruimGetDeliverPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/android/internal/telephony/SmsHeader;J)Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;

    #@1d
    move-result-object v8

    #@1e
    .line 1192
    .local v8, spb:Lcom/android/internal/telephony/SmsMessageBase$DeliverPduBase;
    :goto_1e
    if-nez v8, :cond_2b

    #@20
    new-instance v0, Landroid/telephony/SmsMessage$DeliverPdu;

    #@22
    invoke-direct {v0}, Landroid/telephony/SmsMessage$DeliverPdu;-><init>()V

    #@25
    :goto_25
    return-object v0

    #@26
    .line 1189
    .end local v8           #spb:Lcom/android/internal/telephony/SmsMessageBase$DeliverPduBase;
    :cond_26
    invoke-static/range {p0 .. p7}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDeliverPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ[BI)Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;

    #@29
    move-result-object v8

    #@2a
    .restart local v8       #spb:Lcom/android/internal/telephony/SmsMessageBase$DeliverPduBase;
    goto :goto_1e

    #@2b
    .line 1192
    :cond_2b
    new-instance v0, Landroid/telephony/SmsMessage$DeliverPdu;

    #@2d
    invoke-direct {v0, v8}, Landroid/telephony/SmsMessage$DeliverPdu;-><init>(Lcom/android/internal/telephony/SmsMessageBase$DeliverPduBase;)V

    #@30
    goto :goto_25
.end method

.method public static getDeliverPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ[BII)Landroid/telephony/SmsMessage$DeliverPdu;
    .registers 20
    .parameter "scAddress"
    .parameter "originatorAddress"
    .parameter "message"
    .parameter "statusReportRequested"
    .parameter "time"
    .parameter "header"
    .parameter "encodingtype"
    .parameter "smsformat"

    #@0
    .prologue
    .line 1408
    const/4 v9, 0x1

    #@1
    .line 1409
    .local v9, SMS_FORMAT_USIM:I
    const/4 v8, 0x2

    #@2
    .line 1411
    .local v8, SMS_FORMAT_CSIM:I
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getDeliverPdu(), scAddress = "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@18
    .line 1412
    new-instance v1, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v2, "getDeliverPdu(), originatorAddress = "

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@2e
    .line 1413
    new-instance v1, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v2, "getDeliverPdu(), message = "

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v1

    #@39
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v1

    #@41
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@44
    .line 1414
    new-instance v1, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v2, "getDeliverPdu(), smsformat = "

    #@4b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v1

    #@4f
    move/from16 v0, p8

    #@51
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@54
    move-result-object v1

    #@55
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v1

    #@59
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5c
    .line 1415
    const/4 v1, 0x2

    #@5d
    move/from16 v0, p8

    #@5f
    if-ne v1, v0, :cond_83

    #@61
    .line 1416
    if-eqz p6, :cond_78

    #@63
    .line 1417
    invoke-static/range {p6 .. p6}, Lcom/android/internal/telephony/SmsHeader;->fromByteArray([B)Lcom/android/internal/telephony/SmsHeader;

    #@66
    move-result-object v5

    #@67
    move-object v1, p0

    #@68
    move-object v2, p1

    #@69
    move-object v3, p2

    #@6a
    move v4, p3

    #@6b
    move-wide v6, p4

    #@6c
    invoke-static/range {v1 .. v7}, Lcom/android/internal/telephony/cdma/SmsMessage;->ruimGetDeliverPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/android/internal/telephony/SmsHeader;J)Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;

    #@6f
    move-result-object v10

    #@70
    .line 1427
    .local v10, spb:Lcom/android/internal/telephony/SmsMessageBase$DeliverPduBase;
    :goto_70
    if-nez v10, :cond_88

    #@72
    new-instance v1, Landroid/telephony/SmsMessage$DeliverPdu;

    #@74
    invoke-direct {v1}, Landroid/telephony/SmsMessage$DeliverPdu;-><init>()V

    #@77
    :goto_77
    return-object v1

    #@78
    .line 1420
    .end local v10           #spb:Lcom/android/internal/telephony/SmsMessageBase$DeliverPduBase;
    :cond_78
    const/4 v5, 0x0

    #@79
    move-object v1, p0

    #@7a
    move-object v2, p1

    #@7b
    move-object v3, p2

    #@7c
    move v4, p3

    #@7d
    move-wide v6, p4

    #@7e
    invoke-static/range {v1 .. v7}, Lcom/android/internal/telephony/cdma/SmsMessage;->ruimGetDeliverPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/android/internal/telephony/SmsHeader;J)Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;

    #@81
    move-result-object v10

    #@82
    .restart local v10       #spb:Lcom/android/internal/telephony/SmsMessageBase$DeliverPduBase;
    goto :goto_70

    #@83
    .line 1424
    .end local v10           #spb:Lcom/android/internal/telephony/SmsMessageBase$DeliverPduBase;
    :cond_83
    invoke-static/range {p0 .. p7}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDeliverPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ[BI)Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;

    #@86
    move-result-object v10

    #@87
    .restart local v10       #spb:Lcom/android/internal/telephony/SmsMessageBase$DeliverPduBase;
    goto :goto_70

    #@88
    .line 1427
    :cond_88
    new-instance v1, Landroid/telephony/SmsMessage$DeliverPdu;

    #@8a
    invoke-direct {v1, v10}, Landroid/telephony/SmsMessage$DeliverPdu;-><init>(Lcom/android/internal/telephony/SmsMessageBase$DeliverPduBase;)V

    #@8d
    goto :goto_77
.end method

.method public static getEncodingType()I
    .registers 1

    #@0
    .prologue
    .line 1518
    const/4 v0, -0x1

    #@1
    .line 1519
    .local v0, tmpencodingg:I
    sget v0, Landroid/telephony/SmsMessage;->mEncodingtype:I

    #@3
    .line 1520
    return v0
.end method

.method public static getMessagePrivacyInd()I
    .registers 1

    #@0
    .prologue
    .line 2091
    invoke-static {}, Lcom/android/internal/telephony/cdma/SmsMessage;->getMessagePrivacyInd()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static getSmsIsRoaming()Z
    .registers 2

    #@0
    .prologue
    .line 2344
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "getSmsIsRoaming(), Return value = "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-static {}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSmsIsRoaming()Z

    #@e
    move-result v1

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1a
    .line 2346
    invoke-static {}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSmsIsRoaming()Z

    #@1d
    move-result v0

    #@1e
    return v0
.end method

.method public static getSmsPriority()I
    .registers 1

    #@0
    .prologue
    .line 2075
    invoke-static {}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSmsPriority()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static getStaticDeliverPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJII[B)Landroid/telephony/SmsMessage$DeliverPdu;
    .registers 15
    .parameter "scAddress"
    .parameter "originatorAddress"
    .parameter "message"
    .parameter "statusReportRequested"
    .parameter "time"
    .parameter "protocolId"
    .parameter "dataCodingScheme"
    .parameter "header"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1483
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@4
    move-result-object v4

    #@5
    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    #@8
    move-result v0

    #@9
    .line 1486
    .local v0, activePhone:I
    const/4 v4, 0x2

    #@a
    if-ne v4, v0, :cond_1c

    #@c
    const-string v4, "save_usim_3gpp_in_cdma"

    #@e
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@11
    move-result v4

    #@12
    if-nez v4, :cond_1c

    #@14
    .line 1489
    new-instance v3, Ljava/lang/RuntimeException;

    #@16
    const-string v4, "Not supported!"

    #@18
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v3

    #@1c
    .line 1491
    :cond_1c
    invoke-static/range {p0 .. p8}, Lcom/android/internal/telephony/gsm/SmsMessage;->getStaticDeliverPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJII[B)Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;

    #@1f
    move-result-object v1

    #@20
    .line 1495
    .local v1, dpb:Lcom/android/internal/telephony/SmsMessageBase$DeliverPduBase;
    if-nez v1, :cond_28

    #@22
    .line 1496
    const-string v4, "getStaticDeliverPdu(), dpb is null"

    #@24
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@27
    .line 1504
    :goto_27
    return-object v3

    #@28
    .line 1501
    :cond_28
    :try_start_28
    new-instance v4, Landroid/telephony/SmsMessage$DeliverPdu;

    #@2a
    invoke-direct {v4, v1}, Landroid/telephony/SmsMessage$DeliverPdu;-><init>(Lcom/android/internal/telephony/SmsMessageBase$DeliverPduBase;)V
    :try_end_2d
    .catch Ljava/lang/RuntimeException; {:try_start_28 .. :try_end_2d} :catch_2f

    #@2d
    move-object v3, v4

    #@2e
    goto :goto_27

    #@2f
    .line 1502
    :catch_2f
    move-exception v2

    #@30
    .line 1503
    .local v2, e:Ljava/lang/RuntimeException;
    new-instance v4, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v5, "getStaticDeliverPdu(), "

    #@37
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v4

    #@3f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v4

    #@43
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@46
    goto :goto_27
.end method

.method public static getStaticSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZII[B)Landroid/telephony/SmsMessage$SubmitPdu;
    .registers 17
    .parameter "scAddress"
    .parameter "destinationAddress"
    .parameter "message"
    .parameter "statusReportRequested"
    .parameter "protocolId"
    .parameter "dataCodingSchene"
    .parameter "header"

    #@0
    .prologue
    .line 1449
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    #@7
    move-result v7

    #@8
    .line 1453
    .local v7, activePhone:I
    const/4 v0, 0x2

    #@9
    if-ne v0, v7, :cond_1c

    #@b
    const/4 v0, 0x0

    #@c
    const-string v1, "save_usim_3gpp_in_cdma"

    #@e
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@11
    move-result v0

    #@12
    if-nez v0, :cond_1c

    #@14
    .line 1456
    new-instance v0, Ljava/lang/RuntimeException;

    #@16
    const-string v1, "Not supported!"

    #@18
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    :cond_1c
    move-object v0, p0

    #@1d
    move-object v1, p1

    #@1e
    move-object v2, p2

    #@1f
    move v3, p3

    #@20
    move-object/from16 v4, p6

    #@22
    move v5, p5

    #@23
    move v6, p4

    #@24
    .line 1458
    invoke-static/range {v0 .. v6}, Lcom/android/internal/telephony/gsm/SmsMessage;->getStaticSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BII)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@27
    move-result-object v9

    #@28
    .line 1465
    .local v9, spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    if-nez v9, :cond_31

    #@2a
    .line 1466
    :try_start_2a
    const-string v0, "getStaticSubmitPdu(), spb null! encoding == ENCODING_UNKNOWN "

    #@2c
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@2f
    .line 1467
    const/4 v0, 0x0

    #@30
    .line 1472
    :goto_30
    return-object v0

    #@31
    .line 1469
    :cond_31
    new-instance v0, Landroid/telephony/SmsMessage$SubmitPdu;

    #@33
    invoke-direct {v0, v9}, Landroid/telephony/SmsMessage$SubmitPdu;-><init>(Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)V
    :try_end_36
    .catch Ljava/lang/RuntimeException; {:try_start_2a .. :try_end_36} :catch_37

    #@36
    goto :goto_30

    #@37
    .line 1470
    :catch_37
    move-exception v8

    #@38
    .line 1471
    .local v8, e:Ljava/lang/RuntimeException;
    new-instance v0, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v1, "getStaticSubmitPdu(), "

    #@3f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v0

    #@43
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v0

    #@4b
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@4e
    .line 1472
    const/4 v0, 0x0

    #@4f
    goto :goto_30
.end method

.method public static getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/telephony/SmsMessage$SubmitPdu;
    .registers 6
    .parameter "scAddress"
    .parameter "destinationAddress"
    .parameter "message"
    .parameter "statusReportRequested"

    #@0
    .prologue
    .line 1209
    invoke-static {}, Landroid/telephony/SmsMessage;->useCdmaFormatForMoSms()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_11

    #@6
    .line 1210
    const/4 v1, 0x0

    #@7
    invoke-static {p0, p1, p2, p3, v1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/android/internal/telephony/SmsHeader;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@a
    move-result-object v0

    #@b
    .line 1217
    .local v0, spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    :goto_b
    new-instance v1, Landroid/telephony/SmsMessage$SubmitPdu;

    #@d
    invoke-direct {v1, v0}, Landroid/telephony/SmsMessage$SubmitPdu;-><init>(Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)V

    #@10
    return-object v1

    #@11
    .line 1213
    .end local v0           #spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    :cond_11
    invoke-static {p0, p1, p2, p3}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@14
    move-result-object v0

    #@15
    .restart local v0       #spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    goto :goto_b
.end method

.method public static getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/telephony/SmsMessage$SubmitPdu;
    .registers 12
    .parameter "scAddress"
    .parameter "destinationAddress"
    .parameter "message"
    .parameter "statusReportRequested"
    .parameter "cbAddress"

    #@0
    .prologue
    .line 1296
    invoke-static {}, Landroid/telephony/SmsMessage;->useCdmaFormatForMoSms()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_18

    #@6
    .line 1297
    const/4 v4, 0x0

    #@7
    move-object v0, p0

    #@8
    move-object v1, p1

    #@9
    move-object v2, p2

    #@a
    move v3, p3

    #@b
    move-object v5, p4

    #@c
    invoke-static/range {v0 .. v5}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/android/internal/telephony/SmsHeader;Ljava/lang/String;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@f
    move-result-object v6

    #@10
    .line 1305
    .local v6, spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    :goto_10
    if-nez v6, :cond_1d

    #@12
    new-instance v0, Landroid/telephony/SmsMessage$SubmitPdu;

    #@14
    invoke-direct {v0}, Landroid/telephony/SmsMessage$SubmitPdu;-><init>()V

    #@17
    :goto_17
    return-object v0

    #@18
    .line 1300
    .end local v6           #spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    :cond_18
    invoke-static {p0, p1, p2, p3}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@1b
    move-result-object v6

    #@1c
    .restart local v6       #spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    goto :goto_10

    #@1d
    .line 1305
    :cond_1d
    new-instance v0, Landroid/telephony/SmsMessage$SubmitPdu;

    #@1f
    invoke-direct {v0, v6}, Landroid/telephony/SmsMessage$SubmitPdu;-><init>(Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)V

    #@22
    goto :goto_17
.end method

.method public static getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[B)Landroid/telephony/SmsMessage$SubmitPdu;
    .registers 7
    .parameter "scAddress"
    .parameter "destinationAddress"
    .parameter "message"
    .parameter "statusReportRequested"
    .parameter "header"

    #@0
    .prologue
    .line 1140
    invoke-static {}, Landroid/telephony/SmsMessage;->useCdmaFormatForMoSms()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_14

    #@6
    .line 1141
    invoke-static {p4}, Lcom/android/internal/telephony/SmsHeader;->fromByteArray([B)Lcom/android/internal/telephony/SmsHeader;

    #@9
    move-result-object v1

    #@a
    invoke-static {p0, p1, p2, p3, v1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/android/internal/telephony/SmsHeader;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@d
    move-result-object v0

    #@e
    .line 1149
    .local v0, spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    :goto_e
    new-instance v1, Landroid/telephony/SmsMessage$SubmitPdu;

    #@10
    invoke-direct {v1, v0}, Landroid/telephony/SmsMessage$SubmitPdu;-><init>(Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)V

    #@13
    return-object v1

    #@14
    .line 1145
    .end local v0           #spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    :cond_14
    invoke-static {p0, p1, p2, p3, p4}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[B)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@17
    move-result-object v0

    #@18
    .restart local v0       #spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    goto :goto_e
.end method

.method public static getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BI)Landroid/telephony/SmsMessage$SubmitPdu;
    .registers 16
    .parameter "scAddress"
    .parameter "destinationAddress"
    .parameter "message"
    .parameter "statusReportRequested"
    .parameter "header"
    .parameter "encodingtype"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 1159
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@4
    move-result-object v0

    #@5
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    #@8
    move-result v8

    #@9
    .line 1161
    .local v8, activePhone:I
    const/4 v0, 0x2

    #@a
    if-ne v0, v8, :cond_1c

    #@c
    .line 1162
    invoke-static {p4}, Lcom/android/internal/telephony/SmsHeader;->fromByteArray([B)Lcom/android/internal/telephony/SmsHeader;

    #@f
    move-result-object v0

    #@10
    invoke-static {p0, p1, p2, p3, v0}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/android/internal/telephony/SmsHeader;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@13
    move-result-object v9

    #@14
    .line 1169
    .local v9, spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    :goto_14
    if-nez v9, :cond_28

    #@16
    new-instance v0, Landroid/telephony/SmsMessage$SubmitPdu;

    #@18
    invoke-direct {v0}, Landroid/telephony/SmsMessage$SubmitPdu;-><init>()V

    #@1b
    :goto_1b
    return-object v0

    #@1c
    .end local v9           #spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    :cond_1c
    move-object v0, p0

    #@1d
    move-object v1, p1

    #@1e
    move-object v2, p2

    #@1f
    move v3, p3

    #@20
    move-object v4, p4

    #@21
    move v5, p5

    #@22
    move v7, v6

    #@23
    .line 1166
    invoke-static/range {v0 .. v7}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BIII)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@26
    move-result-object v9

    #@27
    .restart local v9       #spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    goto :goto_14

    #@28
    .line 1169
    :cond_28
    new-instance v0, Landroid/telephony/SmsMessage$SubmitPdu;

    #@2a
    invoke-direct {v0, v9}, Landroid/telephony/SmsMessage$SubmitPdu;-><init>(Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)V

    #@2d
    goto :goto_1b
.end method

.method public static getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BLjava/lang/String;)Landroid/telephony/SmsMessage$SubmitPdu;
    .registers 13
    .parameter "scAddress"
    .parameter "destinationAddress"
    .parameter "message"
    .parameter "statusReportRequested"
    .parameter "header"
    .parameter "cbAddress"

    #@0
    .prologue
    .line 1266
    invoke-static {}, Landroid/telephony/SmsMessage;->useCdmaFormatForMoSms()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_1b

    #@6
    .line 1267
    invoke-static {p4}, Lcom/android/internal/telephony/SmsHeader;->fromByteArray([B)Lcom/android/internal/telephony/SmsHeader;

    #@9
    move-result-object v4

    #@a
    move-object v0, p0

    #@b
    move-object v1, p1

    #@c
    move-object v2, p2

    #@d
    move v3, p3

    #@e
    move-object v5, p5

    #@f
    invoke-static/range {v0 .. v5}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/android/internal/telephony/SmsHeader;Ljava/lang/String;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@12
    move-result-object v6

    #@13
    .line 1276
    .local v6, spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    :goto_13
    if-nez v6, :cond_20

    #@15
    new-instance v0, Landroid/telephony/SmsMessage$SubmitPdu;

    #@17
    invoke-direct {v0}, Landroid/telephony/SmsMessage$SubmitPdu;-><init>()V

    #@1a
    :goto_1a
    return-object v0

    #@1b
    .line 1271
    .end local v6           #spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    :cond_1b
    invoke-static {p0, p1, p2, p3, p4}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[B)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@1e
    move-result-object v6

    #@1f
    .restart local v6       #spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    goto :goto_13

    #@20
    .line 1276
    :cond_20
    new-instance v0, Landroid/telephony/SmsMessage$SubmitPdu;

    #@22
    invoke-direct {v0, v6}, Landroid/telephony/SmsMessage$SubmitPdu;-><init>(Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)V

    #@25
    goto :goto_1a
.end method

.method public static getSubmitPdu(Ljava/lang/String;Ljava/lang/String;S[BZ)Landroid/telephony/SmsMessage$SubmitPdu;
    .registers 7
    .parameter "scAddress"
    .parameter "destinationAddress"
    .parameter "destinationPort"
    .parameter "data"
    .parameter "statusReportRequested"

    #@0
    .prologue
    .line 1238
    invoke-static {}, Landroid/telephony/SmsMessage;->useCdmaFormatForMoSms()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_10

    #@6
    .line 1239
    invoke-static {p0, p1, p2, p3, p4}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;I[BZ)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@9
    move-result-object v0

    #@a
    .line 1246
    .local v0, spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    :goto_a
    new-instance v1, Landroid/telephony/SmsMessage$SubmitPdu;

    #@c
    invoke-direct {v1, v0}, Landroid/telephony/SmsMessage$SubmitPdu;-><init>(Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)V

    #@f
    return-object v1

    #@10
    .line 1242
    .end local v0           #spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    :cond_10
    invoke-static {p0, p1, p2, p3, p4}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;I[BZ)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@13
    move-result-object v0

    #@14
    .restart local v0       #spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    goto :goto_a
.end method

.method public static getSubmitPdu(Ljava/lang/String;Ljava/lang/String;S[BZLjava/lang/String;)Landroid/telephony/SmsMessage$SubmitPdu;
    .registers 8
    .parameter "scAddress"
    .parameter "destinationAddress"
    .parameter "destinationPort"
    .parameter "data"
    .parameter "statusReportRequested"
    .parameter "cbAddress"

    #@0
    .prologue
    .line 1329
    invoke-static {}, Landroid/telephony/SmsMessage;->useCdmaFormatForMoSms()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_12

    #@6
    .line 1330
    invoke-static/range {p0 .. p5}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;I[BZLjava/lang/String;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@9
    move-result-object v0

    #@a
    .line 1338
    .local v0, spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    :goto_a
    if-nez v0, :cond_17

    #@c
    new-instance v1, Landroid/telephony/SmsMessage$SubmitPdu;

    #@e
    invoke-direct {v1}, Landroid/telephony/SmsMessage$SubmitPdu;-><init>()V

    #@11
    :goto_11
    return-object v1

    #@12
    .line 1333
    .end local v0           #spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    :cond_12
    invoke-static {p0, p1, p2, p3, p4}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;I[BZ)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@15
    move-result-object v0

    #@16
    .restart local v0       #spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    goto :goto_a

    #@17
    .line 1338
    :cond_17
    new-instance v1, Landroid/telephony/SmsMessage$SubmitPdu;

    #@19
    invoke-direct {v1, v0}, Landroid/telephony/SmsMessage$SubmitPdu;-><init>(Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)V

    #@1c
    goto :goto_11
.end method

.method public static getSubmitPduforEmailOverSms(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/telephony/SmsMessage$SubmitPdu;
    .registers 11
    .parameter "scAddress"
    .parameter "destinationAddress"
    .parameter "message"
    .parameter "statusReportRequested"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1369
    invoke-static {}, Landroid/telephony/SmsMessage;->useCdmaFormatForMoSms()Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_13

    #@7
    .line 1370
    invoke-static {p0, p1, p2, p3, v4}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/android/internal/telephony/SmsHeader;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@a
    move-result-object v6

    #@b
    .line 1378
    .local v6, spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    :goto_b
    if-nez v6, :cond_1d

    #@d
    new-instance v0, Landroid/telephony/SmsMessage$SubmitPdu;

    #@f
    invoke-direct {v0}, Landroid/telephony/SmsMessage$SubmitPdu;-><init>()V

    #@12
    :goto_12
    return-object v0

    #@13
    .line 1373
    .end local v6           #spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    :cond_13
    const/4 v5, 0x0

    #@14
    move-object v0, p0

    #@15
    move-object v1, p1

    #@16
    move-object v2, p2

    #@17
    move v3, p3

    #@18
    invoke-static/range {v0 .. v5}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPduforEmailoverSms(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BI)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@1b
    move-result-object v6

    #@1c
    .restart local v6       #spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    goto :goto_b

    #@1d
    .line 1378
    :cond_1d
    new-instance v0, Landroid/telephony/SmsMessage$SubmitPdu;

    #@1f
    invoke-direct {v0, v6}, Landroid/telephony/SmsMessage$SubmitPdu;-><init>(Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)V

    #@22
    goto :goto_12
.end method

.method public static getSubmitPduforEmailOverSms(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BI)Landroid/telephony/SmsMessage$SubmitPdu;
    .registers 8
    .parameter "scAddress"
    .parameter "destinationAddress"
    .parameter "message"
    .parameter "statusReportRequested"
    .parameter "header"
    .parameter "encodingtype"

    #@0
    .prologue
    .line 1350
    invoke-static {}, Landroid/telephony/SmsMessage;->useCdmaFormatForMoSms()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_16

    #@6
    .line 1351
    invoke-static {p4}, Lcom/android/internal/telephony/SmsHeader;->fromByteArray([B)Lcom/android/internal/telephony/SmsHeader;

    #@9
    move-result-object v1

    #@a
    invoke-static {p0, p1, p2, p3, v1}, Lcom/android/internal/telephony/cdma/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/android/internal/telephony/SmsHeader;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@d
    move-result-object v0

    #@e
    .line 1360
    .local v0, spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    :goto_e
    if-nez v0, :cond_1b

    #@10
    new-instance v1, Landroid/telephony/SmsMessage$SubmitPdu;

    #@12
    invoke-direct {v1}, Landroid/telephony/SmsMessage$SubmitPdu;-><init>()V

    #@15
    :goto_15
    return-object v1

    #@16
    .line 1355
    .end local v0           #spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    :cond_16
    invoke-static/range {p0 .. p5}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPduforEmailoverSms(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BI)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@19
    move-result-object v0

    #@1a
    .restart local v0       #spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    goto :goto_e

    #@1b
    .line 1360
    :cond_1b
    new-instance v1, Landroid/telephony/SmsMessage$SubmitPdu;

    #@1d
    invoke-direct {v1, v0}, Landroid/telephony/SmsMessage$SubmitPdu;-><init>(Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)V

    #@20
    goto :goto_15
.end method

.method public static getTPLayerLengthForPDU(Ljava/lang/String;)I
    .registers 2
    .parameter "pdu"

    #@0
    .prologue
    .line 444
    invoke-static {}, Landroid/telephony/SmsMessage;->isCdmaVoice()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 445
    invoke-static {p0}, Lcom/android/internal/telephony/cdma/SmsMessage;->getTPLayerLengthForPDU(Ljava/lang/String;)I

    #@9
    move-result v0

    #@a
    .line 447
    :goto_a
    return v0

    #@b
    :cond_b
    invoke-static {p0}, Lcom/android/internal/telephony/gsm/SmsMessage;->getTPLayerLengthForPDU(Ljava/lang/String;)I

    #@e
    move-result v0

    #@f
    goto :goto_a
.end method

.method public static isCdmaVoice()Z
    .registers 2

    #@0
    .prologue
    .line 2064
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCurrentPhoneType()I

    #@7
    move-result v0

    #@8
    .line 2065
    .local v0, activePhone:I
    const/4 v1, 0x2

    #@9
    if-ne v1, v0, :cond_d

    #@b
    const/4 v1, 0x1

    #@c
    :goto_c
    return v1

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_c
.end method

.method public static makeSmsHeader(ILjava/lang/String;)[B
    .registers 4
    .parameter "readReplyValue"
    .parameter "replyAddress"

    #@0
    .prologue
    .line 1990
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    #@7
    move-result v0

    #@8
    .line 1992
    .local v0, activePhone:I
    const/4 v1, 0x2

    #@9
    if-ne v1, v0, :cond_18

    #@b
    .line 1993
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@e
    move-result v1

    #@f
    if-nez v1, :cond_16

    #@11
    .line 1994
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    #@14
    move-result-object v1

    #@15
    .line 1999
    :goto_15
    return-object v1

    #@16
    .line 1996
    :cond_16
    const/4 v1, 0x0

    #@17
    goto :goto_15

    #@18
    .line 1999
    :cond_18
    invoke-static {p0, p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->makeSmsHeader(ILjava/lang/String;)[B

    #@1b
    move-result-object v1

    #@1c
    goto :goto_15
.end method

.method public static newFromCMT([Ljava/lang/String;)Landroid/telephony/SmsMessage;
    .registers 3
    .parameter "lines"

    #@0
    .prologue
    .line 383
    invoke-static {p0}, Lcom/android/internal/telephony/gsm/SmsMessage;->newFromCMT([Ljava/lang/String;)Lcom/android/internal/telephony/gsm/SmsMessage;

    #@3
    move-result-object v0

    #@4
    .line 386
    .local v0, wrappedMessage:Lcom/android/internal/telephony/SmsMessageBase;
    new-instance v1, Landroid/telephony/SmsMessage;

    #@6
    invoke-direct {v1, v0}, Landroid/telephony/SmsMessage;-><init>(Lcom/android/internal/telephony/SmsMessageBase;)V

    #@9
    return-object v1
.end method

.method public static newFromParcel(Landroid/os/Parcel;)Landroid/telephony/SmsMessage;
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 392
    invoke-static {p0}, Lcom/android/internal/telephony/cdma/SmsMessage;->newFromParcel(Landroid/os/Parcel;)Lcom/android/internal/telephony/cdma/SmsMessage;

    #@3
    move-result-object v0

    #@4
    .line 395
    .local v0, wrappedMessage:Lcom/android/internal/telephony/SmsMessageBase;
    new-instance v1, Landroid/telephony/SmsMessage;

    #@6
    invoke-direct {v1, v0}, Landroid/telephony/SmsMessage;-><init>(Lcom/android/internal/telephony/SmsMessageBase;)V

    #@9
    return-object v1
.end method

.method public static setSmsIsRoaming(Z)V
    .registers 3
    .parameter "isRoaming"

    #@0
    .prologue
    .line 2333
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "setSmsIsRoaming(), isRoaming = "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@16
    .line 2335
    invoke-static {p0}, Lcom/android/internal/telephony/cdma/SmsMessage;->setSmsIsRoaming(Z)V

    #@19
    .line 2336
    return-void
.end method

.method public static setSmsPriority(I)V
    .registers 1
    .parameter "smsPriority"

    #@0
    .prologue
    .line 2082
    invoke-static {p0}, Lcom/android/internal/telephony/cdma/SmsMessage;->setSmsPriority(I)V

    #@3
    .line 2083
    return-void
.end method

.method public static setValidityPeriod(I)V
    .registers 3
    .parameter "validityperiod"

    #@0
    .prologue
    .line 1975
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    #@7
    move-result v0

    #@8
    .line 1976
    .local v0, activePhone:I
    const/4 v1, 0x2

    #@9
    if-eq v1, v0, :cond_e

    #@b
    .line 1977
    invoke-static {p0}, Lcom/android/internal/telephony/gsm/SmsMessage;->setValidityPeriod(I)V

    #@e
    .line 1979
    :cond_e
    return-void
.end method

.method public static uiccGetSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)Landroid/telephony/SmsMessage$SubmitPdu;
    .registers 9
    .parameter "scAddress"
    .parameter "destinationAddress"
    .parameter "message"
    .parameter "statusReportRequested"
    .parameter "smsformat"

    #@0
    .prologue
    .line 1563
    const/4 v1, 0x1

    #@1
    .line 1564
    .local v1, SMS_FORMAT_USIM:I
    const/4 v0, 0x2

    #@2
    .line 1566
    .local v0, SMS_FORMAT_CSIM:I
    const/4 v3, 0x1

    #@3
    if-ne p4, v3, :cond_11

    #@5
    .line 1567
    invoke-static {p0, p1, p2, p3}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@8
    move-result-object v2

    #@9
    .line 1574
    .local v2, spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    :goto_9
    if-nez v2, :cond_17

    #@b
    new-instance v3, Landroid/telephony/SmsMessage$SubmitPdu;

    #@d
    invoke-direct {v3}, Landroid/telephony/SmsMessage$SubmitPdu;-><init>()V

    #@10
    :goto_10
    return-object v3

    #@11
    .line 1570
    .end local v2           #spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    :cond_11
    const/4 v3, 0x0

    #@12
    invoke-static {p0, p1, p2, p3, v3}, Lcom/android/internal/telephony/cdma/SmsMessage;->ruimGetSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/android/internal/telephony/SmsHeader;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@15
    move-result-object v2

    #@16
    .restart local v2       #spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    goto :goto_9

    #@17
    .line 1574
    :cond_17
    new-instance v3, Landroid/telephony/SmsMessage$SubmitPdu;

    #@19
    invoke-direct {v3, v2}, Landroid/telephony/SmsMessage$SubmitPdu;-><init>(Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)V

    #@1c
    goto :goto_10
.end method

.method public static uiccGetSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BI)Landroid/telephony/SmsMessage$SubmitPdu;
    .registers 11
    .parameter "scAddress"
    .parameter "destinationAddress"
    .parameter "message"
    .parameter "statusReportRequested"
    .parameter "smsHeader"
    .parameter "smsformat"

    #@0
    .prologue
    .line 1589
    const/4 v1, 0x1

    #@1
    .line 1590
    .local v1, SMS_FORMAT_USIM:I
    const/4 v0, 0x2

    #@2
    .line 1592
    .local v0, SMS_FORMAT_CSIM:I
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "uiccGetSubmitPdu(), scAddress = "

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@18
    .line 1593
    new-instance v3, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v4, "uiccGetSubmitPdu(), destinationAddress = "

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@2e
    .line 1594
    new-instance v3, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v4, "uiccGetSubmitPdu(), message = "

    #@35
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v3

    #@41
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@44
    .line 1595
    new-instance v3, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v4, "uiccGetSubmitPdu(), statusReportRequested = "

    #@4b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v3

    #@4f
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@52
    move-result-object v3

    #@53
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v3

    #@57
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5a
    .line 1596
    new-instance v3, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v4, "uiccGetSubmitPdu(), smsformat = "

    #@61
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v3

    #@65
    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@68
    move-result-object v3

    #@69
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v3

    #@6d
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@70
    .line 1598
    const/4 v3, 0x1

    #@71
    if-ne p5, v3, :cond_86

    #@73
    .line 1599
    if-eqz p4, :cond_81

    #@75
    .line 1600
    invoke-static {p0, p1, p2, p3, p4}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[B)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@78
    move-result-object v2

    #@79
    .line 1616
    .local v2, spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    :goto_79
    if-nez v2, :cond_97

    #@7b
    new-instance v3, Landroid/telephony/SmsMessage$SubmitPdu;

    #@7d
    invoke-direct {v3}, Landroid/telephony/SmsMessage$SubmitPdu;-><init>()V

    #@80
    :goto_80
    return-object v3

    #@81
    .line 1603
    .end local v2           #spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    :cond_81
    invoke-static {p0, p1, p2, p3}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;

    #@84
    move-result-object v2

    #@85
    .restart local v2       #spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    goto :goto_79

    #@86
    .line 1607
    .end local v2           #spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    :cond_86
    if-eqz p4, :cond_91

    #@88
    .line 1608
    invoke-static {p4}, Lcom/android/internal/telephony/SmsHeader;->fromByteArray([B)Lcom/android/internal/telephony/SmsHeader;

    #@8b
    move-result-object v3

    #@8c
    invoke-static {p0, p1, p2, p3, v3}, Lcom/android/internal/telephony/cdma/SmsMessage;->ruimGetSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/android/internal/telephony/SmsHeader;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@8f
    move-result-object v2

    #@90
    .restart local v2       #spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    goto :goto_79

    #@91
    .line 1611
    .end local v2           #spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    :cond_91
    const/4 v3, 0x0

    #@92
    invoke-static {p0, p1, p2, p3, v3}, Lcom/android/internal/telephony/cdma/SmsMessage;->ruimGetSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/android/internal/telephony/SmsHeader;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@95
    move-result-object v2

    #@96
    .restart local v2       #spb:Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;
    goto :goto_79

    #@97
    .line 1616
    :cond_97
    new-instance v3, Landroid/telephony/SmsMessage$SubmitPdu;

    #@99
    invoke-direct {v3, v2}, Landroid/telephony/SmsMessage$SubmitPdu;-><init>(Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;)V

    #@9c
    goto :goto_80
.end method

.method public static uiccgetDeliverPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ[BII)Landroid/telephony/SmsMessage$DeliverPdu;
    .registers 21
    .parameter "scAddress"
    .parameter "originatorAddress"
    .parameter "message"
    .parameter "statusReportRequested"
    .parameter "time"
    .parameter "header"
    .parameter "encodingtype"
    .parameter "sms_format"

    #@0
    .prologue
    .line 1631
    const/4 v9, 0x1

    #@1
    .line 1632
    .local v9, SMS_FORMAT_USIM:I
    const/4 v8, 0x2

    #@2
    .line 1634
    .local v8, SMS_FORMAT_CSIM:I
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    #@9
    move-result v10

    #@a
    .line 1636
    .local v10, activePhone:I
    const/4 v1, 0x1

    #@b
    move/from16 v0, p8

    #@d
    if-ne v0, v1, :cond_1b

    #@f
    .line 1637
    invoke-static/range {p0 .. p7}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDeliverPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ[BI)Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;

    #@12
    move-result-object v11

    #@13
    .line 1645
    .local v11, spb:Lcom/android/internal/telephony/SmsMessageBase$DeliverPduBase;
    :goto_13
    if-nez v11, :cond_27

    #@15
    new-instance v1, Landroid/telephony/SmsMessage$DeliverPdu;

    #@17
    invoke-direct {v1}, Landroid/telephony/SmsMessage$DeliverPdu;-><init>()V

    #@1a
    :goto_1a
    return-object v1

    #@1b
    .line 1640
    .end local v11           #spb:Lcom/android/internal/telephony/SmsMessageBase$DeliverPduBase;
    :cond_1b
    const/4 v5, 0x0

    #@1c
    move-object v1, p0

    #@1d
    move-object v2, p1

    #@1e
    move-object v3, p2

    #@1f
    move v4, p3

    #@20
    move-wide/from16 v6, p4

    #@22
    invoke-static/range {v1 .. v7}, Lcom/android/internal/telephony/cdma/SmsMessage;->ruimGetDeliverPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/android/internal/telephony/SmsHeader;J)Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;

    #@25
    move-result-object v11

    #@26
    .restart local v11       #spb:Lcom/android/internal/telephony/SmsMessageBase$DeliverPduBase;
    goto :goto_13

    #@27
    .line 1645
    :cond_27
    new-instance v1, Landroid/telephony/SmsMessage$DeliverPdu;

    #@29
    invoke-direct {v1, v11}, Landroid/telephony/SmsMessage$DeliverPdu;-><init>(Lcom/android/internal/telephony/SmsMessageBase$DeliverPduBase;)V

    #@2c
    goto :goto_1a
.end method

.method private static useCdmaFormatForMoSms()Z
    .registers 2

    #@0
    .prologue
    .line 2030
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/telephony/SmsManager;->isImsSmsSupported()Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_f

    #@a
    .line 2032
    invoke-static {}, Landroid/telephony/SmsMessage;->isCdmaVoice()Z

    #@d
    move-result v0

    #@e
    .line 2035
    :goto_e
    return v0

    #@f
    :cond_f
    const-string v0, "3gpp2"

    #@11
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Landroid/telephony/SmsManager;->getImsSmsFormat()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v0

    #@1d
    goto :goto_e
.end method

.method private static useCdmaFormatForMoSms(Z)Z
    .registers 3
    .parameter "is3GPP2Composing"

    #@0
    .prologue
    .line 2048
    move v0, p0

    #@1
    .line 2049
    .local v0, is3GPP2:Z
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    #@4
    move-result-object v1

    #@5
    invoke-virtual {v1}, Landroid/telephony/SmsManager;->isImsSmsSupported()Z

    #@8
    move-result v1

    #@9
    if-nez v1, :cond_f

    #@b
    .line 2051
    invoke-static {}, Landroid/telephony/SmsMessage;->isCdmaVoice()Z

    #@e
    move-result v0

    #@f
    .line 2053
    .end local v0           #is3GPP2:Z
    :cond_f
    return v0
.end method


# virtual methods
.method public createDataCodingScheme(Landroid/telephony/SmsMessage$MessageClass;ZIIZI)B
    .registers 9
    .parameter "messageclass"
    .parameter "isCompressed"
    .parameter "encodingtype"
    .parameter "msgwatingtype"
    .parameter "msgwaitingactive"
    .parameter "msgwaitingkind"

    #@0
    .prologue
    .line 1386
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    #@7
    move-result v0

    #@8
    .line 1387
    .local v0, activePhone:I
    const/4 v1, 0x2

    #@9
    if-eq v1, v0, :cond_b

    #@b
    .line 1390
    :cond_b
    const/4 v1, -0x1

    #@c
    return v1
.end method

.method public getBearData()Lcom/android/internal/telephony/cdma/sms/BearerData;
    .registers 2

    #@0
    .prologue
    .line 1861
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    check-cast v0, Lcom/android/internal/telephony/cdma/SmsMessage;

    #@4
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/SmsMessage;->getBearData()Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getConfirmReadInfo()Landroid/os/Bundle;
    .registers 3

    #@0
    .prologue
    .line 1703
    iget-object v1, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    instance-of v1, v1, Lcom/android/internal/telephony/gsm/SmsMessage;

    #@4
    if-eqz v1, :cond_f

    #@6
    .line 1704
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@8
    check-cast v0, Lcom/android/internal/telephony/gsm/SmsMessage;

    #@a
    .line 1706
    .local v0, gsmWrappedSmsMessage:Lcom/android/internal/telephony/gsm/SmsMessage;
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/SmsMessage;->getConfirmReadInfo()Landroid/os/Bundle;

    #@d
    move-result-object v1

    #@e
    .line 1708
    .end local v0           #gsmWrappedSmsMessage:Lcom/android/internal/telephony/gsm/SmsMessage;
    :goto_e
    return-object v1

    #@f
    :cond_f
    const/4 v1, 0x0

    #@10
    goto :goto_e
.end method

.method public getDataCodingScheme()I
    .registers 3

    #@0
    .prologue
    .line 2012
    iget-object v1, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    instance-of v1, v1, Lcom/android/internal/telephony/gsm/SmsMessage;

    #@4
    if-eqz v1, :cond_f

    #@6
    .line 2013
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@8
    check-cast v0, Lcom/android/internal/telephony/gsm/SmsMessage;

    #@a
    .line 2015
    .local v0, gsmWrappedSmsMessage:Lcom/android/internal/telephony/gsm/SmsMessage;
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDataCodingScheme()I

    #@d
    move-result v1

    #@e
    .line 2017
    .end local v0           #gsmWrappedSmsMessage:Lcom/android/internal/telephony/gsm/SmsMessage;
    :goto_e
    return v1

    #@f
    :cond_f
    const/4 v1, 0x0

    #@10
    goto :goto_e
.end method

.method public getDestinationAddress()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1529
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->getDestinationAddress()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getDisplayCallbackNum()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1742
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->getCallbackNum()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getDisplayMessageBody()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1732
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->getDisplayMessageBody()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getDisplayMessageBodyEx()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 2176
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->getDisplayMessageBodyEx()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getDisplayOriginatingAddress()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1672
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->getDisplayOriginatingAddress()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getDisplayOriginatingAddressEx()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 2148
    invoke-virtual {p0}, Landroid/telephony/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getEmailBody()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1776
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->getEmailBody()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getEmailFrom()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1784
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->getEmailFrom()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getIndexOnIcc()I
    .registers 2

    #@0
    .prologue
    .line 1919
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->getIndexOnIcc()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getIndexOnSim()I
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1910
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->getIndexOnIcc()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getMessageBody()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1680
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->getMessageBody()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getMessageClass()Landroid/telephony/SmsMessage$MessageClass;
    .registers 3

    #@0
    .prologue
    .line 1717
    sget-object v0, Landroid/telephony/SmsMessage$1;->$SwitchMap$com$android$internal$telephony$SmsConstants$MessageClass:[I

    #@2
    iget-object v1, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@4
    invoke-virtual {v1}, Lcom/android/internal/telephony/SmsMessageBase;->getMessageClass()Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v1}, Lcom/android/internal/telephony/SmsConstants$MessageClass;->ordinal()I

    #@b
    move-result v1

    #@c
    aget v0, v0, v1

    #@e
    packed-switch v0, :pswitch_data_20

    #@11
    .line 1722
    sget-object v0, Landroid/telephony/SmsMessage$MessageClass;->UNKNOWN:Landroid/telephony/SmsMessage$MessageClass;

    #@13
    :goto_13
    return-object v0

    #@14
    .line 1718
    :pswitch_14
    sget-object v0, Landroid/telephony/SmsMessage$MessageClass;->CLASS_0:Landroid/telephony/SmsMessage$MessageClass;

    #@16
    goto :goto_13

    #@17
    .line 1719
    :pswitch_17
    sget-object v0, Landroid/telephony/SmsMessage$MessageClass;->CLASS_1:Landroid/telephony/SmsMessage$MessageClass;

    #@19
    goto :goto_13

    #@1a
    .line 1720
    :pswitch_1a
    sget-object v0, Landroid/telephony/SmsMessage$MessageClass;->CLASS_2:Landroid/telephony/SmsMessage$MessageClass;

    #@1c
    goto :goto_13

    #@1d
    .line 1721
    :pswitch_1d
    sget-object v0, Landroid/telephony/SmsMessage$MessageClass;->CLASS_3:Landroid/telephony/SmsMessage$MessageClass;

    #@1f
    goto :goto_13

    #@20
    .line 1717
    :pswitch_data_20
    .packed-switch 0x1
        :pswitch_14
        :pswitch_17
        :pswitch_1a
        :pswitch_1d
    .end packed-switch
.end method

.method public getNumOfVoicemails()I
    .registers 2

    #@0
    .prologue
    .line 2157
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->getNumOfVoicemails()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getOriginalAddress()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 2242
    iget-object v2, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    instance-of v2, v2, Lcom/android/internal/telephony/gsm/SmsMessage;

    #@4
    if-eqz v2, :cond_f

    #@6
    .line 2243
    iget-object v1, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@8
    check-cast v1, Lcom/android/internal/telephony/gsm/SmsMessage;

    #@a
    .line 2245
    .local v1, gsmWrappedSmsMessage:Lcom/android/internal/telephony/gsm/SmsMessage;
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SmsMessage;->getOriginalAddress()Ljava/lang/String;

    #@d
    move-result-object v2

    #@e
    .line 2251
    .end local v1           #gsmWrappedSmsMessage:Lcom/android/internal/telephony/gsm/SmsMessage;
    :goto_e
    return-object v2

    #@f
    .line 2246
    :cond_f
    iget-object v2, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@11
    instance-of v2, v2, Lcom/android/internal/telephony/cdma/SmsMessage;

    #@13
    if-eqz v2, :cond_1e

    #@15
    .line 2247
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@17
    check-cast v0, Lcom/android/internal/telephony/cdma/SmsMessage;

    #@19
    .line 2249
    .local v0, cdmaWrappedSmsMessage:Lcom/android/internal/telephony/cdma/SmsMessage;
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/SmsMessage;->getOriginalAddress()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    goto :goto_e

    #@1e
    .line 2251
    .end local v0           #cdmaWrappedSmsMessage:Lcom/android/internal/telephony/cdma/SmsMessage;
    :cond_1e
    const/4 v2, 0x0

    #@1f
    goto :goto_e
.end method

.method public getOriginatingAddress()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1663
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->getOriginatingAddress()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getPdu()[B
    .registers 2

    #@0
    .prologue
    .line 1871
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->getPdu()[B

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getProtocolIdentifier()I
    .registers 2

    #@0
    .prologue
    .line 1791
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->getProtocolIdentifier()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getPseudoSubject()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1751
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->getPseudoSubject()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getReplyAddress()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 2126
    iget-object v2, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    instance-of v2, v2, Lcom/android/internal/telephony/gsm/SmsMessage;

    #@4
    if-eqz v2, :cond_f

    #@6
    .line 2127
    iget-object v1, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@8
    check-cast v1, Lcom/android/internal/telephony/gsm/SmsMessage;

    #@a
    .line 2129
    .local v1, gsmWrappedSmsMessage:Lcom/android/internal/telephony/gsm/SmsMessage;
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/SmsMessage;->getreplyAddress()Ljava/lang/String;

    #@d
    move-result-object v2

    #@e
    .line 2135
    .end local v1           #gsmWrappedSmsMessage:Lcom/android/internal/telephony/gsm/SmsMessage;
    :goto_e
    return-object v2

    #@f
    .line 2130
    :cond_f
    iget-object v2, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@11
    instance-of v2, v2, Lcom/android/internal/telephony/cdma/SmsMessage;

    #@13
    if-eqz v2, :cond_1e

    #@15
    .line 2131
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@17
    check-cast v0, Lcom/android/internal/telephony/cdma/SmsMessage;

    #@19
    .line 2133
    .local v0, cdmaWrappedSmsMessage:Lcom/android/internal/telephony/cdma/SmsMessage;
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    goto :goto_e

    #@1e
    .line 2135
    .end local v0           #cdmaWrappedSmsMessage:Lcom/android/internal/telephony/cdma/SmsMessage;
    :cond_1e
    const/4 v2, 0x0

    #@1f
    goto :goto_e
.end method

.method public getServiceCenterAddress()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1655
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->getServiceCenterAddress()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getSmsDisplayMode()I
    .registers 2

    #@0
    .prologue
    .line 2102
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->getSmsDisplayMode()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getSpecialMessageInfo()Landroid/os/Bundle;
    .registers 3

    #@0
    .prologue
    .line 1688
    iget-object v1, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    instance-of v1, v1, Lcom/android/internal/telephony/gsm/SmsMessage;

    #@4
    if-eqz v1, :cond_f

    #@6
    .line 1689
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@8
    check-cast v0, Lcom/android/internal/telephony/gsm/SmsMessage;

    #@a
    .line 1691
    .local v0, gsmWrappedSmsMessage:Lcom/android/internal/telephony/gsm/SmsMessage;
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/SmsMessage;->getSpecialMessageInfo()Landroid/os/Bundle;

    #@d
    move-result-object v1

    #@e
    .line 1693
    .end local v0           #gsmWrappedSmsMessage:Lcom/android/internal/telephony/gsm/SmsMessage;
    :goto_e
    return-object v1

    #@f
    :cond_f
    const/4 v1, 0x0

    #@10
    goto :goto_e
.end method

.method public getStatus()I
    .registers 2

    #@0
    .prologue
    .line 1949
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->getStatus()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getStatusOnIcc()I
    .registers 2

    #@0
    .prologue
    .line 1900
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->getStatusOnIcc()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getStatusOnSim()I
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1886
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->getStatusOnIcc()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getStatusReportReq()I
    .registers 2

    #@0
    .prologue
    .line 1538
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->getStatusReportReq()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getSubId()I
    .registers 2

    #@0
    .prologue
    .line 238
    iget v0, p0, Landroid/telephony/SmsMessage;->mSubId:I

    #@2
    return v0
.end method

.method public getTeleServiceId()I
    .registers 4

    #@0
    .prologue
    .line 2110
    iget-object v1, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    instance-of v1, v1, Lcom/android/internal/telephony/cdma/SmsMessage;

    #@4
    if-eqz v1, :cond_f

    #@6
    .line 2111
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@8
    check-cast v0, Lcom/android/internal/telephony/cdma/SmsMessage;

    #@a
    .line 2112
    .local v0, cdmaWrappedSmsMessage:Lcom/android/internal/telephony/cdma/SmsMessage;
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/SmsMessage;->getTeleServiceId()I

    #@d
    move-result v1

    #@e
    return v1

    #@f
    .line 2114
    .end local v0           #cdmaWrappedSmsMessage:Lcom/android/internal/telephony/cdma/SmsMessage;
    :cond_f
    new-instance v1, Ljava/lang/RuntimeException;

    #@11
    const-string v2, "[getTeleService] GSM Not supported!"

    #@13
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@16
    throw v1
.end method

.method public getTimeforSMSonSIM()J
    .registers 3

    #@0
    .prologue
    .line 1545
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-static {}, Lcom/android/internal/telephony/SmsMessageBase;->getTimeforSMSonSIM()J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method public getTimestampMillis()J
    .registers 3

    #@0
    .prologue
    .line 1758
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->getTimestampMillis()J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method public getUserData()[B
    .registers 2

    #@0
    .prologue
    .line 1853
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->getUserData()[B

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getUserDataHeader()Lcom/android/internal/telephony/SmsHeader;
    .registers 2

    #@0
    .prologue
    .line 2166
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->getUserDataHeader()Lcom/android/internal/telephony/SmsHeader;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public isCphsMwiMessage()Z
    .registers 2

    #@0
    .prologue
    .line 1809
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->isCphsMwiMessage()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isEmail()Z
    .registers 2

    #@0
    .prologue
    .line 1768
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->isEmail()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isMWIClearMessage()Z
    .registers 2

    #@0
    .prologue
    .line 1817
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->isMWIClearMessage()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isMWISetMessage()Z
    .registers 2

    #@0
    .prologue
    .line 1825
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->isMWISetMessage()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isMwiDontStore()Z
    .registers 2

    #@0
    .prologue
    .line 1833
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->isMwiDontStore()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isMwiUrgentMessage()Z
    .registers 2

    #@0
    .prologue
    .line 1844
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->isMwiUrgentMessage()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isReplace()Z
    .registers 2

    #@0
    .prologue
    .line 1799
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->isReplace()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isReplyPathPresent()Z
    .registers 2

    #@0
    .prologue
    .line 1964
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->isReplyPathPresent()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isStatusReportMessage()Z
    .registers 2

    #@0
    .prologue
    .line 1956
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsMessageBase;->isStatusReportMessage()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public replaceMessageBody(Ljava/lang/String;)Z
    .registers 3
    .parameter "newText"

    #@0
    .prologue
    .line 2259
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/SmsMessageBase;->replaceMessageBody(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public setIndexOnIcc(I)V
    .registers 3
    .parameter "indexOnIcc"

    #@0
    .prologue
    .line 1928
    iget-object v0, p0, Landroid/telephony/SmsMessage;->mWrappedSmsMessage:Lcom/android/internal/telephony/SmsMessageBase;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/SmsMessageBase;->setIndexOnIcc(I)V

    #@5
    .line 1929
    return-void
.end method

.method public setSubId(I)V
    .registers 2
    .parameter "subId"

    #@0
    .prologue
    .line 230
    iput p1, p0, Landroid/telephony/SmsMessage;->mSubId:I

    #@2
    .line 231
    return-void
.end method
