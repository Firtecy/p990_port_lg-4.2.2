.class public Landroid/telephony/SmsCbLocation;
.super Ljava/lang/Object;
.source "SmsCbLocation.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/telephony/SmsCbLocation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mCid:I

.field private final mLac:I

.field private final mPlmn:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 181
    new-instance v0, Landroid/telephony/SmsCbLocation$1;

    #@2
    invoke-direct {v0}, Landroid/telephony/SmsCbLocation$1;-><init>()V

    #@5
    sput-object v0, Landroid/telephony/SmsCbLocation;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 44
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 45
    const-string v0, ""

    #@6
    iput-object v0, p0, Landroid/telephony/SmsCbLocation;->mPlmn:Ljava/lang/String;

    #@8
    .line 46
    iput v1, p0, Landroid/telephony/SmsCbLocation;->mLac:I

    #@a
    .line 47
    iput v1, p0, Landroid/telephony/SmsCbLocation;->mCid:I

    #@c
    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 73
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/telephony/SmsCbLocation;->mPlmn:Ljava/lang/String;

    #@9
    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v0

    #@d
    iput v0, p0, Landroid/telephony/SmsCbLocation;->mLac:I

    #@f
    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@12
    move-result v0

    #@13
    iput v0, p0, Landroid/telephony/SmsCbLocation;->mCid:I

    #@15
    .line 77
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "plmn"

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 54
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 55
    iput-object p1, p0, Landroid/telephony/SmsCbLocation;->mPlmn:Ljava/lang/String;

    #@6
    .line 56
    iput v0, p0, Landroid/telephony/SmsCbLocation;->mLac:I

    #@8
    .line 57
    iput v0, p0, Landroid/telephony/SmsCbLocation;->mCid:I

    #@a
    .line 58
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter "plmn"
    .parameter "lac"
    .parameter "cid"

    #@0
    .prologue
    .line 64
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 65
    iput-object p1, p0, Landroid/telephony/SmsCbLocation;->mPlmn:Ljava/lang/String;

    #@5
    .line 66
    iput p2, p0, Landroid/telephony/SmsCbLocation;->mLac:I

    #@7
    .line 67
    iput p3, p0, Landroid/telephony/SmsCbLocation;->mCid:I

    #@9
    .line 68
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 200
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 113
    if-ne p1, p0, :cond_5

    #@4
    .line 120
    :cond_4
    :goto_4
    return v1

    #@5
    .line 116
    :cond_5
    if-eqz p1, :cond_b

    #@7
    instance-of v3, p1, Landroid/telephony/SmsCbLocation;

    #@9
    if-nez v3, :cond_d

    #@b
    :cond_b
    move v1, v2

    #@c
    .line 117
    goto :goto_4

    #@d
    :cond_d
    move-object v0, p1

    #@e
    .line 119
    check-cast v0, Landroid/telephony/SmsCbLocation;

    #@10
    .line 120
    .local v0, other:Landroid/telephony/SmsCbLocation;
    iget-object v3, p0, Landroid/telephony/SmsCbLocation;->mPlmn:Ljava/lang/String;

    #@12
    iget-object v4, v0, Landroid/telephony/SmsCbLocation;->mPlmn:Ljava/lang/String;

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v3

    #@18
    if-eqz v3, :cond_26

    #@1a
    iget v3, p0, Landroid/telephony/SmsCbLocation;->mLac:I

    #@1c
    iget v4, v0, Landroid/telephony/SmsCbLocation;->mLac:I

    #@1e
    if-ne v3, v4, :cond_26

    #@20
    iget v3, p0, Landroid/telephony/SmsCbLocation;->mCid:I

    #@22
    iget v4, v0, Landroid/telephony/SmsCbLocation;->mCid:I

    #@24
    if-eq v3, v4, :cond_4

    #@26
    :cond_26
    move v1, v2

    #@27
    goto :goto_4
.end method

.method public getCid()I
    .registers 2

    #@0
    .prologue
    .line 100
    iget v0, p0, Landroid/telephony/SmsCbLocation;->mCid:I

    #@2
    return v0
.end method

.method public getLac()I
    .registers 2

    #@0
    .prologue
    .line 92
    iget v0, p0, Landroid/telephony/SmsCbLocation;->mLac:I

    #@2
    return v0
.end method

.method public getPlmn()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 84
    iget-object v0, p0, Landroid/telephony/SmsCbLocation;->mPlmn:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    .line 105
    iget-object v1, p0, Landroid/telephony/SmsCbLocation;->mPlmn:Ljava/lang/String;

    #@2
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    #@5
    move-result v0

    #@6
    .line 106
    .local v0, hash:I
    mul-int/lit8 v1, v0, 0x1f

    #@8
    iget v2, p0, Landroid/telephony/SmsCbLocation;->mLac:I

    #@a
    add-int v0, v1, v2

    #@c
    .line 107
    mul-int/lit8 v1, v0, 0x1f

    #@e
    iget v2, p0, Landroid/telephony/SmsCbLocation;->mCid:I

    #@10
    add-int v0, v1, v2

    #@12
    .line 108
    return v0
.end method

.method public isInLocationArea(Landroid/telephony/SmsCbLocation;)Z
    .registers 6
    .parameter "area"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v3, -0x1

    #@2
    .line 135
    iget v1, p0, Landroid/telephony/SmsCbLocation;->mCid:I

    #@4
    if-eq v1, v3, :cond_d

    #@6
    iget v1, p0, Landroid/telephony/SmsCbLocation;->mCid:I

    #@8
    iget v2, p1, Landroid/telephony/SmsCbLocation;->mCid:I

    #@a
    if-eq v1, v2, :cond_d

    #@c
    .line 141
    :cond_c
    :goto_c
    return v0

    #@d
    .line 138
    :cond_d
    iget v1, p0, Landroid/telephony/SmsCbLocation;->mLac:I

    #@f
    if-eq v1, v3, :cond_17

    #@11
    iget v1, p0, Landroid/telephony/SmsCbLocation;->mLac:I

    #@13
    iget v2, p1, Landroid/telephony/SmsCbLocation;->mLac:I

    #@15
    if-ne v1, v2, :cond_c

    #@17
    .line 141
    :cond_17
    iget-object v0, p0, Landroid/telephony/SmsCbLocation;->mPlmn:Ljava/lang/String;

    #@19
    iget-object v1, p1, Landroid/telephony/SmsCbLocation;->mPlmn:Ljava/lang/String;

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v0

    #@1f
    goto :goto_c
.end method

.method public isInLocationArea(Ljava/lang/String;II)Z
    .registers 7
    .parameter "plmn"
    .parameter "lac"
    .parameter "cid"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 153
    iget-object v1, p0, Landroid/telephony/SmsCbLocation;->mPlmn:Ljava/lang/String;

    #@4
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_b

    #@a
    .line 165
    :cond_a
    :goto_a
    return v0

    #@b
    .line 157
    :cond_b
    iget v1, p0, Landroid/telephony/SmsCbLocation;->mLac:I

    #@d
    if-eq v1, v2, :cond_13

    #@f
    iget v1, p0, Landroid/telephony/SmsCbLocation;->mLac:I

    #@11
    if-ne v1, p2, :cond_a

    #@13
    .line 161
    :cond_13
    iget v1, p0, Landroid/telephony/SmsCbLocation;->mCid:I

    #@15
    if-eq v1, v2, :cond_1b

    #@17
    iget v1, p0, Landroid/telephony/SmsCbLocation;->mCid:I

    #@19
    if-ne v1, p3, :cond_a

    #@1b
    .line 165
    :cond_1b
    const/4 v0, 0x1

    #@1c
    goto :goto_a
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    const/16 v2, 0x2c

    #@2
    .line 125
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const/16 v1, 0x5b

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    iget-object v1, p0, Landroid/telephony/SmsCbLocation;->mPlmn:Ljava/lang/String;

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Landroid/telephony/SmsCbLocation;->mLac:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    iget v1, p0, Landroid/telephony/SmsCbLocation;->mCid:I

    #@23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    const/16 v1, 0x5d

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v0

    #@2d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v0

    #@31
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 176
    iget-object v0, p0, Landroid/telephony/SmsCbLocation;->mPlmn:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 177
    iget v0, p0, Landroid/telephony/SmsCbLocation;->mLac:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 178
    iget v0, p0, Landroid/telephony/SmsCbLocation;->mCid:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 179
    return-void
.end method
