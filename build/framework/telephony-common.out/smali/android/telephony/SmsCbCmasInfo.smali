.class public Landroid/telephony/SmsCbCmasInfo;
.super Ljava/lang/Object;
.source "SmsCbCmasInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CMAS_CATEGORY_CBRNE:I = 0xa

.field public static final CMAS_CATEGORY_ENV:I = 0x7

.field public static final CMAS_CATEGORY_FIRE:I = 0x5

.field public static final CMAS_CATEGORY_GEO:I = 0x0

.field public static final CMAS_CATEGORY_HEALTH:I = 0x6

.field public static final CMAS_CATEGORY_INFRA:I = 0x9

.field public static final CMAS_CATEGORY_MET:I = 0x1

.field public static final CMAS_CATEGORY_OTHER:I = 0xb

.field public static final CMAS_CATEGORY_RESCUE:I = 0x4

.field public static final CMAS_CATEGORY_SAFETY:I = 0x2

.field public static final CMAS_CATEGORY_SECURITY:I = 0x3

.field public static final CMAS_CATEGORY_TRANSPORT:I = 0x8

.field public static final CMAS_CATEGORY_UNKNOWN:I = -0x1

.field public static final CMAS_CERTAINTY_LIKELY:I = 0x1

.field public static final CMAS_CERTAINTY_OBSERVED:I = 0x0

.field public static final CMAS_CERTAINTY_UNKNOWN:I = -0x1

.field public static final CMAS_CLASS_CHILD_ABDUCTION_EMERGENCY:I = 0x3

.field public static final CMAS_CLASS_CMAS_EXERCISE:I = 0x5

.field public static final CMAS_CLASS_EXTREME_THREAT:I = 0x1

.field public static final CMAS_CLASS_OPERATOR_DEFINED_USE:I = 0x6

.field public static final CMAS_CLASS_PRESIDENTIAL_LEVEL_ALERT:I = 0x0

.field public static final CMAS_CLASS_REQUIRED_MONTHLY_TEST:I = 0x4

.field public static final CMAS_CLASS_SEVERE_THREAT:I = 0x2

.field public static final CMAS_CLASS_UNKNOWN:I = -0x1

.field public static final CMAS_RESPONSE_TYPE_ASSESS:I = 0x6

.field public static final CMAS_RESPONSE_TYPE_AVOID:I = 0x5

.field public static final CMAS_RESPONSE_TYPE_EVACUATE:I = 0x1

.field public static final CMAS_RESPONSE_TYPE_EXECUTE:I = 0x3

.field public static final CMAS_RESPONSE_TYPE_MONITOR:I = 0x4

.field public static final CMAS_RESPONSE_TYPE_NONE:I = 0x7

.field public static final CMAS_RESPONSE_TYPE_PREPARE:I = 0x2

.field public static final CMAS_RESPONSE_TYPE_SHELTER:I = 0x0

.field public static final CMAS_RESPONSE_TYPE_UNKNOWN:I = -0x1

.field public static final CMAS_SEVERITY_EXTREME:I = 0x0

.field public static final CMAS_SEVERITY_SEVERE:I = 0x1

.field public static final CMAS_SEVERITY_UNKNOWN:I = -0x1

.field public static final CMAS_URGENCY_EXPECTED:I = 0x1

.field public static final CMAS_URGENCY_IMMEDIATE:I = 0x0

.field public static final CMAS_URGENCY_UNKNOWN:I = -0x1

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/telephony/SmsCbCmasInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mCategory:I

.field private final mCertainty:I

.field private mExpires:J

.field private final mMessageClass:I

.field private final mResponseType:I

.field private final mSeverity:I

.field private final mUrgency:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 331
    new-instance v0, Landroid/telephony/SmsCbCmasInfo$1;

    #@2
    invoke-direct {v0}, Landroid/telephony/SmsCbCmasInfo$1;-><init>()V

    #@5
    sput-object v0, Landroid/telephony/SmsCbCmasInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(IIIIII)V
    .registers 9
    .parameter "messageClass"
    .parameter "category"
    .parameter "responseType"
    .parameter "severity"
    .parameter "urgency"
    .parameter "certainty"

    #@0
    .prologue
    .line 203
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 198
    const-wide/16 v0, -0x1

    #@5
    iput-wide v0, p0, Landroid/telephony/SmsCbCmasInfo;->mExpires:J

    #@7
    .line 204
    iput p1, p0, Landroid/telephony/SmsCbCmasInfo;->mMessageClass:I

    #@9
    .line 205
    iput p2, p0, Landroid/telephony/SmsCbCmasInfo;->mCategory:I

    #@b
    .line 206
    iput p3, p0, Landroid/telephony/SmsCbCmasInfo;->mResponseType:I

    #@d
    .line 207
    iput p4, p0, Landroid/telephony/SmsCbCmasInfo;->mSeverity:I

    #@f
    .line 208
    iput p5, p0, Landroid/telephony/SmsCbCmasInfo;->mUrgency:I

    #@11
    .line 209
    iput p6, p0, Landroid/telephony/SmsCbCmasInfo;->mCertainty:I

    #@13
    .line 210
    return-void
.end method

.method public constructor <init>(IIIIIIJ)V
    .registers 9
    .parameter "messageClass"
    .parameter "category"
    .parameter "responseType"
    .parameter "severity"
    .parameter "urgency"
    .parameter "certainty"
    .parameter "expires"

    #@0
    .prologue
    .line 216
    invoke-direct/range {p0 .. p6}, Landroid/telephony/SmsCbCmasInfo;-><init>(IIIIII)V

    #@3
    .line 218
    iput-wide p7, p0, Landroid/telephony/SmsCbCmasInfo;->mExpires:J

    #@5
    .line 219
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "in"

    #@0
    .prologue
    .line 223
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 198
    const-wide/16 v0, -0x1

    #@5
    iput-wide v0, p0, Landroid/telephony/SmsCbCmasInfo;->mExpires:J

    #@7
    .line 224
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@a
    move-result v0

    #@b
    iput v0, p0, Landroid/telephony/SmsCbCmasInfo;->mMessageClass:I

    #@d
    .line 225
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@10
    move-result v0

    #@11
    iput v0, p0, Landroid/telephony/SmsCbCmasInfo;->mCategory:I

    #@13
    .line 226
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@16
    move-result v0

    #@17
    iput v0, p0, Landroid/telephony/SmsCbCmasInfo;->mResponseType:I

    #@19
    .line 227
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1c
    move-result v0

    #@1d
    iput v0, p0, Landroid/telephony/SmsCbCmasInfo;->mSeverity:I

    #@1f
    .line 228
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@22
    move-result v0

    #@23
    iput v0, p0, Landroid/telephony/SmsCbCmasInfo;->mUrgency:I

    #@25
    .line 229
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@28
    move-result v0

    #@29
    iput v0, p0, Landroid/telephony/SmsCbCmasInfo;->mCertainty:I

    #@2b
    .line 231
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@2e
    move-result-wide v0

    #@2f
    iput-wide v0, p0, Landroid/telephony/SmsCbCmasInfo;->mExpires:J

    #@31
    .line 233
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 326
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getCategory()I
    .registers 2

    #@0
    .prologue
    .line 267
    iget v0, p0, Landroid/telephony/SmsCbCmasInfo;->mCategory:I

    #@2
    return v0
.end method

.method public getCertainty()I
    .registers 2

    #@0
    .prologue
    .line 299
    iget v0, p0, Landroid/telephony/SmsCbCmasInfo;->mCertainty:I

    #@2
    return v0
.end method

.method public getExpires()J
    .registers 3

    #@0
    .prologue
    .line 309
    iget-wide v0, p0, Landroid/telephony/SmsCbCmasInfo;->mExpires:J

    #@2
    return-wide v0
.end method

.method public getMessageClass()I
    .registers 2

    #@0
    .prologue
    .line 259
    iget v0, p0, Landroid/telephony/SmsCbCmasInfo;->mMessageClass:I

    #@2
    return v0
.end method

.method public getResponseType()I
    .registers 2

    #@0
    .prologue
    .line 275
    iget v0, p0, Landroid/telephony/SmsCbCmasInfo;->mResponseType:I

    #@2
    return v0
.end method

.method public getSeverity()I
    .registers 2

    #@0
    .prologue
    .line 283
    iget v0, p0, Landroid/telephony/SmsCbCmasInfo;->mSeverity:I

    #@2
    return v0
.end method

.method public getUrgency()I
    .registers 2

    #@0
    .prologue
    .line 291
    iget v0, p0, Landroid/telephony/SmsCbCmasInfo;->mUrgency:I

    #@2
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 315
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "SmsCbCmasInfo{messageClass="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Landroid/telephony/SmsCbCmasInfo;->mMessageClass:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", category="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Landroid/telephony/SmsCbCmasInfo;->mCategory:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ", responseType="

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Landroid/telephony/SmsCbCmasInfo;->mResponseType:I

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, ", severity="

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    iget v1, p0, Landroid/telephony/SmsCbCmasInfo;->mSeverity:I

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const-string v1, ", urgency="

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    iget v1, p0, Landroid/telephony/SmsCbCmasInfo;->mUrgency:I

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    const-string v1, ", certainty="

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    iget v1, p0, Landroid/telephony/SmsCbCmasInfo;->mCertainty:I

    #@49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    const/16 v1, 0x7d

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@52
    move-result-object v0

    #@53
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v0

    #@57
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 243
    iget v0, p0, Landroid/telephony/SmsCbCmasInfo;->mMessageClass:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 244
    iget v0, p0, Landroid/telephony/SmsCbCmasInfo;->mCategory:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 245
    iget v0, p0, Landroid/telephony/SmsCbCmasInfo;->mResponseType:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 246
    iget v0, p0, Landroid/telephony/SmsCbCmasInfo;->mSeverity:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 247
    iget v0, p0, Landroid/telephony/SmsCbCmasInfo;->mUrgency:I

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 248
    iget v0, p0, Landroid/telephony/SmsCbCmasInfo;->mCertainty:I

    #@1b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 250
    iget-wide v0, p0, Landroid/telephony/SmsCbCmasInfo;->mExpires:J

    #@20
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@23
    .line 252
    return-void
.end method
