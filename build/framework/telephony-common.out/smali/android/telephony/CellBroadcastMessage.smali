.class public Landroid/telephony/CellBroadcastMessage;
.super Ljava/lang/Object;
.source "CellBroadcastMessage.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/telephony/CellBroadcastMessage;",
            ">;"
        }
    .end annotation
.end field

.field public static final SMS_CB_MESSAGE_EXTRA:Ljava/lang/String; = "com.android.cellbroadcastreceiver.SMS_CB_MESSAGE"


# instance fields
.field private final mDeliveryTime:J

.field private mIsRead:Z

.field private final mSmsCbMessage:Landroid/telephony/SmsCbMessage;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 89
    new-instance v0, Landroid/telephony/CellBroadcastMessage$1;

    #@2
    invoke-direct {v0}, Landroid/telephony/CellBroadcastMessage$1;-><init>()V

    #@5
    sput-object v0, Landroid/telephony/CellBroadcastMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "in"

    #@0
    .prologue
    .line 72
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 73
    new-instance v0, Landroid/telephony/SmsCbMessage;

    #@5
    invoke-direct {v0, p1}, Landroid/telephony/SmsCbMessage;-><init>(Landroid/os/Parcel;)V

    #@8
    iput-object v0, p0, Landroid/telephony/CellBroadcastMessage;->mSmsCbMessage:Landroid/telephony/SmsCbMessage;

    #@a
    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@d
    move-result-wide v0

    #@e
    iput-wide v0, p0, Landroid/telephony/CellBroadcastMessage;->mDeliveryTime:J

    #@10
    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_1a

    #@16
    const/4 v0, 0x1

    #@17
    :goto_17
    iput-boolean v0, p0, Landroid/telephony/CellBroadcastMessage;->mIsRead:Z

    #@19
    .line 76
    return-void

    #@1a
    .line 75
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_17
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/telephony/CellBroadcastMessage$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/telephony/CellBroadcastMessage;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method public constructor <init>(Landroid/telephony/SmsCbMessage;)V
    .registers 4
    .parameter "message"

    #@0
    .prologue
    .line 60
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 61
    iput-object p1, p0, Landroid/telephony/CellBroadcastMessage;->mSmsCbMessage:Landroid/telephony/SmsCbMessage;

    #@5
    .line 62
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@8
    move-result-wide v0

    #@9
    iput-wide v0, p0, Landroid/telephony/CellBroadcastMessage;->mDeliveryTime:J

    #@b
    .line 63
    const/4 v0, 0x0

    #@c
    iput-boolean v0, p0, Landroid/telephony/CellBroadcastMessage;->mIsRead:Z

    #@e
    .line 64
    return-void
.end method

.method private constructor <init>(Landroid/telephony/SmsCbMessage;JZ)V
    .registers 5
    .parameter "message"
    .parameter "deliveryTime"
    .parameter "isRead"

    #@0
    .prologue
    .line 66
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 67
    iput-object p1, p0, Landroid/telephony/CellBroadcastMessage;->mSmsCbMessage:Landroid/telephony/SmsCbMessage;

    #@5
    .line 68
    iput-wide p2, p0, Landroid/telephony/CellBroadcastMessage;->mDeliveryTime:J

    #@7
    .line 69
    iput-boolean p4, p0, Landroid/telephony/CellBroadcastMessage;->mIsRead:Z

    #@9
    .line 70
    return-void
.end method

.method public static createFromCursor(Landroid/database/Cursor;)Landroid/telephony/CellBroadcastMessage;
    .registers 43
    .parameter "cursor"

    #@0
    .prologue
    .line 107
    const-string v22, "geo_scope"

    #@2
    move-object/from16 v0, p0

    #@4
    move-object/from16 v1, v22

    #@6
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@9
    move-result v22

    #@a
    move-object/from16 v0, p0

    #@c
    move/from16 v1, v22

    #@e
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    #@11
    move-result v14

    #@12
    .line 109
    .local v14, geoScope:I
    const-string v22, "serial_number"

    #@14
    move-object/from16 v0, p0

    #@16
    move-object/from16 v1, v22

    #@18
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@1b
    move-result v22

    #@1c
    move-object/from16 v0, p0

    #@1e
    move/from16 v1, v22

    #@20
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    #@23
    move-result v15

    #@24
    .line 111
    .local v15, serialNum:I
    const-string v22, "service_category"

    #@26
    move-object/from16 v0, p0

    #@28
    move-object/from16 v1, v22

    #@2a
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@2d
    move-result v22

    #@2e
    move-object/from16 v0, p0

    #@30
    move/from16 v1, v22

    #@32
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    #@35
    move-result v17

    #@36
    .line 113
    .local v17, category:I
    const-string v22, "language"

    #@38
    move-object/from16 v0, p0

    #@3a
    move-object/from16 v1, v22

    #@3c
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@3f
    move-result v22

    #@40
    move-object/from16 v0, p0

    #@42
    move/from16 v1, v22

    #@44
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@47
    move-result-object v18

    #@48
    .line 115
    .local v18, language:Ljava/lang/String;
    const-string v22, "body"

    #@4a
    move-object/from16 v0, p0

    #@4c
    move-object/from16 v1, v22

    #@4e
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@51
    move-result v22

    #@52
    move-object/from16 v0, p0

    #@54
    move/from16 v1, v22

    #@56
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@59
    move-result-object v19

    #@5a
    .line 117
    .local v19, body:Ljava/lang/String;
    const-string v22, "format"

    #@5c
    move-object/from16 v0, p0

    #@5e
    move-object/from16 v1, v22

    #@60
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@63
    move-result v22

    #@64
    move-object/from16 v0, p0

    #@66
    move/from16 v1, v22

    #@68
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    #@6b
    move-result v13

    #@6c
    .line 119
    .local v13, format:I
    const-string v22, "priority"

    #@6e
    move-object/from16 v0, p0

    #@70
    move-object/from16 v1, v22

    #@72
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@75
    move-result v22

    #@76
    move-object/from16 v0, p0

    #@78
    move/from16 v1, v22

    #@7a
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    #@7d
    move-result v20

    #@7e
    .line 123
    .local v20, priority:I
    const-string v22, "plmn"

    #@80
    move-object/from16 v0, p0

    #@82
    move-object/from16 v1, v22

    #@84
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@87
    move-result v38

    #@88
    .line 124
    .local v38, plmnColumn:I
    const/16 v22, -0x1

    #@8a
    move/from16 v0, v38

    #@8c
    move/from16 v1, v22

    #@8e
    if-eq v0, v1, :cond_248

    #@90
    move-object/from16 v0, p0

    #@92
    move/from16 v1, v38

    #@94
    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    #@97
    move-result v22

    #@98
    if-nez v22, :cond_248

    #@9a
    .line 125
    move-object/from16 v0, p0

    #@9c
    move/from16 v1, v38

    #@9e
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@a1
    move-result-object v37

    #@a2
    .line 131
    .local v37, plmn:Ljava/lang/String;
    :goto_a2
    const-string v22, "lac"

    #@a4
    move-object/from16 v0, p0

    #@a6
    move-object/from16 v1, v22

    #@a8
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@ab
    move-result v36

    #@ac
    .line 132
    .local v36, lacColumn:I
    const/16 v22, -0x1

    #@ae
    move/from16 v0, v36

    #@b0
    move/from16 v1, v22

    #@b2
    if-eq v0, v1, :cond_24c

    #@b4
    move-object/from16 v0, p0

    #@b6
    move/from16 v1, v36

    #@b8
    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    #@bb
    move-result v22

    #@bc
    if-nez v22, :cond_24c

    #@be
    .line 133
    move-object/from16 v0, p0

    #@c0
    move/from16 v1, v36

    #@c2
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    #@c5
    move-result v35

    #@c6
    .line 139
    .local v35, lac:I
    :goto_c6
    const-string v22, "cid"

    #@c8
    move-object/from16 v0, p0

    #@ca
    move-object/from16 v1, v22

    #@cc
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@cf
    move-result v24

    #@d0
    .line 140
    .local v24, cidColumn:I
    const/16 v22, -0x1

    #@d2
    move/from16 v0, v24

    #@d4
    move/from16 v1, v22

    #@d6
    if-eq v0, v1, :cond_250

    #@d8
    move-object/from16 v0, p0

    #@da
    move/from16 v1, v24

    #@dc
    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    #@df
    move-result v22

    #@e0
    if-nez v22, :cond_250

    #@e2
    .line 141
    move-object/from16 v0, p0

    #@e4
    move/from16 v1, v24

    #@e6
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    #@e9
    move-result v23

    #@ea
    .line 146
    .local v23, cid:I
    :goto_ea
    new-instance v16, Landroid/telephony/SmsCbLocation;

    #@ec
    move-object/from16 v0, v16

    #@ee
    move-object/from16 v1, v37

    #@f0
    move/from16 v2, v35

    #@f2
    move/from16 v3, v23

    #@f4
    invoke-direct {v0, v1, v2, v3}, Landroid/telephony/SmsCbLocation;-><init>(Ljava/lang/String;II)V

    #@f7
    .line 149
    .local v16, location:Landroid/telephony/SmsCbLocation;
    const-string v22, "etws_warning_type"

    #@f9
    move-object/from16 v0, p0

    #@fb
    move-object/from16 v1, v22

    #@fd
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@100
    move-result v33

    #@101
    .line 151
    .local v33, etwsWarningTypeColumn:I
    const/16 v22, -0x1

    #@103
    move/from16 v0, v33

    #@105
    move/from16 v1, v22

    #@107
    if-eq v0, v1, :cond_254

    #@109
    move-object/from16 v0, p0

    #@10b
    move/from16 v1, v33

    #@10d
    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    #@110
    move-result v22

    #@111
    if-nez v22, :cond_254

    #@113
    .line 152
    move-object/from16 v0, p0

    #@115
    move/from16 v1, v33

    #@117
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    #@11a
    move-result v39

    #@11b
    .line 153
    .local v39, warningType:I
    new-instance v21, Landroid/telephony/SmsCbEtwsInfo;

    #@11d
    const/16 v22, 0x0

    #@11f
    const/16 v40, 0x0

    #@121
    const/16 v41, 0x0

    #@123
    move-object/from16 v0, v21

    #@125
    move/from16 v1, v39

    #@127
    move/from16 v2, v22

    #@129
    move/from16 v3, v40

    #@12b
    move-object/from16 v4, v41

    #@12d
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/telephony/SmsCbEtwsInfo;-><init>(IZZ[B)V

    #@130
    .line 159
    .end local v39           #warningType:I
    .local v21, etwsInfo:Landroid/telephony/SmsCbEtwsInfo;
    :goto_130
    const-string v22, "cmas_message_class"

    #@132
    move-object/from16 v0, p0

    #@134
    move-object/from16 v1, v22

    #@136
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@139
    move-result v27

    #@13a
    .line 161
    .local v27, cmasMessageClassColumn:I
    const/16 v22, -0x1

    #@13c
    move/from16 v0, v27

    #@13e
    move/from16 v1, v22

    #@140
    if-eq v0, v1, :cond_265

    #@142
    move-object/from16 v0, p0

    #@144
    move/from16 v1, v27

    #@146
    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    #@149
    move-result v22

    #@14a
    if-nez v22, :cond_265

    #@14c
    .line 162
    move-object/from16 v0, p0

    #@14e
    move/from16 v1, v27

    #@150
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    #@153
    move-result v6

    #@154
    .line 165
    .local v6, messageClass:I
    const-string v22, "cmas_category"

    #@156
    move-object/from16 v0, p0

    #@158
    move-object/from16 v1, v22

    #@15a
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@15d
    move-result v25

    #@15e
    .line 167
    .local v25, cmasCategoryColumn:I
    const/16 v22, -0x1

    #@160
    move/from16 v0, v25

    #@162
    move/from16 v1, v22

    #@164
    if-eq v0, v1, :cond_258

    #@166
    move-object/from16 v0, p0

    #@168
    move/from16 v1, v25

    #@16a
    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    #@16d
    move-result v22

    #@16e
    if-nez v22, :cond_258

    #@170
    .line 168
    move-object/from16 v0, p0

    #@172
    move/from16 v1, v25

    #@174
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    #@177
    move-result v7

    #@178
    .line 174
    .local v7, cmasCategory:I
    :goto_178
    const-string v22, "cmas_response_type"

    #@17a
    move-object/from16 v0, p0

    #@17c
    move-object/from16 v1, v22

    #@17e
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@181
    move-result v28

    #@182
    .line 176
    .local v28, cmasResponseTypeColumn:I
    const/16 v22, -0x1

    #@184
    move/from16 v0, v28

    #@186
    move/from16 v1, v22

    #@188
    if-eq v0, v1, :cond_25b

    #@18a
    move-object/from16 v0, p0

    #@18c
    move/from16 v1, v28

    #@18e
    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    #@191
    move-result v22

    #@192
    if-nez v22, :cond_25b

    #@194
    .line 177
    move-object/from16 v0, p0

    #@196
    move/from16 v1, v28

    #@198
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    #@19b
    move-result v8

    #@19c
    .line 183
    .local v8, responseType:I
    :goto_19c
    const-string v22, "cmas_severity"

    #@19e
    move-object/from16 v0, p0

    #@1a0
    move-object/from16 v1, v22

    #@1a2
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@1a5
    move-result v29

    #@1a6
    .line 185
    .local v29, cmasSeverityColumn:I
    const/16 v22, -0x1

    #@1a8
    move/from16 v0, v29

    #@1aa
    move/from16 v1, v22

    #@1ac
    if-eq v0, v1, :cond_25e

    #@1ae
    move-object/from16 v0, p0

    #@1b0
    move/from16 v1, v29

    #@1b2
    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    #@1b5
    move-result v22

    #@1b6
    if-nez v22, :cond_25e

    #@1b8
    .line 186
    move-object/from16 v0, p0

    #@1ba
    move/from16 v1, v29

    #@1bc
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    #@1bf
    move-result v9

    #@1c0
    .line 192
    .local v9, severity:I
    :goto_1c0
    const-string v22, "cmas_urgency"

    #@1c2
    move-object/from16 v0, p0

    #@1c4
    move-object/from16 v1, v22

    #@1c6
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@1c9
    move-result v30

    #@1ca
    .line 194
    .local v30, cmasUrgencyColumn:I
    const/16 v22, -0x1

    #@1cc
    move/from16 v0, v30

    #@1ce
    move/from16 v1, v22

    #@1d0
    if-eq v0, v1, :cond_261

    #@1d2
    move-object/from16 v0, p0

    #@1d4
    move/from16 v1, v30

    #@1d6
    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    #@1d9
    move-result v22

    #@1da
    if-nez v22, :cond_261

    #@1dc
    .line 195
    move-object/from16 v0, p0

    #@1de
    move/from16 v1, v30

    #@1e0
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    #@1e3
    move-result v10

    #@1e4
    .line 201
    .local v10, urgency:I
    :goto_1e4
    const-string v22, "cmas_certainty"

    #@1e6
    move-object/from16 v0, p0

    #@1e8
    move-object/from16 v1, v22

    #@1ea
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@1ed
    move-result v26

    #@1ee
    .line 203
    .local v26, cmasCertaintyColumn:I
    const/16 v22, -0x1

    #@1f0
    move/from16 v0, v26

    #@1f2
    move/from16 v1, v22

    #@1f4
    if-eq v0, v1, :cond_263

    #@1f6
    move-object/from16 v0, p0

    #@1f8
    move/from16 v1, v26

    #@1fa
    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    #@1fd
    move-result v22

    #@1fe
    if-nez v22, :cond_263

    #@200
    .line 204
    move-object/from16 v0, p0

    #@202
    move/from16 v1, v26

    #@204
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    #@207
    move-result v11

    #@208
    .line 209
    .local v11, certainty:I
    :goto_208
    new-instance v5, Landroid/telephony/SmsCbCmasInfo;

    #@20a
    invoke-direct/range {v5 .. v11}, Landroid/telephony/SmsCbCmasInfo;-><init>(IIIIII)V

    #@20d
    .line 215
    .end local v6           #messageClass:I
    .end local v7           #cmasCategory:I
    .end local v8           #responseType:I
    .end local v9           #severity:I
    .end local v10           #urgency:I
    .end local v11           #certainty:I
    .end local v25           #cmasCategoryColumn:I
    .end local v26           #cmasCertaintyColumn:I
    .end local v28           #cmasResponseTypeColumn:I
    .end local v29           #cmasSeverityColumn:I
    .end local v30           #cmasUrgencyColumn:I
    .local v5, cmasInfo:Landroid/telephony/SmsCbCmasInfo;
    :goto_20d
    new-instance v12, Landroid/telephony/SmsCbMessage;

    #@20f
    move-object/from16 v22, v5

    #@211
    invoke-direct/range {v12 .. v22}, Landroid/telephony/SmsCbMessage;-><init>(IIILandroid/telephony/SmsCbLocation;ILjava/lang/String;Ljava/lang/String;ILandroid/telephony/SmsCbEtwsInfo;Landroid/telephony/SmsCbCmasInfo;)V

    #@214
    .line 218
    .local v12, msg:Landroid/telephony/SmsCbMessage;
    const-string v22, "date"

    #@216
    move-object/from16 v0, p0

    #@218
    move-object/from16 v1, v22

    #@21a
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@21d
    move-result v22

    #@21e
    move-object/from16 v0, p0

    #@220
    move/from16 v1, v22

    #@222
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    #@225
    move-result-wide v31

    #@226
    .line 220
    .local v31, deliveryTime:J
    const-string v22, "read"

    #@228
    move-object/from16 v0, p0

    #@22a
    move-object/from16 v1, v22

    #@22c
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@22f
    move-result v22

    #@230
    move-object/from16 v0, p0

    #@232
    move/from16 v1, v22

    #@234
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    #@237
    move-result v22

    #@238
    if-eqz v22, :cond_267

    #@23a
    const/16 v34, 0x1

    #@23c
    .line 223
    .local v34, isRead:Z
    :goto_23c
    new-instance v22, Landroid/telephony/CellBroadcastMessage;

    #@23e
    move-object/from16 v0, v22

    #@240
    move-wide/from16 v1, v31

    #@242
    move/from16 v3, v34

    #@244
    invoke-direct {v0, v12, v1, v2, v3}, Landroid/telephony/CellBroadcastMessage;-><init>(Landroid/telephony/SmsCbMessage;JZ)V

    #@247
    return-object v22

    #@248
    .line 127
    .end local v5           #cmasInfo:Landroid/telephony/SmsCbCmasInfo;
    .end local v12           #msg:Landroid/telephony/SmsCbMessage;
    .end local v16           #location:Landroid/telephony/SmsCbLocation;
    .end local v21           #etwsInfo:Landroid/telephony/SmsCbEtwsInfo;
    .end local v23           #cid:I
    .end local v24           #cidColumn:I
    .end local v27           #cmasMessageClassColumn:I
    .end local v31           #deliveryTime:J
    .end local v33           #etwsWarningTypeColumn:I
    .end local v34           #isRead:Z
    .end local v35           #lac:I
    .end local v36           #lacColumn:I
    .end local v37           #plmn:Ljava/lang/String;
    :cond_248
    const/16 v37, 0x0

    #@24a
    .restart local v37       #plmn:Ljava/lang/String;
    goto/16 :goto_a2

    #@24c
    .line 135
    .restart local v36       #lacColumn:I
    :cond_24c
    const/16 v35, -0x1

    #@24e
    .restart local v35       #lac:I
    goto/16 :goto_c6

    #@250
    .line 143
    .restart local v24       #cidColumn:I
    :cond_250
    const/16 v23, -0x1

    #@252
    .restart local v23       #cid:I
    goto/16 :goto_ea

    #@254
    .line 155
    .restart local v16       #location:Landroid/telephony/SmsCbLocation;
    .restart local v33       #etwsWarningTypeColumn:I
    :cond_254
    const/16 v21, 0x0

    #@256
    .restart local v21       #etwsInfo:Landroid/telephony/SmsCbEtwsInfo;
    goto/16 :goto_130

    #@258
    .line 170
    .restart local v6       #messageClass:I
    .restart local v25       #cmasCategoryColumn:I
    .restart local v27       #cmasMessageClassColumn:I
    :cond_258
    const/4 v7, -0x1

    #@259
    .restart local v7       #cmasCategory:I
    goto/16 :goto_178

    #@25b
    .line 179
    .restart local v28       #cmasResponseTypeColumn:I
    :cond_25b
    const/4 v8, -0x1

    #@25c
    .restart local v8       #responseType:I
    goto/16 :goto_19c

    #@25e
    .line 188
    .restart local v29       #cmasSeverityColumn:I
    :cond_25e
    const/4 v9, -0x1

    #@25f
    .restart local v9       #severity:I
    goto/16 :goto_1c0

    #@261
    .line 197
    .restart local v30       #cmasUrgencyColumn:I
    :cond_261
    const/4 v10, -0x1

    #@262
    .restart local v10       #urgency:I
    goto :goto_1e4

    #@263
    .line 206
    .restart local v26       #cmasCertaintyColumn:I
    :cond_263
    const/4 v11, -0x1

    #@264
    .restart local v11       #certainty:I
    goto :goto_208

    #@265
    .line 212
    .end local v6           #messageClass:I
    .end local v7           #cmasCategory:I
    .end local v8           #responseType:I
    .end local v9           #severity:I
    .end local v10           #urgency:I
    .end local v11           #certainty:I
    .end local v25           #cmasCategoryColumn:I
    .end local v26           #cmasCertaintyColumn:I
    .end local v28           #cmasResponseTypeColumn:I
    .end local v29           #cmasSeverityColumn:I
    .end local v30           #cmasUrgencyColumn:I
    :cond_265
    const/4 v5, 0x0

    #@266
    .restart local v5       #cmasInfo:Landroid/telephony/SmsCbCmasInfo;
    goto :goto_20d

    #@267
    .line 220
    .restart local v12       #msg:Landroid/telephony/SmsCbMessage;
    .restart local v31       #deliveryTime:J
    :cond_267
    const/16 v34, 0x0

    #@269
    goto :goto_23c
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 80
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getCmasMessageClass()I
    .registers 2

    #@0
    .prologue
    .line 364
    iget-object v0, p0, Landroid/telephony/CellBroadcastMessage;->mSmsCbMessage:Landroid/telephony/SmsCbMessage;

    #@2
    invoke-virtual {v0}, Landroid/telephony/SmsCbMessage;->isCmasMessage()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_13

    #@8
    .line 365
    iget-object v0, p0, Landroid/telephony/CellBroadcastMessage;->mSmsCbMessage:Landroid/telephony/SmsCbMessage;

    #@a
    invoke-virtual {v0}, Landroid/telephony/SmsCbMessage;->getCmasWarningInfo()Landroid/telephony/SmsCbCmasInfo;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {v0}, Landroid/telephony/SmsCbCmasInfo;->getMessageClass()I

    #@11
    move-result v0

    #@12
    .line 367
    :goto_12
    return v0

    #@13
    :cond_13
    const/4 v0, -0x1

    #@14
    goto :goto_12
.end method

.method public getCmasWarningInfo()Landroid/telephony/SmsCbCmasInfo;
    .registers 2

    #@0
    .prologue
    .line 304
    iget-object v0, p0, Landroid/telephony/CellBroadcastMessage;->mSmsCbMessage:Landroid/telephony/SmsCbMessage;

    #@2
    invoke-virtual {v0}, Landroid/telephony/SmsCbMessage;->getCmasWarningInfo()Landroid/telephony/SmsCbCmasInfo;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getContentValues()Landroid/content/ContentValues;
    .registers 9

    #@0
    .prologue
    const/4 v7, -0x1

    #@1
    .line 231
    new-instance v1, Landroid/content/ContentValues;

    #@3
    const/16 v5, 0x10

    #@5
    invoke-direct {v1, v5}, Landroid/content/ContentValues;-><init>(I)V

    #@8
    .line 232
    .local v1, cv:Landroid/content/ContentValues;
    iget-object v4, p0, Landroid/telephony/CellBroadcastMessage;->mSmsCbMessage:Landroid/telephony/SmsCbMessage;

    #@a
    .line 233
    .local v4, msg:Landroid/telephony/SmsCbMessage;
    const-string v5, "geo_scope"

    #@c
    invoke-virtual {v4}, Landroid/telephony/SmsCbMessage;->getGeographicalScope()I

    #@f
    move-result v6

    #@10
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13
    move-result-object v6

    #@14
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@17
    .line 234
    invoke-virtual {v4}, Landroid/telephony/SmsCbMessage;->getLocation()Landroid/telephony/SmsCbLocation;

    #@1a
    move-result-object v3

    #@1b
    .line 235
    .local v3, location:Landroid/telephony/SmsCbLocation;
    invoke-virtual {v3}, Landroid/telephony/SmsCbLocation;->getPlmn()Ljava/lang/String;

    #@1e
    move-result-object v5

    #@1f
    if-eqz v5, :cond_2a

    #@21
    .line 236
    const-string v5, "plmn"

    #@23
    invoke-virtual {v3}, Landroid/telephony/SmsCbLocation;->getPlmn()Ljava/lang/String;

    #@26
    move-result-object v6

    #@27
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2a
    .line 238
    :cond_2a
    invoke-virtual {v3}, Landroid/telephony/SmsCbLocation;->getLac()I

    #@2d
    move-result v5

    #@2e
    if-eq v5, v7, :cond_3d

    #@30
    .line 239
    const-string v5, "lac"

    #@32
    invoke-virtual {v3}, Landroid/telephony/SmsCbLocation;->getLac()I

    #@35
    move-result v6

    #@36
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@39
    move-result-object v6

    #@3a
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@3d
    .line 241
    :cond_3d
    invoke-virtual {v3}, Landroid/telephony/SmsCbLocation;->getCid()I

    #@40
    move-result v5

    #@41
    if-eq v5, v7, :cond_50

    #@43
    .line 242
    const-string v5, "cid"

    #@45
    invoke-virtual {v3}, Landroid/telephony/SmsCbLocation;->getCid()I

    #@48
    move-result v6

    #@49
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4c
    move-result-object v6

    #@4d
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@50
    .line 244
    :cond_50
    const-string v5, "serial_number"

    #@52
    invoke-virtual {v4}, Landroid/telephony/SmsCbMessage;->getSerialNumber()I

    #@55
    move-result v6

    #@56
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@59
    move-result-object v6

    #@5a
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@5d
    .line 245
    const-string v5, "service_category"

    #@5f
    invoke-virtual {v4}, Landroid/telephony/SmsCbMessage;->getServiceCategory()I

    #@62
    move-result v6

    #@63
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@66
    move-result-object v6

    #@67
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@6a
    .line 246
    const-string v5, "language"

    #@6c
    invoke-virtual {v4}, Landroid/telephony/SmsCbMessage;->getLanguageCode()Ljava/lang/String;

    #@6f
    move-result-object v6

    #@70
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@73
    .line 247
    const-string v5, "body"

    #@75
    invoke-virtual {v4}, Landroid/telephony/SmsCbMessage;->getMessageBody()Ljava/lang/String;

    #@78
    move-result-object v6

    #@79
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@7c
    .line 248
    const-string v5, "date"

    #@7e
    iget-wide v6, p0, Landroid/telephony/CellBroadcastMessage;->mDeliveryTime:J

    #@80
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@83
    move-result-object v6

    #@84
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@87
    .line 249
    const-string v5, "read"

    #@89
    iget-boolean v6, p0, Landroid/telephony/CellBroadcastMessage;->mIsRead:Z

    #@8b
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@8e
    move-result-object v6

    #@8f
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@92
    .line 250
    const-string v5, "format"

    #@94
    invoke-virtual {v4}, Landroid/telephony/SmsCbMessage;->getMessageFormat()I

    #@97
    move-result v6

    #@98
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9b
    move-result-object v6

    #@9c
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@9f
    .line 251
    const-string v5, "priority"

    #@a1
    invoke-virtual {v4}, Landroid/telephony/SmsCbMessage;->getMessagePriority()I

    #@a4
    move-result v6

    #@a5
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a8
    move-result-object v6

    #@a9
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@ac
    .line 253
    iget-object v5, p0, Landroid/telephony/CellBroadcastMessage;->mSmsCbMessage:Landroid/telephony/SmsCbMessage;

    #@ae
    invoke-virtual {v5}, Landroid/telephony/SmsCbMessage;->getEtwsWarningInfo()Landroid/telephony/SmsCbEtwsInfo;

    #@b1
    move-result-object v2

    #@b2
    .line 254
    .local v2, etwsInfo:Landroid/telephony/SmsCbEtwsInfo;
    if-eqz v2, :cond_c1

    #@b4
    .line 255
    const-string v5, "etws_warning_type"

    #@b6
    invoke-virtual {v2}, Landroid/telephony/SmsCbEtwsInfo;->getWarningType()I

    #@b9
    move-result v6

    #@ba
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@bd
    move-result-object v6

    #@be
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@c1
    .line 258
    :cond_c1
    iget-object v5, p0, Landroid/telephony/CellBroadcastMessage;->mSmsCbMessage:Landroid/telephony/SmsCbMessage;

    #@c3
    invoke-virtual {v5}, Landroid/telephony/SmsCbMessage;->getCmasWarningInfo()Landroid/telephony/SmsCbCmasInfo;

    #@c6
    move-result-object v0

    #@c7
    .line 259
    .local v0, cmasInfo:Landroid/telephony/SmsCbCmasInfo;
    if-eqz v0, :cond_117

    #@c9
    .line 260
    const-string v5, "cmas_message_class"

    #@cb
    invoke-virtual {v0}, Landroid/telephony/SmsCbCmasInfo;->getMessageClass()I

    #@ce
    move-result v6

    #@cf
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d2
    move-result-object v6

    #@d3
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@d6
    .line 261
    const-string v5, "cmas_category"

    #@d8
    invoke-virtual {v0}, Landroid/telephony/SmsCbCmasInfo;->getCategory()I

    #@db
    move-result v6

    #@dc
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@df
    move-result-object v6

    #@e0
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@e3
    .line 262
    const-string v5, "cmas_response_type"

    #@e5
    invoke-virtual {v0}, Landroid/telephony/SmsCbCmasInfo;->getResponseType()I

    #@e8
    move-result v6

    #@e9
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ec
    move-result-object v6

    #@ed
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@f0
    .line 263
    const-string v5, "cmas_severity"

    #@f2
    invoke-virtual {v0}, Landroid/telephony/SmsCbCmasInfo;->getSeverity()I

    #@f5
    move-result v6

    #@f6
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f9
    move-result-object v6

    #@fa
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@fd
    .line 264
    const-string v5, "cmas_urgency"

    #@ff
    invoke-virtual {v0}, Landroid/telephony/SmsCbCmasInfo;->getUrgency()I

    #@102
    move-result v6

    #@103
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@106
    move-result-object v6

    #@107
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@10a
    .line 265
    const-string v5, "cmas_certainty"

    #@10c
    invoke-virtual {v0}, Landroid/telephony/SmsCbCmasInfo;->getCertainty()I

    #@10f
    move-result v6

    #@110
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@113
    move-result-object v6

    #@114
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@117
    .line 268
    :cond_117
    return-object v1
.end method

.method public getDateString(Landroid/content/Context;)Ljava/lang/String;
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 407
    const v0, 0x80b11

    #@3
    .line 410
    .local v0, flags:I
    iget-wide v1, p0, Landroid/telephony/CellBroadcastMessage;->mDeliveryTime:J

    #@5
    invoke-static {p1, v1, v2, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    return-object v1
.end method

.method public getDeliveryTime()J
    .registers 3

    #@0
    .prologue
    .line 288
    iget-wide v0, p0, Landroid/telephony/CellBroadcastMessage;->mDeliveryTime:J

    #@2
    return-wide v0
.end method

.method public getEtwsWarningInfo()Landroid/telephony/SmsCbEtwsInfo;
    .registers 2

    #@0
    .prologue
    .line 308
    iget-object v0, p0, Landroid/telephony/CellBroadcastMessage;->mSmsCbMessage:Landroid/telephony/SmsCbMessage;

    #@2
    invoke-virtual {v0}, Landroid/telephony/SmsCbMessage;->getEtwsWarningInfo()Landroid/telephony/SmsCbEtwsInfo;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getLanguageCode()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 280
    iget-object v0, p0, Landroid/telephony/CellBroadcastMessage;->mSmsCbMessage:Landroid/telephony/SmsCbMessage;

    #@2
    invoke-virtual {v0}, Landroid/telephony/SmsCbMessage;->getLanguageCode()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getMessageBody()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 292
    iget-object v0, p0, Landroid/telephony/CellBroadcastMessage;->mSmsCbMessage:Landroid/telephony/SmsCbMessage;

    #@2
    invoke-virtual {v0}, Landroid/telephony/SmsCbMessage;->getMessageBody()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getSerialNumber()I
    .registers 2

    #@0
    .prologue
    .line 300
    iget-object v0, p0, Landroid/telephony/CellBroadcastMessage;->mSmsCbMessage:Landroid/telephony/SmsCbMessage;

    #@2
    invoke-virtual {v0}, Landroid/telephony/SmsCbMessage;->getSerialNumber()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getServiceCategory()I
    .registers 2

    #@0
    .prologue
    .line 284
    iget-object v0, p0, Landroid/telephony/CellBroadcastMessage;->mSmsCbMessage:Landroid/telephony/SmsCbMessage;

    #@2
    invoke-virtual {v0}, Landroid/telephony/SmsCbMessage;->getServiceCategory()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getSpokenDateString(Landroid/content/Context;)Ljava/lang/String;
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 419
    const/16 v0, 0x11

    #@2
    .line 420
    .local v0, flags:I
    iget-wide v1, p0, Landroid/telephony/CellBroadcastMessage;->mDeliveryTime:J

    #@4
    invoke-static {p1, v1, v2, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    return-object v1
.end method

.method public isCmasMessage()Z
    .registers 2

    #@0
    .prologue
    .line 355
    iget-object v0, p0, Landroid/telephony/CellBroadcastMessage;->mSmsCbMessage:Landroid/telephony/SmsCbMessage;

    #@2
    invoke-virtual {v0}, Landroid/telephony/SmsCbMessage;->isCmasMessage()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isEmergencyAlertMessage()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 331
    iget-object v2, p0, Landroid/telephony/CellBroadcastMessage;->mSmsCbMessage:Landroid/telephony/SmsCbMessage;

    #@3
    invoke-virtual {v2}, Landroid/telephony/SmsCbMessage;->isEmergencyMessage()Z

    #@6
    move-result v2

    #@7
    if-nez v2, :cond_a

    #@9
    .line 339
    :cond_9
    :goto_9
    return v1

    #@a
    .line 334
    :cond_a
    iget-object v2, p0, Landroid/telephony/CellBroadcastMessage;->mSmsCbMessage:Landroid/telephony/SmsCbMessage;

    #@c
    invoke-virtual {v2}, Landroid/telephony/SmsCbMessage;->getCmasWarningInfo()Landroid/telephony/SmsCbCmasInfo;

    #@f
    move-result-object v0

    #@10
    .line 335
    .local v0, cmasInfo:Landroid/telephony/SmsCbCmasInfo;
    if-eqz v0, :cond_19

    #@12
    invoke-virtual {v0}, Landroid/telephony/SmsCbCmasInfo;->getMessageClass()I

    #@15
    move-result v2

    #@16
    const/4 v3, 0x3

    #@17
    if-eq v2, v3, :cond_9

    #@19
    .line 339
    :cond_19
    const/4 v1, 0x1

    #@1a
    goto :goto_9
.end method

.method public isEtwsEmergencyUserAlert()Z
    .registers 3

    #@0
    .prologue
    .line 387
    iget-object v1, p0, Landroid/telephony/CellBroadcastMessage;->mSmsCbMessage:Landroid/telephony/SmsCbMessage;

    #@2
    invoke-virtual {v1}, Landroid/telephony/SmsCbMessage;->getEtwsWarningInfo()Landroid/telephony/SmsCbEtwsInfo;

    #@5
    move-result-object v0

    #@6
    .line 388
    .local v0, etwsInfo:Landroid/telephony/SmsCbEtwsInfo;
    if-eqz v0, :cond_10

    #@8
    invoke-virtual {v0}, Landroid/telephony/SmsCbEtwsInfo;->isEmergencyUserAlert()Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_10

    #@e
    const/4 v1, 0x1

    #@f
    :goto_f
    return v1

    #@10
    :cond_10
    const/4 v1, 0x0

    #@11
    goto :goto_f
.end method

.method public isEtwsMessage()Z
    .registers 2

    #@0
    .prologue
    .line 347
    iget-object v0, p0, Landroid/telephony/CellBroadcastMessage;->mSmsCbMessage:Landroid/telephony/SmsCbMessage;

    #@2
    invoke-virtual {v0}, Landroid/telephony/SmsCbMessage;->isEtwsMessage()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isEtwsPopupAlert()Z
    .registers 3

    #@0
    .prologue
    .line 377
    iget-object v1, p0, Landroid/telephony/CellBroadcastMessage;->mSmsCbMessage:Landroid/telephony/SmsCbMessage;

    #@2
    invoke-virtual {v1}, Landroid/telephony/SmsCbMessage;->getEtwsWarningInfo()Landroid/telephony/SmsCbEtwsInfo;

    #@5
    move-result-object v0

    #@6
    .line 378
    .local v0, etwsInfo:Landroid/telephony/SmsCbEtwsInfo;
    if-eqz v0, :cond_10

    #@8
    invoke-virtual {v0}, Landroid/telephony/SmsCbEtwsInfo;->isPopupAlert()Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_10

    #@e
    const/4 v1, 0x1

    #@f
    :goto_f
    return v1

    #@10
    :cond_10
    const/4 v1, 0x0

    #@11
    goto :goto_f
.end method

.method public isEtwsTestMessage()Z
    .registers 4

    #@0
    .prologue
    .line 396
    iget-object v1, p0, Landroid/telephony/CellBroadcastMessage;->mSmsCbMessage:Landroid/telephony/SmsCbMessage;

    #@2
    invoke-virtual {v1}, Landroid/telephony/SmsCbMessage;->getEtwsWarningInfo()Landroid/telephony/SmsCbEtwsInfo;

    #@5
    move-result-object v0

    #@6
    .line 397
    .local v0, etwsInfo:Landroid/telephony/SmsCbEtwsInfo;
    if-eqz v0, :cond_11

    #@8
    invoke-virtual {v0}, Landroid/telephony/SmsCbEtwsInfo;->getWarningType()I

    #@b
    move-result v1

    #@c
    const/4 v2, 0x3

    #@d
    if-ne v1, v2, :cond_11

    #@f
    const/4 v1, 0x1

    #@10
    :goto_10
    return v1

    #@11
    :cond_11
    const/4 v1, 0x0

    #@12
    goto :goto_10
.end method

.method public isPublicAlertMessage()Z
    .registers 2

    #@0
    .prologue
    .line 321
    iget-object v0, p0, Landroid/telephony/CellBroadcastMessage;->mSmsCbMessage:Landroid/telephony/SmsCbMessage;

    #@2
    invoke-virtual {v0}, Landroid/telephony/SmsCbMessage;->isEmergencyMessage()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isRead()Z
    .registers 2

    #@0
    .prologue
    .line 296
    iget-boolean v0, p0, Landroid/telephony/CellBroadcastMessage;->mIsRead:Z

    #@2
    return v0
.end method

.method public setIsRead(Z)V
    .registers 2
    .parameter "isRead"

    #@0
    .prologue
    .line 276
    iput-boolean p1, p0, Landroid/telephony/CellBroadcastMessage;->mIsRead:Z

    #@2
    .line 277
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    .line 84
    iget-object v0, p0, Landroid/telephony/CellBroadcastMessage;->mSmsCbMessage:Landroid/telephony/SmsCbMessage;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/telephony/SmsCbMessage;->writeToParcel(Landroid/os/Parcel;I)V

    #@5
    .line 85
    iget-wide v0, p0, Landroid/telephony/CellBroadcastMessage;->mDeliveryTime:J

    #@7
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@a
    .line 86
    iget-boolean v0, p0, Landroid/telephony/CellBroadcastMessage;->mIsRead:Z

    #@c
    if-eqz v0, :cond_13

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 87
    return-void

    #@13
    .line 86
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_f
.end method
