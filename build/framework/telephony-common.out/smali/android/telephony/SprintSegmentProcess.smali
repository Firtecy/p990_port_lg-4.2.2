.class public Landroid/telephony/SprintSegmentProcess;
.super Ljava/lang/Object;
.source "SprintSegmentProcess.java"


# static fields
.field public static final SEG_DASH:C = '-'

.field public static final SEG_SPACE:C = ' '


# instance fields
.field private EmailLen:I

.field public codeUnitSize:I

.field private mSegmentedString:Ljava/lang/StringBuilder;

.field private final mSmileyTexts:[Ljava/lang/String;

.field private msegmentPosInfo:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private segInsertPos:I

.field private textLen:I


# direct methods
.method constructor <init>(II)V
    .registers 5
    .parameter "bodylen"
    .parameter "emailLen"

    #@0
    .prologue
    .line 29
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 17
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    iput-object v1, p0, Landroid/telephony/SprintSegmentProcess;->mSegmentedString:Ljava/lang/StringBuilder;

    #@a
    .line 19
    const/4 v1, 0x0

    #@b
    iput v1, p0, Landroid/telephony/SprintSegmentProcess;->segInsertPos:I

    #@d
    .line 30
    iput p1, p0, Landroid/telephony/SprintSegmentProcess;->textLen:I

    #@f
    .line 31
    iput p2, p0, Landroid/telephony/SprintSegmentProcess;->EmailLen:I

    #@11
    .line 32
    new-instance v1, Ljava/util/HashMap;

    #@13
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@16
    iput-object v1, p0, Landroid/telephony/SprintSegmentProcess;->msegmentPosInfo:Ljava/util/HashMap;

    #@18
    .line 34
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@1b
    move-result-object v0

    #@1c
    .line 35
    .local v0, rsrc:Landroid/content/res/Resources;
    const v1, 0x1070009

    #@1f
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    iput-object v1, p0, Landroid/telephony/SprintSegmentProcess;->mSmileyTexts:[Ljava/lang/String;

    #@25
    .line 36
    return-void
.end method

.method private checkBoundSmiley(I)I
    .registers 10
    .parameter "pos"

    #@0
    .prologue
    .line 110
    move v2, p1

    #@1
    .line 111
    .local v2, modifidPos:I
    const/4 v1, 0x0

    #@2
    .line 112
    .local v1, lenOfSmiley:I
    const/4 v0, 0x0

    #@3
    .line 114
    .local v0, cmpStPos:I
    iget-object v6, p0, Landroid/telephony/SprintSegmentProcess;->mSmileyTexts:[Ljava/lang/String;

    #@5
    array-length v6, v6

    #@6
    add-int/lit8 v5, v6, -0x1

    #@8
    .local v5, smileyNum:I
    :goto_8
    if-ltz v5, :cond_3d

    #@a
    .line 116
    iget-object v6, p0, Landroid/telephony/SprintSegmentProcess;->mSmileyTexts:[Ljava/lang/String;

    #@c
    aget-object v6, v6, v5

    #@e
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@11
    move-result v1

    #@12
    .line 117
    const/4 v4, 0x1

    #@13
    .local v4, smileyLen:I
    :goto_13
    if-ge v4, v1, :cond_3a

    #@15
    .line 119
    sub-int v0, p1, v4

    #@17
    .line 120
    iget-object v6, p0, Landroid/telephony/SprintSegmentProcess;->mSegmentedString:Ljava/lang/StringBuilder;

    #@19
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    #@1c
    move-result v6

    #@1d
    add-int v7, v0, v1

    #@1f
    if-lt v6, v7, :cond_37

    #@21
    .line 122
    iget-object v6, p0, Landroid/telephony/SprintSegmentProcess;->mSegmentedString:Ljava/lang/StringBuilder;

    #@23
    add-int v7, v0, v1

    #@25
    invoke-virtual {v6, v0, v7}, Ljava/lang/StringBuilder;->subSequence(II)Ljava/lang/CharSequence;

    #@28
    move-result-object v6

    #@29
    iget-object v7, p0, Landroid/telephony/SprintSegmentProcess;->mSmileyTexts:[Ljava/lang/String;

    #@2b
    aget-object v7, v7, v5

    #@2d
    invoke-virtual {v6, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v6

    #@31
    if-eqz v6, :cond_37

    #@33
    .line 124
    sub-int v2, p1, v4

    #@35
    move v3, v2

    #@36
    .line 132
    .end local v2           #modifidPos:I
    .end local v4           #smileyLen:I
    .local v3, modifidPos:I
    :goto_36
    return v3

    #@37
    .line 117
    .end local v3           #modifidPos:I
    .restart local v2       #modifidPos:I
    .restart local v4       #smileyLen:I
    :cond_37
    add-int/lit8 v4, v4, 0x1

    #@39
    goto :goto_13

    #@3a
    .line 114
    :cond_3a
    add-int/lit8 v5, v5, -0x1

    #@3c
    goto :goto_8

    #@3d
    .end local v4           #smileyLen:I
    :cond_3d
    move v3, v2

    #@3e
    .line 132
    .end local v2           #modifidPos:I
    .restart local v3       #modifidPos:I
    goto :goto_36
.end method

.method public static countAsciiSeptets(Ljava/lang/CharSequence;)I
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 79
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@4
    move-result v1

    #@5
    .line 80
    .local v1, msgLen:I
    const/4 v0, 0x0

    #@6
    .local v0, i:I
    :goto_6
    if-ge v0, v1, :cond_15

    #@8
    .line 81
    sget-object v3, Lcom/android/internal/telephony/cdma/sms/UserData;->charToAscii:Landroid/util/SparseIntArray;

    #@a
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    #@d
    move-result v4

    #@e
    invoke-virtual {v3, v4, v2}, Landroid/util/SparseIntArray;->get(II)I

    #@11
    move-result v3

    #@12
    if-ne v3, v2, :cond_16

    #@14
    move v1, v2

    #@15
    .line 85
    .end local v1           #msgLen:I
    :cond_15
    return v1

    #@16
    .line 80
    .restart local v1       #msgLen:I
    :cond_16
    add-int/lit8 v0, v0, 0x1

    #@18
    goto :goto_6
.end method

.method private findSegmentIndPosition(I)Z
    .registers 11
    .parameter "segIndex"

    #@0
    .prologue
    const/16 v8, 0x20

    #@2
    const/4 v5, 0x0

    #@3
    const/4 v4, 0x1

    #@4
    .line 136
    const/4 v3, 0x0

    #@5
    .line 137
    .local v3, tempInsertPos:I
    const/4 v1, 0x0

    #@6
    .line 138
    .local v1, remainStringLen:I
    const/4 v0, 0x0

    #@7
    .line 139
    .local v0, previousSIPos:I
    const/4 v2, 0x0

    #@8
    .line 141
    .local v2, srchIdx:I
    if-nez p1, :cond_1c

    #@a
    .line 143
    iput v5, p0, Landroid/telephony/SprintSegmentProcess;->segInsertPos:I

    #@c
    .line 144
    iget-object v5, p0, Landroid/telephony/SprintSegmentProcess;->msegmentPosInfo:Ljava/util/HashMap;

    #@e
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v6

    #@12
    iget v7, p0, Landroid/telephony/SprintSegmentProcess;->segInsertPos:I

    #@14
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v7

    #@18
    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1b
    .line 198
    :cond_1b
    :goto_1b
    return v4

    #@1c
    .line 148
    :cond_1c
    iget v6, p0, Landroid/telephony/SprintSegmentProcess;->segInsertPos:I

    #@1e
    invoke-virtual {p0}, Landroid/telephony/SprintSegmentProcess;->getMaxCharacterSegment()I

    #@21
    move-result v7

    #@22
    add-int/2addr v6, v7

    #@23
    invoke-virtual {p0}, Landroid/telephony/SprintSegmentProcess;->getMaxSegment()I

    #@26
    move-result v7

    #@27
    sub-int v3, v6, v7

    #@29
    .line 149
    iget v0, p0, Landroid/telephony/SprintSegmentProcess;->segInsertPos:I

    #@2b
    .line 152
    iget-object v6, p0, Landroid/telephony/SprintSegmentProcess;->mSegmentedString:Ljava/lang/StringBuilder;

    #@2d
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->charAt(I)C

    #@30
    move-result v6

    #@31
    if-ne v6, v8, :cond_52

    #@33
    .line 153
    iput v3, p0, Landroid/telephony/SprintSegmentProcess;->segInsertPos:I

    #@35
    .line 190
    :goto_35
    iget-object v6, p0, Landroid/telephony/SprintSegmentProcess;->msegmentPosInfo:Ljava/util/HashMap;

    #@37
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3a
    move-result-object v7

    #@3b
    iget v8, p0, Landroid/telephony/SprintSegmentProcess;->segInsertPos:I

    #@3d
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@40
    move-result-object v8

    #@41
    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@44
    .line 192
    iget v6, p0, Landroid/telephony/SprintSegmentProcess;->textLen:I

    #@46
    iget v7, p0, Landroid/telephony/SprintSegmentProcess;->segInsertPos:I

    #@48
    sub-int v1, v6, v7

    #@4a
    .line 194
    invoke-virtual {p0}, Landroid/telephony/SprintSegmentProcess;->getMaxCharacterSegment()I

    #@4d
    move-result v6

    #@4e
    if-gt v1, v6, :cond_1b

    #@50
    move v4, v5

    #@51
    .line 198
    goto :goto_1b

    #@52
    .line 155
    :cond_52
    add-int/lit8 v2, v3, -0x1

    #@54
    :goto_54
    if-le v2, v0, :cond_6d

    #@56
    .line 159
    iget-object v6, p0, Landroid/telephony/SprintSegmentProcess;->mSegmentedString:Ljava/lang/StringBuilder;

    #@58
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    #@5b
    move-result v6

    #@5c
    if-ne v6, v8, :cond_7c

    #@5e
    .line 161
    if-ne p1, v4, :cond_79

    #@60
    .line 165
    iget-object v6, p0, Landroid/telephony/SprintSegmentProcess;->mSegmentedString:Ljava/lang/StringBuilder;

    #@62
    add-int/lit8 v7, v2, -0x1

    #@64
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->charAt(I)C

    #@67
    move-result v6

    #@68
    const/16 v7, 0x2d

    #@6a
    if-ne v6, v7, :cond_76

    #@6c
    .line 167
    move v2, v0

    #@6d
    .line 183
    :cond_6d
    :goto_6d
    if-ne v2, v0, :cond_7f

    #@6f
    .line 184
    invoke-direct {p0, v3}, Landroid/telephony/SprintSegmentProcess;->checkBoundSmiley(I)I

    #@72
    move-result v6

    #@73
    iput v6, p0, Landroid/telephony/SprintSegmentProcess;->segInsertPos:I

    #@75
    goto :goto_35

    #@76
    .line 171
    :cond_76
    add-int/lit8 v2, v2, 0x1

    #@78
    goto :goto_6d

    #@79
    .line 176
    :cond_79
    add-int/lit8 v2, v2, 0x1

    #@7b
    .line 178
    goto :goto_6d

    #@7c
    .line 155
    :cond_7c
    add-int/lit8 v2, v2, -0x1

    #@7e
    goto :goto_54

    #@7f
    .line 187
    :cond_7f
    iput v2, p0, Landroid/telephony/SprintSegmentProcess;->segInsertPos:I

    #@81
    goto :goto_35
.end method

.method private removeSegmentInfoAll()V
    .registers 4

    #@0
    .prologue
    .line 233
    iget-object v1, p0, Landroid/telephony/SprintSegmentProcess;->msegmentPosInfo:Ljava/util/HashMap;

    #@2
    if-nez v1, :cond_a

    #@4
    .line 234
    new-instance v1, Ljava/lang/NullPointerException;

    #@6
    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    #@9
    throw v1

    #@a
    .line 236
    :cond_a
    iget-object v1, p0, Landroid/telephony/SprintSegmentProcess;->msegmentPosInfo:Ljava/util/HashMap;

    #@c
    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    #@f
    move-result v1

    #@10
    add-int/lit8 v0, v1, -0x1

    #@12
    .local v0, index:I
    :goto_12
    if-ltz v0, :cond_20

    #@14
    .line 238
    iget-object v1, p0, Landroid/telephony/SprintSegmentProcess;->msegmentPosInfo:Ljava/util/HashMap;

    #@16
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@1d
    .line 236
    add-int/lit8 v0, v0, -0x1

    #@1f
    goto :goto_12

    #@20
    .line 241
    :cond_20
    return-void
.end method


# virtual methods
.method public calcNeedTotalSegment(Ljava/lang/String;)I
    .registers 6
    .parameter "msgText"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 203
    const/4 v0, 0x0

    #@2
    .line 205
    .local v0, segmentIndex:I
    iput v3, p0, Landroid/telephony/SprintSegmentProcess;->segInsertPos:I

    #@4
    .line 207
    iget-object v1, p0, Landroid/telephony/SprintSegmentProcess;->mSegmentedString:Ljava/lang/StringBuilder;

    #@6
    iget-object v2, p0, Landroid/telephony/SprintSegmentProcess;->mSegmentedString:Ljava/lang/StringBuilder;

    #@8
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    #@b
    move-result v2

    #@c
    invoke-virtual {v1, v3, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    #@f
    .line 208
    iget-object v1, p0, Landroid/telephony/SprintSegmentProcess;->mSegmentedString:Ljava/lang/StringBuilder;

    #@11
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    .line 210
    const/4 v0, 0x0

    #@15
    .line 212
    :goto_15
    invoke-virtual {p0}, Landroid/telephony/SprintSegmentProcess;->getMaxSegment()I

    #@18
    move-result v1

    #@19
    add-int/lit8 v1, v1, -0x1

    #@1b
    if-le v0, v1, :cond_24

    #@1d
    .line 221
    :cond_1d
    invoke-virtual {p0}, Landroid/telephony/SprintSegmentProcess;->getMaxSegment()I

    #@20
    move-result v1

    #@21
    if-ne v0, v1, :cond_2d

    #@23
    .line 228
    .end local v0           #segmentIndex:I
    :goto_23
    return v0

    #@24
    .line 217
    .restart local v0       #segmentIndex:I
    :cond_24
    invoke-direct {p0, v0}, Landroid/telephony/SprintSegmentProcess;->findSegmentIndPosition(I)Z

    #@27
    move-result v1

    #@28
    if-eqz v1, :cond_1d

    #@2a
    .line 210
    add-int/lit8 v0, v0, 0x1

    #@2c
    goto :goto_15

    #@2d
    .line 228
    :cond_2d
    add-int/lit8 v0, v0, 0x1

    #@2f
    goto :goto_23
.end method

.method public getMaxCharacterCountPaginationIndicator()I
    .registers 3

    #@0
    .prologue
    .line 93
    const/4 v0, 0x0

    #@1
    const-string v1, "remove_pagination_indicator"

    #@3
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_b

    #@9
    .line 94
    const/4 v0, 0x0

    #@a
    .line 96
    :goto_a
    return v0

    #@b
    :cond_b
    const/16 v0, 0x8

    #@d
    goto :goto_a
.end method

.method public getMaxCharacterSegment()I
    .registers 3

    #@0
    .prologue
    .line 71
    iget v0, p0, Landroid/telephony/SprintSegmentProcess;->EmailLen:I

    #@2
    if-lez v0, :cond_e

    #@4
    .line 72
    invoke-virtual {p0}, Landroid/telephony/SprintSegmentProcess;->segmentLimitLen()I

    #@7
    move-result v0

    #@8
    iget v1, p0, Landroid/telephony/SprintSegmentProcess;->EmailLen:I

    #@a
    add-int/lit8 v1, v1, 0x1

    #@c
    sub-int/2addr v0, v1

    #@d
    .line 74
    :goto_d
    return v0

    #@e
    :cond_e
    invoke-virtual {p0}, Landroid/telephony/SprintSegmentProcess;->segmentLimitLen()I

    #@11
    move-result v0

    #@12
    goto :goto_d
.end method

.method public getMaxSegment()I
    .registers 2

    #@0
    .prologue
    .line 89
    const/16 v0, 0xa

    #@2
    return v0
.end method

.method public makeSegmentString(II)Ljava/lang/String;
    .registers 5
    .parameter "curSeg"
    .parameter "totalSeg"

    #@0
    .prologue
    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "("

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    const-string v1, "/"

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    const-string v1, ") "

    #@23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v0

    #@2b
    return-object v0
.end method

.method public msegmentPosInfoGet(I)I
    .registers 4
    .parameter "segIdx"

    #@0
    .prologue
    .line 102
    iget-object v0, p0, Landroid/telephony/SprintSegmentProcess;->msegmentPosInfo:Ljava/util/HashMap;

    #@2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Ljava/lang/Integer;

    #@c
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@f
    move-result v0

    #@10
    return v0
.end method

.method public msegmentPosInfoSize()I
    .registers 2

    #@0
    .prologue
    .line 51
    iget-object v0, p0, Landroid/telephony/SprintSegmentProcess;->msegmentPosInfo:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public needSprintSegmentProcess()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 55
    const/4 v0, 0x0

    #@2
    .line 56
    .local v0, limitLen:I
    invoke-virtual {p0}, Landroid/telephony/SprintSegmentProcess;->segmentLimitLen()I

    #@5
    move-result v0

    #@6
    .line 58
    iget v2, p0, Landroid/telephony/SprintSegmentProcess;->textLen:I

    #@8
    if-nez v2, :cond_b

    #@a
    .line 67
    :cond_a
    :goto_a
    return v1

    #@b
    .line 63
    :cond_b
    iget v3, p0, Landroid/telephony/SprintSegmentProcess;->textLen:I

    #@d
    iget v2, p0, Landroid/telephony/SprintSegmentProcess;->EmailLen:I

    #@f
    if-lez v2, :cond_1a

    #@11
    iget v2, p0, Landroid/telephony/SprintSegmentProcess;->EmailLen:I

    #@13
    add-int/lit8 v2, v2, 0x1

    #@15
    :goto_15
    add-int/2addr v2, v3

    #@16
    if-le v2, v0, :cond_a

    #@18
    .line 67
    const/4 v1, 0x1

    #@19
    goto :goto_a

    #@1a
    :cond_1a
    move v2, v1

    #@1b
    .line 63
    goto :goto_15
.end method

.method public segmentLimitLen()I
    .registers 4

    #@0
    .prologue
    .line 39
    const/4 v0, 0x0

    #@1
    .line 41
    .local v0, limitLen:I
    iget v1, p0, Landroid/telephony/SprintSegmentProcess;->codeUnitSize:I

    #@3
    const/4 v2, 0x1

    #@4
    if-ne v1, v2, :cond_9

    #@6
    .line 42
    const/16 v0, 0xa0

    #@8
    .line 46
    :goto_8
    return v0

    #@9
    .line 44
    :cond_9
    const/16 v0, 0x46

    #@b
    goto :goto_8
.end method
