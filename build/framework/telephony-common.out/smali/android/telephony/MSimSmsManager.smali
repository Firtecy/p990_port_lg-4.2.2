.class public Landroid/telephony/MSimSmsManager;
.super Ljava/lang/Object;
.source "MSimSmsManager.java"


# static fields
.field public static final RESULT_ERROR_FDN_CHECK_FAILURE:I = 0x6

.field public static final RESULT_ERROR_GENERIC_FAILURE:I = 0x1

.field public static final RESULT_ERROR_LIMIT_EXCEEDED:I = 0x5

.field public static final RESULT_ERROR_NO_SERVICE:I = 0x4

.field public static final RESULT_ERROR_NULL_PDU:I = 0x3

.field public static final RESULT_ERROR_RADIO_OFF:I = 0x2

.field public static final STATUS_ON_ICC_FREE:I = 0x0

.field public static final STATUS_ON_ICC_READ:I = 0x1

.field public static final STATUS_ON_ICC_SENT:I = 0x5

.field public static final STATUS_ON_ICC_UNREAD:I = 0x3

.field public static final STATUS_ON_ICC_UNSENT:I = 0x7

.field protected static isMultiSimEnabled:Z

.field private static final sInstance:Landroid/telephony/MSimSmsManager;


# instance fields
.field private final DEFAULT_SUB:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 50
    new-instance v0, Landroid/telephony/MSimSmsManager;

    #@2
    invoke-direct {v0}, Landroid/telephony/MSimSmsManager;-><init>()V

    #@5
    sput-object v0, Landroid/telephony/MSimSmsManager;->sInstance:Landroid/telephony/MSimSmsManager;

    #@7
    .line 51
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0}, Landroid/telephony/MSimTelephonyManager;->isMultiSimEnabled()Z

    #@e
    move-result v0

    #@f
    sput-boolean v0, Landroid/telephony/MSimSmsManager;->isMultiSimEnabled:Z

    #@11
    return-void
.end method

.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 251
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 53
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/telephony/MSimSmsManager;->DEFAULT_SUB:I

    #@6
    .line 253
    return-void
.end method

.method private static createMessageListFromRawRecords(Ljava/util/List;)Ljava/util/ArrayList;
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/SmsRawData;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/telephony/SmsMessage;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 522
    .local p0, records:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/SmsRawData;>;"
    new-instance v3, Ljava/util/ArrayList;

    #@2
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 523
    .local v3, messages:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/telephony/SmsMessage;>;"
    if-eqz p0, :cond_28

    #@7
    .line 524
    invoke-interface {p0}, Ljava/util/List;->size()I

    #@a
    move-result v0

    #@b
    .line 525
    .local v0, count:I
    const/4 v2, 0x0

    #@c
    .local v2, i:I
    :goto_c
    if-ge v2, v0, :cond_28

    #@e
    .line 526
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Lcom/android/internal/telephony/SmsRawData;

    #@14
    .line 528
    .local v1, data:Lcom/android/internal/telephony/SmsRawData;
    if-eqz v1, :cond_25

    #@16
    .line 529
    add-int/lit8 v5, v2, 0x1

    #@18
    invoke-virtual {v1}, Lcom/android/internal/telephony/SmsRawData;->getBytes()[B

    #@1b
    move-result-object v6

    #@1c
    invoke-static {v5, v6}, Landroid/telephony/SmsMessage;->createFromEfRecord(I[B)Landroid/telephony/SmsMessage;

    #@1f
    move-result-object v4

    #@20
    .line 530
    .local v4, sms:Landroid/telephony/SmsMessage;
    if-eqz v4, :cond_25

    #@22
    .line 531
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@25
    .line 525
    .end local v4           #sms:Landroid/telephony/SmsMessage;
    :cond_25
    add-int/lit8 v2, v2, 0x1

    #@27
    goto :goto_c

    #@28
    .line 536
    .end local v0           #count:I
    .end local v1           #data:Lcom/android/internal/telephony/SmsRawData;
    .end local v2           #i:I
    :cond_28
    return-object v3
.end method

.method public static getDefault()Landroid/telephony/MSimSmsManager;
    .registers 1

    #@0
    .prologue
    .line 248
    sget-object v0, Landroid/telephony/MSimSmsManager;->sInstance:Landroid/telephony/MSimSmsManager;

    #@2
    return-object v0
.end method


# virtual methods
.method public copyMessageToIcc([B[BII)Z
    .registers 9
    .parameter "smsc"
    .parameter "pdu"
    .parameter "status"
    .parameter "subscription"

    #@0
    .prologue
    .line 271
    const/4 v1, 0x0

    #@1
    .line 273
    .local v1, success:Z
    if-nez p2, :cond_b

    #@3
    .line 274
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@5
    const-string v3, "pdu is NULL"

    #@7
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v2

    #@b
    .line 277
    :cond_b
    :try_start_b
    const-string v2, "isms_msim"

    #@d
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@10
    move-result-object v2

    #@11
    invoke-static {v2}, Lcom/android/internal/telephony/msim/ISmsMSim$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/msim/ISmsMSim;

    #@14
    move-result-object v0

    #@15
    .line 278
    .local v0, iccISms:Lcom/android/internal/telephony/msim/ISmsMSim;
    if-eqz v0, :cond_1b

    #@17
    .line 279
    invoke-interface {v0, p3, p2, p1, p4}, Lcom/android/internal/telephony/msim/ISmsMSim;->copyMessageToIccEf(I[B[BI)Z
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_1a} :catch_1c

    #@1a
    move-result v1

    #@1b
    .line 286
    .end local v0           #iccISms:Lcom/android/internal/telephony/msim/ISmsMSim;
    :cond_1b
    :goto_1b
    return v1

    #@1c
    .line 282
    :catch_1c
    move-exception v2

    #@1d
    goto :goto_1b
.end method

.method public copySmsToIcc([B[BII)I
    .registers 6
    .parameter "smsc"
    .parameter "pdu"
    .parameter "status"
    .parameter "subscription"

    #@0
    .prologue
    .line 583
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public deleteMessageFromIcc(II)Z
    .registers 7
    .parameter "messageIndex"
    .parameter "subscription"

    #@0
    .prologue
    .line 301
    const/4 v2, 0x0

    #@1
    .line 302
    .local v2, success:Z
    const/16 v3, 0xaf

    #@3
    new-array v1, v3, [B

    #@5
    .line 303
    .local v1, pdu:[B
    const/4 v3, -0x1

    #@6
    invoke-static {v1, v3}, Ljava/util/Arrays;->fill([BB)V

    #@9
    .line 306
    :try_start_9
    const-string v3, "isms_msim"

    #@b
    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@e
    move-result-object v3

    #@f
    invoke-static {v3}, Lcom/android/internal/telephony/msim/ISmsMSim$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/msim/ISmsMSim;

    #@12
    move-result-object v0

    #@13
    .line 307
    .local v0, iccISms:Lcom/android/internal/telephony/msim/ISmsMSim;
    if-eqz v0, :cond_1a

    #@15
    .line 308
    const/4 v3, 0x0

    #@16
    invoke-interface {v0, p1, v3, v1, p2}, Lcom/android/internal/telephony/msim/ISmsMSim;->updateMessageOnIccEf(II[BI)Z
    :try_end_19
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_19} :catch_1b

    #@19
    move-result v2

    #@1a
    .line 315
    .end local v0           #iccISms:Lcom/android/internal/telephony/msim/ISmsMSim;
    :cond_1a
    :goto_1a
    return v2

    #@1b
    .line 311
    :catch_1b
    move-exception v3

    #@1c
    goto :goto_1a
.end method

.method public disableCellBroadcast(II)Z
    .registers 6
    .parameter "messageIdentifier"
    .parameter "subscription"

    #@0
    .prologue
    .line 422
    const/4 v1, 0x0

    #@1
    .line 425
    .local v1, success:Z
    :try_start_1
    const-string v2, "isms_msim"

    #@3
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v2

    #@7
    invoke-static {v2}, Lcom/android/internal/telephony/msim/ISmsMSim$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/msim/ISmsMSim;

    #@a
    move-result-object v0

    #@b
    .line 426
    .local v0, iccISms:Lcom/android/internal/telephony/msim/ISmsMSim;
    if-eqz v0, :cond_11

    #@d
    .line 427
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/msim/ISmsMSim;->disableCellBroadcast(II)Z
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_10} :catch_12

    #@10
    move-result v1

    #@11
    .line 434
    .end local v0           #iccISms:Lcom/android/internal/telephony/msim/ISmsMSim;
    :cond_11
    :goto_11
    return v1

    #@12
    .line 430
    :catch_12
    move-exception v2

    #@13
    goto :goto_11
.end method

.method public disableCellBroadcastRange(III)Z
    .registers 8
    .parameter "startMessageId"
    .parameter "endMessageId"
    .parameter "subscription"

    #@0
    .prologue
    .line 495
    const/4 v1, 0x0

    #@1
    .line 497
    .local v1, success:Z
    if-ge p2, p1, :cond_b

    #@3
    .line 498
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@5
    const-string v3, "endMessageId < startMessageId"

    #@7
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v2

    #@b
    .line 501
    :cond_b
    :try_start_b
    const-string v2, "isms_msim"

    #@d
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@10
    move-result-object v2

    #@11
    invoke-static {v2}, Lcom/android/internal/telephony/msim/ISmsMSim$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/msim/ISmsMSim;

    #@14
    move-result-object v0

    #@15
    .line 502
    .local v0, iccISms:Lcom/android/internal/telephony/msim/ISmsMSim;
    if-eqz v0, :cond_1b

    #@17
    .line 503
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/msim/ISmsMSim;->disableCellBroadcastRange(III)Z
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_1a} :catch_1c

    #@1a
    move-result v1

    #@1b
    .line 510
    .end local v0           #iccISms:Lcom/android/internal/telephony/msim/ISmsMSim;
    :cond_1b
    :goto_1b
    return v1

    #@1c
    .line 506
    :catch_1c
    move-exception v2

    #@1d
    goto :goto_1b
.end method

.method public divideMessage(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 4
    .parameter "text"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 118
    if-nez p1, :cond_a

    #@2
    .line 119
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "text is null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 121
    :cond_a
    invoke-static {p1}, Landroid/telephony/SmsMessage;->fragmentText(Ljava/lang/String;)Ljava/util/ArrayList;

    #@d
    move-result-object v0

    #@e
    return-object v0
.end method

.method public enableCellBroadcast(II)Z
    .registers 6
    .parameter "messageIdentifier"
    .parameter "subscription"

    #@0
    .prologue
    .line 390
    const/4 v1, 0x0

    #@1
    .line 393
    .local v1, success:Z
    :try_start_1
    const-string v2, "isms_msim"

    #@3
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v2

    #@7
    invoke-static {v2}, Lcom/android/internal/telephony/msim/ISmsMSim$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/msim/ISmsMSim;

    #@a
    move-result-object v0

    #@b
    .line 394
    .local v0, iccISms:Lcom/android/internal/telephony/msim/ISmsMSim;
    if-eqz v0, :cond_11

    #@d
    .line 395
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/msim/ISmsMSim;->enableCellBroadcast(II)Z
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_10} :catch_12

    #@10
    move-result v1

    #@11
    .line 402
    .end local v0           #iccISms:Lcom/android/internal/telephony/msim/ISmsMSim;
    :cond_11
    :goto_11
    return v1

    #@12
    .line 398
    :catch_12
    move-exception v2

    #@13
    goto :goto_11
.end method

.method public enableCellBroadcastRange(III)Z
    .registers 8
    .parameter "startMessageId"
    .parameter "endMessageId"
    .parameter "subscription"

    #@0
    .prologue
    .line 457
    const/4 v1, 0x0

    #@1
    .line 459
    .local v1, success:Z
    if-ge p2, p1, :cond_b

    #@3
    .line 460
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@5
    const-string v3, "endMessageId < startMessageId"

    #@7
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v2

    #@b
    .line 463
    :cond_b
    :try_start_b
    const-string v2, "isms_msim"

    #@d
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@10
    move-result-object v2

    #@11
    invoke-static {v2}, Lcom/android/internal/telephony/msim/ISmsMSim$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/msim/ISmsMSim;

    #@14
    move-result-object v0

    #@15
    .line 464
    .local v0, iccISms:Lcom/android/internal/telephony/msim/ISmsMSim;
    if-eqz v0, :cond_1b

    #@17
    .line 465
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/msim/ISmsMSim;->enableCellBroadcastRange(III)Z
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_1a} :catch_1c

    #@1a
    move-result v1

    #@1b
    .line 472
    .end local v0           #iccISms:Lcom/android/internal/telephony/msim/ISmsMSim;
    :cond_1b
    :goto_1b
    return v1

    #@1c
    .line 468
    :catch_1c
    move-exception v2

    #@1d
    goto :goto_1b
.end method

.method public getAllMessagesFromIcc(I)Ljava/util/ArrayList;
    .registers 5
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/telephony/SmsMessage;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 359
    const/4 v1, 0x0

    #@1
    .line 362
    .local v1, records:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/SmsRawData;>;"
    :try_start_1
    const-string v2, "isms_msim"

    #@3
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v2

    #@7
    invoke-static {v2}, Lcom/android/internal/telephony/msim/ISmsMSim$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/msim/ISmsMSim;

    #@a
    move-result-object v0

    #@b
    .line 363
    .local v0, iccISms:Lcom/android/internal/telephony/msim/ISmsMSim;
    if-eqz v0, :cond_11

    #@d
    .line 364
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/msim/ISmsMSim;->getAllMessagesFromIccEf(I)Ljava/util/List;
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_10} :catch_16

    #@10
    move-result-object v1

    #@11
    .line 370
    .end local v0           #iccISms:Lcom/android/internal/telephony/msim/ISmsMSim;
    :cond_11
    :goto_11
    invoke-static {v1}, Landroid/telephony/MSimSmsManager;->createMessageListFromRawRecords(Ljava/util/List;)Ljava/util/ArrayList;

    #@14
    move-result-object v2

    #@15
    return-object v2

    #@16
    .line 366
    :catch_16
    move-exception v2

    #@17
    goto :goto_11
.end method

.method getMaxEfSms(I)I
    .registers 3
    .parameter "subscription"

    #@0
    .prologue
    .line 597
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public getPreferredSmsSubscription()I
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 546
    const/4 v1, 0x0

    #@2
    .line 548
    .local v1, iccISms:Lcom/android/internal/telephony/msim/ISmsMSim;
    :try_start_2
    const-string v3, "isms_msim"

    #@4
    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@7
    move-result-object v3

    #@8
    invoke-static {v3}, Lcom/android/internal/telephony/msim/ISmsMSim$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/msim/ISmsMSim;

    #@b
    move-result-object v1

    #@c
    .line 549
    invoke-interface {v1}, Lcom/android/internal/telephony/msim/ISmsMSim;->getPreferredSmsSubscription()I
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_f} :catch_11
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_f} :catch_13

    #@f
    move-result v2

    #@10
    .line 553
    :goto_10
    return v2

    #@11
    .line 550
    :catch_11
    move-exception v0

    #@12
    .line 551
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_10

    #@13
    .line 552
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_13
    move-exception v0

    #@14
    .line 553
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_10
.end method

.method public getServiceCenterAddress(I)Ljava/lang/String;
    .registers 3
    .parameter "subscription"

    #@0
    .prologue
    .line 576
    const-string v0, ""

    #@2
    return-object v0
.end method

.method public isFdnEnabledOnSubscription(I)Z
    .registers 3
    .parameter "subscription"

    #@0
    .prologue
    .line 590
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public sendDataMessage(Ljava/lang/String;Ljava/lang/String;S[BLandroid/app/PendingIntent;Landroid/app/PendingIntent;I)V
    .registers 16
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter "destinationPort"
    .parameter "data"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "subscription"

    #@0
    .prologue
    .line 223
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_e

    #@6
    .line 224
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v2, "Invalid destinationAddress"

    #@a
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v1

    #@e
    .line 227
    :cond_e
    if-eqz p4, :cond_13

    #@10
    array-length v1, p4

    #@11
    if-nez v1, :cond_1b

    #@13
    .line 228
    :cond_13
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@15
    const-string v2, "Invalid message data"

    #@17
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v1

    #@1b
    .line 232
    :cond_1b
    :try_start_1b
    const-string v1, "isms_msim"

    #@1d
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@20
    move-result-object v1

    #@21
    invoke-static {v1}, Lcom/android/internal/telephony/msim/ISmsMSim$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/msim/ISmsMSim;

    #@24
    move-result-object v0

    #@25
    .line 233
    .local v0, iccISms:Lcom/android/internal/telephony/msim/ISmsMSim;
    if-eqz v0, :cond_35

    #@27
    .line 234
    const v1, 0xffff

    #@2a
    and-int v3, p3, v1

    #@2c
    move-object v1, p1

    #@2d
    move-object v2, p2

    #@2e
    move-object v4, p4

    #@2f
    move-object v5, p5

    #@30
    move-object v6, p6

    #@31
    move v7, p7

    #@32
    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/msim/ISmsMSim;->sendData(Ljava/lang/String;Ljava/lang/String;I[BLandroid/app/PendingIntent;Landroid/app/PendingIntent;I)V
    :try_end_35
    .catch Landroid/os/RemoteException; {:try_start_1b .. :try_end_35} :catch_36

    #@35
    .line 240
    .end local v0           #iccISms:Lcom/android/internal/telephony/msim/ISmsMSim;
    :cond_35
    :goto_35
    return-void

    #@36
    .line 237
    :catch_36
    move-exception v1

    #@37
    goto :goto_35
.end method

.method public sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;I)V
    .registers 15
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter
    .parameter
    .parameter
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;I)V"
        }
    .end annotation

    #@0
    .prologue
    .local p3, parts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, sentIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .local p5, deliveryIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 160
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_10

    #@8
    .line 161
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@a
    const-string v2, "Invalid destinationAddress"

    #@c
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v1

    #@10
    .line 163
    :cond_10
    if-eqz p3, :cond_18

    #@12
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    #@15
    move-result v1

    #@16
    if-ge v1, v3, :cond_20

    #@18
    .line 164
    :cond_18
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@1a
    const-string v2, "Invalid message body"

    #@1c
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v1

    #@20
    .line 167
    :cond_20
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    #@23
    move-result v1

    #@24
    if-le v1, v3, :cond_3c

    #@26
    .line 169
    :try_start_26
    const-string v1, "isms_msim"

    #@28
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@2b
    move-result-object v1

    #@2c
    invoke-static {v1}, Lcom/android/internal/telephony/msim/ISmsMSim$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/msim/ISmsMSim;

    #@2f
    move-result-object v0

    #@30
    .line 171
    .local v0, iccISms:Lcom/android/internal/telephony/msim/ISmsMSim;
    if-eqz v0, :cond_3b

    #@32
    move-object v1, p1

    #@33
    move-object v2, p2

    #@34
    move-object v3, p3

    #@35
    move-object v4, p4

    #@36
    move-object v5, p5

    #@37
    move v6, p6

    #@38
    .line 172
    invoke-interface/range {v0 .. v6}, Lcom/android/internal/telephony/msim/ISmsMSim;->sendMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;I)V
    :try_end_3b
    .catch Landroid/os/RemoteException; {:try_start_26 .. :try_end_3b} :catch_68

    #@3b
    .line 190
    .end local v0           #iccISms:Lcom/android/internal/telephony/msim/ISmsMSim;
    :cond_3b
    :goto_3b
    return-void

    #@3c
    .line 179
    :cond_3c
    const/4 v5, 0x0

    #@3d
    .line 180
    .local v5, sentIntent:Landroid/app/PendingIntent;
    const/4 v6, 0x0

    #@3e
    .line 181
    .local v6, deliveryIntent:Landroid/app/PendingIntent;
    if-eqz p4, :cond_4c

    #@40
    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    #@43
    move-result v1

    #@44
    if-lez v1, :cond_4c

    #@46
    .line 182
    invoke-virtual {p4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@49
    move-result-object v5

    #@4a
    .end local v5           #sentIntent:Landroid/app/PendingIntent;
    check-cast v5, Landroid/app/PendingIntent;

    #@4c
    .line 184
    .restart local v5       #sentIntent:Landroid/app/PendingIntent;
    :cond_4c
    if-eqz p5, :cond_5a

    #@4e
    invoke-virtual {p5}, Ljava/util/ArrayList;->size()I

    #@51
    move-result v1

    #@52
    if-lez v1, :cond_5a

    #@54
    .line 185
    invoke-virtual {p5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@57
    move-result-object v6

    #@58
    .end local v6           #deliveryIntent:Landroid/app/PendingIntent;
    check-cast v6, Landroid/app/PendingIntent;

    #@5a
    .line 187
    .restart local v6       #deliveryIntent:Landroid/app/PendingIntent;
    :cond_5a
    invoke-virtual {p3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5d
    move-result-object v4

    #@5e
    check-cast v4, Ljava/lang/String;

    #@60
    move-object v1, p0

    #@61
    move-object v2, p1

    #@62
    move-object v3, p2

    #@63
    move v7, p6

    #@64
    invoke-virtual/range {v1 .. v7}, Landroid/telephony/MSimSmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;I)V

    #@67
    goto :goto_3b

    #@68
    .line 175
    .end local v5           #sentIntent:Landroid/app/PendingIntent;
    .end local v6           #deliveryIntent:Landroid/app/PendingIntent;
    :catch_68
    move-exception v1

    #@69
    goto :goto_3b
.end method

.method public sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;I)V
    .registers 14
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "subscription"

    #@0
    .prologue
    .line 88
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_e

    #@6
    .line 89
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v2, "Invalid destinationAddress"

    #@a
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v1

    #@e
    .line 92
    :cond_e
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_1c

    #@14
    .line 93
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@16
    const-string v2, "Invalid message body"

    #@18
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v1

    #@1c
    .line 97
    :cond_1c
    :try_start_1c
    const-string v1, "isms_msim"

    #@1e
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@21
    move-result-object v1

    #@22
    invoke-static {v1}, Lcom/android/internal/telephony/msim/ISmsMSim$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/msim/ISmsMSim;

    #@25
    move-result-object v0

    #@26
    .line 98
    .local v0, iccISms:Lcom/android/internal/telephony/msim/ISmsMSim;
    if-eqz v0, :cond_31

    #@28
    move-object v1, p1

    #@29
    move-object v2, p2

    #@2a
    move-object v3, p3

    #@2b
    move-object v4, p4

    #@2c
    move-object v5, p5

    #@2d
    move v6, p6

    #@2e
    .line 99
    invoke-interface/range {v0 .. v6}, Lcom/android/internal/telephony/msim/ISmsMSim;->sendText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;I)V
    :try_end_31
    .catch Landroid/os/RemoteException; {:try_start_1c .. :try_end_31} :catch_32

    #@31
    .line 105
    .end local v0           #iccISms:Lcom/android/internal/telephony/msim/ISmsMSim;
    :cond_31
    :goto_31
    return-void

    #@32
    .line 102
    :catch_32
    move-exception v1

    #@33
    goto :goto_31
.end method

.method public setServiceCenterAddress(Ljava/lang/String;I)Z
    .registers 4
    .parameter "smsc"
    .parameter "subscription"

    #@0
    .prologue
    .line 569
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public updateMessageOnIcc(II[BI)Z
    .registers 8
    .parameter "messageIndex"
    .parameter "newStatus"
    .parameter "pdu"
    .parameter "subscription"

    #@0
    .prologue
    .line 334
    const/4 v1, 0x0

    #@1
    .line 337
    .local v1, success:Z
    :try_start_1
    const-string v2, "isms_msim"

    #@3
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v2

    #@7
    invoke-static {v2}, Lcom/android/internal/telephony/msim/ISmsMSim$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/msim/ISmsMSim;

    #@a
    move-result-object v0

    #@b
    .line 338
    .local v0, iccISms:Lcom/android/internal/telephony/msim/ISmsMSim;
    if-eqz v0, :cond_11

    #@d
    .line 339
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/android/internal/telephony/msim/ISmsMSim;->updateMessageOnIccEf(II[BI)Z
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_10} :catch_12

    #@10
    move-result v1

    #@11
    .line 346
    .end local v0           #iccISms:Lcom/android/internal/telephony/msim/ISmsMSim;
    :cond_11
    :goto_11
    return v1

    #@12
    .line 342
    :catch_12
    move-exception v2

    #@13
    goto :goto_11
.end method

.method public updateSmsOnSimReadStatus(IZI)Z
    .registers 5
    .parameter "index"
    .parameter "read"
    .parameter "subscription"

    #@0
    .prologue
    .line 562
    const/4 v0, 0x1

    #@1
    return v0
.end method
