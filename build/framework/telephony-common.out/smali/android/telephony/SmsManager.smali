.class public final Landroid/telephony/SmsManager;
.super Ljava/lang/Object;
.source "SmsManager.java"


# static fields
.field static final LOG_TAG:Ljava/lang/String; = "SmsManager"

.field public static final RESULT_ERROR_FDN_CHECK_FAILURE:I = 0x6

.field public static final RESULT_ERROR_GENERIC_FAILURE:I = 0x1

.field public static final RESULT_ERROR_LIMIT_EXCEEDED:I = 0x5

.field public static final RESULT_ERROR_NO_SERVICE:I = 0x4

.field public static final RESULT_ERROR_NULL_PDU:I = 0x3

.field public static final RESULT_ERROR_RADIO_OFF:I = 0x2

.field public static final RESULT_ERROR_ROAMING_COVERAGE:I = 0x7

.field private static final SMS_FORMAT_CSIM:I = 0x2

.field private static final SMS_FORMAT_USIM:I = 0x1

.field public static final STATUS_ON_ICC_FREE:I = 0x0

.field public static final STATUS_ON_ICC_READ:I = 0x1

.field public static final STATUS_ON_ICC_SENT:I = 0x5

.field public static final STATUS_ON_ICC_UNREAD:I = 0x3

.field public static final STATUS_ON_ICC_UNSENT:I = 0x7

.field private static mSubmitIsRoaming:Z

.field private static mSubmitPriority:I

.field private static final sInstance:Landroid/telephony/SmsManager;

.field private static vp:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 60
    new-instance v0, Landroid/telephony/SmsManager;

    #@3
    invoke-direct {v0}, Landroid/telephony/SmsManager;-><init>()V

    #@6
    sput-object v0, Landroid/telephony/SmsManager;->sInstance:Landroid/telephony/SmsManager;

    #@8
    .line 65
    const/4 v0, -0x1

    #@9
    sput v0, Landroid/telephony/SmsManager;->vp:I

    #@b
    .line 74
    sput v1, Landroid/telephony/SmsManager;->mSubmitPriority:I

    #@d
    .line 78
    sput-boolean v1, Landroid/telephony/SmsManager;->mSubmitIsRoaming:Z

    #@f
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 821
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 823
    return-void
.end method

.method private static combineScaMsg([B[B)[B
    .registers 6
    .parameter "encodedScAddress"
    .parameter "encodedMessage"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 896
    array-length v1, p0

    #@2
    array-length v2, p1

    #@3
    add-int/2addr v1, v2

    #@4
    new-array v0, v1, [B

    #@6
    .line 897
    .local v0, encodedMegWithSca:[B
    array-length v1, p0

    #@7
    invoke-static {p0, v3, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@a
    .line 898
    array-length v1, p0

    #@b
    array-length v2, p1

    #@c
    invoke-static {p1, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@f
    .line 899
    return-object v0
.end method

.method private static createMessageListFromRawRecords(Ljava/util/List;)Ljava/util/ArrayList;
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/SmsRawData;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/telephony/SmsMessage;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1670
    .local p0, records:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/SmsRawData;>;"
    new-instance v3, Ljava/util/ArrayList;

    #@2
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 1671
    .local v3, messages:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/telephony/SmsMessage;>;"
    if-eqz p0, :cond_28

    #@7
    .line 1672
    invoke-interface {p0}, Ljava/util/List;->size()I

    #@a
    move-result v0

    #@b
    .line 1673
    .local v0, count:I
    const/4 v2, 0x0

    #@c
    .local v2, i:I
    :goto_c
    if-ge v2, v0, :cond_28

    #@e
    .line 1674
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Lcom/android/internal/telephony/SmsRawData;

    #@14
    .line 1676
    .local v1, data:Lcom/android/internal/telephony/SmsRawData;
    if-eqz v1, :cond_25

    #@16
    .line 1677
    add-int/lit8 v5, v2, 0x1

    #@18
    invoke-virtual {v1}, Lcom/android/internal/telephony/SmsRawData;->getBytes()[B

    #@1b
    move-result-object v6

    #@1c
    invoke-static {v5, v6}, Landroid/telephony/SmsMessage;->createFromEfRecord(I[B)Landroid/telephony/SmsMessage;

    #@1f
    move-result-object v4

    #@20
    .line 1678
    .local v4, sms:Landroid/telephony/SmsMessage;
    if-eqz v4, :cond_25

    #@22
    .line 1679
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@25
    .line 1673
    .end local v4           #sms:Landroid/telephony/SmsMessage;
    :cond_25
    add-int/lit8 v2, v2, 0x1

    #@27
    goto :goto_c

    #@28
    .line 1684
    .end local v0           #count:I
    .end local v1           #data:Lcom/android/internal/telephony/SmsRawData;
    .end local v2           #i:I
    :cond_28
    return-object v3
.end method

.method public static getAllMessagesFromIcc()Ljava/util/ArrayList;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/telephony/SmsMessage;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1295
    const/4 v1, 0x0

    #@1
    .line 1296
    .local v1, records:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/SmsRawData;>;"
    const-string v2, "getAllMessagesFromIcc(), start"

    #@3
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@6
    .line 1298
    :try_start_6
    const-string v2, "isms"

    #@8
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@b
    move-result-object v2

    #@c
    invoke-static {v2}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@f
    move-result-object v0

    #@10
    .line 1299
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_16

    #@12
    .line 1300
    invoke-interface {v0}, Lcom/android/internal/telephony/ISms;->getAllMessagesFromIccEf()Ljava/util/List;
    :try_end_15
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_15} :catch_1b

    #@15
    move-result-object v1

    #@16
    .line 1306
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_16
    :goto_16
    invoke-static {v1}, Landroid/telephony/SmsManager;->createMessageListFromRawRecords(Ljava/util/List;)Ljava/util/ArrayList;

    #@19
    move-result-object v2

    #@1a
    return-object v2

    #@1b
    .line 1302
    :catch_1b
    move-exception v2

    #@1c
    goto :goto_16
.end method

.method public static getDefault()Landroid/telephony/SmsManager;
    .registers 1

    #@0
    .prologue
    .line 818
    sget-object v0, Landroid/telephony/SmsManager;->sInstance:Landroid/telephony/SmsManager;

    #@2
    return-object v0
.end method

.method public static getSmsIsRoaming()Z
    .registers 1

    #@0
    .prologue
    .line 1998
    sget-boolean v0, Landroid/telephony/SmsManager;->mSubmitIsRoaming:Z

    #@2
    return v0
.end method

.method public static makeSimDeliverPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ[BI)[B
    .registers 20
    .parameter "sca"
    .parameter "oa"
    .parameter "snippet"
    .parameter "statusReport"
    .parameter "time"
    .parameter "smsHeader"
    .parameter "smsformat"

    #@0
    .prologue
    .line 1098
    const/4 v2, 0x0

    #@1
    const-string v3, "use_update_for_copy2sim"

    #@3
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@6
    move-result v2

    #@7
    if-eqz v2, :cond_8d

    #@9
    .line 1099
    move-object v1, p0

    #@a
    .line 1103
    .local v1, smscAddr:Ljava/lang/String;
    :goto_a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "makeSimDeliverPdu(), smscAddr = "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@20
    .line 1104
    new-instance v2, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v3, "makeSimDeliverPdu(), address = "

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v2

    #@33
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@36
    .line 1105
    new-instance v2, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v3, "makeSimDeliverPdu(), message body = "

    #@3d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v2

    #@41
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v2

    #@45
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v2

    #@49
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@4c
    .line 1106
    new-instance v2, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v3, "makeSimDeliverPdu(), smsformat = "

    #@53
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v2

    #@57
    move/from16 v0, p7

    #@59
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v2

    #@5d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v2

    #@61
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@64
    .line 1107
    invoke-static {}, Landroid/telephony/SmsMessage;->getEncodingType()I

    #@67
    move-result v8

    #@68
    move-object v2, p1

    #@69
    move-object v3, p2

    #@6a
    move v4, p3

    #@6b
    move-wide/from16 v5, p4

    #@6d
    move-object/from16 v7, p6

    #@6f
    move/from16 v9, p7

    #@71
    invoke-static/range {v1 .. v9}, Landroid/telephony/SmsMessage;->getDeliverPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ[BII)Landroid/telephony/SmsMessage$DeliverPdu;

    #@74
    move-result-object v11

    #@75
    .line 1116
    .local v11, pdus:Landroid/telephony/SmsMessage$DeliverPdu;
    if-eqz v11, :cond_a3

    #@77
    .line 1117
    const-string v2, "makeSimDeliverPdu(), SmsMessage.DeliverPdu pdus is NOT NULL"

    #@79
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@7c
    .line 1119
    iget-object v2, v11, Landroid/telephony/SmsMessage$DeliverPdu;->encodedScAddress:[B

    #@7e
    if-nez v2, :cond_90

    #@80
    const/4 v2, 0x2

    #@81
    move/from16 v0, p7

    #@83
    if-ne v0, v2, :cond_90

    #@85
    .line 1120
    const-string v2, "makeSimDeliverPdu(), pdus.encodedScAddress is NULL and 3GPP2 format"

    #@87
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@8a
    .line 1121
    iget-object v10, v11, Landroid/telephony/SmsMessage$DeliverPdu;->encodedMessage:[B

    #@8c
    .line 1134
    :goto_8c
    return-object v10

    #@8d
    .line 1101
    .end local v1           #smscAddr:Ljava/lang/String;
    .end local v11           #pdus:Landroid/telephony/SmsMessage$DeliverPdu;
    :cond_8d
    const/4 v1, 0x0

    #@8e
    .restart local v1       #smscAddr:Ljava/lang/String;
    goto/16 :goto_a

    #@90
    .line 1123
    .restart local v11       #pdus:Landroid/telephony/SmsMessage$DeliverPdu;
    :cond_90
    const-string v2, "makeSimDeliverPdu(), pdus.encodedScAddress is NOT NULL "

    #@92
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@95
    .line 1124
    iget-object v2, v11, Landroid/telephony/SmsMessage$DeliverPdu;->encodedScAddress:[B

    #@97
    if-nez v2, :cond_a0

    #@99
    .line 1125
    const-string v2, "makeSimDeliverPdu(), pdus.encodedScAddress is NULL "

    #@9b
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@9e
    .line 1126
    const/4 v10, 0x0

    #@9f
    goto :goto_8c

    #@a0
    .line 1128
    :cond_a0
    iget-object v10, v11, Landroid/telephony/SmsMessage$DeliverPdu;->encodedMessage:[B

    #@a2
    .local v10, encodedPduWithSca:[B
    goto :goto_8c

    #@a3
    .line 1131
    .end local v10           #encodedPduWithSca:[B
    :cond_a3
    const-string v2, "makeSimDeliverPdu(), SmsMessage.DeliverPdu pdus is NULL"

    #@a5
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@a8
    .line 1132
    const/4 v10, 0x0

    #@a9
    goto :goto_8c
.end method

.method public static makeSimPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJII[B)[B
    .registers 11
    .parameter "sca"
    .parameter "oa"
    .parameter "msg"
    .parameter "statusReport"
    .parameter "time"
    .parameter "pid"
    .parameter "dcs"
    .parameter "smsHeader"

    #@0
    .prologue
    .line 910
    invoke-static/range {p0 .. p8}, Landroid/telephony/SmsMessage;->getStaticDeliverPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJII[B)Landroid/telephony/SmsMessage$DeliverPdu;

    #@3
    move-result-object v0

    #@4
    .line 913
    .local v0, pdus:Landroid/telephony/SmsMessage$DeliverPdu;
    if-eqz v0, :cond_a

    #@6
    iget-object v1, v0, Landroid/telephony/SmsMessage$DeliverPdu;->encodedMessage:[B

    #@8
    if-nez v1, :cond_11

    #@a
    .line 914
    :cond_a
    const-string v1, "makeSimPdu(), pdus = null"

    #@c
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@f
    .line 915
    const/4 v1, 0x0

    #@10
    .line 917
    :goto_10
    return-object v1

    #@11
    :cond_11
    iget-object v1, v0, Landroid/telephony/SmsMessage$DeliverPdu;->encodedMessage:[B

    #@13
    goto :goto_10
.end method

.method public static makeSimPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ[B)[B
    .registers 17
    .parameter "sca"
    .parameter "oa"
    .parameter "snippet"
    .parameter "statusReport"
    .parameter "time"
    .parameter "smsHeader"

    #@0
    .prologue
    .line 859
    const/4 v1, 0x0

    #@1
    const-string v2, "use_update_for_copy2sim"

    #@3
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_1f

    #@9
    .line 860
    move-object v0, p0

    #@a
    .line 864
    .local v0, smscAddr:Ljava/lang/String;
    :goto_a
    invoke-static {}, Landroid/telephony/SmsMessage;->getEncodingType()I

    #@d
    move-result v7

    #@e
    move-object v1, p1

    #@f
    move-object v2, p2

    #@10
    move v3, p3

    #@11
    move-wide v4, p4

    #@12
    move-object/from16 v6, p6

    #@14
    invoke-static/range {v0 .. v7}, Landroid/telephony/SmsMessage;->getDeliverPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ[BI)Landroid/telephony/SmsMessage$DeliverPdu;

    #@17
    move-result-object v9

    #@18
    .line 872
    .local v9, pdus:Landroid/telephony/SmsMessage$DeliverPdu;
    if-eqz v9, :cond_3c

    #@1a
    .line 873
    if-nez v0, :cond_21

    #@1c
    .line 874
    iget-object v8, v9, Landroid/telephony/SmsMessage$DeliverPdu;->encodedMessage:[B

    #@1e
    .line 889
    :cond_1e
    :goto_1e
    return-object v8

    #@1f
    .line 862
    .end local v0           #smscAddr:Ljava/lang/String;
    .end local v9           #pdus:Landroid/telephony/SmsMessage$DeliverPdu;
    :cond_1f
    const/4 v0, 0x0

    #@20
    .restart local v0       #smscAddr:Ljava/lang/String;
    goto :goto_a

    #@21
    .line 876
    .restart local v9       #pdus:Landroid/telephony/SmsMessage$DeliverPdu;
    :cond_21
    iget-object v1, v9, Landroid/telephony/SmsMessage$DeliverPdu;->encodedScAddress:[B

    #@23
    if-eqz v1, :cond_29

    #@25
    iget-object v1, v9, Landroid/telephony/SmsMessage$DeliverPdu;->encodedMessage:[B

    #@27
    if-nez v1, :cond_2b

    #@29
    .line 878
    :cond_29
    const/4 v8, 0x0

    #@2a
    goto :goto_1e

    #@2b
    .line 880
    :cond_2b
    iget-object v1, v9, Landroid/telephony/SmsMessage$DeliverPdu;->encodedScAddress:[B

    #@2d
    iget-object v2, v9, Landroid/telephony/SmsMessage$DeliverPdu;->encodedMessage:[B

    #@2f
    invoke-static {v1, v2}, Landroid/telephony/SmsManager;->combineScaMsg([B[B)[B

    #@32
    move-result-object v8

    #@33
    .line 881
    .local v8, encodedPduWithSca:[B
    if-nez v8, :cond_1e

    #@35
    .line 882
    const-string v1, "makeSimPdu(), encodedPduWithSca is null"

    #@37
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@3a
    .line 883
    const/4 v8, 0x0

    #@3b
    goto :goto_1e

    #@3c
    .line 887
    .end local v8           #encodedPduWithSca:[B
    :cond_3c
    const/4 v8, 0x0

    #@3d
    goto :goto_1e
.end method

.method public static makeSimPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ[BI)[B
    .registers 20
    .parameter "sca"
    .parameter "oa"
    .parameter "snippet"
    .parameter "statusReport"
    .parameter "time"
    .parameter "smsHeader"
    .parameter "smsformat"

    #@0
    .prologue
    .line 1051
    const/4 v2, 0x0

    #@1
    const-string v3, "use_update_for_copy2sim"

    #@3
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@6
    move-result v2

    #@7
    if-eqz v2, :cond_8d

    #@9
    .line 1052
    move-object v1, p0

    #@a
    .line 1056
    .local v1, smscAddr:Ljava/lang/String;
    :goto_a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "makeSimPdu(), smscAddr = "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@20
    .line 1057
    new-instance v2, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v3, "makeSimPdu(), address = "

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v2

    #@33
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@36
    .line 1058
    new-instance v2, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v3, "makeSimPdu(), message body = "

    #@3d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v2

    #@41
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v2

    #@45
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v2

    #@49
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@4c
    .line 1059
    new-instance v2, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v3, "makeSimPdu(), smsformat = "

    #@53
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v2

    #@57
    move/from16 v0, p7

    #@59
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v2

    #@5d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v2

    #@61
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@64
    .line 1060
    invoke-static {}, Landroid/telephony/SmsMessage;->getEncodingType()I

    #@67
    move-result v8

    #@68
    move-object v2, p1

    #@69
    move-object v3, p2

    #@6a
    move v4, p3

    #@6b
    move-wide/from16 v5, p4

    #@6d
    move-object/from16 v7, p6

    #@6f
    move/from16 v9, p7

    #@71
    invoke-static/range {v1 .. v9}, Landroid/telephony/SmsMessage;->getDeliverPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ[BII)Landroid/telephony/SmsMessage$DeliverPdu;

    #@74
    move-result-object v11

    #@75
    .line 1069
    .local v11, pdus:Landroid/telephony/SmsMessage$DeliverPdu;
    if-eqz v11, :cond_a3

    #@77
    .line 1070
    const-string v2, "makeSimPdu(), SmsMessage.DeliverPdu pdus is NOT NULL"

    #@79
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@7c
    .line 1071
    iget-object v2, v11, Landroid/telephony/SmsMessage$DeliverPdu;->encodedScAddress:[B

    #@7e
    if-nez v2, :cond_90

    #@80
    const/4 v2, 0x2

    #@81
    move/from16 v0, p7

    #@83
    if-ne v0, v2, :cond_90

    #@85
    .line 1072
    const-string v2, "makeSimPdu(), pdus.encodedScAddress is NULL and 3GPP2 format"

    #@87
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@8a
    .line 1073
    iget-object v10, v11, Landroid/telephony/SmsMessage$DeliverPdu;->encodedMessage:[B

    #@8c
    .line 1086
    :goto_8c
    return-object v10

    #@8d
    .line 1054
    .end local v1           #smscAddr:Ljava/lang/String;
    .end local v11           #pdus:Landroid/telephony/SmsMessage$DeliverPdu;
    :cond_8d
    const/4 v1, 0x0

    #@8e
    .restart local v1       #smscAddr:Ljava/lang/String;
    goto/16 :goto_a

    #@90
    .line 1075
    .restart local v11       #pdus:Landroid/telephony/SmsMessage$DeliverPdu;
    :cond_90
    const-string v2, "makeSimPdu(), pdus.encodedScAddress is NOT NULL "

    #@92
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@95
    .line 1076
    iget-object v2, v11, Landroid/telephony/SmsMessage$DeliverPdu;->encodedScAddress:[B

    #@97
    if-nez v2, :cond_a0

    #@99
    .line 1077
    const-string v2, "makeSimPdu(), pdus.encodedScAddress is NULL "

    #@9b
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@9e
    .line 1078
    const/4 v10, 0x0

    #@9f
    goto :goto_8c

    #@a0
    .line 1080
    :cond_a0
    iget-object v10, v11, Landroid/telephony/SmsMessage$DeliverPdu;->encodedMessage:[B

    #@a2
    .local v10, encodedPduWithSca:[B
    goto :goto_8c

    #@a3
    .line 1083
    .end local v10           #encodedPduWithSca:[B
    :cond_a3
    const-string v2, "makeSimPdu(), SmsMessage.DeliverPdu pdus is NULL"

    #@a5
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@a8
    .line 1084
    const/4 v10, 0x0

    #@a9
    goto :goto_8c
.end method

.method public static makeSimSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ[BI)[B
    .registers 16
    .parameter "sca"
    .parameter "da"
    .parameter "snippet"
    .parameter "statusReport"
    .parameter "time"
    .parameter "smsHeader"
    .parameter "smsformat"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 1146
    const-string v1, "use_update_for_copy2sim"

    #@3
    invoke-static {v6, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_81

    #@9
    .line 1147
    move-object v0, p0

    #@a
    .line 1151
    .local v0, smscAddr:Ljava/lang/String;
    :goto_a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "makeSimSubmitPdu(), smscAddr = "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@20
    .line 1152
    new-instance v1, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v2, "makeSimSubmitPdu(), address = "

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@36
    .line 1153
    new-instance v1, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v2, "makeSimSubmitPdu(), message body = "

    #@3d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v1

    #@45
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v1

    #@49
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@4c
    .line 1154
    new-instance v1, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v2, "makeSimSubmitPdu(), smsformat = "

    #@53
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v1

    #@57
    invoke-virtual {v1, p7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v1

    #@5b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v1

    #@5f
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@62
    move-object v1, p1

    #@63
    move-object v2, p2

    #@64
    move v3, p3

    #@65
    move-object v4, p6

    #@66
    move v5, p7

    #@67
    .line 1155
    invoke-static/range {v0 .. v5}, Landroid/telephony/SmsMessage;->uiccGetSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BI)Landroid/telephony/SmsMessage$SubmitPdu;

    #@6a
    move-result-object v7

    #@6b
    .line 1162
    .local v7, pdus:Landroid/telephony/SmsMessage$SubmitPdu;
    if-eqz v7, :cond_95

    #@6d
    .line 1163
    const-string v1, "makeSimSubmitPdu(), SmsMessage.SubmitPdu pdus is NOT NULL"

    #@6f
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@72
    .line 1164
    iget-object v1, v7, Landroid/telephony/SmsMessage$SubmitPdu;->encodedScAddress:[B

    #@74
    if-nez v1, :cond_83

    #@76
    const/4 v1, 0x2

    #@77
    if-ne p7, v1, :cond_83

    #@79
    .line 1165
    const-string v1, "makeSimSubmitPdu(), pdus.encodedScAddress is NULL and 3GPP2 format"

    #@7b
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@7e
    .line 1166
    iget-object v6, v7, Landroid/telephony/SmsMessage$SubmitPdu;->encodedMessage:[B

    #@80
    .line 1179
    :goto_80
    return-object v6

    #@81
    .line 1149
    .end local v0           #smscAddr:Ljava/lang/String;
    .end local v7           #pdus:Landroid/telephony/SmsMessage$SubmitPdu;
    :cond_81
    const/4 v0, 0x0

    #@82
    .restart local v0       #smscAddr:Ljava/lang/String;
    goto :goto_a

    #@83
    .line 1168
    .restart local v7       #pdus:Landroid/telephony/SmsMessage$SubmitPdu;
    :cond_83
    const-string v1, "makeSimSubmitPdu(), pdus.encodedScAddress is NOT NULL "

    #@85
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@88
    .line 1169
    iget-object v1, v7, Landroid/telephony/SmsMessage$SubmitPdu;->encodedScAddress:[B

    #@8a
    if-nez v1, :cond_92

    #@8c
    .line 1170
    const-string v1, "makeSimSubmitPdu(), pdus.encodedScAddress is NULL "

    #@8e
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@91
    goto :goto_80

    #@92
    .line 1173
    :cond_92
    iget-object v6, v7, Landroid/telephony/SmsMessage$SubmitPdu;->encodedMessage:[B

    #@94
    .local v6, encodedPduWithSca:[B
    goto :goto_80

    #@95
    .line 1176
    .end local v6           #encodedPduWithSca:[B
    :cond_95
    const-string v1, "makeSimSubmitPdu(), SmsMessage.SubmitPdu pdus is NULL"

    #@97
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@9a
    goto :goto_80
.end method

.method public static sendExceptionbySentIntent(Landroid/app/PendingIntent;I)V
    .registers 5
    .parameter "sentIntent"
    .parameter "failureCause"

    #@0
    .prologue
    .line 1936
    const/4 v1, 0x0

    #@1
    const-string v2, "SendIntentFailure"

    #@3
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@6
    move-result v1

    #@7
    const/4 v2, 0x1

    #@8
    if-ne v1, v2, :cond_14

    #@a
    .line 1937
    const-string v1, "sendExceptionbySentIntent(), start"

    #@c
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@f
    .line 1939
    if-eqz p0, :cond_15

    #@11
    .line 1940
    :try_start_11
    invoke-virtual {p0, p1}, Landroid/app/PendingIntent;->send(I)V

    #@14
    .line 1948
    :cond_14
    :goto_14
    return-void

    #@15
    .line 1942
    :cond_15
    const-string v1, "sendExceptionbySentIntent(), sentIntent null"

    #@17
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I
    :try_end_1a
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_11 .. :try_end_1a} :catch_1b

    #@1a
    goto :goto_14

    #@1b
    .line 1944
    :catch_1b
    move-exception v0

    #@1c
    .line 1945
    .local v0, ex:Landroid/app/PendingIntent$CanceledException;
    const-string v1, "sendExceptionbySentIntent(), CanceledException"

    #@1e
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@21
    goto :goto_14
.end method

.method public static sendExceptionbySentIntent(Ljava/util/ArrayList;I)V
    .registers 4
    .parameter
    .parameter "failureCause"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1925
    .local p0, sentIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    const/4 v0, 0x0

    #@1
    .line 1926
    .local v0, sentIntent:Landroid/app/PendingIntent;
    if-eqz p0, :cond_10

    #@3
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v1

    #@7
    if-lez v1, :cond_10

    #@9
    .line 1927
    const/4 v1, 0x0

    #@a
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    .end local v0           #sentIntent:Landroid/app/PendingIntent;
    check-cast v0, Landroid/app/PendingIntent;

    #@10
    .line 1929
    .restart local v0       #sentIntent:Landroid/app/PendingIntent;
    :cond_10
    invoke-static {v0, p1}, Landroid/telephony/SmsManager;->sendExceptionbySentIntent(Landroid/app/PendingIntent;I)V

    #@13
    .line 1930
    return-void
.end method


# virtual methods
.method public SendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;)V
    .registers 15
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter
    .parameter
    .parameter
    .parameter "cbAddress"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .local p3, parts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, sentIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .local p5, deliveryIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    const/4 v2, 0x0

    #@3
    .line 639
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@6
    move-result-object v1

    #@7
    if-eqz v1, :cond_19

    #@9
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@c
    move-result-object v1

    #@d
    invoke-interface {v1, v3, p4}, Lcom/lge/cappuccino/IMdm;->isAllowSendMessage(Landroid/content/ComponentName;Ljava/util/ArrayList;)Z

    #@10
    move-result v1

    #@11
    if-nez v1, :cond_19

    #@13
    .line 641
    const-string v1, "SendMultipartTextMessage(), Block Sending SMS in SMSManager2"

    #@15
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@18
    .line 686
    :cond_18
    :goto_18
    return-void

    #@19
    .line 645
    :cond_19
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1c
    move-result v1

    #@1d
    if-eqz v1, :cond_27

    #@1f
    .line 646
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@21
    const-string v2, "Invalid destinationAddress"

    #@23
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@26
    throw v1

    #@27
    .line 648
    :cond_27
    if-eqz p3, :cond_2f

    #@29
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    #@2c
    move-result v1

    #@2d
    if-ge v1, v4, :cond_37

    #@2f
    .line 649
    :cond_2f
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@31
    const-string v2, "Invalid message body"

    #@33
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@36
    throw v1

    #@37
    .line 652
    :cond_37
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    #@3a
    move-result v1

    #@3b
    if-le v1, v4, :cond_71

    #@3d
    .line 654
    :try_start_3d
    const-string v1, "isms"

    #@3f
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@42
    move-result-object v1

    #@43
    invoke-static {v1}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@46
    move-result-object v0

    #@47
    .line 655
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_18

    #@49
    .line 657
    const/4 v1, 0x0

    #@4a
    const-string v2, "cdma_priority_indicator"

    #@4c
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@4f
    move-result v1

    #@50
    if-eqz v1, :cond_57

    #@52
    .line 658
    sget v1, Landroid/telephony/SmsManager;->mSubmitPriority:I

    #@54
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/ISms;->setSmsPriority(I)V

    #@57
    .line 663
    :cond_57
    const/4 v1, 0x0

    #@58
    const-string v2, "support_sprint_sms_roaming_guard"

    #@5a
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@5d
    move-result v1

    #@5e
    if-eqz v1, :cond_65

    #@60
    .line 664
    sget-boolean v1, Landroid/telephony/SmsManager;->mSubmitIsRoaming:Z

    #@62
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/ISms;->setSmsIsRoaming(Z)V

    #@65
    :cond_65
    move-object v1, p1

    #@66
    move-object v2, p2

    #@67
    move-object v3, p3

    #@68
    move-object v4, p4

    #@69
    move-object v5, p5

    #@6a
    move-object v6, p6

    #@6b
    .line 668
    invoke-interface/range {v0 .. v6}, Lcom/android/internal/telephony/ISms;->SendMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V
    :try_end_6e
    .catch Landroid/os/RemoteException; {:try_start_3d .. :try_end_6e} :catch_6f

    #@6e
    goto :goto_18

    #@6f
    .line 671
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :catch_6f
    move-exception v1

    #@70
    goto :goto_18

    #@71
    .line 675
    :cond_71
    const/4 v5, 0x0

    #@72
    .line 676
    .local v5, sentIntent:Landroid/app/PendingIntent;
    const/4 v6, 0x0

    #@73
    .line 677
    .local v6, deliveryIntent:Landroid/app/PendingIntent;
    if-eqz p4, :cond_81

    #@75
    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    #@78
    move-result v1

    #@79
    if-lez v1, :cond_81

    #@7b
    .line 678
    invoke-virtual {p4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@7e
    move-result-object v5

    #@7f
    .end local v5           #sentIntent:Landroid/app/PendingIntent;
    check-cast v5, Landroid/app/PendingIntent;

    #@81
    .line 680
    .restart local v5       #sentIntent:Landroid/app/PendingIntent;
    :cond_81
    if-eqz p5, :cond_8f

    #@83
    invoke-virtual {p5}, Ljava/util/ArrayList;->size()I

    #@86
    move-result v1

    #@87
    if-lez v1, :cond_8f

    #@89
    .line 681
    invoke-virtual {p5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@8c
    move-result-object v6

    #@8d
    .end local v6           #deliveryIntent:Landroid/app/PendingIntent;
    check-cast v6, Landroid/app/PendingIntent;

    #@8f
    .line 683
    .restart local v6       #deliveryIntent:Landroid/app/PendingIntent;
    :cond_8f
    invoke-virtual {p3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@92
    move-result-object v4

    #@93
    check-cast v4, Ljava/lang/String;

    #@95
    move-object v1, p0

    #@96
    move-object v2, p1

    #@97
    move-object v3, p2

    #@98
    move-object v7, p6

    #@99
    invoke-virtual/range {v1 .. v7}, Landroid/telephony/SmsManager;->SendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)V

    #@9c
    goto/16 :goto_18
.end method

.method public SendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)V
    .registers 14
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "cbAddress"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 173
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@4
    move-result-object v1

    #@5
    if-eqz v1, :cond_17

    #@7
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@a
    move-result-object v1

    #@b
    invoke-interface {v1, v2, p4}, Lcom/lge/cappuccino/IMdm;->isAllowSendMessage(Landroid/content/ComponentName;Landroid/app/PendingIntent;)Z

    #@e
    move-result v1

    #@f
    if-nez v1, :cond_17

    #@11
    .line 175
    const-string v1, "SendTextMessage(), Block Sending SMS in SMSManager1"

    #@13
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@16
    .line 208
    :cond_16
    :goto_16
    return-void

    #@17
    .line 180
    :cond_17
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1a
    move-result v1

    #@1b
    if-eqz v1, :cond_25

    #@1d
    .line 181
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@1f
    const-string v2, "Invalid destinationAddress"

    #@21
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@24
    throw v1

    #@25
    .line 184
    :cond_25
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@28
    move-result v1

    #@29
    if-eqz v1, :cond_33

    #@2b
    .line 185
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@2d
    const-string v2, "Invalid message body"

    #@2f
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@32
    throw v1

    #@33
    .line 189
    :cond_33
    :try_start_33
    const-string v1, "isms"

    #@35
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@38
    move-result-object v1

    #@39
    invoke-static {v1}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@3c
    move-result-object v0

    #@3d
    .line 190
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_16

    #@3f
    .line 192
    const/4 v1, 0x0

    #@40
    const-string v2, "cdma_priority_indicator"

    #@42
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@45
    move-result v1

    #@46
    if-eqz v1, :cond_4d

    #@48
    .line 193
    sget v1, Landroid/telephony/SmsManager;->mSubmitPriority:I

    #@4a
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/ISms;->setSmsPriority(I)V

    #@4d
    .line 198
    :cond_4d
    const/4 v1, 0x0

    #@4e
    const-string v2, "support_sprint_sms_roaming_guard"

    #@50
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@53
    move-result v1

    #@54
    if-eqz v1, :cond_5b

    #@56
    .line 199
    sget-boolean v1, Landroid/telephony/SmsManager;->mSubmitIsRoaming:Z

    #@58
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/ISms;->setSmsIsRoaming(Z)V

    #@5b
    :cond_5b
    move-object v1, p1

    #@5c
    move-object v2, p2

    #@5d
    move-object v3, p3

    #@5e
    move-object v4, p4

    #@5f
    move-object v5, p5

    #@60
    move-object v6, p6

    #@61
    .line 203
    invoke-interface/range {v0 .. v6}, Lcom/android/internal/telephony/ISms;->SendText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)V
    :try_end_64
    .catch Landroid/os/RemoteException; {:try_start_33 .. :try_end_64} :catch_65

    #@64
    goto :goto_16

    #@65
    .line 205
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :catch_65
    move-exception v1

    #@66
    goto :goto_16
.end method

.method public copyMessageToIcc([B[BI)Z
    .registers 8
    .parameter "smsc"
    .parameter "pdu"
    .parameter "status"

    #@0
    .prologue
    .line 937
    const/4 v1, 0x0

    #@1
    .line 939
    .local v1, success:Z
    if-nez p2, :cond_b

    #@3
    .line 940
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@5
    const-string v3, "pdu is NULL"

    #@7
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v2

    #@b
    .line 943
    :cond_b
    :try_start_b
    const-string v2, "isms"

    #@d
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@10
    move-result-object v2

    #@11
    invoke-static {v2}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@14
    move-result-object v0

    #@15
    .line 944
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_20

    #@17
    .line 945
    const-string v2, "copyMessageToIcc(), SmsManager --> IccSmsInterfaceManager"

    #@19
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->f(Ljava/lang/String;)I

    #@1c
    .line 946
    invoke-interface {v0, p3, p2, p1}, Lcom/android/internal/telephony/ISms;->copyMessageToIccEf(I[B[B)Z
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_1f} :catch_21

    #@1f
    move-result v1

    #@20
    .line 952
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_20
    :goto_20
    return v1

    #@21
    .line 948
    :catch_21
    move-exception v2

    #@22
    goto :goto_20
.end method

.method public copySmsToIcc([B[BI)I
    .registers 8
    .parameter "smsc"
    .parameter "pdu"
    .parameter "status"

    #@0
    .prologue
    .line 1803
    const/4 v1, -0x1

    #@1
    .line 1804
    .local v1, indexOnIcc:I
    new-instance v2, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v3, "copySmsToIcc(), status = "

    #@8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@17
    .line 1806
    :try_start_17
    const-string v2, "isms"

    #@19
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v2}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@20
    move-result-object v0

    #@21
    .line 1807
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_2c

    #@23
    .line 1808
    const-string v2, "copySmsToIcc(), SmsManager --> IccSmsInterfaceManager"

    #@25
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->f(Ljava/lang/String;)I

    #@28
    .line 1809
    invoke-interface {v0, p3, p2, p1}, Lcom/android/internal/telephony/ISms;->copySmsToIccEf(I[B[B)I
    :try_end_2b
    .catch Landroid/os/RemoteException; {:try_start_17 .. :try_end_2b} :catch_43

    #@2b
    move-result v1

    #@2c
    .line 1814
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_2c
    :goto_2c
    new-instance v2, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v3, "copySmsToIcc(), indexOnIcc = "

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v2

    #@3f
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@42
    .line 1815
    return v1

    #@43
    .line 1811
    :catch_43
    move-exception v2

    #@44
    goto :goto_2c
.end method

.method public copySmsToIccPrivate([B[BII)I
    .registers 9
    .parameter "smsc"
    .parameter "pdu"
    .parameter "status"
    .parameter "sms_format"

    #@0
    .prologue
    .line 970
    const/4 v1, -0x1

    #@1
    .line 971
    .local v1, indexOnIcc:I
    new-instance v2, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v3, "copySmsToIccPrivate(), status = "

    #@8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    const-string v3, "  sms_format = "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@21
    .line 973
    :try_start_21
    const-string v2, "isms"

    #@23
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@26
    move-result-object v2

    #@27
    invoke-static {v2}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@2a
    move-result-object v0

    #@2b
    .line 974
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_31

    #@2d
    .line 975
    invoke-interface {v0, p3, p2, p1, p4}, Lcom/android/internal/telephony/ISms;->copySmsToIccEfPrivate(I[B[BI)I
    :try_end_30
    .catch Landroid/os/RemoteException; {:try_start_21 .. :try_end_30} :catch_48

    #@30
    move-result v1

    #@31
    .line 980
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_31
    :goto_31
    new-instance v2, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v3, "copySmsToIccPrivate(), indexOnIcc = "

    #@38
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v2

    #@40
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v2

    #@44
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@47
    .line 981
    return v1

    #@48
    .line 977
    :catch_48
    move-exception v2

    #@49
    goto :goto_31
.end method

.method public deleteMessageFromIcc(I)Z
    .registers 7
    .parameter "messageIndex"

    #@0
    .prologue
    .line 1195
    const/4 v2, 0x0

    #@1
    .line 1196
    .local v2, success:Z
    const/16 v3, 0xaf

    #@3
    new-array v1, v3, [B

    #@5
    .line 1197
    .local v1, pdu:[B
    const/4 v3, -0x1

    #@6
    invoke-static {v1, v3}, Ljava/util/Arrays;->fill([BB)V

    #@9
    .line 1198
    new-instance v3, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v4, "deleteMessageFromIcc(), messageIndex = "

    #@10
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@1f
    .line 1201
    :try_start_1f
    const-string v3, "isms"

    #@21
    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@24
    move-result-object v3

    #@25
    invoke-static {v3}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@28
    move-result-object v0

    #@29
    .line 1202
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_35

    #@2b
    .line 1203
    const-string v3, "deleteMessageFromIcc(), SmsManager --> IccSmsInterfaceManager"

    #@2d
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->f(Ljava/lang/String;)I

    #@30
    .line 1205
    const/4 v3, 0x0

    #@31
    invoke-interface {v0, p1, v3, v1}, Lcom/android/internal/telephony/ISms;->updateMessageOnIccEf(II[B)Z
    :try_end_34
    .catch Landroid/os/RemoteException; {:try_start_1f .. :try_end_34} :catch_4c

    #@34
    move-result v2

    #@35
    .line 1210
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_35
    :goto_35
    new-instance v3, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v4, "deleteMessageFromIcc(), success = "

    #@3c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@43
    move-result-object v3

    #@44
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v3

    #@48
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@4b
    .line 1212
    return v2

    #@4c
    .line 1207
    :catch_4c
    move-exception v3

    #@4d
    goto :goto_35
.end method

.method public deleteMessageFromIccMultiMode(II)Z
    .registers 9
    .parameter "messageIndex"
    .parameter "smsformat"

    #@0
    .prologue
    const/16 v5, 0xaf

    #@2
    .line 1228
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "deleteMessageFromIccMultiMode(), messageIndex = "

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@18
    .line 1229
    new-instance v3, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v4, "deleteMessageFromIccMultiMode(), smsformat = "

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@2e
    .line 1230
    const/4 v2, 0x0

    #@2f
    .line 1232
    .local v2, success:Z
    const/4 v3, 0x1

    #@30
    if-ne p2, v3, :cond_60

    #@32
    .line 1233
    new-array v1, v5, [B

    #@34
    .line 1239
    .local v1, pdu:[B
    :goto_34
    const/4 v3, -0x1

    #@35
    invoke-static {v1, v3}, Ljava/util/Arrays;->fill([BB)V

    #@38
    .line 1242
    :try_start_38
    const-string v3, "isms"

    #@3a
    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@3d
    move-result-object v3

    #@3e
    invoke-static {v3}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@41
    move-result-object v0

    #@42
    .line 1243
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_49

    #@44
    .line 1244
    const/4 v3, 0x0

    #@45
    invoke-interface {v0, p1, v3, v1, p2}, Lcom/android/internal/telephony/ISms;->updateMessageOnIccEfMultiMode(II[BI)Z
    :try_end_48
    .catch Landroid/os/RemoteException; {:try_start_38 .. :try_end_48} :catch_6b

    #@48
    move-result v2

    #@49
    .line 1249
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_49
    :goto_49
    new-instance v3, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v4, "deleteMessageFromIccMultiMode(), success = "

    #@50
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v3

    #@54
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@57
    move-result-object v3

    #@58
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v3

    #@5c
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5f
    .line 1250
    return v2

    #@60
    .line 1234
    .end local v1           #pdu:[B
    :cond_60
    const/4 v3, 0x2

    #@61
    if-ne p2, v3, :cond_68

    #@63
    .line 1235
    const/16 v3, 0xfd

    #@65
    new-array v1, v3, [B

    #@67
    .restart local v1       #pdu:[B
    goto :goto_34

    #@68
    .line 1237
    .end local v1           #pdu:[B
    :cond_68
    new-array v1, v5, [B

    #@6a
    .restart local v1       #pdu:[B
    goto :goto_34

    #@6b
    .line 1246
    :catch_6b
    move-exception v3

    #@6c
    goto :goto_49
.end method

.method public disableCdmaBroadcast(I)Z
    .registers 5
    .parameter "messageIdentifier"

    #@0
    .prologue
    .line 1578
    const/4 v1, 0x0

    #@1
    .line 1581
    .local v1, success:Z
    :try_start_1
    const-string v2, "isms"

    #@3
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v2

    #@7
    invoke-static {v2}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@a
    move-result-object v0

    #@b
    .line 1582
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_11

    #@d
    .line 1583
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ISms;->disableCdmaBroadcast(I)Z
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_10} :catch_12

    #@10
    move-result v1

    #@11
    .line 1589
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_11
    :goto_11
    return v1

    #@12
    .line 1585
    :catch_12
    move-exception v2

    #@13
    goto :goto_11
.end method

.method public disableCdmaBroadcastRange(II)Z
    .registers 7
    .parameter "startMessageId"
    .parameter "endMessageId"

    #@0
    .prologue
    .line 1644
    const/4 v1, 0x0

    #@1
    .line 1646
    .local v1, success:Z
    if-ge p2, p1, :cond_b

    #@3
    .line 1647
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@5
    const-string v3, "endMessageId < startMessageId"

    #@7
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v2

    #@b
    .line 1650
    :cond_b
    :try_start_b
    const-string v2, "isms"

    #@d
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@10
    move-result-object v2

    #@11
    invoke-static {v2}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@14
    move-result-object v0

    #@15
    .line 1651
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_1b

    #@17
    .line 1652
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ISms;->disableCdmaBroadcastRange(II)Z
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_1a} :catch_1c

    #@1a
    move-result v1

    #@1b
    .line 1658
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_1b
    :goto_1b
    return v1

    #@1c
    .line 1654
    :catch_1c
    move-exception v2

    #@1d
    goto :goto_1b
.end method

.method public disableCellBroadcast(I)Z
    .registers 5
    .parameter "messageIdentifier"

    #@0
    .prologue
    .line 1452
    const/4 v1, 0x0

    #@1
    .line 1455
    .local v1, success:Z
    :try_start_1
    const-string v2, "isms"

    #@3
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v2

    #@7
    invoke-static {v2}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@a
    move-result-object v0

    #@b
    .line 1456
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_11

    #@d
    .line 1457
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ISms;->disableCellBroadcast(I)Z
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_10} :catch_12

    #@10
    move-result v1

    #@11
    .line 1463
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_11
    :goto_11
    return v1

    #@12
    .line 1459
    :catch_12
    move-exception v2

    #@13
    goto :goto_11
.end method

.method public disableCellBroadcastRange(II)Z
    .registers 7
    .parameter "startMessageId"
    .parameter "endMessageId"

    #@0
    .prologue
    .line 1520
    const/4 v1, 0x0

    #@1
    .line 1522
    .local v1, success:Z
    if-ge p2, p1, :cond_b

    #@3
    .line 1523
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@5
    const-string v3, "endMessageId < startMessageId"

    #@7
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v2

    #@b
    .line 1526
    :cond_b
    :try_start_b
    const-string v2, "isms"

    #@d
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@10
    move-result-object v2

    #@11
    invoke-static {v2}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@14
    move-result-object v0

    #@15
    .line 1527
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_20

    #@17
    .line 1528
    const-string v2, "disableCellBroadcastRange(), SmsManager --> IccSmsInterfaceManager"

    #@19
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->f(Ljava/lang/String;)I

    #@1c
    .line 1529
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ISms;->disableCellBroadcastRange(II)Z
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_1f} :catch_21

    #@1f
    move-result v1

    #@20
    .line 1535
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_20
    :goto_20
    return v1

    #@21
    .line 1531
    :catch_21
    move-exception v2

    #@22
    goto :goto_20
.end method

.method public divideMessage(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 4
    .parameter "text"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 265
    if-nez p1, :cond_a

    #@2
    .line 266
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "text is null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 268
    :cond_a
    invoke-static {p1}, Landroid/telephony/SmsMessage;->fragmentText(Ljava/lang/String;)Ljava/util/ArrayList;

    #@d
    move-result-object v0

    #@e
    return-object v0
.end method

.method public divideMessage(Ljava/lang/String;I)Ljava/util/ArrayList;
    .registers 4
    .parameter "text"
    .parameter "eMailAddrLen"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1916
    invoke-static {p1, p2}, Landroid/telephony/SmsMessage;->fragmentText(Ljava/lang/String;I)Ljava/util/ArrayList;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public divideMessageEx(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 3
    .parameter "text"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 297
    invoke-static {p1}, Landroid/telephony/SmsMessage;->fragmentTextEx(Ljava/lang/String;)Ljava/util/ArrayList;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public divideMessageEx(Ljava/lang/String;IZ)Ljava/util/ArrayList;
    .registers 5
    .parameter "text"
    .parameter "dataCodingScheme"
    .parameter "bReplyAddress"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IZ)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 311
    invoke-static {p1, p2, p3}, Landroid/telephony/SmsMessage;->fragmentTextEx(Ljava/lang/String;IZ)Ljava/util/ArrayList;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public divideMessageForCopyToSIM(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 3
    .parameter "text"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 282
    const/4 v0, 0x1

    #@1
    invoke-static {p1, v0}, Landroid/telephony/SmsMessage;->fragmentText(Ljava/lang/String;Z)Ljava/util/ArrayList;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public enableAutoDCDisconnect(I)V
    .registers 4
    .parameter "timeOut"

    #@0
    .prologue
    .line 1849
    :try_start_0
    const-string v1, "isms"

    #@2
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v1

    #@6
    invoke-static {v1}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@9
    move-result-object v0

    #@a
    .line 1850
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_14

    #@c
    .line 1851
    const-string v1, "enableAutoDCDisconnect(), enableAutoDCDisconnect"

    #@e
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@11
    .line 1852
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ISms;->enableAutoDCDisconnect(I)V
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_14} :catch_15

    #@14
    .line 1857
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_14
    :goto_14
    return-void

    #@15
    .line 1854
    :catch_15
    move-exception v1

    #@16
    goto :goto_14
.end method

.method public enableCdmaBroadcast(I)Z
    .registers 5
    .parameter "messageIdentifier"

    #@0
    .prologue
    .line 1552
    const/4 v1, 0x0

    #@1
    .line 1555
    .local v1, success:Z
    :try_start_1
    const-string v2, "isms"

    #@3
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v2

    #@7
    invoke-static {v2}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@a
    move-result-object v0

    #@b
    .line 1556
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_11

    #@d
    .line 1557
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ISms;->enableCdmaBroadcast(I)Z
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_10} :catch_12

    #@10
    move-result v1

    #@11
    .line 1563
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_11
    :goto_11
    return v1

    #@12
    .line 1559
    :catch_12
    move-exception v2

    #@13
    goto :goto_11
.end method

.method public enableCdmaBroadcastRange(II)Z
    .registers 7
    .parameter "startMessageId"
    .parameter "endMessageId"

    #@0
    .prologue
    .line 1611
    const/4 v1, 0x0

    #@1
    .line 1613
    .local v1, success:Z
    if-ge p2, p1, :cond_b

    #@3
    .line 1614
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@5
    const-string v3, "endMessageId < startMessageId"

    #@7
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v2

    #@b
    .line 1617
    :cond_b
    :try_start_b
    const-string v2, "isms"

    #@d
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@10
    move-result-object v2

    #@11
    invoke-static {v2}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@14
    move-result-object v0

    #@15
    .line 1618
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_1b

    #@17
    .line 1619
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ISms;->enableCdmaBroadcastRange(II)Z
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_1a} :catch_1c

    #@1a
    move-result v1

    #@1b
    .line 1625
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_1b
    :goto_1b
    return v1

    #@1c
    .line 1621
    :catch_1c
    move-exception v2

    #@1d
    goto :goto_1b
.end method

.method public enableCellBroadcast(I)Z
    .registers 5
    .parameter "messageIdentifier"

    #@0
    .prologue
    .line 1422
    const/4 v1, 0x0

    #@1
    .line 1425
    .local v1, success:Z
    :try_start_1
    const-string v2, "isms"

    #@3
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v2

    #@7
    invoke-static {v2}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@a
    move-result-object v0

    #@b
    .line 1426
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_11

    #@d
    .line 1427
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ISms;->enableCellBroadcast(I)Z
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_10} :catch_12

    #@10
    move-result v1

    #@11
    .line 1433
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_11
    :goto_11
    return v1

    #@12
    .line 1429
    :catch_12
    move-exception v2

    #@13
    goto :goto_11
.end method

.method public enableCellBroadcastRange(II)Z
    .registers 7
    .parameter "startMessageId"
    .parameter "endMessageId"

    #@0
    .prologue
    .line 1484
    const/4 v1, 0x0

    #@1
    .line 1486
    .local v1, success:Z
    if-ge p2, p1, :cond_b

    #@3
    .line 1487
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@5
    const-string v3, "endMessageId < startMessageId"

    #@7
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v2

    #@b
    .line 1490
    :cond_b
    :try_start_b
    const-string v2, "isms"

    #@d
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@10
    move-result-object v2

    #@11
    invoke-static {v2}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@14
    move-result-object v0

    #@15
    .line 1491
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_20

    #@17
    .line 1492
    const-string v2, "enableCellBroadcastRange(), SmsManager --> IccSmsInterfaceManager"

    #@19
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->f(Ljava/lang/String;)I

    #@1c
    .line 1493
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ISms;->enableCellBroadcastRange(II)Z
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_1f} :catch_21

    #@1f
    move-result v1

    #@20
    .line 1499
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_20
    :goto_20
    return v1

    #@21
    .line 1495
    :catch_21
    move-exception v2

    #@22
    goto :goto_20
.end method

.method public getAllMessagesFromIcc3GPP2()Ljava/util/ArrayList;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/telephony/SmsMessage;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 995
    const/4 v1, 0x0

    #@1
    .line 996
    .local v1, records:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/SmsRawData;>;"
    const-string v2, "getAllMessagesFromIcc3GPP2(), start"

    #@3
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@6
    .line 998
    :try_start_6
    const-string v2, "isms"

    #@8
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@b
    move-result-object v2

    #@c
    invoke-static {v2}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@f
    move-result-object v0

    #@10
    .line 999
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_16

    #@12
    .line 1000
    invoke-interface {v0}, Lcom/android/internal/telephony/ISms;->getAllMessagesFromIccEf3GPP2()Ljava/util/List;
    :try_end_15
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_15} :catch_1b

    #@15
    move-result-object v1

    #@16
    .line 1006
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_16
    :goto_16
    invoke-static {v1}, Landroid/telephony/SmsManager;->createMessageListFromRawRecords(Ljava/util/List;)Ljava/util/ArrayList;

    #@19
    move-result-object v2

    #@1a
    return-object v2

    #@1b
    .line 1002
    :catch_1b
    move-exception v2

    #@1c
    goto :goto_16
.end method

.method public getCharacterEncoding()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 1838
    const-string v1, "persist.gsm.sms.forcegsm7"

    #@2
    const-string v2, "1"

    #@4
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 1839
    .local v0, encodingType:Ljava/lang/String;
    return-object v0
.end method

.method public getImsSmsFormat()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 1723
    const-string v0, "unknown"

    #@2
    .line 1725
    .local v0, format:Ljava/lang/String;
    :try_start_2
    const-string v2, "isms"

    #@4
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@7
    move-result-object v2

    #@8
    invoke-static {v2}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@b
    move-result-object v1

    #@c
    .line 1726
    .local v1, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v1, :cond_12

    #@e
    .line 1727
    invoke-interface {v1}, Lcom/android/internal/telephony/ISms;->getImsSmsFormat()Ljava/lang/String;
    :try_end_11
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_11} :catch_13

    #@11
    move-result-object v0

    #@12
    .line 1732
    .end local v1           #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_12
    :goto_12
    return-object v0

    #@13
    .line 1729
    :catch_13
    move-exception v2

    #@14
    goto :goto_12
.end method

.method public getMaxEfSms()I
    .registers 6

    #@0
    .prologue
    .line 1756
    const/4 v2, -0x1

    #@1
    .line 1759
    .local v2, maxSms:I
    :try_start_1
    const-string v3, "isms"

    #@3
    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v3

    #@7
    invoke-static {v3}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@a
    move-result-object v1

    #@b
    .line 1760
    .local v1, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v1, :cond_27

    #@d
    .line 1761
    invoke-interface {v1}, Lcom/android/internal/telephony/ISms;->getMaxEfSms()I

    #@10
    move-result v2

    #@11
    .line 1762
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "getMaxEfSms(), maxSms"

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I
    :try_end_27
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_27} :catch_28

    #@27
    .line 1767
    .end local v1           #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_27
    :goto_27
    return v2

    #@28
    .line 1764
    :catch_28
    move-exception v0

    #@29
    .line 1765
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@2c
    goto :goto_27
.end method

.method public getReadItemMessagesFromIccEf(I)Ljava/util/ArrayList;
    .registers 6
    .parameter "item"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/telephony/SmsMessage;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1315
    const/4 v1, 0x0

    #@1
    .line 1316
    .local v1, records:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/SmsRawData;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v3, "getReadItemMessagesFromIccEf(), item = "

    #@8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@17
    .line 1318
    :try_start_17
    const-string v2, "isms"

    #@19
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v2}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@20
    move-result-object v0

    #@21
    .line 1320
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_27

    #@23
    .line 1321
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ISms;->getReadItemMessagesFromIccEf(I)Ljava/util/List;
    :try_end_26
    .catch Landroid/os/RemoteException; {:try_start_17 .. :try_end_26} :catch_2c

    #@26
    move-result-object v1

    #@27
    .line 1327
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_27
    :goto_27
    invoke-static {v1}, Landroid/telephony/SmsManager;->createMessageListFromRawRecords(Ljava/util/List;)Ljava/util/ArrayList;

    #@2a
    move-result-object v2

    #@2b
    return-object v2

    #@2c
    .line 1323
    :catch_2c
    move-exception v2

    #@2d
    goto :goto_27
.end method

.method public getServiceCenterAddress()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 1372
    const-string v1, ""

    #@2
    .line 1374
    .local v1, scaddr:Ljava/lang/String;
    :try_start_2
    const-string v2, "isms"

    #@4
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@7
    move-result-object v2

    #@8
    invoke-static {v2}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@b
    move-result-object v0

    #@c
    .line 1375
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_12

    #@e
    .line 1376
    invoke-interface {v0}, Lcom/android/internal/telephony/ISms;->getSmscenterAddress()Ljava/lang/String;
    :try_end_11
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_11} :catch_13

    #@11
    move-result-object v1

    #@12
    .line 1381
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_12
    :goto_12
    return-object v1

    #@13
    .line 1378
    :catch_13
    move-exception v2

    #@14
    goto :goto_12
.end method

.method public getUiccType()I
    .registers 5

    #@0
    .prologue
    .line 1028
    const/4 v1, -0x1

    #@1
    .line 1029
    .local v1, uiccType:I
    const-string v2, "getUiccType(), start"

    #@3
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@6
    .line 1031
    :try_start_6
    const-string v2, "isms"

    #@8
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@b
    move-result-object v2

    #@c
    invoke-static {v2}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@f
    move-result-object v0

    #@10
    .line 1032
    .local v0, simISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_16

    #@12
    .line 1033
    invoke-interface {v0}, Lcom/android/internal/telephony/ISms;->getUiccType()I
    :try_end_15
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_15} :catch_2d

    #@15
    move-result v1

    #@16
    .line 1038
    .end local v0           #simISms:Lcom/android/internal/telephony/ISms;
    :cond_16
    :goto_16
    new-instance v2, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v3, "getUiccType(), uiccType: "

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@2c
    .line 1039
    return v1

    #@2d
    .line 1035
    :catch_2d
    move-exception v2

    #@2e
    goto :goto_16
.end method

.method public isFdnEnabled()Z
    .registers 4

    #@0
    .prologue
    .line 1866
    :try_start_0
    const-string v2, "phone"

    #@2
    invoke-static {v2}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v2

    #@6
    invoke-static {v2}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    #@9
    move-result-object v1

    #@a
    .line 1867
    .local v1, phone:Lcom/android/internal/telephony/ITelephony;
    if-eqz v1, :cond_17

    #@c
    .line 1868
    invoke-interface {v1}, Lcom/android/internal/telephony/ITelephony;->getIccFdnEnabled()Z
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_f} :catch_11

    #@f
    move-result v2

    #@10
    .line 1873
    .end local v1           #phone:Lcom/android/internal/telephony/ITelephony;
    :goto_10
    return v2

    #@11
    .line 1870
    :catch_11
    move-exception v0

    #@12
    .line 1871
    .local v0, ex:Landroid/os/RemoteException;
    const-string v2, "isFdnEnabled(), RemoteException"

    #@14
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@17
    .line 1873
    .end local v0           #ex:Landroid/os/RemoteException;
    :cond_17
    const/4 v2, 0x0

    #@18
    goto :goto_10
.end method

.method public isGsmMo()Z
    .registers 4

    #@0
    .prologue
    .line 834
    const/4 v1, 0x0

    #@1
    .line 837
    .local v1, isGsmEncoding:Z
    :try_start_1
    const-string v2, "isms"

    #@3
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v2

    #@7
    invoke-static {v2}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@a
    move-result-object v0

    #@b
    .line 838
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_11

    #@d
    .line 839
    invoke-interface {v0}, Lcom/android/internal/telephony/ISms;->isGsmMo()Z
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_10} :catch_12

    #@10
    move-result v1

    #@11
    .line 845
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_11
    :goto_11
    return v1

    #@12
    .line 841
    :catch_12
    move-exception v2

    #@13
    goto :goto_11
.end method

.method isImsSmsSupported()Z
    .registers 4

    #@0
    .prologue
    .line 1696
    const/4 v0, 0x0

    #@1
    .line 1698
    .local v0, boSupported:Z
    :try_start_1
    const-string v2, "isms"

    #@3
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v2

    #@7
    invoke-static {v2}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@a
    move-result-object v1

    #@b
    .line 1699
    .local v1, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v1, :cond_11

    #@d
    .line 1700
    invoke-interface {v1}, Lcom/android/internal/telephony/ISms;->isImsSmsSupported()Z
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_10} :catch_12

    #@10
    move-result v0

    #@11
    .line 1705
    .end local v1           #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_11
    :goto_11
    return v0

    #@12
    .line 1702
    :catch_12
    move-exception v2

    #@13
    goto :goto_11
.end method

.method public makeParts(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .registers 10
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1956
    .local p1, parts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v4, 0x0

    #@1
    .line 1957
    .local v4, newParts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v0, Ljava/lang/String;

    #@3
    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    #@6
    .line 1958
    .local v0, combinedString:Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v3

    #@a
    .local v3, i$:Ljava/util/Iterator;
    :goto_a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v6

    #@e
    if-eqz v6, :cond_28

    #@10
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v5

    #@14
    check-cast v5, Ljava/lang/String;

    #@16
    .line 1959
    .local v5, partString:Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v6

    #@1f
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v6

    #@23
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    goto :goto_a

    #@28
    .line 1961
    .end local v5           #partString:Ljava/lang/String;
    :cond_28
    invoke-virtual {p0, v0}, Landroid/telephony/SmsManager;->divideMessageEx(Ljava/lang/String;)Ljava/util/ArrayList;

    #@2b
    move-result-object v4

    #@2c
    .line 1962
    if-nez v4, :cond_33

    #@2e
    .line 1963
    new-instance v4, Ljava/util/ArrayList;

    #@30
    .end local v4           #newParts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@33
    .line 1966
    .restart local v4       #newParts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_33
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@36
    move-result v6

    #@37
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@3a
    move-result v7

    #@3b
    sub-int v1, v6, v7

    #@3d
    .line 1967
    .local v1, diff:I
    if-lez v1, :cond_4e

    #@3f
    .line 1968
    move v2, v1

    #@40
    .local v2, i:I
    :goto_40
    if-lez v2, :cond_5b

    #@42
    .line 1969
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@45
    move-result v6

    #@46
    add-int/lit8 v6, v6, -0x1

    #@48
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@4b
    .line 1968
    add-int/lit8 v2, v2, -0x1

    #@4d
    goto :goto_40

    #@4e
    .line 1971
    .end local v2           #i:I
    :cond_4e
    if-gez v1, :cond_5b

    #@50
    .line 1972
    move v2, v1

    #@51
    .restart local v2       #i:I
    :goto_51
    if-gez v2, :cond_5b

    #@53
    .line 1973
    const-string v6, " "

    #@55
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@58
    .line 1972
    add-int/lit8 v2, v2, 0x1

    #@5a
    goto :goto_51

    #@5b
    .line 1976
    .end local v2           #i:I
    :cond_5b
    new-instance v6, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string v7, "makeParts(), parts = "

    #@62
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v6

    #@66
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v6

    #@6a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v6

    #@6e
    invoke-static {v6}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@71
    .line 1977
    new-instance v6, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v7, "makeParts(), newParts = "

    #@78
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v6

    #@7c
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v6

    #@80
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v6

    #@84
    invoke-static {v6}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@87
    .line 1978
    return-object v4
.end method

.method public removeAllMessagesFromIcc()I
    .registers 10

    #@0
    .prologue
    .line 1338
    const/4 v6, 0x0

    #@1
    .line 1339
    .local v6, records:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/SmsRawData;>;"
    const/4 v1, 0x0

    #@2
    .line 1340
    .local v1, deleteCount:I
    const-string v7, "removeAllMessagesFromIcc(), start"

    #@4
    invoke-static {v7}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@7
    .line 1342
    :try_start_7
    const-string v7, "isms"

    #@9
    invoke-static {v7}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@c
    move-result-object v7

    #@d
    invoke-static {v7}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@10
    move-result-object v5

    #@11
    .line 1343
    .local v5, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v5, :cond_17

    #@13
    .line 1344
    invoke-interface {v5}, Lcom/android/internal/telephony/ISms;->getAllMessagesFromIccEf()Ljava/util/List;
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_16} :catch_2c

    #@16
    move-result-object v6

    #@17
    .line 1350
    :cond_17
    if-eqz v6, :cond_2f

    #@19
    .line 1351
    invoke-interface {v6}, Ljava/util/List;->size()I

    #@1c
    move-result v0

    #@1d
    .line 1352
    .local v0, count:I
    const/4 v4, 0x0

    #@1e
    .local v4, i:I
    :goto_1e
    if-ge v4, v0, :cond_2f

    #@20
    .line 1353
    invoke-virtual {p0, v4}, Landroid/telephony/SmsManager;->deleteMessageFromIcc(I)Z

    #@23
    move-result v7

    #@24
    const/4 v8, 0x1

    #@25
    if-ne v7, v8, :cond_29

    #@27
    .line 1354
    add-int/lit8 v1, v1, 0x1

    #@29
    .line 1352
    :cond_29
    add-int/lit8 v4, v4, 0x1

    #@2b
    goto :goto_1e

    #@2c
    .line 1346
    .end local v0           #count:I
    .end local v4           #i:I
    .end local v5           #iccISms:Lcom/android/internal/telephony/ISms;
    :catch_2c
    move-exception v3

    #@2d
    .local v3, ex:Landroid/os/RemoteException;
    move v2, v1

    #@2e
    .line 1359
    .end local v1           #deleteCount:I
    .end local v3           #ex:Landroid/os/RemoteException;
    .local v2, deleteCount:I
    :goto_2e
    return v2

    #@2f
    .line 1358
    .end local v2           #deleteCount:I
    .restart local v1       #deleteCount:I
    .restart local v5       #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_2f
    new-instance v7, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v8, "removeAllMessagesFromIcc(), deleteCount = "

    #@36
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v7

    #@3a
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v7

    #@3e
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v7

    #@42
    invoke-static {v7}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@45
    move v2, v1

    #@46
    .line 1359
    .end local v1           #deleteCount:I
    .restart local v2       #deleteCount:I
    goto :goto_2e
.end method

.method public sendDataMessage(Ljava/lang/String;Ljava/lang/String;S[BLandroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    .registers 15
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter "destinationPort"
    .parameter "data"
    .parameter "sentIntent"
    .parameter "deliveryIntent"

    #@0
    .prologue
    .line 769
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@3
    move-result-object v1

    #@4
    if-eqz v1, :cond_17

    #@6
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@9
    move-result-object v1

    #@a
    const/4 v2, 0x0

    #@b
    invoke-interface {v1, v2, p5}, Lcom/lge/cappuccino/IMdm;->isAllowSendMessage(Landroid/content/ComponentName;Landroid/app/PendingIntent;)Z

    #@e
    move-result v1

    #@f
    if-nez v1, :cond_17

    #@11
    .line 771
    const-string v1, "sendDataMessage(), Block Sending SMS in SMSManager3"

    #@13
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@16
    .line 799
    :goto_16
    return-void

    #@17
    .line 775
    :cond_17
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1a
    move-result v1

    #@1b
    if-eqz v1, :cond_25

    #@1d
    .line 776
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@1f
    const-string v2, "Invalid destinationAddress"

    #@21
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@24
    throw v1

    #@25
    .line 779
    :cond_25
    if-eqz p4, :cond_2a

    #@27
    array-length v1, p4

    #@28
    if-nez v1, :cond_32

    #@2a
    .line 780
    :cond_2a
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@2c
    const-string v2, "Invalid message data"

    #@2e
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@31
    throw v1

    #@32
    .line 784
    :cond_32
    :try_start_32
    const-string v1, "isms"

    #@34
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@37
    move-result-object v1

    #@38
    invoke-static {v1}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@3b
    move-result-object v0

    #@3c
    .line 785
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_53

    #@3e
    .line 786
    const v1, 0xffff

    #@41
    and-int v3, p3, v1

    #@43
    move-object v1, p1

    #@44
    move-object v2, p2

    #@45
    move-object v4, p4

    #@46
    move-object v5, p5

    #@47
    move-object v6, p6

    #@48
    invoke-interface/range {v0 .. v6}, Lcom/android/internal/telephony/ISms;->sendData(Ljava/lang/String;Ljava/lang/String;I[BLandroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    :try_end_4b
    .catch Landroid/os/RemoteException; {:try_start_32 .. :try_end_4b} :catch_4c

    #@4b
    goto :goto_16

    #@4c
    .line 793
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :catch_4c
    move-exception v7

    #@4d
    .line 796
    .local v7, ex:Landroid/os/RemoteException;
    const-string v1, "sendDataMessage(), RemoteException"

    #@4f
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@52
    goto :goto_16

    #@53
    .line 790
    .end local v7           #ex:Landroid/os/RemoteException;
    .restart local v0       #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_53
    const/4 v1, 0x1

    #@54
    :try_start_54
    invoke-static {p5, v1}, Landroid/telephony/SmsManager;->sendExceptionbySentIntent(Landroid/app/PendingIntent;I)V
    :try_end_57
    .catch Landroid/os/RemoteException; {:try_start_54 .. :try_end_57} :catch_4c

    #@57
    goto :goto_16
.end method

.method public sendEmailoverMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .registers 13
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p3, parts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, sentIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .local p5, deliveryIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 694
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_10

    #@8
    .line 695
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@a
    const-string v2, "Invalid destinationAddress"

    #@c
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v1

    #@10
    .line 697
    :cond_10
    if-eqz p3, :cond_18

    #@12
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    #@15
    move-result v1

    #@16
    if-ge v1, v3, :cond_20

    #@18
    .line 698
    :cond_18
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@1a
    const-string v2, "Invalid message body"

    #@1c
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v1

    #@20
    .line 701
    :cond_20
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    #@23
    move-result v1

    #@24
    if-le v1, v3, :cond_57

    #@26
    .line 703
    :try_start_26
    const-string v1, "isms"

    #@28
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@2b
    move-result-object v1

    #@2c
    invoke-static {v1}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@2f
    move-result-object v0

    #@30
    .line 704
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_56

    #@32
    .line 706
    const/4 v1, 0x0

    #@33
    const-string v2, "cdma_priority_indicator"

    #@35
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@38
    move-result v1

    #@39
    if-eqz v1, :cond_40

    #@3b
    .line 707
    sget v1, Landroid/telephony/SmsManager;->mSubmitPriority:I

    #@3d
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/ISms;->setSmsPriority(I)V

    #@40
    .line 712
    :cond_40
    const/4 v1, 0x0

    #@41
    const-string v2, "support_sprint_sms_roaming_guard"

    #@43
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@46
    move-result v1

    #@47
    if-eqz v1, :cond_4e

    #@49
    .line 713
    sget-boolean v1, Landroid/telephony/SmsManager;->mSubmitIsRoaming:Z

    #@4b
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/ISms;->setSmsIsRoaming(Z)V

    #@4e
    :cond_4e
    move-object v1, p1

    #@4f
    move-object v2, p2

    #@50
    move-object v3, p3

    #@51
    move-object v4, p4

    #@52
    move-object v5, p5

    #@53
    .line 717
    invoke-interface/range {v0 .. v5}, Lcom/android/internal/telephony/ISms;->sendEmailoverMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    :try_end_56
    .catch Landroid/os/RemoteException; {:try_start_26 .. :try_end_56} :catch_82

    #@56
    .line 735
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_56
    :goto_56
    return-void

    #@57
    .line 724
    :cond_57
    const/4 v5, 0x0

    #@58
    .line 725
    .local v5, sentIntent:Landroid/app/PendingIntent;
    const/4 v6, 0x0

    #@59
    .line 726
    .local v6, deliveryIntent:Landroid/app/PendingIntent;
    if-eqz p4, :cond_67

    #@5b
    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    #@5e
    move-result v1

    #@5f
    if-lez v1, :cond_67

    #@61
    .line 727
    invoke-virtual {p4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@64
    move-result-object v5

    #@65
    .end local v5           #sentIntent:Landroid/app/PendingIntent;
    check-cast v5, Landroid/app/PendingIntent;

    #@67
    .line 729
    .restart local v5       #sentIntent:Landroid/app/PendingIntent;
    :cond_67
    if-eqz p5, :cond_75

    #@69
    invoke-virtual {p5}, Ljava/util/ArrayList;->size()I

    #@6c
    move-result v1

    #@6d
    if-lez v1, :cond_75

    #@6f
    .line 730
    invoke-virtual {p5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@72
    move-result-object v6

    #@73
    .end local v6           #deliveryIntent:Landroid/app/PendingIntent;
    check-cast v6, Landroid/app/PendingIntent;

    #@75
    .line 732
    .restart local v6       #deliveryIntent:Landroid/app/PendingIntent;
    :cond_75
    invoke-virtual {p3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@78
    move-result-object v4

    #@79
    check-cast v4, Ljava/lang/String;

    #@7b
    move-object v1, p0

    #@7c
    move-object v2, p1

    #@7d
    move-object v3, p2

    #@7e
    invoke-virtual/range {v1 .. v6}, Landroid/telephony/SmsManager;->sendEmailoverTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    #@81
    goto :goto_56

    #@82
    .line 720
    .end local v5           #sentIntent:Landroid/app/PendingIntent;
    .end local v6           #deliveryIntent:Landroid/app/PendingIntent;
    :catch_82
    move-exception v1

    #@83
    goto :goto_56
.end method

.method public sendEmailoverTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    .registers 12
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 217
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@4
    move-result-object v1

    #@5
    if-eqz v1, :cond_17

    #@7
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@a
    move-result-object v1

    #@b
    invoke-interface {v1, v2, p4}, Lcom/lge/cappuccino/IMdm;->isAllowSendMessage(Landroid/content/ComponentName;Landroid/app/PendingIntent;)Z

    #@e
    move-result v1

    #@f
    if-nez v1, :cond_17

    #@11
    .line 219
    const-string v1, "sendEmailoverTextMessage(), Block Sending SMS in SMSManager1"

    #@13
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@16
    .line 251
    :cond_16
    :goto_16
    return-void

    #@17
    .line 223
    :cond_17
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1a
    move-result v1

    #@1b
    if-eqz v1, :cond_25

    #@1d
    .line 224
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@1f
    const-string v2, "Invalid destinationAddress"

    #@21
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@24
    throw v1

    #@25
    .line 227
    :cond_25
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@28
    move-result v1

    #@29
    if-eqz v1, :cond_33

    #@2b
    .line 228
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@2d
    const-string v2, "Invalid message body"

    #@2f
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@32
    throw v1

    #@33
    .line 232
    :cond_33
    :try_start_33
    const-string v1, "isms"

    #@35
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@38
    move-result-object v1

    #@39
    invoke-static {v1}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@3c
    move-result-object v0

    #@3d
    .line 233
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_16

    #@3f
    .line 235
    const/4 v1, 0x0

    #@40
    const-string v2, "cdma_priority_indicator"

    #@42
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@45
    move-result v1

    #@46
    if-eqz v1, :cond_4d

    #@48
    .line 236
    sget v1, Landroid/telephony/SmsManager;->mSubmitPriority:I

    #@4a
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/ISms;->setSmsPriority(I)V

    #@4d
    .line 241
    :cond_4d
    const/4 v1, 0x0

    #@4e
    const-string v2, "support_sprint_sms_roaming_guard"

    #@50
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@53
    move-result v1

    #@54
    if-eqz v1, :cond_5b

    #@56
    .line 242
    sget-boolean v1, Landroid/telephony/SmsManager;->mSubmitIsRoaming:Z

    #@58
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/ISms;->setSmsIsRoaming(Z)V

    #@5b
    :cond_5b
    move-object v1, p1

    #@5c
    move-object v2, p2

    #@5d
    move-object v3, p3

    #@5e
    move-object v4, p4

    #@5f
    move-object v5, p5

    #@60
    .line 246
    invoke-interface/range {v0 .. v5}, Lcom/android/internal/telephony/ISms;->sendEmailoverText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    :try_end_63
    .catch Landroid/os/RemoteException; {:try_start_33 .. :try_end_63} :catch_64

    #@63
    goto :goto_16

    #@64
    .line 248
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :catch_64
    move-exception v1

    #@65
    goto :goto_16
.end method

.method public sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .registers 14
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p3, parts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, sentIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .local p5, deliveryIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    const/4 v2, 0x0

    #@3
    .line 351
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@6
    move-result-object v1

    #@7
    if-eqz v1, :cond_19

    #@9
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@c
    move-result-object v1

    #@d
    invoke-interface {v1, v2, p4}, Lcom/lge/cappuccino/IMdm;->isAllowSendMessage(Landroid/content/ComponentName;Ljava/util/ArrayList;)Z

    #@10
    move-result v1

    #@11
    if-nez v1, :cond_19

    #@13
    .line 353
    const-string v1, "sendMultipartTextMessage(), Block Sending SMS in SMSManager2"

    #@15
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@18
    .line 417
    :goto_18
    return-void

    #@19
    .line 357
    :cond_19
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1c
    move-result v1

    #@1d
    if-eqz v1, :cond_27

    #@1f
    .line 358
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@21
    const-string v2, "Invalid destinationAddress"

    #@23
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@26
    throw v1

    #@27
    .line 360
    :cond_27
    if-eqz p3, :cond_2f

    #@29
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    #@2c
    move-result v1

    #@2d
    if-ge v1, v4, :cond_37

    #@2f
    .line 361
    :cond_2f
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@31
    const-string v2, "Invalid message body"

    #@33
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@36
    throw v1

    #@37
    .line 365
    :cond_37
    const-string v1, "MakePartsSendConcatMessage"

    #@39
    invoke-static {v2, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@3c
    move-result v1

    #@3d
    if-eqz v1, :cond_43

    #@3f
    .line 366
    invoke-virtual {p0, p3}, Landroid/telephony/SmsManager;->makeParts(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    #@42
    move-result-object p3

    #@43
    .line 370
    :cond_43
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    #@46
    move-result v1

    #@47
    if-le v1, v4, :cond_94

    #@49
    .line 372
    :try_start_49
    const-string v1, "isms"

    #@4b
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@4e
    move-result-object v1

    #@4f
    invoke-static {v1}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@52
    move-result-object v0

    #@53
    .line 373
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_8f

    #@55
    .line 375
    sget v1, Landroid/telephony/SmsManager;->vp:I

    #@57
    if-lez v1, :cond_5e

    #@59
    .line 376
    sget v1, Landroid/telephony/SmsManager;->vp:I

    #@5b
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/ISms;->setMultipartTextValidityPeriod(I)V

    #@5e
    .line 380
    :cond_5e
    const/4 v1, 0x0

    #@5f
    const-string v2, "cdma_priority_indicator"

    #@61
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@64
    move-result v1

    #@65
    if-eqz v1, :cond_6c

    #@67
    .line 381
    sget v1, Landroid/telephony/SmsManager;->mSubmitPriority:I

    #@69
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/ISms;->setSmsPriority(I)V

    #@6c
    .line 386
    :cond_6c
    const/4 v1, 0x0

    #@6d
    const-string v2, "support_sprint_sms_roaming_guard"

    #@6f
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@72
    move-result v1

    #@73
    if-eqz v1, :cond_7a

    #@75
    .line 387
    sget-boolean v1, Landroid/telephony/SmsManager;->mSubmitIsRoaming:Z

    #@77
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/ISms;->setSmsIsRoaming(Z)V

    #@7a
    .line 391
    :cond_7a
    const-string v1, "sendMultipartTextMessage(), SmsManager --> IccSmsInterfaceManager"

    #@7c
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->f(Ljava/lang/String;)I

    #@7f
    move-object v1, p1

    #@80
    move-object v2, p2

    #@81
    move-object v3, p3

    #@82
    move-object v4, p4

    #@83
    move-object v5, p5

    #@84
    .line 392
    invoke-interface/range {v0 .. v5}, Lcom/android/internal/telephony/ISms;->sendMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    :try_end_87
    .catch Landroid/os/RemoteException; {:try_start_49 .. :try_end_87} :catch_88

    #@87
    goto :goto_18

    #@88
    .line 399
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :catch_88
    move-exception v7

    #@89
    .line 402
    .local v7, ex:Landroid/os/RemoteException;
    const-string v1, "sendMultipartTextMessage(), RemoteException"

    #@8b
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@8e
    goto :goto_18

    #@8f
    .line 396
    .end local v7           #ex:Landroid/os/RemoteException;
    .restart local v0       #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_8f
    const/4 v1, 0x1

    #@90
    :try_start_90
    invoke-static {p4, v1}, Landroid/telephony/SmsManager;->sendExceptionbySentIntent(Ljava/util/ArrayList;I)V
    :try_end_93
    .catch Landroid/os/RemoteException; {:try_start_90 .. :try_end_93} :catch_88

    #@93
    goto :goto_18

    #@94
    .line 406
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_94
    const/4 v5, 0x0

    #@95
    .line 407
    .local v5, sentIntent:Landroid/app/PendingIntent;
    const/4 v6, 0x0

    #@96
    .line 408
    .local v6, deliveryIntent:Landroid/app/PendingIntent;
    if-eqz p4, :cond_a4

    #@98
    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    #@9b
    move-result v1

    #@9c
    if-lez v1, :cond_a4

    #@9e
    .line 409
    invoke-virtual {p4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@a1
    move-result-object v5

    #@a2
    .end local v5           #sentIntent:Landroid/app/PendingIntent;
    check-cast v5, Landroid/app/PendingIntent;

    #@a4
    .line 411
    .restart local v5       #sentIntent:Landroid/app/PendingIntent;
    :cond_a4
    if-eqz p5, :cond_b2

    #@a6
    invoke-virtual {p5}, Ljava/util/ArrayList;->size()I

    #@a9
    move-result v1

    #@aa
    if-lez v1, :cond_b2

    #@ac
    .line 412
    invoke-virtual {p5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@af
    move-result-object v6

    #@b0
    .end local v6           #deliveryIntent:Landroid/app/PendingIntent;
    check-cast v6, Landroid/app/PendingIntent;

    #@b2
    .line 414
    .restart local v6       #deliveryIntent:Landroid/app/PendingIntent;
    :cond_b2
    invoke-virtual {p3, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@b5
    move-result-object v4

    #@b6
    check-cast v4, Ljava/lang/String;

    #@b8
    move-object v1, p0

    #@b9
    move-object v2, p1

    #@ba
    move-object v3, p2

    #@bb
    invoke-virtual/range {v1 .. v6}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    #@be
    goto/16 :goto_18
.end method

.method public sendMultipartTextMessageLge(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;)V
    .registers 17
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter
    .parameter
    .parameter
    .parameter "replyAddress"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 467
    .local p3, parts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, sentIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .local p5, deliveryIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    const/4 v7, -0x1

    #@1
    const/4 v8, 0x0

    #@2
    const/4 v9, 0x0

    #@3
    move-object v0, p0

    #@4
    move-object v1, p1

    #@5
    move-object v2, p2

    #@6
    move-object v3, p3

    #@7
    move-object v4, p4

    #@8
    move-object v5, p5

    #@9
    move-object/from16 v6, p6

    #@b
    invoke-virtual/range {v0 .. v9}, Landroid/telephony/SmsManager;->sendMultipartTextMessageLge(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;III)V

    #@e
    .line 468
    return-void
.end method

.method public sendMultipartTextMessageLge(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;III)V
    .registers 23
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter
    .parameter
    .parameter
    .parameter "replyAddress"
    .parameter "confirmRead"
    .parameter "replyOption"
    .parameter "protocolId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/lang/String;",
            "III)V"
        }
    .end annotation

    #@0
    .prologue
    .line 476
    .local p3, parts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, sentIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .local p5, deliveryIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v2

    #@4
    if-eqz v2, :cond_e

    #@6
    .line 477
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v3, "Invalid destinationAddress"

    #@a
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v2

    #@e
    .line 479
    :cond_e
    if-eqz p3, :cond_17

    #@10
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    #@13
    move-result v2

    #@14
    const/4 v3, 0x1

    #@15
    if-ge v2, v3, :cond_1f

    #@17
    .line 480
    :cond_17
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@19
    const-string v3, "Invalid message body"

    #@1b
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v2

    #@1f
    .line 484
    :cond_1f
    const/4 v2, 0x0

    #@20
    const-string v3, "MakePartsSendConcatMessage"

    #@22
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@25
    move-result v2

    #@26
    if-eqz v2, :cond_2e

    #@28
    .line 485
    move-object/from16 v0, p3

    #@2a
    invoke-virtual {p0, v0}, Landroid/telephony/SmsManager;->makeParts(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    #@2d
    move-result-object p3

    #@2e
    .line 489
    :cond_2e
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    #@31
    move-result v2

    #@32
    const/4 v3, 0x1

    #@33
    if-le v2, v3, :cond_6c

    #@35
    .line 492
    :try_start_35
    const-string v2, "isms"

    #@37
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@3a
    move-result-object v2

    #@3b
    invoke-static {v2}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@3e
    move-result-object v1

    #@3f
    .line 493
    .local v1, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v1, :cond_5e

    #@41
    .line 495
    sget v2, Landroid/telephony/SmsManager;->vp:I

    #@43
    if-lez v2, :cond_4a

    #@45
    .line 496
    sget v2, Landroid/telephony/SmsManager;->vp:I

    #@47
    invoke-interface {v1, v2}, Lcom/android/internal/telephony/ISms;->setMultipartTextValidityPeriod(I)V

    #@4a
    :cond_4a
    move-object v2, p1

    #@4b
    move-object v3, p2

    #@4c
    move-object/from16 v4, p3

    #@4e
    move-object/from16 v5, p4

    #@50
    move-object/from16 v6, p5

    #@52
    move-object/from16 v7, p6

    #@54
    move/from16 v8, p7

    #@56
    move/from16 v9, p8

    #@58
    move/from16 v10, p9

    #@5a
    .line 498
    invoke-interface/range {v1 .. v10}, Lcom/android/internal/telephony/ISms;->sendMultipartTextLge(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;III)V

    #@5d
    .line 525
    .end local v1           #iccISms:Lcom/android/internal/telephony/ISms;
    :goto_5d
    return-void

    #@5e
    .line 502
    .restart local v1       #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_5e
    const/4 v2, 0x1

    #@5f
    move-object/from16 v0, p4

    #@61
    invoke-static {v0, v2}, Landroid/telephony/SmsManager;->sendExceptionbySentIntent(Ljava/util/ArrayList;I)V
    :try_end_64
    .catch Landroid/os/RemoteException; {:try_start_35 .. :try_end_64} :catch_65

    #@64
    goto :goto_5d

    #@65
    .line 505
    .end local v1           #iccISms:Lcom/android/internal/telephony/ISms;
    :catch_65
    move-exception v12

    #@66
    .line 508
    .local v12, ex:Landroid/os/RemoteException;
    const-string v2, "sendMultipartTextMessageLge(), RemoteException"

    #@68
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@6b
    goto :goto_5d

    #@6c
    .line 513
    .end local v12           #ex:Landroid/os/RemoteException;
    :cond_6c
    const/4 v6, 0x0

    #@6d
    .line 514
    .local v6, sentIntent:Landroid/app/PendingIntent;
    const/4 v7, 0x0

    #@6e
    .line 515
    .local v7, deliveryIntent:Landroid/app/PendingIntent;
    if-eqz p4, :cond_7f

    #@70
    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    #@73
    move-result v2

    #@74
    if-lez v2, :cond_7f

    #@76
    .line 516
    const/4 v2, 0x0

    #@77
    move-object/from16 v0, p4

    #@79
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@7c
    move-result-object v6

    #@7d
    .end local v6           #sentIntent:Landroid/app/PendingIntent;
    check-cast v6, Landroid/app/PendingIntent;

    #@7f
    .line 518
    .restart local v6       #sentIntent:Landroid/app/PendingIntent;
    :cond_7f
    if-eqz p5, :cond_90

    #@81
    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->size()I

    #@84
    move-result v2

    #@85
    if-lez v2, :cond_90

    #@87
    .line 519
    const/4 v2, 0x0

    #@88
    move-object/from16 v0, p5

    #@8a
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@8d
    move-result-object v7

    #@8e
    .end local v7           #deliveryIntent:Landroid/app/PendingIntent;
    check-cast v7, Landroid/app/PendingIntent;

    #@90
    .line 521
    .restart local v7       #deliveryIntent:Landroid/app/PendingIntent;
    :cond_90
    const-string v2, "sendMultipartTextMessageLge(), sendMultipartTextMessageLge > sendTextMessageLge"

    #@92
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@95
    .line 522
    const/4 v2, 0x0

    #@96
    move-object/from16 v0, p3

    #@98
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@9b
    move-result-object v5

    #@9c
    check-cast v5, Ljava/lang/String;

    #@9e
    move-object v2, p0

    #@9f
    move-object v3, p1

    #@a0
    move-object v4, p2

    #@a1
    move-object/from16 v8, p6

    #@a3
    move/from16 v9, p7

    #@a5
    move/from16 v10, p8

    #@a7
    move/from16 v11, p9

    #@a9
    invoke-virtual/range {v2 .. v11}, Landroid/telephony/SmsManager;->sendTextMessageLge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;III)V

    #@ac
    goto :goto_5d
.end method

.method public sendMultipartTextMessageMoreLge(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;)V
    .registers 17
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter
    .parameter
    .parameter
    .parameter "replyAddress"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 576
    .local p3, parts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, sentIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .local p5, deliveryIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    const/4 v7, -0x1

    #@1
    const/4 v8, 0x0

    #@2
    const/4 v9, 0x0

    #@3
    move-object v0, p0

    #@4
    move-object v1, p1

    #@5
    move-object v2, p2

    #@6
    move-object v3, p3

    #@7
    move-object v4, p4

    #@8
    move-object v5, p5

    #@9
    move-object/from16 v6, p6

    #@b
    invoke-virtual/range {v0 .. v9}, Landroid/telephony/SmsManager;->sendMultipartTextMessageMoreLge(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;III)V

    #@e
    .line 577
    return-void
.end method

.method public sendMultipartTextMessageMoreLge(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;III)V
    .registers 23
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter
    .parameter
    .parameter
    .parameter "replyAddress"
    .parameter "confirmRead"
    .parameter "replyOption"
    .parameter "protocolId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/lang/String;",
            "III)V"
        }
    .end annotation

    #@0
    .prologue
    .line 584
    .local p3, parts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, sentIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .local p5, deliveryIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v2

    #@4
    if-eqz v2, :cond_e

    #@6
    .line 585
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v3, "Invalid destinationAddress"

    #@a
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v2

    #@e
    .line 587
    :cond_e
    if-eqz p3, :cond_17

    #@10
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    #@13
    move-result v2

    #@14
    const/4 v3, 0x1

    #@15
    if-ge v2, v3, :cond_1f

    #@17
    .line 588
    :cond_17
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@19
    const-string v3, "Invalid message body"

    #@1b
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v2

    #@1f
    .line 592
    :cond_1f
    const/4 v2, 0x0

    #@20
    const-string v3, "MakePartsSendConcatMessage"

    #@22
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@25
    move-result v2

    #@26
    if-eqz v2, :cond_2e

    #@28
    .line 593
    move-object/from16 v0, p3

    #@2a
    invoke-virtual {p0, v0}, Landroid/telephony/SmsManager;->makeParts(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    #@2d
    move-result-object p3

    #@2e
    .line 597
    :cond_2e
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    #@31
    move-result v2

    #@32
    const/4 v3, 0x1

    #@33
    if-le v2, v3, :cond_63

    #@35
    .line 599
    :try_start_35
    const-string v2, "isms"

    #@37
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@3a
    move-result-object v2

    #@3b
    invoke-static {v2}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@3e
    move-result-object v1

    #@3f
    .line 600
    .local v1, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v1, :cond_55

    #@41
    move-object v2, p1

    #@42
    move-object v3, p2

    #@43
    move-object/from16 v4, p3

    #@45
    move-object/from16 v5, p4

    #@47
    move-object/from16 v6, p5

    #@49
    move-object/from16 v7, p6

    #@4b
    move/from16 v8, p7

    #@4d
    move/from16 v9, p8

    #@4f
    move/from16 v10, p9

    #@51
    .line 601
    invoke-interface/range {v1 .. v10}, Lcom/android/internal/telephony/ISms;->sendMultipartTextMoreLge(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;III)V

    #@54
    .line 626
    .end local v1           #iccISms:Lcom/android/internal/telephony/ISms;
    :goto_54
    return-void

    #@55
    .line 605
    .restart local v1       #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_55
    const/4 v2, 0x1

    #@56
    move-object/from16 v0, p4

    #@58
    invoke-static {v0, v2}, Landroid/telephony/SmsManager;->sendExceptionbySentIntent(Ljava/util/ArrayList;I)V
    :try_end_5b
    .catch Landroid/os/RemoteException; {:try_start_35 .. :try_end_5b} :catch_5c

    #@5b
    goto :goto_54

    #@5c
    .line 608
    .end local v1           #iccISms:Lcom/android/internal/telephony/ISms;
    :catch_5c
    move-exception v12

    #@5d
    .line 611
    .local v12, ex:Landroid/os/RemoteException;
    const-string v2, "sendMultipartTextMessageMoreLge(), RemoteException"

    #@5f
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@62
    goto :goto_54

    #@63
    .line 615
    .end local v12           #ex:Landroid/os/RemoteException;
    :cond_63
    const/4 v6, 0x0

    #@64
    .line 616
    .local v6, sentIntent:Landroid/app/PendingIntent;
    const/4 v7, 0x0

    #@65
    .line 617
    .local v7, deliveryIntent:Landroid/app/PendingIntent;
    if-eqz p4, :cond_76

    #@67
    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    #@6a
    move-result v2

    #@6b
    if-lez v2, :cond_76

    #@6d
    .line 618
    const/4 v2, 0x0

    #@6e
    move-object/from16 v0, p4

    #@70
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@73
    move-result-object v6

    #@74
    .end local v6           #sentIntent:Landroid/app/PendingIntent;
    check-cast v6, Landroid/app/PendingIntent;

    #@76
    .line 620
    .restart local v6       #sentIntent:Landroid/app/PendingIntent;
    :cond_76
    if-eqz p5, :cond_87

    #@78
    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->size()I

    #@7b
    move-result v2

    #@7c
    if-lez v2, :cond_87

    #@7e
    .line 621
    const/4 v2, 0x0

    #@7f
    move-object/from16 v0, p5

    #@81
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@84
    move-result-object v7

    #@85
    .end local v7           #deliveryIntent:Landroid/app/PendingIntent;
    check-cast v7, Landroid/app/PendingIntent;

    #@87
    .line 623
    .restart local v7       #deliveryIntent:Landroid/app/PendingIntent;
    :cond_87
    const/4 v2, 0x0

    #@88
    move-object/from16 v0, p3

    #@8a
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@8d
    move-result-object v5

    #@8e
    check-cast v5, Ljava/lang/String;

    #@90
    move-object v2, p0

    #@91
    move-object v3, p1

    #@92
    move-object v4, p2

    #@93
    move-object/from16 v8, p6

    #@95
    move/from16 v9, p7

    #@97
    move/from16 v10, p8

    #@99
    move/from16 v11, p9

    #@9b
    invoke-virtual/range {v2 .. v11}, Landroid/telephony/SmsManager;->sendTextMessageMoreLge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;III)V

    #@9e
    goto :goto_54
.end method

.method public sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    .registers 13
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 111
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@4
    move-result-object v1

    #@5
    if-eqz v1, :cond_17

    #@7
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@a
    move-result-object v1

    #@b
    invoke-interface {v1, v2, p4}, Lcom/lge/cappuccino/IMdm;->isAllowSendMessage(Landroid/content/ComponentName;Landroid/app/PendingIntent;)Z

    #@e
    move-result v1

    #@f
    if-nez v1, :cond_17

    #@11
    .line 113
    const-string v1, "sendTextMessage(), Block Sending SMS in SMSManager1"

    #@13
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@16
    .line 161
    :goto_16
    return-void

    #@17
    .line 117
    :cond_17
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1a
    move-result v1

    #@1b
    if-eqz v1, :cond_25

    #@1d
    .line 118
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@1f
    const-string v2, "Invalid destinationAddress"

    #@21
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@24
    throw v1

    #@25
    .line 122
    :cond_25
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@28
    move-result v1

    #@29
    if-eqz v1, :cond_3b

    #@2b
    const-string v1, "allow_sending_empty_sms"

    #@2d
    invoke-static {v2, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@30
    move-result v1

    #@31
    if-nez v1, :cond_3b

    #@33
    .line 123
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@35
    const-string v2, "Invalid message body"

    #@37
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@3a
    throw v1

    #@3b
    .line 128
    :cond_3b
    :try_start_3b
    const-string v1, "isms"

    #@3d
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@40
    move-result-object v1

    #@41
    invoke-static {v1}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@44
    move-result-object v0

    #@45
    .line 129
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_81

    #@47
    .line 131
    sget v1, Landroid/telephony/SmsManager;->vp:I

    #@49
    if-lez v1, :cond_50

    #@4b
    .line 132
    sget v1, Landroid/telephony/SmsManager;->vp:I

    #@4d
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/ISms;->setMultipartTextValidityPeriod(I)V

    #@50
    .line 137
    :cond_50
    const/4 v1, 0x0

    #@51
    const-string v2, "cdma_priority_indicator"

    #@53
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@56
    move-result v1

    #@57
    if-eqz v1, :cond_5e

    #@59
    .line 138
    sget v1, Landroid/telephony/SmsManager;->mSubmitPriority:I

    #@5b
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/ISms;->setSmsPriority(I)V

    #@5e
    .line 143
    :cond_5e
    const/4 v1, 0x0

    #@5f
    const-string v2, "support_sprint_sms_roaming_guard"

    #@61
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@64
    move-result v1

    #@65
    if-eqz v1, :cond_6c

    #@67
    .line 144
    sget-boolean v1, Landroid/telephony/SmsManager;->mSubmitIsRoaming:Z

    #@69
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/ISms;->setSmsIsRoaming(Z)V

    #@6c
    .line 148
    :cond_6c
    const-string v1, "sendTextMessage(), SmsManager --> IccSmsInterfaceManager"

    #@6e
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->f(Ljava/lang/String;)I

    #@71
    move-object v1, p1

    #@72
    move-object v2, p2

    #@73
    move-object v3, p3

    #@74
    move-object v4, p4

    #@75
    move-object v5, p5

    #@76
    .line 149
    invoke-interface/range {v0 .. v5}, Lcom/android/internal/telephony/ISms;->sendText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    :try_end_79
    .catch Landroid/os/RemoteException; {:try_start_3b .. :try_end_79} :catch_7a

    #@79
    goto :goto_16

    #@7a
    .line 155
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :catch_7a
    move-exception v6

    #@7b
    .line 158
    .local v6, ex:Landroid/os/RemoteException;
    const-string v1, "sendTextMessage(), RemoteException"

    #@7d
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@80
    goto :goto_16

    #@81
    .line 152
    .end local v6           #ex:Landroid/os/RemoteException;
    .restart local v0       #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_81
    const/4 v1, 0x1

    #@82
    :try_start_82
    invoke-static {p4, v1}, Landroid/telephony/SmsManager;->sendExceptionbySentIntent(Landroid/app/PendingIntent;I)V
    :try_end_85
    .catch Landroid/os/RemoteException; {:try_start_82 .. :try_end_85} :catch_7a

    #@85
    goto :goto_16
.end method

.method public sendTextMessageLge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)V
    .registers 17
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "replyAddr"

    #@0
    .prologue
    .line 426
    const/4 v7, -0x1

    #@1
    const/4 v8, 0x0

    #@2
    const/4 v9, 0x0

    #@3
    move-object v0, p0

    #@4
    move-object v1, p1

    #@5
    move-object v2, p2

    #@6
    move-object v3, p3

    #@7
    move-object v4, p4

    #@8
    move-object v5, p5

    #@9
    move-object/from16 v6, p6

    #@b
    invoke-virtual/range {v0 .. v9}, Landroid/telephony/SmsManager;->sendTextMessageLge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;III)V

    #@e
    .line 427
    return-void
.end method

.method public sendTextMessageLge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;III)V
    .registers 21
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "replyAddr"
    .parameter "confirmRead"
    .parameter "replyOption"
    .parameter "protocolId"

    #@0
    .prologue
    .line 435
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_e

    #@6
    .line 436
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v2, "Invalid destinationAddress"

    #@a
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v1

    #@e
    .line 439
    :cond_e
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_1c

    #@14
    .line 440
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@16
    const-string v2, "Invalid message body"

    #@18
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v1

    #@1c
    .line 444
    :cond_1c
    :try_start_1c
    const-string v1, "isms"

    #@1e
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@21
    move-result-object v1

    #@22
    invoke-static {v1}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@25
    move-result-object v0

    #@26
    .line 445
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_3f

    #@28
    .line 446
    const-string v1, "sendTextMessageLge(), sendTextMessageLge > sendTextLge"

    #@2a
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->v(Ljava/lang/String;)I

    #@2d
    move-object v1, p1

    #@2e
    move-object v2, p2

    #@2f
    move-object v3, p3

    #@30
    move-object v4, p4

    #@31
    move-object/from16 v5, p5

    #@33
    move-object/from16 v6, p6

    #@35
    move/from16 v7, p7

    #@37
    move/from16 v8, p8

    #@39
    move/from16 v9, p9

    #@3b
    .line 447
    invoke-interface/range {v0 .. v9}, Lcom/android/internal/telephony/ISms;->sendTextLge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;III)V

    #@3e
    .line 459
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :goto_3e
    return-void

    #@3f
    .line 450
    .restart local v0       #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_3f
    const/4 v1, 0x1

    #@40
    invoke-static {p4, v1}, Landroid/telephony/SmsManager;->sendExceptionbySentIntent(Landroid/app/PendingIntent;I)V
    :try_end_43
    .catch Landroid/os/RemoteException; {:try_start_1c .. :try_end_43} :catch_44

    #@43
    goto :goto_3e

    #@44
    .line 453
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :catch_44
    move-exception v10

    #@45
    .line 456
    .local v10, ex:Landroid/os/RemoteException;
    const-string v1, "sendTextMessageLge(), RemoteException"

    #@47
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@4a
    goto :goto_3e
.end method

.method public sendTextMessageMoreLge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)V
    .registers 17
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "replyAddress"

    #@0
    .prologue
    .line 535
    const/4 v7, -0x1

    #@1
    const/4 v8, 0x0

    #@2
    const/4 v9, 0x0

    #@3
    move-object v0, p0

    #@4
    move-object v1, p1

    #@5
    move-object v2, p2

    #@6
    move-object v3, p3

    #@7
    move-object v4, p4

    #@8
    move-object v5, p5

    #@9
    move-object/from16 v6, p6

    #@b
    invoke-virtual/range {v0 .. v9}, Landroid/telephony/SmsManager;->sendTextMessageMoreLge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;III)V

    #@e
    .line 536
    return-void
.end method

.method public sendTextMessageMoreLge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;III)V
    .registers 21
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "replyAddress"
    .parameter "confirmRead"
    .parameter "replyOption"
    .parameter "protocolId"

    #@0
    .prologue
    .line 544
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_e

    #@6
    .line 545
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v2, "Invalid destinationAddress"

    #@a
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v1

    #@e
    .line 548
    :cond_e
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_1c

    #@14
    .line 549
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@16
    const-string v2, "Invalid message body"

    #@18
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v1

    #@1c
    .line 553
    :cond_1c
    :try_start_1c
    const-string v1, "isms"

    #@1e
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@21
    move-result-object v1

    #@22
    invoke-static {v1}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@25
    move-result-object v0

    #@26
    .line 554
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_3a

    #@28
    move-object v1, p1

    #@29
    move-object v2, p2

    #@2a
    move-object v3, p3

    #@2b
    move-object v4, p4

    #@2c
    move-object/from16 v5, p5

    #@2e
    move-object/from16 v6, p6

    #@30
    move/from16 v7, p7

    #@32
    move/from16 v8, p8

    #@34
    move/from16 v9, p9

    #@36
    .line 555
    invoke-interface/range {v0 .. v9}, Lcom/android/internal/telephony/ISms;->sendTextMoreLge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;III)V

    #@39
    .line 568
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :goto_39
    return-void

    #@3a
    .line 558
    .restart local v0       #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_3a
    const/4 v1, 0x1

    #@3b
    invoke-static {p4, v1}, Landroid/telephony/SmsManager;->sendExceptionbySentIntent(Landroid/app/PendingIntent;I)V
    :try_end_3e
    .catch Landroid/os/RemoteException; {:try_start_1c .. :try_end_3e} :catch_3f

    #@3e
    goto :goto_39

    #@3f
    .line 562
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :catch_3f
    move-exception v10

    #@40
    .line 565
    .local v10, ex:Landroid/os/RemoteException;
    const-string v1, "sendTextMessageMoreLge(), RemoteException"

    #@42
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@45
    goto :goto_39
.end method

.method public setCharacterEncoding(Ljava/lang/String;)Z
    .registers 4
    .parameter "encoding"

    #@0
    .prologue
    .line 1826
    const/4 v0, 0x0

    #@1
    .line 1827
    .local v0, retResult:Z
    const-string v1, "1"

    #@3
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6
    move-result v1

    #@7
    if-nez v1, :cond_11

    #@9
    const-string v1, "0"

    #@b
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_17

    #@11
    .line 1828
    :cond_11
    const-string v1, "persist.gsm.sms.forcegsm7"

    #@13
    invoke-static {v1, p1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@16
    .line 1829
    const/4 v0, 0x1

    #@17
    .line 1831
    :cond_17
    return v0
.end method

.method public setServiceCenterAddress(Ljava/lang/String;)Z
    .registers 5
    .parameter "smsc"

    #@0
    .prologue
    .line 1393
    const/4 v1, 0x0

    #@1
    .line 1395
    .local v1, success:Z
    :try_start_1
    const-string v2, "isms"

    #@3
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v2

    #@7
    invoke-static {v2}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@a
    move-result-object v0

    #@b
    .line 1396
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_11

    #@d
    .line 1397
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ISms;->setSmscenterAddress(Ljava/lang/String;)Z
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_10} :catch_12

    #@10
    move-result v1

    #@11
    .line 1402
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_11
    :goto_11
    return v1

    #@12
    .line 1399
    :catch_12
    move-exception v2

    #@13
    goto :goto_11
.end method

.method public setSmsIsRoaming(Z)V
    .registers 2
    .parameter "isRoaming"

    #@0
    .prologue
    .line 1989
    sput-boolean p1, Landroid/telephony/SmsManager;->mSubmitIsRoaming:Z

    #@2
    .line 1990
    return-void
.end method

.method public setSmsPriority(I)V
    .registers 2
    .parameter "priority"

    #@0
    .prologue
    .line 808
    sput p1, Landroid/telephony/SmsManager;->mSubmitPriority:I

    #@2
    .line 809
    return-void
.end method

.method public setUiccType(I)V
    .registers 5
    .parameter "uiccType"

    #@0
    .prologue
    .line 1013
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "setUiccType(), uiccType: "

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@16
    .line 1015
    :try_start_16
    const-string v1, "isms"

    #@18
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v1}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@1f
    move-result-object v0

    #@20
    .line 1016
    .local v0, simISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_25

    #@22
    .line 1017
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ISms;->setUiccType(I)V
    :try_end_25
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_25} :catch_26

    #@25
    .line 1022
    .end local v0           #simISms:Lcom/android/internal/telephony/ISms;
    :cond_25
    :goto_25
    return-void

    #@26
    .line 1019
    :catch_26
    move-exception v1

    #@27
    goto :goto_25
.end method

.method public setValidityPeriod(I)V
    .registers 2
    .parameter "validityperiod"

    #@0
    .prologue
    .line 1743
    sput p1, Landroid/telephony/SmsManager;->vp:I

    #@2
    .line 1744
    return-void
.end method

.method public startTestCase(Ljava/lang/String;I)I
    .registers 5
    .parameter "pdu"
    .parameter "num"

    #@0
    .prologue
    .line 2005
    :try_start_0
    const-string v1, "startTestCase(), [KDDI] SMSManager"

    #@2
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5
    .line 2006
    const-string v1, "isms"

    #@7
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@a
    move-result-object v1

    #@b
    invoke-static {v1}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@e
    move-result-object v0

    #@f
    .line 2007
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_14

    #@11
    .line 2008
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ISms;->startTestCase(Ljava/lang/String;I)V
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_14} :catch_16

    #@14
    .line 2012
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_14
    :goto_14
    const/4 v1, 0x0

    #@15
    return v1

    #@16
    .line 2010
    :catch_16
    move-exception v1

    #@17
    goto :goto_14
.end method

.method public updateMessageOnIcc(II[B)Z
    .registers 8
    .parameter "messageIndex"
    .parameter "newStatus"
    .parameter "pdu"

    #@0
    .prologue
    .line 1269
    const/4 v1, 0x0

    #@1
    .line 1270
    .local v1, success:Z
    new-instance v2, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v3, "updateMessageOnIcc(), messageIndex = "

    #@8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@17
    .line 1271
    new-instance v2, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v3, "updateMessageOnIcc(), newStatus = "

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@2d
    .line 1273
    :try_start_2d
    const-string v2, "isms"

    #@2f
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@32
    move-result-object v2

    #@33
    invoke-static {v2}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@36
    move-result-object v0

    #@37
    .line 1274
    .local v0, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v0, :cond_42

    #@39
    .line 1275
    const-string v2, "updateMessageOnIcc(), SmsManager --> IccSmsInterfaceManager"

    #@3b
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->f(Ljava/lang/String;)I

    #@3e
    .line 1276
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/ISms;->updateMessageOnIccEf(II[B)Z
    :try_end_41
    .catch Landroid/os/RemoteException; {:try_start_2d .. :try_end_41} :catch_59

    #@41
    move-result v1

    #@42
    .line 1281
    .end local v0           #iccISms:Lcom/android/internal/telephony/ISms;
    :cond_42
    :goto_42
    new-instance v2, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v3, "updateMessageOnIcc(), success = "

    #@49
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v2

    #@4d
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v2

    #@55
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@58
    .line 1282
    return v1

    #@59
    .line 1278
    :catch_59
    move-exception v2

    #@5a
    goto :goto_42
.end method

.method public updateSmsOnSimReadStatus(IZ)Z
    .registers 6
    .parameter "index"
    .parameter "read"

    #@0
    .prologue
    .line 1779
    :try_start_0
    const-string v2, "isms"

    #@2
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v2

    #@6
    invoke-static {v2}, Lcom/android/internal/telephony/ISms$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ISms;

    #@9
    move-result-object v1

    #@a
    .line 1780
    .local v1, iccISms:Lcom/android/internal/telephony/ISms;
    if-eqz v1, :cond_17

    #@c
    .line 1781
    invoke-interface {v1, p1, p2}, Lcom/android/internal/telephony/ISms;->updateSmsOnSimReadStatus(IZ)Z
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_f} :catch_11

    #@f
    move-result v2

    #@10
    .line 1786
    .end local v1           #iccISms:Lcom/android/internal/telephony/ISms;
    :goto_10
    return v2

    #@11
    .line 1783
    :catch_11
    move-exception v0

    #@12
    .line 1784
    .local v0, ex:Landroid/os/RemoteException;
    const-string v2, "updateSmsOnSimReadStatus(), RemoteException"

    #@14
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@17
    .line 1786
    .end local v0           #ex:Landroid/os/RemoteException;
    :cond_17
    const/4 v2, 0x0

    #@18
    goto :goto_10
.end method
